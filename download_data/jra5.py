# python script to download selected files from rda.ucar.edu
#
import sys
import os
import urllib.request, urllib.error, urllib.parse
import http.cookiejar
import time
#
# if (len(sys.argv) != 2):
#     print("usage: " + sys.argv[0] + " [-q] password_on_RDA_webserver")
#     print("-q suppresses the progress message for each file that is downloaded")
#     sys.exit(1)
# #
# print(1)
# passwd_idx = 1
# verbose = True
# if (len(sys.argv) == 3 and sys.argv[1] == "-q"):
#     print(2)
#     passwd_idx = 2
#     verbose = False
#

def get_cookie():
    cj = http.cookiejar.MozillaCookieJar()
    opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
    #
    # check for existing cookies file and authenticate if necessary
    # do_authentication = False
    # if (os.path.isfile("auth.rda.ucar.edu")):
    #     cj.load("auth.rda.ucar.edu", False, True)
    #     print(4)
    #     for cookie in cj:
    #         if (cookie.name == "sess" and cookie.is_expired()):
    #             do_authentication = True
    # else:
    #     print(5)
    do_authentication = True
    if (do_authentication):

        data = {"email": "838324938@qq.com", "password": "rreCtUX2", "action": "login"}
        postdata = urllib.parse.urlencode(data)
        postdata = postdata.encode('ascii')
        login = opener.open("https://rda.ucar.edu/cgi-bin/login",
                            data=postdata)
        #data="email=838324938@qq.com&password=rreCtUX2&action=login"
        # save the authentication cookies for future downloads
        # NOTE! - cookies are saved for future sessions because overly-frequent authentication to our server can cause your data access to be blocked
        cj.clear_session_cookies()
        cj.save("auth.rda.ucar.edu", True, True)
        return opener
    #
    # download the data file(s)
listoffiles =["fcst_mdl/1990/fcst_mdl.034_vgrd.reg_tl319.1990011100_1990012018"]
opener = get_cookie()
for file in listoffiles:
    idx = file.rfind("/")
    if (idx > 0):
        ofile = file[idx + 1:]
    else:
        ofile = file
    # if (verbose):
    #     print(11)
    #     sys.stdout.write("downloading " + ofile + "...")
    #     sys.stdout.flush()
    flag = True
    while flag:
        try:
            infile = opener.open("http://rda.ucar.edu/data/ds628.0/" + file)
            code = infile.code
            print(code)
            if code != 200:
                print("休眠五分钟……重新获取cookie")
                time.sleep(30)
                opener = get_cookie()
            else:
                outfile = open(ofile, "wb")
                outfile.write(infile.read())
                outfile.close()
                flag = False
        except Exception as e:
            print(e)
            print("休眠五分钟……重新获取cookie")
            time.sleep(30)
            opener = get_cookie()
    # if (verbose):
    #     sys.stdout.write("done.\n")
