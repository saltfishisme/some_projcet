import os
import xarray as xr
import numpy as np
import pandas as pd
# #1.1 get all file names and addresses from wget.sh files.
#
# delete_error_file = True
# path_wget_sh_file = r'G:\cmip6_seaice\NEW/'
# keyword_cmip6FileName = ['_']  # skip if keyword is not in fileName
#
# file_name = []
# file_address = []
#
# # loop all .sh file and extract address
# for i_wget_file in os.listdir(path_wget_sh_file):
#     if not '.sh' in i_wget_file:
#         continue
#     print(i_wget_file)
#     with open(path_wget_sh_file+i_wget_file, 'r', encoding='utf-8') as file:
#         wget_sh_File = file.read()
#     for i_findNC in wget_sh_File.split("'"):
#         if not all(substring in i_findNC for substring in keyword_cmip6FileName):
#             continue
#         if '.nc' in i_findNC[-3:]:
#             if 'http' in i_findNC:
#                 file_address.append(i_findNC)
#             else:
#                 file_name.append(i_findNC)
# cmip6_file_address = pd.DataFrame(file_address, index=file_name)
#
# # loop all .sh file and extract address
# fileAll = os.listdir(path_wget_sh_file)
# missingData = []
# cmip6_z500 = []
# for i_file in fileAll:
#     if '.hdf' in i_file:
#         os.rename(path_wget_sh_file + i_file, path_wget_sh_file + i_file.replace('.hdf', '.nc'))
#         i_file = i_file.replace('.hdf', '.nc')
#
#     if '_2.nc' in i_file:
#         print(i_file.replace('_2.nc', '.nc'))
#         if os.path.exists(path_wget_sh_file+i_file.replace('_2.nc', '.nc')):
#             os.remove(path_wget_sh_file+i_file)
#         else:
#             os.rename(path_wget_sh_file+i_file, path_wget_sh_file+i_file.replace('_2.nc', '.nc'))
#         i_file =  i_file.replace('_2.nc', '.nc')
#     if not all(substring in i_file for substring in keyword_cmip6FileName):
#         continue
#     if not '.nc' in i_file:
#         continue
#     try:
#         xr.open_dataset(path_wget_sh_file + i_file)
#         cmip6_z500.append(i_file)
#     except:
#         if delete_error_file:
#             os.remove(path_wget_sh_file + i_file)
#         missingData.append(i_file)
#
# # missingData = np.loadtxt('missingData_cimp6Z500.txt', dtype='str')[-15:]
# # cmip6_z500 =np.loadtxt('cimp6_Z500.txt', dtype='str')
#
# missingData2 = list(set(file_name)-set(cmip6_z500))
# missingData = cmip6_file_address.loc[list(dict.fromkeys(list(missingData)+missingData2))]
#
#
# np.savetxt(path_wget_sh_file+'missingData_Address.txt', missingData[0],fmt='%s')
# np.savetxt(path_wget_sh_file+r'cimp6_download.txt', cmip6_z500, fmt='%s')
#
# # file = np.loadtxt(path_wget_sh_file+'missingData_Address.txt', dtype=str)
# # new_file = []
# # for i in file:
# #     new_file.append('start '+i)
# # np.savetxt(path_wget_sh_file+'bash_missingData_Address.txt', new_file,fmt='%s')
# exit()
#
#
#
# """
# 1, get CMIP6 data Addresses from .sh file.
# """
# path_wget_sh_file = r'G:\cmip6_seaice\tas_with_hist/'
# keyword_cmip6FileName = ['']  # skip if keyword is not in fileName
#
# file_name = []
# file_address = []
# for i_wget_file in os.listdir(path_wget_sh_file):
#     if not '.sh' in i_wget_file:
#         continue
#     with open(path_wget_sh_file+i_wget_file, 'r', encoding='utf-8') as file:
#         wget_sh_File = file.read()
#     for i_findNC in wget_sh_File.split("'"):
#         if not all(substring in i_findNC for substring in keyword_cmip6FileName):
#             continue
#         if '.nc' in i_findNC[-3:]:
#             if 'http' in i_findNC:
#                 file_address.append(i_findNC)
#             else:
#                 file_name.append(i_findNC)
# cmip6_file_address = pd.DataFrame(file_address, index=file_name)
# np.savetxt(path_wget_sh_file+'Address.txt', cmip6_file_address[0],fmt='%s')
#
# exit()

"""
===============================
combine cmip6 data
model-ssp126-r1i1p1f1-**time**.nc
===============================
"""

path_cmip6 = r'G:\cmip6_seaice\NEW/'
path_concat_cmip6 = r'G:\cmip6_seaice\zg500/'
cmip6_filename = os.listdir(path_cmip6)
cmip6_filename_splite = []
for i in cmip6_filename:
    sp = i.split('_')
    if len(sp) == 7:
        cmip6_filename_splite.append(sp)

cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
print(cmip6_filename_splite)
cmip6_filename_use = cmip6_filename_splite[(cmip6_filename_splite['0'] == 'zg')]
# cmip6_filename_use = cmip6_filename_splite[(cmip6_filename_splite['0'] == 'sivoln')
#                                            & (cmip6_filename_splite['1'] == 'Amon')]
cmip6_filename_missing = []
print('begin')
def find_file_name(x):
    file_titile =  ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'])]
    file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
    # get period of cmip6 file
    print(file)
    period = x['6']
    period_year = []
    period_sort = []
    for i_period in period:
        i_period_year_begin = int(i_period[:4])
        period_sort.append(i_period_year_begin)
        i_period_year_end = int(i_period[7:11])
        period_year.extend(np.arange(i_period_year_begin, i_period_year_end + 1))

    period_year_pd = np.arange(np.min(period_year), np.max(period_year) + 1)

    if len(period_year_pd) != len(period_year):
        print(period_year_pd)
        print(period_year)
        print(len(period_year_pd), len(period_year))
        print(file)
        cmip6_filename_missing.append(file)
    print(file_titile[0])
    if os.path.exists(path_concat_cmip6 +r'%s_'
                      r'%i01-%i12.nc' %
                      (file_titile[0],
                       np.min(period_year),
                       np.max(period_year))) | os.path.exists(path_concat_cmip6 +r'%s_'
        r'%i01-%i12.hdf' % (
            file_titile[0], np.min(period_year), np.max(period_year))):
        print('continue')
        return
    if len(file) == 1:
        import shutil
        shutil.copy(path_cmip6 + file[0], path_concat_cmip6)
        return
    period_sort = sorted(range(len(period_sort)), key=lambda k: period_sort[k])
    a = xr.open_dataset(path_cmip6 + file[period_sort[0]], use_cftime=True)
    a = a.sel(plev=50000)
    try:
        for i in period_sort[1:]:
            b = xr.open_dataset(path_cmip6 + file[i], use_cftime=True)
            b = b.sel(plev=50000)
            a = xr.concat([a, b], dim='time')
    except:
        too_big_file.append(file[0])
        return
    a.to_netcdf(path_concat_cmip6 + r'%s_%i01-%i12.nc' % (file_titile[0], np.min(period_year),
                                                          np.max(period_year)))

too_big_file = []
cmip6_filename_use.groupby([cmip6_filename_use['0'],
                            cmip6_filename_use['1'],
                            cmip6_filename_use['2'],
                            cmip6_filename_use['3'],
                            cmip6_filename_use['4'],
                            cmip6_filename_use['5']]).apply(
    lambda x: find_file_name(x))
print(too_big_file)
exit()
# for too big file

ds = xr.open_mfdataset(r'G:\cmip6_seaice\aa/*.nc', combine = 'nested', concat_dim = 'time')

ds.to_netcdf(r'G:\cmip6_seaice\nc_combined.nc') # Export netcdf file
exit()
exit()

"""
===============================
get model names in given path and check if hist, ssp285, 585 ara all contained.
and compare
===============================
"""


path_cmip6_1 = r'G:\cmip6_seaice\sivol/'
need_ssp = ['ssp245', 'ssp585', 'historical']

def get_model_name(path_cmip6):
    cmip6_filename = os.listdir(path_cmip6)
    cmip6_filename_splite = []
    for i in cmip6_filename:
        sp = i.split('_')
        if len(sp) == 7:
            cmip6_filename_splite.append(sp)

    cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
    cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
    cmip6_filename_use = pd.read_csv('cmip6_z500_split.csv', index_col=0)

    model_with_allssp = []

    def loop_groupby(x):
        need_ssp_01 = 1
        for i_need_ssp in need_ssp:
            if i_need_ssp in x['3'].values:
                continue
            else:
                need_ssp_01 = 0
        if need_ssp_01:
            model_with_allssp.extend(np.unique(x['2'].values))

    cmip6_filename_use.groupby([
                                cmip6_filename_use['2'],
                                ]).apply(
        lambda x: loop_groupby(x))
    return model_with_allssp

a_1 = get_model_name(r'G:\cmip6_seaice\sivol/')
print(a_1)
a = get_model_name(path_cmip6_1)
b = pd.read_csv(r"G:\cmip6_z500_split_tas.csv", index_col=0)
b = np.unique(b['2'].values)
c = os.listdir(r'G:\Atmospheric River Database\CMIP6/')
filtered_list = [item for item in a if item in b]
print(filtered_list)
print([item for item in c if item in a])
print([item for item in c if item in a_1])
exit()
"""
===============================
divided tas file into historical and ssp
===============================
"""

# files = os.listdir(r'G:\cmip6_seaice\tas_with_hist/')
# for i in files:
#     a = xr.open_dataset(r'G:\cmip6_seaice\tas_with_hist/'+i)
#     a.sel(time=slice('1850-01', '2014-12'))
#     i = i.replace('_mon_', '_Amon_')
#     a.to_netcdf(r'G:\cmip6_seaice\tas/%shistorical_r1i1p1f1_gn_185001-201412.nc'%i[:-9])
# for i in files:
#     a = xr.open_dataset(r'G:\cmip6_seaice\tas_with_hist/'+i)
#     a.sel(time=slice('2015-01', '2100-12'))
#     i = i.replace('_mon_', '_Amon_')
#     a.to_netcdf(r'G:\cmip6_seaice\tas/%s_r1i1p1f1_gn_185001-201412.nc'%i[:-3])
# exit()