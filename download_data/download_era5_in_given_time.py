from queue import Queue
from threading import Thread
import cdsapi
from time import time
import datetime
import os

download_address = r"/home/dell/work/Data/pressure_levels/corruption/"
vari_all=['geopotential', 'Vertical_velocity', 'v_component_of_wind', 'u_component_of_wind', 'temperature']
vari_save_all=['Hgt', 'wwnd', 'vwnd', 'uwnd', 'Temperature']
all_links = [
[['1979','02','06'],['1987','12','28'],['1987','12','19'],['1990','09','30'],['1992','07','16'],['1992','09','23'],['1994','09','19'],['1995','04','24'],['1997','02','06'],['1997','02','10'],['2013','02','19'],['1988','04','21'],['1990','01','08'],['1990','03','11'],['1991','06','09'],['1991','09','17'],['1997','09','23'],['1996','07','07']],
[['1979','06','01'],['1979','11','05'],['1980','01','06'],['1981','10','19'],['1987','12','02'],['1987','05','15'],['1987','12','05'],['1989','04','05'],['1989','04','30'],['1990','09','03'],['1994','03','14'],['1996','07','26'],['1997','03','26'],['1999','12','31'],['1980','11','28'],['1984','12','23'],['1986','11','15'],['1988','08','21'],['1994','05','20'],['1994','09','15'],['1998','10','10']],
[['1980','01','09'],['1980','01','10'],['1980','01','13'],['1980','01','13'],['1987','02','13'],['1989','04','11'],['1989','08','24'],['1990','04','28'],['1994','03','06'],['1994','09','20'],['1995','04','27'],['1997','03','27'],['1998','05','11'],['1986','11','21'],['1990','03','10'],['1990','03','24'],['1990','06','24'],['1990','09','03'],['1991','12','11'],['1990','10','15'],['1996','09','01'],['1999','02','16'],['2004','02','01']],
[['1979','04','26'],['1980','01','09'],['1984','02','29'],['1987','01','26'],['1990','09','22'],['1992','09','23'],['1994','12','31'],['1996','01','31'],['1997','02','06'],['1998','12','06'],['1999','06','10'],['1980','02','16'],['1981','05','02'],['1985','02','15'],['1986','05','12'],['1988','04','25'],['1990','06','08'],['1990','10','16'],['1991','12','16'],['1994','01','20'],['1995','07','29'],['1997','09','04'],['1999','02','16'],['2004','02','01']],
[['1979','04','24'],['1985','11','13'],['1987','12','12'],['1990','01','02'],['1997','02','14'],['1980','10','14'],['1983','04','27'],['1984','07','25'],['1986','05','19'],['1986','11','21'],['1988','04','20'],['1999','06','22'],['1991','12','05']]
             ]


def main_main(vari, vari_save_name, links):
    def downloadonefile(riqi):
        ts = time()

        filename = download_address+  '%s_%s.nc'%(vari_save_name, riqi)
        if(os.path.isfile(filename)): #如果存在文件名则返回
          print("ok",filename)
        else:
          print(filename)
          c = cdsapi.Client()
          c.retrieve(
              'reanalysis-data_to_mat_moisture_traj-pressure-levels',
              {
                  'product_type' : 'reanalysis',
                  'format'       : 'netcdf', # Supported format: grib and netcdf. Default: grib
                  'variable'     : vari, #其它变量名参见 https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels
                  'levelist': ['1', '2', '3', '5', '7', '10', '20', '30', '50', '70', '100', '125', '150', '175', '200',
                               '225', '250', '300', '350', '400', '450', '500', '550', '600', '650', '700', '750',
                               '775', '800', '825', '850', '875', '900', '925', '950', '975', '1000'],
                  'year'         : riqi[0:4],
                  'month'        : riqi[-4:-2],
                  'day'          : riqi[-2:],
                  'time':[
                      '00:00','01:00','02:00',
                      '03:00','04:00','05:00',
                      '06:00','07:00','08:00',
                      '09:00','10:00','11:00',
                      '12:00','13:00','14:00',
                      '15:00','16:00','17:00',
                      '18:00','19:00','20:00',
                      '21:00','22:00','23:00'
                      ],
                 'grid': [0.5, 0.5]
              },filename)

    #下载脚本
    class DownloadWorker(Thread):
       def __init__(self, queue):
           Thread.__init__(self)
           self.queue = queue

       def run(self):
           while True:
               # 从队列中获取任务并扩展tuple
               riqi = self.queue.get()
               downloadonefile(riqi)
               self.queue.task_done()

    #主程序
    def main():
       #起始时间
       ts = time()
       #
       # #起始日期
       # begin = datetime.date(2012,1,1)
       # end = datetime.date(2021,12,31)
       # d=begin
       # delta = datetime.timedelta(days=1)
       #
       # #建立下载日期序列
       # links = []
       # while d <= end:
       #     riqi=d.strftime("%Y%m%d")
       #     links.append(str(riqi))
       #     d += delta
       links = [i[0] + i[1] + i[2] for i in a]
       #创建一个主进程与工作进程通信
       queue = Queue()

       # 注意，每个用户同时最多接受4个request https://cds.climate.copernicus.eu/vision
       #创建四个工作线程
       for x in range(4):
           worker = DownloadWorker(queue)
           #将daemon设置为True将会使主线程退出，即使所有worker都阻塞了
           worker.daemon = True
           worker.start()

       #将任务以tuple的形式放入队列中
       for link in links:
           queue.put((link))

       #让主线程等待队列完成所有的任务
       queue.join()
       print('Took {}'.format(time() - ts))

    main()

for i,vari_i in enumerate(vari_all):
    main_main(vari_i, vari_save_all[i], all_links[i])
