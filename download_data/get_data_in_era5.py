import xarray as xr
import pandas as pd
import numpy as np
import scipy.io as scio






path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5x0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/daily_AR/'
time_loop = pd.date_range('1979-01-01', '2019-12-31', freq='1D')

time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

for sI_Season, s_Season in enumerate(time_Seaon_divide):
    s_time_loop_Season = time_loop[np.in1d(time_loop.month, s_Season)]

    s_data = []

    for s_time in s_time_loop_Season.strftime('%Y%m%d'):
        s_index_vivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time[:4], s_time))
        s_index_uivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time[:4], s_time))
        for s_time_daily in range(4):
            timestep = (s_time_daily+1)*6

            ivt_north = s_index_vivt['p72.162'].values[s_time_daily]
            ivt_east = s_index_uivt['p71.162'].values[s_time_daily]
            ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
            s_data.append(ivt_total)
    s_data = np.array(s_data, dtype='double')
    print(s_data.shape)
    scio.savemat(path_SaveData+'pdata_%i.mat'%sI_Season, {'data':s_data
                                                               })
    percentile = s_data.reshape(s_data.shape[0], s_data.shape[1]*s_data.shape[2])
    s_result_95 = np.percentile(percentile, 95, axis=0)
    s_result_95 = s_result_95.reshape(s_data.shape[1], s_data.shape[2])

    s_result_90 = np.percentile(percentile, 90, axis=0)
    s_result_90 = s_result_90.reshape(s_data.shape[1], s_data.shape[2])

    s_result_85 = np.percentile(percentile, 85, axis=0)
    s_result_85 = s_result_85.reshape(s_data.shape[1], s_data.shape[2])

    scio.savemat(path_SaveData+'percentile_%i.mat'%sI_Season, {'p85':np.array(s_result_85, dtype='double'),
                                                 'p90':np.array(s_result_90, dtype='double'),
                                                 'p95':np.array(s_result_95, dtype='double')
                                                 })


