import xarray as xr
import numpy as np
import pandas as pd
import sys
import os
from configparser import ConfigParser
import logging.handlers
from shutil import copy


def rename_Var_SaveName(path, row_var_savename, var_SaveName):
    if path[-1] != '/':
        path += '/'
    dic_first = os.listdir(path)
    for i_file in dic_first:
        if (i_file[-2:] == 'nc') & (row_var_savename in i_file):
            s_new_name = path+'%s'%(i_file.replace(row_var_savename, var_SaveName))
            os.rename(path+'%s'%(i_file), s_new_name)

# check all dic
    for i_year in range(1950, 2021+1):
        os.makedirs(path + '%i/' % i_year, exist_ok=True)
        dic_second = os.listdir(path + '%i/' % i_year)
        for i_file in dic_second:
            if (i_file[-2:] == 'nc') & (row_var_savename in i_file):
                s_new_name = path+'%i/%s'%(i_year, i_file.replace(row_var_savename, var_SaveName))
                os.rename(path+'%i/%s'%(i_year, i_file), s_new_name)

    return

def monthly_to_daily(DataPath):
    s_data = xr.open_dataset(DataPath)
    time_days = s_data.time.values
    for i_day in time_days:
        s_time = pd.to_datetime(i_day)

        # make sure path is exist
        if os.path.exists('%s%s.nc' % (
                DataPath[:-3], s_time.strftime('%d'))):
            continue

        s_data.sel(time=s_time.strftime('%Y-%m-%d')).to_netcdf(
            '%s%s.nc' % (
                DataPath[:-3], s_time.strftime('%d')))

def copy_to_Yearly_dic(year, path, var_SaveName):

    lack_file = []

    date = pd.date_range('%i0101'%year, '%i1231'%year)
    date = date.strftime('%Y%m%d')

    for i in date:
        print(path[:-5]+'%s.%s.nc'%(var_SaveName, i), path)
        try:
            copy(path[:-5]+'%s.%s.nc'%(var_SaveName, i), path)
        except:
            try:
                copy(path[:-5]+'%s_%s.nc'%(var_SaveName, i), path)
            except:
                lack_file.append('%s_%s.nc'%(var_SaveName, i))
                continue

    logger.error('%s does not exist' % (str(lack_file)))


########################################################
# make sure data has been divided into yearly directory.
def Step01_yearly_dic(var_dirName, var_SaveName):
    file_isin_first_dic = 0
    dic_first = os.listdir(path_MainData + resolution_file+ '/%s/' % (var_dirName))
    if len(dic_first) > 200:
        file_isin_first_dic = 1
    file_notin_second_dic = []
    if file_isin_first_dic:
        for year in range(1950, 2021):
            os.makedirs(path_MainData +resolution_file+ '/%s/%i/' % (var_dirName, year), exist_ok=True)
            dic_second = os.listdir(path_MainData + resolution_file+ '/%s/%i/' % (var_dirName, year))
            if len(dic_second) < 350:
                file_notin_second_dic.append(year)
                logger.info('Processing: copy to yearly dic  for %s' % (resolution_file+ '/%s/%i/' % (var_dirName, year)))
                print('Processing: copy to yearly dic  for %s' % (resolution_file+ '/%s/%i/' % (var_dirName, year)))
                copy_to_Yearly_dic(year, path_MainData + resolution_file+ '/%s/%i/' % (var_dirName, year), var_SaveName)
    return


def Step02_isexist(var_dirName, var_SaveName, year):
    date = pd.date_range('%i0101' % year, '%i1231' % year)
    date_str = date.strftime('%Y%m%d')

    for i in date_str:
        if os.path.exists(path_MainData + resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i)):
            continue
        elif os.path.exists(path_MainData + resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i[:6])):
            logger.info('Processing: Monthly to daily for %s' % (resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i[:6])))
            print('Processing: Monthly to daily for %s' % (resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i[:6])))
            monthly_to_daily(path_MainData + resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i[:6]))
            continue
        elif os.path.exists(path_MainData + resolution_file+ '/%s/%i/%s_%s.nc' % (var_dirName, year, var_SaveName, i)):
            logger.info('Processing: Modify _ to  for %s' % (resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i[:6])))
            print('Processing: Modify _ to  for %s' % (resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i[:6])))

            os.rename(path_MainData + resolution_file+ '/%s/%i/%s_%s.nc' % (var_dirName, year, var_SaveName, i),
                      path_MainData + resolution_file+ '/%s/%i/%s.%s.nc' % (var_dirName, year, var_SaveName, i))
            continue

#########################################################
path_MainData = '/home/linhaozhong/work/Data/ERA5/single_level/'
pressure_single = 'single'
resolution_file = '0.5X0.5'
vari_dirName_all=['Convective_precipitation', 'Large_scale_precipitation',
              'Evaporation',
              'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
              'Total_column_water_vapour', 'Total_precipitation']
vari_SaveName_all=['ConPreci', 'LSPreci',
               'Evapo',
               'EastWaterVapor', 'NorthWater', 'WaterVapor', 'Tp']

year_begin = 1950
year_end = 2020
"# ########  log file  Don't change################"

logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)
f_handler = logging.FileHandler('%s/OF_log_%s_%s.log' % (path_MainData, pressure_single, resolution_file), mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(levelname)s:  %(message)s"))
logger.addHandler(f_handler)
logger.info('############### reconstruction for %s_level %s ##################' % (pressure_single, resolution_file))


for iI_var in range(len(vari_SaveName_all)):
    for iYear in range(year_begin, year_end + 1):
        Step02_isexist(vari_dirName_all[iI_var], vari_SaveName_all[iI_var], iYear)

exit(0)

# arg = [['pressure_level', '0.5X0.5'], ['single_level', '0.5X0.5'], ['single_level', '1X1']]
#
# for i_arg in arg:
#     system = 'liunx'
#
#     """
#     get path_MainData based on .py path
#     !!!!!!! attention !!!!
#     path_MainData will join pressure/single_level after the logger file created.
#     such as /work/Data/ERA5/ to /work/Data/ERA5/single_level/
#     """
#
#     data_Path_r = sys.argv[0].split('/')[:-1]
#     path_MainData = ''
#     for i in data_Path_r:
#         path_MainData = path_MainData + '/' + i +'/'
#     if system == 'windows':
#         path_MainData = path_MainData[1:]
#
#     pressure_single, resolution_file = i_arg
#
#     "# ########  get var_dirName_all and var_SaveName_all  ################"
#
#     if pressure_single == 'single_level':
#         if resolution_file == '1X1':
#             var_dirName_all = ['2m_temperature','Mean_sea_level_pressure','Sea_Ice_Cover','Sea_surface_temperature','Snow_albedo','Snow_depth','Snow_evaporation','Snowfall','Snowmelt', 'Convective_precipitation', 'Large_scale_precipitation',
#                                 'Evaporation',
#                                 'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
#                                 'Total_column_water_vapour', 'Total_precipitation']
#
#             var_SaveName_all=['2m_temperature','Mean_sea_level_pressure','Sea_Ice_Cover','Sea_surface_temperature','Snow_albedo','Snow_depth','Snow_evaporation','Snowfall','Snowmelt','ConPreci', 'LSPreci',  'Evapo',  'EastWaterVapor', 'NorthWater', 'WaterVapor', 'Tp']
#         else:
#             var_dirName_all=['Convective_precipitation', 'Large_scale_precipitation',
#                               'Evaporation',
#                               'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
#                               'Total_column_water_vapour', 'Total_precipitation']
#             var_SaveName_all=['ConPreci', 'LSPreci',
#                                'Evapo',
#                                'EastWaterVapor', 'NorthWater', 'WaterVapor', 'Tp']
#     else:
#         var_dirName_all = ['Hgt','RH', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
#         var_SaveName_all=['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
#
#     "# ########  log file  ################"
#
#     logger = logging.getLogger('mylogger')
#     logger.setLevel(logging.DEBUG)
#     f_handler = logging.FileHandler('%s/OF_log_%s_%s.log' % (path_MainData, pressure_single, resolution_file), mode='w')
#     f_handler.setLevel(logging.DEBUG)
#     f_handler.setFormatter(logging.Formatter("%(levelname)s:  %(message)s"))
#     logger.addHandler(f_handler)
#     logger.info('############### reconstruction for %s_level %s ##################'%(pressure_single, resolution_file))
#
#
#     path_MainData = os.path.join(path_MainData, pressure_single)+'/'
#
#     if os.path.exists(path_MainData + resolution_file) != 1:
#         logger.error('%s does not exist' % (path_MainData + resolution_file))
#
#     """
#     ================================================================================
#     begin
#     path_MainData, pressure_single, resolution_file, var_dirName_all and var_SaveName_all
#     are the Variables needed.
#     ================================================================================
#     """
#
#     "# ########  rename  /only work when you need !!  ################"
#
#     if pressure_single == 'single_level':
#         rename_Var_SaveName('%s/Total_precipitation/'%(path_MainData+resolution_file),
#                             'TotPreci', 'tp')
#     else:
#         var_row = ['hgt', 'RH', 'temperature', 'uwnd', 'vwnd', 'wwnd']
#         var_new = ['Hgt', 'RH',  'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
#         for iI_var in range(len(var_new)):
#             if os.path.exists(path_MainData + resolution_file+ '/' + var_dirName_all[iI_var]):
#                 rename_Var_SaveName(path_MainData + resolution_file+ '/' +var_dirName_all[iI_var],
#                                     var_row[iI_var], var_new[iI_var])
#             else:
#                 logger.error(path_MainData + var_dirName_all[iI_var] + ' does not exist')
#
#
#     for i in range(len(var_dirName_all)):
#
#         Step01_yearly_dic(var_dirName_all[i], var_SaveName_all[i])
#         logger.info('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
#                      '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
#                      '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
#                      '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
#                      '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
#                      '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
#         for iYear in range(1950, 2021 + 1):
#             Step02_isexist(var_dirName_all[i], var_SaveName_all[i], iYear)
#
#
