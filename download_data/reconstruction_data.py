import xarray as xr

def monthly_to_daily(DataPath):
    s_data = xr.open_dataset(DataPath)
    time_days = s_data.time.values
    for i_day in time_days:
        s_time = pd.to_datetime(i_day)

        # make sure path is exist
        if os.path.exists('%s%s.nc' % (
                DataPath[:-3], s_time.strftime('%d'))):
            continue

        s_data.sel(time=s_time.strftime('%Y-%m-%d')).to_netcdf(
            '%s%s.nc' % (
                DataPath[:-3], s_time.strftime('%d')))




##################################  rename ##############################################
import shutil
import pandas as pd
import os

time_begin = 1979
time_end = 1994

row_data_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/Wwnd/1979_1994/'
row_Name = 'wwnd'
new_Name = 'Wwnd'

def year(year):
   date = pd.date_range('%i0101'%year, '%i1231'%year)
   date = date.strftime('%Y%m%d')
   for i in date:
       os.rename(row_data_Path+'%s_%s.nc'%(row_Name, i),
                 row_data_Path+'%s.%s.nc'%(new_Name, i))
for i in range(time_begin, time_end+1):
   year(i)

exit(0)
###################################  rename ##############################################
import shutil
import pandas as pd
import os
from shutil import copy

time_begin = 1950
time_end = 2021

reconstruction_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
row_data_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'


vari_dirName_all = ['Hgt','relative_humidity', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']

vari_SaveName_all=['Hgt_', 'RH_', 'Tem_', 'Uwnd_', 'Vwnd_', 'Wwnd_']
vari_newName_all=['Hgt.', 'RH.', 'Tem.', 'Uwnd.', 'Vwnd.', 'Wwnd.']

def year(vari_dirName, vari_SaveName, vari_newName, year):

    date = pd.date_range('%i0101'%year, '%i1231'%year)
    date = date.strftime('%Y%m%d')

    for i in date:
        try:
            os.rename(reconstruction_Path+vari_dirName +'/%s'%(i[:4])+'/%s%s.nc'%(vari_SaveName, i), reconstruction_Path+vari_dirName +'/%s'%(i[:4])+'/%s%s.nc'%(vari_newName, i))
        except:
            continue
for i_vari in range(len(vari_SaveName_all)):
    for i in range(time_begin, time_end+1):
        year(vari_dirName_all[i_vari], vari_SaveName_all[i_vari], vari_newName_all[i_vari], i)


exit(0)


import shutil
import pandas as pd
import os

time_begin = 1950
time_end = 2020

row_data_Path = r'/home/linhaozhong/work/Data/ERA5/single_level/1x1/Total_column_water_vapour/'
row_Name = 'WaterVapor'
new_Name = 'WaterVapor.'

def year(year):
    date = pd.date_range('%i0101'%year, '%i1231'%year)
    date = date.strftime('%Y%m%d')
    for i in date:
        os.rename(row_data_Path+'%s_%s.nc'%(row_Name, i),
                  row_data_Path+'%s_%s.nc'%(new_Name, i))
for i in range(time_begin, time_end+1):
    year(i)

exit(0)
###################################  copy to yearly file  ##############################################

import shutil
import pandas as pd
import os
from shutil import copy

time_begin = 1950
time_end = 2021

reconstruction_Path = r'/home/linhaozhong/work/Data/ERA5/single_level/1X1/'
row_data_Path = r'/home/linhaozhong/work/Data/ERA5/single_level/1X1/'


vari_dirName_all = ['2m_temperature','Mean_sea_level_pressure','Sea_Ice_Cover','Sea_surface_temperature','Snow_albedo','Snow_depth','Snow_evaporation','Snowfall','Snowmelt', 'Convective_precipitation', 'Large_scale_precipitation',
                    'Evaporation',
                    'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
                    'Total_column_water_vapour', 'Total_precipitation']

vari_SaveName_all=['2m_temperature','Mean_sea_level_pressure','Sea_Ice_Cover','Sea_surface_temperature','Snow_albedo','Snow_depth','Snow_evaporation','Snowfall','Snowmelt','ConPreci', 'LSPreci',  'Evapo',  'EastWaterVapor', 'NorthWater', 'WaterVapor', 'Tp']

def year(vari_dirName, vari_SaveName, year):
    os.makedirs(reconstruction_Path +vari_dirName +'/'+ str(year), exist_ok=True)

    date = pd.date_range('%i0101'%year, '%i1231'%year)
    date = date.strftime('%Y%m%d')

    for i in date:
        try:
            copy(row_data_Path+vari_dirName +'/'+'%s.%s.nc'%(vari_SaveName, i), reconstruction_Path +vari_dirName +'/'+ str(year) + '/')
        except:
            continue
for i_vari in range(len(vari_SaveName_all)):
    for i in range(time_begin, time_end+1):
        year(vari_dirName_all[i_vari], vari_SaveName_all[i_vari], i)


###################################  delete to yearly file  ##############################################

import shutil
import pandas as pd
import os
from shutil import move

time_begin = 1950
time_end = 2020


row_data_Path = r'/home/linhaozhong/work/Data/ERA5/single_level/1X1/'

address = ['Convective_precipitation', 'Large_scale_precipitation',
           'Evaporation',
           'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
           'Total_column_water_vapour']

namelist = ['ConPreci', 'LSPreci',
            'Evapo',
            'EastWaterVapor', 'NorthWater', 'WaterVapor']
def delete(vari_Dir, vari_Name):

    date = pd.date_range('%i0101'%time_begin, '%i1231'%time_end)
    date = date.strftime('%Y%m')

    for i in date:
        try:
            move(row_data_Path+'%s/%s/%s.%s.nc'%(vari_Dir, i[:4], vari_Name, i), '/home/linhaozhong/work/delete/')
        except:
            print(i)

for i in range(len(address)):
    year(address[i], namelist[i])
