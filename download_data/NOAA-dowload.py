from urllib import request
from bs4 import BeautifulSoup
import urllib
import re
import threading
import os
import pandas as pd
import socket

socket.setdefaulttimeout(240)#设置超时时间为120s，避免堵塞,根据下载文件大小和网络修改
year=str(1999)
sem = threading.Semaphore(60)  # 限制线程的最大数量为60个
ii = pd.date_range('19820101', periods=3, freq='24H').strftime("%m")  # 插入时间序列,输出格式为月份，%Y年用四位表示，%y年用两位表示
ii0 = pd.date_range('19820101', periods=3, freq='24H').strftime("%m%d")  # 插入时间序列输出格式为月份天
l=list(zip(ii,ii0))#将ii和ii0列表里的元素一一对应上，方便创建文件夹和建url目录

for tt in l:
    month =year+str(tt[:1])[2:4]
    day=year+str(tt[1:2])[2:6]
    aa = os.makedirs('E:\\NOAA\\' + year + '\\' + month + '\\' + day)  # 创建文件夹
    url= 'https://www.ncei.noaa.gov/data/climate-forecast-system/access/reforecast/6-hourly-flux-45-day-runs/'+year+'/'+month+'/'+day+'/'
    content =request.urlopen(url,timeout=240).read()#设置timeout为120S，防止被网站误认为在攻击
    soup=BeautifulSoup(content,'lxml')
    link=soup.find_all('a')
    reinfo = re.compile(r'(flxf[1-2][0-9][0-9][0-9][0-1][0-9][0-3][0-9][0-1][0-8].[1-2][0-9][0-9][0-9][0-1][0-9][0-3][0-9][0-1][0-8])\.grb2')  # 只下载netcdf文件，最简单的正则表达式
    link_dup = []
    URL1 = []
    for i in link:
        a = i.get('href')  # 获得链接
        if (type(a) == str):  # 去掉不是String类型的链接
            link_dup.append(a)
    link_all = list(set(link_dup))  # 去掉重复的链接
    for k in link_all:
        if (re.findall(reinfo, k)):  # 获得符合正则表达式的链接
            kk=url+k
            URL1.append(kk)
    def join(h):
        with sem:
            a=str(h)[120:]#文件名
            year1=str(h)[99:103]
            month1=str(h)[104:110]
            day1=str(h)[111:119]
            file_ = r'E:\\NOAA\\' + year1 + '\\' + month1 + '\\' + day1  # 存放下载文件的地址
            file_name = file_ + '\\' + a  # 保存的文件名
            try:
                urllib.request.urlretrieve(h, file_name)  # 获取文件
            except socket.timeout:
                count = 1
                while count <= 3:#三次尝试下载
                    try:
                        urllib.request.urlretrieve(h, file_name)  # 获取文件
                        break
                    except socket.timeout:
                        count += 1
                if count >3:
                    print("downloading fialed!")
    for h in URL1:
        t=threading.Thread(target=join,args=(h,))
        t.start()