import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import scipy.io
import pandas as pd
from scipy.interpolate import Rbf
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import cartopy.feature as cfeature
import matplotlib.path as mpath
import matplotlib.patches as mpatches
from matplotlib.path import Path
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import matplotlib.patches as mpatches
from shapely.geometry import Point, LineString,Polygon
import numpy as np

import nc4


def main():
    central_lon = 88
    central_lat = 40
    lat_boundary = 10
    alpha_constant = 0.95
    # data = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\figure3_1750-1899.mat')
    # # data = scipy.io.loadmat(r'D:\figure3_1900-2003.mat')
    # #
    # lat_small = np.squeeze(data['lat'])
    # lon_small = np.squeeze(data['lon'])
    #
    # lat = np.arange(91,-86.01, -1)
    # lon = np.arange(0,360.01,1)
    #
    # def regrid(sparse_grid, x, y, new_x, new_y):
    #     lon, lat = np.meshgrid(x, y)
    #     func = Rbf(lon, lat, sparse_grid, function='linear')
    #     new_x, new_y = np.meshgrid(new_x, new_y)
    #     data_new = func(new_x, new_y)
    #     return data_new
    #
    # import cartopy.util as cutil
    # hgt,_,_ = cutil.add_cyclic(data['f3a_M234_Hgt500'], lon_small, lat_small)
    # hgt_p,_,_  = cutil.add_cyclic(data['f3a_M234_Hgt500_p'], lon_small, lat_small)
    # t2m,_,_  = cutil.add_cyclic(data['f3a_M234_t2m'], lon_small, lat_small)
    # t2m_p, lon_small, lat_small = cutil.add_cyclic(data['f3a_M234_t2m_p'], lon_small, lat_small)
    #
    # hgt = regrid(hgt, lon_small, lat_small, lon, lat)
    # hgt_p  =regrid(hgt_p, lon_small, lat_small, lon, lat)
    # t2m  = regrid(t2m, lon_small, lat_small, lon, lat)
    # t2m_p = regrid(t2m_p, lon_small, lat_small, lon, lat)
    # print(lon.shape)
    #
    # scipy.io.savemat('test.mat', {'t2m':t2m, 't2m_p':t2m_p,
    #                               'hgt': hgt, 'hgt_p': hgt_p,
    #                               'lon':lon, 'lat':lat})

    data = scipy.io.loadmat('test.mat')
    t2m = data['t2m']
    t2m_p = data['t2m_p']
    hgt = data['hgt']
    hgt_p = data['hgt_p']
    lon = np.squeeze(data['lon'])

    lat = np.squeeze(data['lat'])
    lon2d, lat2d = np.meshgrid(lon,lat)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_lon, central_lat))
    # plt.subplots_adjust()
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))
    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/BlueWhiteOrangeRed.mat', 'BlueWhiteOrangeRed')

    # plot contour Hgt
    cf = ax.contour(lon, lat, hgt, levels=np.arange(-16,16+1,4),
               colors='black', linewidths=0.3,
               transform=ccrs.PlateCarree())
    ax.contour(lon, lat, hgt, levels=[0],
               colors='black', linewidths=0.7,
               transform=ccrs.PlateCarree())

    ############# plot contourf ##########
    levels = np.arange(-1, 1.001,0.05)
    import cmaps
    # contour =ax.contourf(lon, lat, t2m, levels=levels, cmap=cmaps.MPL_RdYlGn_r,
    #                      norm=colors.CenteredNorm(),extend='both',
    #                            transform=ccrs.PlateCarree(), alpha=1)
    #
    # # set alpha based on value
    # def custom_alpha(value, min_val, max_val):
    #     normalized_value = abs(value) / max(abs(min_val), abs(max_val))
    #     return 0 + alpha_constant * np.sqrt(normalized_value)
    #
    # for i, collection in enumerate(contour.collections):
    #     try: # extend == 'both', actual levels will larger than given levels
    #         alpha = custom_alpha(levels[i] ,levels.min(), levels.max())
    #         collection.set_alpha(alpha)
    #     except:
    #         print('ha')

    # # colorbar
    # cb_ax = fig.add_axes([0.2, 0.25, 0.6, 0.01])
    # cbar = plt.colorbar(contour, orientation="horizontal", cax=cb_ax)
    # cb_ax.set_xlabel('℃')

    # significance
    lon2d[t2m_p>0.1] = np.nan
    lat2d[t2m_p>0.1] = np.nan
    ax.scatter(lon2d, lat2d, s=1, color='gray', edgecolor='gray', linewidth=0.1
               , transform=ccrs.PlateCarree(), zorder=20)

    ax.set_global()
    ax.gridlines(linewidth=0.1,color='black')
    ax.background_img(name='ETOPO', resolution='high')
    # ax.set_extent([-6078070, 6078070, -6008070, 6008070], crs=ccrs.Orthographic(central_lon, central_lat))

    theta = np.linspace(0, 2*np.pi, 100)
    center, radius = [0.5, 0.5], 0.5
    verts = np.vstack([np.sin(theta), np.cos(theta)]).T
    circle = mpath.Path(verts * radius + center)
    ax.set_boundary(circle, transform=ax.transAxes)


    ranges = [30, 170, 35, 80]
    lon1 = np.linspace(ranges[0], ranges[1], 100)
    lat1 = np.full_like(lon1, ranges[2])
    lon2 = np.linspace(ranges[1], ranges[1], 100)
    lat2 = np.linspace(ranges[2], ranges[3], 100)
    lon3 = np.linspace(ranges[1], ranges[0], 100)
    lat3 = np.full_like(lon3, ranges[3])
    lon4 = np.linspace(ranges[0], ranges[0], 100)
    lat4 = np.linspace(ranges[3], ranges[2], 100)
    lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
    lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
    ax.plot(lons,
            lats,
            color='red', transform=ccrs.PlateCarree(), linewidth=1.2)

    coords = pd.read_csv(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire/Coordinates.csv')
    lat = coords['lat']
    lon = coords['lon']
    ax.scatter(lon, lat, s=12.5, c='green', marker='^', edgecolor='black',linewidths=0.4,
               transform=ccrs.PlateCarree())

    # plt.show()
    ##############################  re boundary ##############################
    # get the transAxes coordination's for a longitude to split earth
    # ax.plot(np.linspace(-180, 180, 100), [lat_boundary] * 100, color='black', linewidth=1, transform=ccrs.PlateCarree())


    def get_boundary():

        center, radius = [0.5, 0.5], 0.5
        num_points = 1000
        # 从 90°（π/2 弧度）开始的角度范围
        angle = np.linspace(3 * np.pi / 2, 3 * np.pi / 2 + 2 * np.pi, num_points)
        x = center[0] + radius * np.cos(angle)
        y = center[1] + radius * np.sin(angle)

        new_line_path = []
        for iI_xyLatLine, _ in enumerate(x):
            point_in_data = ax.transData.inverted().transform(ax.transAxes.transform((x[iI_xyLatLine], y[iI_xyLatLine])))
            x_mollw, y_mollw = ccrs.PlateCarree().transform_point(point_in_data[0], point_in_data[1], ax.projection)

            if not (np.isnan(x_mollw) | np.isnan(y_mollw)):
                new_line_path.append([x_mollw, y_mollw])
        line_path = np.array(new_line_path)

        x = np.squeeze(line_path[:,0] ) # x坐标
        y = np.squeeze(line_path[:,1])  # y坐标

        y_sorted_indices = np.argsort(abs(y-lat_boundary))
        selected_indices = [y_sorted_indices[0]]
        for idx in y_sorted_indices[1:]:
            if abs(idx - selected_indices[-1]) > 2:
                selected_indices.append(idx)
                break
        print(selected_indices)
        line_path = np.column_stack((x[np.min(selected_indices):np.max(selected_indices)+1],
                                           y[np.min(selected_indices):np.max(selected_indices)+1]))

        # ax.scatter([x[selected_indices[0]], x[selected_indices[1]]], [y[selected_indices[0]], y[selected_indices[1]]], color='red', transform=ccrs.PlateCarree())

        # ax.scatter(line_path[:,0], line_path[:,1], s=np.arange(line_path.shape[0]),transform=ccrs.PlateCarree())

        def cut_longitude(lon1, lon2):
            lon = np.arange(0, 360, 0.25)
            lon = np.roll(lon, (180-central_lon)*4) # set central_lon as centre

            from function_shared_Main import roll_longitude_from_359to_negative_180
            lon = roll_longitude_from_359to_negative_180(lon)
            arglon1 = np.argmin(abs(lon-lon1))
            arglon2 = np.argmin(abs(lon - lon2))

            lon = lon[np.min([arglon1,arglon2]):np.max([arglon1,arglon2])+1]
            return np.column_stack([lon,
                                    np.array([lat_boundary] * len(lon))])

        line_2 = cut_longitude(x[selected_indices[0]], x[selected_indices[1]])
        line_path = np.append(line_path, line_2, axis=0)
        # ax.scatter(line_path[:,0], line_path[:,1], s=np.arange(line_path.shape[0]),transform=ccrs.PlateCarree())

        def trans_to_transAxes(line_path):
            new_line_path = []
            for iI_xyLatLine in range(line_path.shape[0]):
                x_mollw, y_mollw = ax.projection.transform_point(line_path[iI_xyLatLine, 0], line_path[iI_xyLatLine, 1],
                                                                 ccrs.PlateCarree())
                # convert to display coordinates
                x_disp, y_disp = ax.transData.transform((x_mollw, y_mollw))
                # convert to axes coordinates
                x_disp, y_disp = ax.transAxes.inverted().transform((x_disp, y_disp))
                if not (np.isnan(x_disp) | np.isnan(y_disp)):
                    new_line_path.append([x_disp, y_disp])
            line_path = np.array(new_line_path)
            return line_path

        circle = mpath.Path(trans_to_transAxes(line_path))
        return circle

    circle = get_boundary()
    bd = ax.set_boundary(circle, transform=ax.transAxes)

    ax.set_title('central_lon, lat=%i %i\nlat_boundary=%i\nalpha=%0.3f'%(central_lon, central_lat, lat_boundary, alpha_constant),
                 fontsize=6)
    lh = ax.clabel(
        cf, fontsize=4,  # Typically best results when labelling line contours.
        colors=['gray'],
        inline=True,  # Cut the line where the label will be placed.
        fmt='%i',  # Labes as integers, with some extra space.
    )

    def trans_to_PlateCarree(points):
        if points.shape[0] == 1:
            t_position = ax.transData.inverted().transform(ax.transAxes.transform(points))
            x_mollw, y_mollw = ccrs.PlateCarree().transform_point(t_position[0], t_position[1], ax.projection)
            return ([x_mollw, y_mollw])
        else:
            new_points = []
            for point in points:
                t_position = ax.transData.inverted().transform(ax.transAxes.transform(point))
                x_mollw, y_mollw = ccrs.PlateCarree().transform_point(t_position[0], t_position[1], ax.projection)
                if not (np.isnan(x_mollw) | np.isnan(y_mollw)):
                    new_points.append([x_mollw, y_mollw])
            return np.array(new_points)

    def trans_to_transAxes(points):
        if points.shape[0] == 1:
            x_mollw, y_mollw = ax.projection.transform_point(points[0], points[1],
                                                             ccrs.PlateCarree())
            x_disp, y_disp = ax.transData.transform((x_mollw, y_mollw))
            x_disp, y_disp = ax.transAxes.inverted().transform((x_disp, y_disp))
            return ([x_disp, y_disp])
        else:
            new_line_path = []
            for iI_xyLatLine in range(points.shape[0]):
                x_mollw, y_mollw = ax.projection.transform_point(points[iI_xyLatLine, 0], points[iI_xyLatLine, 1],
                                                                 ccrs.PlateCarree())
                x_disp, y_disp = ax.transData.transform((x_mollw, y_mollw))
                x_disp, y_disp = ax.transAxes.inverted().transform((x_disp, y_disp))
                if not (np.isnan(x_disp) | np.isnan(y_disp)):
                    new_line_path.append([x_disp, y_disp])
            points = np.array(new_line_path)
            return points

    for t in lh:
        t_position = t.get_position()
        x_disp, y_disp = ax.transData.transform((t_position[0], t_position[1]))
        x_disp, y_disp = ax.transAxes.inverted().transform((x_disp, y_disp))
        if not circle.contains_point((x_disp, y_disp)):
            t.set_visible(False)

    plt.show()


if __name__ == '__main__':
    main()