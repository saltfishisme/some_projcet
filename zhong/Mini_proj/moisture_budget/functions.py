def get_centre_asia_range(data, lat, lon):
    import numpy as np
    lat_range = [56, 35]
    lon_range = [45, 100]
    arg_lat = np.where((lat <= lat_range[0]) & (lat >= lat_range[1]))
    arg_lon = np.where((lon >= lon_range[0]) & (lon <= lon_range[1]))
    return data[arg_lat, arg_lon]


def gradient(data):
    import metpy.calc as mpcalc
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd
    lat = np.arange(56, 35.25, -0.25)
    lon = np.arange(45, 100.25, 0.25)
    dx, dy = mpcalc.lat_lon_grid_deltas(lon, lat)
    dzdy, dzdx = mpcalc.gradient(data, deltas=(dy, dx))
    return dzdx + dzdy