import metpy.calc as mpcalc
import xarray as xr
import scipy.io as scio
import numpy as np
import pandas as pd
from metpy.units import units

def read_data(time):
    def get_centre_asia_range(data, lat, lon):
        return data[34:56, 45:101]

    def gradient(data):
        import metpy.calc as mpcalc
        import xarray as xr
        import scipy.io as scio
        import numpy as np
        import pandas as pd
        lat = np.arange(56, 34.25, -1)
        lon = np.arange(45, 100.25, 1)
        print(lat.shape, lon.shape)
        dx, dy = mpcalc.lat_lon_grid_deltas(lon, lat)
        dzdy, dzdx = mpcalc.gradient(data, deltas=(dy, dx))
        return dzdx + dzdy

    pe = scio.loadmat(addr+'/precipevap/Daily_pe_%s'%time)['PE']
    vp = scio.loadmat(addr+'/vapordata/Daily_vapor_%s'%time)['QPcol']
    lon = np.arange(0,360,1)
    lat =  np.arange(90,-91,-1)

    # P: mm
    p1 = pe['ACPCP'][0][0]
    p2 = pe['NCPCP'][0][0]
    P = get_centre_asia_range(p1+p2, lat, lon)
    print(P.shape)

    # E :mm
    E = get_centre_asia_range(pe['EV'][0][0], lat, lon)

    # total columns precipitable water :mm
    PWAT = get_centre_asia_range(vp['PWAT'][0][0], lat, lon)

    # vertically integrated moisture flux :kg m**-1 s**-1
    u = vp['UWVcol'][0][0]
    v = vp['VWVcol'][0][0]
    MF = get_centre_asia_range(u*v, lat, lon)
    Q = gradient(MF* units('kg/(m*s)'))

    return [PWAT, Q, E, P]

def read_Q(time):
    def get_centre_asia_range(data, lat, lon):
        import numpy as np
        lat_range = [56, 35]
        lon_range = [45, 100]
        arg_lat = np.where((lat <= lat_range[0]) & (lat >= lat_range[1]))
        arg_lon = np.where((lon >= lon_range[0]) & (lon <= lon_range[1]))

        return data[34:56, 45:101]

    def gradient(data):
        import metpy.calc as mpcalc
        import xarray as xr
        import scipy.io as scio
        import numpy as np
        import pandas as pd
        lat = np.arange(56, 34.25, -1)
        lon = np.arange(45, 100.25, 1)
        print(lat.shape, lon.shape)
        dx, dy = mpcalc.lat_lon_grid_deltas(lon, lat)
        dzdy, dzdx = mpcalc.gradient(data, deltas=(dy, dx))
        return dzdx + dzdy

    vp = scio.loadmat(addr+'/vapordata/Daily_vapor_%s'%time)['QPcol']
    lon = np.arange(0,360,1)
    lat =  np.arange(90,-91,-1)

    # vertically integrated moisture flux :kg m**-1 s**-1
    u = get_centre_asia_range(vp['UWVcol'][0][0], lat, lon)
    v = get_centre_asia_range(vp['VWVcol'][0][0], lat, lon)

    return [u,v]

addr = '/work/Data/MoistureTrackData/ERA_interim'
date_time = pd.date_range('1979-01-01', '2015-12-31', freq='1D')
SEASONS = [
    [12, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [9, 10, 11]
]


for seai, season in enumerate(SEASONS):
    sdate_time = date_time[date_time.month.isin(season)].strftime('%Y%m%d')
    sw = np.zeros([len(sdate_time), 22, 56])
    sQ = np.zeros([len(sdate_time), 22, 56])
    sE = np.zeros([len(sdate_time), 22, 56])
    sP = np.zeros([len(sdate_time), 22, 56])
    for i, si in enumerate(sdate_time):
        PWAT, Q, E, P = read_data(si)
        sw[i,:,:] = PWAT; sQ[i,:,:] = Q; sE[i,:,:] = E; sP[i,:,:] = P
    scio.savemat('/work/%i.mat'%seai, {'w':sw, 'Q':sQ, 'E':sE, 'P':sP})

# for seai, season in enumerate(SEASONS):
#     sdate_time = date_time[date_time.month.isin(season)].strftime('%Y%m%d')
#     print(sdate_time)
    # sE = np.zeros([len(sdate_time), 22, 56])
    # sP = np.zeros([len(sdate_time), 22, 56])
    # for i, si in enumerate(sdate_time):
    #     E, P = read_Q(si)
    #     sE[i,:,:] = E; sP[i,:,:] = P
    # scio.savemat('/work/Q_%i.mat'%seai, {'u':sE, 'v':sP})