#
# import xarray as xr
# import scipy.io as scio
# import numpy as np
# import pandas as pd
#
# # main_addr = r'D:\a_matlab\subregion_centre_asia_era0.25\output_sourcerange_int12hr_sub_centre_asia-25deg/'
# main_addr = r'/home/linhaozhong/work/subregion_centre_asia_era0.25/'
#
# row_data = xr.open_dataset(main_addr+'Moist1979_2019.nc')
# print(row_data)
# lon = row_data.lon.values[:,0].flatten()
# lat = row_data.lat.values[0,:].flatten()
#
# times = pd.date_range('19790101', '20191231', freq='1m')
#
# def get_dataarray(data):
#     foo = xr.DataArray(np.transpose(data, [0,2,1]), coords=[times, lat, lon], dims=['time', "lat", 'lon'])
#     return foo
#
# ds = xr.Dataset(
#     {
#         "PW_contrib": get_dataarray(row_data.PW_contrib.values),
#         "Prep_contrib": get_dataarray(row_data.Prep_contrib.values),
#         "RecyclingRatio": get_dataarray(row_data.RecyclingRatio.values),
#         "TrajInden": get_dataarray(row_data.TrajInden.values),
#
#
#         "Precipitable_water": xr.DataArray(row_data.variables['Precipitable water'].values, coords=[times], dims=['time']),
#         "Precipitation": xr.DataArray(row_data.Precipitation.values, coords=[times], dims=['time'])
#
#     }
# )
# print(ds)
# ds.to_netcdf(main_addr + 'Moist_1979_2019.nc')

import metpy.calc as mpcalc
import xarray as xr
import scipy.io as scio
import numpy as np
import pandas as pd
from metpy.units import units
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import scipy.io as scio
import cmaps
import numpy as np

def backup():
    import numpy as np
    import pandas as pd
    import xarray as xr
    import scipy.io as scio

    # main_addr = r'D:\a_matlab\subregion_centre_asia_era0.25\output_sourcerange_int12hr_sub_centre_asia-25deg/'
    main_addr = r'/home/linhaozhong/work/subregion_centre_asia_era0.25/output_sourcerange_int12hr_sub_centre_asia-25deg/'
    years = ['1979', '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
             '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995', '1996',
             '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004', '2005',
             '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014',
             '2015', '2016', '2017', '2018', '2019']

    lat = scio.loadmat(main_addr+r'lat_H.mat')['lat_H']
    lon = scio.loadmat(main_addr+r'lon_H.mat')['lon_H']

    vari_list = ['ACP_all_mon', 'ACPm_loc_mon_eul_sr', 'NCP_all_mon',
                 'NCPm_loc_mon_eul_sr', 'PW_all_mon', 'PWm_loc_mon_eul_sr',
                 'P_all_mon', 'Pm_loc_eul_mon_sr', 'rho_eul_mon_sr',
                 'rr_eul_mon_sr', 'traj_den_eul_mon_sr']

    month_names = ['01','02','03','04','05','06','07','08','09','10','11','12']

    print(years)
    lev = np.arange(1,31)

    for i, year in enumerate(years[1:]):

        traj_den_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        rr_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        rho_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        PWm_loc_mon_eul_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        ACPm_loc_mon_eul_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        NCPm_loc_mon_eul_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        Pm_loc_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')

        P_all_mon = np.zeros(12)
        NCP_all_mon = np.zeros(12)
        ACP_all_mon = np.zeros(12)
        PW_all_mon = np.zeros(12)

        times = pd.date_range('%s0101' % year, periods=12, freq='1m').strftime('%Y%m')
        print(times)
        for imonth in range(12):
            data = scio.loadmat(main_addr + 'Yearly_%s_%i' % (month_names[imonth], i + 1))
            traj_den_eul_mon_sr[imonth] = data['s_traj_den_eul_mon_sr']
            rr_eul_mon_sr[imonth] = data['s_rr_eul_mon_sr']
            rho_eul_mon_sr[imonth] = data['s_rho_eul_mon_sr']
            PWm_loc_mon_eul_sr[imonth] = data['s_PWm_loc_mon_eul_sr']
            ACPm_loc_mon_eul_sr[imonth] = data['s_ACPm_loc_mon_eul_sr']
            NCPm_loc_mon_eul_sr[imonth] = data['s_NCPm_loc_mon_eul_sr']
            Pm_loc_eul_mon_sr[imonth] = data['s_Pm_loc_eul_mon_sr']
            P_all_mon[imonth] = data['s_P_all_mon']
            NCP_all_mon[imonth] = data['s_NCP_all_mon']
            ACP_all_mon[imonth] = data['s_ACP_all_mon']
            PW_all_mon[imonth] = data['s_PW_all_mon']
        ds = xr.Dataset(
            {
                "ACPm_loc_mon_eul_sr": (["time", "y", "x", "lev"], ACPm_loc_mon_eul_sr),
                "NCPm_loc_mon_eul_sr": (["time", "y", "x", "lev"], NCPm_loc_mon_eul_sr),
                "PWm_loc_mon_eul_sr": (["time", "y", "x", "lev"], PWm_loc_mon_eul_sr),
                "Pm_loc_eul_mon_sr": (["time", "y", "x", "lev"], Pm_loc_eul_mon_sr),
                "rho_eul_mon_sr": (["time", "y", "x", "lev"], rho_eul_mon_sr),
                "rr_eul_mon_sr": (["time", "y", "x", "lev"], rr_eul_mon_sr),
                "traj_den_eul_mon_sr": (["time", "y", "x", "lev"], traj_den_eul_mon_sr),
                "ACP_all_mon": (["time"], ACP_all_mon),
                "NCP_all_mon": (["time"], NCP_all_mon),
                "PW_all_mon": (["time"], PW_all_mon),
                "P_all_mon": (["time"], P_all_mon),
            },
            coords={
                "lon": (["y", "x"], lon),
                "lat": (["y", "x"], lat),
                "lev": lev,
                "time": times
            },
        )
        ds.to_netcdf('Moist_%s.nc' % year)
        print('try')


# main_addr = r'D:\a_matlab\subregion_centre_asia_era0.25\output_sourcerange_int12hr_sub_centre_asia-25deg/'
main_addr = r'/home/linhaozhong/work/subregion_centre_asia_era0.25/'

row_data = xr.open_dataset(main_addr+'Moist_1979_2019.nc')['PW_contrib']

# data = xr.open_dataset('D:/pw.nc').isel(time=slice(0,420))

def is_amj(month, seleceted_mont):
    return (month >= 5) & (month <= 8)
for i in range(5, 9+1):
    seasonal_data = row_data.sel(time=np.in1d(row_data['time.month'], [i]))
    print(seasonal_data)
    seasonal_data.to_netcdf(main_addr+'pw_%i.nc'%i)
# a = seasonal_data.mean(dim='time')
# a.to_Data
# lon=seasonal_data.lon.values
# lat = seasonal_data.lat.values

# def plot_eof(fig, ax, data):
#     import cmaps
#     cb = ax.contourf(lon, lat, data,levels=np.arange(0,1.5,0.01), cmap = cmaps.WhiteBlue, extend='both')
#     plt.colorbar(cb)
#     # ax.ZhengZhou(np.array([76.375,77.125,77.125,76.375,76.375]),np.array([37.125,37.125,37.625,37.625,37.125]), transform=ccrs.PlateCarree(), color='red',
#     #         linewidth=1)
#     ax.scatter(76.625,37.375, s=2,transform=ccrs.PlateCarree(), color='red',
#             linewidth=0.01)
#     ax.coastlines()
#     ax.gridlines(linestyle='--', alpha=0.5)
#     # cbar.ax.xaxis.set_major_locator(MultipleLocator(ticks))
#
#     from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
#     from matplotlib import colors
#     from cartopy.feature import ShapelyFeature
#     from cartopy.io.shapereader import Reader
#     shplinewidth=0.1
#     shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries(),
#                                    ccrs.PlateCarree(),
#                                    edgecolor='k', facecolor='none', linewidths=0.55)
#     ax.add_feature(shape_feature)
#     shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\jiuduanxian\jiuduanxian.shp').geometries(),
#                                    ccrs.PlateCarree(),
#                                    edgecolor='k', facecolor='none', linewidths=shplinewidth)
#     ax.add_feature(shape_feature)
#     shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\jiuduanxian\nanhai.shp').geometries(),
#                                    ccrs.PlateCarree(),
#                                    edgecolor='k', facecolor='none', linewidths=shplinewidth)
#     ax.add_feature(shape_feature)
#
#
#
#     ax.set_extent([65, 85, 40, 30], ccrs.Geodetic())
#     ax.set_yticks(np.arange(40, 30, -5), crs=ccrs.PlateCarree())
#     ax.set_xticks(np.arange(65, 85, 5), crs=ccrs.PlateCarree())
#     ax.tick_params(axis='x', labelsize=6)
#     ax.tick_params(axis='y', labelsize=6)
#     from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
#     lon_formatter = LongitudeFormatter(zero_direction_label=True)
#     lat_formatter = LatitudeFormatter()
#     ax.xaxis.set_major_formatter(lon_formatter)
#     ax.yaxis.set_major_formatter(lat_formatter)
#
#
#     return
#
# fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs.PlateCarree()},figsize=[5.5,3])
#
# # a = scio.loadmat(r'D:/Monthly_200012res_6hr.mat')['PWm_loc_eul_mon']
# # plt.imshow(a)
# # plt.show()
# # sdata[np.where(sdata<0.01)] = np.nan
# sdata = a.PW_contrib.values
# # sdata = scio.loadmat(r'D:\Monthly_201803res_6hr.mat')['PWm_loc_eul_mon']
# plot_eof(fig, ax, sdata)
# plt.show()
# plt.savefig('fig.png', dpi=800)