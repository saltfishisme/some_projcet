import metpy.calc as mpcalc
import xarray as xr
import scipy.io as scio
import numpy as np
import pandas as pd
from metpy.units import units
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import scipy.io as scio
import cmaps
import numpy as np
from numpy import ma
from matplotlib import cbook
from matplotlib.colors import Normalize
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)

def main1_get_data():

    def read_Q(time):

        vp = scio.loadmat(addr + '/vapordata/Daily_vapor_%s' % time)['QPcol']

        # vertically integrated moisture flux :kg m**-1 s**-1
        u = vp['UWVcol'][0][0]
        v = vp['VWVcol'][0][0]

        return [u, v]

    addr = '/work/Data/MoistureTrackData/ERA_interim'
    date_time = pd.date_range('1979-01-01', '2015-12-31', freq='1D')
    SEASONS = [
        [12, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [9, 10, 11]
    ]

    for seai, season in enumerate(SEASONS):
        sdate_time = date_time[date_time.month.isin(season)].strftime('%Y%m%d')
        lentime = len(sdate_time)
        sE = np.zeros([91, 360])
        sP = np.zeros([91, 360])

        for i, si in enumerate(sdate_time):
            E, P = read_Q(si)
            sE = sE+E; sP = sP+P
        scio.savemat('/work/Qclimately_%i.mat'%seai, {'u':sE/lentime, 'v':sP/lentime})

def main2():
    # 将四个季节的数据合并到一个mat，以季节命名
    datas = {}
    seasons = [2,3,0,1]
    u = np.zeros([4,181,360])
    v = np.zeros([4,181,360])

    for i in range(4):
        print(seasons[i])
        data = scio.loadmat('Qclimately_%i.mat' % seasons[i])
        u[i,:,0:180] = data['u'][:,180:]
        u[i, :, 180:] = data['u'][:, :180]
        v[i, :, 0:180] = data['v'][:, 180:]
        v[i, :, 180:] = data['v'][:, :180]

    # mask a white space
    lon_q, lat_q = np.meshgrid(np.arange(-180, 180, 1), np.arange(90, -91, -1))

    mask_lon = np.where(lon_q>100)
    mask_lat = np.where(lat_q<-30)

    lat = 112
    lon = 100
    u[:, lat:, :lon] = np.nan
    v[:, lat:, :lon] = np.nan


    datas['u'] = u
    datas['v'] = v


    datas['lon_q'] = np.array(lon_q, dtype='double')
    datas['lat_q'] = np.array(lat_q, dtype='double')
    scio.savemat(r'D:\a_matlab\paper_zhong/Qclimately_fig4.mat', datas)


def main2_unuse():
    # 弃之不用，转为画到老师的图上去。
    def plot_eof(fig, ax, u, v, title):
        # ax.contour(lon, lat, data, levels[::4], colors='k')
        Q = ax.quiver(np.arange(0, 360, 1)[::space_lon], np.arange(90, -91, -1)[::space_lat],
                      u[::space_lat, ::space_lon], v[::space_lat, ::space_lon],
                      transform=ccrs.PlateCarree())

        qk = ax.quiverkey(Q, 0.9, 0.97, 200, r'$200 \frac{kg}{m*s}$', labelpos='E',
                          coordinates='figure', fontproperties={'size': 6})
        ax.coastlines(alpha=0.5)
        ax.gridlines(linestyle='--', alpha=0.5)
        # cbar.ax.xaxis.set_major_locator(MultipleLocator(ticks))

        ax.set_yticks(np.arange(80, -80, -40), crs=ccrs.PlateCarree())
        ax.set_xticks(np.arange(-170, 170, 60), crs=ccrs.PlateCarree())
        ax.tick_params(axis='x', labelsize=6)
        ax.tick_params(axis='y', labelsize=6)
        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)

        plt.text(0.01, 0.95, title,
                 horizontalalignment='left',
                 verticalalignment='center',
                 transform=ax.transAxes, fontdict={'fontsize': 6},
                 bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', pad=0.00001))

        return
    fig, ax = plt.subplots(2, 2, subplot_kw={'projection': ccrs.PlateCarree()},sharex='col',sharey='row',
                           figsize=[5.5,3])

    plt.subplots_adjust(top=0.94,
bottom=0.045,
left=0.06,
right=0.99,
hspace=0.055,
wspace=0.095)
    figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)',
                   '(o)',
                   '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)'];

    seasons = ['Winter', 'Spring', 'Summer', 'Fall']
    for i in range(2):
        for j in range(2):
            ci = i*2+j
            data = scio.loadmat('Qclimately_%i.mat'%ci)
            plot_eof(fig, ax[i][j], data['u'], data['v'], figlabelstr[ci]+' '+seasons[ci])



    if show_mode == 'show':
        plt.show()
    elif show_mode == 'save':
        plt.savefig('quqv_climately.png', dpi=800)
        plt.close()

# main1_get_data()
show_mode  = 'show'
space_lon = 10
space_lat =  5
# main2_unuse()
main2()