import metpy.calc as mpcalc
import xarray as xr
import scipy.io as scio
import numpy as np
import pandas as pd
from metpy.units import units


def gradient(data):
    import metpy.calc as mpcalc
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd
    lat = np.arange(56, 34.25, -1)
    lon = np.arange(45, 100.25, 1)
    dx, dy = mpcalc.lat_lon_grid_deltas(lon, lat)
    dzdy, dzdx = mpcalc.gradient(data, deltas=(dy, dx))
    return dzdx + dzdy

u = scio.loadmat('Q_0.mat')['u']
v = scio.loadmat('Q_0.mat')['v']
b = u*v
c1 = gradient(b[0]* units('kg/(m*s)'))
print(c1.shape)


def gradient(u,v):
    import metpy.calc as mpcalc
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd
    lat = np.arange(56, 34.25, -1)
    lon = np.arange(45, 100.25, 1)
    lons, lats = np.meshgrid(lon, lat)  #
    dx, dy = mpcalc.lat_lon_grid_deltas(lons, lats)
    dzdy= mpcalc.divergence(u,v, dx=dx, dy=dy)
    return dzdy

u = scio.loadmat('Q_0.mat')['u']
v = scio.loadmat('Q_0.mat')['v']
print(u)
c2 = gradient(u[0], v[0])
print(c2)

pe = scio.loadmat('0.mat')['E']
print(pe)

pe = scio.loadmat('Daily_pe_19790101.mat')['PE']
print(pe['EV'][0][0])