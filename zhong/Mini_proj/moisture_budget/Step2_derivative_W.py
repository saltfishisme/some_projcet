import metpy.calc as mpcalc
import xarray as xr
import scipy.io as scio
import numpy as np
import pandas as pd
from metpy.units import units
import matplotlib.pyplot as plt
def creat_word_table(document, title, df, index=False, col=False):
    from docx import Document
    from docx.enum.table import WD_TABLE_ALIGNMENT
    from docx.shared import Cm
    from docx.oxml.ns import qn
    '''
    document = Document()
document.styles['Normal'].font.name = '宋体'
document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')'''
    from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
    p = document.add_paragraph('')
    p.add_run(title).bold = True
    p.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    if (index is False) & (col is False):
        tb = document.add_table(rows=len(df.index), cols=len(df.columns))
        # tb.add_row()
        for row in range(0,len(df.index)):
            for col in range(0,len(df.columns)):
                tb.cell(row, col).text = str(df.iloc[row, col])
    elif (index is True) & (col is True):
        tb = document.add_table(rows=len(df.index)+1, cols=len(df.columns)+1)
        for j in range(df.shape[-1]):
            tb.cell(0, j+1).text = df.columns[j]
        for i in range(df.shape[0]):
            tb.cell(i+1, 0).text = df.index[i]
        for row in range(0,len(df.index)):
            for col in range(0,len(df.columns)):
                tb.cell(row+1, col+1).text = str(df.iloc[row, col])
    elif (index is True) & (col is False):
        tb = document.add_table(rows=len(df.index), cols=len(df.columns)+1)
        for i in range(df.shape[0]):
            tb.cell(i, 0).text = df.index[i]
        for row in range(0,len(df.index)):
            for col in range(0,len(df.columns)):
                tb.cell(row, col+1).text = str(df.iloc[row, col])
    elif (index is False) & (col is True):
        tb = document.add_table(rows=len(df.index)+1, cols=len(df.columns))
        for j in range(df.shape[-1]):
            tb.cell(0, j).text = df.columns[j]

        for row in range(0,len(df.index)):
            for col in range(0,len(df.columns)):
                tb.cell(row+1, col).text = str(df.iloc[row, col])

    tb.style = 'Table Grid'
    tb.autofit = True

def main():
    for i in range(4):
        data = scio.loadmat('%i'%i)
        w_d = mpcalc.first_derivative(data['w'], delta=1)

        data['w'] = np.array(w_d)
        scio.savemat('AMB%i.mat'%i, data)

def plot_box_analysis(data):
    return [np.nanmax(data), np.quantile(data, 0.75), np.nanmean(data),
            np.quantile(data, 0.25), np.nanmin(data)]
# main()
def second_main(i):

    data = scio.loadmat('AMB%i'%i)
    w = np.nanmean(data['w'], axis=(1,2))
    e = np.nanmean(-1*data['E'], axis=(1,2))
    p = np.nanmean(data['P'], axis=(1,2))

    def gradient(u, v):
        import metpy.calc as mpcalc
        import xarray as xr
        import scipy.io as scio
        import numpy as np
        import pandas as pd
        lat = np.arange(56, 34.25, -1)
        lon = np.arange(45, 100.25, 1)
        lons, lats = np.meshgrid(lon, lat)  #
        dx, dy = mpcalc.lat_lon_grid_deltas(lons, lats)
        dzdy = mpcalc.divergence(u, v, dx=dx, dy=dy)

        return dzdy*24*3600

    q_data = scio.loadmat('Q_%i'%i)
    u = q_data['u'] # left, right
    v = q_data['v'] # upper, low

    q = np.zeros(u.shape)

    for iss in range(u.shape[0]):
        q[iss] = gradient(u[iss]* units('kg/(m*s)'), v[iss]* units('kg/(m*s)'))

    a = np.nanmean(data['w'] + q + data['P'] + data['E'], axis=(1, 2))
    q = np.nanmean(q, axis=(1,2))

    q_data = scio.loadmat('Q_%i' % i)
    u = q_data['u']  # left, right
    v = q_data['v']  # upper, low

    q_w = np.nansum(u[:, :, 0], axis=1)
    q_e = np.nansum(u[:, :, -1], axis=1)

    q_n = np.nansum(v[:, 0, :], axis=1)
    q_s = np.nansum(v[:, -1, :], axis=1)

    q_t = q_w - q_e + q_s - q_n

    df1 = pd.DataFrame()
    df1['Q_West'] = q_w
    df1['Q_East'] = q_e
    df1['Q_North'] = q_n
    df1['Q_South'] = q_s
    df1['Q_Total'] = q_t
    all_mean1 = pd.DataFrame(df1.mean()).T


    df = pd.DataFrame()
    df['P'] = p
    df['E'] = e
    df['Q'] = q
    df['w'] = w
    df['a'] = a
    all_mean = pd.DataFrame(df.mean()).T

    return all_mean, all_mean1

from docx import Document
from docx.oxml.ns import qn

document = Document()
document.styles['Normal'].font.name = '宋体'
document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

seasons = ['Winter', 'Spring', 'Summer', 'Fall']
all_df = pd.DataFrame()
all_df1 = pd.DataFrame()
for i in range(4):
    df, df1 = second_main(i)
    all_df[seasons[i]] = df.loc[0]
    all_df1[seasons[i]] = df1.loc[0]

#
creat_word_table(document, 'Moisture Budget(unit: mm/day)', all_df.T.round(3), col=True, index=True)
creat_word_table(document, 'Moisture transport(unit: kg/(m*s))', all_df1.T.round(1), col=True, index=True)
document.save('Moisture Budget.docx' )
#




