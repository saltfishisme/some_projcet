# ### Exectutalbe gernralised budget script
#
#
import numpy as np
import pickle
import datetime as dt
#
from dateutil.relativedelta import relativedelta

import copy
from mpl_toolkits.basemap import Basemap
import grid_set as gs
import budget_inputs as bi
import budget as b
#
#
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present


def budget_forAR(spath, ts, te):
    save_dir = os.path.dirname(spath)
    # check it exists
    if not os.path.exists(save_dir):
        # make if it doesn't
        os.makedirs(save_dir)
        print('Creating directory: ', save_dir)
    else:
        print('Existing directory: ', save_dir)

    ### copy the option from this file to spath
    write_l = False
    with open("./budget_process.py") as f:
        with open(spath + "config.txt", 'w+') as f1:
            for l in f:
                if len(l) > 10:
                    if l.split()[0] == "#####":
                        write_l = True
                if write_l and l[0:2] != "# ":
                    f1.write(l)
                if "END USER" in l and write_l:
                    break

    ##### USER OPTIONS #####
    ## dates

    tw = relativedelta(days=1)

    ## message option
    load_verbos = False
    hole_verbos = False

    ## diagnostic plots - for the first time point
    print_inputs = False
    print_budget = False
    print_budget_square = False
    print_dstats = False
    print_history = False
    print_history_square = False

    ## deformation options
    calc_dstats = False

    ## masking options
    mask_poleV = False
    mask_poleT = False
    pole_hole_lat = 88.0

    ## Thickness/Con limit masking
    Tlim = 0.2  ### meters below which we mask
    Clim = 0.15  ### conc.  below which we mask

    ### Time smoothing velocity option
    V_time_smooth = False

    ### First we need grids for the data we're using
    ## the grids sit with a projection North pole in this case

    m = Basemap(projection='stere',
                lon_0=-45.0, lat_0=83, lat_ts=0,
                height=3335000 * 1.8, width=3335000 * 1.8)

    ### VELOCITY
    GV = gs.grid_set(m)
    # GV.load_grid('grids/PIOMAS_gs.npz')
    # GV.load_mask('grids/PIOMAS_gs_mask.npz')
    GV.load_grid('grids/Pathfinder_gs.npz')
    GV.load_mask('grids/Pathfinder_gs_mask.npz')
    # GV.load_grid('grids/Kimura_gs.npz')
    # GV.load_mask('grids/Kimura_gs_mask.npz')
    # GV.load_grid('grids/osisaf_gs.npz')
    # GV.load_mask('grids/osisaf_gs_mask2.npz')
    GV.reproject(m)
    if mask_poleV:
        GV.mask[GV.lats > pole_hole_lat] = np.nan

    ### THICKNESSS
    GT = gs.grid_set(m)
    GT.load_grid('grids/PIOMAS_gs.npz')
    GT.load_mask('grids/PIOMAS_gs_mask.npz')
    GT.reproject(m)
    # # if mask_poleT:
    #     GT.mask[GT.lats>pole_hole_lat] = np.nan

    #### CONCENTRATION
    GC = gs.grid_set(m)
    # GC.load_grid('grids/AWI_gs.npz')
    # GC.load_mask('grids/AWI_gs_mask.npz')
    GC.load_grid('grids/PIOMAS_gs.npz')
    GC.load_mask('grids/PIOMAS_gs_mask.npz')
    GC.reproject(m)

    ##### Set up budget input objects
    ### all the code is in the budget_inputs.py
    ### first we query the dates of interest
    # path = '/Volumes/BU_extra/BUDGET/PIOMAS_data/'
    path = '/home/linhaozhong/work/Data/Pathfinderv4/'
    # path = '/Volumes/BU_extra/BUDGET/Data/Drift/Kimura/All_binary_data/'
    # path = '/Volumes/BU_extra/BUDGET/Data/Drift/OSISAF/osisaf.met.no/archive/ice/drift_lr/merged/'
    # P = bi.PIOMAS(path)
    P = bi.Pathfinder(path)
    # P = bi.Kimura(path)
    # P = bi.OSISAF(path)

    P.get_dates(ts, te)

    ### THICKNESS
    path = '/home/linhaozhong/work/Data/PIOMAS_TXT/'
    # path = '/Volumes/BU_extra/BUDGET/Data/Thickness/CPOM/OfficialGridded/'
    A = bi.PIOMAS(path)
    # A = bi.AWI_SMOS_daily(path)
    # A = bi.CPOM_hi(path,GV)

    A.get_dates(ts, te)

    ### CONCENTRATION
    nt_path = '/home/linhaozhong/work/Data/PIOMAS_TXT/'
    C = bi.PIOMAS(nt_path)

    C.get_dates(ts, te)

    ### use data to set the dates to process
    dlist = b.get_loop_list(A.dates, tw)
    ts = dlist[0]
    ndays = np.shape(dlist)[0]

    ### compare to additional data
    # dlist = b.comp_dates(dlist,P.dates)

    # input select
    InputV = P  # velocity forcing
    InputT = A  # thickness
    InputC = C  # concentration
    # option for whether we're using effective thickness or not
    make_eff_thk = False

    # grid select
    Gvel = GV
    Gthk = GT
    Gcon = GC

    rgd_thk = True
    rgd_con = True

    # smoothing options
    smthvel = False
    smththk = False
    smthcon = False

    #### old uniform
    velsmrad = 6
    Vsmth = lambda x: b.smooth2a(x, velsmrad)
    thksmrad = 4
    Tsmth = lambda x: b.smooth2a(x, thksmrad)
    consmrad = 1
    Csmth = lambda x: b.smooth2a(x, consmrad)

    #### new vary smoothing
    # velsmthdist = 200e3
    # thksmthdist = 100e3
    # consmthdist = 60e3
    #
    # VSmthObj = gs.geo_vary_smooth(Gvel,velsmthdist,verbos=True)
    # TSmthObj = gs.geo_vary_smooth(Gvel,thksmthdist,verbos=True)
    # CSmthObj = gs.geo_vary_smooth(Gvel,consmthdist,verbos=True)
    #
    # Vsmth = VSmthObj.smooth
    # Tsmth = TSmthObj.smooth
    # Csmth = CSmthObj.smooth

    #### history fields
    budhist = b.budhist(Gvel, dlist, tw, drift_stats=calc_dstats,
                        )
    #                     dumptw = relativedelta(days=20),budday0=ts)

    ### optional history regrid
    histrgd = gs.Gs2Gs(GV, GC, vectors=True)

    #### END USER OPTIONS

    ##### below here should be left alone (should)

    # regrid objects
    if rgd_thk:
        print("Building thickness regridder")
        print(InputT.name + " to " + InputV.name)
        RGDthk = gs.Gs2Gs(Gthk, Gvel)
    if rgd_con:
        print("Building concentration regridder")
        print(InputC.name + " to " + InputV.name)
        RGDcon = gs.Gs2Gs(Gcon, Gvel)

    Uslices = np.empty([3, Gvel.m, Gvel.n])
    Vslices = np.empty([3, Gvel.m, Gvel.n])
    Tslices = np.empty([3, Gvel.m, Gvel.n])
    Cslices = np.empty([3, Gvel.m, Gvel.n])
    Vol_slices = np.empty([3, Gvel.m, Gvel.n])

    # divergence advection are runnign means so make array
    budadv = np.empty([3, Gvel.m, Gvel.n])
    buddiv = np.empty([3, Gvel.m, Gvel.n])

    ### initalising the slices

    Utemp, Vtemp = b.get_vels_array(InputV, ts, tw, verbos=load_verbos)
    Utemp = Utemp * Gvel.mask
    Vtemp = Vtemp * Gvel.mask
    if smthvel:
        Uslices[0] = Vsmth(Utemp)
        Vslices[0] = Vsmth(Vtemp)
    else:
        Uslices[0] = Utemp
        Vslices[0] = Vtemp
    Ttemp = b.get_hi_array(InputT, ts, tw, verbos=load_verbos)
    if print_inputs: Tplot = copy.copy(Ttemp)
    Ttemp[Ttemp < Tlim] = np.nan
    Ttemp = Ttemp * Gthk.mask
    if rgd_thk:
        # call the regrid object
        Ttemp = RGDthk.rg_array(Ttemp)
    if smththk:
        Tslices[0] = Tsmth(Ttemp)
    else:
        Tslices[0] = Ttemp
    Ctemp = b.get_aice_array(InputC, ts, tw, verbos=load_verbos)
    if print_inputs: Cplot = copy.copy(Ctemp)
    Ctemp[Ctemp < Clim] = np.nan
    Ctemp = Ctemp * Gcon.mask
    if rgd_con:
        # call the regrid object
        Ctemp = RGDcon.rg_array(Ctemp)
    if smthcon:
        Cslices[0] = Csmth(Ctemp)
    else:
        Cslices[0] = Ctemp

    if print_inputs: b.plot_inputs(Gvel, Gthk, Gcon, InputV, InputT, InputC,
                                   Utemp, Vtemp, Tplot, Cplot, spath, ts)

    if make_eff_thk:
        Vol_slices[0] = Tslices[0] * Cslices[0]
    else:
        Vol_slices[0] = Tslices[0]

    Uslices[1], Vslices[1] = Uslices[0], Vslices[0]
    Tslices[1] = Tslices[0]
    Cslices[1] = Cslices[0]
    Vol_slices[1] = Vol_slices[0]

    #### adv & div at single points
    # advection and divergence are calculated slice by slice
    # so do it as we load
    # ADVECTION
    advtemp = b.advection(Uslices[0], Vslices[0], Vol_slices[0], Gvel)
    budadv[0] = advtemp

    # DIVERGENCE
    divtemp = b.divergence(Uslices[0], Vslices[0], Vol_slices[0], Gvel)
    buddiv[0] = divtemp

    budadv[1] = budadv[0]
    buddiv[1] = buddiv[0]

    ### Now loop along the list of dates
    for n, d in enumerate(dlist[1:]):
        ### check list for
        budhist.check_dump(n, d, hole_verbos)

        print("new slice from " + d.strftime('%Y%m%d'))
        # update final slice

        Utemp, Vtemp = b.get_vels_array(InputV, d, tw, verbos=load_verbos)
        Utemp = Utemp * Gvel.mask
        Vtemp = Vtemp * Gvel.mask
        if smthvel:
            Uslices[2] = Vsmth(Utemp)
            Vslices[2] = Vsmth(Vtemp)
        else:
            Uslices[2] = Utemp
            Vslices[2] = Vtemp
        Ttemp = b.get_hi_array(InputT, d, tw, verbos=load_verbos)
        Ttemp[Ttemp < Tlim] = np.nan
        Ttemp = Ttemp * Gthk.mask
        if rgd_thk:
            # call the regrid object
            Ttemp = RGDthk.rg_array(Ttemp)
        if smththk:
            Tslices[2] = Tsmth(Ttemp)
        else:
            Tslices[2] = Ttemp
        Ctemp = b.get_aice_array(InputC, d, tw, verbos=load_verbos)
        Ctemp[Ctemp < Clim] = np.nan
        Ctemp = Ctemp * Gcon.mask
        if rgd_con:
            # call the regrid object
            Ctemp = RGDcon.rg_array(Ctemp)
        if smthcon:
            Cslices[2] = Csmth(Ctemp)
        else:
            Cslices[2] = Ctemp

        #### actual budget
        if make_eff_thk:
            Vol_slices[2] = Tslices[2] * Cslices[2]
        else:
            Vol_slices[2] = Tslices[2]

        ### time smooth the velocity before doing the budget
        if V_time_smooth:
            UTsmth = np.nanmean(Uslices, axis=0)
            VTsmth = np.nanmean(Vslices, axis=0)
            VolTsmth = np.nanmean(Vol_slices, axis=0)

            budadv3 = b.advection(UTsmth, VTsmth, VolTsmth, Gvel)
            buddiv3 = b.divergence(UTsmth, VTsmth, VolTsmth, Gvel)

        else:
            # advection and divergence are calculated slice by slice
            # so do it as we load
            # ADVECTION
            advtemp = b.advection(Uslices[2], Vslices[2], Vol_slices[2], Gvel)
            budadv[2] = advtemp

            # DIVERGENCE
            divtemp = b.divergence(Uslices[2], Vslices[2], Vol_slices[2], Gvel)
            buddiv[2] = divtemp

            #     print(np.nanmean(buddif))
            # we also need to smooth the div and adv over 3 time steps
            buddiv3 = np.nanmean(buddiv, axis=0)
            budadv3 = np.nanmean(budadv, axis=0)

        # CHANGE IN VOL USES ALL SLICES
        nsecs = b.get_step_size(d, tw)
        buddif = b.intensification(Vol_slices, nsecs)

        if n == 0 and print_budget: b.plot_budget(Gvel, buddif, buddiv3,
                                                  budadv3, Utemp, Vtemp, spath, ts)
        if n == 0 and print_budget_square: b.plot_budget_square(Gvel, buddif, buddiv3,
                                                                budadv3, Utemp, Vtemp, spath, ts)

        budhist.accumulate_budget(d, buddif, budadv3, buddiv3,
                                  Tslices[1], Cslices[1], Uslices[1], Vslices[1],
                                  hole_verbos)

        if calc_dstats:
            ### get components
            Dudx, Dudy = gs.geo_gradient(Uslices[1] * Gvel.mask, Gvel)
            Dvdx, Dvdy = gs.geo_gradient(Vslices[1] * Gvel.mask, Gvel)
            ddiv = Dudx + Dvdy
            dcrl = Dvdx - Dudy
            dshr = Dvdx + Dudy
            dshr = np.sqrt((Dudx + Dvdy) ** 2 + dshr ** 2)
            budhist.accumulate_dstats(ddiv, dcrl, dshr)
            if n == 0 and print_dstats: b.plot_dstats(Gvel, Utemp, Vtemp,
                                                      InputV, ddiv, dcrl, dshr, spath, ts)

        # update slices
        Uslices[0] = Uslices[1]
        Uslices[1] = Uslices[2]
        Vslices[0] = Vslices[1]
        Vslices[1] = Vslices[2]

        Tslices[0] = Tslices[1]
        Tslices[1] = Tslices[2]
        Cslices[0] = Cslices[1]
        Cslices[1] = Cslices[2]

        Vol_slices[0] = Vol_slices[1]
        Vol_slices[1] = Vol_slices[2]

        budadv[0] = budadv[1]
        budadv[1] = budadv[2]

        buddiv[0] = buddiv[1]
        buddiv[1] = buddiv[2]

        # also need to dump on end of list
        # also need to dump when we reach a hole
        if budhist.hist_dump_now:
            if print_history:
                b.plot_budget(Gvel, budhist.buddifhist,
                              budhist.buddivhist, budhist.buddivhist,
                              budhist.budxvelhist / budhist.hist_count,
                              budhist.budyvelhist / budhist.hist_count,
                              spath, d)
            if print_history_square:
                b.plot_budget_square(Gvel, budhist.buddifhist,
                                     budhist.buddivhist, budhist.buddivhist,
                                     budhist.budxvelhist / budhist.hist_count,
                                     budhist.budyvelhist / budhist.hist_count,
                                     spath, d)
            ### optional second dump
            budhist.dump_nc(spath, d, Gvel, InputV, InputT, InputC,
                            outrgd=[histrgd, '_EASE', GC], accu_reset=False)

            budhist.dump_nc(spath, d, Gvel, InputV, InputT, InputC)
        #         budhist.dump_pkl(spath,d,Gvel)

        if budhist.hist_dump_now and calc_dstats:
            budhist.dump_dstats_nc(spath, d, Gvel, InputV)
    #         budhist.dump_pkl(spath,d,dstats = calc_dstats)


# budget_forAR(path_SaveAR, i_ts_te-timedelta(days=1), i_ts_te+timedelta(days=1))
# outputs

import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4
import scipy.io as scio
from datetime import timedelta

main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
path_main_Save = r'/home/linhaozhong/work/AR_NP85/budget_daily/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']


bu_CALCULATE_budget = True
for iI, i in enumerate(np.unique(save_labels_bmus)):
    if i == 0:
        continue
    file = file_all[save_labels_bmus==i]

    for i_file in file:

        info_AR_row = xr.open_dataset(main_path+i_file[:-1])
        time_all = info_AR_row.time_all.values

        time_here = []
        time_here_loc = []
        for iI_timeall, i_timeall in enumerate(time_all):
            i_contour = info_AR_row['contour_all'].sel(time_all = i_timeall).values
            from function_shared_Main import cal_area
            weighted = cal_area(info_AR_row.lon.values, info_AR_row.lat.values)
            sic = get_circulation_data_based_on_date(pd.to_datetime(i_timeall),'Sea_Ice_Cover', 'siconc')
            i_contour[sic<0.01] = 0
            i_contour[np.isnan(sic)] = 0
            area_here = np.nansum(weighted[i_contour>0.01])
            if area_here > 50*(10**4):
                time_here.append(str(i_timeall))
                time_here_loc.append(iI_timeall)

        # skip if two consecutive time points faill to meet the criteria.
        def consecutive(data):
            max_segment = []
            max_indices = []
            current_segment = []
            current_indices = []

            for index, num in enumerate(data):
                if num == 1:
                    current_segment.append(num)
                    current_indices.append(index)
                else:
                    if len(current_segment) > len(max_segment):
                        max_segment = current_segment
                        max_indices = current_indices
                    current_segment = []
                    current_indices = []

            if len(current_segment) > len(max_segment):
                max_segment = current_segment
                max_indices = current_indices

            return max_indices


        time_here_loc_div = np.diff(time_here_loc)
        time_here_loc_div[time_here_loc_div <= 2] = 1
        time_here_loc_div[time_here_loc_div>2] = 2

        time_here_loc_div = consecutive(time_here_loc_div)
        if len(time_here_loc_div) <= 1:
            continue
        time_here = np.array(time_here)[time_here_loc_div + [time_here_loc_div[-1]+1]]

        "==================================="
        "==================================="
        # ts = dt.datetime(int(time_here[0][:4]), int(time_here[0][5:7]), int(time_here[0][8:10]))
        # te = dt.datetime(int(time_here[-1][:4]), int(time_here[-1][5:7]), int(time_here[-1][8:10]))
        # delta = te - ts
        #
        # path_SaveAR = path_main_Save+'AR/'+i_file+'/'
        # os.makedirs(path_SaveAR, exist_ok=True)
        #
        # if bu_CALCULATE_budget:
        #     for i_ts_te in pd.date_range(ts, te):
        #         budget_forAR(path_SaveAR, i_ts_te-timedelta(days=1), i_ts_te+timedelta(days=1))
        #     # budget_forAR(path_SaveAR, ts-timedelta(days=1), te+timedelta(days=1))
        #
        # "==================================="
        # path_SaveAR = path_SaveAR + 'anomaly/'
        # os.makedirs(path_SaveAR, exist_ok=True)
        # for i_year in range(2000,2020+1):
        #     ts = dt.datetime(i_year, int(time_here[0][5:7]), int(time_here[0][8:10]))
        #     te = ts + delta
        #
        #     if bu_CALCULATE_budget:
        #         for i_ts_te in pd.date_range(ts, te):
        #             budget_forAR(path_SaveAR, i_ts_te - timedelta(days=1), i_ts_te + timedelta(days=1))
        #
        #             # if os.path.exists(path_SaveAR+(ts - timedelta(days=1)).strftime('%Y%m%d.nc')):
        #             #     continue
        #             # budget_forAR(path_SaveAR, ts - timedelta(days=1), te + timedelta(days=1))


        "==================================="
        "==================================="
        # ts = dt.datetime(int(time_here[0][:4]), int(time_here[0][5:7]), int(time_here[0][8:10]))-timedelta(days=6)
        # te = dt.datetime(int(time_here[0][:4]), int(time_here[0][5:7]), int(time_here[0][8:10]))-timedelta(days=1)
        # delta = te - ts
        # path_SaveAR = path_main_Save + 'before/' + i_file + '/'
        # os.makedirs(path_SaveAR, exist_ok=True)
        # if bu_CALCULATE_budget:
        #     if os.path.exists(path_SaveAR + (ts - timedelta(days=1)).strftime('budfields_%Y%m%d.nc')):
        #         print('i am ok')
        #         continue
        #     for i_ts_te in pd.date_range(ts, te):
        #         budget_forAR(path_SaveAR, i_ts_te-timedelta(days=1), i_ts_te+timedelta(days=1))
        #     # budget_forAR(path_SaveAR, ts-timedelta(days=1), te+timedelta(days=1))
        # "==================================="
        # path_SaveAR = path_SaveAR + 'anomaly/'
        # os.makedirs(path_SaveAR, exist_ok=True)
        # for i_year in range(2000,2020+1):
        #     try:
        #         ts = dt.datetime(i_year, int(time_here[0][5:7]), int(time_here[0][8:10]))-timedelta(days=6)
        #     except:
        #         # if date is not leap year and 2/29
        #         ts = dt.datetime(i_year, int(time_here[0][5:7]), int(time_here[0][8:10])-1) - timedelta(days=6)
        #     te = ts + delta
        #
        #     if bu_CALCULATE_budget:
        #         if os.path.exists(path_SaveAR + (ts - timedelta(days=1)).strftime('budfields_%Y%m%d.nc')):
        #             print('i am ok')
        #             continue
        #         for i_ts_te in pd.date_range(ts, te):
        #             budget_forAR(path_SaveAR, i_ts_te - timedelta(days=1), i_ts_te + timedelta(days=1))
        #         # budget_forAR(path_SaveAR, ts - timedelta(days=1), te + timedelta(days=1))

        "==================================="
        "==================================="
        ts = dt.datetime(int(time_here[-1][:4]), int(time_here[-1][5:7]), int(time_here[-1][8:10]))+timedelta(days=7)
        te = dt.datetime(int(time_here[-1][:4]), int(time_here[-1][5:7]), int(time_here[-1][8:10]))+timedelta(days=15)
        delta = te - ts
        path_SaveAR = path_main_Save + 'after/' + i_file + '/'
        os.makedirs(path_SaveAR, exist_ok=True)

        if bu_CALCULATE_budget:
            # if os.path.exists(path_SaveAR + (ts - timedelta(days=1)).strftime('budfields_%Y%m%d.nc')):
            #     print('i am ok')
            #     continue
            for i_ts_te in pd.date_range(ts, te):
                budget_forAR(path_SaveAR, i_ts_te-timedelta(days=1), i_ts_te+timedelta(days=1))
            # budget_forAR(path_SaveAR, ts-timedelta(days=1), te+timedelta(days=1))

        "==================================="
        path_SaveAR = path_SaveAR + 'anomaly/'
        os.makedirs(path_SaveAR, exist_ok=True)
        for i_year in range(2000,2020+1):
            try:
                ts = dt.datetime(i_year, int(time_here[-1][5:7]), int(time_here[-1][8:10]))+timedelta(days=1)
            except:
                # if date is not leap year and 2/29
                ts = dt.datetime(i_year, int(time_here[-1][5:7]), int(time_here[-1][8:10])-1)+timedelta(days=1)
            te = ts + delta

            if bu_CALCULATE_budget:
                # if os.path.exists(path_SaveAR+(ts - timedelta(days=1)).strftime('budfields_%Y%m%d.nc')):
                #     print('i am ok')
                #     continue
                for i_ts_te in pd.date_range(ts, te):
                    budget_forAR(path_SaveAR, i_ts_te - timedelta(days=1), i_ts_te + timedelta(days=1))
                # budget_forAR(path_SaveAR, ts - timedelta(days=1), te + timedelta(days=1))
exit()

import numpy as np
import xarray as xr
import pandas as pd
import warnings


warnings.filterwarnings("ignore", category=RuntimeWarning)
import nc4
budget_load = ['residual', 'dynamics'
               ]

import os

import nc4
import scipy.io as scio
from datetime import timedelta

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
path_main_Save = r'/home/linhaozhong/work/AR_NP85/budget_daily/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']

path_Save = r'/home/linhaozhong/work/AR_NP85/PDF_line_new_Clustering/'
bu_CALCULATE_budget = False
for iI, i in enumerate(np.unique(save_labels_bmus)):
    if (i != 1) & (i!=6):
        continue
    file = file_all[save_labels_bmus == i]

    data_collection_All = []
    AR_delta_All = []
    for i_file in file:

        info_AR_row = xr.open_dataset(main_path + i_file[:-1])
        time_all = info_AR_row.time_all.values
        time_AR = info_AR_row.time_AR.values
        time_here = []
        time_here_loc = []
        for iI_timeall, i_timeall in enumerate(time_all):
            i_contour = info_AR_row['contour_all'].sel(time_all=i_timeall).values
            from function_shared_Main import cal_area

            weighted = cal_area(info_AR_row.lon.values, info_AR_row.lat.values)
            sic = get_circulation_data_based_on_date(pd.to_datetime(i_timeall), 'Sea_Ice_Cover', 'siconc')
            i_contour[sic < 0.01] = 0
            i_contour[np.isnan(sic)] = 0
            area_here = np.nansum(weighted[i_contour > 0.01])
            if area_here > 50 * (10 ** 4):
                time_here.append(str(i_timeall))
                time_here_loc.append(iI_timeall)


        # skip if two consecutive time points faill to meet the criteria.
        def consecutive(data):
            max_segment = []
            max_indices = []
            current_segment = []
            current_indices = []

            for index, num in enumerate(data):
                if num == 1:
                    current_segment.append(num)
                    current_indices.append(index)
                else:
                    if len(current_segment) > len(max_segment):
                        max_segment = current_segment
                        max_indices = current_indices
                    current_segment = []
                    current_indices = []

            if len(current_segment) > len(max_segment):
                max_segment = current_segment
                max_indices = current_indices

            return max_indices


        time_here_loc_div = np.diff(time_here_loc)
        time_here_loc_div[time_here_loc_div <= 2] = 1
        time_here_loc_div[time_here_loc_div > 2] = 2

        time_here_loc_div = consecutive(time_here_loc_div)
        if len(time_here_loc_div) <= 1:
            continue
        time_here = np.array(time_here)[time_here_loc_div + [time_here_loc_div[-1] + 1]]
        print(time_here)
        print(time_AR)
        AR_delta_All.append([(pd.to_datetime(time_here[0])-pd.to_datetime(time_AR[0])).total_seconds()/3600,
                             (pd.to_datetime(time_here[0])-pd.to_datetime(time_AR[-1])).total_seconds()/3600
                             ])
        print(time_here[0], time_AR[0])
        print(AR_delta_All)
        print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
        "==================================="

        def load_budget_inGivenFile(path, i_time):
            def load_by_path(path_now):
                var_pattern = []
                file_here = os.listdir(path_now)

                for i_file_here in file_here:

                    if not len(i_file_here) == 21:
                        continue
                    if i_time not in i_file_here:
                        continue

                    budget = xr.open_dataset(path_now + i_file_here)
                    var_in_single_file = []
                    for var in budget_load:
                        aaa = budget[var].values[0]*24*60*60
                        var_in_single_file.append(aaa)
                    var_pattern.append(var_in_single_file)
                return np.nanmean(np.array(var_pattern), axis=0), budget

            data_now,budget =  load_by_path(path)
            data_cli, budget = load_by_path(path+'anomaly/')
            data = data_now-data_cli

            budget_lon = budget.lon.values[0]
            from function_shared_Main import roll_longitude_from_359to_negative_180, cal_area_differentLL
            budget_lon = roll_longitude_from_359to_negative_180(budget_lon, True)
            budget_lat = budget.lat.values[0]

            weighted_PIOMS = cal_area_differentLL(budget_lon, budget_lat)

            if i == 1:
                index = (((budget_lat < 85) & (budget_lat > 65) & (budget_lon > 0) & (budget_lon < 85)))
                index2 = (((budget_lat < 85) & (budget_lat > 65) & (budget_lon > 345) & (budget_lon < 359)))
                index = index | index2
            if i == 6:
                index = ((budget_lat < 80) & (budget_lat > 70) & (budget_lon > 165) & (budget_lon < 215))
                # index = ((budget_lat < 75) & (budget_lat > 50) & (budget_lon > 165) & (budget_lon < 215))

            data_avg = []
            for i_data in data:
                i_index = index
                # i_index = (index)&(i_data<0)
                data_avg.append(np.nansum(i_data[i_index]*(weighted_PIOMS[i_index]/np.nansum(weighted_PIOMS[i_index]))))

            return np.array(data_avg)
            # return np.array([data_now, data_cli])

        data_collection = []

        "==================================="
        ts = dt.datetime(int(time_here[0][:4]), int(time_here[0][5:7]), int(time_here[0][8:10])) - timedelta(days=6)
        te = dt.datetime(int(time_here[0][:4]), int(time_here[0][5:7]), int(time_here[0][8:10])) - timedelta(days=1)
        delta = te - ts
        path_SaveAR = path_main_Save + 'before/' + i_file + '/'
        os.makedirs(path_SaveAR, exist_ok=True)

        for i_ts_te in pd.date_range(ts, te):
            t_now = (i_ts_te - timedelta(days=1)).strftime('%m%d.nc')

            data_collection.append(load_budget_inGivenFile(path_SaveAR,
                                                           t_now))



        "==================================="
        "==================================="
        ts = dt.datetime(int(time_here[0][:4]), int(time_here[0][5:7]), int(time_here[0][8:10]))
        te = dt.datetime(int(time_here[-1][:4]), int(time_here[-1][5:7]), int(time_here[-1][8:10]))


        delta = te - ts

        path_SaveAR = path_main_Save + 'AR/' + i_file + '/'
        os.makedirs(path_SaveAR, exist_ok=True)

        count_after_day = 0
        for i_ts_te in pd.date_range(ts, te):
            if count_after_day>=8:
                break
            t_now = (i_ts_te - timedelta(days=1)).strftime('%m%d.nc')
            data_collection.append(load_budget_inGivenFile(path_SaveAR,
                                                           t_now))

            count_after_day += 1


        "==================================="
        "==================================="

        ts = dt.datetime(int(time_here[-1][:4]), int(time_here[-1][5:7]), int(time_here[-1][8:10])) + timedelta(days=1)
        te = dt.datetime(int(time_here[-1][:4]), int(time_here[-1][5:7]), int(time_here[-1][8:10])) + timedelta(days=6)
        delta = te - ts
        path_SaveAR = path_main_Save + 'after/' + i_file + '/'
        os.makedirs(path_SaveAR, exist_ok=True)

        for i_ts_te in pd.date_range(ts, te):
            if count_after_day>=8:
                continue
            t_now = (i_ts_te - timedelta(days=1)).strftime('%m%d.nc')
            data_collection.append(load_budget_inGivenFile(path_SaveAR,
                                                           t_now))

            count_after_day += 1


        data_collection_All.append(data_collection)





    # data_collection_All = np.nanmean(np.array(data_collection_All) , axis=0)
    # nc4.save_nc4(np.array(data_collection_All), path_Save+'aar-65-85'+str(i))

    # AR_delta_All = np.nanmean(np.array(AR_delta_All) , axis=0)
    nc4.save_nc4(np.array(AR_delta_All), path_Save+'AR_delta_'+str(i))

exit()
"""
composite pattern
"""
# for time_use_forAR_name in ['AR' 'all', 'all_butAR']:
#     for iI, i_type in enumerate(np.unique(save_labels_bmus)):
#         if i_type == 0:
#             continue
#         file = file_all[save_labels_bmus == i_type]
#
#         var_AR_pattern = []
#         var_cli_pattern = []
#
#         for i_file in file:
#
#             path_SaveAR = path_main_Save+'%s/'%time_use_forAR_name+i_file+'/'
#
#             def load_budget_inGivenFile(path):
#                 var_pattern = []
#                 file_here = os.listdir(path)
#                 for i_file_here in file_here:
#                     if not len(i_file_here) == 21:
#                         continue
#                     budget = xr.open_dataset(path_SaveAR + i_file_here)
#                     var_in_single_file = []
#                     for var in budget_load:
#                         var_in_single_file.append(budget[var].values[0])
#                     var_pattern.append(var_in_single_file)
#                 return np.nansum(np.array(var_pattern), axis=0)
#             var_AR_pattern.append(load_budget_inGivenFile(path_SaveAR))
#
#             path_SaveAR = path_SaveAR + 'anomaly/'
#             var_cli_pattern.append(load_budget_inGivenFile(path_SaveAR))
#
#         var_AR_pattern = np.array(var_AR_pattern)
#         var_cli_pattern = np.array(var_cli_pattern)
#         nc4.save_nc4(var_AR_pattern,
#                      r'/home/linhaozhong/work/AR_NP85/budget/composite/AR_%i_%s'%(i_type,time_use_forAR_name))
#         nc4.save_nc4(var_cli_pattern,
#                      r'/home/linhaozhong/work/AR_NP85/budget/composite/cli_%i_%s'%(i_type,time_use_forAR_name))
