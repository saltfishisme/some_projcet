import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

budget_load = ['intensification', 'advection', 'divergence', 'residual', 'dynamics',
               'ice_drift_x', 'ice_drift_y',
               'thickness','concentration'
               ]
plus = [24*60*60,24*60*60,24*60*60,
        24*60*60,24*60*60,
        1,1,
        1,1]
# plus = dict(zip(budget_load, plus))
path = r'D:\budget/budfields_20060104.nc'


budget = xr.open_dataset(path)
var_in_single_file = []
for var in budget_load:
    var_in_single_file.append(budget[var].values[0])

lon = budget.lon.values[0]
lat = budget.lat.values[0]
fig, axs = plt.subplots(2, 4, subplot_kw={'projection': ccrs.NorthPolarStereo()},figsize=[10,6])
for i_i, i in enumerate([0, 1, 2, 3, 4, 7, 8]):
    ax = axs[int(i_i / 4), i_i % 4]

    data = var_in_single_file[i]
    import cartopy.crs as ccrs
    import matplotlib.colors as colors


    data[data==0]=np.nan
    cb = ax.pcolormesh(lon, lat, data*plus[i], transform=ccrs.PlateCarree(),
                       cmap='bwr', vmin=-0.2,vmax=0.2)
    plt.colorbar(cb, ax=ax)
    ax.coastlines()
    ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())

stab = 8
u = var_in_single_file[5][::stab, ::stab]
v = var_in_single_file[6][::stab, ::stab]
cb = axs[1][3].quiver(lon[::stab, ::stab], lat[::stab, ::stab], u, v, transform=ccrs.PlateCarree())
axs[1][3].set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
axs[1][3].coastlines()

plt.show()
