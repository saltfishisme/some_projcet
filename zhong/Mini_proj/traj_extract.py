import xarray as xr
import pandas as pd
import numpy as np
import scipy.io as scio
import os

path_Inputinfo = r"D:\OneDrive\a_matlab\Mini_proj\China_0.5\China-5deg_Inputinfo.mat"
path_mask = r'D:\OneDrive\basis\some_projects\zhong\Mini_proj\dongbei_0_5degGrid_H.mat'
path_data = r"D:\OneDrive\a_matlab\Mini_proj\China_0.5\output_China-5deg\20220728_1-level-int_trajN.mat"


mask_sta = scio.loadmat(path_mask)
lon_H_sta = mask_sta['lon_H']
lat_H_sta = mask_sta['lat_H']
mask_sta = mask_sta['mask']

# Step01: rewrite Inputinfo
Ntraj = np.argwhere(mask_sta>0).shape[0]
# lat_ts=latmin;lat_tn=latmax;lon_tw=lonmin;lon_te=lonmax;
lat_ts = np.min(lat_H_sta[mask_sta>0])
lat_tn = np.max(lat_H_sta[mask_sta>0])
lon_tw = np.min(lon_H_sta[mask_sta>0])
lon_te = np.max(lon_H_sta[mask_sta>0])
seedidx = np.argwhere(mask_sta>0)+1

inputinfo = scio.loadmat(path_Inputinfo)
inputinfo['Ntraj'] = Ntraj
inputinfo['lat_ts'] = lat_ts
inputinfo['lat_tn'] = lat_tn
inputinfo['lon_tw'] = lon_tw
inputinfo['lon_te'] = lon_te

### get subregion idx in row region
seedidx_row = inputinfo['seedidx']
seedidx_index = (seedidx_row[:, None] == seedidx).all(-1).any(-1)

inputinfo['seedidx'] = np.array(seedidx, dtype='double')

os.rename(path_Inputinfo, path_Inputinfo[:-4]+'_old.mat')
scio.savemat(path_Inputinfo, inputinfo)

# Step01: rewrite res-6hr.mat
data = scio.loadmat(path_data)

for ikey in data.keys():
    if ikey[:1] == '_':
        continue

    ivalue = data[ikey]
    if ivalue.shape[-1] == seedidx_row.shape[0]:
        data[ikey] = np.array(ivalue[..., seedidx_index], dtype='double')
os.rename(path_data, path_data[:-4]+'_old.mat')
scio.savemat(path_data, data)

