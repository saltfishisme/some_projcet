import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import pearsonr
import xarray as xr
import scipy.io as scio
import nc4
from sklearn import preprocessing

def cal_area(lon, lat):
    import math
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S

def area_weighted_mean(data, area_weight):

    area_weight = area_weight / np.sum(area_weight)

    avg_AR = np.nansum(data * area_weight)
    return avg_AR

# # # fig1
tcwv_yearly = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\contributeTrend/tcwv_yearly', )
siconc_yearly=nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\contributeTrend/siconc_yearly', )
t2m_yearly=nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\contributeTrend/t2m_yearly', )

# ar_yearly = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\contributeTrend\AR_wv_trans_Arctic')
ivt_yearly2d = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/ivt_ar_yearly')
density_yearly2d = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/density_ar_yearly')
freq_yearly2d = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/freq_ar_yearly')

ivt_yearly2d = ivt_yearly2d/freq_yearly2d
duration_yearly2d = density_yearly2d/freq_yearly2d


weight = cal_area(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5))
ivt_yearly = []
duration_yearly = []
for i in range(ivt_yearly2d.shape[0]):
    ivt_yearly.append(area_weighted_mean(ivt_yearly2d[i], weight))
    duration_yearly.append(area_weighted_mean(duration_yearly2d[i], weight))


df = pd.DataFrame(index=np.arange(1979,2020+1))
# df['ivt'] = ivt_yearly
# df['duration'] = duration_yearly
def fast_moving_average(x, N):
    return np.convolve(x, np.ones((N,)) / N)[(N - 1):]
from scipy import signal
freq = np.sum(freq_yearly2d,axis=(1,2))
print(freq.shape)
print(signal.savgol_filter(freq,
                           5, # window size used for filtering
                           3).shape)
df['freq'] = freq


# df['ar'] = ar_yearly
# df['tcwv'] = tcwv_yearly[:-2]
# df['sic'] = siconc_yearly[:-2]
# df['t2m'] = t2m_yearly[:-2]

def plot_columns(df, linregress_2000=[]):
    x = df.values #returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df=pd.DataFrame(x_scaled, columns=df.columns, index=df.index)
    # df['year'] = np.arange(1979,2020+1)
    df_2000 = df.loc[2000:].copy()
    lens = 0
    fig, ax = plt.subplots()
    colors = ['black', 'red', 'orange', 'orange']
    for i_columns in df.columns:
        d = np.polyfit(df.index, df[i_columns], 1)
        from scipy.stats import linregress
        slope, intercept, r_value, p_value, std_err = linregress(df.index, df[i_columns])
        print(i_columns, p_value)
        f = np.poly1d(d)

        # print(df)
        ax.plot(df.index, df[i_columns],'--', label=i_columns, color=colors[lens], linewidth=0.5)
        ax.plot(df.index, f(df.index),'-', color=colors[lens])

        if i_columns in linregress_2000:

            d = np.polyfit(df_2000.index, df_2000[i_columns], 1)
            from scipy.stats import linregress
            slope, intercept, r_value, p_value, std_err = linregress(df_2000.index, df_2000[i_columns])
            print(i_columns, p_value)
            f = np.poly1d(d)
            ax.plot(df_2000.index, f(df_2000.index), 'r-.', color=colors[lens])

        lens += 1



    # df.plot()
    plt.legend()
plot_columns(df, df.columns)
plt.show()

exit()

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east','pacific_east','MS',
             'greenland_west', 'pacific_west', 'GE']

plot_name = ['(a) GRE (28,4.7%)','(c) BSE (19,4.4%)', '(e) MED (26,3.8%)',
             '(b) BAF (20,7.1%)', '(d) BSW (30,13.1%)', '(f) ARC (10,1.6%)']

central_lon = [359,180,75,300,180,75]

var_fmt = ['%0.1f', '%i', '%i', '%0.1f', '%0.1f']
var_title = []
var_ylabel = ['impacted region/the Arctic (units:%)', 'duration (units:6h)']

resolution=0.5



# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5,3.9])

plt.subplots_adjust(top=0.97,
bottom=0.1,
left=0.005,
right=0.995,
hspace=0.14,
wspace=0.055)


ax = fig.add_subplot(1,1,1, projection=ccrs.Orthographic(0, 90))

params = {
         'sic':['freq_trend_ar_pattern'],
            'strd':['ivt_trend_ar_pattern'],
          't2m':['duration_trend_ar_pattern'],
          }
i_columns = 'sic'
trend = scio.loadmat(r'D:\OneDrive\a_matlab\result_ivt.mat')['%s_re' % i_columns]

import matplotlib.colors as colors
print(np.arange(-0.5,0.51,0.1))
cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:140], abs(trend[:140])*100
                 ,extend='both',levels=np.arange(0,101,10),
                 cmap=cmaps.cmocean_balance,
            transform=ccrs.PlateCarree())
plt.colorbar(cb)
# ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:140], np.ma.masked_greater(p, 0.1),
#
#             colors='none', levels=[0, 0.1],
#             hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
# ax.set_global()
ax.coastlines(linewidth=0.3,zorder=10)
ax.set_title(i_columns)
ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
# plt.show()
plt.savefig(i_columns + '_ivt.png', dpi=400)
plt.close()


exit(0)



ds = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\T2mTcwvSic_DJF.nc')

# # deseasonal
climatology = ds.groupby('time.month').mean('time')
ds= ds.groupby("time.month") - climatology
ds.to_netcdf(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\T2mTcwvSic_DJF_deseasonal.nc')
#
ds = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\T2mTcwvSic_DJF_deseasonal.nc')

ds = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\T2mTcwvSic_DJF.nc')
# yearly series


# Wrap it into a simple function
def weighted_temporal_mean(ds, var):
    """
    weight by days in each month
    """
    # Determine the month length
    month_length = ds.time.dt.days_in_month

    # Calculate the weights
    wgts = month_length.groupby("time.year") / month_length.groupby("time.year").sum()

    # Make sure the weights in each year add up to 1
    np.testing.assert_allclose(wgts.groupby("time.year").sum(xr.ALL_DIMS), 1.0)

    # Subset our dataset for our variable
    obs = ds[var]
    year = obs['time.year']
    month = obs['time.month']
    year[month == 12] = year[month == 12] + 1
    obs['time.year'] = year

    # Setup our masking for nan values
    cond = obs.isnull()
    ones = xr.where(cond, 0.0, 1.0)

    # Calculate the numerator
    obs_sum = (obs * wgts).groupby("time.year").sum(dim="time")

    # Calculate the denominator
    ones_out = (ones * wgts).groupby("time.year").sum(dim="time")

    # Return the weighted average
    return obs_sum / ones_out


t2m = weighted_temporal_mean(ds, 't2m').values
strd = weighted_temporal_mean(ds, 'strd').values
siconc = weighted_temporal_mean(ds, 'siconc').values
tcwv = weighted_temporal_mean(ds, 'tcwv').values

scio.savemat('T2mStrdSic_yearlyPattern.mat',
             {'t2m':np.array(t2m, dtype='double'),
            'strd':np.array(strd, dtype='double'),
            'siconc':np.array(siconc, dtype='double'),
              })
exit(0)
a = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend\density_ar_yearly')
print(a.shape)
scio.savemat('ar_yearlyPattern_detrend.mat',
             {'ar':np.array(a, dtype='double'),})
a = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend\ivt_ar_yearly')
print(a.shape)
scio.savemat('ivt_yearlyPattern.mat',
             {'ar':np.array(a, dtype='double'),})

def cal_area(lon, lat):
    import math
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S


def area_weighted_mean(data, area_weight):
    area_weight = area_weight / np.sum(area_weight)

    avg_AR = np.nansum(data * area_weight, axis=(1,2))
    return avg_AR




area_weight = cal_area(ds.longitude.values, ds.latitude.values)
area_weight[ds.latitude.values<66]=0


tcwv_yearly = area_weighted_mean(tcwv, area_weight)

siconc_yearly = area_weighted_mean(siconc, area_weight)
t2m_yearly = area_weighted_mean(t2m, area_weight)

nc4.save_nc4(tcwv_yearly, r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\contributeTrend/tcwv_yearly', )
nc4.save_nc4(siconc_yearly, r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\contributeTrend/siconc_yearly', )
nc4.save_nc4(t2m_yearly,r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\contributeTrend/t2m_yearly', )





