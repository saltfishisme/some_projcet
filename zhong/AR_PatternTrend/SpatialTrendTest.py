import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from scipy.stats import linregress
import nc4
# ar = scipy.io.loadmat(r"D:\ar_yearly_YY.mat")['ar_type']

# import nc4

# freq = nc4.read_nc4(r"D:\density_ar_yearly_YY")
ssp = 'ssp585'
sivol_tas = 'tas'
# field = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6\model_%s_%s' % (ssp, sivol_tas))
#
# field = np.nanmean(field, axis=0)[:85,]

field = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_DRM\mositure_contribution\pwm_loc_eul_mean_yearly")

field = field[50:,]
additional_name = r''
period = np.arange(1990,2022+1)
mutation_year = 2000-1979

data_row = field

skip_0 = False
data_row[np.isnan(data_row)] = 0
import xarray as xr
# a = xr.open_dataset(r"E:\Guan_DJF.nc")
# lon = a.lon.values
# lat = a.lat.values

n_lats = data_row.shape[1]
n_lons = data_row.shape[2]

save_name = ['%i_%i' % (period[0], period[-1]), '%i_%i' % (period[0], period[mutation_year]),
             '%i_%i' % (period[mutation_year], period[-1])]
# # for iI, data in enumerate([data_row, data_row[:mutation_year], data_row[mutation_year:]]):
for iI, data in enumerate([data_row[:]]):
#     trend = np.zeros((n_lats, n_lons))
#     p_values = np.ones((n_lats, n_lons))
#
#     for i in range(n_lats):
#         for j in range(n_lons):
#             s_data = data[:, i, j]
#             ######
#             # if (density[:,i,j] == 0).any():
#             #     print('hi')
#             if skip_0:
#                 s_data = s_data[s_data != 0]
#             if len(s_data) != 0:
#                 # slope, intercept, r_value, p_value, std_err = linregress(xxxxxx, s_data)
#
#                 slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
#                 trend[i, j] = slope
#                 p_values[i, j] = p_value
#     # #
#     nc4.save_nc4(trend, 'trend%s'%additional_name)
#     nc4.save_nc4(p_values, 'p%s'%additional_name)

    ################################ plot #########################################
    trend = nc4.read_nc4(r'trend%s'%additional_name)
    print(trend.shape)
    p_values = nc4.read_nc4(r'p%s'%additional_name)
    # plt.imshow(p_values)
    # plt.show()
    import cartopy.crs as ccrs
    import cmaps

    fig = plt.figure(figsize=[6.5,3])

    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(0, 90))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=100))

    import matplotlib.pyplot as plt
    import numpy as np
    import numpy.ma as ma
    import scipy.special as sp
    import cartopy.crs as ccrs


    # trend[np.isnan(trend)] = 0
    trend[trend == 0] = np.nan
    import matplotlib.colors as colors

    # X, Y, masked_drm = z_masked_overlap(
    #     ax, np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :],
    #     source_projection=ccrs.Geodetic())
    # cb = ax.contourf(X, Y, masked_drm, 51,
    #             cmap='seismic')
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  ,levels=[-0.1,-0.05,-0.025, -0.0001, 0.0001, 0.025,0.05,0.1], extend='both', cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    trend[np.abs(trend) < 1.e-3] = 0.
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())
    cb = ax.pcolormesh(np.arange(0.25, 360, 0.5), np.arange(89.75, -90.01, -0.5),
    # np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5),
                        trend
                     , cmap='bwr_r', norm=colors.CenteredNorm(),
                     transform=ccrs.PlateCarree())
    plt.colorbar(cb, orientation="horizontal")
    ax.contourf(np.arange(0.25, 360, 0.5), np.arange(89.75, -90.01, -0.5),
                np.ma.masked_greater(p_values, 0.1),

                colors='none', levels=[0, 0.1],
                hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(draw_labels=True, ylocs=[0,20,40,60,66,80], linewidth=0.3, color='black')
    ax.set_extent([0, 359, -5, 90], crs=ccrs.PlateCarree())

    plt.show()
    plt.close()


import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from scipy.stats import linregress
import nc4
# ar = scipy.io.loadmat(r"D:\ar_yearly_YY.mat")['ar_type']

# import nc4

# freq = nc4.read_nc4(r"D:\density_ar_yearly_YY")
ssp = 'ssp585'
sivol_tas = 'tas'
# field = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6\model_%s_%s' % (ssp, sivol_tas))
#
# field = np.nanmean(field, axis=0)[:85,]

field = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_DRM\mositure_contribution\pwm_loc_eul_mean_yearly")

field = field[50:,]
additional_name = r''
period = np.arange(1990,2022+1)
mutation_year = 2000-1979

data_row = field

skip_0 = False
data_row[np.isnan(data_row)] = 0
import xarray as xr
# a = xr.open_dataset(r"E:\Guan_DJF.nc")
# lon = a.lon.values
# lat = a.lat.values

n_lats = data_row.shape[1]
n_lons = data_row.shape[2]

save_name = ['%i_%i' % (period[0], period[-1]), '%i_%i' % (period[0], period[mutation_year]),
             '%i_%i' % (period[mutation_year], period[-1])]
# # for iI, data in enumerate([data_row, data_row[:mutation_year], data_row[mutation_year:]]):
for iI, data in enumerate([data_row[:]]):
#     trend = np.zeros((n_lats, n_lons))
#     p_values = np.ones((n_lats, n_lons))
#
#     for i in range(n_lats):
#         for j in range(n_lons):
#             s_data = data[:, i, j]
#             ######
#             # if (density[:,i,j] == 0).any():
#             #     print('hi')
#             if skip_0:
#                 s_data = s_data[s_data != 0]
#             if len(s_data) != 0:
#                 # slope, intercept, r_value, p_value, std_err = linregress(xxxxxx, s_data)
#
#                 slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
#                 trend[i, j] = slope
#                 p_values[i, j] = p_value
#     # #
#     nc4.save_nc4(trend, 'trend%s'%additional_name)
#     nc4.save_nc4(p_values, 'p%s'%additional_name)

    ################################ plot #########################################
    trend = nc4.read_nc4(r'trend%s'%additional_name)
    print(trend.shape)
    p_values = nc4.read_nc4(r'p%s'%additional_name)
    # plt.imshow(p_values)
    # plt.show()
    import cartopy.crs as ccrs
    import cmaps

    fig = plt.figure(figsize=[6.5,3])

    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(0, 90))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=100))

    import matplotlib.pyplot as plt
    import numpy as np
    import numpy.ma as ma
    import scipy.special as sp
    import cartopy.crs as ccrs


    # trend[np.isnan(trend)] = 0
    trend[trend == 0] = np.nan
    import matplotlib.colors as colors

    # X, Y, masked_drm = z_masked_overlap(
    #     ax, np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :],
    #     source_projection=ccrs.Geodetic())
    # cb = ax.contourf(X, Y, masked_drm, 51,
    #             cmap='seismic')
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  ,levels=[-0.1,-0.05,-0.025, -0.0001, 0.0001, 0.025,0.05,0.1], extend='both', cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    trend[np.abs(trend) < 1.e-3] = 0.
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())
    cb = ax.pcolormesh(np.arange(0.25, 360, 0.5), np.arange(89.75, -90.01, -0.5),
    # np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5),
                        trend
                     , cmap='bwr_r', norm=colors.CenteredNorm(),
                     transform=ccrs.PlateCarree())
    plt.colorbar(cb, orientation="horizontal")
    ax.contourf(np.arange(0.25, 360, 0.5), np.arange(89.75, -90.01, -0.5),
                np.ma.masked_greater(p_values, 0.1),

                colors='none', levels=[0, 0.1],
                hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(draw_labels=True, ylocs=[0,20,40,60,66,80], linewidth=0.3, color='black')
    ax.set_extent([0, 359, -5, 90], crs=ccrs.PlateCarree())

    plt.show()
    plt.close()