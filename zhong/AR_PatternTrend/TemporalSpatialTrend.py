import numpy as np
import os
import scipy.io as scio

"""
when calculate the area-weighted mean, we will ## mask 0 ## to avoid error.
"""


"""  additional process for IVT  """
# a = scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\ivt_type_ar_yearly_sum.mat")['ar_type']
# b = scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\density_type_ar_yearly.mat")['ar_type']
# a = a/b
# scio.savemat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\ivt_type_ar_yearly.mat", {'ar_type':a})
# exit()

ar_type = False
file_exist = True
path_Main = r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering/'
VAR = 'freq'
PARAM_VAR = {'ivt':[True, [path_Main + 'ivt_ar_yearly', 'ar_type'], False, [np.arange(1979, 2020 + 1), 2000]],
             'freq':[False, [path_Main+'freq_ar_yearly', 'ar_type'], True, [np.arange(1980,2019+1), 2000]],
             'density':[True, [path_Main+'density_ar_yearly', 'ar_type'], True, [np.arange(1980,2019+1), 2000]]
             }
# for Trend, keep 0 value or not?
# file path and mat file var name
# cut 1979 and 2020 or not?

period_VAR = PARAM_VAR[VAR][3][0]
mutation_year_VAR = int(PARAM_VAR[VAR][3][1] - period_VAR[0])
arType_shortName = ['GRE', '1', 'BAF', 'BSE' , 'BSW', 'MED', 'ARC', '7']

os.makedirs(path_Main + VAR, exist_ok=True)

main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
file = os.listdir(main_path)

"""
============================cal data [year, lat, lon]=============================================
"""

# import os
# import numpy as np
# import pandas as pd
# import xarray as xr
# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# import nc4
# def load_ivt_total(s_time):
#     '''
#     :param s_time: datetime64
#     :return: ivt_total
#     '''
#     s_time = pd.to_datetime(s_time)
#
#     s_time_str = s_time.strftime('%Y%m%d')
#     index_vivt = xr.open_dataset(
#         path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
#             s_time_str[:4], s_time_str))
#     index_uivt = xr.open_dataset(
#         path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
#             s_time_str[:4], s_time_str))
#     # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
#     # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))
#
#     ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
#     ivt_east = index_uivt.sel(time=s_time)['p71.162'].values
#
#     ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
#     return ivt_total
#
#
#
#
# year_begin = 1979
# year_end = 2020
# density = np.zeros([year_end - year_begin + 1, 361, 720])
# avg_freq = np.zeros([year_end-year_begin+1, 361,720])
# ivt_all = np.zeros([year_end-year_begin+1, 361,720])
#
# for i_file in file:
#     info_AR_row = xr.open_dataset(main_path+i_file)
#     time_AR = info_AR_row.time_AR.values
#     time_all = info_AR_row.time_all.values
#     AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#     time_use = int(len(time_all[AR_loc:]) / 2)
#
#     contour_all = np.zeros([361,720])
#
#     for i_time in time_AR:
#         year_loc = int(pd.to_datetime(i_time).strftime('%Y'))-year_begin
#         if pd.to_datetime(i_time).strftime('%m') == '12':
#             year_loc = year_loc + 1
#         if year_loc == year_end - year_begin + 1:
#             continue
#         # sum for all year
#
#         contour = info_AR_row['contour_all'].sel(time_all=i_time).values
#
#         ivt = load_ivt_total(pd.to_datetime(i_time))
#         ivt[contour<0.5] = 0
#         density[year_loc] += contour
#         ivt_all[year_loc] += ivt
#
#         contour_all += contour
#     contour_all[contour_all>0] = 1
#     if year_loc == year_end - year_begin + 1:
#         continue
#     avg_freq[year_loc] += contour_all
#
# import scipy.io  as scio
# ivt_all = ivt_all / density
# ivt_all[np.isnan(ivt_all)] = 0
# scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering//ivt_ar_yearly.mat', {'ar_type':np.array(ivt_all)})
# scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/freq_ar_yearly.mat', {'ar_type':np.array(avg_freq)})
# scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/density_ar_yearly.mat', {'ar_type':np.array(density)})
# # exit(0)
"""
============================ temporal trend =============================================
"""
path_Save_now = path_Main + VAR + '/'

import matplotlib.pyplot as plt
from sklearn import preprocessing
import nc4
import scipy.io as scio
import pandas as pd
import numpy as np
import xarray as xr

def plot_columns(df_row, linregress_2000=[]):
    # x = df_row.values  # returns a numpy array
    # min_max_scaler = preprocessing.MinMaxScaler()
    # x_scaled = min_max_scaler.fit_transform(x)
    # df = pd.DataFrame(x_scaled, columns=df_row.columns, index=period)

    df = df_row.copy()
    # df['year'] = np.arange(1979,2020+1)
    df_mutation = df.iloc[mutation_year_VAR:].copy()

    lens = 0
    fig, ax = plt.subplots()
    colors = ['#FF0000', '#00FF00', '#0000FF', '#FFA500', '#9400D3', '#FFC0CB', '#00FFFF', '#FFFF00']


    for i_columns in df.columns:
        # plot raw line
        ax.plot(df.index, df[i_columns], '-', label=i_columns, color=colors[lens], linewidth=0.3)

        # fit 1979-2020

        from scipy.stats import linregress

        notnan_loc = np.argwhere(~np.isnan(np.array(df[i_columns])))
        x = np.array(df.index)[notnan_loc].flatten()
        y = np.array(df[i_columns])[notnan_loc].flatten()
        print(x,y)
        slope, intercept, r_value, p_value, std_err = linregress(x,y)
        f = x * slope + intercept
        if p_value < 0.1:
            ax.plot(x, f, '-', color=colors[lens])
        else:
            ax.plot(x, f, '-.', color=colors[lens], linewidth=0.8)

        # fit 1979-2020
        if i_columns in linregress_2000:
            from scipy.stats import linregress
            notnan_loc = np.argwhere(~np.isnan(np.array(df_mutation[i_columns])))
            x = np.array(df_mutation.index)[notnan_loc].flatten()
            y = np.array(df_mutation[i_columns])[notnan_loc].flatten()
            slope, intercept, r_value, p_value, std_err = linregress(x, y)
            print(slope)
            f = x * slope + intercept
            if p_value < 0.1:
                ax.plot(x, f, '-', color=colors[lens])
            else:
                ax.plot(x, f, '-.', color=colors[lens], linewidth=0.8)
        lens += 1
    plt.legend()

def cal_area(lon, lat):
    import math
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S

def area_weighted_mean(data, area_weight, maskvalue=''):

    if not isinstance(maskvalue, str):
        area_weight[data==maskvalue] = np.nan
    area_weight[np.isnan(data)] = np.nan

    area_weight = area_weight / np.nansum(area_weight)
    avg_AR = np.nansum(data * area_weight)

    return avg_AR


data_row = scio.loadmat(PARAM_VAR[VAR][1][0])[PARAM_VAR[VAR][1][1]]
if PARAM_VAR[VAR][2]:
    if ar_type:
        data_row = data_row[:, 1:-1, :]  # cut 1979, 2020
    else:
        data_row = data_row[1:-1, :,:]  # cut 1979, 2020

weight = cal_area(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5))

########## for Arctic ##########

if ar_type:
    data = np.nansum(data_row, axis=0)

else:
    data = data_row.copy()
avg = []

for ii in range(data.shape[0]):
    avg.append(area_weighted_mean(data[ii], weight.copy(), maskvalue=0))

avg = np.array(avg)
if PARAM_VAR[VAR][0]:
    avg[avg == 0] = np.nan
from scipy import signal
from scipy.interpolate import splrep, BSpline
df = pd.DataFrame(index=period_VAR)
# df[var] = np.array(signal.savgol_filter(avg,
#                           15 , # window size used for filtering
#                            5)) # order of fitted polynomial
# x = np.array(param_var[var][-1])[0]
# y = avg
# print(x.shape, y.shape)
# tck = splrep(x, y, s=1)
# df[var] = np.array(BSpline(*tck)(x)) # order of fitted polynomial
df[VAR] = avg
plot_columns(df, df.columns)
plt.show()
plt.savefig(path_Save_now +'%s_Arctic_TemporalTrend.png' % VAR, dpi=400)
plt.close()

########## for arType ##########
if ar_type:
    df = pd.DataFrame(index=period_VAR)
    for i_ar in np.arange(data_row.shape[0]):
        avg = []
        for ii in range(data_row.shape[1]):
            avg.append(area_weighted_mean(data_row[i_ar, ii], weight.copy(), maskvalue=0))

        avg = np.array(avg)
        if PARAM_VAR[VAR][0]:
            avg[avg == 0] = np.nan
        df[VAR + ' ' + arType_shortName[i_ar]] = avg

    plot_columns(df, df.columns)
    # plt.show()
    plt.savefig(path_Save_now +'%s_arType_TemporalTrend.png' % VAR, dpi=400)
    plt.close()

"""
============================ spatial trend =============================================
"""

#%%
import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from scipy.stats import linregress
import nc4

# ar = scio.loadmat(param_var[var][1][0])[param_var[var][1][1]]

# import cartopy.crs as ccrs
# fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})
# ax.contourf(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), np.nansum(ar[1], axis=(0)), transform=ccrs.PlateCarree())
# ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
# ax.coastlines(linewidth=0.3,zorder=10)
# plt.show()
# plt.show()


ar = scio.loadmat(PARAM_VAR[VAR][1][0])[PARAM_VAR[VAR][1][1]]
if VAR == 'ivt':
    ar[np.isnan(ar)] = 0
if PARAM_VAR[VAR][2]:
    ar = ar[:,1:-1,:] # cut 1979, 2020

def spatial_trend(data_row, save_name):
    n_lats = data_row.shape[1]
    n_lons = data_row.shape[2]

    period_name = [
                 '%i_%i' % (period_VAR[mutation_year_VAR], period_VAR[-1])]
    # period_name = ['%i_%i' % (period[0], period[-1]), '%i_%i' % (period[0], period[mutation_year]),
    #              '%i_%i' % (period[mutation_year], period[-1])]
    # for iI, data in enumerate([data_row, data_row[:mutation_year], data_row[mutation_year:]]):
    for iI, data in enumerate([data_row[mutation_year_VAR:]]):
        save_path = path_Main + VAR + '/spatialTrend/' + period_name[iI] + '/'
        os.makedirs(save_path, exist_ok=True)

        if not file_exist:
            trend = np.zeros((n_lats, n_lons))
            p_values = np.ones((n_lats, n_lons))

            for i in range(n_lats):
                for j in range(n_lons):
                    s_data = data[:, i, j]
                    ######
                    # if (density[:,i,j] == 0).any():
                    #     print('hi')
                    if PARAM_VAR[VAR][0]:
                        s_data = s_data[s_data != 0]
                    if len(s_data) != 0:
                        slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
                        trend[i, j] = slope
                        p_values[i, j] = p_value

            nc4.save_nc4(trend, save_path + r'%s_trend_ar_pattern' % (VAR + '_' + save_name))
            nc4.save_nc4(p_values, save_path + r'%s_p_ar_pattern' % (VAR + '_' + save_name))

        else:
            trend = nc4.read_nc4(save_path + r'%s_trend_ar_pattern' % (VAR + '_' + save_name))
            p_values = nc4.read_nc4(save_path + r'%s_p_ar_pattern' % (VAR + '_' + save_name))

        ################################# plot #########################################

        import cartopy.crs as ccrs
        import cmaps

        fig = plt.figure(figsize=[5, 3.9])
        plt.subplots_adjust(top=0.9,
                            bottom=0.05,
                            left=0.005,
                            right=0.995,
                            hspace=0.14,
                            wspace=0.055)
        ax = fig.add_subplot(1, 1, 1, projection=ccrs.NorthPolarStereo())

        plt.title(VAR + '_' + period_name[iI])

        # trend[np.isnan(trend)] = 0
        trend[trend == 0] = np.nan
        import matplotlib.colors as colors
        import matplotlib.colors as colors
        cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
                         , cmap='bwr', norm=colors.CenteredNorm(),
                         transform=ccrs.PlateCarree())
        plt.colorbar(cb)
        ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:120],
                    np.ma.masked_greater(p_values[:120, :], 0.1),

                    colors='none', levels=[0, 0.05],
                    hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
        # ax.set_global()
        ax.coastlines(linewidth=0.3, zorder=10)
        # ax.stock_img()
        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
        plt.title(VAR + '_' + save_name)
        plt.show()
        plt.savefig(
            save_path + '%s_trend_pattern.png' % (VAR + '_' + save_name),
            dpi=400)
        plt.close()




if ar_type:
    ar_sum = np.sum(ar, axis=0)
else:
    ar_sum = ar
spatial_trend(ar_sum, 'total')

if ar_type:
    for iloop in range(ar.shape[0]):
        data_row = ar[iloop]
        spatial_trend(data_row, arType_shortName[iloop])