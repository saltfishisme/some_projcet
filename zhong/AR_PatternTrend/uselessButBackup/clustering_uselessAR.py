""" freq  """

import os
import numpy as np
import pandas as pd
import xarray as xr
import scipy.io as scio
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'
year_begin = 1979
year_end = 2020

file_a = os.listdir(r'/home/linhaozhong/work/AR_NP85/AR_contour_42/')
density = []

for season in ['DJF']:
    df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

    for iI, i in enumerate(df.columns):
        avg = np.zeros([year_end - year_begin + 1, 361, 720])

        times_file_all = df[i].values
        for time_file in times_file_all:
            if len(str(time_file)) < 5:
                continue
            density.append(time_file)

import shutil
for i in density:
    shutil.copy('/home/linhaozhong/work/AR_NP85/AR_contour_42/%s'%i,'/home/linhaozhong/work/AR_NP85/AR_contour_42_new/')
    shutil.copy('/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR/%s'%i,'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_new/')
# for i in file_a:
#     if not i in density:
#         print(i)


