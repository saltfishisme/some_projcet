"""
get grid-trend and P from given param file [time, lat, lon]
plot param in given range and colors
plot trend and p
"""
# import matplotlib.pyplot as plt
# import numpy as np
# from scipy.stats import linregress
# import nc4
# ivt = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\OccurrenceTrend/ivt_ar_yearly")
# density_begin = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\OccurrenceTrend\density_all_begin_yearly")
# density = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\OccurrenceTrend/density_ar_yearly")
# freq = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\OccurrenceTrend/freq_ar_yearly")
#
#
# for iloop in [['ivt', ivt/freq],
#               ['duration', density/freq],
#               ['freq', freq],
#               ['freq_begin', density_begin/freq]]:
#     var, data = iloop
#     n_lats = data.shape[1]
#     n_lons = data.shape[2]
#     year_begin = 1979
#     year_end = 2020
#     years = np.arange(year_begin, year_end+1)
#     # 创建空数组用于存放趋势和显著性检验结果
#     trend = np.zeros((n_lats, n_lons))
#     p_values = np.ones((n_lats, n_lons))
#
#     mutation_year_loc =2000-1979
#     # 针对每个经纬度格点，计算趋势并进行显著性检验
#     for i in range(n_lats):
#         for j in range(n_lons):
#             s_data = data[:mutation_year_loc+1, i, j]
#
#             ######
#             # if (density[:,i,j] == 0).any():
#             #     print('hi')
#             s_data = s_data[s_data!=0]
#             if len(s_data) != 0:
#                 from scipy.stats import linregress
#                 slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
#                 trend[i, j] = slope
#                 p_values[i, j] = p_value
#
#     nc4.save_nc4(trend, r'D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\OccurrenceTrend/1979_2000/%s_trend_ar_pattern'%var)
#     nc4.save_nc4(p_values, r'D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\OccurrenceTrend/1979_2000/%s_p_ar_pattern'%var)
# exit(0)

"""
==============================plot param in given range and colors========================================
"""

import numpy as np
import nc4
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib.colors import ListedColormap
import matplotlib.colors as mcolors

path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east','pacific_east','MS',
             'greenland_west', 'pacific_west', 'GE']

plot_name = ['(a) GRE (28,4.7%)','(c) BSE (19,4.4%)', '(e) MED (26,3.8%)',
             '(b) BAF (20,7.1%)', '(d) BSW (30,13.1%)', '(f) ARC (10,1.6%)']

central_lon = [359,180,75,300,180,75]

var_fmt = ['%0.1f', '%i', '%i', '%0.1f', '%0.1f']
var_title = []
var_ylabel = ['impacted region/the Arctic (units:%)', 'duration (units:6h)']

resolution=0.5

# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5,3.9])

plt.subplots_adjust(top=0.97,
bottom=0.1,
left=0.005,
right=0.995,
hspace=0.14,
wspace=0.055)


import cmaps
ax = fig.add_subplot(1,1,1, projection=ccrs.NorthPolarStereo())

cmap = ListedColormap(cmaps.GHRSST_anomaly.colors[0:-2])
cmap.set_over(cmap.colors[-1])
# lev = [1,5,10,15,20,25,30,35,40,50,60,70,80,90,100,120,140,160,180]
lev = list(np.arange(1,10,0.5))+list(range(10,20,1))+list(range(20,50,5))
lev = list(np.arange(1,20,0.5))
lev = list(list(range(250,600,35))+list(range(600,800,50))+list(range(800,1000,100)))
norm = mcolors.BoundaryNorm(lev, cmap.N)

density = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\freq_all")

ax.set_title('occurrence')
data = density
# data = np.sum(density[2000-1979:,], axis=0)
# print(np.max(data))
# exit(0)
# plt.imshow(data)
# plt.show()
# data[data>100] = 100
cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.001, -0.5), data,
                 levels = lev, cmap=cmap,extend='both',norm=norm,
            transform=ccrs.PlateCarree())

# cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.001, -0.5), np.ma.masked_less(data,1),
#                  cmap=cmap,extend='both',
#             transform=ccrs.PlateCarree())
# ax.set_global()
# ax.set_title('occurrence Entry into the Arctic')

ax.coastlines(linewidth=0.3,zorder=10)
# ax.stock_img()
ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
# plt.show()
ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)

cb_ax.set_xlabel('counts')
plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)
# plt.savefig('ex_figure1_500hPa.png', dpi=400)
plt.close()
exit()


"""
===============================plot trend and P =============================================
"""

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east','pacific_east','MS',
             'greenland_west', 'pacific_west', 'GE']

plot_name = ['(a) GRE (28,4.7%)','(c) BSE (19,4.4%)', '(e) MED (26,3.8%)',
             '(b) BAF (20,7.1%)', '(d) BSW (30,13.1%)', '(f) ARC (10,1.6%)']

central_lon = [359,180,75,300,180,75]

var_fmt = ['%0.1f', '%i', '%i', '%0.1f', '%0.1f']
var_title = []
var_ylabel = ['impacted region/the Arctic (units:%)', 'duration (units:6h)']

resolution=0.5

# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5,3.9])

plt.subplots_adjust(top=0.97,
bottom=0.1,
left=0.005,
right=0.995,
hspace=0.14,
wspace=0.055)


ax = fig.add_subplot(1,1,1, projection=ccrs.Orthographic(0, 90))

params = {
         'freq trend':['freq_trend_ar_pattern','freq_p_ar_pattern','freq trend', [-0.45,0.45]],
            'ivt':['ivt_trend_ar_pattern','ivt_p_ar_pattern', 'ivt trend',[-80,80]],
          'duration':['duration_trend_ar_pattern','duration_p_ar_pattern', 'duration trend', [-0.24, 0.24]],
            'initial freq trend':['freq_begin_trend_ar_pattern','freq_begin_p_ar_pattern',
                                  'the initial freq trend', [-0.6,0.6]]
          }
var = 'initial freq trend'
trend = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/1979_2000/%s'%params[var][0])[:140,]
p = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/1979_2000/%s'%params[var][1])[:140,]
plt.title(params[var][2])
# trend[np.isnan(trend)] = 0

import matplotlib.colors as colors
print(np.arange(-0.5,0.51,0.1))
cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:140], trend
                 ,vmin=params[var][3][0],vmax=params[var][3][1],extend='both',
                 cmap=cmaps.cmocean_balance,
            transform=ccrs.PlateCarree())
plt.colorbar(cb)
ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:140], np.ma.masked_greater(p, 0.1),

            colors='none', levels=[0, 0.1],
            hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
# ax.set_global()
ax.coastlines(linewidth=0.3,zorder=10)
# ax.stock_img()
ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
# plt.show()

# cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#
# cb_ax.set_xlabel('trend')
plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)
# plt.savefig('ex_figure1_500hPa.png', dpi=400)
plt.close()





