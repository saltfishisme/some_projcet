import os
# import mat73
import matplotlib.pyplot as plt

import nc4
import scipy.io as scio
import pandas as pd
import numpy as np
import xarray as xr

# time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
#
# path_MainData = r'/home/linhaozhong/work/AR_NP85/'
# year_begin = 1979
# year_end = 2020
#
# density = []
#
# def main(var):
#
#     def contour_cal(time_file, avg):
#
#         # mat file always has some bugs with str type. skip whitespace
#         time_file_new = ''
#         for i in time_file:
#             time_file_new += i
#             if i == 'c':
#                 break
#
#         info_AR_row = xr.open_dataset(path_MainData + 'AR_contour_42/%s' % (time_file_new))
#
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#         AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#         time_use = int(len(time_all[AR_loc:]) / 2)
#
#         year_loc_all = []
#
#         for i_time in time_AR:
#             year_loc = int(pd.to_datetime(i_time).strftime('%Y')) - year_begin
#             year_loc_all.append(year_loc)
#             # sum for all year
#
#             contour = info_AR_row['contour_all'].sel(time_all=i_time).values
#
#             avg[year_loc] += contour
#         return avg
#
#     for season in ['DJF']:
#         df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)
#
#         for iI, i in enumerate(df.columns):
#             avg = np.zeros([year_end - year_begin + 1, 361, 720])
#
#             times_file_all = df[i].values
#             for time_file in times_file_all:
#                 if len(str(time_file)) < 5:
#                     continue
#                 avg = contour_cal(time_file, avg)
#                 print(avg)
#             density.append(avg)
# main('density')
# scio.savemat('/home/linhaozhong/work/AR_NP85/density_type_ar_yearly.mat', {'ar_type':np.array(density)})

"""
===============================================================================================
"""

#%%
import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from scipy.stats import linregress
import nc4
ivt = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/ivt_ar_yearly")
density_begin = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend\density_all_begin_yearly")
density = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/density_ar_yearly")
freq = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_Arctic\OccurrenceTrend/freq_ar_yearly")

# ar = scipy.io.loadmat(r'D:\OneDrive\a_matlab\density_type_ar_yearly.mat')['ar_type']
# ar = scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\freq_type_ar_yearly.mat")['ar_type']
# print(ar.shape)
# for iloop in [['ivt', ivt/freq],
#               ['duration', density/freq],
#               ['freq', freq],
#               ['freq_begin', density_begin/freq]]:


# print(freq.shape)
# # data = np.sum(ar[[0,1,2,3,4,5,6,7],:], axis=0)
# data = freq
# n_lats = data.shape[1]
# n_lons = data.shape[2]
# year_begin = 1979
# year_end = 2020
# years = np.arange(year_begin, year_end+1)
# # 创建空数组用于存放趋势和显著性检验结果
# trend = np.zeros((n_lats, n_lons))
# p_values = np.ones((n_lats, n_lons))
#
# mutation_year_loc =2000-1979
# # 针对每个经纬度格点，计算趋势并进行显著性检验
# for i in range(n_lats):
#     for j in range(n_lons):
#         s_data = data[:, i, j]
#
#         ######
#         # if (density[:,i,j] == 0).any():
#         #     print('hi')
#         # s_data = s_data[s_data!=0]
#         if len(s_data) != 0:
#             slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
#             trend[i, j] = slope
#             p_values[i, j] = p_value
#
# nc4.save_nc4(trend, r'D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\arTypeContribute\trend_ar_pattern_1979')
# nc4.save_nc4(p_values, r'D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\arTypeContribute\p_ar_pattern_1979')

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5, 3.9])

plt.subplots_adjust(top=0.97,
                    bottom=0.1,
                    left=0.005,
                    right=0.995,
                    hspace=0.14,
                    wspace=0.055)

ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(0, 90))

trend = nc4.read_nc4(
    r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/trend_ar_pattern')[:120, ]
p = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/p_ar_pattern')[
    :120, ]
plt.title('freq trend')
# trend[np.isnan(trend)] = 0
# trend[trend < 0.00001] = np.nan
import matplotlib.colors as colors

print(np.arange(-0.5, 0.51, 0.1))
# cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend
#                  , vmin=-5, vmax=5, extend='both',
#                  cmap=cmaps.cmocean_balance,
#                  transform=ccrs.PlateCarree())
cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend
                 , vmin=-0.3, vmax=0.3,
                 cmap=cmaps.cmocean_balance,
                 transform=ccrs.PlateCarree())
plt.colorbar(cb)
ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:120], np.ma.masked_greater(p, 0.1),

            colors='none', levels=[0, 0.1],
            hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
# ax.set_global()
ax.coastlines(linewidth=0.3, zorder=10)
# ax.stock_img()
ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
# plt.show()

# cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#
# cb_ax.set_xlabel('trend')
plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)

plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/1979_2000/ar%i_trend_pattern.png' % i,
            dpi=400)
plt.close()
exit()

exit(0)



for iloop in range(8):
    iloop = ['ar%i'%iloop, ar[iloop]]
    var, data = iloop
    n_lats = data.shape[1]
    n_lons = data.shape[2]
    year_begin = 1979
    year_end = 2020
    years = np.arange(year_begin, year_end+1)
    # 创建空数组用于存放趋势和显著性检验结果
    trend = np.zeros((n_lats, n_lons))
    p_values = np.ones((n_lats, n_lons))

    mutation_year_loc =2000-1979
    # 针对每个经纬度格点，计算趋势并进行显著性检验
    for i in range(n_lats):
        for j in range(n_lons):
            s_data = data[:, i, j]

            ######
            # if (density[:,i,j] == 0).any():
            #     print('hi')
            s_data = s_data[s_data!=0]
            if len(s_data) != 0:
                slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
                trend[i, j] = slope
                p_values[i, j] = p_value

    nc4.save_nc4(trend, r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute\%s_trend_ar_pattern'%var)
    nc4.save_nc4(p_values, r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute\%s_p_ar_pattern'%var)
exit(0)

"""
===============================================================================================
"""


import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5, 3.9])

plt.subplots_adjust(top=0.97,
                    bottom=0.1,
                    left=0.005,
                    right=0.995,
                    hspace=0.14,
                    wspace=0.055)

ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(0, 90))

trend = nc4.read_nc4(
    r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/trend_ar_pattern')[:120, ]
p = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/p_ar_pattern')[
    :120, ]
plt.title('freq trend')
# trend[np.isnan(trend)] = 0
trend[trend == 0] = np.nan
import matplotlib.colors as colors

print(np.arange(-0.5, 0.51, 0.1))
cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend
                 , vmin=-5, vmax=5, extend='both',
                 cmap=cmaps.cmocean_balance,
                 transform=ccrs.PlateCarree())
plt.colorbar(cb)
ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:120], np.ma.masked_greater(p, 0.1),

            colors='none', levels=[0, 0.1],
            hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
# ax.set_global()
ax.coastlines(linewidth=0.3, zorder=10)
# ax.stock_img()
ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
# plt.show()

# cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#
# cb_ax.set_xlabel('trend')
plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)

plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/1979_2000/ar%i_trend_pattern.png' % i,
            dpi=400)
plt.close()
exit()
for i in range(8):
    fig = plt.figure(figsize=[5,3.9])

    plt.subplots_adjust(top=0.97,
    bottom=0.1,
    left=0.005,
    right=0.995,
    hspace=0.14,
    wspace=0.055)


    ax = fig.add_subplot(1,1,1, projection=ccrs.Orthographic(0, 90))

    trend = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/1979_2000\ar%i_trend_ar_pattern'%i)[:120,]
    p = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/1979_2000\ar%i_p_ar_pattern'%i)[:120,]
    plt.title('freq trend')
    # trend[np.isnan(trend)] = 0
    trend[trend==0] = np.nan
    import matplotlib.colors as colors
    print(np.arange(-0.5,0.51,0.1))
    cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend
                     ,vmin=-5,vmax=5,extend='both',
                     cmap=cmaps.cmocean_balance,
                transform=ccrs.PlateCarree())
    plt.colorbar(cb)
    ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:120], np.ma.masked_greater(p, 0.1),

                colors='none', levels=[0, 0.1],
                hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3,zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
    # plt.show()

    # cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
    # cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    #
    # cb_ax.set_xlabel('trend')
    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    ax.set_title(str(i))
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/1979_2000/ar%i_trend_pattern.png'%i,
                dpi=400)
    plt.close()

exit(0)


"""
===============================================================================================
"""

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east','pacific_east','MS',
             'greenland_west', 'pacific_west', 'GE']

# plot_name = ['(a) GRE (90,4.7%)','(c) BSE (60,4.4%)', '(e) MED (85,3.8%)',
#              '(b) BAF (65,7.1%)', '(d) BSW (100,13.1%)', '(f) ARC (32,1.6%)']
plot_name = ['(a) GRE (28,4.7%)','(c) BSE (19,4.4%)', '(e) MED (26,3.8%)',
             '(b) BAF (20,7.1%)', '(d) BSW (30,13.1%)', '(f) ARC (10,1.6%)']
central_lon = [359,180,75,300,180,75]

var_fmt = ['%0.1f', '%i', '%i', '%0.1f', '%0.1f']
var_title = []
var_ylabel = ['impacted region/the Arctic (units:%)', 'duration (units:6h)']

resolution=0.5



# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])





params = {
         'sic':['freq_trend_ar_pattern'],
            'strd':['ivt_trend_ar_pattern'],
          't2m':['duration_trend_ar_pattern'],
          }
i_columns = 'sic'
trend = scio.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute\result_arType.mat')['t2m_re']
trend_all = trend

for i in range(7):
    trend = trend_all[i]
    fig = plt.figure(figsize=[5, 3.9])

    plt.subplots_adjust(top=0.97,
                        bottom=0.1,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(0, 90))
    import cmaps
    print(np.arange(-0.5,0.51,0.1))
    cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5), abs(trend)*100
                     ,extend='both',levels=np.arange(0,101,10),
                transform=ccrs.PlateCarree())
    plt.colorbar(cb)
    # ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:140], np.ma.masked_greater(p, 0.1),
    #
    #             colors='none', levels=[0, 0.1],
    #             hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    ax.set_extent([0, 359, 10, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3,zorder=10)
    ax.set_title(str(i))
    ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
    # plt.show()
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/AR_typeContributeArtcticTrend_%i.png'%i, dpi=400)
    plt.close()


exit(0)