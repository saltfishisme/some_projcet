import os
# import mat73
import matplotlib.pyplot as plt

import nc4
import scipy.io as scio
import pandas as pd
import numpy as np
import xarray as xr
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'


def main(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_MainData + 'AR_contour_42/%s' % (time_file_new))

        time_all = info_AR_row.time_all.values

        for i_time_row in time_all:
            i_time = pd.to_datetime(i_time_row)
            ar_contour = info_AR_row['contour_all'].sel(time_all=i_time_row).values

            # 0.5*0.5 to 1*1
            ar_contour = ar_contour[::2, ::2][:-1,:]
            for i_time_allyear in range(1979,2019+1):
                """
                ############################
                ############################
                #############################
                # check create_params_for_pw_eul
                #########
                """

                i_time_year = str(i_time_allyear)+i_time.strftime('%m%d')



    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            if (i != '1')| (i != '7'):
                times_file_all = df[i].values
                for time_file in times_file_all:
                    if len(str(time_file)) < 5:
                        continue
                    contour_cal(time_file, var)


"""
=====================================================================================
"""

#%%

import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4

def load_ivt_total_anomaly(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    import scipy.io as scio
    vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                '%s.mat' % s_time.strftime('%m%d'))[
                       s_time.strftime('%m%d')][int(s_time.strftime('%H'))]

    uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                '%s.mat' % s_time.strftime('%m%d'))[
                       s_time.strftime('%m%d')][int(s_time.strftime('%H'))]


    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42/'
file = os.listdir(main_path)

year_begin = 1979
year_end = 2020
avg = np.zeros([year_end-year_begin+1])

avg_add1 = np.zeros([year_end-year_begin+1])
for i_file in file:
    info_AR_row = xr.open_dataset(main_path+i_file)
    time_AR = info_AR_row.time_AR.values
    time_all = info_AR_row.time_all.values
    AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
    time_use = int(len(time_all[AR_loc:]) / 2)

    contour_all = np.zeros([361,720])

    for i_time in time_AR:
        year_loc = int(pd.to_datetime(i_time).strftime('%Y'))-year_begin
        if pd.to_datetime(i_time).strftime('%m') == '12':
            year_loc += 1
            if year_loc == year_end - year_begin+1:
                continue

        # sum for all year
        contour = info_AR_row['contour_all'].sel(time_all=i_time).values

        ivt = load_ivt_total_anomaly(pd.to_datetime(i_time))
        ivt_entry_arctic = np.sum(ivt[46,:])
        ivt_entry_arctic = ivt_entry_arctic*6*60*60

        avg[year_loc] += ivt_entry_arctic
        avg_add1[year_loc] +=1



    # # sum for all year
    # avg += np.nansum(info_AR_row['contour_all'].values, axis=0)
# avg = avg/avg_add1
nc4.save_nc4(avg, '/home/linhaozhong/work/AR_NP85/AR_wv_trans_Arctic')

# nc4.save_nc4(avg_freq, '/home/linhaozhong/work/AR_NP85/freq_ar_yearly')


"""
=====================================================================================
"""


import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4
def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42/'
file = os.listdir(main_path)

year_begin = 1979
year_end = 2020
avg = np.zeros([year_end-year_begin+1, 361,720])
avg_freq = np.zeros([year_end-year_begin+1, 361,720])

for i_file in file:
    info_AR_row = xr.open_dataset(main_path+i_file)
    time_AR = info_AR_row.time_AR.values
    time_all = info_AR_row.time_all.values
    AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
    time_use = int(len(time_all[AR_loc:]) / 2)

    contour_all = np.zeros([361,720])
    year_loc_all = []

    for i_time in time_AR:
        year_loc = int(pd.to_datetime(i_time).strftime('%Y'))-year_begin
        if pd.to_datetime(i_time).strftime('%m') == '12':
            year_loc = year_loc + 1
        if year_loc == year_end - year_begin + 1:
            continue
        year_loc_all.append(year_loc)
        # sum for all year

        contour = info_AR_row['contour_all'].sel(time_all=i_time).values

        ivt = load_ivt_total(pd.to_datetime(i_time))
        ivt[contour<0.5] = 0
        avg[year_loc] += contour

        contour_all += contour
    contour_all[contour_all>0] = 1
    avg_freq[year_loc_all] += contour_all
    # # sum for all year
    # avg += np.nansum(info_AR_row['contour_all'].values, axis=0)
# nc4.save_nc4(avg, '/home/linhaozhong/work/AR_NP85/ivt_ar_yearly')
nc4.save_nc4(avg_freq, '/home/linhaozhong/work/AR_NP85/freq_ar_yearly')