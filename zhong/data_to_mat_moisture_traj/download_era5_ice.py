import cdsapi

c = cdsapi.Client()
dirs = r'/home/linhaozhong/work/Data/ERA5/sea_ice/sea_ice_edge/'
for year in range(1979,2020+1):
    c.retrieve(
        'satellite-sea-ice-edge-type',
        {
            'format': 'zip',
            'version': '2_0',
            'day': [
                '01', '02', '03',
                '04', '05', '06',
                '07', '08', '09',
                '10', '11', '12',
                '13', '14', '15',
                '16', '17', '18',
                '19', '20', '21',
                '22', '23', '24',
                '25', '26', '27',
                '28', '29', '30',
                '31',
            ],
            'month': [
                '01', '02', '12',
            ],
            'year': str(year),
            'cdr_type': 'cdr',
            'region': 'northern_hemisphere',
            'variable': [
                'sea_ice_edge', 'sea_ice_type',
            ],
        },
        dirs+'%s.zip'%year)