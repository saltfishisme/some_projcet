import datetime
import sched
import time
import subprocess
import threading

scripts_to_run = [
    {'script': '/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/download_0_5.py', 'run_time': '9:30:00'},
    {'script': '/home/linhaozhong/work/Data/ERA5/single_level/0.25X0.25/download_0_25.py', 'run_time': '10:00:00'},
    {'script': '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/download_0_5.py', 'run_time': '10:30:00'}]


def run_script(script):
    subprocess.Popen(['python3', script])


class ScriptThread(threading.Thread):
    def __init__(self, script):
        threading.Thread.__init__(self)
        self.script = script

    def run(self):
        run_script(self.script)


scheduler = sched.scheduler(time.time, time.sleep)

while True:
    now = datetime.datetime.now()
    for s in scripts_to_run:
        run_time = datetime.datetime.combine(now.date(),
                                             datetime.datetime.strptime(s['run_time'], '%H:%M:%S').time())
        if run_time < now:
            run_time += datetime.timedelta(days=1)
        run_time_timestamp = time.mktime(run_time.timetuple())
        t = ScriptThread(s['script'])
        scheduler.enterabs(run_time_timestamp, 1, t.start)
    scheduler.run()
    time.sleep(86400 - time.time() % 86400)  # wait until the next day
