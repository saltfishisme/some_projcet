import numpy as np
import pandas as pd
import xarray as xr
import scipy.io as scio

def main_save():
    import numpy as np
    import pandas as pd
    import xarray as xr
    import scipy.io as scio

    # main_addr = r'D:\a_matlab\subregion_centre_asia_era0.25\output_sourcerange_int12hr_sub_centre_asia-25deg/'
    main_addr = r'/home/linhaozhong/work/subregion_centre_asia_era0.25/output_sourcerange_int12hr_sub_centre_asia-25deg/'
    years = ['1979', '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
             '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995', '1996',
             '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004', '2005',
             '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014',
             '2015', '2016', '2017', '2018', '2019']

    lat = scio.loadmat(main_addr + r'lat_H.mat')['lat_H'][:, 0].flatten()
    lon = scio.loadmat(main_addr + r'lon_H.mat')['lon_H'][0, :].flatten()
    print(lat, lon)
    vari_list = ['ACP_all_mon', 'ACPm_loc_mon_eul_sr', 'NCP_all_mon',
                 'NCPm_loc_mon_eul_sr', 'PW_all_mon', 'PWm_loc_mon_eul_sr',
                 'P_all_mon', 'Pm_loc_eul_mon_sr', 'rho_eul_mon_sr',
                 'rr_eul_mon_sr', 'traj_den_eul_mon_sr']

    month_names = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']

    lev = np.arange(1, 31)

    for i, year in enumerate(years):

        traj_den_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        rr_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        rho_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        PWm_loc_mon_eul_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        ACPm_loc_mon_eul_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        NCPm_loc_mon_eul_sr = np.zeros([12, 720, 1440, 30], dtype='float32')
        Pm_loc_eul_mon_sr = np.zeros([12, 720, 1440, 30], dtype='float32')

        P_all_mon = np.zeros(12)
        NCP_all_mon = np.zeros(12)
        ACP_all_mon = np.zeros(12)
        PW_all_mon = np.zeros(12)

        times = pd.date_range('%s0101' % year, periods=12, freq='1m')

        for imonth in range(12):
            data = scio.loadmat(main_addr + 'Yearly_%s_%i' % (month_names[imonth], i + 1))
            traj_den_eul_mon_sr[imonth] = data['s_traj_den_eul_mon_sr']
            rr_eul_mon_sr[imonth] = data['s_rr_eul_mon_sr']
            rho_eul_mon_sr[imonth] = data['s_rho_eul_mon_sr']
            PWm_loc_mon_eul_sr[imonth] = data['s_PWm_loc_mon_eul_sr']
            ACPm_loc_mon_eul_sr[imonth] = data['s_ACPm_loc_mon_eul_sr']
            NCPm_loc_mon_eul_sr[imonth] = data['s_NCPm_loc_mon_eul_sr']
            Pm_loc_eul_mon_sr[imonth] = data['s_Pm_loc_eul_mon_sr']
            P_all_mon[imonth] = data['s_P_all_mon']
            NCP_all_mon[imonth] = data['s_NCP_all_mon']
            ACP_all_mon[imonth] = data['s_ACP_all_mon']
            PW_all_mon[imonth] = data['s_PW_all_mon']

        def get_dataarray(data):

            foo = xr.DataArray(data, coords=[times, lat, lon, lev], dims=["time", "lat", 'lon', 'lev'])
            return foo

        ds = xr.Dataset(
            {
                "ACPm_loc_mon_eul_sr": get_dataarray(ACPm_loc_mon_eul_sr),
                "NCPm_loc_mon_eul_sr": get_dataarray(NCPm_loc_mon_eul_sr),
                "PWm_loc_mon_eul_sr": get_dataarray(PWm_loc_mon_eul_sr),
                "Pm_loc_eul_mon_sr": get_dataarray(Pm_loc_eul_mon_sr),
                "rho_eul_mon_sr": get_dataarray(rho_eul_mon_sr),
                "rr_eul_mon_sr": get_dataarray(rr_eul_mon_sr),
                "traj_den_eul_mon_sr": get_dataarray(traj_den_eul_mon_sr),
                "ACP_all_mon": xr.DataArray(ACP_all_mon, coords=[times], dims=['time']),
                "NCP_all_mon": xr.DataArray(NCP_all_mon, coords=[times], dims=['time']),
                "PW_all_mon": xr.DataArray(PW_all_mon, coords=[times], dims=['time']),
                "P_all_mon": xr.DataArray(P_all_mon, coords=[times], dims=['time']),
            }
        )
        ds.to_netcdf('Moist_%s.nc' % year)
        print('try')


def main_save2():
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd

    main_addr = r'D:\a_matlab\subregion_centre_asia_era0.25\output_sourcerange_int12hr_sub_centre_asia-25deg/'
    main_addr = r'/home/linhaozhong/work/subregion_centre_asia_era0.25/'

    row_data = xr.open_dataset(main_addr+'Moist1979_2019.nc')
    print(row_data)
    lon = row_data.lon.values[:,0].flatten()
    lat = row_data.lat.values[0,:].flatten()

    times = pd.date_range('19790101', '20191231', freq='1m')

    def get_dataarray(data):
        foo = xr.DataArray(np.transpose(data, [0,2,1]), coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            "PW_contrib": get_dataarray(row_data.PW_contrib.values),
            "Prep_contrib": get_dataarray(row_data.Prep_contrib.values),
            "RecyclingRatio": get_dataarray(row_data.RecyclingRatio.values),
            "TrajInden": get_dataarray(row_data.TrajInden.values),


            "Precipitable_water": xr.DataArray(row_data.variables['Precipitable water'].values, coords=[times], dims=['time']),
            "Precipitation": xr.DataArray(row_data.Precipitation.values, coords=[times], dims=['time'])

        }
    )
    print(ds)
    ds.to_netcdf('Moist_1979_2019.nc')


def main_save3():
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd


    data = scio.loadmat('z500_era5.mat')['z500']
    data = np.transpose(data, [2,0,1])
    print(data.shape)
    lon = np.arange(0,360)
    lat = np.arange(90,-91,-1)

    ################################# create date based on years  #################################

    date_year_s = 1979
    date_year_e = 2015

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s0101'%iyear; date_e = '%s0301'%iyear
            sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
            dates.extend(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e+1)
    times = date_ranges(years)

    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            "z500": get_dataarray(data),

        }
    )
    print(ds)
    ds.to_netcdf('z500_era5.nc')

def main_save4(data):
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd


    lon = np.arange(0,360)
    lat = np.arange(90,-91,-1)

    ################################# create date based on years  #################################

    date_year_s = 1979
    date_year_e = 2015

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s0301'%iyear; date_e = '%s0630'%iyear
            sdate = pd.date_range(date_s, date_e, freq='1D')
            if iyear == years[0]:
                dates = sdate
            else:
                dates = dates.append(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e+1)
    times = date_ranges(years)

    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            "wind": get_dataarray(data),

        }
    )
    print(ds)
    ds.to_netcdf('wind_era5.nc')

def main_save5():
    import xarray as xr
    import scipy.io as scio
    import numpy as np
    import pandas as pd


    data = scio.loadmat('wind_era5_12.mat')['wind']

    print(data.shape)
    lon = np.arange(0,360)
    lat = np.arange(90,-91,-1)

    ################################# create date based on years  #################################

    date_year_s = 1979
    date_year_e = 2015

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s0101'%iyear; date_e = '%s0301'%iyear
            sdate = pd.date_range(date_s, date_e, freq='1D')
            if iyear == years[0]:
                dates = sdate
            else:
                dates = dates.append(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e+1)
    times = date_ranges(years)
    print(times.month)
    exit(0)
    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            "wind": get_dataarray(data),

        }
    )
    print(ds)
    ds.to_netcdf('wind_era5_12.nc')
main_save4()