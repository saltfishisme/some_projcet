import xarray as xr
import numpy as np
import os
import pandas as pd
os.environ['ECCODES_DEFINITION_PATH']=r'E:\Users\zhaoh\anaconda3\envs\cm\Library\share\eccodes\definitions'
# a = xr.open_dataset(r'D:\a_matlab\data_to_mat_moisture_traj\data\NorthWater.19790101.nc').variables['p72.162']
# print(a)
# index = 5
# a = xr.open_dataset(r'D:\a_matlab\data_to_mat_moisture_traj\data\%s.19790101.nc'%namelist[index]).variables[varilist[index]]
# print(a)
import os

def main(time1, time2):
    import scipy.io as scio

    for index in range(4):
        if os.path.exists(main_address + r'\fcst_land%s.%s00_%s21' % (address[index], time1, time2)) is not True:
            print(main_address + r'\fcst_land%s.%s00_%s21' % (address[index], time1, time2))

os.listdir()
address = ['125.225_soilw', '125.224_msg','125.223_msc',
           '125.226_smc']

varilist = ['precipc', 'precipl', 'evapor',
             'tcwv','viwve','viwvn']

typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']

main_address = r'E:'


import pandas as pd
a = pd.date_range('1958-01-01', '2013-01-01', freq='1YS')
b = pd.date_range('1958-12-31', '2013-12-31', freq='1Y')

for i in range(len(a)):
    main(a[i].strftime('%Y%m%d'),b[i].strftime('%Y%m%d'))

a = pd.date_range('2014-01-01', '2019-12-01', freq='1MS')
b = pd.date_range('2014-01-31', '2019-12-31', freq='1M')
for i in range(len(a)):
    main(a[i].strftime('%Y%m%d'), b[i].strftime('%Y%m%d'))