import xarray as xr
from metpy import calc
from metpy.units import units
import numpy as np
from nc4 import save_nc4, read_nc4

namelist = ['hgt', 'uwnd', 'shum']
unitslist = ['kg/kg', 'K', 'hPa']
datas = []
for i in range(3):
    data = xr.open_dataset(r'D:\a_matlab\jra55\%s.daily.jra55.1979.nc' % namelist[i]).isel(time=slice(0,5))
    data.to_netcdf('%s.nc'%namelist[i])

# 处理hgt数据为pa
levels = xr.open_dataset('hgt.nc').level
hgt = np.ones([37,145,288])
for i in range(37):
    hgt[i] = hgt[i]*levels.values[i]
datas.append(hgt)

for i in range(1,3):
    # a = xr.open_dataset(r'%s.nc'%namelist[i]).variables[namelist[i]]
    # print(a)
    # datas.append(xr.open_dataset(r'D:\a_matlab\jra55\%s.daily.jra55.1979.nc'%namelist[i]).variables[namelist[i]].values)
    datas.append(xr.open_dataset(r'%s.nc'%namelist[i]).variables[namelist[i]].values[0,:,:,:])

hgt, uwnd, shum = datas
fu = 1 / 0.98 * uwnd * shum

