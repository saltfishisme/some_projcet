# import os.path
#
# from PyPIOMAS.PyPIOMAS import PyPIOMAS
#
# if __name__ == '__main__':
#
#     # Define variables and years to download
#     variables_all = ['aiday', 'hiday', 'uiday']
#     years_all = list(range(1979,2021+1))
#
#     out_dir = r'D:\PIOMS_nc/'
#
#
#     for variables in variables_all:
#         for years in years_all:
#             if not os.path.exists(out_dir + '%s.H%i.nc'%(variables, years)):
#                 print(variables, years)
#                 # Define a downloader
#                 try:
#                     downloader = PyPIOMAS(out_dir, [variables], [years])
#                 except:
#                     continue
#                 # You can view the downloader like this
#                 print(downloader)
#
#                 # Download data
#                 downloader.download()
#
#                 # Unzip data
#                 downloader.unzip()
#
#                 # Convert to NetCDF
#                 downloader.to_netcdf(out_dir + '%s.H%i.nc'%(variables, years))


import pandas as pd
import xarray as xr
import os
import numpy as np

import nc4

path_data = r'/home/linhaozhong/work/Data/PIOMS_nc/'
path_Save = r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/'

files = os.listdir(path_data)
vars = ['hiday', 'aiday']
# 9-yr moving climatology
path_PIOMS = r'/home/linhaozhong/work/Data/PIOMS_nc/'
def get_anomaly_pioms(s_time, aihi='aihi', yr_se=[4,5]):
    from dateutil.relativedelta import relativedelta
    # get climatology from
    climatology = []
    if 'ai' in aihi:
        mean_var = []
        for i_time in pd.date_range(s_time+relativedelta(years=-yr_se[0]),
                                    s_time+relativedelta(years=+yr_se[1]),
                                    freq='1Y'):
            pioms_ai = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % i_time.strftime('%Y'))['aiday'].values
            mean_var.extend(pioms_ai)
        mean_var = np.nanmean(np.array(mean_var, dtype='double'), axis=0)
        climatology.append(mean_var)
    if 'hi' in aihi:
        mean_var = []
        for i_time in pd.date_range(s_time+relativedelta(years=-yr_se[0]),
                                    s_time+relativedelta(years=+yr_se[1]),
                                    freq='1Y'):
            print(i_time)
            pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % i_time.strftime('%Y'))['hiday'].values
            mean_var.extend(pioms_hi)
        mean_var = np.nanmean(np.array(mean_var, dtype='double'), axis=0)
        climatology.append(mean_var)
    return climatology
path_Save = r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly_9yr/'
for i_year in range(2000,2020+1):
    s_time = pd.to_datetime('%i-01-01 05:00'%i_year)
    if i_year<=2017:
        print('ok')
        nc4.save_nc4(get_anomaly_pioms(s_time, 'ai')[0], path_Save + 'ai_' + str(i_year))
        nc4.save_nc4(get_anomaly_pioms(s_time, 'hi')[0], path_Save + 'hi_' + str(i_year))
    else:
        yr_se = [(4-2021+i_year)+4,2021-i_year+1]
        print(yr_se)
        nc4.save_nc4(get_anomaly_pioms(s_time, 'ai',yr_se)[0], path_Save + 'ai_' + str(i_year))
        nc4.save_nc4(get_anomaly_pioms(s_time, 'hi', yr_se)[0], path_Save + 'hi_' + str(i_year))
exit()



# avg-climatology
# for var in vars:
#     mean_var = []
#     for i_file in files:
#         if not var in i_file:
#             continue
#         if int(i_file[7:11])<2000:
#             continue
#         print(path_data+i_file)
#         data = xr.open_dataset(path_data+i_file)
#         mean_var.extend(data[var].values)
#     mean_var = np.nanmean(np.array(mean_var, dtype='double'), axis=0)
#     print(mean_var)
#     nc4.save_nc4(mean_var, path_Save+var+'_2000_2020')



"""fail! I wanna regrid PIOMS to era-grid, but need more work"""

# import pandas as pd
# import xarray as xr
# import os
# import numpy as np
#
# path_data = r'D:\PIOMS_nc/'
# path_Save = r'/home/linhaozhong/work/Data/PIOMS/'
#
# files = os.listdir(path_data)
# vars = ['hiday', 'aiday']
# for var in vars:
#     for i_file in files:
#         if not var in i_file:
#             continue
#         print(path_data+i_file)
#         data = xr.open_dataset(path_data+i_file)
#         lon = np.sort(np.unique(data.x.values))
#         lon_len = len(lon)
#         lat = np.sort(np.unique(data.y.values))
#         lat_len = len(lat)
#         y = data.y.values.reshape([lat_len, lon_len])
#         vars = [i for i in data.data_vars]
#         times = data.time.values
#
#         for i_time in range(len(times)):
#
#             s_data = data.isel(time=i_time)
#             i_time_pd = pd.to_datetime(s_data.time.values)
#
#             save_dir = path_Save+var+'/'+i_time_pd.strftime('%Y')+'/'
#             os.makedirs(save_dir, exist_ok=True)
#
#             s_data.to_netcdf(save_dir+var+'.'+i_time_pd.strftime('%Y%m%d')+'.nc')