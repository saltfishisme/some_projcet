import xarray as xr
import numpy as np
import os

# for yearly save file


def step1_to_daily(data, type='sum'):
    '''
    :param data: xr.variable
    :param type:
    :return:
    '''
    if type == 'sum':
        daily_data = data.sum(dim='time') * 1000
    elif type == 'speed':
        daily_data = data.mean(dim='time')
        daily_data = daily_data * 24 * 60 * 60
    elif type == 'mean':
        daily_data = data.mean(dim='time')
    else:
        print("given type can't be recognized")
        return
    return daily_data.values


def main(time):
    import scipy.io as scio
    result = []
    for index in range(3):
        rdata = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[index], time[:4], namelist[index], time))
        if len(rdata.dims) == 4:
            rdata = rdata.isel(expver=0)
        result.append(step1_to_daily(rdata.variables[varilist[index]], typelist[index]))
    print(result[0])
    data = {'ACPCP': np.array(result[0], dtype='double'),
            'NCPCP': np.array(result[1], dtype='double'),
            'EV': np.array(result[2], dtype='double'),
            'long': lon,
            'lat': lat}
    os.makedirs(save_address + '/precipevap/', exist_ok=True)
    scio.savemat(save_address + '/precipevap/Daily_pe_%s.mat' % time, {'PE': data})

    # def reduce_expver(data, axis='dim'):
    #     result = np.nansum(data, axis=axis)
    #     result[np.all(np.isnan(data), axis=axis)] = np.nan
    #     return result
    # u = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[3], time[:4], namelist[3], time)).variables[varilist[3]].reduce(reduce_expver, 'expver')
    # v = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[4], time[:4], namelist[4], time)).variables[varilist[4]].reduce(reduce_expver, 'expver')
    # w = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[5], time[:4], namelist[5], time)).variables[varilist[5]].reduce(reduce_expver, 'expver')
    #

    u = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[3], time[:4], namelist[3], time)).variables[varilist[3]]
    if len(u.dims) == 4:
        u = u.isel(expver=0)
    v = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[4], time[:4], namelist[4], time)).variables[varilist[4]]
    if len(v.dims) == 4:
        v = v.isel(expver=0)
    w = xr.open_dataset(main_address + r'/%s/%s/%s.%s.nc' % (address[5], time[:4], namelist[5], time)).variables[varilist[5]]
    if len(w.dims) == 4:
        w = w.isel(expver=0)

    uq = u / w
    vq = v / w

    data = {'PWAT': np.array(step1_to_daily(w, 'mean'), dtype='double'),
            'UWVcol': np.array(step1_to_daily(u, 'mean'), dtype='double'),
            'VWVcol': np.array(step1_to_daily(v, 'mean'), dtype='double'),
            'UQ': np.array(step1_to_daily(uq, 'mean'), dtype='double'),
            'VQ': np.array(step1_to_daily(vq, 'mean'), dtype='double'),
            'long': lon,
            'lat': lat}

    os.makedirs(save_address + '/vapordata/', exist_ok=True)
    scio.savemat(save_address + '/vapordata/Daily_vapor_%s.mat' % time, {'QPcol': data})


address = ['Convective_precipitation', 'Large_scale_precipitation',
           'Evaporation',
           'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
           'Total_column_water_vapour']

namelist = ['ConPreci', 'LSPreci',
            'Evapo',
            'EastWaterVapor', 'NorthWater', 'WaterVapor']

### ****************************************** no change the rank!!!!!!!!!!
varilist = ['cp', 'lsp', 'e',
            'p71.162', 'p72.162', 'tcwv']
typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']

main_address = '/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
save_address = '/home/linhaozhong/work/Data/MoistureTrackData/ERA_0.5/'
rdata = xr.open_dataset(r'%s/%s/%s/%s.%s.nc' % (main_address, address[0], '1959', namelist[0], '19590701'))
# rdata = xr.open_dataset(r'D:\a_matlab\era5\%s.%s.nc'%(namelist[3], '19790101')).variables['p71.162']
lon = rdata.longitude.values
lat = rdata.latitude.values
lon, lat = np.meshgrid(lon, lat)

import pandas as pd

times = pd.date_range('2022-10-01', '2022-12-31', freq='1D')
for i in times:
    i = str(i)
    main(i[:4] + i[5:7] + i[8:10])
