import xarray as xr
import numpy as np


# a = xr.open_dataset(r'D:\a_matlab\era5\data\NorthWater.19790101.nc').variables['p72.162']
# print(a)
# index = 5
# a = xr.open_dataset(r'D:\a_matlab\era5\data\%s.19790101.nc'%namelist[index]).variables[varilist[index]]
# print(a)

def step1_to_daily(data, type='sum'):
    '''
    :param data: xr.variable
    :param type:
    :return:
    '''
    print(data.shape)
    if type == 'sum':
        daily_data = np.nansum(data, axis=0) * 1000
    elif type == 'speed':
        daily_data = data.mean(dim='time')
        daily_data = daily_data * 24 * 60 * 60
    elif type == 'mean':
        daily_data = np.nanmean(data, axis=0)
    else:
        print("given type can't be recognized")
        return
    return daily_data

def reduce_expver(data, axis='dim'):
    result = np.nansum(data,axis=axis)
    result[np.all(np.isnan(data), axis=axis)] = np.nan
    return result
def main(time, time_1):
    import scipy.io as scio
    result = []
    for index in range(3):
        rdata_1 = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[index], namelist[index], time_1))
        rdata = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[index], namelist[index], time))

        if len(rdata.dims) == 4:
            rdata = rdata.reduce(reduce_expver, 'expver')
        if len(rdata_1.dims) == 4:
            rdata_1 = rdata_1.reduce(reduce_expver, 'expver')
        print(rdata.isel(time=slice(0,17)).time)
        print(rdata_1.isel(time=slice(17, 24)).time)
        rdata = rdata.isel(time=slice(0,17)).variables[varilist[index]].values
        rdata_1 = rdata_1.isel(time=slice(17, 24)).variables[varilist[index]].values

        result.append(step1_to_daily(np.concatenate((rdata_1, rdata), axis=0), typelist[index]))

    data = {'ACPCP': np.array(result[0], dtype='double'),
            'NCPCP': np.array(result[1], dtype='double'),
            'EV': np.array(result[2], dtype='double'),
            'long': lon,
            'lat': lat}
    scio.savemat(save_address + '/precipevap/Daily_pe_%s.mat' % time, {'PE': data})

    result = []
    for index in range(3,6):
        rdata_1 = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[index], namelist[index], time_1))
        rdata = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[index], namelist[index], time))
        print(rdata.variables[varilist[index]])
        if len(rdata.dims) == 4:
            rdata = rdata.reduce(reduce_expver, 'expver')
        if len(rdata_1.dims) == 4:
            rdata_1 = rdata_1.reduce(reduce_expver, 'expver')

        rdata = rdata.isel(time=slice(0,17)).variables[varilist[index]].values
        rdata_1 = rdata_1.isel(time=slice(17, 24)).variables[varilist[index]].values

        result.append(step1_to_daily(np.concatenate((rdata_1, rdata), axis=0), typelist[index]))


    uq = result[0] / result[2]
    vq = result[1] / result[2]

    data = {'PWAT': np.array(result[2], dtype='double'),
            'UWVcol': np.array(result[0], dtype='double'),
            'VWVcol': np.array(result[1], dtype='double'),
            'UQ': np.array(uq, dtype='double'),
            'VQ': np.array(vq, dtype='double'),
            'long': lon,
            'lat': lat}

    scio.savemat(save_address + '/vapordata/Daily_vapor_%s.mat' % time, {'QPcol': data})


address = ['Convective_precipitation', 'Large_scale_precipitation',
           'Evaporation',
           'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
           'Total_column_water_vapour']

namelist = ['ConPreci', 'LSPreci',
            'Evapo',
            'EastWaterVapor', 'NorthWater', 'WaterVapor']

### ****************************************** no change the rank!!!!!!!!!!
varilist = ['cp', 'lsp', 'e',
            'p71.162', 'p72.162', 'tcwv']
typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']

main_address = '/home/linhaozhong/work/Data/MoistureTrackData/ERA5_0.25/'
save_address = '/home/linhaozhong/work/Data/MoistureTrackData/ERA5_0.25_UTM8/'
rdata = xr.open_dataset(r'%s/%s/%s.%s.nc' % (main_address, address[0], namelist[0], '19790701'))
# rdata = xr.open_dataset(r'D:\a_matlab\era5\%s.%s.nc'%(namelist[3], '19790101')).variables['p71.162']
lon = rdata.longitude.values
lat = rdata.latitude.values
lon, lat = np.meshgrid(lon, lat)

import pandas as pd

# 需要提前一天
times = pd.date_range('2021-06-30', '2021-07-20', freq='1D').strftime('%Y%m%d')
for si, i in enumerate(times[1:]):
    print(i, times[si])
    main(i, times[si])
