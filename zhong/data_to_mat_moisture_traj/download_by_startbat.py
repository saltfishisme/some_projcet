import numpy as np

url = 'start https://pscfiles.apl.washington.edu/zhang/PIOMAS/data/v2.1/other/uiday.H'
all_str = []
for i_year in range(1979,2022+1):
    str_now = url+str(i_year)+'.gz'
    all_str.append(str_now)

url = 'start https://pscfiles.apl.washington.edu/zhang/PIOMAS/data/v2.1/area/aiday.H'
for i_year in range(1979,2022+1):
    str_now = url+str(i_year)+'.gz'
    all_str.append(str_now)

url = 'start https://pscfiles.apl.washington.edu/zhang/PIOMAS/data/v2.1/hiday/hiday.H'
for i_year in range(1979,2022+1):
    str_now = url+str(i_year)+'.gz'
    all_str.append(str_now)

np.savetxt('down.bat', all_str, fmt='%s')