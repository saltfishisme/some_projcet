import xarray as xr
import numpy as np
from xarray.backends import NetCDF4DataStore
from datetime import datetime

import cartopy.crs as ccrs
import cartopy.feature as cfeat
from cartopy.mpl.ticker import LongitudeFormatter,LatitudeFormatter
from cartopy.io.shapereader import Reader, natural_earth
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

##### 注意shp文件的对应！！！！！有些经纬度是-180，180需要转换
area_day1 = [32,40,110,120]


area_day = area_day1
print(xr.open_dataset('Tp_20210720.nc'))
a = xr.open_dataset('Tp_20210720.nc').sum(dim='time').tp

sa = a.where((a.latitude>area_day[0]) & (a.latitude<area_day[1]) & (a.longitude>area_day[2]) & (a.longitude<area_day[3])).values
sa[np.where(sa<20/1000)]=np.nan
mask_data = sa
mask_data[np.where(mask_data==1)] = np.nan

# plt.imshow(mask_data)
# plt.show()
# fig, ax = plt.subplots(1,1, subplot_kw={'projection':ccrs.PlateCarree(central_longitude=150)})
# ax.contourf(a.longitude.values, a.latitude.values, mask_data, transform=ccrs.PlateCarree())
# ax.set_extent([60,150, 0,60,], crs=ccrs.PlateCarree())
# ax.coastlines()
# shplinewidth=1
# from cartopy.feature import ShapelyFeature
# from cartopy.io.shapereader import Reader
#
# shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
#                                ccrs.PlateCarree(),
#                                edgecolor='k', facecolor='none', linewidths=0.55)
# ax.add_feature(shape_feature)
# ax.ZhengZhou( [110, 110, 115.5, 115.5, 110], [32,38, 38, 32, 32], color='black', transform=ccrs.PlateCarree())
#
# plt.show()

def trans_into_nc(nc_data_addr, nc_vari_name, mask_data, savename=''):
    nc_data = xr.open_dataset(nc_data_addr).isel(time=0, expver=0)
    print(nc_data)
    data = nc_data[nc_vari_name].values
    data[~np.isnan(mask_data)] = 1
    data[np.isnan(mask_data)] = 0

    ds = xr.Dataset(
        {nc_vari_name: (("latitude", "longitude"), data)},
        coords={
            "longitude": nc_data.longitude.values,
            "latitude": nc_data.latitude.values,
        },
    )

    ds.to_netcdf(savename+ r'mask_shape.nc')

trans_into_nc('TotPreci.20210101.nc', 'tp', mask_data)
a = xr.open_dataset('mask_shape.nc').tp.values
print(np.argwhere(a==1))
plt.imshow(a)
plt.show()