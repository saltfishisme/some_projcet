import xarray as xr
import numpy as np
from nc4 import read_nc4,save_nc4
import scipy.io as scio
# from global_land_mask import globe
# lat = np.arange(90, -90.01, -0.5)
# lon = np.arange(0, 360, 0.5)
# # lat = np.arange(89.75, -89.8, -0.5)
# # lon = np.arange(0.25, 360, 0.5)
# from function_shared_Main import roll_longitude_from_359to_negative_180
# lon = roll_longitude_from_359to_negative_180(np.arange(0, 360, 0.5))
# lon, lat = np.meshgrid(lon,lat)
# globe_land_mask = globe.is_land(lat, lon)
# # plt.imshow(globe_land_mask)
# # plt.show()
# lat = np.arange(90, -90.01, -0.5)
# lon = np.arange(0, 360, 0.5)
# # lon, lat = np.meshgrid(lon,lat)
# # plt.imshow(globe_land_mask)
# # plt.show()
def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    # def get_dataarray(data):
    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
    #     return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon


    for iI_vari, ivari in enumerate(var_name):

        ds[ivari] = (("lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + '%s.nc' % file_name)
# save_standard_nc([globe_land_mask],['mask'], 1, lon=lon, lat=lat, file_name='D:/EAR5_landseaMask_0.5')
# #
# # # scio.savemat(r'D:/ERA5_landsea_mask_0.25.mat', {'lat':lat, 'lon':lon, 'mask':globe_land_mask})
# # exit()
# import numpy as np


'''create basis mask (all are 1)'''
# a = xr.open_dataset(r'D:\basis\data\rea2\vwnd_rea2.nc4')
# lat = a.lat.values
# lon = a.lon.values
# lat, lon = np.meshgrid(lon, lat)
# mask_basis = np.ones(lat.shape)
# save_nc4(mask_basis, 'mask_basis')
#
# '''mask basis_mask'''
""" create basis mask based on shp file"""
import xarray as xr
import os
os.environ['GDAL_DATA'] = r'E:\Users\zhaoh\anaconda3\envs\maskl\Library\share\gdal/'

lat = np.arange(14.75, 55.25+0.0001, 0.25)
lon = np.arange(69.75, 140.25+0.0001, 0.25)

path_shp = r"G:\OneDrive\basis\data\shp\shengjie\taiwan.shp"
path_save = r"D:\taiwan_mask_0.25.nc"
central_point = ''

ds = xr.Dataset()
# lat = np.arange(89.75, -89.8, -0.5)
# lon = np.arange(0.25, 360, 0.5)
lat = np.arange(14.75, 55.25+0.0001, 0.25)
lon = np.arange(69.75, 140.25+0.0001, 0.25)
print(lat.shape, lon.shape)
ds.coords["latitude"] = lat
ds.coords["longitude"] = lon

ds['tp'] = (("latitude", "longitude"), np.ones([lat.shape[0], lon.shape[0]]))

mask_basis = ds['tp']
mask_array= ds
import salem
import geopandas
# from cartopy.feature import ShapelyFeature
# from cartopy.io.shapereader import Reader
# import cartopy.crs as ccrs


# shape_feature1 = geopandas.read_file(r"D:\OneDrive\basis\data\shp\no_taiwan_hainan\bou2_4l.shp")
shape_feature2 = geopandas.read_file(path_shp)
#
# shape_feature3 = geopandas.read_file(r'D:\basis\data\shp\jiuduanxian\nanhai.shp')
t1 = mask_basis.salem.roi(shape=shape_feature2)
# # mask_basis = mask_basis.salem.roi(shape=shape_feature2)
# t2 = mask_basis.salem.roi(shape=shape_feature1)

a = t1.values
mask_array['tp'].values[~np.isnan(a)] = 1
mask_array['tp'].values[np.isnan(a)] = 0

lat = mask_array.latitude.values
lon = mask_array.longitude.values
lon, lat = np.meshgrid(lon,lat)
mask_array.to_netcdf(path_save)
# scio.savemat(path_save, {'lat%s'%central_point:lat, 'lon%s'%central_point:lon, 'mask':mask_array['tp'].values})
# save_standard_nc([mask_array['tp'].values],['mask'], 1, lon=lon, lat=lat, file_name='D:/EAR5_landseaMask_0.25')
exit(0)



'''看结果'''
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
import cartopy.crs as ccrs

import scipy.io as scio

# data = scio.loadmat(r'D:\a_matlab\china_ERA5_0.25\China_seedinfo.mat')
# sdata = xr.open_dataset('bb_news.nc')
# sdata['p72.162'].values[1:,:] = data['seedmat']
# sdata.to_netcdf(r'D:\a_matlab\china_ERA5_0.25\bb.nc')




# shplinewidth = 0.5
# ssdata = xr.open_dataset('bb_new.nc')
# lat = ssdata.latitude.values
# lon = ssdata.longitude.values
# lon, lat = np.meshgrid(lon,lat)
# scio.savemat(r'D:\a_matlab\china_ERA5_0.25\ERA25degGrid.mat', {'lat':lat, 'lon':lon})


# data = scio.loadmat(path_save)
shplinewidth = 0.1

import matplotlib.pyplot as plt
fig, ax= plt.subplots(subplot_kw={'projection':ccrs.PlateCarree(central_longitude=120)})
# print(data['seedmat'].shape)
data = xr.open_dataset(r"D:\China-snow_20221125_track.nc")
print(data['longitude'])
ax.pcolormesh(data['longitude'], data['latitude'], data['var'].values, transform=ccrs.PlateCarree())

# ax.pcolormesh(data['lon%s'%central_point][0,:], data['lat%s'%central_point][:,0].flatten(), data['mask'], transform=ccrs.PlateCarree())
# shape_feature = ShapelyFeature(Reader(path_shp).geometries(),
#                                ccrs.PlateCarree(),
#                                edgecolor='red', facecolor='none', linewidths=shplinewidth)
# ax.add_feature(shape_feature)
shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
                               ccrs.PlateCarree(),
                               edgecolor='k', facecolor='none', linewidths=0.4)
ax.add_feature(shape_feature)
shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
                               ccrs.PlateCarree(),
                               edgecolor='k', facecolor='none', linewidths=0.4)
ax.add_feature(shape_feature)
# ax.format(latlim=[2, 60], lonlim=[60, 140])
import matplotlib.pyplot as plt
plt.show()