import xarray as xr
import numpy as np

def main(time):

    # for index in range(6):
    #     try:
    #         rdata = xr.open_dataset(main_address+r'/%s/%s.%s.nc'%(address[index], namelist[index], time))
    #     except:
    #         result_wrong.append('%s.%s.nc'%(namelist[index], time))
    print(namelist[index])
    try:
        rdata = xr.open_dataset(main_address+r'/%s/%s.%s.nc'%(address[index], namelist[index], time))
    except:
        result_wrong.append('%s.%s.nc'%(namelist[index], time))



address = ['Convective_precipitation', 'Large_scale_precipitation',
           'Evaporation',
           'Vertical_integral_of_eastward_water_vapour_flux','Vertical_integral_of_northward_water_vapour_flux','Total_column_water_vapour']

namelist = ['ConPreci', 'LSPreci',
             'Evapo',
            'EastWaterVapor','NorthWater', 'WaterVapor']
varilist = ['cp', 'lsp', 'e',
            'p71.162', 'p72.162','tcwv']
typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']

main_address = '/work/Data/ERA5/MoistureModel_0.25_part'



import pandas as pd
times = pd.date_range('1979-01-01', '2019-12-31', freq='1D')

for index in range(6):
    result_wrong = []
    for i in times:
        i = str(i)
        main(i[:4]+i[5:7]+i[8:10])
    np.savetxt('error_list_test_%s.txt'%namelist[index], np.array(result_wrong),fmt='%s')