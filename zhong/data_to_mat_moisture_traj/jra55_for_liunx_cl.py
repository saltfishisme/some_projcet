import xarray as xr
from metpy import calc
from metpy.units import units
import numpy as np
from nc4 import save_nc4, read_nc4

def to_pw(air, shum, levels, savename):
    hgt = np.ones(shum.shape)
    for i in range(shum.shape[0]):
        hgt[i] = hgt[i] * levels[i]

    def calculate_pw(hgt, air, shum):
        # print(time.process_time())
        # hgt, air, shum = np.split(data, 3)
        hgt = hgt * units.hPa
        air = air * units.K
        shum = shum*units('kg/kg')

        dewpoint = calc.dewpoint_from_specific_humidity(shum, air, hgt)
        pw = calc.precipitable_water(dewpoint, hgt)
        # print(time.process_time())
        return np.array(pw)


    nlat, nlon = hgt.shape[1:3]
    result = np.empty([nlat, nlon])
    # data = np.concatenate([hgt, air, shum])
    # data = data.reshape(data.shape[0], data.shape[1]*data.shape[2])
    #
    # a = np.apply_along_axis(calculate_pw, 0, data)
    # print(a)

    for ilat in range(nlat):
        for ilon in range(nlon):
            result[ilat, ilon] = calculate_pw(hgt[:, ilat, ilon],
                                               air[:, ilat, ilon],
                                               shum[:, ilat, ilon])

    save_nc4(result, 'pw%s'%savename)

def to_uwnd_vwnd(shum, uwnd, vwnd, levels, savaname):
    from metpy.calc import get_layer
    import numpy as np
    def precipitable_water(pressure, dewpt, bottom=None, top=None):

        # Sort pressure and dewpoint to be in decreasing pressure order (increasing height)
        sort_inds = np.argsort(pressure)[::-1]
        pressure = pressure[sort_inds]
        dewpt = dewpt[sort_inds]

        if top is None:
            top = np.nanmin(pressure.magnitude) * pressure.units

        if bottom is None:
            bottom = np.nanmax(pressure.magnitude) * pressure.units

        pres_layer, w = get_layer(pressure, dewpt, bottom=bottom, depth=bottom - top)

        # Since pressure is in decreasing order, pw will be the opposite sign of that expected.
        pw = np.trapz(w.magnitude, pres_layer.magnitude) * (w.units * pres_layer.units)
        return -pw

    hgt = np.ones(shum.shape)
    for i in range(shum.shape[0]):
        hgt[i] = hgt[i] * levels[i]

    from metpy.units import units
    qu = uwnd * shum * 9.8
    qv = vwnd * shum * 9.8
    qu = qu * units('kg/(s*hPa*cm)')
    qv = qv * units('kg/(s*hPa*cm)')
    hgt = hgt * units.hPa
    nlat, nlon = hgt.shape[1:3]
    uresult = np.empty([nlat, nlon])
    vresult = np.empty([nlat, nlon])
    import numpy as np
    for ilat in range(nlat):
        for ilon in range(nlon):
            uresult[ilat, ilon] = np.array(precipitable_water(hgt[:, ilat, ilon],
                                                             qu[:, ilat, ilon]))
            vresult[ilat, ilon] = np.array(precipitable_water(hgt[:, ilat, ilon],
                                                              qv[:, ilat, ilon]))
    from nc4 import save_nc4
    save_nc4(uresult, 'qu%s'%savaname)
    save_nc4(vresult, 'qv%s' % savaname)

for year in range(1990,1990):
    shum = xr.open_dataset(r'/home/linhaozhong/work/Data/JRA55/JRA55-Daily/pressure/shum.daily.jra55.%s.nc' %year)
    uwnd = xr.open_dataset(r'/home/linhaozhong/work/Data/JRA55/JRA55-Daily/pressure/uwnd.daily.jra55.%s.nc' %year).sel(level=shum.level)
    vwnd = xr.open_dataset(r'/home/linhaozhong/work/Data/JRA55/JRA55-Daily/pressure/vwnd.daily.jra55.%s.nc' %year).sel(level=shum.level)
    hgt = xr.open_dataset(r'/home/linhaozhong/work/Data/JRA55/JRA55-Daily/pressure/hgt.daily.jra55.%s.nc' %year).sel(level=shum.level)
    air = xr.open_dataset(r'/home/linhaozhong/work/Data/JRA55/JRA55-Daily/pressure/air.daily.jra55.%s.nc' %year).sel(level=shum.level)

    for si in shum.time.values:
        i = str(si)
        to_uwnd_vwnd(shum.sel(time=si).variables['shum'].values,
                     uwnd.sel(time=si).variables['uwnd'].values,
                     vwnd.sel(time=si).variables['vwnd'].values,
                     hgt.level.values, i[:4]+i[5:7]+i[8:10])
        to_pw(air.sel(time=si).variables['air'].values,
                     shum.sel(time=si).variables['shum'].values,
                     hgt.level.values, i[:4]+i[5:7]+i[8:10])



