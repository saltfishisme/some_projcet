import xarray as xr
import numpy as np
import pandas as pd

vari_dirName_all = ['Vwnd']

vari_SaveName_all = ['Vwnd']

### ****************************************** dont change the rank!!!!!!!!!!
varilist = ['v']
times = pd.date_range('1979-01-01', '1994-12-31', freq='1M').strftime('%Y%m')
dates_for_day = pd.date_range('1979-01-01', '1994-12-31', freq='1D')
main_address = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'

for var_i in range(len(varilist)):
    for i in times:
        s_data = xr.open_dataset(main_address + '%s/' % vari_dirName_all[var_i] + '%s/' % i[:4] + '%s.%s.nc' % (
            vari_SaveName_all[var_i], i))
        time_days = dates_for_day[(dates_for_day.year == int(i[0:4])) & (dates_for_day.month == int(i[4:6]))]
        for i_day in time_days:
            s_data.sel(time=i_day.strftime('%Y-%m-%d')).to_netcdf(
                main_address + '%s/' % vari_dirName_all[var_i] + '%s/' % i[:4] + '%s.%s.nc' % (
                    vari_SaveName_all[var_i], i_day.strftime('%Y%m%d')))