import schedule
from queue import Queue
from threading import Thread
import time
import os
import pandas as pd
import xarray as xr
import logging.handlers
import cdsapi
from requests.exceptions import ChunkedEncodingError

dataSource_type = 'reanalysis-era5-single-levels'

'''reanalysis-era5-pressure-levels-monthly-means
reanalysis-era5-single-levels-monthly-means-preliminary-back-extension
reanalysis-era5-pressure-levels
reanalysis-era5-pressure-levels-preliminary-back-extension
reanalysis-era5-single-levels
reanalysis-era5-single-levels-preliminary-back-extension
... and so on'''

thread = 4
one_File_Path = False  # False or 'D:/era5/hgt2021/', download all files into this path
separate_files_by_year = True  # separated files based on year
combine_daily_files_into_monthly = False

#########  variable  control ###########
var_all = ['convective_precipitation', 'large_scale_precipitation',
            'evaporation',
            'vertical_integral_of_eastward_water_vapour_flux',
            'vertical_integral_of_northward_water_vapour_flux', 'total_column_water_vapour', 'total_precipitation']


use_default_settings = True
'''
Variables such as 'specific_humidity' will be saved into this Path: 
 /vari_dirName/vari_SaveName.19790101.nc
'''
if use_default_settings:
    var_dirName_all = var_all
    var_SaveName_all = var_all
else:
    ###############  if easy_set is False, please change.

    var_dirName_all = ['total_precipitation']

    var_SaveName_all = ['TP']

var_dirName_all=['Convective_precipitation', 'Large_scale_precipitation',
              'Evaporation',
              'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
              'Total_column_water_vapour', 'Total_precipitation']
var_SaveName_all=['ConPreci', 'LSPreci',
               'Evapo',
               'EastWaterVapor', 'NorthWater', 'WaterVapor', 'Tp']

#########  Time and resolution control ###########
date_begin = '20230101'

# a = pd.to_datetime(datetime.datetime.now())
# date_end = (a-np.timedelta64(20, 'D')).strftime('%Y%m%d')

months_to_download = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

grid_resolution = [0.5, 0.5]
#########  Other params control ###########

pressure_levels_to_download = [
    '1', '2', '3',
    '5', '7', '10',
    '20', '30', '50',
    '70', '100', '125',
    '150', '175', '200',
    '225', '250', '300',
    '350', '400', '450',
    '500', '550', '600',
    '650', '700', '750',
    '775', '800', '825',
    '850', '875', '900',
    '925', '950', '975',
    '1000',
]  # only works when you download pressure_level data.

import random
import string

digits = string.digits
rand_str = ''.join(random.choice(digits) for i in range(3))

index_arg = {
    'product_type': 'reanalysis',
    'format': 'netcdf',  # Supported format: grib and netcdf. Default: grib
    'grid': grid_resolution,
    'nocache': rand_str,
}


################################ Don't change #####################################
def download():
    # get current date and download 2 months ago.
    from datetime import date, timedelta
    today = date.today()
    date_end = today - timedelta(days=60)
    date_end = date_end.strftime('%Y%m%d')

    # get_download_dateList
    freq = '1D'
    strftime = '%Y%m%d'
    if 'month' in dataSource_type:
        freq = '1M'
        strftime = '%Y%m'
    if combine_daily_files_into_monthly:
        dates = pd.date_range(date_begin, date_end, freq='1M')
        dates_for_day = pd.date_range(date_begin, date_end, freq=freq)
        links = list(dates[dates.month.isin(months_to_download)].strftime('%Y%m'))
    else:
        dates = pd.date_range(date_begin, date_end, freq=freq)
        links = list(dates[dates.month.isin(months_to_download)].strftime(strftime))

    def download_every_var(vari, vari_save_name, vari_dir):
        def monthly_to_daily(path):
            s_data = xr.open_dataset(path)
            time_days = s_data.time.values
            for i_day in time_days:
                s_time = pd.to_datetime(i_day)

                # make sure path is existed
                if os.path.exists('%s%s.nc' % (
                        path[:-3], s_time.strftime('%d'))):
                    continue

                s_data.sel(time=s_time.strftime('%Y-%m-%d')).to_netcdf(
                    '%s%s.nc' % (
                        path[:-3], s_time.strftime('%d')))

        def download_era5(date):
            # create filename
            if one_File_Path is False:
                makedir = data_Path + '/' + vari_dir + '/'
                if separate_files_by_year:
                    makedir += date[:4] + '/'
                os.makedirs(makedir, exist_ok=True)
            else:
                makedir = one_File_Path
            filename = makedir + vari_save_name + '.' + date + ".nc"

            # if monthly, check if the file is existed.
            if combine_daily_files_into_monthly:
                days = list(dates_for_day[(dates_for_day.year == int(date[0:4])) & (
                        dates_for_day.month == int(date[4:6]))].strftime('%Y%m%d'))
                lack = 0
                for i_day in days:
                    if os.path.isfile(makedir + vari_save_name + '.' + i_day + ".nc"):
                        # 1, if the file is existed
                        try:
                            # 2, if the file is current
                            xr.open_dataset(makedir + vari_save_name + '.' + i_day + ".nc")
                        except:
                            logger.info('error in %s' % (makedir + vari_save_name + '.' + i_day + ".nc"))
                        lack += 0
                    else:
                        lack += 1
                if lack == 0:
                    print("ok", filename)
                    return

            """==============================================================="""
            # 1, check if the daily file is exist,
            # 2, or the monthly file is exist and need to separate.
            if os.path.isfile(filename):
                try:
                    # 1, if the file is current
                    xr.open_dataset(filename)
                    if combine_daily_files_into_monthly:
                        # 2, the monthly file is exist and need to separate.
                        monthly_to_daily(filename)
                        os.remove(filename)
                    print("ok", filename)
                except:
                    logger.info('error in %s' % (filename))
                return
            """==============================================================="""

            c = cdsapi.Client()

            # create index_arg for cdsapi
            """==============================================================="""
            s_index_arg = {**{
                'variable': vari,
                'year': date[0:4],
                'month': date[4:6],
                'time': ['00:00']
            }, **index_arg}

            if 'pressure' in dataSource_type:
                s_index_arg['pressure_level'] = pressure_levels_to_download
            if 'month' not in dataSource_type:
                if combine_daily_files_into_monthly:
                    s_index_arg['day'] = list(dates_for_day[(dates_for_day.year == int(date[0:4])) & (
                            dates_for_day.month == int(date[4:6]))].strftime('%d'))
                else:
                    s_index_arg['day'] = date[-2:]

                s_index_arg['time'] = [
                    '00:00', '01:00', '02:00',
                    '03:00', '04:00', '05:00',
                    '06:00', '07:00', '08:00',
                    '09:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00',
                    '18:00', '19:00', '20:00',
                    '21:00', '22:00', '23:00'
                ]
            """==============================================================="""
            print('downloading %s'%filename)

            while True:
                try:
                    c.retrieve(
                        dataSource_type, s_index_arg, filename)
                    break
                except ChunkedEncodingError:
                    print('The URL appears to be blocked. Retry in 5 minutes...')
                    time.sleep(5*60)

            # separate monthly file into daily file
            if combine_daily_files_into_monthly:
                monthly_to_daily(filename)
                os.remove(filename)

        class DownloadWorker(Thread):
            def __init__(self, queue):
                Thread.__init__(self)
                self.queue = queue

            def run(self):
                while True:
                    riqi = self.queue.get()
                    download_era5(riqi)
                    self.queue.task_done()

        queue = Queue()
        for x in range(thread):
            worker = DownloadWorker(queue)
            worker.daemon = True
            worker.start()
        for date in links:
            queue.put((date))
        queue.join()

    for i in range(len(var_all)):
        download_every_var(var_all[i], var_SaveName_all[i], var_dirName_all[i])


# ERROR LOGGER
data_Path = os.path.dirname(os.path.abspath(__file__))
logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)
f_handler = logging.FileHandler('%s/OF_log_auto.log' % (data_Path), mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(levelname)s:  %(message)s"))
logger.addHandler(f_handler)

download()

schedule.every().day.at("11:00").do(download)
while True:
    schedule.run_pending()
    time.sleep(1)

