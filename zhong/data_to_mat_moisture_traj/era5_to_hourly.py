import xarray as xr
import numpy as np


# a = xr.open_dataset(r'D:\a_matlab\era5\data\NorthWater.19790101.nc').variables['p72.162']
# print(a)
# index = 5
# a = xr.open_dataset(r'D:\a_matlab\era5\data\%s.19790101.nc'%namelist[index]).variables[varilist[index]]
# print(a)

def step1_to_daily(data, type='sum'):
    '''
    :param data: xr.variable
    :param type:
    :return:
    '''
    if type == 'sum':
        daily_data = data * 1000
    elif type == 'mean':
        daily_data = data
    else:
        print("given type can't be recognized")
        return
    return daily_data.values


def main(time):
    import scipy.io as scio
    result = []
    

    acpcp = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[0], namelist[0], time))
    if len(acpcp.dims) == 4:
        acpcp = acpcp.isel(expver=0)

    acpcp = acpcp[varilist[0]].values*1000

    ncpcp = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[1], namelist[1], time))
    if len(ncpcp.dims) == 4:
        ncpcp = ncpcp.isel(expver=0)
    ncpcp = ncpcp[varilist[1]].values*1000

    ev = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[2], namelist[2], time))
    if len(ev.dims) == 4:
        ev = ev.isel(expver=0)
    ev = ev[varilist[2]].values*1000

    for ihour in range(24):
        data = {'ACPCP': np.array(acpcp[ihour], dtype='double'),
                'NCPCP': np.array(ncpcp[ihour], dtype='double'),
                'EV': np.array(ev[ihour], dtype='double'),
                'long': lon,
                'lat': lat}
        shour = str(ihour)
        if len(shour)<2:
            shour = '0'+shour
        scio.savemat(save_address + '/precipevap/Daily_pe_%s%s.mat' % (time, shour), {'PE': data})
    
    

    u = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[3], namelist[3], time)).variables[varilist[3]]
    if len(u.dims) == 4:
        u = u.isel(expver=0)
    u = u.values
    v = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[4], namelist[4], time)).variables[varilist[4]]
    if len(v.dims) == 4:
        v = v.isel(expver=0)
    v = v.values
    w = xr.open_dataset(main_address + r'/%s/%s.%s.nc' % (address[5], namelist[5], time)).variables[varilist[5]]
    print(time, w)
    if len(w.dims) == 4:
        w = w.isel(expver=0)
    w = w.values

    # print(namelist[3], namelist[4], namelist[5], time)
    # v = xr.open_dataset(r'D:\a_matlab\era5\%s.%s.nc' % (namelist[4], time)).variables[varilist[4]]
    # u = xr.open_dataset(r'D:\a_matlab\era5\%s.%s.nc' % (namelist[3], time)).variables[varilist[3]]
    # w = xr.open_dataset(r'D:\a_matlab\era5\%s.%s.nc' % (namelist[5], time)).variables[varilist[5]]
    uq = u / w
    vq = v / w
    for ihour in range(24):
        data = {'PWAT': np.array(w[ihour], dtype='double'),
                'UWVcol': np.array(u[ihour], dtype='double'),
                'VWVcol': np.array(v[ihour], dtype='double'),
                'UQ': np.array(uq[ihour], dtype='double'),
                'VQ': np.array(vq[ihour], dtype='double'),
                'long': lon,
                'lat': lat}

        shour = str(ihour)
        if len(shour)<2:
            shour = '0'+shour
        scio.savemat(save_address + '/vapordata/Daily_vapor_%s%s.mat' % (time, shour), {'QPcol': data})


address = ['Convective_precipitation', 'Large_scale_precipitation',
           'Evaporation',
           'Vertical_integral_of_eastward_water_vapour_flux', 'Vertical_integral_of_northward_water_vapour_flux',
           'Total_column_water_vapour']

namelist = ['ConPreci', 'LSPreci',
            'Evapo',
            'EastWaterVapor', 'NorthWater', 'WaterVapor']

### ****************************************** no change the rank!!!!!!!!!!
varilist = ['cp', 'lsp', 'e',
            'p71.162', 'p72.162', 'tcwv']
typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']

main_address = '/home/linhaozhong/work/Data/MoistureTrackData/ERA5_0.25/'
save_address = '/home/linhaozhong/work/Data/MoistureTrackData/ERA_0.25_daily/'

rdata = xr.open_dataset(r'%s/%s/%s.%s.nc' % (main_address, address[0], namelist[0], '19790701'))
# rdata = xr.open_dataset(r'D:\a_matlab\era5\%s.%s.nc'%(namelist[3], '19790101')).variables['p71.162']
lon = rdata.longitude.values
lat = rdata.latitude.values
lon, lat = np.meshgrid(lon, lat)

import pandas as pd

times = pd.date_range('2021-07-1', '2021-07-20', freq='1D')
for i in times:
    i = str(i)
    main(i[:4] + i[5:7] + i[8:10])
