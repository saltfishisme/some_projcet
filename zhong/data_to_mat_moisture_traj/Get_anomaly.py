import os

import matplotlib.pyplot as plt
import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
from configparser import ConfigParser
import logging.handlers
import struct
import sys
import os
import traceback



logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)

f_handler = logging.FileHandler(os.path.dirname(os.path.abspath(__file__)) + '/OF_log.log', mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))

logger.addHandler(f_handler)

''' get hourly anomaly of circulation factor '''


##########################

def anomaly_for_separate_file(var_dir, var_save, var_ShortName):
    path_SaveData = path_MainData + '%s/anomaly/' % var_dir
    os.makedirs(path_SaveData, exist_ok=True)

    dates = pd.date_range('2000-01-01', '2000-12-31', freq='1D')
    dates = dates[dates.month.isin(months_to_download)]
    dates = dates.strftime('%m%d')
    # containing 2/29

    def get_data(var_dir, i_Year, var_save, i_MonDay):
        s_data = xr.open_dataset(path_MainData + '%s/%i/%s.%i%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
        if levels:
            if level_isobaric != -999:
                s_data = s_data.sel(level=level_isobaric)

        s_data = s_data[var_ShortName].values

        if s_data.shape[0] == 24:
            return s_data
        else:
            print(s_data.shape)
            logger.warning('lack of %s%s' % (i_Year, i_MonDay))
            return 'wrong'

    for iI_MonDay, i_MonDay in enumerate(dates):

        if os.path.exists(path_SaveData + '%s.mat' % i_MonDay):
            print('exist '+path_SaveData + '%s.mat' % i_MonDay)
            continue

        data_in_MonDay = []
        for i_Year in years:

            # work for 0229
            if i_MonDay != '0229':
                print(path_MainData + '%s/%i/%s.%i%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
                s_data = get_data(var_dir, i_Year, var_save, i_MonDay)
                if isinstance(s_data, str) != 1:
                    data_in_MonDay.append(s_data)
            else:
                try:
                    print(path_MainData + '%s/%i/%s.%i%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
                    s_data = get_data(var_dir, i_Year, var_save, i_MonDay)
                    if isinstance(s_data, str) != 1:
                        data_in_MonDay.append(s_data)

                except Exception as e:
                    logger.warning('lack of %s%s' % (i_Year, i_MonDay))
                    logger.error(traceback.format_exc())

        data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
        data_in_MonDay = np.array(data_in_MonDay, dtype='double')

        if level_isobaric != -999:
            data_in_MonDay = np.transpose(data_in_MonDay, [1, 0, 2, 3])
            print(data_in_MonDay.shape)
        path_SaveData = path_MainData + '%s/anomaly/' % var_dir
        os.makedirs(path_SaveData, exist_ok=True)
        scipy.io.savemat(path_SaveData + '%s.mat' % i_MonDay, {'%s' % i_MonDay: data_in_MonDay})



def monthly_mean(var_dir, var_save, var_ShortName, time):
    path_SaveData = path_MainData + '%s/monthly_mean/' % var_dir
    os.makedirs(path_SaveData, exist_ok=True)

    date_montly_now = pd.date_range(time.strftime('%Y-%m-') + '01', freq='1D', periods=31)
    dates = date_montly_now[date_montly_now.time.month==time.strftime('%m')].strftime('%Y%m%d')
    year = dates[:4]

    # containing 2/29

    def get_data(var_dir, i_Year, var_save, i_MonDay):
        s_data = xr.open_dataset(path_MainData + '%s/%i/%s.%i%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
        if levels:
            if level_isobaric != -999:
                s_data = s_data.sel(level=level_isobaric)

        s_data = s_data[var_ShortName].values

        if s_data.shape[0] == 24:
            return s_data
        else:
            print(s_data.shape)
            logger.warning('lack of %s%s' % (i_Year, i_MonDay))
            return 'wrong'

    data_in_MonDay = []
    for iI_MonDay, i_MonDay in enumerate(dates):



        # work for 0229
        if i_MonDay != '0229':
            print(path_MainData + '%s/%i/%s.%i%s.nc' % (var_dir, year, var_save, year, i_MonDay))
            s_data = get_data(var_dir, year, var_save, i_MonDay)
            if isinstance(s_data, str) != 1:
                data_in_MonDay.append(s_data)
        else:
            try:
                print(path_MainData + '%s/%i/%s.%i%s.nc' % (var_dir, year, var_save, year, i_MonDay))
                s_data = get_data(var_dir, year, var_save, i_MonDay)
                if isinstance(s_data, str) != 1:
                    data_in_MonDay.append(s_data)

            except Exception as e:
                logger.warning('lack of %s%s' % (year, i_MonDay))
                logger.error(traceback.format_exc())

    data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
    data_in_MonDay = np.array(data_in_MonDay, dtype='double')
    if level_isobaric != -999:
        data_in_MonDay = np.transpose(data_in_MonDay, [1, 0, 2, 3])
        print(data_in_MonDay.shape)
    path_SaveData = path_MainData + '%s/monthly_mean/' % var_dir
    os.makedirs(path_SaveData, exist_ok=True)
    scipy.io.savemat(path_SaveData + '%s.mat' % time.strftime('%Y%m'), {'%s' % i_MonDay: data_in_MonDay})


path_MainData = '/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
levels = True
level_isobaric = -999  # -999 for single level


vari_dirName_all = ['surface_solar_radiation_downwards']
vari_SaveName_all = ['surface_solar_radiation_downwards']
var_ShortName = ['ssrd']

years = np.arange(1979, 2020 + 1)

months_to_download = [1,2,3,11,12]

for iI_var in range(len(var_ShortName)):
    anomaly_for_separate_file(vari_dirName_all[iI_var], vari_SaveName_all[iI_var], var_ShortName[iI_var])

    # try:
    #     anomaly_for_separate_file(vari_dirName_all[iI_var], vari_SaveName_all[iI_var], var_ShortName[iI_var])
    # except Exception as e:
    #     logger.warning('something wrong with main function %s' % vari_SaveName_all[iI_var])
    #     logger.error(traceback.format_exc())
exit()

path_MainData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
levels = False
level_isobaric = [1000, 850, 500, 200]  # -999 for single level, works when levels == True
vari_dirName_all = ['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
vari_SaveName_all = ['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
var_ShortName = ['z', 'r', 't', 'u', 'v', 'w']
years = np.arange(1979, 2020 + 1)

for iI_var in range(len(var_ShortName)):
    try:
        anomaly_for_separate_file(vari_dirName_all[iI_var], vari_SaveName_all[iI_var], var_ShortName[iI_var])
    except Exception as e:
        logger.warning('something wrong with main function %s' % vari_SaveName_all[iI_var])
        logger.error(traceback.format_exc())

''' plot '''
# a = scipy.io.loadmat('D:/1116.mat')['1116']
# for i in a:
#     cb = plt.imshow(i[0]/98)
#     plt.colorbar(cb)
#     plt.show()
#     plt.close()
# print(a)
# exit(0)