import xarray as xr
import numpy as np
import os
import pandas as pd
os.environ['ECCODES_DEFINITION_PATH']=r'E:\Users\zhaoh\anaconda3\envs\cm\Library\share\eccodes\definitions'
# a = xr.open_dataset(r'D:\a_matlab\data_to_mat_moisture_traj\data\NorthWater.19790101.nc').variables['p72.162']
# print(a)
# index = 5
# a = xr.open_dataset(r'D:\a_matlab\data_to_mat_moisture_traj\data\%s.19790101.nc'%namelist[index]).variables[varilist[index]]
# print(a)

def step1_to_daily(data, vari, type='sum'):
    '''
    :param data: xr.variable
    :param type:
    :return:
    '''
    if type == 'sum':
        daily_data = data.resample(time='1D').sum()
    elif type == 'speed':
        daily_data = data.mean(dim='time')
        daily_data = daily_data * 24 * 60 * 60
    elif type == 'mean':
        daily_data = data.resample(time='1D').mean()
    else:
        print("given type can't be recognized")
        return
    return daily_data[vari].values, daily_data.time.values

def main(time1, time2):
    import scipy.io as scio

    result = []
    for index in range(3):
        rdata = xr.open_dataset(main_address + r'/%s.daily.jra55.%s.nc' % (address[index], time1))
        stime = rdata.time.values
        result.append(rdata.variables[varilist[index]].values)

    for i, day in enumerate(stime):
        day = pd.to_datetime(day).strftime('%Y%m%d')

        data = {'ACPCP':result[0][i],
                'NCPCP':result[1][i],
                'EV':result[2][i],
                'long': lon,
                'lat': lat}
        scio.savemat('/home/linhaozhong/work/Data/MoistureTrackData/JRA55/precipevap/Daily_pe_%s.mat'%day, {'PE':data})


    # result = []
    # for index in range(3,6):
    #     rdata = xr.open_dataset(main_address + r'anl_column%s.%s00_%s18' % (address[index], time1, time2),
    #                             engine='cfgrib')
    #
    #     sdata, time = step1_to_daily(rdata, varilist[index], typelist[index])
    #     result.append(sdata)
    # result.append(result[1]/result[0])
    # result.append(result[2] / result[0])
    # for i, day in enumerate(time):
    #     day = pd.to_datetime(day).strftime('%Y%m%d')
    #     data = {'PWAT': result[0][i],
    #             'UWVcol':result[1][i],
    #             'VWVcol':result[2][i],
    #             'UQ':result[3][i],
    #             'VQ':result[4][i],
    #             'long':lon,
    #             'lat':lat}
    #
    #     scio.savemat('/home/linhaozhong/work/Data/MoistureTrackData/JRA55'+'/vapordata/Daily_vapor_%s.mat'%day, {'QPcol':data})


address = ['precipc', 'precipl','evapor',
           '125.054_pwat', '125.157_uwv','125.152_vwv']

varilist = ['precipc', 'precipl', 'evapor',
             'tcwv','viwve','viwvn']

typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']

main_address = r'/home/linhaozhong/work/Data/JRA55/JRA55-Daily/surface'


rdata = xr.open_dataset(main_address + r'/%s.daily.jra55.%s.nc' % (address[0], '1979'))
lon = rdata.longitude.values
lat = rdata.latitude.values
lon, lat = np.meshgrid(lon, lat)

for i in range(1958, 2018):
    main(i,0)

def vapor():
    address = ['Convective_precipitation', 'Large_scale_precipitation', 'Evaporation',
               '125.054_pwat', '125.157_uwv', '125.152_vwv']

    varilist = ['cp', 'lsp', 'e',
                'tcwv', 'viwve', 'viwvn']

    typelist = ['sum', 'sum', 'sum', 'mean', 'mean', 'mean']
    main_address = r'/home/linhaozhong/work/Data/JRA55/JRA55_6H/total_columns'

    rdata = xr.open_dataset(main_address + r'anl_column%s.%s00_%s18' % (address[5], '19790101', '19791231'),
                            engine='cfgrib')
    # rdata = xr.open_dataset(r'D:\a_matlab\data_to_mat_moisture_traj\%s.%s.nc'%(namelist[3], '19790101')).variables['p71.162']
    lon = rdata.longitude.values
    lat = rdata.latitude.values
    lon, lat = np.meshgrid(lon, lat)
    import pandas as pd
    a = pd.date_range('1958-01-01', '2019-01-01', freq='1YS')
    b = pd.date_range('1958-12-31', '2019-12-31', freq='1Y')

    for i in range(len(a)):
        main(a[i].strftime('%Y%m%d'),b[i].strftime('%Y%m%d'))

    a = pd.date_range('2014-01-01', '2019-01-01', freq='1MS')
    b = pd.date_range('2014-01-31', '2019-01-31', freq='1M')
    for i in range(len(a)):
        main(a[i].strftime('%Y%m%d'), b[i].strftime('%Y%m%d'))