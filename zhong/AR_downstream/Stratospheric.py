import os
import pandas as pd
import numpy as np
import xarray as xr
import scipy.io as scio
from metpy.interpolate import cross_section
import nc4

# # import cartopy.crs as ccrs
# # import cartopy.feature as cfeature
# # import matplotlib.pyplot as plt
# # import numpy as np
# # import xarray as xr
# # import matplotlib.colors as colors
# # import metpy.calc as mpcalc
# # from metpy.cbook import get_test_data
# # from metpy.interpolate import cross_section
#
#
#
#
# def save_standard_nc(data, var_name, levels, lon=None, lat=None):
#     """
#     :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
#     :param var_name: ['t2m', 'ivt', 'uivt']
#     :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
#     :param lon: list, 720
#     :param lat: list, 360
#     :param path_save: r'D://'
#     :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
#     :return:
#     """
#     import xarray as xr
#     import numpy as np
#
#     if lon is None:
#         lon = np.arange(0, 360)
#     if lat is None:
#         lat = np.arange(90, -91, -1)
#
#     ds = xr.Dataset()
#     ds.coords["latitude"] = lat
#     ds.coords["longitude"] = lon
#     ds.coords['level'] = levels
#
#     for iI_vari, ivari in enumerate(var_name):
#         ds[ivari] = (('level', "latitude", "longitude"), np.array(data[iI_vari]))
#
#     return ds
#
# def get_circulation_data_based_on_pressure_date(date, var, var_ShortName):
#     """date containing Hour information
#     type: daily/monthly/monthly_now
#     """
#     import scipy
#     time = pd.to_datetime(date)
#
#     # load hourly anomaly data
#     def load_anomaly():
#         data_anomaly = \
#             scipy.io.loadmat(
#                 path_PressureData + '%s/anomaly_levels/' % var + '%s.mat' % time.strftime('%m%d'))[
#                 time.strftime('%m%d')][14, int(time.strftime('%H')), :, :]
#         return data_anomaly
#
#     data_anomaly = load_anomaly()
#
#     # load present data
#     data_present = xr.open_dataset(
#         path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
#             var, time.strftime('%Y%m%d')))
#
#     data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=200)[
#         var_ShortName].values
#
#     return data_present, data_anomaly
#     # return data_present
#
#
#
# def contour_cal(time_file):
#     info_AR_row = xr.open_dataset(path_contour + time_file[:-1])
#
#     time_AR = info_AR_row.time_AR.values
#     time_all = info_AR_row.time_all.values
#     AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#
#     data = []
#     anomaly = []
#     for iI_timeall, i_timeall in enumerate(time_all[AR_loc:]):
#         i_time = pd.to_datetime(i_timeall)
#
#         # i_data, i_anomaly = get_circulation_data_based_on_pressure_date(i_time, 'Hgt', 'z')
#         # data.append(i_data/9.8)
#         # anomaly.append(i_anomaly/9.8)
#
#         i_data, i_anomaly = get_circulation_data_based_on_pressure_date(i_time, 'Tem', 't')
#         data.append(i_data)
#         anomaly.append(i_anomaly)
#
#     nc4.save_nc4(np.array(data), path_SaveData+time_file[:-4]+'_Tem')
#     nc4.save_nc4(np.array(anomaly), path_SaveData+time_file[:-4]+'_anomaly'+'_Tem')
#
#
# path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
# resolution = 0.5
# path_SaveData = r'/home/linhaozhong/work/AR_NP85/stratosphere/'
# os.makedirs(path_SaveData, exist_ok=True)
#
# i_type = 4
# file = file_all[save_labels_bmus == i_type]
# data = []
# anomaly = []
# for i_file in file:
#     aa = contour_cal(i_file)
# exit()


# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979.mat")['filename']
# path_SaveData = r'/home/linhaozhong/work/AR_NP85/stratosphere/'
#
# i_type = 4
# file = file_all[save_labels_bmus == i_type]
# # data = np.empty([len(file), 361,720])
# data = []
# anomaly = []
# for i_file in file:
#     # aa = contour_cal(i_file)
#     a = nc4.read_nc4(path_SaveData+i_file[:-4])
#     b = nc4.read_nc4(path_SaveData + i_file[:-4] + '_anomaly')
#     data.extend(a)
#     anomaly.extend(b)
# from scipy.stats import ttest_ind
# _, pvalues = ttest_ind(data, anomaly, axis=0, equal_var=False)
# data = np.nanmean(np.array(data),axis=0)
# anomaly = np.nanmean(np.array(anomaly), axis=0)
#
# nc4.save_nc4(np.array([data-anomaly, pvalues]), '/home/linhaozhong/work/AR_NP85/SOM4_200hPa_hgt')
#
# exit()


import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.colors as colors


def plot_linregress(ax, trend):
    from matplotlib.colors import ListedColormap
    import matplotlib.colors as mcolors
    import matplotlib
    import cmaps

    cmap = ListedColormap(cmaps.MPL_RdBu_r.colors[0:-2])
    cmap.set_over(cmap.colors[-1])
    lev = list(range(-70, -30, 5)) + list(range(-30, 0, 25)) + list(range(0, 50, 25))+ list(range(50, 94, 5))
    # cmap=ListedColormap(matplotlib.colormaps['bwr'].color[0:-1])
    # norm = mcolors.BoundaryNorm(lev, matplotlib.colormaps['bwr'].N)
    norm = mcolors.BoundaryNorm(lev, cmap.N)


    cb = ax.contourf(lon, lat, np.ma.masked_where(trend[0][1]>0.1,trend[0][0])
                     , cmap=cmap, norm=norm,
                     levels=lev,
                     extend='both',
                     transform=ccrs.PlateCarree())
    cbar = fig.colorbar(cb, ax=ax, extend='both',
                        shrink=0.8, pad=0.02)
    cbar.set_label('gpm', labelpad=-10)
    # cbar.ax.yaxis.labelpad=0.001

    # ax.contourf(lon, lat, np.ma.masked_greater(trend[1][1], 0.01),
    #             colors='none', levels=[0, 0.01],
    #             hatches=[3 * '/', 3 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # tem
    cb = ax.contour(lon, lat, np.ma.masked_where(trend[1][1]>0.1, trend[1][0])
                     , colors='black',
                     levels=np.arange(-3,3.00001,0.2),
                     transform=ccrs.PlateCarree(),linewidths=0.8)

    clabels = cb.clabel(cb.levels[1::3], fontsize=5, colors='white', inline=1,
                                   inline_spacing=8, fmt='%0.1f', rightside_up=True, use_clabeltext=True)
    [txt.set_bbox(dict(facecolor='black', edgecolor='none', pad=0)) for txt in clabels]

    from function_shared_Main import add_longitude_latitude_labels
    add_longitude_latitude_labels(fig, ax, xloc=30, yloc=0)
    # ax.gridlines(ylocs=[20,40,60,80], xlocs=[0, 60, 120, 179.9999, -60, -120],
    #              draw_labels=True,
    #              linewidth=0.3, color='black', xpadding=3)
    ax.set_extent([0, 360, 20, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.1, zorder=10)


SMALL_SIZE=7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize
import matplotlib as mpl
mpl.rcParams['hatch.color'] = 'black'  # previous pdf hatch linewidth
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth


fig = plt.figure(figsize=[7,3])
plt.subplots_adjust(top=0.925,
bottom=0.08,
left=0.025,
right=0.95,
hspace=0.185,
wspace=0.14)

lon = np.arange(0,360,0.5)
lat = np.arange(90,-90.001,-0.5)

ax = fig.add_subplot(1, 2, 1, projection=ccrs.NorthPolarStereo())
hgt_p = nc4.read_nc4('SOM4_200hPa_hgt')
t200_p = nc4.read_nc4('SOM4_200hPa_t')
plot_linregress(ax, [hgt_p, t200_p])
ax.set_title('(a)', loc='left', fontdict={'size':9})

"""
cross-section for SOM4
"""
ax = fig.add_subplot(1, 2, 2)

lat_or_lon = 'lat'

if lat_or_lon == 'lat':
    cross_0 = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\1_lat_2000")
    cross_anomaly =  nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\1_lat_anomaly_2000")
    cross = xr.open_dataset(r'D:/20190215_11_lon.nc')
    xx = np.arange(100)

else:
    cross_0 = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\1_lon_2000")
    cross_anomaly =  nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\1_lon_anomaly_2000")
    cross = xr.open_dataset(r"D:\20201220_05_lat.nc")
    xx = cross['latitude']

data = cross_0-cross_anomaly
from scipy.stats import ttest_ind, ttest_1samp

_, p = ttest_1samp(data, 0, axis=0)
# _,p = ttest_ind(cross_0, cross_anomaly, axis=0)
data = np.nanmean(data, axis=0)
p_uv = (p[2] > 0.1) & (p[3] > 0.1)


xx, yy = np.meshgrid(xx, cross['level'].values)
# Plot RH using contourf
divnorm = colors.TwoSlopeNorm(vmin=-300., vcenter=0, vmax=200)
rh_contour = ax.contourf(xx, yy,
                         data[0]/9.8, levels=np.arange(-300,201,10),
                         norm=divnorm,cmap='RdBu_r', extend='both')
rh_colorbar = fig.colorbar(rh_contour,
                        shrink=0.8, pad=0.02)
rh_colorbar.set_label('gpm', labelpad=-5)
ax.contourf(xx, yy, np.ma.masked_greater(p[0], 0.1),
            colors='none', levels=[0, 0.1],
            hatches=[3 * '/', 3 * '/'], alpha=0, zorder=20)



# Plot potential temperature using contour, with some custom labeling
theta_contour = ax.contour(xx, yy,
                         np.ma.masked_where(p[1]>0.1, data[1]), levels=np.arange(-10,11,2),colors='darkgreen', linewidths=1)
clabels = theta_contour.clabel(theta_contour.levels[1::2], fontsize=5, colors='k', inline=1,
                     inline_spacing=8, fmt='%i', rightside_up=True, use_clabeltext=True,)
[txt.set_bbox(dict(facecolor='white', edgecolor='none', pad=0)) for txt in clabels]

theta_contour = ax.contour(xx[:9], yy[:9],
                         np.ma.masked_where(p[1]>0.1, data[1])[:9], levels=np.arange(-10,11,1),colors='darkgreen', linewidths=1)
clabels = theta_contour.clabel(theta_contour.levels[1::2], fontsize=5, colors='k', inline=1,
                     inline_spacing=8, fmt='%i', rightside_up=True, use_clabeltext=True)
[txt.set_bbox(dict(facecolor='white', edgecolor='none', pad=0)) for txt in clabels]



spacelevel = [3,4]
ax.barbs(xx[::spacelevel[0], ::spacelevel[1]], yy[::spacelevel[0], ::spacelevel[1]],
         np.ma.masked_where(p_uv, data[2])[::spacelevel[0], ::spacelevel[1]],
         np.ma.masked_where(p_uv, data[3])[::spacelevel[0], ::spacelevel[1]],
            color='k', length=5)

spacelevel = [1,4]
ax.barbs(xx[:9,:][::spacelevel[0], ::spacelevel[1]], yy[:9,:][::spacelevel[0], ::spacelevel[1]],
         np.ma.masked_where(p_uv, data[2])[:9, :][::spacelevel[0], ::spacelevel[1]],
         np.ma.masked_where(p_uv, data[3])[:9, :][::spacelevel[0], ::spacelevel[1]],
            color='k', length=5)

# Adjust the y-axis to be logarithmic
ax.set_yscale('symlog')
print(cross['level'])
ax.set_ylim([1000,1])
ax.set_yticks([1000,500,200,100,50,20,7,5,2,1])
ax.set_yticklabels([1000,500,200,100,50,20,7,5,2,1])
ax.set_ylabel('hPa', labelpad=-8)

ax.set_xticks([10,30,50,70,90])
ax.set_xticklabels(['-4°','-2°', 'AR centroid', '2°', '4°'])

ax.set_title('(b)', loc='left', fontdict={'size':9})
plt.savefig('dissertation_figure5-6.png', dpi=400)
plt.show()

# nc4.save_nc4(, r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\ridge_data')
# nc4.save_nc4(np.nansum(anomaly,axis=0), r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\ridge_anomaly')
exit()
