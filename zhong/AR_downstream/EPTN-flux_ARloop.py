import os
import numpy as np
import pandas as pd
import scipy.io as scio
from aostools import climate
import xarray as xr
import datetime as dt
from datetime import timedelta
import netCDF4 as nc

def slice_xarray_longitude(ds, lon_slice):
    """
    :param lon_slice: list with slice object, such as [slice(340,360),slice(0,60)]
    :return: recheck if the rank of lon is correct!
    """
    if isinstance(lon_slice, list):
        ds_collect = []
        for i_lonslice in lon_slice:
            ds_collect.append(ds.sel(lon=i_lonslice))
        return xr.merge(ds_collect)
    else:
        return ds.sel(lon=lon_slice)


"""
EP_flux_0M: 
        if i_type == 4:
            lon_slice = slice(320,360)
        if i_type == 1:
            lon_slice = [slice(320,360),slice(0,120)]
        if i_type == 6:
            lon_slice = slice(120,250)
            
EP_flux_0M_old:
        if i_type == 1:
            lon_slice = [slice(320,360),slice(0,60?)]
        if i_type == 6:
            lon_slice = slice(120??,250??)
            
EP_flux_allLon:
            total Arctic,!!!useful for SOM6 for the downward motion!            
EP_flux_1M_SOM1:
            total Arctic, 1M lag for SOM1,no use!
TN_flux_1M:
           total Arctic, 1M lag for SOM1,no use! 
           

"""
# path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
# resolution = 0.5
#
# add_month = 0
# monthly_ep = False
#
#
# for i_type in [4]:
#     file = file_all[save_labels_bmus == i_type]
#     data = []
#     anomaly = []
#     for i_file in file:
#         info_AR_row = xr.open_dataset(path_contour + i_file[:-1])
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#         AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#
#         if not monthly_ep:
#             time_all = time_all[AR_loc:]
#             # 6H to daily
#             time_all = np.unique([pd.to_datetime(i).strftime('%Y-%m-%d') for i in time_all])
#             if len(time_all)<3:
#                 ts = dt.datetime(int(time_all[0][:4]), int(time_all[0][5:7]), int(time_all[0][8:10])) - timedelta(days=1)
#                 te = dt.datetime(int(time_all[0][:4]), int(time_all[0][5:7]), int(time_all[0][8:10])) + timedelta(days=1)
#                 time_all = pd.date_range(ts.strftime('%Y%m%d'), te.strftime('%Y%m%d'), freq='1D')
#             else:
#
#                 time_all = pd.date_range(str(time_all[0]), str(time_all[-1]), freq='1D')
#
#             time_all = time_all+ pd.DateOffset(months=add_month)
#         else:
#             year_AR = int(time_AR[0][:4])
#             if int(time_AR[0][4:6])>3: # if AR month is 11 or 12, find March in next year.
#                 year_AR += 1
#             time_all = pd.to_datetime('%i-03'%year_AR)
#         """
#         create nc for analysis from ERA5
#         u,v,w, t and climate ones.
#         """
#
#         def create_epflux_calculate_data(dates_all, eptn='ep'):
#
#             uvw = []
#             ref = []
#             for i_date in dates_all:
#                 i_date = i_date.strftime('%Y%m%d')
#                 s_uvw = []
#                 s_ref = []
#
#                 for i_varShort in var_shortName:
#                     var = xr.open_dataset(path_pressure + '%s/'%var_Name[i_varShort] + i_date[:4] + '/%s.%s.nc' %(var_Name[i_varShort], i_date))
#
#                     var = var.isel(level=level_iloc_used, latitude=lat_iloc, longitude=lon_iloc)
#                     var = var.mean(dim=['time'])
#
#                     s_uvw.append(var[i_varShort].values)
#                     if eptn=='tn':
#                         var = scio.loadmat(path_pressure + '%s/anomaly_levels/%s.mat' % (var_Name[i_varShort],i_date[4:]))
#                         var = np.nanmean(var[i_date[4:]], axis=1)
#                         s_ref.append(np.array(var[level_iloc_used][:,lat_iloc,:][:,:,lon_iloc]))
#                 uvw.append(s_uvw)
#                 if eptn == 'tn':
#                     ref.append(s_ref)
#
#             uvw = np.array(uvw)
#
#             if eptn == 'tn':
#                 ref = np.array(ref)
#
#             ds = xr.Dataset()
#             ds.coords["lat"] = lat[lat_iloc]
#             ds.coords["lon"] = lon[lon_iloc]
#             ds.coords["time"] = dates_all
#             ds.coords["level"] = level_used
#
#             for iI_varShort, i_varShort in enumerate(var_shortName):
#                 ds[i_varShort ] = (("time", "level", "lat", "lon"), np.array(uvw[:,iI_varShort]))
#                 if eptn == 'tn':
#                     ds[i_varShort + '_cli'] = (("time", "level", "lat", "lon"), np.array(ref[:,iI_varShort]))
#             # ds.to_netcdf(r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/eptnFlux_test.nc')
#             return ds
#
#         path_pressure = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
#         eptn = 'ep'
#         var_shortName = ['u', 'v', 't']
#         var_Name = dict(zip(var_shortName, ['Uwnd', 'Vwnd', 'Tem']))
#         # level_used = [10,20,30,50,70,100,150,200,300,400,500,700,1000]
#         # level_iloc_used = [5,6,7,8,9,10,12,14,17,19,21,25,36]
#         level_used = [1, 2, 3, 5, 7, 10, 20, 30, 50, 70, 100, 125, 150, 175, 200, 225, 250, 300, 350, 400, 450, 500, 550,
#                       600,
#                       650, 700, 750, 775, 800, 825, 850, 875, 900, 925, 950, 975, 1000]
#         level_iloc_used = np.arange(0, 37)
#         lat = np.arange(90, -90.001, -0.5)
#         lon = np.arange(0, 360, 0.5)
#
#         lat_iloc = np.arange(0, 181, 2)
#         lon_iloc = np.arange(0, 720, 2)
#         era_u_v_t = create_epflux_calculate_data(time_all, eptn=eptn)
#         if i_type == 4:
#             lon_slice = slice(320,360)
#         if i_type == 6:
#             lon_slice = slice(120,250)
#         era_u_v_t = slice_xarray_longitude(era_u_v_t, lon_slice)
#
#         f1, f2, div1, div2 = climate.ComputeEPfluxDivXr(
#             u=era_u_v_t.u, v=era_u_v_t.v, t=era_u_v_t.t,
#             lon='lon', lat='lat', pres='level', time='time')
#         ds = xr.merge([f1, f2, div1, div2])
#
#         path_SaveData = r'/home/linhaozhong/work/AR_NP85/EP_flux_%iM/'%add_month
#         os.makedirs(path_SaveData, exist_ok=True)
#         ds.to_netcdf(path_SaveData+i_file[:-1])
#
#
#         # eptn = 'tn'
#         # var_shortName = ['u', 'v', 'z']
#         # var_Name = dict(zip(var_shortName, ['Uwnd', 'Vwnd', 'Hgt']))
#         # level_used = [300, 500]
#         # level_iloc_used = [17,21]
#         # lat = np.arange(90, -90.001, -0.5)
#         # lon = np.arange(0, 360, 0.5)
#         # lat_iloc = np.arange(0, 181, 1)
#         # lon_iloc = np.arange(0, 720, 1)
#         #
#         # def tn_flux(climate_datas):
#         #     a = 6.37e6  # Earth Radius
#         #     omega = 7.292e-5  # Rotational angular velocity of the Earth
#         #
#         #     lon = climate_datas.lon.values
#         #     lat = climate_datas.lat.values
#         #
#         #     dlon = np.gradient(lon) * np.pi / 180.0
#         #     dlat = np.gradient(lat) * np.pi / 180.0
#         #     f = np.array(
#         #         list(map(lambda x: 2 * omega * np.sin(x * np.pi / 180.0), lat)))  # Coriolis parameter: f=2*omgega*sin(lat)
#         #
#         #     cos_lat = np.array(list(map(lambda x: np.cos(x * np.pi / 180.0), lat)))  # cos(lat)
#         #
#         #     pxpypsi = []
#         #     for iI_climate_data in range(len(climate_datas.time.values)):
#         #         climate_data = climate_datas.isel(time=iI_climate_data)
#         #         u_c = climate_data.variables['u_cli'].values
#         #         v_c = climate_data.variables['v_cli'].values
#         #         phi_c = climate_data.variables['z_cli'].values
#         #         psi_p = ((climate_data.variables['z'].values - phi_c).T / f).T  # Pertubation stream-function
#         #         dpsi_dlon = np.gradient(psi_p, dlon[1])[1]
#         #         dpsi_dlat = np.gradient(psi_p, dlat[1])[0]
#         #         d2psi_dlon2 = np.gradient(dpsi_dlon, dlon[1])[1]
#         #         d2psi_dlat2 = np.gradient(dpsi_dlat, dlat[1])[0]
#         #         d2psi_dlondlat = np.gradient(dpsi_dlat, dlon[1])[1]
#         #
#         #         termxu = dpsi_dlon * dpsi_dlon - psi_p * d2psi_dlon2
#         #         termxv = dpsi_dlon * dpsi_dlat - psi_p * d2psi_dlondlat
#         #         termyv = dpsi_dlat * dpsi_dlat - psi_p * d2psi_dlat2
#         #
#         #         # coefficient
#         #         p_lev = 300.0  # unit in hPa
#         #         p = p_lev / 1000.0
#         #         magU = np.sqrt(u_c ** 2 + v_c ** 2)
#         #         coeff = ((p * cos_lat) / (2 * magU.T)).T
#         #         # x-component of TN-WAF
#         #         px = (coeff.T / (a * a * cos_lat)).T * (((u_c.T) / cos_lat).T * termxu + v_c * termxv)
#         #         # y-component of TN-WAF
#         #         py = (coeff.T / (a * a)).T * (((u_c.T) / cos_lat).T * termxv + v_c * termyv)
#         #         pxpypsi.append([px,py,psi_p])
#         #     return np.array(pxpypsi)
#         #
#         # era_u_v_t = create_epflux_calculate_data(time_all, eptn=eptn)
#         #
#         # ds = xr.Dataset()
#         # ds.coords["lat"] = era_u_v_t.lat
#         # ds.coords["lon"] = era_u_v_t.lon
#         # ds.coords['level'] = era_u_v_t.level
#         # ds.coords['time'] = era_u_v_t.time
#         #
#         # ds_level = []
#         # for i_level in era_u_v_t.level.values:
#         #     pxpypsi = tn_flux(era_u_v_t.sel(level=i_level))
#         #     ds_level.append(pxpypsi)
#         # ds_level = np.array(ds_level)
#         # ds['px'] = (("level", "time", "lat", "lon"), ds_level[:,:,0])
#         # ds['py'] = (("level", "time", "lat", "lon"), ds_level[:,:,1])
#         # ds['psi'] = (("level", "time", "lat", "lon"), ds_level[:,:,2])
#         # path_SaveData = r'/home/linhaozhong/work/AR_NP85/TN_flux_%iM/'%add_month
#         # os.makedirs(path_SaveData, exist_ok=True)
#         # ds.to_netcdf(path_SaveData+i_file[:-1])
# exit()

"""
plot
"""

path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
resolution = 0.5
path_TNData = r'/home/linhaozhong/work/AR_NP85/TN_flux/'
# path_EPData = r'/home/linhaozhong/work/AR_NP85/EP_flux_0M/'

eptn = 'tn'
# if eptn == 'ep':
#
#     for i_type in [4,6]:
#         composite = []
#         file = file_all[save_labels_bmus == i_type]
#         data = []
#         anomaly = []
#         for i_file in file:
#             flux = xr.open_dataset(path_EPData+i_file[:-1])
#
#             var = ['ep1', 'ep2', 'div1', 'div2']
#             # flux = flux.mean(dim='time')
#             # flux = flux.isel(time=-1)
#             varlist = []
#             for i_var in var:
#                 varlist.append(flux.isel(time=-1)[i_var].values-flux.isel(time=0)[i_var].values)
#             composite.append(varlist)
#         # composite = np.nanmean(np.array(composite), axis=0)
#         # ds = xr.Dataset()
#         # ds.coords["lat"] = flux.lat
#         # ds.coords["level"] = flux.level
#         # ds['ep1'] = (("level", "lat"),composite[0])
#         # ds['ep2'] = (("level", "lat"), composite[1])
#         # ds['div1'] = (("level", "lat"),composite[2])
#         # ds['div2'] = (("level", "lat"), composite[3])
#         # ds.to_netcdf(r'/home/linhaozhong/work/AR_NP85/epComposite_time-end-begin_%i_mean.nc'%i_type)
#
#         composite = np.array(composite)
#         ds = xr.Dataset()
#         ds.coords["lat"] = flux.lat
#         ds.coords["level"] = flux.level
#         ds.coords["time"] = np.arange(composite.shape[0])
#         ds['ep1'] = (("time","level", "lat"),composite[:,0])
#         ds['ep2'] = (("time","level", "lat"), composite[:,1])
#         ds['div1'] = (("time","level", "lat"),composite[:,2])
#         ds['div2'] = (("time","level", "lat"), composite[:,3])
#         ds.to_netcdf(r'/home/linhaozhong/work/AR_NP85/epComposite_time-end-begin_%i.nc'%i_type)

if eptn == 'tn':

    for i_type in [6]:
        composite = []
        file = file_all[save_labels_bmus == i_type]
        data = []
        anomaly = []
        for i_file in file:
            flux = xr.open_dataset(path_TNData + i_file[:-1])
            flux = flux.isel(time=0)
            var = ['px', 'py', 'psi']
            varlist = []
            for i_var in var:
                varlist.append(flux[i_var].values)
            composite.append(varlist)
        composite = np.array(composite)
        ds = xr.Dataset()
        ds.coords["level"] = flux.level
        ds.coords["lat"] = flux.lat
        ds.coords["lon"] = flux.lon
        ds.coords["time"] = np.arange(composite.shape[0])

        ds['px'] = (("time","level", "lat", "lon"), composite[:,0])
        ds['py'] = (("time","level", "lat", "lon"), composite[:,1])
        ds['psi'] = (("time","level", "lat", "lon"), composite[:,2])
        ds.to_netcdf(r'/home/linhaozhong/work/AR_NP85/tnComposite_time-begin_%i.nc'%i_type)

    for i_type in [6]:
        composite = []
        file = file_all[save_labels_bmus == i_type]
        data = []
        anomaly = []
        for i_file in file:
            flux = xr.open_dataset(path_TNData + i_file[:-1])
            flux = flux.isel(time=-1)
            var = ['px', 'py', 'psi']
            varlist = []
            for i_var in var:
                varlist.append(flux[i_var].values)
            composite.append(varlist)
        composite = np.array(composite)
        ds = xr.Dataset()
        ds.coords["level"] = flux.level
        ds.coords["lat"] = flux.lat
        ds.coords["lon"] = flux.lon
        ds.coords["time"] = np.arange(composite.shape[0])

        ds['px'] = (("time","level", "lat", "lon"), composite[:,0])
        ds['py'] = (("time","level", "lat", "lon"), composite[:,1])
        ds['psi'] = (("time","level", "lat", "lon"), composite[:,2])
        ds.to_netcdf(r'/home/linhaozhong/work/AR_NP85/tnComposite_time-end_%i.nc'%i_type)