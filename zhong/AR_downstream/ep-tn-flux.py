import numpy as np
from aostools import climate
import xarray as xr
import cartopy.crs as ccrs
import matplotlib.pyplot as plt


data = xr.open_dataset(r'G:/uvz-300hPa_1979-2017_Jan.nc')

wx,wy,div = climate.ComputeWaveActivityFlux(phi_or_u=data.u,
                                                 phiref_or_v=data.v,
                                                 uref=data.uref,
                                                 vref=data.vref,
                                     lat='latitude',
                                     lon='longitude',
                                    pres='',
                                     qg=False)

# wx,wy,div = climate.ComputeWaveActivityFlux(phi_or_u=data.z,
#                                             phiref_or_v=data.zref,
#                                      lat='latitude',
#                                      lon='longitude',
#                                     pres='',
#                                      qg=True)
lon = data.longitude.values
lat= data.latitude.values
lon, lat = np.meshgrid(lon, lat)
spacewind = [20, 20]
fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})

qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                wx.values[::spacewind[1], ::spacewind[0]],
                wy.values[::spacewind[1], ::spacewind[0]]
                , transform=ccrs.PlateCarree())
qk = ax.quiverkey(qui, 0.6, 1.05, 20, r'$\mathrm{20\,m^{2}s^{-2}}$', labelpos='E',
                  coordinates='axes', color='black', labelsep=0.01)
ax.set_extent([0,359,20,89], crs=ccrs.PlateCarree())
ax.coastlines()
plt.show()