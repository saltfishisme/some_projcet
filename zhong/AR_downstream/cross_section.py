import os
import pandas as pd
import numpy as np
import xarray as xr
import scipy.io as scio
from metpy.interpolate import cross_section
import nc4
import metpy.calc as mpcalc


import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import matplotlib.colors as colors
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
import cartopy.crs as ccrs
import cartopy.feature as cfeature


lat_or_lon = 'lat'

for i_file in os.listdir(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section/'):
    if lat_or_lon == 'lat':
        cross_0 = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\6_lat_2000")
        cross_anomaly =  nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\6_lat_2000_anomaly")
        cross = xr.open_dataset(r'D:/20190215_11_lon.nc')
        xx = np.arange(100)

    else:
        cross_0 = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\1_lon_2000")
        cross_anomaly =  nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section\1_lon_2000_anomaly")
        cross = xr.open_dataset(r"D:\20201220_05_lat.nc")
        xx = cross['latitude']

    data = cross_0-cross_anomaly
    from scipy.stats import ttest_ind, ttest_1samp
    _,p = ttest_1samp(data, 0,axis=0)
    # _,p = ttest_ind(cross_0, cross_anomaly, axis=0)
    data = np.nanmean(data, axis=0)
    p_uv = (p[2] > 0.1)& (p[3] > 0.1)

    plt.rcParams['hatch.linewidth'] = 0.2
    fig = plt.figure(1)
    ax = plt.axes()
    plt.subplots_adjust(
        top=0.95,
        bottom=0.08,
        left=0.105,
        right=0.99,
        hspace=0.2,
        wspace=0.2
    )


    xx, yy = np.meshgrid(xx, cross['level'].values)
    # Plot RH using contourf
    # divnorm = colors.TwoSlopeNorm(vmin=-50., vcenter=0, vmax=300)
    rh_contour = ax.contourf(xx, yy,
                             data[0]/9.8, levels=np.arange(0,221,5),
                             cmap='YlOrRd', extend='both')
    rh_colorbar = fig.colorbar(rh_contour)
    rh_colorbar.set_label('gpm')

    ax.contourf(xx, yy, np.ma.masked_greater(p[0], 0.1),
                colors='none', levels=[0, 0.1],
                hatches=[3 * '/', 3 * '/'], alpha=0, zorder=20)


    # Plot potential temperature using contour, with some custom labeling
    theta_contour = ax.contour(xx, yy,
                             np.ma.masked_where(p[1]>0.1, data[1]), levels=np.arange(-10,11,1),colors='darkgreen', linewidths=1)
    clabels = theta_contour.clabel(theta_contour.levels[1::2], fontsize=7, colors='k', inline=1,
                         inline_spacing=8, fmt='%i', rightside_up=True, use_clabeltext=True)
    [txt.set_bbox(dict(facecolor='white', edgecolor='none', pad=0)) for txt in clabels]

    theta_contour = ax.contour(xx[:9], yy[:9],
                               np.ma.masked_where(p[1]>0.1, data[1])[:9], levels=np.arange(-10, 11, 0.5), colors='darkgreen', linewidths=1)
    clabels = theta_contour.clabel(theta_contour.levels[1::2], fontsize=7, colors='k', inline=1,
                                   inline_spacing=8, fmt='%0.1f', rightside_up=True, use_clabeltext=True)
    [txt.set_bbox(dict(facecolor='white', edgecolor='none', pad=0)) for txt in clabels]

    spacelevel = [3,4]
    ax.barbs(xx[::spacelevel[0], ::spacelevel[1]], yy[::spacelevel[0], ::spacelevel[1]],
                np.ma.masked_where(p_uv, data[2])[::spacelevel[0], ::spacelevel[1]],
                np.ma.masked_where(p_uv, data[3])[::spacelevel[0], ::spacelevel[1]],
                color='k', length=5)

    spacelevel = [1,4]
    ax.barbs(xx[:9,:][::spacelevel[0], ::spacelevel[1]], yy[:9,:][::spacelevel[0], ::spacelevel[1]],
                np.ma.masked_where(p_uv, data[2])[:9,:][::spacelevel[0], ::spacelevel[1]],
                np.ma.masked_where(p_uv, data[3])[:9,:][::spacelevel[0], ::spacelevel[1]],
                color='k', length=5)

    # Adjust the y-axis to be logarithmic
    ax.set_yscale('symlog')
    print(cross['level'])
    ax.set_ylim([1000,1])
    ax.set_yticks([1000,500,200,100,50,20,7,5,2,1])
    ax.set_yticklabels([1000,500,200,100,50,20,7,5,2,1])
    ax.set_ylabel('hPa')

    ax.set_xticks([10,30,50,70,90])
    ax.set_xticklabels(['-4°','-2°', 'AR centroid', '2°', '4°'])
    plt.savefig('dissertation_figure5-4.png', dpi=400)
    plt.show()
    plt.close()
exit()


def save_standard_nc(data, var_name, levels, lon=None, lat=None):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    ds = xr.Dataset()
    ds.coords["latitude"] = lat
    ds.coords["longitude"] = lon
    ds.coords['level'] = levels

    for iI_vari, ivari in enumerate(var_name):
        ds[ivari] = (('level', "latitude", "longitude"), np.array(data[iI_vari]))

    return ds

def get_circulation_data_based_on_pressure_date(date, var, var_ShortName):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    # load hourly anomaly data
    def load_anomaly():
        data_anomaly = \
            scipy.io.loadmat(
                path_PressureData + '%s/anomaly_levels/' % var + '%s.mat' % time.strftime('%m%d'))[
                time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
        return data_anomaly

    data_anomaly = load_anomaly()

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
            var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[
        var_ShortName].values
    return data_present,data_anomaly
    # return data_present


def cross_section_Analysis(s_time, start, end):
    info_nc = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/Hgt/2019/Hgt.20191231.nc'

    # vari_SaveName_all=['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
    # var_ShortName = ['z', 'r', 't', 'u', 'v', 'w']
    vari_SaveName_all=['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
    var_ShortName = ['z', 'r', 't', 'u', 'v']
    data = []
    anomaly = []
    for iI_var, i_var in enumerate(vari_SaveName_all):
        i_data, i_anomaly = get_circulation_data_based_on_pressure_date(s_time, i_var, var_ShortName[iI_var])
        data.append(i_data)
        anomaly.append(i_anomaly)

    info_nc = xr.open_dataset(info_nc)

    data = save_standard_nc(np.array(data), vari_SaveName_all,
                            info_nc.level.values, lat=info_nc.latitude.values, lon=info_nc.longitude.values)
    data = data.metpy.parse_cf().squeeze()
    cross = cross_section(data, start, end).set_coords(('latitude', 'longitude'))
    cross['t_wind'], cross['n_wind'] = mpcalc.cross_section_components(
        cross['Uwnd'],
        cross['Vwnd']
    )


    anomaly = save_standard_nc(np.array(anomaly), vari_SaveName_all,
                            info_nc.level.values, lat=info_nc.latitude.values, lon=info_nc.longitude.values)
    anomaly = anomaly.metpy.parse_cf().squeeze()
    cross_anomaly = cross_section(anomaly, start, end).set_coords(('latitude', 'longitude'))
    cross_anomaly['t_wind'], cross_anomaly['n_wind'] = mpcalc.cross_section_components(
        cross_anomaly['Uwnd'],
        cross_anomaly['Vwnd']
    )

    return cross, cross_anomaly


def contour_cal(time_file):
    save_path = path_SaveData+time_file[:-1]+'/'
    os.makedirs(save_path, exist_ok=True)

    info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

    time_AR = info_AR_row.time_AR.values
    time_all = info_AR_row.time_all.values
    AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))

    def construct_lon(long):
        long = long%360
        return xr.where(long > 180,
                 long - 360,
                 long)

    for iI_timeall, i_timeall in enumerate(time_all[AR_loc:]):

        i_contour = info_AR_row['contour_all'].sel(time_all=i_timeall).values
        i_centroid = info_AR_row['centroid_all'].sel(time_all=i_timeall).values
        c_lat, c_lon = i_centroid[0:2]
        i_time = pd.to_datetime(i_timeall)

        if os.path.exists(save_path+'%s_lat.nc'%i_time.strftime('%Y%m%d_%H')):
            continue
        cross, cross_anomaly = cross_section_Analysis(i_time,
                               start = (np.min([c_lat+5, 90]),construct_lon(c_lon)),
                               end = (np.min([c_lat-5, 90]),construct_lon(c_lon)))
        cross = cross.reset_coords('metpy_crs', drop=True)
        cross.to_netcdf(save_path+'%s_lat.nc'%i_time.strftime('%Y%m%d_%H'))

        cross_anomaly = cross_anomaly.reset_coords('metpy_crs', drop=True)
        cross_anomaly.to_netcdf(save_path+'%s_lat_anomaly.nc'%i_time.strftime('%Y%m%d_%H'))


        if os.path.exists(save_path+'%s_lon.nc'%i_time.strftime('%Y%m%d_%H')):
            continue
        cross, cross_anomaly = cross_section_Analysis(i_time,
                               start = (c_lat,construct_lon(c_lon-5)),
                               end = (c_lat,construct_lon(c_lon+5)))
        cross = cross.reset_coords('metpy_crs', drop=True)
        cross.to_netcdf(save_path+'%s_lon.nc'%i_time.strftime('%Y%m%d_%H'))
        cross_anomaly = cross_anomaly.reset_coords('metpy_crs', drop=True)
        cross_anomaly.to_netcdf(save_path+'%s_lon_anomaly.nc'%i_time.strftime('%Y%m%d_%H'))

path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
resolution = 0.5
path_SaveData = r'/home/linhaozhong/work/AR_NP85/cross_section_withAnomaly/'
i_type = 4
file = file_all[save_labels_bmus == i_type]
cs_lon = []
cs_lat = []
for i_file in file:
    aa = contour_cal(i_file)

i_type = 6
file = file_all[save_labels_bmus == i_type]
cs_lon = []
cs_lat = []
for i_file in file:
    aa = contour_cal(i_file)
exit()


path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
resolution = 0.5
path_SaveData = r'/home/linhaozhong/work/AR_NP85/cross_section_withAnomaly/'

add_ano = '_anomaly'
for i_type in [1,6]:
    if i_type == 0:
        continue
    file = file_all[save_labels_bmus == i_type]
    cs_lon = []
    cs_lat = []
    for i_file in file:
        # aa = contour_cal(i_file)

        save_path = path_SaveData + i_file[:-1] + '/'
        cross_section_AR = os.listdir(save_path)
        var_collec_lon = []
        var_collec_lat = []
        for i_cs in os.listdir(save_path):

            if '_lon%s.nc'%add_ano in i_cs:
                a = xr.open_dataset(save_path+i_cs)
                var_collec_lon.append([a['Hgt'].values,a['Tem'].values,a['t_wind'].values,a['n_wind'].values])

            if '_lat%s.nc'%add_ano in i_cs:
                a = xr.open_dataset(save_path+i_cs)
                var_collec_lat.append([a['Hgt'].values,a['Tem'].values,a['t_wind'].values,a['n_wind'].values])

        var_collec_lat = np.nanmean(np.array(var_collec_lat), axis=0)
        var_collec_lon = np.nanmean(np.array(var_collec_lon), axis=0)
        cs_lon.append(var_collec_lon)

        cs_lat.append(var_collec_lat)
    cs_lat = np.array(cs_lat)
    cs_lon = np.array(cs_lon)
    nc4.save_nc4(cs_lon, path_SaveData+'%i_lon_2000%s'%(i_type, add_ano))
    nc4.save_nc4(cs_lat, path_SaveData + '%i_lat_2000%s' % (i_type, add_ano))

exit()

