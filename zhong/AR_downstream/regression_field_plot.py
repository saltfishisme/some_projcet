import os

import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
import nc4
import pandas as pd
import cartopy.crs as ccrs
"""
downstream analysis
given a series (sea ice loss), given month/day 
"""

'''
Constructing the field
monthly
only works when AR occurrence in this year is not zero.
we will exclude the years without AR from the given series.
'''


def plot_linregress(fig, trend, filename, p_values=None):
    central_longitude = 180
    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
    ax = fig.add_subplot(2, 1, 2, projection=ccrs.PlateCarree(central_longitude=central_longitude))

    trend[trend == 0] = np.nan

    # trend[trend > 1.8] = 1.8
    import matplotlib.colors as colors

    if p_values is not None:
        p_values[np.abs(trend) < 0.01] = np.nan
    # trend[np.abs(trend) < 0.01] = np.nan

    colors_undersea = plt.cm.bwr(np.linspace(0, 0.5, 256))
    colors_land = plt.cm.bwr(np.linspace(0.7, 1, 256))
    all_colors = np.vstack((colors_undersea, colors_land))
    terrain_map = colors.LinearSegmentedColormap.from_list(
        'terrain_map', all_colors)

    divnorm = colors.TwoSlopeNorm(vmin=-0.1, vcenter=0, vmax=0.1)

    cb = ax.contourf(lon, lat, trend
                     , cmap='bwr_r', norm=divnorm,
                    levels=list(np.arange(-0.1,0,0.01))+list(np.arange(0,0.100001,0.01)),
                     extend='both',
                     transform=ccrs.PlateCarree())
    # cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    # cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal',extend='both')
    cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                        shrink=1, pad=0.08, aspect=40)
    cbar.set_label('℃ m$^{-1}$')
    # cb_ax.set_xticks(np.arange(-1,1.001,0.5))
    if p_values is not None:
        ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                    colors='none', levels=[0, 0.1],
                    hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    ax.set_extent([0, 360, 0, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[20,40,60,80], xlocs=[0, 60, 120, 179.9999, -60, -120],
                 draw_labels={"bottom": "x", "left": "y"},
                 linewidth=0.3, color='black', xpadding=3)
    ax.set_title(filename, fontdict={'size':9}, loc='left')
    ranges = [360-120, 360-65, 47, 66]
    # ranges = [200,223,72,78]
    ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]],
            [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]],linewidth=2, color='red', transform=ccrs.PlateCarree())

def plot_linregress_som3(fig,trend, filename, p_values=None):
    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
    central_longitude = 180
    ax = fig.add_subplot(2, 1, 1, projection=ccrs.PlateCarree(central_longitude=central_longitude))

    trend[trend == 0] = np.nan

    # trend[trend > 1.8] = 1.8
    import matplotlib.colors as colors

    if p_values is not None:
        p_values[np.abs(trend) < 0.01] = np.nan
    # trend[np.abs(trend) < 0.01] = np.nan

    colors_undersea = plt.cm.bwr(np.linspace(0, 0.5, 256))
    colors_land = plt.cm.bwr(np.linspace(0.7, 1, 256))
    all_colors = np.vstack((colors_undersea, colors_land))
    terrain_map = colors.LinearSegmentedColormap.from_list(
        'terrain_map', all_colors)

    divnorm = colors.TwoSlopeNorm(vmin=-0.6, vcenter=0, vmax=0.6)

    cb = ax.contourf(lon, lat, trend
                     , cmap='bwr_r', norm=divnorm,
                    levels=list(np.arange(-0.6,0,0.05))+list(np.arange(0,0.600001,0.05)),
                     extend='both',
                     transform=ccrs.PlateCarree())
    # cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    # cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal',extend='both')
    # cb_ax.set_xticks(np.arange(-1,1.001,0.5))
    cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                        shrink=1, pad=0.08, aspect=40)
    cbar.set_label('℃ m$^{-1}$')
    if p_values is not None:
        ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                    colors='none', levels=[0, 0.1],
                    hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    ax.set_extent([0, 360, 0, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    ax.gridlines(ylocs=[20,40,60,80], xlocs=[0, 60, 120, 179.9999, -60, -120],
                 draw_labels={"bottom": "x", "left": "y"},
                 linewidth=0.3, color='black', xpadding=3)
    ax.set_title(filename, fontdict={'size':9}, loc='left')

    ranges = [40, 145, 40, 61]
    # ranges = [200,223,72,78]
    ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]],
            [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]],linewidth=2, color='red',transform=ccrs.PlateCarree())


lon = np.arange(0,360,0.25)
lat = np.arange(90,-90.001,-0.25)

import matplotlib
SMALL_SIZE=7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

fig = plt.figure(figsize=[7,5])
plt.subplots_adjust(top=0.95,
bottom=0.02,
left=0.08,
right=0.95,
hspace=0.185,
wspace=0.14)
a = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\downsteam\t2m_3_som4")
plot_linregress_som3(fig, a[0], '(a) BAF', a[1])

a = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\downsteam\t2m_1_som6")
plot_linregress(fig, a[0], '(b) BSW', a[1])
# plt.show()
plt.savefig('disseration_figure5-2.png', dpi=400)
plt.show()


def save_standard_nc(data, var_name, times, lon=None, lat=None, ll=False):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360,0.25)
    if lat is None:
        lat = np.arange(90, -90.001, -0.25)

    ds = xr.Dataset()
    ds.coords['time'] = times
    if ll:
        ds.coords["latitude"] = lat
        ds.coords["longitude"] = lon

        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = (('time', "latitude", "longitude"), np.array(data[iI_vari]))
    else:
        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = ('time', np.array(data[iI_vari]))

    return ds
year = np.arange(1979, 2020+1)
def var_field(path_var, var_shortname, year, month):
    def get_yearmonth(var, var_year, var_month):
        wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
        wind_inmonth = wind_inYear.sel(
            time=np.in1d(wind_inYear['time.month'], var_month))
        return wind_inmonth

    data = xr.open_dataset(path_var)

    def reduce_expver(data, axis='dim'):
        result = np.nansum(data, axis=axis)
        result[np.all(np.isnan(data), axis=axis)] = np.nan
        return result

    # data = data.reduce(reduce_expver, 'expver')
    data = data.isel(expver=0)

    data = get_yearmonth(data, year, month)
    # mean
    data = data.assign_coords(year_month=data.time.dt.strftime("%Y"))
    data = data.groupby("year_month").mean("time")
    return data[var_shortname].values

def lingress(series, field, used_year):
    times = pd.date_range('%i-01-01'%year[0], '%i-01-01'%(year[-1]+1), freq='1Y')
    times = times[np.in1d(times.year, used_year)]
    series = save_standard_nc([series], ['var'], times, ll=False)
    field = save_standard_nc([field], ['var'], times, ll=True)

    def lag_linregress_3D(x, y, lagx=0, lagy=0):
        """
        Input: Two xr.Datarrays of any dimensions with the first dim being time.
        Thus the input data could be a 1D time series, or for example, have three
        dimensions (time, lat, lon).
        Datasets can be provided in any order, but note that the regression slope
        and intercept will be calculated for y with respect to x.
        Output: Covariance, correlation, regression slope and intercept, p-value,
        and standard error on regression between the two datasets along their
        aligned time dimension.
        Lag values can be assigned to either of the data, with lagx shifting x, and
        lagy shifting y, with the specified lag amount.
        """

        # 1. Ensure that the data are properly alinged to each other.
        x, y = xr.align(x, y)

        # 2. Add lag information if any, and shift the data accordingly
        if lagx != 0:
            # If x lags y by 1, x must be shifted 1 step backwards.
            # But as the 'zero-th' value is nonexistant, xr assigns it as invalid
            # (nan). Hence it needs to be dropped
            x = x.shift(time=-lagx).dropna(dim='time')

            # Next important step is to re-align the two datasets so that y adjusts
            # to the changed coordinates of x
            x, y = xr.align(x, y)

        if lagy != 0:
            y = y.shift(time=-lagy).dropna(dim='time')
            x, y = xr.align(x, y)

        # 3. Compute data length, mean and standard deviation along time axis:
        n = y.notnull().sum(dim='time')
        xmean = x.mean(dim='time')
        ymean = y.mean(dim='time')
        xstd = x.std(dim='time')
        ystd = y.std(dim='time')

        # 4. Compute covariance along time axis
        cov = ((x - xmean) * (y - ymean)).sum(dim='time') / n

        # 5. Compute correlation along time axis
        cor = cov / (xstd * ystd)

        # 6. Compute regression slope and intercept:
        slope = cov / (xstd ** 2)
        intercept = ymean - xmean * slope

        # 7. Compute P-value and standard error
        # Compute t-statistics
        tstats = cor * np.sqrt(n - 2) / np.sqrt(1 - cor ** 2)
        stderr = slope / tstats

        from scipy.stats import t

        pval = t.sf(np.abs(tstats['var'].values), n['var'].values - 2) * 2

        return cov['var'].values, cor['var'].values, slope['var'].values, intercept['var'].values, pval, stderr['var'].values

    cov, cor, slope, intercept, pval, stderr = lag_linregress_3D(series, field)
    return cov, cor, slope, intercept, pval, stderr

def plot_linregress(trend, p_values, filename):
    fig = plt.figure(figsize=[5, 3.9])
    plt.subplots_adjust(top=0.97,
                        bottom=0.1,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))


    # trend[np.isnan(trend)] = 0
    trend[trend == 0] = np.nan
    import matplotlib.colors as colors

    # X, Y, masked_drm = z_masked_overlap(
    #     ax, np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :],
    #     source_projection=ccrs.Geodetic())
    # cb = ax.contourf(X, Y, masked_drm, 51,
    #             cmap='seismic')
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  ,levels=[-0.1,-0.05,-0.025, -0.0001, 0.0001, 0.025,0.05,0.1], extend='both', cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    trend[np.abs(trend) < 1.e-3] = 0.
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())
    lon = np.arange(0, 360, 0.25)
    lat = np.arange(90, -90.01, -0.25)
    cb = ax.pcolormesh(lon, lat, trend
                     , cmap='bwr', norm=colors.CenteredNorm(),
                     transform=ccrs.PlateCarree())
    plt.colorbar(cb)
    ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                colors='none', levels=[0, 0.1],
                hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    plt.title(filename)
    # plt.show()
    plt.savefig(path_Save + '%s.png'%filename)
    plt.close()

path_Save = r'/home/linhaozhong/work/AR_NP85/downstream/regression/'
for i_era5Var in ['t2m', 'tp']:
    for i_som in range(1,6+1):
        os.makedirs(path_Save+'som%i/'%i_som, exist_ok=True)
        series_arSIH = nc4.read_nc4(r'/home/linhaozhong/work/AR_NP85/new_Clustering_paper_plot/yearlySum_type%i_esiv_pdf'%i_som)
        # series_arSIH = nc4.read_nc4(r'D:/yearlySum_type%i_esiv_pdf'%i_som)

        exclude_YearIndex = np.squeeze(np.argwhere(series_arSIH != 0))
        year_exclude = year[exclude_YearIndex]
        series_arSIH_excluede = series_arSIH[exclude_YearIndex]

        explore_month = list(range(1,12+1))
        explore_month = [[i] for i in explore_month]+[[3,4,5],[6,7,8]]

        for i_explore_month in explore_month:
            tp_field = var_field(r"/home/linhaozhong/work/Data/ERA5/monthly/t2mTP_1940_2023.nc",
                                 i_era5Var,
                                 year_exclude,
                                 i_explore_month
                                 )

            cov, cor, slope, intercept, pval, stderr = lingress(series_arSIH_excluede, tp_field, year_exclude)

            saveName = '/som%i/%s_%s_som%i'%(i_som, i_era5Var, ''.join([str(num) for num in i_explore_month]), i_som)
            plot_linregress(slope, pval, saveName)
            nc4.save_nc4(np.array([slope,pval, cor]), path_Save+r'%s'%saveName)





