import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io as scio
from aostools import climate
import xarray as xr
import datetime as dt
from datetime import timedelta
import netCDF4 as nc
import nc4
from scipy.signal import detrend
from scipy.stats import zscore

year = np.arange(1979, 2020+1)


aar_series_arSIH = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\new_Clustering_paper_plot\yearlySum_type%i_esiv_pdf'%1)
par_series_arSIH = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\new_Clustering_paper_plot\yearlySum_type%i_esiv_pdf'%6)

aar_series_arSIH[np.isnan(aar_series_arSIH)] = 0
par_series_arSIH[np.isnan(par_series_arSIH)] = 0

fig, axs = plt.subplots(2,1)
plt.subplots_adjust(top=0.88,
bottom=0.11,
left=0.125,
right=0.9,
hspace=0.51,
wspace=0.215)

x, y = [year, aar_series_arSIH]
axs[0].plot(x,y, color='black', marker='.',markersize=5)
axs[0].set_title('(a) BAF', loc='left')

x, y = [year, par_series_arSIH]
axs[1].plot(x, y, color='black', marker='.',markersize=5)
axs[1].set_title('(b) BSW', loc='left')

for ax in axs:
    ax.set_xlabel('year')
    ax.set_ylabel('cm')

plt.savefig(r'dissertation_figure5-1.png', dpi=400)
plt.show()
# print(list(np.arange(16,41, 4)))
# if len(var_all) > 2:
#     ax.set_xticks([4,12,  20, 28, 36])
#     ax.set_xticklabels(['-2d','Onset','+2d','+4d', '+6d'])