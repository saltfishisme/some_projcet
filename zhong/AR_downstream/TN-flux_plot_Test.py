import xarray as xr
import numpy as np
import scipy.io as scio
import matplotlib.pyplot as plt
# """
# create nc for analysis from ERA5
# u,v,w, t and climate ones.
# """
#
# dates_all = ['19800102']
# path_pressure = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
#
# for i_date in dates_all:
#     # read u, v, w, hgt, air and averaged into daily scale.
#     u = xr.open_dataset(path_pressure + 'Uwnd/' + i_date[:4] + '/Uwnd.%s.nc' % i_date).mean(dim=['time'])
#     v = xr.open_dataset(path_pressure + 'Vwnd/' + i_date[:4] + '/Vwnd.%s.nc' % i_date).mean(dim=['time'])
#     w = xr.open_dataset(path_pressure + 'Wwnd/' + i_date[:4] + '/Wwnd.%s.nc' % i_date).mean(dim=['time'])
#     air = xr.open_dataset(path_pressure + 'Tem/' + i_date[:4] + '/Tem.%s.nc' % i_date).mean(dim=['time'])
#     hgt = \
#     xr.open_dataset(path_pressure + 'Hgt/' + i_date[:4] + '/Hgt.%s.nc' % i_date).mean(dim=['time'])
#     print(hgt)
#     data = xr.merge([u, v, w, air, hgt])
#
#     # get anomaly for this date.
#
#     var_shortName = ['u', 'v', 'w', 't', 'z']
#     var_Name = dict(zip(var_shortName, ['Uwnd', 'Vwnd', 'Wwnd', 'Tem', 'Hgt']))
#     for i_varShort in var_shortName:
#         var = scio.loadmat(path_pressure + '%s/anomaly_levels/%s.mat' % (var_Name[i_varShort],i_date[4:]))
#         var = np.nanmean(var[i_date[4:]], axis=1)
#         data[i_varShort+'_cli'] = (("level", "latitude", "longitude"), np.array(var))
#
#     data.to_netcdf(r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/tnFlux_test.nc')
#
# exit()

"""
cal tn
"""
# import aostools
# from aostools import climate
# from aostools import constants
# import xarray as xr
#
#
# # print(dir(aostools))
# # print(help(aostools.climate))
# data = xr.open_dataset(r'G:/uvz300_1979-2017_Jan.nc')
# print(data)
#
# wx,wy,div = climate.ComputeWaveActivityFlux(phi_or_u=data.u.isel(time=2),
#                                                  phiref_or_v=data.v.isel(time=2),
#                                                  uref=data.u.mean(dim='time'),
#                                                  vref=data.v.mean(dim='time'),
#                                      lat='latitude',
#                                      lon='longitude',
#                                     pres='',
#                                      qg=False)
#
# plot1=wx.values
#
# phi_or_u = data.z.isel(time=2)
# phiref_or_v=data.z.mean(dim='time')
# lat='latitude'
# lon='longitude'
#
# a0 = 6376.0e3 # [m]
# Omega = 7.292e-5 #[1/s]
# p0 = 1.e3
# rad2deg = 180 / np.pi
# radlat = np.deg2rad(phi_or_u[lat])
# coslat = np.cos(radlat)
# one_over_coslat2 = coslat ** (-2)
# one_over_a2 = a0 ** (-2)
# one_over_acoslat = (a0 * coslat) ** (-1)
# f = 2 * Omega * np.sin(radlat)
#
# psi = (phi_or_u - phiref_or_v) / f  # [m2/s]
#
#
# uref = -(phiref_or_v / f).differentiate(lat, edge_order=2).reduce(np.nan_to_num)
# uref = uref / a0
# vref = (phiref_or_v / f).differentiate(lon, edge_order=2).reduce(np.nan_to_num)
# vref = vref * one_over_acoslat
#
#
# # wave activity flux only valid for westerlies.
# #  it is common practice to also mask weak westerlies,
# #  with a value of 1m/s often seen
# mask = uref > 1.0
# mag_u = np.sqrt(uref ** 2 + vref ** 2)
# # psi.differentiate(lon) == np.gradient(psi)/np.gradient(lon) [psi/lon]
# # this (with qg=True,use_windspharm=False) has been checked against Fig (a) at
# #   http://www.atmos.rcast.u-tokyo.ac.jp/nishii/programs/
# dpsi_dlon = psi.differentiate(lon, edge_order=2).reduce(np.nan_to_num)  # [m2/s/deg]
# dpsi_dlat = psi.differentiate(lat, edge_order=2).reduce(np.nan_to_num)  # [m2/s/deg]
# d2psi_dlon2 = dpsi_dlon.differentiate(lon, edge_order=2)  # [dpsi_dlon/lon] = [m2/s/deg2]
# d2psi_dlat2 = dpsi_dlat.differentiate(lat, edge_order=2)  # [m2/s/deg2]
# d2psi_dlon_dlat = dpsi_dlon.differentiate(lat, edge_order=2)  # [m2/s/deg2]
#
# termxu=dpsi_dlon*dpsi_dlon-psi*d2psi_dlon2
# termxv=dpsi_dlon*dpsi_dlat-psi*d2psi_dlon_dlat
# termyv=dpsi_dlat*dpsi_dlat-psi*d2psi_dlat2
#
# wx = uref * one_over_coslat2 * one_over_a2 * termxu \
#      + vref * one_over_a2 / coslat * termxv
#
# # [m/s*1/m2*m4/s2/deg2] = [m3/s3/deg2]
# wy = uref * one_over_a2 / coslat * termxv \
#      + vref * one_over_a2 * termyv
# # [m3/s3/deg2]
#
# # scaling: In TN01, Eq (38) uses spherical coordinates with z in vertical.
# #  the scaling factor is then pref/p0*coslat/2/mag_u.
# #  Eq (C5) shows the same in pressure coordinates, but with (x,y) in the
# #  horizontal. The scaling factor is then 1/2/mag_u.
# # By testing, I found that the latter is consistent with the horizontal EP flux,
# #  i.e. wy.mean('lon') == ep1 if uref = u.mean('lon') if scaling factor = 1/2/mag_u.
# #  Therefore, I use this as scaling factor. Note that this will then cause a
# #   difference compared to Fig (a) at
# #   http://www.atmos.rcast.u-tokyo.ac.jp/nishii/programs/
#
# coeff = rad2deg ** 2 / 2 / mag_u  # coefficient for p-coords, [deg2*s/m]
#
# # get the vectors in physical units of m2/s2, correcting for radians vs. degrees
# wx = coeff * wx  # [deg2*s/m*m3/s3/deg2] = [m2/s2]
# wy = coeff * wy
# plot1 = uref.values
#
#
#
#
#
#
# import numpy as np
# import netCDF4 as nc
# import matplotlib.pyplot as plt
# from copy import copy
#
# a=6.37e6 #Earth Radius
# omega=7.292e-5 #Rotational angular velocity of the Earth
#
# #Read in data
# climate_data = nc.Dataset('G:/uvz300_1979-2017_Jan.nc')
# print(climate_data)
# lon=climate_data.variables['longitude'][:]
# lat=climate_data.variables['latitude'][:]
#
# dlon=np.gradient(lon)*np.pi/180.0
# dlat=np.gradient(lat)*np.pi/180.0
# f=np.array(list(map(lambda x : 2*omega*np.sin(x*np.pi/180.0),lat))) #Coriolis parameter: f=2*omgega*sin(lat)
#
# cos_lat=np.array(list(map(lambda x : np.cos(x*np.pi/180.0),lat))) #cos(lat)
#
# u_c=np.average(climate_data.variables['u'][:,:,:],axis=0)
# v_c=np.average(climate_data.variables['v'][:,:,:],axis=0)
# phi_c=np.average(climate_data.variables['z'][:,:,:],axis=0)
# psi_p=((climate_data.variables['z'][2,:,:]-phi_c).T/f).T #Pertubation stream-function
# dpsi_dlon=np.gradient(psi_p,dlon[1])[1]
# dpsi_dlat=np.gradient(psi_p,dlat[1])[0]
# d2psi_dlon2=np.gradient(dpsi_dlon,dlon[1])[1]
# d2psi_dlat2=np.gradient(dpsi_dlat,dlat[1])[0]
# d2psi_dlondlat=np.gradient(dpsi_dlat,dlon[1])[1]
#
# termxu=dpsi_dlon*dpsi_dlon-psi_p*d2psi_dlon2
# termxv=dpsi_dlon*dpsi_dlat-psi_p*d2psi_dlondlat
# termyv=dpsi_dlat*dpsi_dlat-psi_p*d2psi_dlat2
#
# #coefficient
# p_lev=300.0 #unit in hPa
# p=p_lev/1000.0
# magU=np.sqrt(u_c**2+v_c**2)
# coeff=((p*cos_lat)/(2*magU.T)).T
# #x-component of TN-WAF
# px=(coeff.T/(a*a*cos_lat)).T * (((u_c.T)/cos_lat).T*termxu+v_c*termxv)
# #y-component of TN-WAF
# py=(coeff.T/(a*a)).T * (((u_c.T)/cos_lat).T*termxv+v_c*termyv)
#
# plot2 = px
#
#
# import cartopy.crs as ccrs
# """
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# """
#
# lon = data.longitude.values
# lat= data.latitude.values
# lon, lat = np.meshgrid(lon, lat)
# spacewind = [20, 20]
# fig, axs = plt.subplots(ncols=2,subplot_kw={'projection':ccrs.NorthPolarStereo()})
#
# ax = axs[0]
# # ax.contour(lon, lat,
# #            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
# #            plot1,colors='black',
# #            transform=ccrs.PlateCarree(), linewidths=1)
# cb = ax.pcolormesh(lon[:180], lat[:180],
#            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
#            plot1[:180],cmap='RdBu_r',
#            transform=ccrs.PlateCarree())
# plt.colorbar(cb, ax=ax)
# ax.set_extent([0,359,20,89], crs=ccrs.PlateCarree())
# ax.coastlines()
#
# ax = axs[1]
# # ax.contour(lon, lat,
# #            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
# #            plot2,colors='black',
# #            transform=ccrs.PlateCarree())
# cb = ax.pcolormesh(lon[:180], lat[:180],
#            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
#            plot2[:180],cmap='RdBu_r',
#            transform=ccrs.PlateCarree())
# plt.colorbar(cb, ax=ax)
# ax.set_extent([0,359,20,89], crs=ccrs.PlateCarree())
# ax.coastlines()
# plt.show()
# exit()
import cartopy.crs as ccrs

# data = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnComposite_time-0_6.nc")
# data2 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnComposite_time-end_6.nc")
# data = data.sel(level=300, lat=slice(90,20))
# data2 = data2.sel(level=300, lat=slice(90,20))
# lon = data.lon.values
# lat= data.lat.values
# # wx = data.px.values
# # wy = data.py.values
# # psi = data.psi.values
# wx = data2.px.values
# wy = data2.py.values
# psi = data2.psi.values
# def flatten_ep(da):
#     lower_quantile = np.quantile(da, 0.1)
#     upper_quantile = np.quantile(da, 0.90)
#     da[da <= upper_quantile] = upper_quantile
#     da[da >= lower_quantile] = lower_quantile
#     return da
# from scipy.ndimage import gaussian_filter
# wx = gaussian_filter(wx, sigma=2)
# wy = gaussian_filter(wy, sigma=2)
# lon, lat = np.meshgrid(lon, lat)
# spacewind = [12, 8]
# fig = plt.figure(figsize=[4, 3])
# plt.subplots_adjust(top=0.91,
#                     bottom=0.15,
#                     left=0.005,
#                     right=0.995,
#                     hspace=0.14,
#                     wspace=0.055)
# # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
# ax = fig.add_subplot(1, 1, 1, projection=ccrs.EquidistantConic(central_longitude=50, central_latitude=60))
# # ax.contour(lon, lat,
# #            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
# #            psi, levels=np.arange(-24, 28, 4),colors='black',
# #            transform=ccrs.PlateCarree(), linewidths=1)
# cb = ax.contourf(lon, lat,
#            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
#            psi/1e6, levels=np.arange(-4, 4.01, 0.5),cmap='RdBu_r',extend='both',
#            transform=ccrs.PlateCarree(), linewidths=1)
# cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
# cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
#
# print(np.nanmax(psi), np.nanmin(psi))
# qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
#                 wx[::spacewind[1], ::spacewind[0]],
#                 wy[::spacewind[1], ::spacewind[0]]
#                 , transform=ccrs.PlateCarree(),width=0.0025,scale=1000,headwidth=2)
# #
#
# qk = ax.quiverkey(qui, 0.6, 1.05, 20, r'$\mathrm{20\,kg\,m^{-1}s^{-1}}$', labelpos='E',
#                   coordinates='axes', color='black', labelsep=0.01)
# ax.set_extent([340, 50,20,80], crs=ccrs.PlateCarree())
# ax.coastlines()
# plt.show()

data = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/tnComposite_time-0_6.nc")
data2 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/tnComposite_time-end_6.nc")
data = data.sel(level=300, lat=slice(90,0))
data2 = data2.sel(level=300, lat=slice(90,0))
lon = data.lon.values
lat= data.lat.values
# wx = data.px.values
# wy = data.py.values
# psi = data.psi.values
wx = data2.px.values
wy = data2.py.values
psi = data.psi.values
def flatten_ep(da):
    lower_quantile = np.quantile(da, 0.1)
    upper_quantile = np.quantile(da, 0.90)
    da[da <= upper_quantile] = upper_quantile
    da[da >= lower_quantile] = lower_quantile
    return da
from scipy.ndimage import gaussian_filter
wx = gaussian_filter(wx, sigma=2)
wy = gaussian_filter(wy, sigma=2)
lon, lat = np.meshgrid(lon, lat)
spacewind = [8, 4]
fig = plt.figure(figsize=[4, 5])
plt.subplots_adjust(top=0.91,
                    bottom=0.15,
                    left=0.005,
                    right=0.995,
                    hspace=0.14,
                    wspace=0.055)
# ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))
# ax.contour(lon, lat,
#            # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
#            psi, levels=np.arange(-24, 28, 4),colors='black',
#            transform=ccrs.PlateCarree(), linewidths=1)
cb = ax.contourf(lon, lat,
           # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
           psi/1e6, levels=np.arange(-8, 8.01, 1),cmap='RdBu_r',extend='both',
           transform=ccrs.PlateCarree(), linewidths=1)
cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')

print(np.nanmax(psi), np.nanmin(psi))
qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                wx[::spacewind[1], ::spacewind[0]],
                wy[::spacewind[1], ::spacewind[0]]
                , transform=ccrs.PlateCarree(),width=0.0025,scale=1200,headwidth=2)
#

qk = ax.quiverkey(qui, 0.6, 1.05, 20, r'$\mathrm{20\,kg\,m^{-1}s^{-1}}$', labelpos='E',
                  coordinates='axes', color='black', labelsep=0.01)
ax.set_extent([0, 320,20,89], crs=ccrs.PlateCarree())
ax.coastlines()
plt.show()