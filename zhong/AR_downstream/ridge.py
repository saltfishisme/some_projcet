import os
import pandas as pd
import scipy.io as scio
import nc4
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr


# def save_standard_nc(data, var_name, levels, lon=None, lat=None):
#     """
#     :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
#     :param var_name: ['t2m', 'ivt', 'uivt']
#     :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
#     :param lon: list, 720
#     :param lat: list, 360
#     :param path_save: r'D://'
#     :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
#     :return:
#     """
#     import xarray as xr
#     import numpy as np
#
#     if lon is None:
#         lon = np.arange(0, 360)
#     if lat is None:
#         lat = np.arange(90, -91, -1)
#
#     ds = xr.Dataset()
#     ds.coords["latitude"] = lat
#     ds.coords["longitude"] = lon
#     ds.coords['level'] = levels
#
#     for iI_vari, ivari in enumerate(var_name):
#         ds[ivari] = (('level', "latitude", "longitude"), np.array(data[iI_vari]))
#
#     return ds
#
#
# def get_circulation_data_based_on_pressure_date(date, var, var_ShortName):
#     """date containing Hour information
#     type: daily/monthly/monthly_now
#     """
#     import scipy
#     time = pd.to_datetime(date)
#
#     # load hourly anomaly data
#     def load_anomaly():
#         data_anomaly = \
#             scipy.io.loadmat(
#                 path_PressureData + '%s/anomaly_levels/' % var + '%s.mat' % time.strftime('%m%d'))[
#                 time.strftime('%m%d')][21, int(time.strftime('%H')), :, :]
#         return data_anomaly
#
#     data_anomaly = load_anomaly()
#
#     # load present data
#     data_present = xr.open_dataset(
#         path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
#             var, time.strftime('%Y%m%d')))
#
#     data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=500)[
#         var_ShortName].values
#
#     return data_present / 9.8, data_anomaly / 9.8
#     # return data_present
#
#
# def contour_cal(time_file):
#     info_AR_row = xr.open_dataset(path_contour + time_file[:-1])
#
#     time_AR = info_AR_row.time_AR.values
#     time_all = info_AR_row.time_all.values
#     AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#
#     data = []
#     anomaly = []
#     for iI_timeall, i_timeall in enumerate(time_all[AR_loc:]):
#         i_time = pd.to_datetime(i_timeall)
#         i_time = i_time + pd.DateOffset(months=add_month)
#         i_data, i_anomaly = get_circulation_data_based_on_pressure_date(i_time, 'Hgt', 'z')
#
#         data.append(i_data)
#         anomaly.append(i_anomaly)
#
#     nc4.save_nc4(np.array(data), path_SaveData + time_file[:-4] + '_%iM' % add_month)
#     nc4.save_nc4(np.array(anomaly), path_SaveData + time_file[:-4] + '_cli_%iM' % add_month)
#
#
# path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
# resolution = 0.5
# path_SaveData = r'/home/linhaozhong/work/AR_NP85/hpa500/'
# os.makedirs(path_SaveData, exist_ok=True)
#
# add_month = 0
# i_type = 6
#
# file = file_all[save_labels_bmus == i_type]
# for i_file in file:
#     contour_cal(i_file)
#
# #################################
# add_month = 1
# i_type = 1
#
# file = file_all[save_labels_bmus == i_type]
# for i_file in file:
#     contour_cal(i_file)
# exit()
"""
ridge
"""
# save_labels_bmus = scio.loadmat(r"G:\OneDrive\a_matlab\a_test\test1979_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\SOM\centroid_deinf_1979.mat")[
#     'filename']
# path_SaveData = r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\hpa500/'
#
# ridge_range = [5400 - 50, 5400 + 50]
#
# i_type = 6
# file = file_all[save_labels_bmus == i_type]
# # data = np.empty([len(file), 361,720])
# data = []
# anomaly = []
# for i_file in file:
#     # aa = contour_cal(i_file)
#     a = nc4.read_nc4(path_SaveData + i_file[:-4]+'_0M')
#     b = nc4.read_nc4(path_SaveData + i_file[:-4] + '_cli_0M')
#
#
#     def get_ridge(hgt):
#         hgt[(hgt < ridge_range[0]) | (hgt > ridge_range[1])] = np.nan
#         return hgt
#
#
#     data.extend(get_ridge(a))
#     anomaly.extend(get_ridge(b))
# data = np.array(data)
# anomaly = np.array(anomaly)
#
# data[~np.isnan(data)] = 1
# anomaly[~np.isnan(anomaly)] = 1
#
# data = np.nansum(data, axis=0)
# anomaly = np.nansum(anomaly, axis=0)
#
# nc4.save_nc4(np.array(data-anomaly), 'ridge')

import cartopy.crs as ccrs
def plot_linregress(ax, trend, filename, p_values=None):

    import matplotlib.colors as colors

    if p_values is not None:
        p_values[np.abs(trend) < 0.01] = np.nan

    import cmaps
    cb = ax.contourf(lon, lat, trend
                     , cmap=cmaps.WhiteYellowOrangeRed,
                     levels=list(np.arange(0, 320, 40)),
                     extend='both',
                     transform=ccrs.PlateCarree())

    if p_values is not None:
        ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                    colors='none', levels=[0, 0.1],
                    hatches=[1 * '/', 1 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    ax.set_extent([120, 320, 0, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.3, zorder=10)
    cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                        shrink=0.6, pad=0.08, aspect=40)
    cbar.set_label('times')

    ax.gridlines(ylocs=[20,40,60,80], xlocs=[0, 60, 120, 179.9999, -60, -120],
                 draw_labels={"bottom": "x", "left": "y"},
                 linewidth=0.3, color='black', xpadding=3)
    ax.set_extent([60,320,10,90],crs=ccrs.PlateCarree())
    # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    ax.set_title(filename, loc='left')
#
"""
plot
"""
SMALL_SIZE=7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

fig = plt.figure(figsize=[7,6])
plt.subplots_adjust(top=0.95,
bottom=0.02,
left=0.08,
right=0.95,
hspace=0.185,
wspace=0.14)
ax = fig.add_subplot(3, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

lon = np.arange(0, 360, 0.5)
lat = np.arange(90, -90.001, -0.5)
plot_linregress(ax, nc4.read_nc4('ridge'), '(a)')
ranges = [360 - 120, 360 - 65, 47, 66]
ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]],
        [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]], linewidth=1, color='red', transform=ccrs.PlateCarree())


data = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/tnComposite_time-begin_6.nc")
data2 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/tnComposite_time-end_6.nc")
data = data.sel(level=500, lat=slice(90,0))
data2 = data2.sel(level=500, lat=slice(90,0))
lon = data.lon.values
lat= data.lat.values

wx = data2.px.values
wy = data2.py.values
psi = data.psi.values

from scipy.stats import ttest_1samp
_,p_psi = ttest_1samp(psi/1e6, 0, axis=0)

wx = np.nanmean(wx, axis=0)
wy = np.nanmean(wy, axis=0)
psi = np.nanmean(psi, axis=0)

from scipy.ndimage import gaussian_filter
wx = gaussian_filter(wx, sigma=2)
wy = gaussian_filter(wy, sigma=2)


ax = fig.add_subplot(3, 1, 2, projection=ccrs.PlateCarree(central_longitude=180))
lon, lat = np.meshgrid(lon, lat)
spacewind = [10, 5]

ax.contour(lon, lat,
           psi/1e6, levels=np.arange(-54, 56, 2),colors='black',linewidth=1,
           transform=ccrs.PlateCarree())
cb = ax.contourf(lon, lat,
           np.ma.masked_where(p_psi>0.1, psi/1e6), levels=np.arange(-8, 8.01, 1),cmap='RdBu_r',extend='both',
           transform=ccrs.PlateCarree())
cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                    shrink=0.6, pad=0.08, aspect=40)
cbar.set_label('10$^{6}$m$^{2}$ s$^{-1}$')



qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                wx[::spacewind[1], ::spacewind[0]],
                wy[::spacewind[1], ::spacewind[0]]
                , transform=ccrs.PlateCarree(), scale=2000, headwidth=7, width=0.0009, headlength=5)

qk = ax.quiverkey(qui, 0.9, 1.05, 40, r'$\mathrm{40\,kg\,m^{-1}s^{-1}}$', labelpos='E',
                  coordinates='axes', color='black', labelsep=0.01)

ax.gridlines(ylocs=[20, 40, 60, 80], xlocs=[0, 60, 120, 179.9999, -60, -120],
             draw_labels={"bottom": "x", "left": "y"},
             linewidth=0.3, color='black', xpadding=3)
ax.set_extent([60, 320, 10, 90], crs=ccrs.PlateCarree())
ax.coastlines()
ranges = [360 - 120, 360 - 65, 47, 66]
ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]],
        [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]], linewidth=1, color='red', transform=ccrs.PlateCarree())
ax.set_title('(b)', loc='left')



data = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/tnComposite_time-begin_6.nc")
data2 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/tnComposite_time-end_6.nc")
data = data.sel(level=500, lat=slice(90,0))
data2 = data2.sel(level=500, lat=slice(90,0))
lon = data.lon.values
lat= data.lat.values

wx = data2.px.values-data.px.values
wy = data2.py.values-data.py.values
psi = data.psi.values

from scipy.stats import ttest_1samp
_,p_wx = ttest_1samp(wx, 0, axis=0)
_,p_wy = ttest_1samp(wy, 0, axis=0)
_,p_psi = ttest_1samp(psi, 0, axis=0)

wx = np.nanmean(wx, axis=0)
wy = np.nanmean(wy, axis=0)
psi = np.nanmean(psi, axis=0)

from scipy.ndimage import gaussian_filter
wx = gaussian_filter(wx, sigma=2)
wy = gaussian_filter(wy, sigma=2)


ax = fig.add_subplot(3, 1, 3, projection=ccrs.EquidistantConic(central_longitude=220,
                                                               central_latitude=60))
lon, lat = np.meshgrid(lon, lat)

cb = ax.contourf(lon, lat,
           np.ma.masked_where(p_psi>0.1, psi/1e6), levels=np.arange(-8, 8.01, 1),cmap='RdBu_r',extend='both',
           transform=ccrs.PlateCarree())
cb_ax = fig.add_axes([0.75, 0.05, 0.01, 0.2])
cbar = fig.colorbar(cb, cax=cb_ax, extend='both')
# cbar = fig.colorbar(cb, ax=ax, extend='both',
#                     shrink=0.8, pad=0.01)
cbar.set_label('10$^{6}$m$^{2}$ s$^{-1}$')

spacewind = [8, 4]
qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                wx[::spacewind[1], ::spacewind[0]],
                wy[::spacewind[1], ::spacewind[0]]
                , transform=ccrs.PlateCarree(),
                scale=150)
qk = ax.quiverkey(qui, 0.9, 1.05, 5, r'$\mathrm{5\,kg\,m^{-1}s^{-1}}$', labelpos='E',
                  coordinates='axes', color='black', labelsep=0.01)

p_wvf = (p_wx> 0.1) & (p_wy > 0.1)
wx = np.ma.masked_where(p_wvf, wx)
wy = np.ma.masked_where(p_wvf, wy)
spacewind = [8, 4]
qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                wx[::spacewind[1], ::spacewind[0]],
                wy[::spacewind[1], ::spacewind[0]]
                , transform=ccrs.PlateCarree(),color='red',
                scale=150)

from function_shared_Main import add_longitude_latitude_labels_givenloc
xlocs = [['180°', 179.99, 50], ['60°W', -60, 50],]

add_longitude_latitude_labels_givenloc(fig, ax, xloc=xlocs, yloc=180)
# ax.gridlines(ylocs=[66], xlocs=[179.9999, -120],
#              draw_labels={"bottom": "x", "left": "y"},
#              linewidth=0.3, color='black', xpadding=3)
ax.set_extent([180, 320, 40, 80], crs=ccrs.PlateCarree())
ax.coastlines(linewidth=0.1)
ax.set_title('(c)', loc='left')
ranges = [360 - 120, 360 - 65, 47, 66]
ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]],
        [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]], linewidth=1, color='red', transform=ccrs.PlateCarree())
plt.savefig('disseration_figure5-5.png', dpi=400)
plt.show()



exit()

"""

"""
# for iI, i_type in enumerate(np.unique(save_labels_bmus)):
#     if i_type == 0:
#         continue
#     file = file_all[save_labels_bmus == i_type]
#     cs_lon = []
#     cs_lat = []
#     for i_file in file:
#         # aa = contour_cal(i_file)
#
#         save_path = path_SaveData + i_file[:-1] + '/'
#         cross_section_AR = os.listdir(save_path)
#         for i_cs in os.listdir(save_path):
#
#             if '_lon' in i_cs:
#                 a = xr.open_dataset(save_path + i_cs)
#                 cs_lon.append([a['Hgt'].values, a['RH'].values])
#
#             if '_lat' in i_cs:
#                 a = xr.open_dataset(save_path + i_cs)
#                 cs_lat.append([a['Hgt'].values, a['RH'].values])
#     nc4.save_nc4(np.nanmean(cs_lon, axis=0), path_SaveData + '%i_lon' % i_type)
#     nc4.save_nc4(np.nanmean(cs_lat, axis=0), path_SaveData + '%i_lat' % i_type)
#
# exit()
