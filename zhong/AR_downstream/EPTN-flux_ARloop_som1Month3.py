import os
import numpy as np
import pandas as pd
import scipy.io as scio
from aostools import climate
import xarray as xr
import datetime as dt
from datetime import timedelta
import netCDF4 as nc
import nc4
from scipy.signal import detrend
from scipy.stats import zscore

def slice_xarray_longitude(ds, lon_slice):
    """
    :param lon_slice: list with slice object, such as [slice(340,360),slice(0,60)]
    :return: recheck if the rank of lon is correct!
    """
    if isinstance(lon_slice, list):
        ds_collect = []
        for i_lonslice in lon_slice:
            ds_collect.append(ds.sel(longitude=i_lonslice))
        return xr.merge(ds_collect)
    else:
        return ds.sel(longitude=lon_slice)

lon_slice = slice(50,140)
year = np.arange(1979, 2020+1)

for i_type in [4]:
    # series_arSIH = nc4.read_nc4(r'/home/linhaozhong/work/AR_NP85/new_Clustering_paper_plot/yearlySum_type%i_esiv_pdf'%i_som)
    series_arSIH = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\new_Clustering_paper_plot\yearlySum_type%i_esiv_pdf'%i_type)

    indices = np.argpartition(series_arSIH, 10)[:10]
    year_AR = year[indices]
    print(list(year_AR))
    exit()
    # indices = np.argpartition(series_arSIH, -10)[-10:]
    # year_ARlow = year[indices]

    data = xr.open_dataset(r"G:\OneDrive\basis\data_2024\ERA5\monthly/uvzt_SpecLevel_NH_NDJFM_1979-2020.nc")

    data = data.sel(time=np.in1d(data['time.month'], [3]))
    data = slice_xarray_longitude(data, lon_slice)

    ####
    era_u_v_t = data.sel(time=np.in1d(data['time.year'], year))
    f1, f2, div1, div2 = climate.ComputeEPfluxDivXr(
        u=era_u_v_t.u, v=era_u_v_t.v, t=era_u_v_t.t,
        lon='longitude', lat='latitude', pres='level', time='time')
    ds_cli = xr.merge([f1, f2, div1, div2])

    ####
    era_u_v_t = data.sel(time=np.in1d(data['time.year'], year_AR))
    f1, f2, div1, div2 = climate.ComputeEPfluxDivXr(
        u=era_u_v_t.u, v=era_u_v_t.v, t=era_u_v_t.t,
        lon='longitude', lat='latitude', pres='level', time='time')
    ds = xr.merge([f1, f2, div1, div2])

    from scipy.stats import ttest_ind,ttest_1samp
    print(ds)
    # _,p1 = ttest_ind(ds.ep1.values, ds_cli.ep1.values, axis=0)
    # _,p2 = ttest_ind(ds.ep2.values, ds_cli.ep2.values, axis=0)
    # _, p3 = ttest_ind(ds.div1.values, ds_cli.div1.values, axis=0)
    # _, p4 = ttest_ind(ds.div2.values, ds_cli.div2.values, axis=0)
    _,p1 = ttest_1samp(ds.ep1.values, 0, axis=0)
    _,p2 = ttest_1samp(ds.ep2.values, 0, axis=0)
    _, p3 = ttest_1samp(ds.div1.values, 0, axis=0)
    _, p4 = ttest_1samp(ds.div2.values, 0, axis=0)
    import matplotlib.pyplot as plt

    p = (p1>0.1)&(p2>0.1)&(p3>0.1)&(p4>0.1)

    era_u_v_t = ds.mean(dim='time')
    era_u_v_t2 = ds_cli.mean(dim='time')

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    # plt.colorbar(cb)

    from matplotlib import colors
    import cmaps
    cb = ax.contourf(era_u_v_t.latitude, era_u_v_t.level, era_u_v_t.div1 - era_u_v_t2.div1,cmap=cmaps.cmp_b2r,
                     norm=colors.CenteredNorm(), levels=np.arange(-1.5,1.5001,0.1), extend='both')
    cbar = plt.colorbar(cb)
    cbar.set_label('m s$^{-1}$ d$^{-1}$')

    climate.PlotEPfluxArrows(x=era_u_v_t.latitude, y=era_u_v_t.level,
                             ep1=era_u_v_t.ep1 - era_u_v_t2.ep1,
                             ep2=era_u_v_t.ep2 - era_u_v_t2.ep2,
                             fig=fig, ax=ax, yscale='log', xlim=[20, 90], p=p)

    # ax.contourf(era_u_v_t.latitude, era_u_v_t.level,
    #             np.ma.masked_greater(p, 0.1),
    #                 colors='none',levels=[0,0.11],
    #                 hatches=[3*'.',3*'.'], alpha=0, zorder=20)

    ax.set_ylabel('hPa')
    ax.set_xticklabels(['%i°N'%i for i in [20, 30, 40, 50, 60, 70, 80, 90,]])
    # plt.show()
    plt.savefig('dissertation_figure5-8.png', dpi=400)
    plt.show()
    exit()



"""
tn
"""
from scipy.ndimage import gaussian_filter
from scipy import stats
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

central_lonlat = [100,60]
spacewind = [8, 4]

SMALL_SIZE=7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

fig = plt.figure(figsize=[7,4.2])
plt.subplots_adjust(top=0.95,
bottom=0.02,
left=0.08,
right=0.95,
hspace=0.185,
wspace=0.14)

param= {'t':[1000,1,np.arange(-2, 2.001, 0.1), '℃'],
        'z':[500,2,np.arange(-44, 44.01, 2),'gpm']}
# level, ax, contourf range
year = np.arange(1979, 2020 + 1)
i_type = 4

for t_or_hgt in ['t', 'z']:

    series_arSIH = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\new_Clustering_paper_plot\yearlySum_type%i_esiv_pdf'%i_type)

    indices = np.argpartition(series_arSIH, 10)[:10]
    indices_low = np.argpartition(series_arSIH, 10)[-10:]
    year_AR = year[indices]

    data = xr.open_dataset(r"G:\OneDrive\basis\data_2024\ERA5\monthly/uvzt_SpecLevel_NH_NDJFM_1979-2020.nc")

    var_shortName = ['u', 'v', 'z']

    data = data.sel(time=np.in1d(data['time.month'], [1, 2, 3]))
    data = data.sel(level=param[t_or_hgt][0])

    data_cli = data.assign_coords(year_month=data.time.dt.strftime("%m"))
    data_cli = data_cli.groupby("year_month").mean("time")

    def create_epflux_calculate_data(singleyear):
        ds = xr.Dataset()
        ds.coords["lat"] = data.latitude
        ds.coords["lon"] = data.longitude
        ds.coords["time"] = np.array([1,2,3])


        for iI_varShort, i_varShort in enumerate(var_shortName):
            ds[i_varShort] = (("time", "lat", "lon"),
                              data[i_varShort].sel(time=np.in1d(data['time.year'], singleyear)).values)
            ds[i_varShort + '_cli'] = (("time", "lat", "lon"),  data_cli[i_varShort].values)
        # ds.to_netcdf(r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/eptnFlux_test.nc')
        return ds

    def tn_flux(climate_datas):
        a = 6.37e6  # Earth Radius
        omega = 7.292e-5  # Rotational angular velocity of the Earth

        lon = climate_datas.lon.values
        lat = climate_datas.lat.values

        dlon = np.gradient(lon) * np.pi / 180.0
        dlat = np.gradient(lat) * np.pi / 180.0
        f = np.array(
            list(map(lambda x: 2 * omega * np.sin(x * np.pi / 180.0), lat)))  # Coriolis parameter: f=2*omgega*sin(lat)

        cos_lat = np.array(list(map(lambda x: np.cos(x * np.pi / 180.0), lat)))  # cos(lat)

        pxpypsi = []
        for iI_climate_data in range(len(climate_datas.time.values)):
            climate_data = climate_datas.isel(time=iI_climate_data)
            u_c = climate_data.variables['u_cli'].values
            v_c = climate_data.variables['v_cli'].values
            phi_c = climate_data.variables['z_cli'].values
            psi_p = ((climate_data.variables['z'].values - phi_c).T / f).T  # Pertubation stream-function
            dpsi_dlon = np.gradient(psi_p, dlon[1])[1]
            dpsi_dlat = np.gradient(psi_p, dlat[1])[0]
            d2psi_dlon2 = np.gradient(dpsi_dlon, dlon[1])[1]
            d2psi_dlat2 = np.gradient(dpsi_dlat, dlat[1])[0]
            d2psi_dlondlat = np.gradient(dpsi_dlat, dlon[1])[1]

            termxu = dpsi_dlon * dpsi_dlon - psi_p * d2psi_dlon2
            termxv = dpsi_dlon * dpsi_dlat - psi_p * d2psi_dlondlat
            termyv = dpsi_dlat * dpsi_dlat - psi_p * d2psi_dlat2

            # coefficient
            p_lev = 300.0  # unit in hPa
            p = p_lev / 1000.0
            magU = np.sqrt(u_c ** 2 + v_c ** 2)
            coeff = ((p * cos_lat) / (2 * magU.T)).T
            # x-component of TN-WAF
            px = (coeff.T / (a * a * cos_lat)).T * (((u_c.T) / cos_lat).T * termxu + v_c * termxv)
            # y-component of TN-WAF
            py = (coeff.T / (a * a)).T * (((u_c.T) / cos_lat).T * termxv + v_c * termyv)
            pxpypsi.append([px,py,psi_p])
        return np.array(pxpypsi)

    ds_level = []
    for singleyear in year_AR:
        era_u_v_t = create_epflux_calculate_data(singleyear)
        pxpypsi = tn_flux(era_u_v_t)
        ds_level.append(pxpypsi)
    ds_level = np.array(ds_level)
    ds_level = ds_level[:,-1,:,:]# select March
    _,p_wvf = stats.ttest_1samp(ds_level, 0, axis=0)

    ds_level = np.nanmean(ds_level, axis=0)
    wx,wy,_ = ds_level
    wx = gaussian_filter(wx, sigma=1)
    wy = gaussian_filter(wy, sigma=1)
    p_wvf = (p_wvf[0]>0.1) & (p_wvf[1]>0.1)
    wx = np.ma.masked_where(p_wvf, wx)
    wy = np.ma.masked_where(p_wvf, wy)

    ########## circulation #######
    # data = data.sel(time=np.in1d(data['time.month'], [3]))

    def get_hgt_hgt_cli_p(varshortname):
        data_3 = data.sel(time=np.in1d(data['time.month'], [3]))
        hgt_cli = data_3[varshortname].values
        from scipy.signal import detrend
        hgt_cli = detrend(hgt_cli, axis=0, type='linear')

        hgt = hgt_cli[indices] ##get hgt in March
        hgt_cli = hgt_cli[indices_low]

        _,p = stats.ttest_ind(hgt,
                         hgt_cli, axis=0, equal_var=False)

        hgt = np.nanmean(hgt, axis=0)
        hgt_cli = np.nanmean(hgt_cli, axis=0)

        return hgt, hgt_cli, p

    hgt, hgt_cli, p = get_hgt_hgt_cli_p(t_or_hgt)


    lon = data.longitude.values
    lat= data.latitude.values
    lon, lat = np.meshgrid(lon, lat)

    # ax = fig.add_subplot(1, 3, 1, projection=ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))
    ax = fig.add_subplot(2, 1, param[t_or_hgt][1], projection=ccrs.PlateCarree(central_longitude=10))

    plot_var = gaussian_filter(hgt-hgt_cli, sigma=1)
    if t_or_hgt == 'z':
        plot_var = plot_var/9.8

    cb = ax.contourf(lon, lat,
               plot_var,
                levels=param[t_or_hgt][2],
               cmap='RdBu_r',extend='both',
               transform=ccrs.PlateCarree())
    ax.contourf(lon, lat, np.ma.masked_greater(p, 0.1),
                    colors='none',levels=[0,0.11],
                    hatches=[8*'/',8*'/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                        shrink=0.6, pad=0.08, aspect=40)
    cbar.set_label(param[t_or_hgt][3])
    # cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    # cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
    if t_or_hgt == 'z':
        qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                        wx[::spacewind[1], ::spacewind[0]],
                        wy[::spacewind[1], ::spacewind[0]]
                        , transform=ccrs.PlateCarree(),scale=100)
        qk = ax.quiverkey(qui, 0.9, 1.05, 5, r'$\mathrm{5\,kg\,m^{-1}s^{-1}}$', labelpos='E',
                          coordinates='axes', color='black', labelsep=0.01)
    ax.gridlines(ylocs=[20,40,60,80], xlocs=[0, 60, 120, 179.9999, -60, -120],
                 draw_labels={"bottom": "x", "left": "y"},
                 linewidth=0.3, color='black', xpadding=3)

    ax.set_extent([0,320,10,90],crs=ccrs.PlateCarree())
    ax.coastlines()
    ax.set_title(figlabelstr[param[t_or_hgt][1]-1], fontdict={'size':9}, loc='left')
    ranges = [40, 145, 40, 61]
    # ranges = [200,223,72,78]
    ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]],
            [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]],linewidth=1.5, color='red',transform=ccrs.PlateCarree())

# plt.show()
plt.savefig('disseration_figure5-9.png', dpi=400)