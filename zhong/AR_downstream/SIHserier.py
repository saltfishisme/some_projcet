import xarray as xr
import matplotlib.pyplot as plt
import nc4
import numpy as np
import pickle
import datetime as dt

from dateutil.relativedelta import relativedelta
import copy

"""
The annual cumulative sea ice loss triggerd by AR.
for downstream analysis
AR in December will be classified into next year. 
"""


def load_ivt_total(s_time, types='anomaly'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]
    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    ivt_climate = np.sqrt(index_uivt_anomaly ** 2 + index_vivt_anomaly ** 2)
    if types == 'anomaly':
        return ivt_total - ivt_climate
    else:
        return [ivt_total, ivt_climate]


def load_ivt_u(s_time, types='anomaly'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    if types == 'anomaly':
        return ivt_east - index_uivt_anomaly
    else:
        return [ivt_east, index_uivt_anomaly]


def load_ivt_v(s_time, types='anomaly'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values

    if types == 'anomaly':
        return ivt_north - index_vivt_anomaly
    else:
        return [ivt_north, index_vivt_anomaly]



import pandas as pd
import nc4
import scipy.io as scio
from datetime import timedelta


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='anomaly'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    if types == 'anomaly':
        return data_present - data_anomaly
    else:
        return [data_present, data_anomaly]


'''
prepare for esiv pdf
'''
"""prepare for esiv, load pioms grid and trans into stree"""
import cv2

def load_esiv(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_ai = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * np.array(pioms_ai, dtype='double') * 100
    pioms_ah_ano = pioms_ah - ah_anomaly[loc]
    # pioms_ah_ano = pioms_ah
    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index] / np.nansum(weighted_PIOMS[index])
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return np.nansum(pioms_ah_ano * weighted_PIOMS_contour)
    else:
        return [pioms_ah, ah_anomaly[loc]]


def load_heff(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * 100
    pioms_ah_ano = pioms_ah - (hi_anomaly[loc] * 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, hi_anomaly[loc] * 100]


def load_sic(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * 100
    pioms_ah_ano = pioms_ah - (ai_anomaly[loc] * 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, ai_anomaly[loc] * 100]

def contour_cal(time_file, var='IVT'):
    info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

    time_all = info_AR_row.time_all.values
    time_AR = info_AR_row.time_AR.values
    time_all = info_AR_row.time_all.values
    AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))

    var_all = []

    for i_time in time_all[AR_loc:]:
        contour = info_AR_row['contour_all'].sel(time_all=i_time).values

        if var == 'IVT':
            s_i = load_ivt_total(pd.to_datetime(i_time), types='now')
        elif var == 'IVT_u':
            s_i = load_ivt_u(pd.to_datetime(i_time), types='now')
        elif var == 'IVT_v':
            s_i = load_ivt_v(pd.to_datetime(i_time), types='now')
        elif var == 'tcwv':
            s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                     'WaterVapor',
                                                     'tcwv', var_dir='Total_column_water_vapour', types='now')
        elif var == 'esiv':
            s_i = load_esiv(pd.to_datetime(i_time), contour)
        elif var == 'heff':
            s_i = load_heff(pd.to_datetime(i_time), contour)
        elif var == 'sic':
            s_i = load_sic(pd.to_datetime(i_time), contour)

        else:
            s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                     var_LongName[var],
                                                     var, types='now')
        # save
        if (plot_type == 'pdf') & (var != 'esiv')& (var != 'sic')& (var != 'heff'):
            s_i = s_i[0] - s_i[1]
            contour[int(30 / resolution), :] = 0
            s_i[contour != 1] = np.nan
            var_all.extend(s_i[~np.isnan(s_i)].flatten())
        else:
            var_all.append(s_i)

    if plot_type == 'pattern':
        var_all = np.nanmean(np.array(var_all), axis=0)
    return var_all


path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
resolution = 0.5
path_SaveData = r'/home/linhaozhong/work/AR_NP85/new_Clustering_paper_plot/'
var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
                'hcc': 'high_cloud_cover',
                'lcc': 'low_cloud_cover',
                'mcc': 'medium_cloud_cover',
                'tcc': 'total_cloud_cover',
                'tciw': 'total_column_cloud_ice_water',
                'tclw': 'total_column_cloud_liquid_water',

                }

resolution = 0.5
long = np.arange(0, 360, 0.5)
lat = np.arange(90, -90.01, -0.5)
bu_addition_Name = ''
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
path_PIOMS = r'/home/linhaozhong/work/Data/PIOMS_nc/'


pioms_grid = xr.open_dataset(path_PIOMS + r"aiday.H1985.nc")
points_lon = pioms_grid.x.values
points_lat = pioms_grid.y.values
from function_shared_Main import cal_area_differentLL_1d

weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)

# transform into 0,1,2,3 relative coordination, only works in north hemisphere
# !!!!!!!!!!!!!!!!!!!!!!!#
points_lat_relaCoord = (90 - points_lat) / resolution
points_lon_relaCoord = points_lon / resolution

# to Stree
from shapely.strtree import STRtree
from shapely.geometry import Point

points = [Point(x, y) for x, y in zip(points_lon_relaCoord, points_lat_relaCoord)]
tree = STRtree(points)  # create a 'database' of points
year_begin = 1979
year_end = 2020

plot_type = 'pdf'  # 'pattern'/'pdf'/'pdf_weighted'
# for var in ['IVT', 't2m', 'esiv', 'tcwv', 'str', 'slhf', 'sshf', 'tcc','cbh','hcc','lcc','mcc']:
for var in ['esiv']:
    for iI, i_type in enumerate(np.unique(save_labels_bmus)):
        if i_type == 0:
            continue
        file = file_all[save_labels_bmus == i_type]

        avg = np.zeros([year_end - year_begin + 1])

        for i_file in file:
            if int(i_file[:4]) <2000:
                # load pioms grid
                ai_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/aiday_1979_1999')
                hi_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/hiday_1979_1999')
                ah_anomaly = ai_anomaly * hi_anomaly * 100
            else:
                # load pioms grid
                ai_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/aiday_2000_2020')
                hi_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/hiday_2000_2020')
                ah_anomaly = ai_anomaly * hi_anomaly * 100

            aa = contour_cal(i_file, var)
            if not isinstance(aa, str):
                year_loc = int(i_file[:4]) - year_begin
                aa = np.array(aa)
                aa[aa>=0] = np.nan
                aa = np.nanmean(aa)
                print(i_file[:4])
                print(aa)
                if i_file[4:6] == '12':
                    year_loc = year_loc+1
                if year_loc > (year_end-year_begin):
                    continue
                avg[year_loc] += aa
            print(avg)
        nc4.save_nc4(avg,
                     path_SaveData + 'yearlySum_type%i_%s_%s' % (i_type, var, plot_type))
        exit()
exit()


"""
downstream analysis
given a series (sea ice loss), given month/day 
"""

'''
Constructing the field
monthly
only works when AR occurrence in this year is not zero.
we will exclude the years without AR from the given series.
'''
def save_standard_nc(data, var_name, times, lon=None, lat=None, ll=False):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360,0.25)
    if lat is None:
        lat = np.arange(90, -90.001, -0.25)

    ds = xr.Dataset()
    ds.coords['time'] = times
    if ll:
        ds.coords["latitude"] = lat
        ds.coords["longitude"] = lon

        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = (('time', "latitude", "longitude"), np.array(data[iI_vari]))
    else:
        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = ('time', np.array(data[iI_vari]))

    return ds
year = np.arange(1979, 2020+1)
def var_field(path_var, var_shortname, year, month):
    def get_yearmonth(var, var_year, var_month):
        wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
        wind_inmonth = wind_inYear.sel(
            time=np.in1d(wind_inYear['time.month'], var_month))
        return wind_inmonth

    data = xr.open_dataset(path_var)

    def reduce_expver(data, axis='dim'):
        result = np.nansum(data, axis=axis)
        result[np.all(np.isnan(data), axis=axis)] = np.nan
        return result

    # data = data.reduce(reduce_expver, 'expver')
    data = data.isel(expver=0)

    data = get_yearmonth(data, year, month)
    return data[var_shortname].values

def lingress(series, field):
    times = pd.date_range('%i-01-01'%year[0], '%i-01-01'%(year[-1]+1), freq='1Y')
    series = save_standard_nc([series], ['var'], times, ll=False)
    field = save_standard_nc([field], ['var'], times, ll=True)

    def lag_linregress_3D(x, y, lagx=0, lagy=0):
        """
        Input: Two xr.Datarrays of any dimensions with the first dim being time.
        Thus the input data could be a 1D time series, or for example, have three
        dimensions (time, lat, lon).
        Datasets can be provided in any order, but note that the regression slope
        and intercept will be calculated for y with respect to x.
        Output: Covariance, correlation, regression slope and intercept, p-value,
        and standard error on regression between the two datasets along their
        aligned time dimension.
        Lag values can be assigned to either of the data, with lagx shifting x, and
        lagy shifting y, with the specified lag amount.
        """

        # 1. Ensure that the data are properly alinged to each other.
        x, y = xr.align(x, y)

        # 2. Add lag information if any, and shift the data accordingly
        if lagx != 0:
            # If x lags y by 1, x must be shifted 1 step backwards.
            # But as the 'zero-th' value is nonexistant, xr assigns it as invalid
            # (nan). Hence it needs to be dropped
            x = x.shift(time=-lagx).dropna(dim='time')

            # Next important step is to re-align the two datasets so that y adjusts
            # to the changed coordinates of x
            x, y = xr.align(x, y)

        if lagy != 0:
            y = y.shift(time=-lagy).dropna(dim='time')
            x, y = xr.align(x, y)

        # 3. Compute data length, mean and standard deviation along time axis:
        n = y.notnull().sum(dim='time')
        xmean = x.mean(dim='time')
        ymean = y.mean(dim='time')
        xstd = x.std(dim='time')
        ystd = y.std(dim='time')

        # 4. Compute covariance along time axis
        cov = ((x - xmean) * (y - ymean)).sum(dim='time') / n

        # 5. Compute correlation along time axis
        cor = cov / (xstd * ystd)

        # 6. Compute regression slope and intercept:
        slope = cov / (xstd ** 2)
        intercept = ymean - xmean * slope

        # 7. Compute P-value and standard error
        # Compute t-statistics
        tstats = cor * np.sqrt(n - 2) / np.sqrt(1 - cor ** 2)
        stderr = slope / tstats

        from scipy.stats import t

        pval = t.sf(np.abs(tstats['var'].values), n['var'].values - 2) * 2
        pval = xr.DataArray(pval, dims=cor.dims, coords=cor.coords)

        return cov, cor, slope, intercept, pval, stderr

    cov, cor, slope, intercept, pval, stderr = lag_linregress_3D(series, field)
    return cov, cor, slope, intercept, pval, stderr

for i_som in [1]:
    # series_arSIH = nc4.read_nc4(r'/home/linhaozhong/work/AR_NP85/new_Clustering_paper_plot/yearlySum_type%i_esiv_pdf'%i_som)
    series_arSIH = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\new_Clustering_paper_plot\yearlySum_type%i_esiv_pdf'%i_som)

    print(series_arSIH.shape)
    exclude_YearIndex = np.squeeze(np.argwhere(series_arSIH != 0))
    # print(exclude_YearIndex)
    # exit()
    # print(len(exclude_YearIndex))
    # continue
    year_exclude = year[exclude_YearIndex]
    # print(year_exclude)
    # exit()
    series_arSIH_excluede = series_arSIH[exclude_YearIndex]
    plt.plot(year, series_arSIH)
    plt.show()


    # explore_month = list(range(1,12+1))
    # explore_month = explore_month+[[3,4,5],[6,7,8]]
    # for i_explore_month in explore_month:
    #     tp_field = var_field(r"/home/linhaozhong/work/Data/ERA5/monthly/t2mTP_1940_2023.nc",
    #                          't2m',
    #                          year,
    #                          i_explore_month
    #                          )
    #
    #     cov, cor, slope, intercept, pval, stderr = lingress(series_arSIH, tp_field)
    #     print(slope.shape)




