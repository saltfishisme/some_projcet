import xarray as xr
import numpy as np
import scipy.io as scio
import pandas as pd

"""
create nc for analysis from ERA5
u,v,w, t and climate ones.
"""

def date_ranges(years):
    dates = []
    for iyear in years:
        # $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ #
        date_s = '%s%s' % (iyear, month1)
        date_e = '%s%s' % (iyear, month2)
        sdate = pd.date_range(date_s, date_e, freq='1D')
        dates.extend(sdate)
    return dates

month1 = '0105'
month2 = '0108'

date_year_s = 1979
date_year_e = 1979

years = np.arange(date_year_s, date_year_e + 1)
dates_all = date_ranges(years)

def create_epflux_calculate_data(dates_all):

    # level_used = [10,20,30,50,70,100,150,200,300,400,500,700,1000]
    # level_iloc_used = [5,6,7,8,9,10,12,14,17,19,21,25,36]
    level_used = [1, 2, 3, 5, 7, 10, 20, 30, 50, 70, 100, 125, 150, 175, 200, 225, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 775, 800, 825, 850, 875, 900, 925, 950, 975, 1000]
    level_iloc_used = np.arange(0,37)
    lat_iloc = np.arange(0,181,6)
    lon_iloc = np.arange(0,720,6)



    path_pressure = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'

    uvw = []
    ref = []
    for i_date in dates_all:
        i_date = i_date.strftime('%Y%m%d')
        s_uvw = []
        s_ref = []
        var_shortName = ['u', 'v', 'w', 't', 'z']
        var_Name = dict(zip(var_shortName, ['Uwnd', 'Vwnd', 'Wwnd', 'Tem', 'Hgt']))
        for i_varShort in var_shortName:
            var = xr.open_dataset(path_pressure + '%s/'%var_Name[i_varShort] + i_date[:4] + '/%s.%s.nc' %(var_Name[i_varShort], i_date))
            print(var[i_varShort])
            var = var.isel(level=level_iloc_used, latitude=lat_iloc, longitude=lon_iloc)
            var = var.mean(dim=['time'])
            print('hi,are you here?')

            s_uvw.append(var[i_varShort].values)

            # var = scio.loadmat(path_pressure + '%s/anomaly_levels/%s.mat' % (var_Name[i_varShort],i_date[4:]))
            # var = np.nanmean(var[i_date[4:]], axis=1)
            # s_ref.append(np.array(var[level_iloc_used][:,lat_iloc,:][:,:,lon_iloc]))
        uvw.append(s_uvw)
        # ref.append(s_ref)
    uvw = np.array(uvw)
    # ref = np.array(ref)

    ds = xr.Dataset()
    ds.coords["lat"] = np.arange(90,-0.001,-3)
    long = np.arange(0,360,3)
    ds.coords["lon"] = long
    ds.coords["time"] = dates_all
    ds.coords["level"] = level_used

    var_shortName = ['u', 'v', 'w', 't', 'z']

    for iI_varShort, i_varShort in enumerate(var_shortName):
        ds[i_varShort ] = (("time", "level", "lat", "lon"), np.array(uvw[:,iI_varShort]))
        # ds[i_varShort + '_cli'] = (("time", "level", "latitude", "longitude"), np.array(ref[:,iI_varShort]))
    print(ds)
    ds.to_netcdf(r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/epFlux_test.nc')
# create_epflux_calculate_data()
# exit()

import aostools
from aostools import climate
from aostools import constants
import xarray as xr
import matplotlib.pyplot as plt



ds4 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/epComposite_time-end-begin_4.nc")
ds6 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/epComposite_time-end-begin_6.nc")


titles = ['(a)BAF', '(b) BSW', '(c) BAF', '(d) BSW']
fig, axs = plt.subplots(2,2, figsize=[8,6])
plt.subplots_adjust(top=0.96,
bottom=0.06,
left=0.065,
right=0.955,
hspace=0.2,
wspace=0.32)
from matplotlib import colors
import cmaps
cmpas_here = cmaps.cmp_b2r
for i_ax, ds in enumerate([ds4, ds6, ds4, ds6]):
    ax = axs[int(i_ax/2)][int(i_ax%2)]

    ds = ds.sel(lat=slice(90,20))

    from scipy.stats import ttest_1samp

    _, p1 = ttest_1samp(ds.ep1.values, 0, axis=0)
    _, p2 = ttest_1samp(ds.ep2.values, 0, axis=0)
    _, p3 = ttest_1samp(ds.div1.values, 0, axis=0)
    _, p4 = ttest_1samp(ds.div2.values, 0, axis=0)
    p = (p1 > 0.1) & (p2 > 0.1) & (p3 > 0.1) & (p4 > 0.1)

    era_u_v_t = ds.mean(dim='time')
    cb = ax.contourf(era_u_v_t.lat, era_u_v_t.level, era_u_v_t.div1, cmap=cmpas_here,
                     norm=colors.CenteredNorm(), levels=np.arange(-10, 10.001, 0.5), extend='both')
    cbar = plt.colorbar(cb)
    cbar.set_label('m s$^{-1}$ d$^{-1}$')
    from scipy.ndimage import gaussian_filter
    era_u_v_t.ep1.values = gaussian_filter(era_u_v_t.ep1.values, sigma=1)
    era_u_v_t.ep2.values = gaussian_filter(era_u_v_t.ep2.values, sigma=1)

    if i_ax<=1:
        dx,dy = climate.PlotEPfluxArrows(x=era_u_v_t.lat, y=era_u_v_t.level,
                                 ep1=era_u_v_t.ep1,
                                 ep2=era_u_v_t.ep2,
                                 fig=fig, ax=ax, yscale='log', xlim=[40, 90], scale=5.91e15, p=p)
        ax.set_xticks([40, 50, 60, 70, 80, 90,])
        ax.set_xticklabels(['%i°N' % i for i in [40, 50, 60, 70, 80, 90,]])
    else:
        dx,dy = climate.PlotEPfluxArrows(x=era_u_v_t.lat, y=era_u_v_t.level,
                                 ep1=era_u_v_t.ep1,
                                 ep2=era_u_v_t.ep2,
                                 fig=fig, ax=ax, yscale='log', xlim=[60, 90], scale=6.91e14, p=p)
        ax.set_xticks([60, 70, 80, 90,])
        ax.set_xticklabels(['%i°N' % i for i in [60, 70, 80, 90,]])

    ax.set_ylabel('hPa')
    ax.set_title(titles[i_ax], loc='left')
plt.show()
plt.savefig('dissertation_figure5-3.png', dpi=400)
plt.show()

"""
if ep has two phase, begin and end.
"""
fig, ax = plt.subplots()
era_u_v_t = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/epComposite_time-end-begin_4_mean.nc")
era_u_v_t2 = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\tnep/epComposite_time-end_6.nc")

era_u_v_t = era_u_v_t.sel(lat=slice(90,60))
era_u_v_t2 = era_u_v_t2.sel(lat=slice(90,60))
def flatten_ep(da):
    # lower_quantile = da.quantile(0.05)
    upper_quantile = da.quantile(0.90)
    return da.where(da <= upper_quantile, upper_quantile)
# climate.PlotEPfluxArrows(x=era_u_v_t.lat, y=era_u_v_t.level,
#                          ep1=flatten_ep(era_u_v_t2.ep1)-flatten_ep(era_u_v_t.ep1),
#                         ep2=flatten_ep(era_u_v_t2.ep2)-flatten_ep(era_u_v_t.ep2),
#                          fig=fig, ax=ax, yscale='log')
# climate.PlotEPfluxArrows(x=era_u_v_t.lat, y=era_u_v_t.level,
#                          ep1=flatten_ep(era_u_v_t2.ep1)-flatten_ep(era_u_v_t.ep1),
#                         ep2=flatten_ep(era_u_v_t2.ep2)-flatten_ep(era_u_v_t.ep2),
#                          fig=fig, ax=ax, yscale='log',xlim=[60,90])
climate.PlotEPfluxArrows(x=era_u_v_t.lat, y=era_u_v_t.level,
                         ep1=flatten_ep(era_u_v_t.ep1),
                        ep2=flatten_ep(era_u_v_t.ep2),
                         fig=fig, ax=ax, yscale='log',xlim=[60,90], scale=2.91e14)
plt.show()