import ftplib
import os
import pandas as pd
from datetime import date, timedelta
import numpy as np
import time
import xarray as xr
import glob
import matplotlib.pyplot as plt
# a = xr.open_dataset(r"E:\NINH\rawData\20241025\pcp_prd_2024102500_24.nc")
# print(a.lat.values)
# b = xr.open_dataset(r"E:\NINH\rawData\20241025\pcp_prd_2024102500_24_ll.nc")
# print(b.lat.values)
# exit()
# fig,axs = plt.subplots(3,1)
# axs[0].imshow(a.apcp_24.values[0])
# axs[1].imshow(b.apcp_24.values[0])
# axs[2].imshow(a.apcp_24.values[0]-b.apcp_24.values[0])
# plt.show()
dir_main = 'E:/NINH/'
rawData_reDown = False
pic_reDraw = False
# date
today = date.today()
if today.weekday() == 3:
    today = today
else:
    days_to_Sday = (today.weekday() - 3) % 7
    today = today - timedelta(days=days_to_Sday)

# for ftp: dic name,file name (pcp_prd_%s00_24.nc)
today = today+timedelta(days=1) # to Friday

# for anomaly_pic.png
date_range_dt_1week = pd.date_range(today+ timedelta(days=1), today + timedelta(days=9 + 1), freq='1D')
date_range_1week = date_range_dt_1week.strftime('%Y%m%d')

# for daily_pic.png
date_range_dt = pd.date_range(today , today + timedelta(days=9 + 1), freq='1D')
date_range = date_range_dt.strftime('%Y%m%d')

date_today = today.strftime('%Y%m%d')

rawdata_dir = "%s/rawData/%s/" % (dir_main, date_today)
workdir = "%s/result/%s/"%(dir_main, date_today)
shp_dir = r'%s/shp/'%(dir_main)
# shp_dir = r'G:\OneDrive\basis\data\shp\shengjie/'

os.makedirs(rawdata_dir, exist_ok=True)
os.makedirs(workdir, exist_ok=True)


def ftp_process(FTP_HOST, FTP_USER, FTP_PASS, ftpfile,localfile, down_up='download', keyword=None):
    """
    :param FTP_HOST:
    :param FTP_USER:
    :param FTP_PASS:
    :param ftpfile:
    :param localfile:
    :param down_up: "download" or "upload"
    :param keyword: only work when down_up == 'download', Only file names containing keywords will be downloaded
    :return:
    """

    MAX_RETRIES = 40
    RETRY_DELAY = 5

    def connect_to_ftp():
        attempt = 0
        while attempt < MAX_RETRIES:
            try:
                ftp = ftplib.FTP(FTP_HOST)
                ftp.login(FTP_USER, FTP_PASS)
                print("Connected to FTP server.")
                return ftp
            except ftplib.all_errors as e:
                os.system("netsh interface set interface name='Ethernet' admin=disable")
                time.sleep(5)
                os.system("netsh interface set interface name='Ethernet' admin=enable")
                time.sleep(5)

                attempt += 1
                print(f"Attempt {attempt} failed: {e}")
                if attempt < MAX_RETRIES:
                    print(f"Retrying in {RETRY_DELAY} seconds...")
                    time.sleep(RETRY_DELAY)
                    if attempt%5 == 0:
                        time.sleep(60*10)
                else:
                    print("Max retries reached. Could not connect to FTP server.")
                    raise

    ftp = connect_to_ftp()

    def download_directory(directory, local_dir):
        def download_file(filename, local_dir):
            local_filepath = os.path.join(local_dir, filename.replace(':', '_'))
            if keyword is not None:
                if not keyword in local_filepath:
                    return

            #  if local file is exist
            if not rawData_reDown:
                if os.path.exists(local_filepath):
                    try:
                        xr.open_dataset(local_filepath)
                        print('exist %s' % local_filepath)
                        return
                    except:
                        os.remove(local_filepath)
                        print('remove %s'%local_filepath)

            with open(local_filepath, "wb") as file:
                ftp.retrbinary(f"RETR {filename}", file.write)
            print(f"Downloaded: {filename}")

        os.makedirs(local_dir, exist_ok=True)
        ftp.cwd(directory)
        filenames = ftp.nlst()

        for filename in filenames:
            local_path = os.path.join(local_dir, filename)
            if is_directory(filename):
                download_directory(filename, local_path)
            else:
                download_file(filename, local_dir)
    def upload_directory(local_dir, remote_dir):
        try:
            ftp.cwd(remote_dir)
        except ftplib.error_perm:
            ftp.mkd(remote_dir)
            ftp.cwd(remote_dir)

        for item in os.listdir(local_dir):
            local_path = os.path.join(local_dir, item)
            if os.path.isdir(local_path):
                upload_directory(local_path, os.path.join(remote_dir, item))
            else:
                with open(local_path, "rb") as file:
                    ftp.storbinary(f"STOR {item}", file)
                print(f"Uploaded: {local_path} to {remote_dir}")

    def is_directory(name):
        current = ftp.pwd()
        try:
            ftp.cwd(name)
            ftp.cwd(current)
            return True
        except ftplib.error_perm:
            return False

    if down_up == 'download':
        download_directory(ftpfile, localfile)
    elif down_up == 'upload':
        upload_directory(localfile, ftpfile)

    ftp.quit()

"""
==============================================================
step1:
download files from ftp server
==============================================================
"""
FTP_do = True

FTP_retry = 0
# Ensure that the nc file is downloaded correctly
while FTP_do:
    FTP_HOST = "159.226.119.25"
    FTP_USER = "ranlk"
    FTP_PASS = "jiangshui2024"

    FTP_retry += 1
    try:
        ftp_process(FTP_HOST, FTP_USER, FTP_PASS, '/%s/' % today.strftime('%Y%m%d00'), rawdata_dir, 'download',
                    keyword='_24.nc')
        ftp_process(FTP_HOST, FTP_USER, FTP_PASS, '/%s/' % today.strftime('%Y%m%d00'), rawdata_dir, 'download',
                    keyword='_24_ll.nc')
        file_a = glob.glob(os.path.join(rawdata_dir, '*_24.nc'))
        file_b = glob.glob(os.path.join(rawdata_dir, '*_24_ll.nc'))
        file_ab = file_a+file_b
        if len(file_ab) >= 2:
            for i_file_ab in file_ab:
                xr.open_dataset(i_file_ab)
            FTP_do = False

    except:
        if FTP_retry < 48:
            time.sleep(30*60)
            print('Error. Retry %i time'%FTP_retry)
            continue
        else:
            break


"""
==============================================================
step2:
run and process

regrid .nc file to _ll.nc
==============================================================
"""

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import xarray as xr


def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    # def get_dataarray(data):
    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
    #     return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times

    for iI_vari, ivari in enumerate(var_name):

        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + '%s.nc' % file_name)
    return ds

file_a = glob.glob(os.path.join(rawdata_dir, '*_24.nc'))
pcp_file = [s for s in file_a if 'pcp_prd' in s][0]
#file_a must be pcp
a = xr.open_dataset(pcp_file)
data = a.apcp_24.values
lat = a.lat.values[:,0]
lon = a.lon.values[0,:]
data = save_standard_nc([data], ['apcp_24'], a.time, lon=lon, lat=lat,
                 path_save=workdir, file_name='pcp_prd_%s00_24_ll'%date_today)

#=================!!!!!!!!!!!!!!==========================
file_a = glob.glob(os.path.join(rawdata_dir, '*_24_ll.nc'))
snow_file = [s for s in file_a if '_24_ll.nc' in s][0]
a = xr.open_dataset(snow_file)
data = a.apcp_24.values
lat = a.lat.values[:,0]
lon = a.lon.values[0,:]
data = save_standard_nc([data], ['apcp_24'], a.time, lon=lon, lat=lat,
                 path_save=workdir, file_name='snow_prd_%s00_24_ll'%date_today)


"""
==============================================================
step2:
run and process

anomaly weekly
==============================================================
"""

data = xr.open_dataset(workdir+"pcp_prd_%s00_24_ll.nc"%date_today)

# shp_dir = r'G:\OneDrive\basis\data\shp\shengjie/'
# data = xr.open_dataset("D:/pcp_prd_%s00_24_ll.nc"%date_today)

lat = data.lat.values
lon = data.lon.values

apcp_all = data.sel(time=slice(date_range_1week[1], date_range_1week[-1])).apcp_24

def regrid(sparse_grid, x, y, new_x, new_y, meshgrid=False):
    import numpy as np
    from scipy.interpolate import griddata
    if meshgrid:
        x, y = np.meshgrid(x, y)
    x_flat = x.ravel()
    y_flat = y.ravel()
    z_flat = sparse_grid.ravel()
    new_x, new_y = np.meshgrid(new_x, new_y)
    return griddata((x_flat, y_flat), z_flat, (new_x, new_y), method='nearest')
data_sum = np.nansum(apcp_all.values, axis=0)

cli = xr.open_dataset(r"%s\pre_cli_cn05.nc"%shp_dir)
time_as_month_day = cli['day'].dt.strftime('%m-%d')

data_sum = regrid(data_sum, lon, lat, cli.lon.values, cli.lat.values, meshgrid=True)

lat = cli.lat.values
lon = cli.lon.values

cli_1week = cli.sel(day=cli['day'][time_as_month_day.isin(date_range_dt_1week.strftime('%m-%d')[:-1])]).sum(dim='day')['clm'].values

anomaly = 100*(data_sum-cli_1week)/cli_1week

def plot_china(apcp):
    plt.clf()
    SMALL_SIZE = 7
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.AlbersEqualArea(central_longitude=105)},
                           figsize=[5, 4.6])
    plt.subplots_adjust(top=1.0,
                        bottom=0.015,
                        left=0.015,
                        right=0.985,
                        hspace=0.2,
                        wspace=0.2)
    # colors = ['#FFFFFF','#A2F589', '#3BBB38', '#64B7FD', '#0200FD', '#FA00FB','#820040']
    # colors = ['#006181','#00C0E1', '#00FFFF', '#9AFCFC',
    #           '#FFFFFF',
    #           '#FCE100', '#FCA200', '#FC6100','#810000']
    # levels = [-999,-60, -40, -20,-5, 5, 20, 40, 200, 999]
    # level_labels = [r'≤-60',r'-60~-40', '-40~-20', '-20~-5', '-5~5','5~20', '20~40', '40~60', '≥60']


    def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0, additional_shp_lw=0.1):
        import cartopy.crs as ccrs
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader
        from matplotlib.patches import PathPatch
        def shp2clip(shpfile):
            import shapefile
            from matplotlib.path import Path
            sf = shapefile.Reader(shpfile)
            vertices = []
            codes = []
            for shape_rec in sf.shapeRecords():
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append((pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
            return clip

        if clipshape == True:
            pathss = shp2clip(r'%sall_china.shp' % shp_dir)
            plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
            upath = PathPatch(pathss, transform=plate_carre_data_transform)

            for collection in fill.collections:
                collection.set_clip_path(upath)

        shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'%sall_china.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=shplinewidth)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=shplinewidth)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'%snanhai.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=shplinewidth)
        ax.add_feature(shape_feature)


    def plots(ax):
        cb = ax.contourf(lon, lat, apcp, transform=ccrs.PlateCarree())
        plt.colorbar(cb)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2)


    def plotss(ax):
        def del_ll(range, lat, lon):
            """
            cut lon and lat, and then return the index to cut data used later.
            :param range:
            :param lat:
            :param lon:
            :return: self.lat, self.lon, self.del_area
            """

            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range)

            if del_area[0] > del_area[1]:  # sort
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        def del_data(data, del_area):
            """
            link with del_ll, use self.del_area from function del_ll
            :param data:
            :return: cut data
            """
            return data[del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]

        lat_jdx, lon_jdx, range_jdx = del_ll([105, 122.5, 2.5, 25], lat, lon)
        apcp_jdx = del_data(apcp, range_jdx)
        cb = ax.contourf(lon_jdx, lat_jdx, apcp_jdx, transform=ccrs.PlateCarree())
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.2, additional_shp_lw=0.1)
        ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
        return cb


    plots(ax)
    ax.set_extent(([80, 127, 17, 54]), crs=ccrs.PlateCarree())

    ax2 = fig.add_axes([0, 0.1, 0.12, 0.28], projection=ccrs.AlbersEqualArea(central_longitude=105))
    contourf_jiuduanxian = plotss(ax2)
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

    ip = InsetPosition(ax, [0.005, -0.054, 0.12, 0.28])
    ax2.set_axes_locator(ip)

    # ocean110 = cartopy.feature.NaturalEarthFeature('physical', 'ocean',
    #                                                scale='10m', edgecolor='none',zorder=-10,
    #                                                facecolor=cartopy.feature.COLORS['water'],)
    # ax.add_feature(ocean110)
    # ax2.add_feature(ocean110)

    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.savefig(workdir + '%s.jpg' % date_range_1week[i_day - 1], dpi=400)
    plt.close()
    plt.clf()

def plot_anomaly(apcp):
    plt.clf()
    SMALL_SIZE = 7
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.AlbersEqualArea(central_longitude=105)},
                           figsize=[5, 4.6])
    plt.subplots_adjust(top=1.0,
                        bottom=0.015,
                        left=0.015,
                        right=0.985,
                        hspace=0.2,
                        wspace=0.2)

    def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0, additional_shp_lw=0.1):
        import cartopy.crs as ccrs
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader
        from matplotlib.patches import PathPatch
        def shp2clip(shpfile):
            import shapefile
            from matplotlib.path import Path
            sf = shapefile.Reader(shpfile)
            vertices = []
            codes = []
            for shape_rec in sf.shapeRecords():
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append((pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
            return clip

        if clipshape == True:
            pathss = shp2clip(r'%sall_china.shp' % shp_dir)
            plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
            upath = PathPatch(pathss, transform=plate_carre_data_transform)

            for collection in fill.collections:
                collection.set_clip_path(upath)

        shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'%sall_china.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=shplinewidth)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=shplinewidth)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'%snanhai.shp' % shp_dir).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=shplinewidth)
        ax.add_feature(shape_feature)

    def plots(ax):
        cb = ax.contourf(lon, lat, apcp,levels=levels, transform=ccrs.PlateCarree(), colors=colors)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2)


    def plotss(ax):
        def del_ll(range, lat, lon):
            """
            cut lon and lat, and then return the index to cut data used later.
            :param range:
            :param lat:
            :param lon:
            :return: self.lat, self.lon, self.del_area
            """

            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range)

            if del_area[0] > del_area[1]:  # sort
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        def del_data(data, del_area):
            """
            link with del_ll, use self.del_area from function del_ll
            :param data:
            :return: cut data
            """
            return data[del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]

        lat_jdx, lon_jdx, range_jdx = del_ll([105, 122.5, 2.5, 25.8], lat, lon)
        apcp_jdx = del_data(apcp, range_jdx)
        cb = ax.contourf(lon_jdx, lat_jdx, apcp_jdx, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.2, additional_shp_lw=0.1)
        ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
        return cb


    plots(ax)
    ax.set_extent(([80, 128.5, 17, 54]), crs=ccrs.PlateCarree())

    ax2 = fig.add_axes([0, 0.1, 0.12, 0.28], projection=ccrs.AlbersEqualArea(central_longitude=105))
    contourf_jiuduanxian = plotss(ax2)
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

    ip = InsetPosition(ax, [0.875, -0.049, 0.12, 0.28])
    ax2.set_axes_locator(ip)

    import matplotlib.patches as mpatches

    patches = [mpatches.Patch(facecolor=colors[i], label=level_labels[i], edgecolor='black', linewidth=0.2) for i in
               range(len(levels) - 1)]
    legend = ax.legend(handles=patches,
                       title=r"图例(%)", title_fontsize=8,
                       borderpad=0.01, labelspacing=0.1,
                       handlelength=1.5, columnspacing=0.5,
                       loc='lower left',
                       ncol=2)

    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_linewidth(0.6)
    legend.get_frame().set_boxstyle('square')
    legend.get_frame().set_alpha(None)


    # add province
    def add_province_name(ax):
        pn = pd.read_csv(r"%s/province_captical_ll.txt" % shp_dir, usecols=[0, 5, 6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            ax.plot(slon, slat, marker='o', color='black', markersize=1,
                    transform=ccrs.Geodetic())

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=5, weight='bold',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform,
                    )

    add_province_name(ax)

    # ocean110 = cartopy.feature.NaturalEarthFeature('physical', 'ocean',
    #                                                scale='10m', edgecolor='none',zorder=-10,
    #                                                facecolor=cartopy.feature.COLORS['water'],)
    # ax.add_feature(ocean110)
    # ax2.add_feature(ocean110)

    plt.rcParams['font.sans-serif'] = ['SimHei']
    if title_zyy == 'top':
        ax.set_title('国家灾研院\nNINH-MWF预报系统\n周累积降水距平百分率',
                     fontsize=11, y=1.0, pad=-45)
    else:
        ax.set_title('周累积降水距平百分率', fontsize=11, y=1.0, pad=-30)
        ax.text(0, -0.022, r'国家灾研院NINH-MWF预报系统', transform=ax.transAxes,
                fontsize=6, color='black')

    plt.savefig(workdir + 'anomaly_%s_%s.jpg'%(save_name, title_zyy), dpi=400)
    plt.close()
    plt.clf()

anomaly[anomaly>999] = 999
anomaly[anomaly<-100] = -100
taiwanmask = xr.open_dataset(r'%s/taiwan_mask_0.25.nc'%shp_dir)['tp'].values
anomaly[taiwanmask==1] = np.nan

colors = ['#FC1C19', '#E65F5E', '#FD986B', '#FFFFA1',
          '#CAFECC', '#3EC749', '#30FEFE', '#1895F9', '#0000F7']
levels = [-100, -80, -50, -20, 0, 20, 50, 100, 200, 999]
level_labels = [r'-100$\sim$-80', r'-80$\sim$-50', '-50$\sim$-20', '-20$\sim$0',
                '0$\sim$20', '20$\sim$50', '50$\sim$100', '100$\sim$200', '≥200']
save_name = 'max200'

if not pic_reDraw:
    if not os.path.exists(workdir + 'anomaly_%s.jpg'%save_name):
        title_zyy = 'top'
        plot_anomaly(anomaly)
        title_zyy = 'left'
        plot_anomaly(anomaly)
    else:
        print('anomaly200 exist')

colors = ['#FC1C19', '#E65F5E', '#FD986B', '#FFFFA1',
          '#CAFECC', '#3EC749', '#30FEFE', '#1895F9', '#0000F7']
levels = [-100, -80, -50, -20, 0, 50, 100, 200, 400, 999]
level_labels = [r'-100$\sim$-80', r'-80$\sim$-50', '-50$\sim$-20', '-20$\sim$0',
                '0$\sim$50', '50$\sim$100', '100$\sim$200', '200$\sim$400', '≥400']
save_name = 'max400'

if not pic_reDraw:
    if not os.path.exists(workdir + 'anomaly_%s.jpg'%save_name):
        title_zyy = 'top'
        plot_anomaly(anomaly)
        title_zyy = 'left'
        plot_anomaly(anomaly)
    else:
        print('anomaly400 exist')
"""
==============================================================
step2:
run and process

daily
==============================================================
"""
data = xr.open_dataset(workdir+"pcp_prd_%s00_24_ll.nc"%date_today)
lat = data.lat.values
lon = data.lon.values
apcp_all = data.apcp_24

#++++++++++++++++++++++++++++++++++++++!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
data_snow = xr.open_dataset(workdir+"snow_prd_%s00_24_ll.nc"%date_today)
snow_all = data_snow.apcp_24

colors = ['#C8FAFA', '#00E3D7', '#006496', '#001882', '#FF00FF']
# levels = [0.01, 10, 25, 50, 100, 250, 999]
# level_labels = ['0$\sim$10', '10$\sim$25', '25$\sim$50', '50$\sim$100', '100$\sim$250', '≥250']
levels = [0.01, 10, 25, 50, 100, 999]
level_labels = ['0$\sim$10', '10$\sim$25', '25$\sim$50', '50$\sim$100', '≥100']

legend_test = 0
for title_zyy in ['top', 'left']:

    for i_day, day_now in enumerate(date_range):
        legend_test += 1
        if i_day == 0:
            continue
        apcp = apcp_all.sel(time=day_now).values
        snow = snow_all.sel(time=day_now).values

        if not pic_reDraw:
            if os.path.exists(workdir +'%s.jpg' % date_range[i_day - 1]):
                print('%s.jpg' % date_range[i_day - 1] + ' exist')
                continue

        plt.clf()
        SMALL_SIZE=7
        plt.rc('axes', titlesize=SMALL_SIZE)
        plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
        plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
        plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
        plt.rc('axes', titlepad=1, labelpad=1)
        plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
        plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
        plt.rc('xtick.major', size=2, width=0.5)
        plt.rc('xtick.minor', size=1.5, width=0.2)
        plt.rc('ytick.major', size=2, width=0.5)
        plt.rc('ytick.minor', size=1.5, width=0.2)
        plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
        plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

        fig, ax = plt.subplots(subplot_kw={'projection':ccrs.AlbersEqualArea(central_longitude=105)},
                               figsize=[5,4.6])
        plt.subplots_adjust(top=1.0,
        bottom=0.015,
        left=0.015,
        right=0.985,
        hspace=0.2,
        wspace=0.2)
        # colors = ['#FFFFFF','#A2F589', '#3BBB38', '#64B7FD', '#0200FD', '#FA00FB','#820040']

        def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0, additional_shp_lw=0.1):
            import cartopy.crs as ccrs
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader
            from matplotlib.patches import PathPatch
            def shp2clip(shpfile):
                import shapefile
                from matplotlib.path import Path
                sf = shapefile.Reader(shpfile)
                vertices = []
                codes = []
                for shape_rec in sf.shapeRecords():
                    pts = shape_rec.shape.points
                    prt = list(shape_rec.shape.parts) + [len(pts)]
                    for i in range(len(prt) - 1):
                        for j in range(prt[i], prt[i + 1]):
                            vertices.append((pts[j][0], pts[j][1]))
                        codes += [Path.MOVETO]
                        codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                        codes += [Path.CLOSEPOLY]
                    clip = Path(vertices, codes)
                return clip

            if clipshape == True:
                pathss = shp2clip(r'%sall_china.shp'%shp_dir)
                plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
                upath = PathPatch(pathss, transform=plate_carre_data_transform)

                for collection in fill.collections:
                    collection.set_clip_path(upath)

            shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sall_china.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%snanhai.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)

        def plots(ax):

            cb = ax.contourf(lon, lat, apcp, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
            shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2)

            cb = ax.contourf(lon, lat, snow, levels= [0.001, 2.5, 5, 10, 20, 999], transform=ccrs.PlateCarree(), colors= [ '#A2A0A3', '#716F72', '#464447', '#7345E3', '#4D0074'])
            shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2)

        def plotss(ax):
            def del_ll(range, lat, lon):
                """
                cut lon and lat, and then return the index to cut data used later.
                :param range:
                :param lat:
                :param lon:
                :return: self.lat, self.lon, self.del_area
                """
                def ll_del(lat, lon, area):
                    import numpy as np
                    lat0 = lat - area[3]
                    slat0 = int(np.argmin(abs(lat0)))
                    lat1 = lat - area[2]
                    slat1 = int(np.argmin(abs(lat1)))
                    lon0 = lon - area[0]
                    slon0 = int(np.argmin(abs(lon0)))
                    lon1 = lon - area[1]
                    slon1 = int(np.argmin(abs(lon1)))
                    return [slat0, slat1, slon0, slon1]

                del_area = ll_del(lat, lon, range)

                if del_area[0] > del_area[1]:  # sort
                    del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
                if del_area[2] > del_area[3]:
                    del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

                lat = lat[del_area[0]:del_area[1] + 1]
                lon = lon[del_area[2]:del_area[3] + 1]
                return lat, lon, del_area
            def del_data(data, del_area):
                """
                link with del_ll, use self.del_area from function del_ll
                :param data:
                :return: cut data
                """
                return data[del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]

            lat_jdx, lon_jdx, range_jdx = del_ll([105, 122.5, 2.5, 25.8], lat, lon)
            apcp_jdx = del_data(apcp, range_jdx)
            cb = ax.contourf(lon_jdx, lat_jdx, apcp_jdx, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
            shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.2, additional_shp_lw=0.1)
            ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
            return cb

        plots(ax)
        ax.set_extent(([80, 128.5, 17, 54]), crs=ccrs.PlateCarree())

        ax2 = fig.add_axes([0, 0.1, 0.12, 0.28], projection=ccrs.AlbersEqualArea(central_longitude=105))
        contourf_jiuduanxian = plotss(ax2)
        from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
        ip = InsetPosition(ax, [0.875, -0.049, 0.12, 0.28])
        ax2.set_axes_locator(ip)

        import matplotlib.patches as mpatches
        patches = [mpatches.Patch(facecolor=colors[i], label=level_labels[i], edgecolor='black', linewidth=0.2) for i in range(len(levels)-1)]
        legend = ax.legend(handles=patches,
                           title=r"雨(毫米)", title_fontsize=8,
                           borderpad=0.01, labelspacing=0.1,
                           handlelength=1.5, columnspacing=0.5,
                           loc='lower left',
                           ncol=2)
        legend.get_frame().set_edgecolor('black')
        legend.get_frame().set_linewidth(0.6)
        legend.get_frame().set_boxstyle('square')
        legend.get_frame().set_alpha(None)


        import matplotlib.patches as mpatches
        patches = [mpatches.Patch(facecolor=['#A2A0A3', '#716F72', '#464447', '#7345E3', '#4D0074'][i],
                                  label=[ '<2.5', '2.5$\sim$5', '5$\sim$10', '10$\sim$20', '≥20'][i],
                                  edgecolor='black', linewidth=0.2) for i in
                   range(len([ 0.01, 2.5, 5, 10, 20, 999]) - 1)]

        legend = plt.legend(handles=patches,
                            title=r"雪(毫米)", title_fontsize=8,
                            borderpad=0.01, labelspacing=0.1,
                            handlelength=1.5, columnspacing=1.05,
                            bbox_to_anchor=(-7.29,0.63),
                            loc='lower left',
                            ncol=2)
        legend.get_frame().set_edgecolor('black')
        legend.get_frame().set_linewidth(0.6)
        legend.get_frame().set_boxstyle('square')
        legend.get_frame().set_alpha(None)


        # add province
        def add_province_name(ax):
            pn = pd.read_csv(r"%s/province_captical_ll.txt"%shp_dir, usecols=[0,5,6])
            for i in range(pn.shape[0]):
                slon = pn['jingdu'][i]
                slat = pn['weidu'][i]
                s_p_c_name = pn['NAME'][i]
                from matplotlib.transforms import offset_copy

                ax.plot(slon, slat, marker='o', color='black', markersize=1,
                        transform=ccrs.Geodetic())

                geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
                text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

                # Add text 15 pixels to the left of the volcano.
                ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=5, weight='bold',
                        verticalalignment='top', horizontalalignment='center',
                        transform=text_transform,
                        )
        add_province_name(ax)

        import cartopy
        # ocean110 = cartopy.feature.NaturalEarthFeature('physical', 'ocean',
        #                                                scale='10m', edgecolor='none',zorder=-10,
        #                                                facecolor=cartopy.feature.COLORS['water'],)
        # ax.add_feature(ocean110)
        # ax2.add_feature(ocean110)

        if title_zyy == 'top':
            ax.set_title('国家灾研院\nNINH-MWF预报系统\n%i月%i日08时-%i月%i日08时' % (
                int(date_range[i_day - 1][4:6]), int(date_range[i_day - 1][6:8]),
                int(date_range[i_day][4:6]), int(date_range[i_day][6:8])),

                         fontsize=11, y=1.0, pad=-40)
        else:
            ax.set_title('%i月%i日08时-%i月%i日08时' % (
                int(date_range[i_day - 1][4:6]), int(date_range[i_day - 1][6:8]),
                int(date_range[i_day][4:6]), int(date_range[i_day][6:8])),
                         fontsize=11, y=1.0, pad=-30)
            ax.text(0, -0.022, r'国家灾研院NINH-MWF预报系统', transform=ax.transAxes,
                    fontsize=6, color='black')

        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig(workdir +'%s_%s.jpg' % (date_range[i_day - 1], title_zyy), dpi=400)
        plt.close()
        plt.clf()




