import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import os
import pandas as pd
from datetime import date, timedelta
import numpy as np
import time
import xarray as xr


path_nc = r"D:\pcp_prd_2024092000_24_ll.nc" # nc文件的绝对路径
var_name_nc = 'apcp_24' # 根据nc文件里变量名字替换， 比如画温度可能是t2m_24
path_savePic = r'D:/' #画出来的图片存哪个文件夹
unit_data = '毫米' #气温就是℃

date_range = ['20240929', '20240930']

#要画10月01日，请写 ['20240930', '20241001']
#要画10月02日，请写 ['20241001', '20241002'] 以此类推。
#要画10月01日，10月02日， 请写 ['20240930', '20241001', '20241002']...类推
#暂时不支持中途跳日子，如画10月01日，10月03日。


#修改颜色和对应的数据范围，可根据图片颜色提取颜色的网址在这 https://www.jiniannet.com/Page/allcolor
colors = ['#D4E8E9', '#C8FAFA', '#00E3D7', '#006496', '#001882', '#810083', '#FF00FF']
levels = [-999, 0.01, 10, 25, 50, 100, 250, 999]
# sim的意思是~,不需要就删掉。
level_labels = [r'无降水', '0$\sim$10', '10$\sim$25', '25$\sim$50', '50$\sim$100', '100$\sim$250', '≥250']


# shp_dir = r'E:/NINH/shp/'
shp_dir = r'G:\OneDrive\basis\data\shp\shengjie/'

data = xr.open_dataset(path_nc)
print(data)
lat = data.lat.values
lon = data.lon.values

apcp_all = data[var_name_nc]


for title_zyy in ['top', 'left']:
    for i_day, day_now in enumerate(date_range):
        if i_day == 0:
            continue
        apcp = apcp_all.sel(time=day_now).values

        plt.clf()
        SMALL_SIZE=7
        plt.rc('axes', titlesize=SMALL_SIZE)
        plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
        plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
        plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
        plt.rc('axes', titlepad=1, labelpad=1)
        plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
        plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
        plt.rc('xtick.major', size=2, width=0.5)
        plt.rc('xtick.minor', size=1.5, width=0.2)
        plt.rc('ytick.major', size=2, width=0.5)
        plt.rc('ytick.minor', size=1.5, width=0.2)
        plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
        plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

        fig, ax = plt.subplots(subplot_kw={'projection':ccrs.AlbersEqualArea(central_longitude=105)},
                               figsize=[5,4.6])
        plt.subplots_adjust(top=1.0,
        bottom=0.015,
        left=0.015,
        right=0.985,
        hspace=0.2,
        wspace=0.2)
        # colors = ['#FFFFFF','#A2F589', '#3BBB38', '#64B7FD', '#0200FD', '#FA00FB','#820040']

        def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0, additional_shp_lw=0.1):
            import cartopy.crs as ccrs
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader
            from matplotlib.patches import PathPatch
            def shp2clip(shpfile):
                import shapefile
                from matplotlib.path import Path
                sf = shapefile.Reader(shpfile)
                vertices = []
                codes = []
                for shape_rec in sf.shapeRecords():
                    pts = shape_rec.shape.points
                    prt = list(shape_rec.shape.parts) + [len(pts)]
                    for i in range(len(prt) - 1):
                        for j in range(prt[i], prt[i + 1]):
                            vertices.append((pts[j][0], pts[j][1]))
                        codes += [Path.MOVETO]
                        codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                        codes += [Path.CLOSEPOLY]
                    clip = Path(vertices, codes)
                return clip

            if clipshape == True:
                pathss = shp2clip(r'%sall_china.shp'%shp_dir)
                plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
                upath = PathPatch(pathss, transform=plate_carre_data_transform)

                for collection in fill.collections:
                    collection.set_clip_path(upath)

            shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sall_china.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%snanhai.shp'%shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)

        def plots(ax):

            cb = ax.contourf(lon, lat, apcp, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
            shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2)

        def plotss(ax):
            def del_ll(range, lat, lon):
                """
                cut lon and lat, and then return the index to cut data used later.
                :param range:
                :param lat:
                :param lon:
                :return: self.lat, self.lon, self.del_area
                """
                def ll_del(lat, lon, area):
                    import numpy as np
                    lat0 = lat - area[3]
                    slat0 = int(np.argmin(abs(lat0)))
                    lat1 = lat - area[2]
                    slat1 = int(np.argmin(abs(lat1)))
                    lon0 = lon - area[0]
                    slon0 = int(np.argmin(abs(lon0)))
                    lon1 = lon - area[1]
                    slon1 = int(np.argmin(abs(lon1)))
                    return [slat0, slat1, slon0, slon1]

                del_area = ll_del(lat, lon, range)

                if del_area[0] > del_area[1]:  # sort
                    del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
                if del_area[2] > del_area[3]:
                    del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

                lat = lat[del_area[0]:del_area[1] + 1]
                lon = lon[del_area[2]:del_area[3] + 1]
                return lat, lon, del_area
            def del_data(data, del_area):
                """
                link with del_ll, use self.del_area from function del_ll
                :param data:
                :return: cut data
                """
                return data[del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]

            lat_jdx, lon_jdx, range_jdx = del_ll([105, 122.5, 2.5, 25.8], lat, lon)
            apcp_jdx = del_data(apcp, range_jdx)
            cb = ax.contourf(lon_jdx, lat_jdx, apcp_jdx, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
            shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.2, additional_shp_lw=0.1)
            ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
            return cb

        plots(ax)
        ax.set_extent(([80, 128.5, 17, 54]), crs=ccrs.PlateCarree())

        ax2 = fig.add_axes([0, 0.1, 0.12, 0.28], projection=ccrs.AlbersEqualArea(central_longitude=105))
        contourf_jiuduanxian = plotss(ax2)
        from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
        ip = InsetPosition(ax, [0.875, -0.049, 0.12, 0.28])
        ax2.set_axes_locator(ip)

        import matplotlib.patches as mpatches
        patches = [mpatches.Patch(facecolor=colors[i], label=level_labels[i], edgecolor='black', linewidth=0.2) for i in range(len(levels)-1)]
        legend = ax.legend(handles=patches,
                           title=r"图例(%s)"%unit_data, title_fontsize=8,
                           borderpad=0.01, labelspacing=0.1,
                           handlelength=1.5, columnspacing=0.5,
                           loc='lower left',
                           ncol=2)

        legend.get_frame().set_edgecolor('black')
        legend.get_frame().set_linewidth(0.6)
        legend.get_frame().set_boxstyle('square')
        legend.get_frame().set_alpha(None)

        # add province
        def add_province_name(ax):
            pn = pd.read_csv(r"%s/province_captical_ll.txt"%shp_dir, usecols=[0,5,6])
            for i in range(pn.shape[0]):
                slon = pn['jingdu'][i]
                slat = pn['weidu'][i]
                s_p_c_name = pn['NAME'][i]
                from matplotlib.transforms import offset_copy

                ax.plot(slon, slat, marker='o', color='black', markersize=1,
                        transform=ccrs.Geodetic())

                geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
                text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

                # Add text 15 pixels to the left of the volcano.
                ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=5, weight='bold',
                        verticalalignment='top', horizontalalignment='center',
                        transform=text_transform,
                        )
        add_province_name(ax)


        ##############################!!!!!!!!!!制作标题！！！！可修改！！！！##################
        if title_zyy == 'top':
            ax.set_title('国家灾研院\nNINH-MWF预报系统\n%i月%i日08时-%i月%i日08时' % (
                int(date_range[i_day - 1][4:6]), int(date_range[i_day - 1][6:8]),
                int(date_range[i_day][4:6]), int(date_range[i_day][6:8])),
                        fontsize=11, y=1.0, pad=-40)
        else:
            ax.set_title('%i月%i日08时-%i月%i日08时' % (
                int(date_range[i_day - 1][4:6]), int(date_range[i_day - 1][6:8]),
                int(date_range[i_day][4:6]), int(date_range[i_day][6:8])),
                         fontsize=11, y=1.0, pad=-30)
            ax.text(0, -0.022, r'国家灾研院NINH-MWF预报系统', transform=ax.transAxes,
                    fontsize=6, color='black')
        ##############################!!!!!!!!!!制作标题！！！！可修改！！！！##################


        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        print('Done: saved %s'%(path_savePic +'%s_%s.jpg' % (date_range[i_day - 1], title_zyy)))
        plt.savefig(path_savePic +'%s_%s.jpg' % (date_range[i_day - 1], title_zyy), dpi=400)
        plt.close()
        plt.clf()


for i_day, day_now in enumerate(date_range):
    if i_day == 0:
        continue
    apcp = apcp_all.sel(time=day_now).values

    plt.clf()
    SMALL_SIZE = 7
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.AlbersEqualArea(central_longitude=105)},
                           figsize=[5, 4.6])
    plt.subplots_adjust(top=1.0,
                        bottom=0.015,
                        left=0.015,
                        right=0.985,
                        hspace=0.2,
                        wspace=0.2)


    def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0,
                             additional_shp_lw=0.1, add_NINH_area=False):
        import cartopy.crs as ccrs
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader
        from matplotlib.patches import PathPatch
        def shp2clip(shpfile):
            import shapefile
            from matplotlib.path import Path
            sf = shapefile.Reader(shpfile)
            vertices = []
            codes = []
            for shape_rec in sf.shapeRecords():
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append((pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
            return clip

        if clipshape == True:

            pathss = shp2clip(r'%sall_china.shp' % shp_dir)
            plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
            upath = PathPatch(pathss, transform=plate_carre_data_transform)

            for collection in fill.collections:
                collection.set_clip_path(upath)

        if add_NINH_area:
            shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, 11)).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.5, zorder=899)
            ax.add_feature(shape_feature)

            for i_shp in range(1, 10 + 1):
                print(i_shp)
                shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, i_shp)).geometries(),
                                               ccrs.PlateCarree(),
                                               edgecolor='red', facecolor='none', linewidths=1.5, zorder=999)
                ax.add_feature(shape_feature)

        else:

            shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sall_china.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%snanhai.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)


    def plots(ax):
        cb = ax.contourf(lon, lat, apcp, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2, add_NINH_area=True)


    plots(ax)
    ax.set_extent(([80, 128.5, 17, 54]), crs=ccrs.PlateCarree())

    import matplotlib.patches as mpatches

    patches = [mpatches.Patch(facecolor=colors[i], label=level_labels[i], edgecolor='black', linewidth=0.2) for i in
               range(len(levels) - 1)]
    legend = ax.legend(handles=patches,
                       title=r"图例(%s)"%unit_data, title_fontsize=8,
                       borderpad=0.01, labelspacing=0.1,
                       handlelength=1.5, columnspacing=0.5,
                       loc='lower left',
                       ncol=2)

    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_linewidth(0.6)
    legend.get_frame().set_boxstyle('square')
    legend.get_frame().set_alpha(None)


    # add province
    def add_province_name(ax):
        pn = pd.read_csv(r"%s/province_ll.txt" % shp_dir, usecols=[0, 5, 6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            ax.plot(slon, slat, marker='o', color='black', markersize=1,
                    transform=ccrs.Geodetic())

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=5, weight='bold',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform,
                    )

        pn = pd.read_csv(r"%s/region_ll.txt" % shp_dir, usecols=[0, 5, 6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=10, weight='bold', color='red',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform
                    )
    add_province_name(ax)


    ax.set_title('国家灾研院\nNINH-MWF预报系统\n%i月%i日08时-%i月%i日08时' % (
        int(date_range[i_day - 1][4:6]), int(date_range[i_day - 1][6:8]),
        int(date_range[i_day][4:6]), int(date_range[i_day][6:8])),

                 fontsize=11, y=1.0, pad=-40)

    plt.rcParams['font.sans-serif'] = ['SimHei']
    # plt.show()
    print('Done: saved %s' % (path_savePic + 'Ref_%s.jpg' % (date_range[i_day - 1])))
    plt.savefig(path_savePic + 'Ref_%s.jpg' % date_range[i_day - 1], dpi=400)
    plt.close()
    plt.clf()



