import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import numpy as np
def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    # def get_dataarray(data):
    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
    #     return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times

    for iI_vari, ivari in enumerate(var_name):

        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + '%s.nc' % file_name)
    return ds

a = xr.open_dataset("D:\pcp_prd_2024083000_24.nc")

data = a.apcp_24.values
lat = a.lat.values
lon = a.lon.values

b = save_standard_nc([data], ['apcp_24'], a.time, lon=lon[0,:], lat=lat[:,0],
                 path_save='D:/', file_name='pcp_prd__2024083000_24_ll')
