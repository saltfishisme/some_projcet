import ftplib
import os
import pandas as pd
from datetime import date, timedelta
import numpy as np
import time
import xarray as xr

dir_main = 'E:/NINH/'
rawData_reDown = False
pic_reDraw = False
# date
today = date.today()
if today.weekday() == 3:
    today = today
else:
    days_to_Sday = (today.weekday() - 3) % 7
    today = today - timedelta(days=days_to_Sday)

today = today+timedelta(days=1) # to Friday


date_range_dt_1week = pd.date_range(today + timedelta(days=1), today + timedelta(days=9 + 1), freq='1D')
date_range_1week = date_range_dt_1week.strftime('%Y%m%d')

date_range_dt = pd.date_range(today , today + timedelta(days=9 + 1), freq='1D')
date_range = date_range_dt.strftime('%Y%m%d')

date_today = today.strftime('%Y%m%d')

rawdata_dir = "%s/rawData/%s/" % (dir_main, date_today)
workdir = "%s/result/%s/"%(dir_main, date_today)
shp_dir = r'%s/shp/'%(dir_main)

os.makedirs(rawdata_dir, exist_ok=True)
os.makedirs(workdir, exist_ok=True)

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import xarray as xr

data = xr.open_dataset(workdir+"pcp_prd_%s00_24_ll.nc"%date_today)


lat = data.lat.values
lon = data.lon.values

apcp_all = data.sel(time=slice(date_range_1week[1], date_range_1week[-1])).apcp_24

def regrid(sparse_grid, x, y, new_x, new_y, meshgrid=False):
    import numpy as np
    from scipy.interpolate import griddata
    if meshgrid:
        x, y = np.meshgrid(x, y)
    x_flat = x.ravel()
    y_flat = y.ravel()
    z_flat = sparse_grid.ravel()
    new_x, new_y = np.meshgrid(new_x, new_y)
    return griddata((x_flat, y_flat), z_flat, (new_x, new_y), method='nearest')
data_sum = np.nansum(apcp_all.values, axis=0)

cli = xr.open_dataset(r"%s\pre_cli_cn05.nc"%shp_dir)
time_as_month_day = cli['day'].dt.strftime('%m-%d')

data_sum = regrid(data_sum, lon, lat, cli.lon.values, cli.lat.values, meshgrid=True)

lat = cli.lat.values
lon = cli.lon.values

cli_1week = cli.sel(day=cli['day'][time_as_month_day.isin(date_range_dt_1week.strftime('%m-%d')[:-1])]).sum(dim='day')['clm'].values

anomaly = 100*(data_sum-cli_1week)/cli_1week

def plot_anomaly(apcp):
    plt.clf()
    SMALL_SIZE = 7
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.AlbersEqualArea(central_longitude=105)},
                           figsize=[5, 4.6])
    plt.subplots_adjust(top=1.0,
                        bottom=0.015,
                        left=0.015,
                        right=0.985,
                        hspace=0.2,
                        wspace=0.2)

    def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0,
                             additional_shp_lw=0.1, add_NINH_area=False):
        import cartopy.crs as ccrs
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader
        from matplotlib.patches import PathPatch
        def shp2clip(shpfile):
            import shapefile
            from matplotlib.path import Path
            sf = shapefile.Reader(shpfile)
            vertices = []
            codes = []
            for shape_rec in sf.shapeRecords():
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append((pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
            return clip

        if clipshape == True:

            pathss = shp2clip(r'%sall_china.shp' % shp_dir)
            plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
            upath = PathPatch(pathss, transform=plate_carre_data_transform)

            for collection in fill.collections:
                collection.set_clip_path(upath)

        if add_NINH_area:

            shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, 11)).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.5, zorder=899)
            ax.add_feature(shape_feature)

            for i_shp in range(1, 10 + 1):
                print(i_shp)
                shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, i_shp)).geometries(),
                                               ccrs.PlateCarree(),
                                               edgecolor='k', facecolor='none', linewidths=2, zorder=999)
                ax.add_feature(shape_feature)


        else:

            shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sall_china.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%snanhai.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)

    def plots(ax):
        cb = ax.contourf(lon, lat, apcp,levels=levels, transform=ccrs.PlateCarree(), colors=colors)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2, add_NINH_area=True)


    plots(ax)
    ax.set_extent(([80, 128.5, 17, 54]), crs=ccrs.PlateCarree())

    import matplotlib.patches as mpatches

    patches = [mpatches.Patch(facecolor=colors[i], label=level_labels[i], edgecolor='black', linewidth=0.2) for i in
               range(len(levels) - 1)]
    legend = ax.legend(handles=patches,
                       title=r"图例(%)", title_fontsize=8,
                       borderpad=0.01, labelspacing=0.1,
                       handlelength=1.5, columnspacing=0.5,
                       loc='lower left',
                       ncol=2)

    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_linewidth(0.6)
    legend.get_frame().set_boxstyle('square')
    legend.get_frame().set_alpha(None)


    # add province
    def add_province_name(ax):
        pn = pd.read_csv(r"%s/province_ll.txt" % shp_dir, usecols=[0, 5, 6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            ax.plot(slon, slat, marker='o', color='black', markersize=1,
                    transform=ccrs.Geodetic())

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=5, weight='bold',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform,
                    )

        pn = pd.read_csv(r"%s/region_ll.txt"%shp_dir, usecols=[0,5,6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=7, weight='bold',color='black',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform, 
 bbox=dict(facecolor='none', edgecolor='black', boxstyle='round,pad=0.01')
                    )
	

    add_province_name(ax)

    # ocean110 = cartopy.feature.NaturalEarthFeature('physical', 'ocean',
    #                                                scale='10m', edgecolor='none',zorder=-10,
    #                                                facecolor=cartopy.feature.COLORS['water'],)
    # ax.add_feature(ocean110)
    # ax2.add_feature(ocean110)

    plt.rcParams['font.sans-serif'] = ['SimHei']
    ax.set_title('国家灾研院\nNINH-MWF预报系统\n周累积降水距平百分率',
                 fontsize=11, y=1.0, pad=-45)

    plt.savefig(workdir + 'Ref_anomaly_%s.jpg'%save_name, dpi=400)
    plt.close()
    plt.clf()

anomaly[anomaly>999] = 999
anomaly[anomaly<-100] = -100
taiwanmask = xr.open_dataset(r'%s/taiwan_mask_0.25.nc'%shp_dir)['tp'].values
anomaly[taiwanmask==1] = np.nan

colors = ['#FC1C19', '#E65F5E', '#FD986B', '#FFFFA1',
          '#CAFECC', '#3EC749', '#30FEFE', '#1895F9', '#0000F7']
levels = [-100, -80, -50, -20, 0, 20, 50, 100, 200, 999]
level_labels = [r'-100$\sim$-80', r'-80$\sim$-50', '-50$\sim$-20', '-20$\sim$0',
                '0$\sim$20', '20$\sim$50', '50$\sim$100', '100$\sim$200', '≥200']
save_name = 'max200'

plot_anomaly(anomaly)

colors = ['#FC1C19', '#E65F5E', '#FD986B', '#FFFFA1',
          '#CAFECC', '#3EC749', '#30FEFE', '#1895F9', '#0000F7']
levels = [-100, -80, -50, -20, 0, 50, 100, 200, 400, 999]
level_labels = [r'-100$\sim$-80', r'-80$\sim$-50', '-50$\sim$-20', '-20$\sim$0',
                '0$\sim$50', '50$\sim$100', '100$\sim$200', '200$\sim$400', '≥400']
save_name = 'max400'

plot_anomaly(anomaly)

"""
==============================================================
step2:
run and process

daily
==============================================================
"""
data = xr.open_dataset(workdir+"pcp_prd_%s00_24_ll.nc"%date_today)
lat = data.lat.values
lon = data.lon.values
apcp_all = data.apcp_24

#++++++++++++++++++++++++++++++++++++++!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
data_snow = xr.open_dataset(workdir+"snow_prd_%s00_24_ll.nc"%date_today)
snow_all = data_snow.apcp_24

colors = ['#C8FAFA', '#00E3D7', '#006496', '#001882', '#FF00FF']
# levels = [-999, 0.01, 10, 25, 50, 100, 250, 999]
# level_labels = [r'无降水', '0$\sim$10', '10$\sim$25', '25$\sim$50', '50$\sim$100', '100$\sim$250', '≥250']
levels = [0.01, 10, 25, 50, 100, 999]
level_labels = ['0$\sim$10', '10$\sim$25', '25$\sim$50', '50$\sim$100', '≥100']

for i_day, day_now in enumerate(date_range):
    if i_day == 0:
        continue
    apcp = apcp_all.sel(time=day_now).values
    snow = snow_all.sel(time=day_now).values

    plt.clf()
    SMALL_SIZE=7
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
    plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
    plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
    plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
    plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

    fig, ax = plt.subplots(subplot_kw={'projection':ccrs.AlbersEqualArea(central_longitude=105)},
                           figsize=[5,4.6])
    plt.subplots_adjust(top=1.0,
    bottom=0.015,
    left=0.015,
    right=0.985,
    hspace=0.2,
    wspace=0.2)
    # colors = ['#FFFFFF','#A2F589', '#3BBB38', '#64B7FD', '#0200FD', '#FA00FB','#820040']

    def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1.0,
                             additional_shp_lw=0.1, add_NINH_area=False):
        import cartopy.crs as ccrs
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader
        from matplotlib.patches import PathPatch
        def shp2clip(shpfile):
            import shapefile
            from matplotlib.path import Path
            sf = shapefile.Reader(shpfile)
            vertices = []
            codes = []
            for shape_rec in sf.shapeRecords():
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append((pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
            return clip

        if clipshape == True:

            pathss = shp2clip(r'%sall_china.shp' % shp_dir)
            plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
            upath = PathPatch(pathss, transform=plate_carre_data_transform)

            for collection in fill.collections:
                collection.set_clip_path(upath)

        if add_NINH_area:
            shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, 11)).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.5, zorder=899)
            ax.add_feature(shape_feature)
            
            for i_shp in range(1, 10 + 1):
                print(i_shp)
                shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, i_shp)).geometries(),
                                               ccrs.PlateCarree(),
                                               edgecolor='red', facecolor='none', linewidths=1.5, zorder=999)
                ax.add_feature(shape_feature)

        else:

            shape_feature = ShapelyFeature(Reader(r'%sbou2_4p.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=additional_shp_lw)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sall_china.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%sjiuduanxian.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'%snanhai.shp' % shp_dir).geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=shplinewidth)
            ax.add_feature(shape_feature)

    def plots(ax):
        cb = ax.contourf(lon, lat, apcp,levels=levels, transform=ccrs.PlateCarree(), colors=colors)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2, add_NINH_area=True)

        cb = ax.contourf(lon, lat, snow, levels=[0.001, 2.5, 5, 10, 20, 999], transform=ccrs.PlateCarree(),
                         colors=['#A2A0A3', '#716F72', '#464447', '#7345E3', '#4D0074'])
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.8, additional_shp_lw=0.2)

    def plotss(ax):
        def del_ll(range, lat, lon):
            """
            cut lon and lat, and then return the index to cut data used later.
            :param range:
            :param lat:
            :param lon:
            :return: self.lat, self.lon, self.del_area
            """
            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range)

            if del_area[0] > del_area[1]:  # sort
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area
        def del_data(data, del_area):
            """
            link with del_ll, use self.del_area from function del_ll
            :param data:
            :return: cut data
            """
            return data[del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]

        lat_jdx, lon_jdx, range_jdx = del_ll([105, 122.5, 2.5, 25.8], lat, lon)
        apcp_jdx = del_data(apcp, range_jdx)
        cb = ax.contourf(lon_jdx, lat_jdx, apcp_jdx, levels=levels, transform=ccrs.PlateCarree(), colors=colors)
        shp_clip_for_proplot(ax, fill=cb, clipshape=True, shplinewidth=0.2, additional_shp_lw=0.1)
        ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
        return cb

    plots(ax)
    ax.set_extent(([80, 128.5, 17, 54]), crs=ccrs.PlateCarree())

    ax2 = fig.add_axes([0, 0.1, 0.12, 0.28], projection=ccrs.AlbersEqualArea(central_longitude=105))
    contourf_jiuduanxian = plotss(ax2)
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.875, -0.049, 0.12, 0.28])
    ax2.set_axes_locator(ip)

    import matplotlib.patches as mpatches
    patches = [mpatches.Patch(facecolor=colors[i], label=level_labels[i], edgecolor='black', linewidth=0.2) for i in range(len(levels)-1)]
    legend = ax.legend(handles=patches,
                       title=r"雨(毫米)", title_fontsize=8,
                       borderpad=0.01, labelspacing=0.1,
                       handlelength=1.5, columnspacing=0.5,
                       loc='lower left',
                       ncol=2)

    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_linewidth(0.6)
    legend.get_frame().set_boxstyle('square')
    legend.get_frame().set_alpha(None)


    patches = [mpatches.Patch(facecolor=['#A2A0A3', '#716F72', '#464447', '#7345E3', '#4D0074'][i],
                              label=['<2.5', '2.5$\sim$5', '5$\sim$10', '10$\sim$20', '≥20'][i],
                              edgecolor='black', linewidth=0.2) for i in
               range(len([0.01, 2.5, 5, 10, 20, 999]) - 1)]
    legend = plt.legend(handles=patches,
                        title=r"雪(毫米)", title_fontsize=8,
                        borderpad=0.01, labelspacing=0.1,
                        handlelength=1.5, columnspacing=1.05,
                        bbox_to_anchor=(-7.29, 0.63),
                        loc='lower left',
                        ncol=2)
    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_linewidth(0.6)
    legend.get_frame().set_boxstyle('square')
    legend.get_frame().set_alpha(None)


    # add province
    def add_province_name(ax):
        pn = pd.read_csv(r"%s/province_ll.txt"%shp_dir, usecols=[0,5,6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            ax.plot(slon, slat, marker='o', color='black', markersize=1,
                    transform=ccrs.Geodetic())

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=5, weight='bold',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform,
                    )


        pn = pd.read_csv(r"%s/region_ll.txt"%shp_dir, usecols=[0,5,6])
        for i in range(pn.shape[0]):
            slon = pn['jingdu'][i]
            slat = pn['weidu'][i]
            s_p_c_name = pn['NAME'][i]
            from matplotlib.transforms import offset_copy

            geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
            text_transform = offset_copy(geodetic_transform, units='dots', y=-10)

            # Add text 15 pixels to the left of the volcano.
            ax.text(slon, slat, u'%s' % s_p_c_name, fontsize=10, weight='bold',color='red',
                    verticalalignment='top', horizontalalignment='center',
                    transform=text_transform
                    )

    add_province_name(ax)

    import cartopy
    # ocean110 = cartopy.feature.NaturalEarthFeature('physical', 'ocean',
    #                                                scale='10m', edgecolor='none',zorder=-10,
    #                                                facecolor=cartopy.feature.COLORS['water'],)
    # ax.add_feature(ocean110)
    # ax2.add_feature(ocean110)

    ax.set_title('国家灾研院\nNINH-MWF预报系统\n%i月%i日08时-%i月%i日08时' % (
        int(date_range[i_day - 1][4:6]), int(date_range[i_day - 1][6:8]),
        int(date_range[i_day][4:6]), int(date_range[i_day][6:8])),

                 fontsize=11, y=1.0, pad=-40)

    plt.rcParams['font.sans-serif'] = ['SimHei']
    # plt.show()
    plt.savefig(workdir +'Ref_%s.jpg' % date_range[i_day - 1], dpi=400)
    plt.close()
    plt.clf()



from PIL import Image
import os

from PIL import Image
import os
import math

def concat_images_grid(image_paths, output_path, images_per_row=3, bg_color=(255, 255, 255), padding=10):
    """
    Concatenates multiple images into a grid layout with specified number of images per row.

    :param image_paths: List of image paths to concatenate.
    :param output_path: Path to save the output image.
    :param images_per_row: Number of images per row.
    :param bg_color: Background color for padding. Default is white.
    :param padding: Padding between images.
    """
    images = [Image.open(img) for img in image_paths]

    # Calculate the number of rows needed
    num_rows = math.ceil(len(images) / images_per_row)

    # Get the maximum width and height for images
    max_width = max(img.width for img in images)
    max_height = max(img.height for img in images)

    # Calculate total width and height of the output image (with padding)
    total_width = images_per_row * max_width + (images_per_row - 1) * padding
    total_height = num_rows * max_height + (num_rows - 1) * padding

    # Create a new blank image with the total size
    output_img = Image.new('RGB', (total_width, total_height), bg_color)

    # Paste images into the output image in grid form
    for i, img in enumerate(images):
        row = i // images_per_row
        col = i % images_per_row
        x_offset = col * (max_width + padding)
        y_offset = row * (max_height + padding)
        output_img.paste(img, (x_offset, y_offset))

    # Save the final image
    output_img.save(output_path)
    print(f"Saved the concatenated image as {output_path}")



image_dir = workdir  # Replace with your directory
output_image = 'concat_pic_Ref.png'  # The path to save the output image


image_files = []
for i_image in os.listdir(image_dir):
    if (i_image.endswith('.jpg')) & ('anomaly' not in i_image)& ('Ref' in i_image):
        image_files.append(i_image)

# Full paths to your images
image_paths = [os.path.join(image_dir, img) for img in image_files]

# Concatenate images horizontally or vertically
concat_images_grid(image_paths, workdir+output_image, padding=10)


########################################################################

output_image = 'concat_pic.png'  # The path to save the output image


image_files = []
for i_image in os.listdir(image_dir):
    if (i_image.endswith('.jpg')) & ('anomaly' not in i_image)& ('Ref' not in i_image)& ('top' in i_image):
        image_files.append(i_image)

# Full paths to your images
image_paths = [os.path.join(image_dir, img) for img in image_files]

# Concatenate images horizontally or vertically
concat_images_grid(image_paths, workdir+output_image, padding=10)
