import os
import numpy as np
import scipy.io as scio

def loadmat(path):
    import mat73
    import scipy.io as scio
    try:
        data = mat73.loadmat(path)
    except:
        data = scio.loadmat(path)
    return data

def get_pw_eul(path_mat, mask_sta=None):
    """
    for 19841212_1-level-int_trajN.mat file
    :param path_mat:
    :param mask_sta:
    :return:
    """
    dt_int=6 * 3600 # time interval of the time interpolation
    t_total= 15 * 24 * 3600 # the total time length of each trajectory. Note: not exceeding the maximal tracking time

    t_int = np.arange(0, t_total+0.01, dt_int)
    area_S = loadmat("/home/linhaozhong/work/AR_NP85_1940/GridAL_ERA0_5degGridC.mat")['S']
    inputinfo = loadmat(r"/home/linhaozhong/work/AR_NP85_1940/NP-5deg_Inputinfo.mat")

    lon_H_sta = inputinfo['lon_H'][0,:]
    lat_H_sta = inputinfo['lat_H'][:,0]
    ### get subregion idx in row region
    seedidx_row = inputinfo['seedidx']
    seedidx = inputinfo['seedidx']-1

    # load mat
    data = loadmat(path_mat)
    print(path_mat)
    print(data)

    if mask_sta is not None:
        seedidx = np.argwhere(mask_sta>0)
        seedidx_index = (seedidx_row[:, None] == seedidx+1).all(-1).any(-1)
        for ikey in data.keys():
            if ikey[:1] == '_':
                continue

            ivalue = data[ikey]
            try:
                if ivalue.shape[-1] == seedidx_row.shape[0]:
                    data[ikey] = np.array(ivalue[..., seedidx_index], dtype='double')
            except:
                continue

    traj_pw = data['traj_PW']
    traj_rr = data['traj_rr']
    p_lonlat = data['plonlat']
    tlev_iter = np.squeeze(data['tlev_iter'])
    tlev_iter = np.array([int(i) for i in tlev_iter])
    traj_time = data['traj_time']

    # get PWm_all
    pw_all_area = 0
    pw_all = 0
    for i_traj in range(traj_pw.shape[-1]):

        pw_all_area += traj_pw[0, i_traj] * area_S[seedidx[i_traj,0],seedidx[i_traj,1]]
        pw_all += traj_pw[0, i_traj]


    new_row = traj_rr[-1,:]
    traj_rr = np.vstack((traj_rr, new_row))

    traj_rho = np.zeros(traj_rr.shape)
    for i_traj in range(traj_rho.shape[-1]):
        traj_rr[tlev_iter-1, i_traj] = traj_rr[tlev_iter-2, i_traj]
        traj_rho[:, i_traj] = traj_rr[:, i_traj] * area_S[seedidx[i_traj,0],seedidx[i_traj,1]] * traj_pw[0, i_traj]/pw_all_area

    pw_eul = np.zeros(area_S.shape)
    rho_loc = np.zeros([len(t_int), traj_pw.shape[-1]])
    pwm_loc = np.zeros(rho_loc.shape)
    pos_int = np.zeros([2,rho_loc.shape[0], rho_loc.shape[1]])
    from scipy import interpolate
    rho_int = np.ones(rho_loc.shape)
    for i_traj in range(traj_pw.shape[-1]):
        t_lev = int(tlev_iter[i_traj])

        pos_org = np.squeeze(p_lonlat[:,0:t_lev, i_traj])
        func = interpolate.interp1d(traj_time[:t_lev, i_traj], np.squeeze(pos_org[0]),kind='nearest')
        pos_int[0, :, i_traj] = func(t_int)
        func = interpolate.interp1d(traj_time[:t_lev, i_traj], np.squeeze(pos_org[1]),kind='nearest')
        pos_int[1, :, i_traj] = func(t_int)

        traj_rho_squeeze = np.squeeze(traj_rho[0:t_lev, i_traj])
        func = interpolate.interp1d(traj_time[:t_lev, i_traj], traj_rho_squeeze,kind='nearest')
        rho_int[:, i_traj] = func(t_int)

        rho_loc[0, i_traj] = rho_int[0,i_traj]
        rho_loc[1:, i_traj] = np.diff(rho_int[:, i_traj])

        pwm_loc[:, i_traj] = pw_all*rho_loc[:, i_traj]

        # print('################################')
        for i_date in range(rho_loc.shape[0]):
            lon_now = pos_int[0,i_date, i_traj]
            lat_now = pos_int[1, i_date, i_traj]

            index = [0, 0]
            index[1] = np.argmin(abs(lon_H_sta - lon_now))
            index[0] = np.argmin(abs(lat_H_sta - lat_now))
            pw_eul[index[0], index[1]] += pwm_loc[i_date, i_traj]
    return pw_eul

#
# # # load data from all AR moisture mat files
# # path_MoisturematFile = r'/home/linhaozhong/work/AR_NP85_1940/moisture_track/'
# #
# # pwm_int_all = []
# # file_name = []
# # for i_file in os.listdir(path_MoisturematFile):
# #     i_file = os.path.join(path_MoisturematFile, i_file)
# #     matfile = os.listdir(i_file)
# #     print(matfile)
# #     for i_matfile in matfile:
# #         if 'res_6hr' in i_matfile:
# #             data = mat73.loadmat(i_file+'/'+i_matfile)
# #
# #             file_name.extend(i_matfile[:8])
# #             pwm_int_all.append(data['PW_eul'])
# #
# #
# # scio.savemat(r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/pwm_loc.mat',
# #              {'file':file_name,
# #               'pwm_loc':np.array(pwm_int_all,dtype='double'), })
# # a = scio.loadmat(r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/loc_filename_eul.mat')
# # print(a['rho_loc_eul'].shape)
# # print(a['file'].shape)
# # exit()
# path_MoisturematFile = r'/home/linhaozhong/work/AR_NP85_1940/moisture_track/'
# pos_int_all = []
# rr_int_all = []
# rho_int_all = []
# pwm_int_all = []
# file_name = []
# for i_file in os.listdir(path_MoisturematFile):
#     i_file = os.path.join(path_MoisturematFile, i_file)
#     matfile = os.listdir(i_file)
#     print(matfile)
#     for i_matfile in matfile:
#         if 'res_6hr' in i_matfile:
#             data = loadmat(i_file+'/'+i_matfile)
#             # file_name.extend(np.repeat(i_matfile[:8], data['rr_loc'].shape[1]))
#             # print(file_name)
#             # pos_int_all.extend(np.transpose(data['pos_int'], [2,0,1]))
#             file_name.append(i_matfile[:8])
#             # rho_int_all.append(data['rho_loc_eul_pwm'])
#             pwm_int_all.append(data['traj_den_eul'])
#
# # nc4.save_nc4(np.array(pos_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/pos_int')
# # nc4.save_nc4(np.array(rr_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/rr_loc')
# # nc4.save_nc4(np.array(rho_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/rho_loc')
# # nc4.save_nc4(np.array(pwm_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/pwm_loc')
# scio.savemat(r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/loc_filename_traj_den_eul.mat',
#              {'file':file_name,
#               # 'rho_loc_eul':np.array(rho_int_all,dtype='double'),
#               # 'rr_loc_eul':np.array(rr_int_all,dtype='double'),
#               'traj_den_eul':np.array(pwm_int_all,dtype='double'), })
# a = scio.loadmat(r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/loc_filename_eul.mat')
# print(a['rho_loc_eul'].shape)
# print(a['file'].shape)
# exit()

"""
to yearly 
"""

import nc4

year_begin = 1940
year_end = 2022
data_yearly = np.zeros([year_end-year_begin+1, 360, 720])
data_yearly_count = np.zeros([year_end-year_begin+1, 360, 720])
# for var in ['pwm_loc_eul', 'rho_loc_eul']:
for var in ['traj_den_eul']:
    for cal_type in ['mean', 'sum']:
        main_dir = r'G:\OneDrive\basis\some_projects\zhong\AR_DRM\mositure_contribution/'
        # seed * timestep

        rr_int_all = scio.loadmat(main_dir+r'/loc_filename_traj_den_eul.mat') # as savename
        # pwm_loc, rr_loc, rho_loc, file

        file_mat = rr_int_all['file']
        # file_mat = np.reshape(file_mat, [1035, 8])
        date_loc = [''.join(string_list) for string_list in file_mat]

        date_loc = np.array([int(i[:4]) + 1 if i[4:6] == '12' else int(i[:4]) for i in date_loc])
        print(date_loc.shape)
        rr_loc = rr_int_all[var]
        print(rr_loc.shape)
        for iI_loc, i_rr in enumerate(date_loc):
            data_yearly[date_loc[iI_loc]-year_begin] += rr_loc[iI_loc]
            data_yearly_count[date_loc[iI_loc]-year_begin][rr_loc[iI_loc]>0] += 1

        if cal_type == 'mean':
            data_yearly = data_yearly/data_yearly_count
        nc4.save_nc4(data_yearly, main_dir+'%s_%s_yearly'%(var, cal_type))
exit()


"""
plot PWm 1940-2022 distribution, mean by yearly
"""

import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import numpy as np


def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1, colors_shape[0]])
    return ListedColormap(np.concatenate((colors, ones.T), axis=1))
cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')

import nc4
import scipy.io as scio

long = np.arange(0.25, 360, 0.5)
lat = np.arange(89.75, -90.01, -0.5)

SMALL_SIZE = 6

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

fig = plt.figure(figsize=[6.5,3])

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']


def plot_quiver():
    plt.rcParams['hatch.color'] = 'darkred'
    ax = fig.add_subplot(1,1, 1,  projection=ccrs.PlateCarree(central_longitude=100))

    # i_contour_cut, z, z_ano= data
    z = data1

    import cmaps
    import matplotlib.colors as colors
    cb = ax.contourf(long, lat,
                     np.array((z),dtype='double'),levels=np.arange(0,5.5,0.2),extend='max',cmap=cmpas_zhong,
                    transform=ccrs.PlateCarree())
    # plt.colorbar(cb, orientation="horizontal")
    # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
    #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
    #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
    import cmaps
    # cb = ax.contourf(long, lat,
    #                  uv_in_AR(u, v, i_contour_cut), extend='both', levels=np.arange(0, 15.1, 0.5),
    #                  transform=ccrs.PlateCarree(), cmap=cmaps.cmocean_matter, zorder=3)
    # plt.colorbar(cb)
    # ax.quiver(long[::delta], lat[::delta_lat],
    #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
    #           transform=ccrs.PlateCarree(), scale=200, zorder=7)
    ax.coastlines(zorder=9)
    ax.gridlines()
    ax.set_extent([0, 359, -5, 90], crs=ccrs.PlateCarree())
    return cb


data1 = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_DRM\mositure_contribution\pwm_loc_eul_mean_yearly")
data1 = np.nanmean(data1, axis=0)

cb = plot_quiver()

cb_ax = fig.add_axes([0.25, 0.15, 0.5, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
cb_ax.set_xlabel('mm/day')
plt.show()

