import os
import numpy as np
# import cartopy.crs as ccrs
import pandas as pd
import scipy.io as scio
import nc4
# import matplotlib.pyplot as plt
# import cmaps
# import cartopy.feature as cfeature
# import sys
# import cmaps
# from cartopy.util import add_cyclic_point


def plot_centroid_eul(save_labels, longpath, savename=''):
    def flatten_trajectorytoEuler(data, lon, lat):
        """
        :param data: [event,2(lon,lat),trajectory],
        :param lon:
        :param lat:
        :return:
        """
        print(data.shape)
        data_eul = np.zeros([len(lat), len(lon)])
        for i_event in range(data.shape[0]):
            for i_date in range(data.shape[2]):
                lon_now = data[i_event, 1, i_date]
                lat_now = data[i_event, 0, i_date]
                print(lon_now, lat_now)
                index = [0, 0]
                index[1] = np.argmin(abs(lon - lon_now))
                index[0] = np.argmin(abs(lat - lat_now))
                data_eul[index[0], index[1]] += 1
        return data_eul

    size = int(np.ceil(len(np.unique(save_labels)) / 3))
    fig = plt.figure()

    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            ax = fig.add_subplot(size, 3, iI + 1, projection=ccrs.NorthPolarStereo())
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
            lon = np.arange(-180, 180, 0.5)
            lat = np.arange(90, -90.1, -0.5)
            centroid_eul = flatten_trajectorytoEuler(longpath[save_labels == i], lon, lat)

            ax.pcolormesh(lon, lat, centroid_eul, transform=ccrs.PlateCarree())
            ax.set_title('Type %i %i, %0.0f%%' % (
            i, longpath[save_labels == i].shape[0], 100 * longpath[save_labels == i].shape[0] / longpath.shape[0]))
            ax.coastlines()
    plt.tight_layout()
    plt.show()
    # plt.savefig(path_pic+'%s_%s.png'%(season, savename), dpi=500)
    plt.close()
    # # # plot classification result
    # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    # for i in np.unique(save_labels[save_labels == 0]):
    #     ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    # ax.coastlines()
    # plt.title(season)
    # plt.show()
    # plt.close()


def plot_trajectory_with_rr(ax, pos_int, rr_int):
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1, colors_shape[0]])
        return ListedColormap(np.concatenate((colors, ones.T), axis=1))

    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    path_color_zhong = 'D:/MATLAB/colormapdata/rainbow200.mat'
    cmpas_zhong = call_colors_in_mat(path_color_zhong, 'rainbow200')

    def plot_colored(ax, x, y, data, cmap, steps=1):
        '''
        :param ax: axes
        :param x: (N,) np.array
        :param y: (N,) np.array
        :param data: (N,) np.array
        :param cmap:
        :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
        :return:
        '''
        ''' from matplotlib import cm
        cmpa should be    cm.get_cmap(cmpas_zhong, 100)

         $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
         before plot all data, please normalized all this data.'''
        data = np.asarray(data)
        it = 0
        while it < (data.size - steps):
            x_segm = x[it:it + steps + 1]
            y_segm = y[it:it + steps + 1]

            c_segm = cmap(data[it + steps // 2])
            ax.plot(x_segm, y_segm, c=c_segm, transform=ccrs.Geodetic(), linewidth=0.2)
            it += steps

    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len + 1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :] = np.arange(0, 101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb

    ####### plot colorbar in given cmap  ##########
    cb_ax = fig.add_axes([0.1, 0.02, 0.8, 0.01])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    cb_ax.set_axes_locator(ip)
    cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
    cbar.set_label('%')
    cbar.set_ticks(np.arange(0, 101, 25))
    ####### plot trajectories by colored lines   ##########
    from matplotlib import cm
    viridis = cm.get_cmap(cmpas_zhong, 500)
    for i in range(pos_int.shape[0])[::lineplot]:
        # for i in range(200):
        plot_colored(ax, pos_int[i, 0, :], pos_int[i, 1, :], rr_int[i, :], cmap=cmpas_zhong)
        ####### plot end of trajectories with black dot  ##########
        ax.scatter(pos_int[i, 0, 0], pos_int[i, 1, 0], c='k', s=1, marker='.', linewidths=0.5,
                   transform=ccrs.PlateCarree())

    ################################# add some feature #################################

    ax.coastlines(linewidth=0.5)


# print("???????////////")
# import mat73
# # load data from all AR moisture mat files
# path_MoisturematFile = r'/home/linhaozhong/work/AR_NP85_1940/moisture_track/'
# pos_int_all = []
# rr_int_all = []
# rho_int_all = []
# pwm_int_all = []
# file_name = []
# for i_file in os.listdir(path_MoisturematFile):
#     i_file = os.path.join(path_MoisturematFile, i_file)
#     matfile = os.listdir(i_file)
#     print(matfile)
#     for i_matfile in matfile:
#         if 'res_6hr' in i_matfile:
#             data = mat73.loadmat(i_file+'/'+i_matfile)
#             file_name.extend(np.repeat(i_matfile[:8], data['rr_loc'].shape[1]))
#             # print(file_name)
#             pos_int_all.extend(np.transpose(data['pos_int'], [2,0,1]))
#             rr_int_all.extend(np.transpose(data['rr_loc'], [1,0]))
#             rho_int_all.extend(np.transpose(data['rho_loc'], [1,0]))
#             pwm_int_all.extend(np.transpose(data['PWm_loc'], [1,0]))
#
# nc4.save_nc4(np.array(pos_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/pos_int')
# # nc4.save_nc4(np.array(rr_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/rr_loc')
# # nc4.save_nc4(np.array(rho_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/rho_loc')
# # nc4.save_nc4(np.array(pwm_int_all), r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/pwm_loc')
# scio.savemat(r'/home/linhaozhong/work/AR_NP85_1940/mositure_contribution/loc_filename.mat',
#              {'file':file_name,
#               'rho_loc':np.array(rho_int_all,dtype='double'),
#               'rr_loc':np.array(rr_int_all,dtype='double'),
#               'pwm_loc':np.array(pwm_int_all,dtype='double'), })
# exit()

############### analysis #########
var = 'pwm_loc'
cal_type = 'sum'

for var in ['pwm_loc', 'rr_loc', 'rho_loc']:
    for cal_type in ['mean', 'sum']:
        main_dir = r'G:\OneDrive\basis\some_projects\zhong\AR_DRM\mositure_contribution/'
        # seed * timestep
        pos_loc = nc4.read_nc4(main_dir + r'/pos_int')
        print(pos_loc.shape)
        rr_int_all = scio.loadmat(main_dir+r'/loc_filename.mat')
        # pwm_loc, rr_loc, rho_loc, file

        file_mat = rr_int_all['file']

        date_loc = [int(i[:4]) + 1 if i[4:6] == '12' else int(i[:4]) for i in file_mat]
        rr_loc = rr_int_all[var]
        pos_loc = pos_loc
        print(rr_loc.flatten().shape)
        print(pos_loc[:,1,:].flatten().shape)
        print(np.repeat(date_loc, rr_loc.shape[-1]).shape)
        df = pd.DataFrame(np.array([rr_loc.flatten()*100,
                                    pos_loc[:,1,:].flatten(),
                                    np.repeat(date_loc, rr_loc.shape[-1])]).T,
                          columns=['rr', 'lat', 'year'])

        bins = np.array([-99]+list(range(5, 90+1, 1)))  # [0, 10, 20, 30, ..., 90]
        labels = bins[1:]       # [5, 15, 25, ..., 85] 代表中心值

        df['lat_bin'] = pd.cut(df['lat'], bins=bins, labels=labels, right=False)
        if cal_type == 'sum':
            df = df['rr'].groupby([df['lat_bin'], df['year']]).sum().reset_index()
        if cal_type == 'mean':
            df = df['rr'].groupby([df['lat_bin'], df['year']]).mean().reset_index()

        df.to_csv('%s_%s_latcut.csv'%(var, cal_type))

exit()

for var in ['pwm_loc', 'rr_loc', 'rho_loc']:
    for cal_type in ['mean', 'sum']:
        df = pd.read_csv('%s_%s_latcut.csv'%(var, cal_type))

        import matplotlib.pyplot as plt
        pivot_table = df.pivot_table(values='rr', index='lat_bin', columns='year')
        # print(pivot_table)
        # rr_array = pivot_table.to_numpy()
        # plt.imshow(rr_array)
        # plt.show()
        # # 使用 pcolormesh 进行可视化
        X, Y = np.meshgrid(pivot_table.columns, pivot_table.index)
        # 绘制图像
        plt.figure(figsize=(8, 6))
        from scipy.ndimage import gaussian_filter
        plt.pcolormesh(X, Y, gaussian_filter(pivot_table.to_numpy(),sigma=2),
                       shading='auto', cmap='coolwarm_r')

        # 添加颜色条
        plt.colorbar(label=var)

        # 设置标签
        plt.xlabel('Year')
        plt.ylabel('Latitude')
        plt.title('%s %s'%(var, cal_type))

        plt.savefig('%s_%s_latcut.png'%(var, cal_type), dpi=400)
        plt.close()

