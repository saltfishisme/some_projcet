import os
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4
from scipy.ndimage import gaussian_filter

class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps

        self.ax = ax
        self.ranges = ranges

        def del_ll(range, lat, lon):
            """
            cut lon and lat, and then return the index to cut data used later.
            :param range:
            :param lat:
            :param lon:
            :return: self.lat, self.lon, self.del_area
            """

            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range[0])

            if del_area[0] > del_area[1]:  # sort
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        if self.ranges[0] is not False:
            self.lat, self.lon, self.del_area = del_ll(ranges, lat, lon)
        else:
            self.lat, self.lon, self.del_area = [lat, lon, False]

        self.crs = ccrs.PlateCarree()
        self.MidPointNorm = False
        self.print_min_max = False

        self.colorbar_fmt = '%.1f'
        self.colorbar = True
        self.colorbar_orientation = 'h'
        self.ranges_reconstrcution_button_contourf = False
        self.contourf_arg = {}

        self.contour_fmt = '%.1f'
        self.ranges_reconstrcution_button_contour = False
        self.contour_clabel_color = 'white'
        self.contour_arg = {'colors': 'black'}
        self.colorbar_clabel_size = 5
        self.contour_clabel= True


        self.quiver_spacewind = [1, 1]
        self.quiverkey_arg = {'X': 0.85, 'Y': 1.05, 'U': 10, 'label': r'$10 \frac{m}{s}$'}
        self.quiver_arg = {}


    def del_data(self, data):
        """
        link with del_ll, use self.del_area from function del_ll
        :param data:
        :return: cut data
        """
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1] + 1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        """
        :param data:
        :param ranges_reconstrcution_button:
        :return: self.levels, self.MidPointNorm
        """
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        class MidPointNorm(Normalize):

            def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                Normalize.__init__(self, vmin, vmax, clip)
                self.midpoint = midpoint

            def __call__(self, value, clip=None):
                if clip is None:
                    clip = self.clip

                result, is_scalar = self.process_value(value)

                self.autoscale_None(result)
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if not (vmin < midpoint < vmax):
                    raise ValueError("midpoint must be between maxvalue and minvalue.")
                elif vmin == vmax:
                    result.fill(0)  # Or should it be all masked? Or 0.5?
                elif vmin > vmax:
                    raise ValueError("maxvalue must be bigger than minvalue")
                else:
                    vmin = float(vmin)
                    vmax = float(vmax)
                    if clip:
                        mask = ma.getmask(result)
                        result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                          mask=mask)

                    # ma division is very slow; we can take a shortcut
                    resdat = result.data

                    # First scale to -1 to 1 range, than to from 0 to 1.
                    resdat -= midpoint
                    resdat[resdat > 0] /= abs(vmax - midpoint)
                    resdat[resdat < 0] /= abs(vmin - midpoint)

                    resdat /= 2.
                    resdat += 0.5
                    result = ma.array(resdat, mask=result.mask, copy=False)

                if is_scalar:
                    result = result[0]
                return result

            def inverse(self, value):
                if not self.scaled():
                    raise ValueError("Not invertible until scaled")
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if cbook.iterable(value):
                    val = ma.asarray(value)
                    val = 2 * (val - 0.5)
                    val[val > 0] *= abs(vmax - midpoint)
                    val[val < 0] *= abs(vmin - midpoint)
                    val += midpoint
                    return val
                else:
                    val = 2 * (value - 0.5)
                    if val < 0:
                        return val * abs(vmin - midpoint) + midpoint
                    else:
                        return val * abs(vmax - midpoint) + midpoint
        def ranges_create(begin, end, inter):


            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0 + inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))

        # if range is given
        if isinstance(ranges_reconstrcution_button, list) or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button


        else:
            ranges_min = np.nanmin(data)
            ranges_max = np.nanmax(data)

            if ranges_reconstrcution_button is False:  # default if False
                if ranges_min > 0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                else:
                    ticks = (ranges_max - ranges_min) / 30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        class MidPointNorm(Normalize):

            def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                Normalize.__init__(self, vmin, vmax, clip)
                self.midpoint = midpoint

            def __call__(self, value, clip=None):
                if clip is None:
                    clip = self.clip

                result, is_scalar = self.process_value(value)

                self.autoscale_None(result)
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if not (vmin < midpoint < vmax):
                    raise ValueError("midpoint must be between maxvalue and minvalue.")
                elif vmin == vmax:
                    result.fill(0)  # Or should it be all masked? Or 0.5?
                elif vmin > vmax:
                    raise ValueError("maxvalue must be bigger than minvalue")
                else:
                    vmin = float(vmin)
                    vmax = float(vmax)
                    if clip:
                        mask = ma.getmask(result)
                        result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                          mask=mask)

                    # ma division is very slow; we can take a shortcut
                    resdat = result.data

                    # First scale to -1 to 1 range, than to from 0 to 1.
                    resdat -= midpoint
                    resdat[resdat > 0] /= abs(vmax - midpoint)
                    resdat[resdat < 0] /= abs(vmin - midpoint)

                    resdat /= 2.
                    resdat += 0.5
                    result = ma.array(resdat, mask=result.mask, copy=False)

                if is_scalar:
                    result = result[0]
                return result

            def inverse(self, value):
                if not self.scaled():
                    raise ValueError("Not invertible until scaled")
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if cbook.iterable(value):
                    val = ma.asarray(value)
                    val = 2 * (val - 0.5)
                    val[val > 0] *= abs(vmax - midpoint)
                    val[val < 0] *= abs(vmin - midpoint)
                    val += midpoint
                    return val
                else:
                    val = 2 * (value - 0.5)
                    if val < 0:
                        return val * abs(vmin - midpoint) + midpoint
                    else:
                        return val * abs(vmax - midpoint) + midpoint
        try:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       norm=MidPointNorm(0), **self.contourf_arg)

        except:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       **self.contourf_arg)

        if self.colorbar:
            if self.colorbar_orientation == 'h':
                orientation = 'horizontal'
            else:
                orientation = 'vertical'

            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.colorbar_fmt)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)

        if self.contour_clabel:
            cbs = self.ax.clabel(
                cb, fontsize=self.colorbar_clabel_size,  # Typically best results when labelling line contours.
                colors=['black'],
                inline=True,  # Cut the line where the label will be placed.
                fmt=self.contour_fmt,  # Labes as integers, with some extra space.
            )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.quiver_spacewind[0]], self.lat[::self.quiver_spacewind[1]],
                             u[::self.quiver_spacewind[1], ::self.quiver_spacewind[0]], v[::self.quiver_spacewind[1], ::self.quiver_spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)

def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1,colors_shape[0]])
    return  ListedColormap(np.concatenate((colors, ones.T), axis=1))


path_MainData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result_AR\\'


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
type_name = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name = ['greenland_east','greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE', 'BAF','BSE', 'BSW',
             'MED', 'ARC']
var_SaveName_all = ['Hgt']

var_ShortName = ['z']
resolution=0.5
import cmaps
cmaps_here = cmaps.prcp_1

for var in ['tcwv']:

    def cal_quiver_var_plot(ax, data, centroid):
        from cartopy.util import add_cyclic_point

        data, lon = add_cyclic_point(data, coord=np.arange(0, 360, 0.5))

        if var == 'IVT':
            print([centroid[0]-50, centroid[0]+50,
                                    centroid[1]-20, centroid[1]+20])
            base_ax = circulation(ax, lon, np.arange(90, -90.001, -0.5),
                                  [False,
                                   [], []])
        else:
            base_ax = circulation(ax, lon, np.arange(90, -90.001, -0.5),
                                  [False, np.arange(0, 360, 90), [50, 60, 70, 80]])
        base_ax.colorbar_orientation = 'h'
        # base_ax.contourf_arg = {'cmap':call_colors_in_mat(r"D:\MATLAB\colormapdata\Tano.mat", 'Tano'),
        #                         }

        base_ax.contourf_arg = {'cmap':cmaps_here,
                                }
        base_ax.colorbar_fmt='%i'

        if var == 'IVT':
            base_ax.ranges_reconstrcution_button_contourf = np.arange(50,max_data,10)
        else:
            base_ax.ranges_reconstrcution_button_contourf = np.arange(0.01,6.01,0.25)
        base_ax.colorbar = False
        data = data
        base_ax.p_contourf(data)

        return base_ax.cb

    def get_data():
        def roll_contour(contour, centroid):
            for i in range(contour.shape[0]):
                for j in range(contour.shape[1]):
                    roll_lat = [-int(centroid[i,j][0] / resolution)]
                    roll_lon = [-int((180 - centroid[i,j][1]) / resolution)]
                    contour[i,j] = np.roll(contour[i,j], roll_lat, 0)
                    contour[i,j] = np.roll(contour[i,j], roll_lon, 1)
            return contour

        data_circulation = []
        centroid = []
        for type_SOM in type_name:
            data_circulation.append(nc4.read_nc4(path_MainData+'all_%s_%s_%s'%(var, 'DJF',type_SOM)))
            centroid.append(nc4.read_nc4(path_MainData+'all_shape_%s_%s'%('DJF',type_SOM)))

        data_circulation = roll_contour(np.array(data_circulation), np.array(centroid))
        return data_circulation, centroid

    data_circulation, centroid = get_data()

    max_data = int(np.max(data_circulation[:,:,:60,:].flatten()))
    min_data = int(np.min(data_circulation[:,:,:60,:].flatten()))
    mm_data = np.max(np.abs([min_data, max_data]))


    SMALL_SIZE = 8

    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
    fig = plt.figure(figsize=[3.5, 7])

    plt.subplots_adjust(top=0.895,
bottom=0.075,
left=0.04,
right=0.98,
hspace=0.09,
wspace=0.0)
    axs = []
    central_lon = [0, 300, 180, 180, 75, 75]
    central_extend = [
        [-50,50, 20, 90],

        ]
    for level in range(6):
        for i_ax in range(3):
            # ax = fig.add_subplot(6, 3, 3*level+i_ax+1,
            #                       projection=ccrs.Orthographic(central_lon[level], 70))
            ax = fig.add_subplot(6, 3, 3*level+i_ax+1,
                                  projection=ccrs.LambertConformal(central_lon[level], 70))

            # ax.set_global()
            ax.set_extent([central_lon[level]-50,int((central_lon[level]+50)%360), 20, 90], crs=ccrs.PlateCarree())
            axs.append(ax)
            cb = cal_quiver_var_plot(ax, data_circulation[level][i_ax], [centroid[0][i_ax][1],
                                  centroid[0][i_ax,0]])
            centroid_now = [centroid[level][i_ax][1], centroid[level][i_ax,0]]
            ax.scatter(centroid_now[0], centroid_now[1], s=8, c='black',transform=ccrs.PlateCarree())
            from matplotlib.patches import Rectangle

            def long_meters_at_lat(lat):
                """Calculate distance (in meters) between longitudes at a given latitude."""
                a = 6378137.0
                b = 6356752.3142
                e_sqr = a ** 2 / b ** 2 - 1
                lat = lat * 2 * np.pi / 360
                return np.pi * a * np.cos(lat) / (180 * np.power(1 - e_sqr * np.square(np.sin(lat)), .5))


            # rec_point = ax.projection.transform_point(centroid_now[0], centroid_now[1], ccrs.PlateCarree())
            # width = 4500000
            # length = 4500000
            # rec = Rectangle([rec_point[0]-width/2, rec_point[1]-length/2],width, length,facecolor='None',
            #                              edgecolor='#e7837a', linewidth=1)
            # ax.add_patch(rec)
            ax.coastlines(linewidth=0.3)
            # ax.stock_img()
            gl = ax.gridlines(xlocs=[],ylocs=[66], linewidth=0.3, color='black')


            # plt.show()
            trans = ax.get_xaxis_transform()
            ax.text(0.01, 0.87, figlabelstr[3 * level + i_ax], transform=ax.transAxes,
                    horizontalalignment='left', bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1})
            # plt.show()

    # fig.suptitle('%s' % (type_SOM))
    title_col = ['t=0', r't=$\frac{1}{2}$T', 't=T']
    for i_col in range(3):
        axs[i_col].set_title(title_col[i_col])
    title_rows = plot_name
    for i_row in range(6):
        axs[i_row*3].text(-0.07, 0.55, title_rows[i_row], va='bottom', ha='center',
                rotation='vertical', rotation_mode='anchor',
                transform=axs[i_row*3].transAxes)
    cb_ax = fig.add_axes([0.15, 0.05, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cb_ax.set_xlabel('kg/$m^{2}$')
    # plt.show()
    save_pic_path = path_MainData+'pic_combine_result/'
    os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig('dissertation_fig3-5.png', dpi=300)
    plt.show()
    plt.close()