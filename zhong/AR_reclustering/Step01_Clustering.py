import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.io as scio
path_SaveData = r'/home/linhaozhong/work/AR_NP85/new_Clustering_2000/'
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
path_ARfile = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'

"""
=================================== Step01 temporal and spatial distribution ===========================
"""

# """
# when calculate the area-weighted mean, we will ## mask 0 ## to avoid error.
# """
#
#
# """  additional process for IVT  """
#
# # a = scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\ivt_type_ar_yearly_sum.mat")['ar_type']
# # b = scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\density_type_ar_yearly.mat")['ar_type']
# # a = a/b
# # scio.savemat(r"D:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\ivt_type_ar_yearly.mat", {'ar_type':a})
# # exit()
#
# ar_type = False
# file_exist = False
# path_Main = r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering/'
# var = 'density'
# param_var = {'ivt':[True, [path_Main+'ivt_ar_yearly', 'ar_type'], False, [np.arange(1979,2020+1), 2000]],
#              'freq':[False, [path_Main+'freq_ar_yearly', 'ar_type'], True, [np.arange(1980,2019+1), 2000]],
#              'density':[True, [path_Main+'density_DailyAR_yearly', 'ar_type'], True, [np.arange(1980,2019+1), 2000]]
#              }
# # for Trend, keep 0 value or not?
# # file path and mat file var name
# # cut 1979 and 2020 or not?
# import xarray as xr
# # a = xr.open_dataset(r"E:/Guan_DJF.nc")
# # lat = a.lat.values
# # lon =a.lon.values
# lon = np.arange(0,360,0.5)
# lat =np.arange(90,-90.01,-0.5)
# period = param_var[var][3][0]
# mutation_year = int(param_var[var][3][1]-period[0])
# arType_shortName = ['GRE', '1', 'BAF', 'BSE' , 'BSW', 'MED', 'ARC', '7']
#
# os.makedirs(path_Main+var, exist_ok=True)
#
# """
# ============================cal data [year, lat, lon]=============================================
# """
#
# # import os
# # import numpy as np
# # import pandas as pd
# # import xarray as xr
# # path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# # import nc4
# # def load_ivt_total(s_time):
# #     '''
# #     :param s_time: datetime64
# #     :return: ivt_total
# #     '''
# #     s_time = pd.to_datetime(s_time)
# #
# #     s_time_str = s_time.strftime('%Y%m%d')
# #     index_vivt = xr.open_dataset(
# #         path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
# #             s_time_str[:4], s_time_str))
# #     index_uivt = xr.open_dataset(
# #         path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
# #             s_time_str[:4], s_time_str))
# #     # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
# #     # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))
# #
# #     ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
# #     ivt_east = index_uivt.sel(time=s_time)['p71.162'].values
# #
# #     ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
# #     return ivt_total
# #
# #
# # main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
# # file = os.listdir(main_path)
# #
# # year_begin = 1979
# # year_end = 2020
# # density = np.zeros([year_end - year_begin + 1, 361, 720])
# # avg_freq = np.zeros([year_end-year_begin+1, 361,720])
# # ivt_all = np.zeros([year_end-year_begin+1, 361,720])
# #
# # for i_file in file:
# #     info_AR_row = xr.open_dataset(main_path+i_file)
# #     time_AR = info_AR_row.time_AR.values
# #     time_all = info_AR_row.time_all.values
# #     AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
# #     time_use = int(len(time_all[AR_loc:]) / 2)
# #
# #     contour_all = np.zeros([361,720])
# #
# #     for i_time in time_AR:
# #         year_loc = int(pd.to_datetime(i_time).strftime('%Y'))-year_begin
# #         if pd.to_datetime(i_time).strftime('%m') == '12':
# #             year_loc = year_loc + 1
# #         if year_loc == year_end - year_begin + 1:
# #             continue
# #         # sum for all year
# #
# #         contour = info_AR_row['contour_all'].sel(time_all=i_time).values
# #
# #         ivt = load_ivt_total(pd.to_datetime(i_time))
# #         ivt[contour<0.5] = 0
# #         density[year_loc] += contour
# #         ivt_all[year_loc] += ivt
# #
# #         contour_all += contour
# #     contour_all[contour_all>0] = 1
# #     if year_loc == year_end - year_begin + 1:
# #         continue
# #     avg_freq[year_loc] += contour_all
# #
# # import scipy.io  as scio
# # ivt_all = ivt_all / density
# # ivt_all[np.isnan(ivt_all)] = 0
# # scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering//ivt_ar_yearly.mat', {'ar_type':np.array(ivt_all)})
# # scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/freq_ar_yearly.mat', {'ar_type':np.array(avg_freq)})
# # scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/density_ar_yearly.mat', {'ar_type':np.array(density)})
# # exit(0)
# """
# ============================ temporal trend =============================================
# """
# path_Save_now = path_Main+var+'/'
#
# import matplotlib.pyplot as plt
# from sklearn import preprocessing
# import nc4
# import scipy.io as scio
# import pandas as pd
# import numpy as np
# import xarray as xr
#
# def plot_columns(df_row, linregress_2000=[]):
#     # x = df_row.values  # returns a numpy array
#     # min_max_scaler = preprocessing.MinMaxScaler()
#     # x_scaled = min_max_scaler.fit_transform(x)
#     # df = pd.DataFrame(x_scaled, columns=df_row.columns, index=period)
#
#     df = df_row.copy()
#     # df['year'] = np.arange(1979,2020+1)
#     df_mutation = df.iloc[mutation_year:].copy()
#
#     lens = 0
#     fig, ax = plt.subplots()
#     colors = ['#FF0000', '#00FF00', '#0000FF', '#FFA500', '#9400D3', '#FFC0CB', '#00FFFF', '#FFFF00']
#
#
#     for i_columns in df.columns:
#         # plot raw line
#         ax.plot(df.index, df[i_columns], '-', label=i_columns, color=colors[lens], linewidth=0.3)
#
#         # fit 1979-2020
#
#         from scipy.stats import linregress
#
#         notnan_loc = np.argwhere(~np.isnan(np.array(df[i_columns])))
#         x = np.array(df.index)[notnan_loc].flatten()
#         y = np.array(df[i_columns])[notnan_loc].flatten()
#         print(x,y)
#         slope, intercept, r_value, p_value, std_err = linregress(x,y)
#         f = x * slope + intercept
#         if p_value < 0.1:
#             ax.plot(x, f, '-', color=colors[lens])
#         else:
#             ax.plot(x, f, '-.', color=colors[lens], linewidth=0.8)
#
#         # fit 1979-2020
#         if i_columns in linregress_2000:
#             from scipy.stats import linregress
#             notnan_loc = np.argwhere(~np.isnan(np.array(df_mutation[i_columns])))
#             x = np.array(df_mutation.index)[notnan_loc].flatten()
#             y = np.array(df_mutation[i_columns])[notnan_loc].flatten()
#             slope, intercept, r_value, p_value, std_err = linregress(x, y)
#             f = x * slope + intercept
#             if p_value < 0.1:
#                 ax.plot(x, f, '-', color=colors[lens])
#             else:
#                 ax.plot(x, f, '-.', color=colors[lens], linewidth=0.8)
#         lens += 1
#     plt.legend()
#
# def cal_area(lon, lat):
#     import math
#     lon, lat = np.meshgrid(lon, lat)
#
#     R = 6378.137
#     dlon = lon[0, 1] - lon[0, 0]
#     delta = dlon * math.pi / 180.
#     S=np.zeros(lon.shape)
#     Dx=np.zeros(lon.shape)
#     Dy=np.ones(lon.shape)
#
#     Dx = R * np.cos(lat * math.pi / 180.) * delta
#     Dy = Dy * delta * R
#
#     S = Dx * Dy
#     return S
#
# def area_weighted_mean(data, area_weight, maskvalue=''):
#
#     if not isinstance(maskvalue, str):
#         area_weight[data==maskvalue] = np.nan
#     area_weight[np.isnan(data)] = np.nan
#
#     area_weight = area_weight / np.nansum(area_weight)
#     avg_AR = np.nansum(data * area_weight)
#
#     return avg_AR
#
#
# data_row = scio.loadmat(param_var[var][1][0])[param_var[var][1][1]]
# if param_var[var][2]:
#     if ar_type:
#         data_row = data_row[:, 1:-1, :]  # cut 1979, 2020
#     else:
#         data_row = data_row[1:-1, :,:]  # cut 1979, 2020
#
#
#
# weight = cal_area(lon, lat)
#
# ########## for Arctic ##########
#
# if ar_type:
#     data = np.nansum(data_row, axis=0)
#
# else:
#     data = data_row.copy()
# avg = []
#
# for ii in range(data.shape[0]):
#     avg.append(area_weighted_mean(data[ii], weight.copy(), maskvalue=0))
#
# avg = np.array(avg)
# if param_var[var][0]:
#     avg[avg == 0] = np.nan
#
# df = pd.DataFrame(index=period)
# df[var] = avg
# plot_columns(df, df.columns)
# # plt.show()
# plt.savefig(path_Save_now+'%s_Arctic_TemporalTrend.png'%var, dpi=400)
# plt.close()
#
# ########## for arType ##########
# if ar_type:
#     df = pd.DataFrame(index=period)
#     for i_ar in np.arange(data_row.shape[0]):
#         avg = []
#         for ii in range(data_row.shape[1]):
#             avg.append(area_weighted_mean(data_row[i_ar, ii], weight.copy(), maskvalue=0))
#
#         avg = np.array(avg)
#         if param_var[var][0]:
#             avg[avg == 0] = np.nan
#         df[var +' '+ arType_shortName[i_ar]] = avg
#
#     plot_columns(df, df.columns)
#     # plt.show()
#     plt.savefig(path_Save_now+'%s_arType_TemporalTrend.png'%var, dpi=400)
#     plt.close()
#
#
# """
# ============================ spatial trend =============================================
# """
BBBBBBB__lock_index01 = 'spatial trend'
# #%%
# import matplotlib.pyplot as plt
# import numpy as np
# import scipy.io
# from scipy.stats import linregress
# import nc4
#
# # ar = scio.loadmat(param_var[var][1][0])[param_var[var][1][1]]
#
# # import cartopy.crs as ccrs
# # fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})
# # ax.contourf(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), np.nansum(ar[1], axis=(0)), transform=ccrs.PlateCarree())
# # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
# # ax.coastlines(linewidth=0.3,zorder=10)
# # plt.show()
# # plt.show()
#
#
# ar = scio.loadmat(param_var[var][1][0])[param_var[var][1][1]]
# if var == 'ivt':
#     ar[np.isnan(ar)] = 0
#
#
# if param_var[var][2]:
#     if ar_type:
#         ar = ar[:,1:-1,:] # cut 1979, 2020
#     else:
#         ar = ar[1:-1] # cut 1979, 2020
#
#
# def spatial_trend(data_row, save_name):
#     n_lats = data_row.shape[1]
#     n_lons = data_row.shape[2]
#
#     # period_name = [
#     #              '%i_%i' % (period[mutation_year], period[-1])]
#     period_name = ['%i_%i' % (period[0], period[-1]), '%i_%i' % (period[0], period[mutation_year]),
#                  '%i_%i' % (period[mutation_year], period[-1])]
#     for iI, data in enumerate([data_row, data_row[:mutation_year], data_row[mutation_year:]]):
#     # for iI, data in enumerate([data_row[mutation_year:]]):
#         save_path = path_Main + var + '/spatialTrend/' + period_name[iI] + '/'
#         os.makedirs(save_path, exist_ok=True)
#
#         if not file_exist:
#             trend = np.zeros((n_lats, n_lons))
#             p_values = np.ones((n_lats, n_lons))
#
#             for i in range(n_lats):
#                 for j in range(n_lons):
#                     s_data = data[:, i, j]
#                     ######
#                     # if (density[:,i,j] == 0).any():
#                     #     print('hi')
#                     if param_var[var][0]:
#                         s_data = s_data[s_data != 0]
#                     if len(s_data) != 0:
#                         slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
#                         trend[i, j] = slope
#                         p_values[i, j] = p_value
#
#             nc4.save_nc4(trend, save_path + r'%s_trend_ar_pattern' % (var + '_' + save_name))
#             nc4.save_nc4(p_values, save_path + r'%s_p_ar_pattern' % (var + '_' + save_name))
#
#         else:
#             trend = nc4.read_nc4(save_path + r'%s_trend_ar_pattern' % (var + '_' + save_name))
#             p_values = nc4.read_nc4(save_path + r'%s_p_ar_pattern' % (var + '_' + save_name))
#
#         ################################# plot #########################################
#
#         import cartopy.crs as ccrs
#         import cmaps
#
#         fig = plt.figure(figsize=[5, 3.9])
#         plt.subplots_adjust(top=0.9,
#                             bottom=0.05,
#                             left=0.005,
#                             right=0.995,
#                             hspace=0.14,
#                             wspace=0.055)
#         ax = fig.add_subplot(1, 1, 1, projection=ccrs.NorthPolarStereo())
#
#         plt.title(var + '_' + period_name[iI])
#
#         # trend[np.isnan(trend)] = 0
#         trend[trend == 0] = np.nan
#         import matplotlib.colors as colors
#         import matplotlib.colors as colors
#         # plt.imshow(trend)
#         # plt.show()
#
#         cb = ax.pcolormesh(lon, lat, trend
#                          , cmap='bwr',vmin=-0.3,vmax=0.3,
#                          transform=ccrs.PlateCarree())
#         # trend[abs(trend)<0.001] = 0
#         # cb = ax.contourf(lon, lat[-120:], trend[-120:],levels=[-1,-0.01,0.01,1]
#         #                    , cmap='bwr', norm=colors.CenteredNorm(),
#         #                    transform=ccrs.PlateCarree())
#         plt.colorbar(cb)
#         ax.contourf(lon, lat,
#                     np.ma.masked_greater(p_values, 0.1),
#
#                     colors='none', levels=[0, 0.05],
#                     hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
#         points_all = [
#             [[5,45],[52,65]],
#             [[65,80],[45,57]],
#             [[70,130],[62,75]],
#             [[140,240],[66,90]],
#             [[295,310],[60,75]],
#             [[235,260],[57,67]]
#         ]
#         for point in points_all:
#             ax.plot([point[0][0], point[0][1], point[0][1], point[0][0],point[0][0]],
#                     [point[1][0], point[1][0], point[1][1], point[1][1],point[1][0]], transform=ccrs.PlateCarree())
#         ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
#         # ax.set_global()
#         ax.coastlines(linewidth=0.3, zorder=10)
#         # ax.stock_img()
#         ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
#         plt.title(var + '_' + save_name)
#         # plt.show()
#         plt.savefig(
#             save_path + '%s_trend_pattern.png' % (var + '_' + save_name),
#             dpi=400)
#         plt.close()
#
#
#
#
# if ar_type:
#     ar_sum = np.sum(ar, axis=0)
# else:
#     ar_sum = ar
# spatial_trend(ar_sum, 'total')
#
# if ar_type:
#     for iloop in range(ar.shape[0]):
#         data_row = ar[iloop]
#         spatial_trend(data_row, arType_shortName[iloop])
#
# exit()


"""
=================================== Step01 clustering  ===========================
"""
BBBBBBB__lock_index02 = 'Step01 clustering'

# import os
# import numpy as np
# import pandas as pd
# import xarray as xr
# import nc4
#
# def overlap_ratio(row_data, exist_contour, lon, lat):
#     import cv2
#     from function_shared_Main import cal_area
#
#     exist_contour = np.array(exist_contour, dtype='uint8')
#     row_data = np.array(row_data, dtype='uint8')
#     weighted = cal_area(lon, lat)
#
#     overlap = cv2.bitwise_and(row_data, exist_contour)
#     if cv2.countNonZero(overlap) != 0:
#         area_rowdata = np.sum(weighted[row_data>0.01])
#         area_existcontour = np.sum(weighted[exist_contour>0.01])
#         area_overlap = np.sum(weighted[overlap > 0.01])
#         return area_overlap/area_existcontour
#     else:
#         return 0
#
# def smooth_sort_longpath(x,y):
#     import networkx as nx
#
#     from scipy.spatial import Delaunay
#     points = np.vstack([x, y]).T
#     tri = Delaunay(points)
#     edges_dist_map = {}
#     edge_list = []
#     edge_length = []
#
#     for tr in tri.simplices:
#         for i in range(3):
#             edge_idx0 = tr[i]
#             edge_idx1 = tr[(i + 1) % 3]
#             if (edge_idx1, edge_idx0) in edges_dist_map:
#                 continue  # already visited this edge from other side
#             p0 = points[edge_idx0]
#             p1 = points[edge_idx1]
#             dist = np.linalg.norm(p1 - p0)
#             edges_dist_map[(edge_idx0, edge_idx1)] = dist
#             edge_list.append([edge_idx0, edge_idx1])
#             edge_length.append(dist)
#
#     # compute shortest weighted path
#     g = nx.Graph((i, j, {'weight': dist}) for (i, j), dist in zip(edge_list, edge_length))
#     # Finding the route
#     solved_path = nx.approximation.traveling_salesman_problem(g, weight='weight', cycle=False)
#
#     x = points[solved_path, 0]
#     y = points[solved_path, 1]
#     return x, y
#
# lon = np.arange(0,360,0.5)
# lat = np.arange(90,-90.01,-0.5)
# ## prepara the classification area
# points_all = [
#     [[5,45],[52,65]],
#     [[65,80],[45,57]],
#     [[70,130],[62,75]],
#     [[140,240],[66,90]],
#     [[295,310],[60,75]],
#     [[235,260],[57,67]]
# ]
#
# # mask contour, the values inside points_all are 1
# contour_classification = []
# for point in points_all:
#     s_contour_classification = np.zeros([lat.shape[0], lon.shape[0]])
#     print(s_contour_classification.shape)
#     lat_S, lat_N = point[1]
#     lon_W,lon_E = point[0]
#
#     lon_2d,lat_2d = np.meshgrid(lon, lat)
#     index_lat = (lat_2d>lat_S)&(lat_2d<lat_N)
#     index_lon = (lon_2d>lon_W)&(lon_2d<lon_E)
#     s_contour_classification[index_lat & index_lon] = 1
#     contour_classification.append(s_contour_classification)
#
# # contour_classification is prepare.
# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
#
# file = os.listdir(path_ARfile)
#
# year_begin = 2000
# year_end = 2020
# bu_overlap_ration = 0.2
#
# def do_clustering(contour_classification):
#     file_list = []
#     for i_file in file:
#         if int(i_file[:4]) >= year_begin:
#             info_AR_row = xr.open_dataset(path_ARfile + i_file)
#             time_AR = info_AR_row.time_AR.values
#             time_all = info_AR_row.time_all.values
#             AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#             time_use = int(len(time_all[AR_loc:]) / 2)
#
#             contour_all = np.zeros([361,720])
#
#             # cal contour_all
#             for i_time in time_AR:
#
#                 contour = info_AR_row['contour_all'].sel(time_all=i_time).values
#                 contour_all += contour
#
#             contour_all[contour_all>0] = 1
#
#             if overlap_ratio(contour_all, contour_classification, lon, lat) > bu_overlap_ration:
#                 file_list.append(i_file)
#     return file_list
#
# classificationNo_fileName = []
# classification_fileName = []
# for i_contour in contour_classification:
#     dc = do_clustering(i_contour)
#     classification_fileName.append(dc)
#     classificationNo_fileName.extend(dc)
#
# print(len(np.unique(classificationNo_fileName)))
# print(len(np.unique(file)))
# difference_row = set(file) - set(classificationNo_fileName)
# difference = []
# for drop_year in difference_row:
#     if int(drop_year[:4]) >= year_begin:
#         difference.append(drop_year)
#
# clustering_fileName_Final = pd.DataFrame()
# for iI in classification_fileName:
#
#     clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(iI)],
#                                          ignore_index=True,axis=1)
#
# if len(difference)!=0:
#     clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(list(difference))],
#                                          ignore_index=True,axis=1)
#
# clustering_fileName_Final.to_csv(path_SaveData + 'type_filename_%0.1f.csv'%bu_overlap_ration, index=False)
#
#
# import pandas as pd
# import matplotlib.pyplot as plt
#
# # 读取 csv 文件
# df = pd.read_csv(path_SaveData + 'type_filename_%0.1f.csv'%bu_overlap_ration)
#
# overlap = pd.DataFrame(columns=df.columns, index=df.columns)
# unique = pd.DataFrame(columns=df.columns, index=df.columns)
# # 找出 A 和 B 列之间的重合和不重合部分
# for a_columns in df.columns:
#     for b_columns in df.columns:
#         if a_columns == b_columns:
#             continue
#         A = set(df[a_columns].dropna())
#         B = set(df[b_columns].dropna())
#         intersect = len(A.intersection(B))
#         diff = len(A.symmetric_difference(B))
#         overlap[b_columns].loc[a_columns] = intersect/len(A)
#         unique[b_columns].loc[a_columns] = diff/len(A)
#
# overlap.plot(kind='bar')
# plt.title(bu_overlap_ration)
# plt.savefig(path_SaveData + 'type_filename_overlap_%f.png'%bu_overlap_ration, dpi=400)
# plt.close()



"""
=================================== Step01 plot clustering longpath ===========================
"""
BBBBBBB__lock_index03 = 'Step01 plot clustering longpath'

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import scipy.io as scio
import pandas as pd
import numpy as np
import xarray as xr

def smooth_sort_longpath(x,y):
    import networkx as nx

    from scipy.spatial import Delaunay
    points = np.vstack([x, y]).T
    tri = Delaunay(points)
    edges_dist_map = {}
    edge_list = []
    edge_length = []

    for tr in tri.simplices:
        for i in range(3):
            edge_idx0 = tr[i]
            edge_idx1 = tr[(i + 1) % 3]
            if (edge_idx1, edge_idx0) in edges_dist_map:
                continue  # already visited this edge from other side
            p0 = points[edge_idx0]
            p1 = points[edge_idx1]
            dist = np.linalg.norm(p1 - p0)
            edges_dist_map[(edge_idx0, edge_idx1)] = dist
            edge_list.append([edge_idx0, edge_idx1])
            edge_length.append(dist)

    # compute shortest weighted path
    g = nx.Graph((i, j, {'weight': dist}) for (i, j), dist in zip(edge_list, edge_length))
    # Finding the route
    solved_path = nx.approximation.traveling_salesman_problem(g, weight='weight', cycle=False)

    x = points[solved_path, 0]
    y = points[solved_path, 1]
    return x, y

time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
lon = np.arange(0,360,0.5)
lat = np.arange(90,-90.01,-0.5)


df = pd.read_csv(path_SaveData + r'type_filename_0.2.csv')
for iI, columns in enumerate(df.columns):
    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})

    longpath_2d = np.zeros([lat.shape[0], lon.shape[0]])

    times_file_all = df[columns].values
    for time_file in times_file_all:
        if len(str(time_file)) < 5:
            continue
        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_contour + time_file_new)
        '============================================'
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        for i_time_row in time_AR:
            i_time = pd.to_datetime(i_time_row)
            y, x = info_AR_row['longpath'].sel(time_AR=i_time_row).values
            x = x[~np.isnan(x)]
            y = y[~np.isnan(y)]

            for i_date in range(x.shape[0]):
                lon_now = x[i_date]
                lat_now = y[i_date]

                index = [0, 0]
                index[1] = np.argmin(abs(lon - lon_now))
                index[0] = np.argmin(abs(lat - lat_now))
                longpath_2d[index[0], index[1]] += 1

            # x, y = smooth_sort_longpath(x,y)
            # ax.plot(x, y, color='black', transform=ccrs.Geodetic(), linewidth=0.1)
            # plt.show()
    nc4.save_nc4(longpath_2d, path_SaveData+r'longpath_%s'%columns)
    cb = ax.pcolormesh(lon, lat, longpath_2d
                     ,
                     transform=ccrs.PlateCarree())
    points_all = [
        [[5,45],[52,65]],
        [[65,80],[45,57]],
        [[70,130],[62,75]],
        [[140,240],[66,90]],
        [[295,310],[60,75]],
        [[235,260],[57,67]]
    ]
    for point in points_all:
        ax.plot([point[0][0], point[0][1], point[0][1], point[0][0],point[0][0]],
                [point[1][0], point[1][0], point[1][1], point[1][1],point[1][0]], transform=ccrs.PlateCarree())
    ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    plt.title(columns)
    ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    ax.coastlines(linewidth=0.5)
    plt.savefig(path_SaveData+r'longpath_%s.png'%columns, dpi=400)
    plt.close()

exit(0)
"""
=================================== Step ar_type frequency and density ===========================
"""
BBBBBBB__lock_index = 'Step ar_type frequency and density'


import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4
import scipy.io as scio

# df = pd.read_csv(r'/home/linhaozhong/work/AR_NP85/new_Clustering/type_filename_0.2.csv')
main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
#
# for i_columns in df.columns:
#     file = np.array(df[i_columns].dropna())

save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
for iI, i in enumerate(np.unique(save_labels_bmus)):
    if i == 0:
        continue
    file = file_all[save_labels_bmus==i]
    year_begin = 1979
    year_end = 2020
    density = np.zeros([year_end - year_begin + 1, 361, 720])
    avg_freq = np.zeros([year_end-year_begin+1, 361,720])
    ivt_all = np.zeros([year_end-year_begin+1, 361,720])

    for i_file in file:
        ################# !!!!!!!!!!!!!!!!!!! ##############
        info_AR_row = xr.open_dataset(main_path+i_file[:-1])
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        time_use = int(len(time_all[AR_loc:]) / 2)

        contour_all = np.zeros([361,720])

        for i_time in time_AR:
            year_loc = int(pd.to_datetime(i_time).strftime('%Y'))-year_begin
            if pd.to_datetime(i_time).strftime('%m') == '12':
                year_loc = year_loc + 1
            if year_loc == year_end - year_begin + 1:
                continue
            # sum for all year

            contour = info_AR_row['contour_all'].sel(time_all=i_time).values


            density[year_loc] += contour


            contour_all += contour
        contour_all[contour_all>0] = 1
        if year_loc == year_end - year_begin + 1:
            continue
        avg_freq[year_loc] += contour_all

    import scipy.io  as scio
    # scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/%s_freq_ar_yearly.mat'%i_columns, {'ar_type':np.array(avg_freq)})
    # scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/%s_density_ar_yearly.mat'%i_columns, {'ar_type':np.array(density)})
    scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/%i_freq_ar_yearly.mat'%i, {'ar_type':np.array(avg_freq)})
    scio.savemat('/home/linhaozhong/work/AR_NP85/new_Clustering/%i_density_ar_yearly.mat'%i, {'ar_type':np.array(density)})



import cartopy.crs as ccrs
import matplotlib.pyplot as plt

import scipy.io as scio


import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4


import os
import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import nc4

SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.cmocean_balance
for arType_name in np.arange(7):
    arType_name =str(arType_name)
    data = scio.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\freq_density/contributeRatio_%s.mat'%arType_name)['cr']

    fig = plt.figure(figsize=[5,3.9])
    plt.subplots_adjust(top=0.97,
    bottom=0.1,
    left=0.005,
    right=0.995,
    hspace=0.14,
    wspace=0.055)

    ax = fig.add_subplot(1,1,1, projection=ccrs.NorthPolarStereo())

    ax.set_title('contribute ratio of %s'%arType_name)


    import cmaps
    import matplotlib.colors as colors
    cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:140], abs(data[:140])*100
                     ,vmin=0,vmax=100,
                     cmap='Reds',
                transform=ccrs.PlateCarree())


    ax.coastlines(linewidth=0.3,zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
    # plt.show()
    ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)

    cb_ax.set_xlabel('%')
    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\freq_density/contributeRatio_%s.png'%arType_name, dpi=400)
    plt.close()

exit(0)


