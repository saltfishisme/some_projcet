import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr


budget_load = ['IVT','t2m','heff',
               'sic','tcwv',
               'str', 'sshf', 'slhf',
               'cbh','tcc','hcc','mcc','lcc']
figlabels_diffVar = dict(zip(budget_load, [
    ['(a)', '(b)'], ['(b)', '(d)'],['(e)', '(g)'],
    ['(f)', '(h)'],['(a)', '(c)'],
['(a)', '(d)'], ['(b)', '(e)'],['(c)', '(f)'],
['(a)', '(d)'], ['(b)', '(e)'],['(a)', '(d)'], ['(b)', '(e)'],['(c)', '(f)'],
]
                             ))
plus = dict(zip(budget_load, [
        1,1,1,
        1,1,
        1 / (6 * 3600),1 / (6 * 3600),1 / (6 * 3600),
        1/1000,1,1,1,1,]))

vrange = dict(zip(budget_load, [
        np.arange(-100,101,10),np.arange(-10,10.01,0.5),np.arange(-100,100.01,10),
        np.arange(-100,100.01,10),np.arange(-2,2.01,0.1),
        np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),
        np.arange(-2,2.01,0.1),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01)]))

unit = dict(zip(budget_load, [
        'kg/(m*s)','K','cm',
        '%','kg/(m**2)',
        'W/$m^{2}$','W/$m^{2}$','W/$m^{2}$',
        'km','','','','',]))
xlocs = [[['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['', 179.99, 85], ['', -120, 85],
          ['60°W', -60, 75]],
         [['0°', 0, 82], ['60°E', 60, 75], ['120°E', 120, 66], ['180°', 179.99, 66], ['120°W', -120, 75],
          ['60°W', -60, 82]],
         [['120°E', 120, 80], ['180°', 179.99, 55], ['120°W', -120, 80]],
         [['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['180°', 179.99, 82], ['120°W', -120, 82],
          ['60°W', -60, 75]],
         [['0°', 0, 80], ['120°W', -120, 80], ['60°W', -60, 55]],
         [['120°E', 120, 75],['180°', 179.99, 55],['120°W', -120, 75]]]
central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]
# central_lonlat_all = [[30,80],[180,70]]
SMALL_SIZE = 6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use_contour = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance_r
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])


# def plot_pcolormeshP_onevar(data, p_values, vartype, lon,lat):
#     fig = plt.figure(figsize=[8, 5.5])
#     plt.subplots_adjust(top=0.98,
# bottom=0.07,
# left=0.02,
# right=0.98,
# hspace=0.0,
# wspace=0.2)
#
#     for i_i, i in enumerate([1,2,3,4,5,6]):
#         central_lonlat = central_lonlat_all[i_i]
#         ax = fig.add_subplot(2, 3, i,
#                              projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
#         # ax = fig.add_subplot(2, 3, i,
#         #                      projection=ccrs.NorthPolarStereo())
#
#         ax.set_title('%s anomaly for Type%s' %(vartype, i))
#
#         import cmaps
#         import matplotlib.colors as colors
#
#         # cb = ax.pcolormesh(lon, lat, data[i_i],
#         #                    cmap=cmap_use,
#         #                    transform=ccrs.PlateCarree(), vmin=-10, vmax=10)
#         p_pass = np.where(p_values[i_i] <= 0.1)
#         if (var != 'esiv') & (var != 'heff')& (var != 'sic'):
#             if var == 'IVT':
#                 if i == 2:
#                     cb = ax.contourf(lon[:120], lat[:120],
#                                      np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype])[:120],
#                                        cmap=cmap_use,levels=np.arange(-50,51, 5),
#                                        transform=ccrs.PlateCarree(), extend='both')
#                 else:
#                     cb = ax.contourf(lon[:120], lat[:120],
#                                      np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype])[:120],
#                                        cmap=cmap_use,levels=vrange[vartype],
#                                        transform=ccrs.PlateCarree(), extend='both')
#                 cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)
#
#             else:
#                 ax.contourf(lon[:120], lat[:120], np.ma.masked_greater(p[i_i][:120], 0.1),
#                                 colors='none',levels=[0,0.1],
#                                 hatches=[3*'.',3*'.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
#                 cb = ax.contourf(lon[:120], lat[:120],
#                                  data[i_i][:120]*plus[vartype],
#                                    cmap=cmap_use,levels=vrange[vartype],
#                                    transform=ccrs.PlateCarree(), extend='both')
#
#         else:
#             cb = ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
#                       transform=ccrs.PlateCarree(), zorder=20)
#
#             lons = np.reshape(lon, (120, 360))
#             lats = np.reshape(lat,(120,360))
#             cb = ax.pcolormesh(lons, lats,
#                              np.reshape(data[i_i],(120, 360)),
#                                cmap=cmap_use,
#                                transform=ccrs.PlateCarree(),vmin=-100, vmax=100)
#
#             # cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)
#         # cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
#         #                    cmap=cmap_use,
#         #                    transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))
#
#         # ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
#         #           transform=ccrs.PlateCarree(), zorder=20)
#
#         ax.coastlines(linewidth=0.3, zorder=10)
#
#         ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
#         # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
#         ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
#                       ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
#         if i == 2:
#             ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
#                           ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))
#     if vartype != 'IVT':
#         cb_ax = fig.add_axes([0.15, 0.06, 0.7, 0.01])
#         cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#         cb_ax.set_xlabel(unit[vartype])
#
#     # plt.show()
#     # save_pic_path = path_MainData + 'pic_combine_result/'
#     # os.makedirs(save_pic_path, exist_ok=True)
#     plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var), dpi=400)
#     plt.close()


import matplotlib.gridspec as gridspec
fig = plt.figure(figsize=[6,3])
gs = gridspec.GridSpec(2, 4)
plt.subplots_adjust(top=0.97,
bottom=0.085,
left=0.045,
right=0.98,
hspace=0.22,
wspace=0.2)

def plot_axes1(data, p_values, vartype, lon,lat):


    for i_i, i in enumerate([1,6]):
        var_inType = data[:, i-1]
        var_inType_p = p_values[:, i-1]

        central_lonlat = central_lonlat_all[i-1]
        ax = fig.add_subplot(gs[0,i_i],
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        # ax = fig.add_subplot(2, 3, i,
        #                      projection=ccrs.NorthPolarStereo())
        # if vartype == 'heff':
        #     ax.set_title('%s SIH anomaly for SOM%s' % (figlabels_diffVar[vartype][i_i], i))
        # else:
        ax.set_title('%s SOM%s' %(figlabels_diffVar['IVT'][i_i], i),loc='left')

        import cmaps
        import matplotlib.colors as colors

        # cb = ax.pcolormesh(lon, lat, data[i_i],
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree(), vmin=-10, vmax=10)

        #only keep HGT pvalues


        ax.contourf(lon[:120], lat[:120], np.ma.masked_greater(var_inType_p[2][:120], 0.1),
                        colors='none',levels=[0,0.1],
                        hatches=[3*'.',3*'.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        import matplotlib.colors as colors
        cb = ax.contourf(lon[:120], lat[:120],
                         var_inType[2][:120]/98,
                         levels=np.arange(-10,11, 1),
                           transform=ccrs.PlateCarree(), extend='both', alpha=0.8,
                         norm=colors.CenteredNorm())
        ax.contour(lon[:120], lat[:120],
                         var_inType[2][:120]/98,
                   colors='black',
                         levels=np.arange(-10,11, 1),
                           transform=ccrs.PlateCarree(), extend='both', alpha=0.8)

        ax.contour(lon[:120], lat[:120],
                         # np.ma.masked_where(var_inType_p[2]>0.1,var_inType[2])[:120]/98,
                   np.ma.masked_where(var_inType_p[1] > 0.1, var_inType[1])[:120],
                           cmap=cmap_use_contour,levels=np.arange(-10,10, 1),
                           transform=ccrs.PlateCarree(), linewidths=1)

        #             # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        u = var_inType[3]
        v = var_inType[4]
        p_uv = var_inType_p[0] > 0.1
        spacewind = [5,5]
        qui = ax.quiver(lon[::spacewind[1], ::spacewind[0]], lat[::spacewind[1], ::spacewind[0]],
                        np.ma.masked_where(p_uv, u)[::spacewind[1], ::spacewind[0]],
                        np.ma.masked_where(p_uv, v)[::spacewind[1], ::spacewind[0]]
                             , transform=ccrs.PlateCarree(), scale=400, width=0.005, color='white',
                        zorder=100)
        #

        qk = ax.quiverkey(qui,  0.6, 1.05, 50, r'$\mathrm{50\,kg\,m^{-1}s^{-1}}$', labelpos='E',
                               coordinates='axes', color='black',labelsep=0.01)
            # cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)
        # cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))

        # ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
        #           transform=ccrs.PlateCarree(), zorder=20)

        ax.coastlines(linewidth=0.5)
        # from function_shared_Main import add_longitude_latitude_labels_givenloc
        # add_longitude_latitude_labels_givenloc(fig, ax, xlocs[i-1], yloc=central_lonlat[0])
        # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')

        # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
        ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
                      ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        if i == 2:
            ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

    cb_ax = fig.add_axes([0.05, 0.575, 0.4, 0.01])
    cbar = plt.colorbar(cb, cax=cb_ax, orientation="horizontal")
    cb_ax.set_xlabel('gpm')



ar = []
p = []
for var in ['IVT', 't2m', 'hgt','IVT_u', 'IVT_v']:
    s_ar = []
    s_p = []
    for i_type in range(1,6+1):
        s_ar.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var)))
        s_p.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_P' % (i_type, var)))
    ar.append(s_ar)
    p.append(s_p)
ar = np.array(ar)
p = np.array(p)
print(ar.shape)

lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
plot_axes1(ar, p, var, lon_2d, lat_2d)
# plt.show()

""" ax 3"""
# aar = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\climate_all_t2mDAY3__1")
# par = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\climate_all_t2mDAY3__6")
# ax = fig.add_subplot(gs[0, 2:3+1])
# print([aar[12],par[12]])
# ax.plot(np.arange(len(aar)), aar, label='SOM1', color='#6baed6', marker='.',markersize=5,mfc='#eff3ff')
# ax.plot(np.arange(len(aar)), par, label='SOM6', color='#2171b5', marker='.',markersize=5,mfc='#bdd7e7')
# ax.scatter([12,12], [aar[12],par[12]], color='red', s=5, zorder=12)
# ax.set_ylabel('℃')
# ax.set_title("(c) T2M'", loc='left')
# ax.set_xticks([0,4,8,12,16,20,24])
# ax.set_xticklabels(['-3d','-2d','-1d','Onset','+1d','+2d', '+3d'])
# plt.legend()

aar = nc4.read_nc4(r"D:\themalMeltingAreaclimate_all_t2mDAY5__1_forzenRegion")[2*4:18*4+1]
par = nc4.read_nc4(r"D:\themalMeltingAreaclimate_all_t2mDAY5__6_forzenRegion")[2*4:18*4+1]
from scipy.ndimage import gaussian_filter
aar = gaussian_filter(aar, sigma=1)
par = gaussian_filter(par, sigma=1)
ax = fig.add_subplot(gs[0, 2:3+1])
print([aar[12],par[12]])
ax.plot(np.arange(len(aar)), aar, label='SOM1', color='#6baed6', linewidth=0.5)
ax.plot(np.arange(len(aar)), par, label='SOM6', color='#2171b5', linewidth=0.5)
ax.scatter(np.arange(len(aar))[::4], aar[::4], color='#6baed6', s=1)
ax.scatter(np.arange(len(aar))[::4], par[::4], color='#2171b5', s=1)

ax.scatter([12,12], [aar[12],par[12]], facecolor='red', edgecolor='red', s=5, zorder=12, linewidths=1)

ax.axvspan((3-0.87)*4, (3+4.5)*4, color='gray', alpha=0.5)
ax.axvspan((3-0.27)*4, (3+3.8)*4, color='gray', alpha=0.5)

# ax.scatter([(3+0.73)*4], [np.interp((3+0.73)*4, np.arange(len(aar)), aar)], color='red',marker='^', s=4, zorder=12)
# ax.scatter([(3+1.25)*4], [np.interp((3+1.25)*4, np.arange(len(aar)), par)], color='red',marker='^', s=4, zorder=12)


ax.set_ylabel('℃')
ax.set_title("(c) T2M'", loc='left')
ax.set_xticks(np.arange(0, len(aar)+1, 4))
# ax.set_xticklabels(['-3d','-2d','-1d','Onset','+1d','+2d', '+3d'])
ax.set_xticklabels(['', '-2d', '', 'Onset', '', '+2d', '', '+4d', '', '+6d', ''
                       , '+8d', '', '+10d', '', '+12d', ''])

x1 = (3-0.87)*4
x2 = (3+4.5)*4
x = np.arange(len(aar))
y = aar
y1 = np.interp(x1, x, y)
y2 = np.interp(x2, x, y)

# ax.fill_betweenx(y_new, x_new, x2, where=((x1 <= x_new) & (x_new <= x2)), color='fuchsia', alpha=0.5)
# ax.scatter([(3+0.73)*4], [np.interp((3+0.73)*4, x, y)], color='fuchsia',marker='s', s=4, zorder=12)
# ax.scatter([x1, x2], [y1, y2], color='black',marker='s', s=2, zorder=12)
ax.scatter([(3-0.87-1.15)*4], [np.interp((3-0.87-1.15)*4, x, y)], color='red',marker='^', s=4, zorder=12)
ax.scatter([(3+0.73)*4], [np.interp((3+0.73)*4, x, y)], color='lime',marker='^', s=4, zorder=12)
x1 = (3-0.27)*4
x2 = (3+3.8)*4
x = np.arange(len(aar))
y = par
y1 = np.interp(x1, x, y)
y2 = np.interp(x2, x, y)
ax.scatter([(3-0.27-1.06)*4], [np.interp((3-0.27-1.06)*4, x, y)], color='red',marker='^', s=4, zorder=12)
ax.scatter([(3+1.25)*4], [np.interp((3+1.25)*4, x, y)], color='lime',marker='^', s=4, zorder=12)

plt.legend()

# plt.show()
# aar = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\themalMeltingAreaclimate_all_hccDAY5__1_forzenRegion")
# par = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\themalMeltingAreaclimate_all_hccDAY5__6_forzenRegion")
# ax = fig.add_subplot(gs[0, :])
# kernel_size = 10
# kernel = np.ones(kernel_size) / kernel_size
#
# ax.plot(np.arange(len(aar)), np.convolve(aar, kernel, mode='same'), label='AAR')
# ax.plot(np.arange(len(aar)), np.convolve(par, kernel, mode='same'), label='PAR')
#
# aar = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\themalMeltingAreaclimate_all_mccDAY5__1_forzenRegion")
# par = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\themalMeltingAreaclimate_all_mccDAY5__6_forzenRegion")
# ax = fig.add_subplot(gs[1, :])
# ax.plot(np.arange(len(aar)), np.convolve(aar, kernel, mode='same'), label='AAR')
# ax.plot(np.arange(len(aar)), np.convolve(par, kernel, mode='same'), label='PAR')
#
# aar = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\themalMeltingAreaclimate_all_lccDAY5__1_forzenRegion")
# par = nc4.read_nc4(r"D:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\themalMeltingAreaclimate_all_lccDAY5__6_forzenRegion")
# ax = fig.add_subplot(gs[2, :])
# ax.plot(np.arange(len(aar)), np.convolve(aar, kernel, mode='same'), label='AAR')
# ax.plot(np.arange(len(aar)), np.convolve(par, kernel, mode='same'), label='PAR')
# print(par)
# plt.legend()
# plt.show()
"""ax 4"""


def plot_axes4(data, p_values, vartype, lon,lat):


    for i_i, i in enumerate([1,6]):
        var_inType = data[:, i-1]
        var_inType_p = p_values[:, i-1]

        central_lonlat = central_lonlat_all[i-1]
        ax = fig.add_subplot(gs[1,i_i],
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

        figlabels_here = ['(d)', '(e)']
        ax.set_title("%s SIH' for SOM%s" % (figlabels_here[i_i], i), loc='left')

        p_pass = np.where(var_inType_p[0] <= 0.1)
        cb = ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(), s=0.1, c='black', marker='*',
                        linewidth=0.1,
                        transform=ccrs.PlateCarree(), zorder=20)

        lons = np.reshape(lon, (120, 360))
        lats = np.reshape(lat, (120, 360))

        cb = ax.pcolormesh(lons, lats,
                           np.reshape(var_inType[0], (120, 360)),
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(), vmin=-100, vmax=100)

        ax.coastlines(linewidth=0.3, zorder=10)

        # from function_shared_Main import add_longitude_latitude_labels_givenloc
        # add_longitude_latitude_labels_givenloc(fig, ax, xlocs[i-1], yloc=central_lonlat[0])
        # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
        ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
                      ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        if i == 2:
            ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

    cb_ax = fig.add_axes([0.05, 0.075, 0.4, 0.01])
    cbar = plt.colorbar(cb,cax=cb_ax, orientation="horizontal")
    cb_ax.set_xlabel('cm')

    for i_i, i in enumerate([1,6]):
        var_inType = data[:, i-1]
        var_inType_p = p_values[:, i-1]

        central_lonlat = central_lonlat_all[i-1]
        ax = fig.add_subplot(gs[1,i_i+2],
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

        figlabels_here = ['(f)', '(g)']
        ax.set_title("%s SIC' for SOM%s" % (figlabels_here[i_i], i), loc='left')

        p_pass = np.where(var_inType_p[1] <= 0.1)
        cb = ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(), s=0.1, c='black', marker='*',
                        linewidth=0.1,
                        transform=ccrs.PlateCarree(), zorder=20)

        lons = np.reshape(lon, (120, 360))
        lats = np.reshape(lat, (120, 360))

        cb = ax.pcolormesh(lons, lats,
                           np.reshape(var_inType[1], (120, 360)),
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(), vmin=-100, vmax=100)

        ax.coastlines(linewidth=0.3, zorder=10)
        # from function_shared_Main import add_longitude_latitude_labels_givenloc
        # add_longitude_latitude_labels_givenloc(fig, ax, xlocs[i-1], yloc=central_lonlat[0])
        # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
        # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
        ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
                      ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        if i == 2:
            ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

    cb_ax = fig.add_axes([0.55, 0.075, 0.4, 0.01])
    cbar = plt.colorbar(cb,cax=cb_ax, orientation="horizontal")
    cb_ax.set_xlabel('%')

ar = []
p = []
for var in ['heff', 'sic']:
    s_ar = []
    s_p = []
    for i_type in range(1,6+1):
        s_ar.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var)))
        s_p.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_P' % (i_type, var)))
    ar.append(s_ar)
    p.append(s_p)
ar = np.array(ar)
p = np.array(p)
print(p.shape)

xy = xr.open_dataset(r"D:\aiday.H1979.nc")
plot_axes4(ar, p, var, xy.x.values, xy.y.values)
# plt.show()
plt.savefig('figure5_R2.png', dpi=400)
plt.show()
# plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-5.png', dpi=400)