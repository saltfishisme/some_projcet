import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import pandas as pd




def cal_contour(data):
    import cv2
    from skimage import measure
    data = np.array(data, dtype='uint8')
    thresh = cv2.threshold(data, 0.1, 255, cv2.THRESH_BINARY)[1]
    labels = measure.label(thresh, connectivity=1, background=0)

    Mask_onePic = np.zeros(data.shape, dtype='uint8')

    for label in np.unique(labels):
        if label == 0:
            continue

        #######  prepare labelMask, labelMask is a narray contains one contour
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)

        ####### get index of this contour
        ar_pathway_index = np.argwhere(labelMask != 0)

        if not (IVT_lat[np.unique(ar_pathway_index[:, 0])]>70).any():
            continue

        Mask_onePic = cv2.add(Mask_onePic, labelMask)

    Mask_onePic = np.array(Mask_onePic, dtype = 'double')
    Mask_onePic[Mask_onePic>0] = 1
    return Mask_onePic



a = xr.open_dataset(r"/home/linhaozhong/work/merra2_Guan/Guan_DJF.nc")
IVT_lat = a.lat.values
values = a['ar_binary_tag'].values[5::6]
# values[:,:,:] =0
times = a['time'].values[5::6]
year_begin = 1980
year_end = 2019
avg_density = np.zeros([2019 - 1980 + 1, 361, 576])

for iI, i_times in enumerate(times):

    year_loc = int(pd.to_datetime(i_times).strftime('%Y')) - year_begin
    if pd.to_datetime(i_times).strftime('%m') == '12':
        year_loc = year_loc + 1
    if year_loc == year_end - year_begin + 1:
        continue
    # sum for all year

    contour = values[iI]
    contour = cal_contour(contour)

    avg_density[year_loc] += contour
import scipy.io as scio
scio.savemat('/home/linhaozhong/work/merra2_Guan/Guan_density_ar_yearly.mat', {'ar_type':np.array(avg_density)})

exit()

import scipy.io as scio
import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4


main_path = r'/home/linhaozhong/work/AR_NP85/daily_AR/'
file = os.listdir(main_path)

year_begin = 1979
year_end = 2020
avg = np.zeros([year_end-year_begin+1, 361,720])

for i_file in file:
    info_AR_row = nc4.read_nc4(main_path+i_file[:-4])


    contour_all = np.zeros([361,720])

    for contour in info_AR_row:
        year_loc = int(i_file[:4])-year_begin
        if i_file[4:6] == '12':
            year_loc = year_loc + 1
        if year_loc == year_end - year_begin + 1:
            continue

        avg[year_loc] += contour


    # # sum for all year
    # avg += np.nansum(info_AR_row['contour_all'].values, axis=0)
# nc4.save_nc4(avg, '/home/linhaozhong/work/AR_NP85/ivt_ar_yearly')
scio.savemat('/home/linhaozhong/work/merra2_Guan/density_dailyAR_yearly.mat', {'ar_type':np.array(avg)})


