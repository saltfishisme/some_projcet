import os
from PIL import Image
from pptx import Presentation
from pptx.util import Inches
import numpy as np

vars = ['IVT','tcwv', 't2m', 'sic']
# 图片所在文件夹路径
folder_path = "/home/linhaozhong/work/AR_NP85/new_Clustering/"
file_num = 6

row_maxnum = 4

col_num = 4.5
auto_height = False



row_num = 16/row_maxnum
if auto_height:
    col_num = 9/np.ceil(file_num / row_maxnum)


positions = []
for i in range(file_num):
    i_col = i%row_maxnum
    i_row = int(i/row_maxnum)

    x = i_col * row_num
    y = i_row * col_num
    positions.append((Inches(x), Inches(y), Inches(row_num), Inches(col_num)))


prs = Presentation()
blank_slide_layout = prs.slide_layouts[6]


for var in vars:
    slide = prs.slides.add_slide(blank_slide_layout)
    prs.slide_height = Inches(9)
    prs.slide_width = Inches(16)
    for iI_pos, pos in enumerate(positions):
        x, y, row, col = pos
        img_path = folder_path+ '/%i/pdf_%i_%s.png'%(iI_pos+1,iI_pos+1, var)
        pic = slide.shapes.add_picture(img_path, x, y, width=row, height=col)

slide = prs.slides.add_slide(blank_slide_layout)
prs.slide_height = Inches(9)
prs.slide_width = Inches(16)
for iI_pos, pos in enumerate(positions):
    x, y, row, col = pos
    img_path = folder_path+'/%i/%i%s.png' % (iI_pos + 1, iI_pos + 1, 'Hgt_500')
    pic = slide.shapes.add_picture(img_path, Inches(iI_pos*2.5), Inches(0),
                                   width=Inches(2.5), height=Inches(9))


# 保存PPT文件
prs.save(folder_path+"output.pptx")
