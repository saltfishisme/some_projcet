import os
import pandas as pd
import numpy as np
import xarray as xr
import scipy.io as scio
from metpy.interpolate import cross_section
import nc4

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import matplotlib.colors as colors
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
from scipy import ndimage

import numpy as np


def shift_grid_over_poles(grid, target_lon, target_lat, polar=False):
    # 计算目标经纬度对应的索引
    n_lons = grid.shape[1]
    n_lats = grid.shape[0]
    center_lon_idx = n_lons // 2
    center_lat_idx = n_lats // 2

    lon_idx = target_lon
    lat_idx = target_lat

    # 计算滚动量
    lon_rolls = 3*(n_lons//4) - lon_idx
    lat_rolls = 0 - lat_idx

    # 滚动经度
    shifted_grid = np.roll(grid, lon_rolls, axis=1)
    # shifted_grid = grid
    # plt.imshow(shifted_grid)
    # plt.show()
    if polar:
        print(shifted_grid[::-1, center_lon_idx].shape)
        shifted_grid_lat = np.vstack([shifted_grid[::-1,:center_lon_idx], shifted_grid[:,center_lon_idx:]])
        print(lat_rolls)
        shifted_grid_lat = np.roll(shifted_grid_lat, lat_rolls, axis=0)
        plt.imshow(shifted_grid_lat)
        plt.show()
        shifted_grid_lat = np.hstack([ shifted_grid_lat[:n_lats, :][::-1],shifted_grid_lat, ])



    exit()
    # if lat_idx < 0 or lat_idx >= n_lats:
    #     shifted_grid = np.flipud(shifted_grid)
    #     lat_rolls = -lat_rolls  # 翻转方向
    #
    # # 滚动纬度
    # shifted_grid = np.roll(shifted_grid, lat_rolls, axis=0)

    # return shifted_grid


# 示例：创建一个180x360的全球网格，纬度从北极90度到南极-90度，经度从-180到180度
grid = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_downstream\stratosphere\1980020205_2_anomaly")

grid = grid[0]

target_lon = 280  # 目标经度
target_lat = 1  # 目标纬度，假设位于北极

# 处理极点并滚动网格
shifted_grid = shift_grid_over_poles(grid, target_lon, target_lat, polar=True)

# 现在，shifted_grid是以目标经纬度为中心的数据

fig, axs = plt.subplots(2,1)
axs[0].imshow(grid)
axs[1].imshow(shifted_grid)
plt.show()
exit()
# cross = xr.open_dataset(r'D:/0.nc')
# print(cross.level)
# exit()
# xx = cross['latitude']
#
# # cross = xr.open_dataset(r'D:/20190215_11_lon.nc')
# # xx = cross['longitude']
#
# for i_file in os.listdir(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\cross_section/'):
#     cross_0 = xr.open_dataset(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\2017123117_1\20171231_17_lat.nc')
#     cross_anomaly = xr.open_dataset(r'G:\OneDrive\basis\some_projects\zhong\AR_downstream\2017123117_1\20171231_17_lat_anomaly.nc')
#
#     fig = plt.figure(1)
#     ax = plt.axes()
#
#     # Plot RH using contourf
#     rh_contour = ax.contourf(xx, cross['level'], cross_0['Hgt'].values-cross_anomaly['Hgt'].values, norm=colors.CenteredNorm())
#     rh_colorbar = fig.colorbar(rh_contour)
#
#     # Plot potential temperature using contour, with some custom labeling
#     theta_contour = ax.contour(xx, cross['level'], cross_0['Tem'].values-cross_anomaly['Tem'].values, colors='k', linewidths=2)
#     theta_contour.clabel(theta_contour.levels[1::2], fontsize=8, colors='k', inline=1,
#                          inline_spacing=8, fmt='%i', rightside_up=True, use_clabeltext=True)
#
#
#
#     # Adjust the y-axis to be logarithmic
#     ax.set_yscale('symlog')
#     ax.set_yticklabels([1000,500,200,100,50,20,10,5,2])
#     ax.set_ylim(cross['level'].max(), cross['level'].min())
#     ax.set_yticks([1000,500,200,100,50,20,10,5,2])
#
#     plt.show()
#     plt.close()
# exit()

def projection_transform(lon, lat, central_latitude, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    central_latitude_abs = abs(central_latitude)
    if (central_latitude_abs <= 60) & (central_latitude_abs >= 30):
        if central_latitude > 0:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': 30, 'lat_2': 60}
        else:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': -30, 'lat_2': -60}

    elif central_latitude_abs > 60:
        proj_Out = {'proj': 'ups'}

    else:
        proj_Out = {'proj': 'merc', 'lon_0': 120}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)

def projection_transform_orth(lon, lat, central_ll, xy2ll=False):
    """

    :param lon:
    :param lat:
    :param central_ll: [lon, lat]
    :param xy2ll:
    :return:
    """
    lon = np.array(lon)
    lat = np.array(lat)
    shape_lon = lon.shape
    shape_lat = lat.shape
    lon = lon.flatten()
    lat = lat.flatten()

    proj_Out = {'proj': 'ortho', 'lon_0': central_ll[0], 'lat_0': central_ll[1]}


    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)

def save_standard_nc(data, var_name, levels, lon=None, lat=None):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    ds = xr.Dataset()
    ds.coords["latitude"] = lat
    ds.coords["longitude"] = lon
    ds.coords['level'] = levels

    for iI_vari, ivari in enumerate(var_name):
        ds[ivari] = (('level', "latitude", "longitude"), np.array(data[iI_vari]))

    return ds

def get_circulation_data_based_on_pressure_date(date, var, var_ShortName):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    # load hourly anomaly data
    def load_anomaly():
        data_anomaly = \
            scipy.io.loadmat(
                path_PressureData + '%s/anomaly_levels/' % var + '%s.mat' % time.strftime('%m%d'))[
                time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
        return data_anomaly

    data_anomaly = load_anomaly()

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
            var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[
        var_ShortName].values
    return data_present,data_anomaly
    # return data_present


def cross_section_Analysis(s_time, start, end):
    info_nc = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/Hgt/2019/Hgt.20191231.nc'

    # vari_SaveName_all=['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
    # var_ShortName = ['z', 'r', 't', 'u', 'v', 'w']
    vari_SaveName_all=['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
    var_ShortName = ['z', 'r', 't', 'u', 'v']
    data = []
    anomaly = []
    for iI_var, i_var in enumerate(vari_SaveName_all):
        i_data, i_anomaly = get_circulation_data_based_on_pressure_date(s_time, i_var, var_ShortName[iI_var])
        data.append(i_data)
        anomaly.append(i_anomaly)

    info_nc = xr.open_dataset(info_nc)

    data = save_standard_nc(np.array(data), vari_SaveName_all,
                            info_nc.level.values, lat=info_nc.latitude.values, lon=info_nc.longitude.values)
    data = data.metpy.parse_cf().squeeze()
    cross = cross_section(data, start, end).set_coords(('latitude', 'longitude'))
    cross['t_wind'], cross['n_wind'] = mpcalc.cross_section_components(
        cross['Uwnd'],
        cross['Vwnd']
    )


    anomaly = save_standard_nc(np.array(anomaly), vari_SaveName_all,
                            info_nc.level.values, lat=info_nc.latitude.values, lon=info_nc.longitude.values)
    anomaly = anomaly.metpy.parse_cf().squeeze()
    cross_anomaly = cross_section(anomaly, start, end).set_coords(('latitude', 'longitude'))
    cross_anomaly['t_wind'], cross_anomaly['n_wind'] = mpcalc.cross_section_components(
        cross_anomaly['Uwnd'],
        cross_anomaly['Vwnd']
    )

    return cross, cross_anomaly


path_PressureData = r'/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test1979_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_1979")['filename']
resolution = 0.5
path_SaveData = r'/home/linhaozhong/work/AR_NP85/cross_section_withAnomaly/'


def contour_cal(time_file):
    save_path = path_SaveData+time_file[:-1]+'/'
    os.makedirs(save_path, exist_ok=True)

    info_AR_row = xr.open_dataset(path_contour + time_file[:-1])
    lat = info_AR_row.lat.values
    lon = info_AR_row.lon.values
    time_AR = info_AR_row.time_AR.values
    time_all = info_AR_row.time_all.values
    AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))

    def construct_lon(long):
        long = long%360
        return xr.where(long > 180,
                 long - 360,
                 long)

    for iI_timeall, i_timeall in enumerate(time_all[AR_loc:]):

        i_contour = info_AR_row['contour_all'].sel(time_all=i_timeall).values
        i_centroid = info_AR_row['centroid_all'].sel(time_all=i_timeall).values
        c_lat, c_lon = i_centroid[0:2]
        i_time = pd.to_datetime(i_timeall)

        # if os.path.exists(save_path+'%s_lat.nc'%i_time.strftime('%Y%m%d_%H')):
        #     continue

        i_hgt, i_hgt_ano = get_circulation_data_based_on_pressure_date(i_time, 'Hgt', 'z')
        i_hgt = i_hgt[20]-i_hgt_ano[20]

        def center_element(arr, index, element_size=None):
            center_index = np.array(arr.shape) // 2
            roll_amount = center_index - index
            rolled_arr = np.roll(arr, roll_amount, axis=(0, 1))


            if element_size is not None:
                return rolled_arr[center_index[0]-element_size[0]:center_index[0]+element_size[0]+1,
                                  center_index[1]-element_size[1]:center_index[1]+element_size[1]+1]
            return rolled_arr

        lon_arg = np.argwhere(lon==c_lon)
        lat_arg = np.argwhere(lat==c_lat)
        i_hgt = center_element(i_hgt, [lon_arg, lat_arg], element_size=[50,30])

        i_hgt_peak = ndimage.maximum_filter(i_hgt, size=5)







        # cross, cross_anomaly = cross_section_Analysis(i_time,
        #                        start = (np.min([c_lat+5, 90]),construct_lon(c_lon)),
        #                        end = (np.min([c_lat-5, 90]),construct_lon(c_lon)))
        # cross = cross.reset_coords('metpy_crs', drop=True)
        # cross.to_netcdf(save_path+'%s_lat.nc'%i_time.strftime('%Y%m%d_%H'))



i_type = 3
file = file_all[save_labels_bmus == i_type]
cs_lon = []
cs_lat = []
for i_file in file:
    aa = contour_cal(i_file)

exit()
for iI, i_type in enumerate(np.unique(save_labels_bmus)):
    if i_type == 0:
        continue
    file = file_all[save_labels_bmus == i_type]
    cs_lon = []
    cs_lat = []
    for i_file in file:
        # aa = contour_cal(i_file)

        save_path = path_SaveData + i_file[:-1] + '/'
        cross_section_AR = os.listdir(save_path)
        for i_cs in os.listdir(save_path):

            if '_lon' in i_cs:
                a = xr.open_dataset(save_path+i_cs)
                cs_lon.append([a['Hgt'].values, a['RH'].values])

            if '_lat' in i_cs:
                a = xr.open_dataset(save_path+i_cs)
                cs_lat.append([a['Hgt'].values, a['RH'].values])
    nc4.save_nc4(np.nanmean(cs_lon, axis=0), path_SaveData+'%i_lon'%i_type)
    nc4.save_nc4(np.nanmean(cs_lat, axis=0), path_SaveData + '%i_lat' % i_type)

exit()

