import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import numpy as np
import scipy.io as scio
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

import numpy as np


import scipy.interpolate as interp
from matplotlib import patches
def query_inserction(traj, cyclone):
    import  shapely.geometry as sgeo
    from shapely.strtree import STRtree
    # print('get cyclone one west and two north')
    def querys(traj, cyclone):
        def get_range_between_two_cyclone(cyclone1, cyclone2):
            query_geom1 = sgeo.Point(cyclone1).buffer(2.5)
            query_geom2 = sgeo.Point(cyclone2).buffer(2.5)
            lines = sgeo.LineString([cyclone1, cyclone2]).buffer(2)
            lines = lines.difference(query_geom1)
            lines = lines.difference(query_geom2)
            return lines
        polys = []
        for i in range(traj.shape[2]):
            polys.append(sgeo.LineString(traj[:,:,i]))
        s = STRtree(polys)
        index_by_id = dict((id(pt), i) for i, pt in enumerate(polys))
        if len(np.array(cyclone).shape) == 1:
            query_geom = sgeo.LineString(cyclone)
        else:
            query_geom = get_range_between_two_cyclone(cyclone[0], cyclone[1])

        bc = []
        for i, o in enumerate(s.query(query_geom)):
            if o.intersects(query_geom):
                bc.append(index_by_id[id(o)])
                # bc.append(i)
        # print(np.max(bc), len(bc))
        return bc, query_geom

    main_idx = np.arange(traj.shape[2])
    dell_idx = []
    classification = []
    geom_all = []
    geom = []
    # get two cyclone traj
    a, query_geom = querys(np.transpose(traj, [1,0,2]), cyclone[0])
    geom.append(query_geom)
    geom_all.append(query_geom)
    classification.append(a)

    dell_idx.extend(a)
    diff_idx = np.setdiff1d(main_idx, dell_idx)
    traj_diff = traj[:,:,diff_idx]

    b, query_geom = querys(np.transpose(traj_diff, [1,0,2]), cyclone[1])
    geom.append(query_geom)
    geom_all.append(query_geom)
    classification.append(diff_idx[b])
    dell_idx.extend(diff_idx[b])
    diff_idx = np.setdiff1d(main_idx, dell_idx)
    traj_diff = traj[:,:,diff_idx]

    final_classifi = classification
    return final_classifi, geom
def projection_transform(lon, lat, central_latitude, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    central_latitude_abs = abs(central_latitude)
    if (central_latitude_abs <= 60) & (central_latitude_abs >= 30):
        if central_latitude > 0:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': 30, 'lat_2': 60}
        else:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': -30, 'lat_2': -60}

    elif central_latitude_abs > 60:
        proj_Out = {'proj': 'ups'}

    else:
        proj_Out = {'proj': 'merc', 'lon_0': 120}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)
def projection_transform_orth(lon, lat, central_ll, xy2ll=False):
    """

    :param lon:
    :param lat:
    :param central_ll: [lon, lat]
    :param xy2ll:
    :return:
    """
    lon = np.array(lon)
    lat = np.array(lat)
    shape_lon = lon.shape
    shape_lat = lat.shape
    lon = lon.flatten()
    lat = lat.flatten()

    proj_Out = {'proj': 'ortho', 'lon_0': central_ll[0], 'lat_0': central_ll[1]}


    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)

def interpolation_nan_for_longpath(longpath):
    """
    skip nan and transform by median
    :param longpath: events * [lat,lon] * longpath
    :return:
    """
    longpath = longpath.copy()
    print(longpath.shape)
    arr_ref_size = longpath.shape[2]

    for i in range(len(longpath)):
        lat = longpath[i,0]
        lon = longpath[i, 1]
        # print(lat,lon)
        lat = lat[~np.isnan(lat)]
        lon = lon[~np.isnan(lon)]
        # print(lat[np.isinf(lat)])
        # print(lon[np.isinf(lon)])
        lat = lat[~np.isinf(lat)]
        lon = lon[~np.isinf(lon)]

        # transpose
        def get_median_lon(lon):
            lon = lon.copy()
            from function_shared_Main import roll_longitude_from_359to_negative_180
            lon_len = len(lon)
            len_larger300 = len(lon[lon>300])
            if len_larger300/lon_len >0.2:
                lon_roll = roll_longitude_from_359to_negative_180(lon)
                median = np.median(lon_roll)
                median = (360+median)%360
            else:
                median = np.median(lon[lon<300])
            return median

        lon_trans, lat_trans = projection_transform_orth(lon, lat, [get_median_lon(lon), np.median(lat)])
        # if len(lon_trans[0][np.isinf(lon_trans[0])]) != 0:
        #     print('1')
            # print([get_median_lon(lon), np.median(lat)])
            # print('#################')
            # print(lon)
            # print(lon_trans)
            # print('#################')
            # print(lat)
            # print(lat_trans)
        ll_inter = []

        for iI_arr, arr in enumerate([lat_trans, lon_trans ]):

            arr1_interp = interp.interp1d(np.arange(len(arr)), arr)
            inter = arr1_interp(np.linspace(0, arr.size - 1, arr_ref_size))
            ll_inter.append(inter)
        # if len(ll_inter[0][np.isinf(ll_inter[0])]) != 0:
        #     print('$$$$$$$$$$$$$$$$$$$$$')
        #     print(lat)
        #     print(lon)
        lon_plate, lat_plate = projection_transform_orth(ll_inter[1], ll_inter[0], [get_median_lon(lon), np.median(lat)], xy2ll=True)
        # print('#################')

        # print(lon)
        # print(lon_plate)

        # if len(ll_inter[0][np.isinf(ll_inter[0])]) != 0:

            # print('SSSS')
            # print(lon)
            # print(lon_plate)
        longpath[i,0] = lat_plate
        longpath[i,1] = lon_plate
    return longpath


def SOM_param(data, xylength, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = []
    for lead in ['X', 'Y']:
        for i in range(1, xylength):
            varname.append(str(i) + lead)
    if isinstance(addtional_varname, list):
        varname.extend(addtional_varname)
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})





def plot_centroid(gs_index, save_labels, longpath, savename=''):
    def reject_outliers(data, m = 0.8):
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev if mdev else 0.
        return np.nanmean(data[s<m])
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1, colors_shape[0]])
        return ListedColormap(np.concatenate((colors, ones.T), axis=1))
    def plot_trajectory(ax, pos_int):


        ################################# prepare data ################################

        from sklearn.preprocessing import MinMaxScaler
        import numpy as np

        data = np.arange(pos_int.shape[-1])  # 给定数据
        scaler = MinMaxScaler()
        scaled_data = scaler.fit_transform(data.reshape(-1, 1))

        rr_int = scaled_data.flatten()

        ################################## plot trajectory ###############################

        import matplotlib.pyplot as plt
        import numpy as np
        cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/rainbow200.mat', 'rainbow200')

        def plot_colored(ax, x, y, data, cmap, steps=1):
            '''
            :param ax: axes
            :param x: (N,) np.array
            :param y: (N,) np.array
            :param data: (N,) np.array
            :param cmap:
            :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
            :return:
            '''
            ''' from matplotlib import cm
            cmpa should be    cm.get_cmap(cmpas_zhong, 100)

             $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
             before plot all data, please normalized all this data.'''
            data = np.asarray(data)
            it = 0
            while it < (data.size - steps):
                x_segm = x[it:it + steps + 1]
                y_segm = y[it:it + steps + 1]

                c_segm = cmap(data[it + steps // 2])
                ax.scatter(x_segm, y_segm, s=0.4, linewidths=0.4,c=c_segm, transform=ccrs.Geodetic(),
                           alpha=0.4,marker='.')
                it += steps

        def plot_colorbar(cmap):
            date_len = 10
            date_len = date_len + 1
            import numpy as np
            import matplotlib.pyplot as plt
            Z = np.zeros([6, date_len])
            Z[0, :] = np.arange(0, 101, 10)
            x = np.arange(-0.5, date_len, 1)  # len = 21
            y = np.arange(4.5, 11, 1)  # len = 7

            ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
            cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

            ax0.set_visible(False)
            return cb

        ####### plot colorbar in given cmap  ##########
        # cb_ax = fig.add_axes([0.1, 0.02, 0.8, 0.01])
        # from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
        # ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
        # cb_ax.set_axes_locator(ip)
        # cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
        # cbar.ax.annotate('%', xy=(0.45, -7.5), xycoords='axes fraction')
        # cbar.set_ticks(np.arange(0, 101, 25))
        ####### plot trajectories by colored lines   ##########
        from matplotlib import cm
        # viridis = cm.get_cmap(cmpas_zhong, 500)
        for i in range(pos_int.shape[0]):
            # for i in range(200):
            plot_colored(ax, pos_int[i, 1, :], pos_int[i, 0, :], rr_int, cmap=cmpas_zhong)

        ####### plot end of trajectories with black dot  ##########
        ax.scatter(pos_int[ :,1, -1,], pos_int[ :, 0, -1,], c='k', s=1, marker='.', linewidths=0.01,
                   transform=ccrs.PlateCarree())

        ################################# add some feature #################################

        ax.coastlines(linewidth=0.2)
        # ax.gridlines(linewidth=0.5, colors='k')
        ################################# plot ERA5 TOPO ###################################

        cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
        topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
        lat25 = topo['lat25']
        lon25 = topo['lon25']
        topo25 = topo['topo25']
        ax.pcolormesh(lon25, lat25, topo25, shading='nearest', vmin=-6000, vmax=6000, cmap=cmap_topo,
                      transform=ccrs.PlateCarree(), zorder=0)
    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len+1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :]=np.arange(0,101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb


    size = int(np.ceil(len(np.unique(save_labels)) / 3))

    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            if gs_index > 0:
                ax = fig.add_subplot(gs[int(iI/3),1+iI%3], projection=ccrs.NorthPolarStereo())
                ax.set_title('%s SOM%i %i, %0.0f%%' % (figlabelstr[iI+1],
                i, longpath[save_labels == i].shape[0],
                100 * longpath[save_labels == i].shape[0] / longpath.shape[0])
                             , fontsize=8)
                # plt.show()


            else:
                ax = fig.add_subplot(gs[:,gs_index], projection=ccrs.NorthPolarStereo())

                ax.set_title('(a) Total', fontsize=8)
                cb_ax = fig.add_axes([0.2, 0.08, 0.6, 0.01])
                cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/rainbow200.mat', 'rainbow200')
                cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
                # cbar.ax.annotate('%', xy=(0.45, -7.5), xycoords='axes fraction')
                cbar.set_ticks([0, 100])
                cbar.set_ticklabels(['origin', 'decay'])
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())

            plot_trajectory(ax, longpath[save_labels == i])
            # ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.05,
            #            transform=ccrs.PlateCarree())
            # s_centroid = np.apply_along_axis(reject_outliers, 0, centroid[save_labels == i])
            # e1 = patches.Ellipse((s_centroid[1], s_centroid[0]), s_centroid[4], s_centroid[5],
            #                      angle=180-np.nanmean(centroid[save_labels == i, 3]), color='red', linewidth=1, fill=False, zorder=2, transform=ccrs.PlateCarree())
            # ax.add_patch(e1)

            # ax.set_title('Total %i'%(longpath[save_labels == i].shape[0]))

            # plt.show()

    # # # plot classification result
    # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    # for i in np.unique(save_labels[save_labels == 0]):
    #     ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    # ax.coastlines()
    # plt.title(season)
    # plt.show()
    # plt.close()
def plot_centroid_eul(save_labels, longpath, savename=''):

    def flatten_trajectorytoEuler(data, lon, lat):
        """
        :param data: [event,2(lon,lat),trajectory],
        :param lon:
        :param lat:
        :return:
        """
        print(data.shape)
        data_eul = np.zeros([len(lat), len(lon)])
        for i_event in range(data.shape[0]):
            for i_date in range(data.shape[1]):
                lon_now = data[i_event, 1,i_date]
                lat_now = data[i_event, 0, i_date]
                print(lon_now, lat_now)
                index = [0, 0]
                index[1] = np.argmin(abs(lon - lon_now))
                index[0] = np.argmin(abs(lat - lat_now))
                data_eul[index[0], index[1]] += 1
        return data_eul

    size = int(np.ceil(len(np.unique(save_labels)) / 3))
    fig = plt.figure()

    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            ax = fig.add_subplot(size, 3, iI + 1, projection=ccrs.NorthPolarStereo())
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
            lon = np.arange(-180,180,0.5)
            lat = np.arange(90,-90.1,-0.5)
            centroid_eul = flatten_trajectorytoEuler(longpath[save_labels==i],lon, lat)

            ax.pcolormesh(lon,lat, centroid_eul,transform=ccrs.PlateCarree())
            ax.set_title('Type %i %i, %0.0f%%'%(i, longpath[save_labels == i].shape[0], 100*longpath[save_labels == i].shape[0]/longpath.shape[0]))
            ax.coastlines()
    plt.tight_layout()
    plt.show()
    # plt.savefig(path_pic+'%s_%s.png'%(season, savename), dpi=500)
    plt.close()
    # # # plot classification result
    # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    # for i in np.unique(save_labels[save_labels == 0]):
    #     ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    # ax.coastlines()
    # plt.title(season)
    # plt.show()
    # plt.close()
import scipy.io as scio


"""
previous step is downscale_1H_centroid.py to create centroid_all_2000.mat
"""

a = scio.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\SOM/centroid_deinf_2000.mat')

longpath = a['centroid']
save_labels_bmus = scio.loadmat(r"D:\OneDrive\a_matlab\a_test\test_BMUS.mat")['BMUS'][:, -1].flatten()
longpath =longpath.reshape([longpath.shape[0], 2,int(longpath.shape[1]/2)])
lon,lat = projection_transform(longpath[:,1], longpath[:,0], 80, xy2ll=True)

longpath[:,1] = lon
longpath[:,0] = lat

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

fig = plt.figure(figsize=[7,4.2])
import matplotlib.gridspec as gridspec
gs = gridspec.GridSpec(2, 4)
plt.subplots_adjust(top=0.95,
bottom=0.11,
left=0.02,
right=0.99,
hspace=0.15,
wspace=0.15)

plot_centroid(1, save_labels_bmus, longpath)

save_labels_bmus[:] = 1
plot_centroid(0, save_labels_bmus, longpath)
####### plot colorbar in given cmap  ##########



from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

# ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
# cb_ax.set_axes_locator(ip)



# plt.tight_layout()
# plt.show()
plt.savefig('SOM_cb.png', dpi=500)
plt.close()