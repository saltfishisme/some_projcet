### Exectutalbe gernralised budget script
import xarray as xr
import matplotlib.pyplot as plt

import numpy as np
import pickle
import datetime as dt

from dateutil.relativedelta import relativedelta
import copy

def load_ivt_total(s_time, types='anomaly'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                '%s.mat' % s_time.strftime('%m%d'))[
                       s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]
    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                '%s.mat' % s_time.strftime('%m%d'))[
                       s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    ivt_climate = np.sqrt(index_uivt_anomaly ** 2 + index_vivt_anomaly ** 2)
    if types=='anomaly':
        return ivt_total-ivt_climate
    else:
        return [ivt_total, ivt_climate]


import os
import numpy as np
import pandas as pd
import xarray as xr
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4
import scipy.io as scio
from datetime import timedelta

def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='anomaly'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    if types == 'anomaly':
        return data_present - data_anomaly
    else:
        return [data_present, data_anomaly]


'''
prepare for esiv pdf
'''
"""prepare for esiv, load pioms grid and trans into stree"""
import cv2
resolution = 0.5
long = np.arange(0, 360, 0.5)
lat = np.arange(90, -90.01, -0.5)
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
path_PIOMS = r'/home/linhaozhong/work/Data/PIOMS_nc/'

# load pioms grid
ai_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/aiday_1979_2020')
hi_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/hiday_1979_2020')
ah_anomaly = ai_anomaly * hi_anomaly * 100
pioms_grid = xr.open_dataset(path_PIOMS + r"aiday.H1985.nc")
points_lon = pioms_grid.x.values
points_lat = pioms_grid.y.values
from function_shared_Main import cal_area_differentLL_1d

weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)

# transform into 0,1,2,3 relative coordination, only works in north hemisphere
# !!!!!!!!!!!!!!!!!!!!!!!#
points_lat_relaCoord = (90 - points_lat) / resolution
points_lon_relaCoord = points_lon / resolution

# to Stree
from shapely.strtree import STRtree
from shapely.geometry import Point

points = [Point(x, y) for x, y in zip(points_lon_relaCoord, points_lat_relaCoord)]
tree = STRtree(points)  # create a 'database' of points


def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, level=[1000, 850, 500, 200],
                                                type='daily'):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    # load hourly anomaly data
    def load_anomaly(type):
        if type == 'daily':
            data_anomaly = \
                scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                    time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly':
            data_anomaly = \
                scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m'))[
                    time.strftime('%m')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly_now':

            def monthly_mean(var_dir, var_save, var_ShortName, time):
                path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir

                if os.path.exists(path_SaveData + '%s.mat' % time.strftime('%Y%m')):
                    return scio.loadmat(path_SaveData + '%s.mat' % time.strftime('%Y%m'))[time.strftime('%Y%m')]

                else:
                    date_montly_now = pd.date_range(time.strftime('%Y-%m-') + '01', freq='1D', periods=31)
                    dates = date_montly_now[date_montly_now.month == int(time.strftime('%m'))].strftime(
                        '%Y%m%d')
                    year = dates[0][:4]

                    def get_data(var_dir, i_Year, var_save, i_MonDay):
                        s_data = xr.open_dataset(
                            path_PressureData + '%s/%s/%s.%s%s.nc' % (
                                var_dir, i_Year, var_save, i_Year, i_MonDay))
                        s_data = s_data.sel(level=level)
                        s_data = s_data[var_ShortName].values
                        return np.nanmean(s_data, axis=0)

                    data_in_MonDay = []

                    for iI_MonDay, i_MonDay in enumerate(dates):
                        # work for 0229
                        s_data = get_data(var_dir, year, var_save, i_MonDay[4:])
                        data_in_MonDay.append(s_data)

                    data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
                    data_in_MonDay = np.array(data_in_MonDay, dtype='double')

                    path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir
                    os.makedirs(path_SaveData, exist_ok=True)
                    scipy.io.savemat(path_SaveData + '%s.mat' % time.strftime('%Y%m'),
                                     {time.strftime('%Y%m'): data_in_MonDay})

                    return data_in_MonDay

            data_anomaly = monthly_mean('Hgt', 'Hgt', 'z', time)

        return data_anomaly

    data_anomaly = load_anomaly(type)

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
            var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

    return np.array([data_present, data_anomaly])



def load_esiv(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_ai = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * np.array(pioms_ai, dtype='double') * 100
    pioms_ah_ano = pioms_ah - ah_anomaly[loc]

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]/np.nansum(weighted_PIOMS[index])
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return np.nansum(pioms_ah_ano * weighted_PIOMS_contour)
    else:
        return [pioms_ah, ah_anomaly[loc]]


def load_heff(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double')  * 100
    pioms_ah_ano = pioms_ah - (hi_anomaly[loc]* 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, hi_anomaly[loc]*100]
def load_sic(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double')  * 100
    pioms_ah_ano = pioms_ah - (ai_anomaly[loc]* 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, ai_anomaly[loc]*100]
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
resolution = 0.5
path_SaveData = r'/home/linhaozhong/work/AR_NP85/new_Clustering_paper_plot/'
var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
               'hcc': 'high_cloud_cover',
               'lcc': 'low_cloud_cover',
            'mcc': 'medium_cloud_cover',
               'tcc': 'total_cloud_cover',
               'tciw': 'total_column_cloud_ice_water',
            'tclw': 'total_column_cloud_liquid_water',

}


def contour_cal(time_file, var='IVT'):
    info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

    time_all = info_AR_row.time_all.values

    time_here = []
    time_here_loc = []
    for iI_timeall, i_timeall in enumerate(time_all):
        i_contour = info_AR_row['contour_all'].sel(time_all=i_timeall).values
        from function_shared_Main import cal_area
        weighted = cal_area(info_AR_row.lon.values, info_AR_row.lat.values)
        sic, _ = get_circulation_data_based_on_date(pd.to_datetime(i_timeall), 'Sea_Ice_Cover', 'siconc',
                                                    types='now')
        i_contour[sic < 0.01] = 0
        i_contour[np.isnan(sic)] = 0
        area_here = np.nansum(weighted[i_contour > 0.01])
        if area_here > 50 * (10 ** 4):
            time_here.append(str(i_timeall))
            time_here_loc.append(iI_timeall)

    # skip if two consecutive time points faill to meet the criteria.
    def consecutive(data):
        max_segment = []
        max_indices = []
        current_segment = []
        current_indices = []

        for index, num in enumerate(data):
            if num == 1:
                current_segment.append(num)
                current_indices.append(index)
            else:
                if len(current_segment) > len(max_segment):
                    max_segment = current_segment
                    max_indices = current_indices
                current_segment = []
                current_indices = []

        if len(current_segment) > len(max_segment):
            max_segment = current_segment
            max_indices = current_indices

        return max_indices

    time_here_loc_div = np.diff(time_here_loc)
    time_here_loc_div[time_here_loc_div <= 2] = 1
    time_here_loc_div[time_here_loc_div > 2] = 2

    time_here_loc_div = consecutive(time_here_loc_div)
    if len(time_here_loc_div) <= 1:
        return 'no'
    time_use = np.array(time_here)[time_here_loc_div + [time_here_loc_div[-1] + 1]]
    # time_use = info_AR_row['time_AR'].values
    var_all = []
    for i_time in time_use:
        contour = info_AR_row['contour_all'].sel(time_all=i_time).values

        if var == 'IVT':
            s_i = load_ivt_total(pd.to_datetime(i_time), types='now')
        elif var == 'tcwv':
            s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                     'WaterVapor',
                                                     'tcwv', var_dir='Total_column_water_vapour', types='now')
        elif var == 'esiv':
            s_i = load_esiv(pd.to_datetime(i_time), contour)
        elif var == 'heff':
            s_i = load_heff(pd.to_datetime(i_time), contour)
        elif var == 'sic':
            s_i = load_sic(pd.to_datetime(i_time), contour)
        elif var == 'hgt':
            # only keep 500hPa
            s_i = get_circulation_data_based_on_pressure_date(pd.to_datetime(i_time), 'Hgt', 'z')[:,2]
            print(s_i.shape)
        else:
            s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                     var_LongName[var],
                                                     var, types='now')
        # save
        if (plot_type == 'pdf') & (var != 'esiv'):
            s_i = s_i[0] - s_i[1]
            contour[int(30 / resolution), :] = 0
            s_i[contour != 1] = np.nan
            var_all.extend(s_i[~np.isnan(s_i)].flatten())
        else:
            var_all.append(s_i)

    if plot_type == 'pattern':
        var_all = np.nanmean(np.array(var_all), axis=0)
    return var_all

plot_type = 'pattern'  # 'pattern'/'pdf'/'pdf_weighted'
# for var in ['IVT', 't2m', 'esiv', 'tcwv', 'str', 'slhf', 'sshf', 'tcc','cbh','hcc','lcc','mcc']:
for var in ['hgt',]:

    for iI, i_type in enumerate(np.unique(save_labels_bmus)):
        if i_type == 0:
            continue
        file = file_all[save_labels_bmus == i_type]

        phase1 = []

        for i_file in file:
            aa = contour_cal(i_file, var)
            if not isinstance(aa, str):
                if plot_type == 'pdf':
                    phase1.extend(aa)
                else:
                    phase1.append(aa)
                    print('hihi')

        if plot_type == 'pdf':
            phase = [np.array(phase1).flatten()]
            for iI_phase, i_phase in enumerate(phase):
                nc4.save_nc4(i_phase,
                             path_SaveData + 'pdf_type%i_%s_%s' % (i_type, var, plot_type))
        else:
            phase = [np.array(phase1)]

            for iI_phase, i_phase in enumerate(phase):
                print(i_phase.shape)
                from scipy import stats

                _, p_values = stats.ttest_ind(i_phase[:,0,:], i_phase[:,1,:], axis=0)

                nc4.save_nc4(p_values,
                             path_SaveData + 'pdf_type%i_%s_P' % (i_type, var))
                i_phase = np.nanmean(np.array(i_phase), axis=0)
                nc4.save_nc4(i_phase[0],
                             path_SaveData + 'pdf_type%i_%s_%s_ar' % (i_type, var, plot_type))
                nc4.save_nc4(i_phase[1],
                             path_SaveData + 'pdf_type%i_%s_%s_cli' % (i_type, var, plot_type))
                nc4.save_nc4(i_phase[0]-i_phase[1],
                             path_SaveData + 'pdf_type%i_%s_%s' % (i_type, var, plot_type))

exit()

"""
plot
"""
# import matplotlib.pyplot as plt
# import nc4
#
# SMALL_SIZE = 7
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
# plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
# plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
#
# infig_fontsize = 5
#
# # [0:1, 1:varname, 2:xrange, 3:yrange, 4:round for mean, 5:plus, 6:BIN]
#
# plot_param = {'cbh': [['T2Mmean=', ' K'], 'CBH', [0, 120], 1],
#               'hcc': [['SSHFmean=', ' W/$m^{2}$'], 'HCC', [0, 120], 1 ],
#               'lcc': [['SLHFmea=', ' W/$m^{2}$'], 'LCC', [0, 120], 1],
#               'mcc': [['ADVmean=', ' K/6h'], 'MCC', [0, 120], 1],
#               'tciw': [['IVTmean=', ' kg/(m*s)'], 'TCIW', [0, 120], 1],
#               'tclw': [['TCWVmean=', ' kg/$m^{2}$'], 'TCLW', [0, 120], 1, ],
#               'advection': [['ADVmean=', ' K/6h'], 'advection', [0, 120], 6 * 3600],
#               'adibatic': [['ABmean=', ' K/6h'], 'adiabatic', [0, 120], 6 * 3600],
#               'siconc': [['SICmean=', ' %'], 'SIC', [0, 120], 100],
#               'ssr': [['TCWVmean=', ' W/$m^{2}$'], 'TCWVano', [0, 120], 1 / (6 * 3600)],
#
#               'IVT': [['IVTmean=\n', ' kg/(m*s)'], 'IVT', [-10, 200], [0, 0.03], 0, 1,600],
#               'tcwv': [['TCWVmean=\n', ' kg/(m**2)'], 'TCWVano', [-1.5, 12], [0, 0.4], 1, 1, 300],
#               't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-5, 25], [0, 0.15], 2, 1,200],
#               'str': [['STRmean=', ' W/$m^{2}$'], 'STR', [-5, 15], [0, 0.2], 2, 1 / (6 * 3600),200],
#               'slhf': [['SLHFmea=', ' W/$m^{2}$'], 'SLHF', [-2, 3], [0, 0.7], 2, 1 / (6 * 3600),2400],
#               'sshf': [['SSHFmean=', ' W/$m^{2}$'], 'SSHF', [-5, 15], [0, 0.2], 2, 1 / (6 * 3600),600],
#               'tcc': [['tccmean=', ' K/6h'], 'TCC', [-0.1, 0.5], [0, 10], 2, 1,300],
#               }
#
# for var in ['IVT', 'tcwv', 't2m', 'str', 'slhf', 'sshf', 'tcc']:
# # for var in ['tcc']:
#     def plots(ax, data):
#         percentile_label = ['Q1', 'Q2', 'Q3']
#         percentile = np.percentile(data, [25,50,75])
#         mean = np.nanmean(data)
#         # HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
#
#         _, _, bar_container = ax.hist(data,bins=bin_col, density=True, lw=1,
#                                   ec="black", fc="gray",zorder=2)
#
#         trans = ax.get_xaxis_transform()
#         for i_percentile in range(3):
#             ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
#             ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
#                     transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
#                     bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
#             )
#             if plot_param[var][4] == 0:
#                 values =percentile[i_percentile].astype('int')
#             else:
#                 values = np.round(percentile[i_percentile], plot_param[var][4])
#
#             ax.text(percentile[i_percentile], 1.01, values, transform=trans,
#                     horizontalalignment='center', fontsize=infig_fontsize)
#
#         return mean, [ax.get_xlim()[0], ax.get_xlim()[1]]
#
#
#     fig, axs = plt.subplots(6, 1, figsize=[4, 5])
#     plt.subplots_adjust(top=0.91,
# bottom=0.04,
# left=0.125,
# right=0.98,
# hspace=0.4,
# wspace=0.2)
#     lim = np.zeros([6, 3])
#
#     # get data and set bin width
#     s_data = []
#     for i in range(6):
#         s_data.extend(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/' +
#                             'pdf_type%s_%s' % (i+1, var))*plot_param[var][5])
#     def flatten(l):
#         l = np.array([item for sublist in l for item in sublist])
#         return l[~np.isnan(l)]
#
#     bin_col = np.histogram(s_data, bins=plot_param[var][6])[1]
#     for i in range(6):
#         data = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/' +
#                             'pdf_type%s_%s' % (i+1, var))*plot_param[var][5]
#         ax = axs[i]
#         mean,  lim[i,:2]= plots(ax, data)
#
#         if var != 'IVT':
#             ax.axvline(0, color='black',zorder=0)
#
#         mean_label = plot_param[var][0]
#         if plot_param[var][4] == 0:
#             mean = int(mean)
#         else:
#             mean = np.round(mean, plot_param[var][4])
#         lim[i,2] = mean
#         if var != 'sic':
#             x = 0.85
#
#         else:
#             x=0.15
#
#         ax.text(x, 0.6, mean_label[0]+str(mean)+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)
#
#     # maxmean = np.max(lim[:, 2])
#     # maxx = np.max(lim[:, 1])
#     # maxleft = np.min(lim[:, 0])
#     # if maxmean>=0:
#     #     maxx = np.min([maxmean*4, maxx])
#     # else:
#     #     maxmean = np.min(lim[:, 2])
#     #     maxleft = np.max([maxmean*4, maxleft])
#
#     x_range = plot_param[var][2]
#     y_range = plot_param[var][3]
#     for i in range(6):
#         axs[i].set_xlim(right=x_range[1], left=x_range[0])
#         axs[i].set_ylim(top=y_range[1])
#         axs[i].set_ylabel('Type%i'%(i+1))
#
#     # axs[0].set_title(arType_name)
#     # plt.show()
#     plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_%s.png' % (var), dpi=300)
#     plt.close()


"""
plot for pattern
"""

central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]

SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])

def plot_pcolormeshP_onetype(data, p_values):


    fig, axs = plt.subplots(2,4, subplot_kw={'projection':ccrs.NorthPolarStereo()}
                            ,figsize=[10,6])

    for i_i, i in enumerate([0,1,2,3,4,7,8]):
        ax = axs[int(i_i/4), i_i%4]
        ax.set_title('%s anomaly for Type%s' %(budget_load[i], i_type))

        import cmaps
        import matplotlib.colors as colors
        a = xr.open_dataset(r"D:\out_test\budfields_20120729.nc")

        lon = a.lon.values[0]
        lat = a.lat.values[0]
        cb = ax.pcolormesh(lon, lat, data[i]*plus[i],
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(), vmin=-0.2,vmax=0.2)
        # ax.pcolormesh(lon, lat, np.ma.masked_greater(p_values[i], 0.1),
        #
        #             colors='none', levels=[0, 0.1],
        #             hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        ax.coastlines(linewidth=0.3, zorder=10)
        # ax.stock_img()
        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
        # plt.show()
        ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
        # cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
        cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)

        # cb_ax.set_xlabel('K')
    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/AR%i_%s.png' % (i_type, time_use_forAR_name), dpi=400)
    plt.close()


def plot_pcolormeshP_onevar(data, p_values, vartype, lon,lat):
    fig = plt.figure(figsize=[8, 5.5])
    plt.subplots_adjust(top=0.98,
bottom=0.07,
left=0.02,
right=0.98,
hspace=0.0,
wspace=0.2)

    for i_i, i in enumerate([1,2,3,4,5,6]):
        central_lonlat = central_lonlat_all[i_i]
        ax = fig.add_subplot(2, 3, i,
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

        ax.set_title('%s anomaly for Type%s' %(vartype, i))

        import cmaps
        import matplotlib.colors as colors

        # cb = ax.pcolormesh(lon, lat, data[i_i]*plus[vartype],
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree())
        p_pass = np.where(p_values[i_i] <= 0.1)
        cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]),
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree())
        # cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))

        ax.scatter(lon[p_pass], lat[p_pass],s=0.1,c='black',marker='*',linewidth=0.1,
                  transform=ccrs.PlateCarree(), zorder=20)

        ax.coastlines(linewidth=0.3, zorder=10)

        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')

        ax.set_extent([-2e+06, 2e+06, -2e+06, 2e+06],ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

    cb_ax = fig.add_axes([0.15, 0.06, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cb_ax.set_xlabel(unit[vartype])

    plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/%s_AR_%s_p.png' % (vartype, time_use_forAR_name), dpi=400)
    plt.close()


for var in ['IVT']:
    ar = []
    p = []
    for i_type in range(1,6+1):
        ar.append(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var)))

        p.append(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_P' % (i_type, var)))
    ar = np.array(ar)
    p = np.array(p)
    print(p.shape)
    plot_pcolormeshP_onevar(ar, p, var, np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))




