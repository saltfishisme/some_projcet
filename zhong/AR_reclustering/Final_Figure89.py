


"""
plot for pattern
"""

import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr


budget_load = ['IVT','t2m','heff',
               'sic','tcwv',
               'str', 'sshf', 'slhf',
               'cbh','tcc','hcc','mcc','lcc']
figlabels_diffVar = dict(zip(budget_load, [
    ['(a)', '(c)'], ['(b)', '(d)'],['(e)', '(g)'],
    ['(f)', '(h)'],['(a)', '(c)'],
['(a)', '(d)'], ['(b)', '(e)'],['(c)', '(f)'],
['(a)', '(d)'], ['(b)', '(e)'],['(a)', '(d)'], ['(b)', '(e)'],['(c)', '(f)'],
]
                             ))
plus = dict(zip(budget_load, [
        1,1,1,
        1,1,
        1 / (6 * 3600),1 / (6 * 3600),1 / (6 * 3600),
        1/1000,1,1,1,1,]))

vrange = dict(zip(budget_load, [
        np.arange(-100,101,10),np.arange(-10,10.01,0.5),np.arange(-100,100.01,10),
        np.arange(-100,100.01,10),np.arange(-2,2.01,0.1),
        np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),
        np.arange(-2,2.01,0.1),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01)]))

unit = dict(zip(budget_load, [
        'kg/(m*s)','K','cm',
        '%','kg/(m**2)',
        'W/$m^{2}$','W/$m^{2}$','W/$m^{2}$',
        'km','','','','',]))
xlocs = [[['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['', 179.99, 85], ['', -120, 85],
          ['60°W', -60, 75]],
         [['0°', 0, 82], ['60°E', 60, 75], ['120°E', 120, 66], ['180°', 179.99, 66], ['120°W', -120, 75],
          ['60°W', -60, 82]],
         [['120°E', 120, 80], ['180°', 179.99, 55], ['120°W', -120, 80]],
         [['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['180°', 179.99, 82], ['120°W', -120, 82],
          ['60°W', -60, 75]],
         [['0°', 0, 80], ['120°W', -120, 80], ['60°W', -60, 55]],
         [['120°E', 120, 75],['180°', 179.99, 55],['120°W', -120, 75]]]
central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]
# central_lonlat_all = [[30,80],[180,70]]
SMALL_SIZE = 6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])




def plot_axes4(iI_axs, data, p_values, vartype, lon,lat, hgt):
    """
    :param axs: axs contains 1,6 axes
    :param data: [som1-6,lat, lon]
    :param p_values:  [som1-6,lat, lon]
    :param vartype: 'mcc' like
    :param lon: 2d
    :param lat: 2d
    :return:
    """

    for i_i, i in enumerate([1,6]):
        var_inType = data[i-1]
        var_inType_p = p_values[i-1]
        hgt_inType = hgt[i-1]

        central_lonlat = central_lonlat_all[i - 1]
        ax = fig.add_subplot(gs[i_i,iI_axs],
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],
                                                          central_latitude=central_lonlat[1]))
        ax.set_title("%s %s' for SOM%s" %(figlabels_diffVar[vartype][i_i], vartype.upper(), i),loc='left')

        import cmaps
        import matplotlib.colors as colors

        ax.contourf(lon[:120], lat[:120], np.ma.masked_greater(var_inType_p[:120], 0.1),
                    colors='none', levels=[0, 0.1],
                    hatches=[3 * '.', 3 * '.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        cb = ax.contourf(lon[:120], lat[:120],
                         var_inType[:120] * plus[vartype],
                         cmap=cmap_use, levels=vrange[vartype],
                         transform=ccrs.PlateCarree(), extend='both')

        if i == 6:
            boundary = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM%i.mat'%i)
            # plt.imshow(np.array(boundary['boundary']))
            # plt.show()
            boundary_index = np.where(np.array(boundary['boundary'], dtype='single')>3)
            ax.scatter(boundary['lon'][boundary_index], boundary['lat'][boundary_index],
                       transform=ccrs.PlateCarree(),
                       zorder=50,
                       color='red', s=0.1)

        sea_edge = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot\pattern_type%i_siconc"%i)

        if i == 6:
            sea_edge = sea_edge[[1, 0], :, :]
        ax.contour(lon[:120], lat[:120],
                         sea_edge[0][:120],
                         colors='blue', levels=[0.15],
                         transform=ccrs.PlateCarree())
        ax.contour(lon[:120], lat[:120],
                         sea_edge[1][:120],
                         colors='red',levels=[0.15],
                         transform=ccrs.PlateCarree())

        cblabel = ax.contour(lon[:120], lat[:120],
                         hgt_inType[:120]/98,
                   colors='black',
                         levels=np.arange(-10,11, 1),
                           transform=ccrs.PlateCarree(), extend='both',zorder=80, alpha=0.8)
        ax.clabel(cblabel,fmt='%i',inline_spacing=0.01)
        ax.coastlines(linewidth=0.3, zorder=10)

        from function_shared_Main import add_longitude_latitude_labels_givenloc
        add_longitude_latitude_labels_givenloc(fig, ax, xlocs[i-1], yloc=central_lonlat[0])
        # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
        ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
                      ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        if i == 2:
            ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

    return cb

import matplotlib.gridspec as gridspec
fig = plt.figure(figsize=[6,4])
gs = gridspec.GridSpec(2, 3)
plt.subplots_adjust(top=0.98,
bottom=0.085,
left=0.02,
right=0.98,
hspace=0.0,
wspace=0.11)

lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
# for iI_var, var in enumerate(['str','sshf', 'slhf']):
for iI_var, var in enumerate(['hcc', 'mcc', 'lcc']):
    ar = []
    p = []
    hgt = []
    sic = []
    for i_type in range(1,6+1):
        ar.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var)))
        p.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_P' % (i_type, var)))
        hgt.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, 'hgt')))

    ar = np.array(ar)
    p = np.array(p)
    hgt = np.array(hgt)

    s_figlabels_diffVar = figlabels_diffVar[var]

    cb = plot_axes4(iI_var, ar, p, var, lon_2d, lat_2d, hgt)

    # plt.show()
cb_ax = fig.add_axes([0.15, 0.06, 0.7, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)

# plt.show()
# cb_ax.set_xlabel('W $\mathrm{m^{-2}}$ every 6 hours')
# plt.savefig(r'figure8.png', dpi=400)
# plt.savefig(r'figure9.png', dpi=400)
plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-14.png', dpi=400)
exit()




