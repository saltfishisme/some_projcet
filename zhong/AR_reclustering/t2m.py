
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os


"""
=============================
2024.1.4 add a 9-yr running windows to compute the climatology of load_esiv, heff, sic
=============================
"""

import datetime


def load_Q(s_time, var, types='climate'):
    """
    :param s_time: datetime64
    :param var:
    :param type: climate or now
    :return:
    """

    def create_save_adv_adi(time):
        import metpy.calc as mpcalc
        from metpy.units import units
        import numpy as np
        import xarray as xr
        import os

        def get_data(time, var, level_isobaric):
            '''
            :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
            :param level_isobaric:
            :return:
            '''

            def get_svar_data(var):
                data = xr.open_dataset(
                    path_PressureData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
                return data

            result = []
            if np.isin(var, 'Hgt').any():
                hght_var = get_svar_data('Hgt').sel(time=time, level=level_isobaric)['z']
                result.append(hght_var)
            if np.isin(var, 'Tem').any():
                temp_var = get_svar_data('Tem').sel(time=time, level=level_isobaric)['t']
                result.append(temp_var)
            if np.isin(var, 'Uwnd').any():
                u_wind_var = get_svar_data('Uwnd').sel(time=time, level=level_isobaric)['u']
                result.append(u_wind_var)
            if np.isin(var, 'Vwnd').any():
                v_wind_var = get_svar_data('Vwnd').sel(time=time, level=level_isobaric)['v']
                result.append(v_wind_var)
            if np.isin(var, 'Wwnd').any():
                w_wind_var = get_svar_data('Wwnd').sel(time=time, level=level_isobaric)['w']
                result.append(w_wind_var)

            lat_var = result[0]['latitude']
            lon_var = result[0]['longitude']
            result.append(lat_var)
            result.append(lon_var)
            return result

        def tem_advection_1000(time):
            temp_var, u_wind_var, v_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Uwnd', 'Vwnd'],
                                                                          level_isobaric=1000)

            # between lat/lon grid points
            dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)

            # Calculate temperature advection using metpy function
            adv = mpcalc.advection(temp_var * units.kelvin, u=u_wind_var * units('m/s'), v=v_wind_var * units('m/s'),
                                   dx=dx, dy=dy)
            return adv

        def adiabatic(time, level_isobaric=[1000, 850, 500, 200]):
            temp_var, w_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Wwnd'], level_isobaric=level_isobaric)

            pressure_var = np.ones([len(level_isobaric), len(lat_var), len(lon_var)])
            for i in range(len(level_isobaric)):
                # transform hPa into Pa
                pressure_var[i] = pressure_var[i] * level_isobaric[i] * 100
            pressure_var = pressure_var * units.Pa

            # Calculate temperature advection using metpy function
            adv = mpcalc.static_stability(pressure_var, temp_var * units.kelvin)

            Rd = units.Quantity(8.314462618, 'J / mol / K') / units.Quantity(28.96546e-3, 'kg / mol')
            adiabatic_warming = adv * (w_wind_var * units('Pa/s')) * pressure_var / Rd
            return adiabatic_warming

        s_advection = tem_advection_1000(time)
        s_adiabatic = adiabatic(time)

        s_year = int(time.strftime('%Y'))
        path_SaveData = r'/home/linhaozhong/work/AR_analysis/'
        os.makedirs(path_SaveData + 'adibatic_cli/%i' % s_year, exist_ok=True)
        os.makedirs(path_SaveData + 'advection_cli/%i' % s_year, exist_ok=True)
        s_adiabatic.to_netcdf(path_SaveData + 'adibatic_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))
        s_advection.to_netcdf(path_SaveData + 'advection_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))

    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q_withpath(s_time_path):
        now = xr.open_dataset(path_Qdata + '%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    if types == 'climate':
        cli = []
        for i_year in range(1979, 2020 + 1):
            # judge whether this date is valid
            try:
                datetime.datetime(i_year, int(s_time.strftime('%m')), int(s_time.strftime('%d')))
            except:
                print('%s is invalid' % (str(i_year) + s_time.strftime('%m%d%H')))
                continue

            s_time_str = var + '_cli/' + str(i_year) + '/' + str(i_year) + s_time.strftime('%m%d%H')
            if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
                a = True
            else:
                create_save_adv_adi(pd.to_datetime(str(i_year) + '-' + s_time.strftime('%m-%d %H:00:00')))
            cli.append(load_Q_withpath(s_time_str))
        cli = np.nanmean(np.array(cli), axis=0)
        now = load_Q_withpath(var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H'))
        return now - cli
    elif types == 'now':
        s_time_str = var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H')
        if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
            a = True
        else:
            create_save_adv_adi(s_time)
        now = load_Q_withpath(s_time_str)
        return now


def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north


def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='climate'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    if types == 'climate':
        data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                           time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values
    if types == 'climate':
        return data_present - data_anomaly
    elif types == 'now':
        return data_present

def load_ivt_total(s_time, types='climate'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]
    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    ivt_climate = np.sqrt(index_uivt_anomaly ** 2 + index_vivt_anomaly ** 2)
    if types == 'climate':
        return ivt_total - ivt_climate
    else:
        return [ivt_total, ivt_climate]


def load_ivt_u(s_time, types='climate'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    if types == 'climate':
        return ivt_east - index_uivt_anomaly
    else:
        return [ivt_east, index_uivt_anomaly]


def load_ivt_v(s_time, types='climate'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values

    if types == 'climate':
        return ivt_north - index_vivt_anomaly
    else:
        return [ivt_north, index_vivt_anomaly]


def load_esiv(s_time, contour):

    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')

        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]

        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []

        for i_contour in contours:

            i_contour = np.squeeze(i_contour)
            # if len(i_contour) < 5:
            #     continue
            # trans to row

            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_ai = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * np.array(pioms_ai, dtype='double') * 100
    ai_anomaly = nc4.read_nc4(path_PIOMS+'/anomaly_9yr/ai_%s'% s_time.strftime('%Y'))
    hi_anomaly =nc4.read_nc4(path_PIOMS+'/anomaly_9yr/hi_%s'% s_time.strftime('%Y'))
    ah_anomaly = ai_anomaly*hi_anomaly*100
    pioms_ah_ano = pioms_ah - ah_anomaly[loc]

    if plot_type == 'pdf':

        index = query_in(contour, tree)

        weighted_PIOMS_contour = weighted_PIOMS[index] / np.nansum(weighted_PIOMS[index])
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))

        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, ah_anomaly[loc]]


def load_heff(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)

            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * 100
    hi_anomaly =nc4.read_nc4(path_PIOMS+'/anomaly_9yr/hi_%s'% s_time.strftime('%Y'))
    pioms_ah_ano = pioms_ah - (hi_anomaly[loc] * 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, hi_anomaly[loc] * 100]


def load_sic(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)

            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * 100
    ai_anomaly = nc4.read_nc4(path_PIOMS+'/anomaly_9yr/ai_%s'% s_time.strftime('%Y'))
    pioms_ah_ano = pioms_ah - (ai_anomaly[loc] * 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, ai_anomaly[loc] * 100]



""" #### prepare for esiv, load pioms grid and trans into stree"""
import cv2
resolution = 0.5
long = np.arange(0, 360, 0.5)
lat = np.arange(90, -90.01, -0.5)
bu_addition_Name = ''
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
path_PIOMS = r'/home/linhaozhong/work/Data/PIOMS_nc/'

# load pioms grid
# ai_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/aiday_1979_2020')
# hi_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/hiday_1979_2020')
# ah_anomaly = ai_anomaly * hi_anomaly * 100
pioms_grid = xr.open_dataset(path_PIOMS + r"aiday.H1985.nc")
points_lon = pioms_grid.x.values
points_lat = pioms_grid.y.values

from function_shared_Main import cal_area_differentLL_1d

weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)

# transform into 0,1,2,3 relative coordination, only works in north hemisphere
# !!!!!!!!!!!!!!!!!!!!!!!#
points_lat_relaCoord = (90 - points_lat) / resolution
points_lon_relaCoord = points_lon / resolution

# to Stree
from shapely.strtree import STRtree
from shapely.geometry import Point
print(np.min(points_lon_relaCoord), np.min(points_lat_relaCoord))
print(np.max(points_lon_relaCoord), np.max(points_lat_relaCoord))

points = [Point(x, y) for x, y in zip(points_lon_relaCoord, points_lat_relaCoord)]
tree = STRtree(points)  # create a 'database' of points
"""prepare for esiv, load pioms grid and trans into stree #### """

# def main_linePhase_given_area(var, types='now'):
#     def contour_cal(time_file, var='IVT'):
#
#
#         info_AR_row = xr.open_dataset(path_contour + time_file[:-1])
#
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#
#         if bu_all_or_AR == 'AR':
#             all_or_AR = [time_AR, info_AR_row['contour_AR'].values]
#         else:
#             all_or_AR = [time_all, info_AR_row['contour_all'].values]
#
#         AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
#
#         def cc_call_date_now(s_time, types):
#             """
#             to avoid too many Duplication
#             :param s_time: pd.datetime
#             :param types: now, climate
#             :return:
#             """
#             if (var == 'advection') | (var == 'adibatic'):
#                 data = load_Q(
#                     s_time,
#                     var, types=types)
#             else:
#                 data = get_circulation_data_based_on_date(
#                     s_time,
#                     var_LongName[var],
#                     var, types=types)
#             return data
#
#
#
#         arg = []
#
#         if AR_series >= len(all_or_AR[0]) - 1:
#             return 'no returen'
#         '''
#         ####################
#         loop time
#         ####################
#         '''
#         time_loop = pd.date_range(pd.to_datetime(all_or_AR[0][0]) ,
#                                   pd.to_datetime(all_or_AR[0][-1]),
#                                   freq='6H')
#
#         date_era5_begin = pd.to_datetime('1979-01-02 00:00')
#         if time_loop[0] < date_era5_begin:
#             return 'no'
#
#         # provided climately
#         if types == 'now':
#             data_climately = cc_call_date_now(time_loop[0], types=types)
#
#         for s_I_AR, datetime_now in enumerate(time_loop):
#
#             # provided climately
#             if types == 'acceleration':
#                 data_climately = cc_call_date_now(datetime_now-np.timedelta64(6, 'h'), types='now')
#                 data_now = cc_call_date_now(datetime_now, types='now')
#             else:
#                 data_now = cc_call_date_now(datetime_now, types=types)
#
#             # mask latitude lesser than 70
#             contour = all_or_AR[1][AR_series]
#             # contour[int(30 / resolution), :] = 0
#
#             if types != 'climately':
#                 if var == 'siconc':
#                     # contour[s_i==0] = 0
#                     contour[data_climately==0] = 0
#                     if data_now[contour == 1].flatten().size == 0:
#                         return 'no ice'
#
#                 data_now = data_now-data_climately
#
#             # get percentile and mean of every time step
#             if s_I_AR == AR_loc:
#                 arg.append(-999)
#             arg.append(np.nanmean(data_now[contour == 1].flatten()))
#         return arg
#
#
#     save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
#     file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
#     for iI, i_type in enumerate(np.unique(save_labels_bmus)):
#         if i_type == 0:
#             continue
#         times_file_all = file_all[save_labels_bmus == i_type]
#
#
#         if types != 'climately':
#             types_save = types + str(delay_time)+'_%i'%AR_series
#         else:
#             types_save = types
#
#         fileNameSaveNow = path_MainData + 'PDF_line_new_Clustering/%s_%s_%s' % (types_save, bu_all_or_AR,
#                                                             var + 'DAY%s_' % time_step + '_' +
#                                                             str(i_type))
#         if os.path.exists(fileNameSaveNow+'.nc4'):
#             print('exist'+fileNameSaveNow)
#             continue
#
#         characteristics = []
#         max_length = 0
#         for time_file in times_file_all:
#             if len(str(time_file)) < 5:
#                 continue
#             s_ii = contour_cal(time_file, var)
#
#             if isinstance(s_ii, str):
#                 continue
#             characteristics.append(s_ii)
#             max_length = np.max([max_length, len(s_ii)])
#         if len(characteristics) == 0:
#             continue
#
#         # pad
#         characteristics = [np.pad(i, (int(max_length-len(i))), 'constant', constant_values=np.nan ) for i in characteristics]
#         characteristics = np.nanmean(characteristics, axis=0)
#         nc4.save_nc4(characteristics,
#                      fileNameSaveNow)

def main_linePhase_AR_centre(var, types='now'):
    def contour_cal(time_file, var='IVT'):
        info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        all_or_AR = [time_all, info_AR_row['contour_all'].values]
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))

        def cc_call_date_now(i_time, types):
            """
            to avoid too many Duplication
            :param s_time: pd.datetime
            :param types: now, climate
            :return:
            """

            if var == 'IVT':
                s_i = load_ivt_total(pd.to_datetime(i_time), types=types)
            elif var == 'IVT_u':
                s_i = load_ivt_u(pd.to_datetime(i_time), types=types)
            elif var == 'IVT_v':
                s_i = load_ivt_v(pd.to_datetime(i_time), types=types)
            elif var == 'tcwv':
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                         'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour', types=types)
            elif var == 'esiv':
                s_i = load_esiv(pd.to_datetime(i_time), contour)
            elif var == 'heff':
                s_i = load_heff(pd.to_datetime(i_time), contour)
            elif var == 'sic':
                s_i = load_sic(pd.to_datetime(i_time), contour)
            else:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                         var_LongName[var],
                                                         var, types=types)
            return s_i

        arg = []

        '''
        ####################
        loop time
        ####################
        '''
        time_loop_before = pd.date_range(pd.to_datetime(time_AR[0] ) -np.timedelta64(day_before, 'D'),
                                  pd.to_datetime(time_AR[0]),
                                  freq='6H')

        time_loop_after = pd.date_range(pd.to_datetime(time_AR[0]),
                                  pd.to_datetime(time_AR[0] ) +np.timedelta64(day_after, 'D'),
                                  freq='6H')

        date_era5_begin = pd.to_datetime('1979-01-02 00:00')
        if time_loop_before[0] < date_era5_begin:
            return 'no'


        for s_I_AR, datetime_now in enumerate(time_loop_before[::-1]):
            data_now = cc_call_date_now(datetime_now, types=types)

            if (AR_loc - s_I_AR) >= 0:
                contour = all_or_AR[1][AR_loc -s_I_AR]
                arg.append(np.nanmean(data_now[contour == 1].flatten()))
            # contour[int(30 / resolution), :] = 0
            else:
                arg.append(np.nan)
        arg = arg[::-1]

        for s_I_AR, datetime_now in enumerate(time_loop_after[1:]):
            data_now = cc_call_date_now(datetime_now, types=types)
            if (AR_loc + s_I_AR +1) < len(all_or_AR[1]):
                contour = all_or_AR[1][AR_loc +s_I_AR +1]
                arg.append(np.nanmean(data_now[contour == 1].flatten()))
            # contour[int(30 / resolution), :] = 0
            else:
                arg.append(np.nan)

        return arg


    save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
    file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
    for iI, i_type in enumerate(np.unique(save_labels_bmus)):
        if i_type == 0:
            continue
        times_file_all = file_all[save_labels_bmus == i_type]


        fileNameSaveNow = path_MainData + 'PDF_line_new_Clustering/%s_%s' % (types,
                                                            var + 'DAYb%s_a%s' % (day_before, day_after) + '_' +
                                                            str(i_type))
        # if os.path.exists(fileNameSaveNow+'.nc4'):
        #     print('exist'+fileNameSaveNow)
        #     continue

        characteristics = []

        for time_file in times_file_all:
            if len(str(time_file)) < 5:
                continue
            s_ii = contour_cal(time_file, var)

            if isinstance(s_ii, str):
                continue
            characteristics.append(s_ii)

        if len(characteristics) == 0:
            continue

        # pad
        characteristics = np.nanmean(characteristics, axis=0)
        nc4.save_nc4(characteristics,
                     fileNameSaveNow)


def main_linePhase_given_area(var, types='now'):
    def contour_cal(time_file, contour, var='IVT'):

        info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

        time_AR= info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        continue_not = True
        AR_loc_new = AR_loc-1
        for i_contour in info_AR_row['contour_all'].values[AR_loc:]:
            AR_loc_new += 1

            i_contour = i_contour+contour

            if np.max(i_contour) > 1.1:
                continue_not = False
                break
        if continue_not:
            return 'no'

        time_AR_new = time_all[AR_loc]

        def cc_call_date_now(i_time, types):
            """
            to avoid too many Duplication
            :param s_time: pd.datetime
            :param types: now, climate
            :return:
            """

            if var == 'IVT':
                s_i = load_ivt_total(pd.to_datetime(i_time), types=types)
            elif var == 'IVT_u':
                s_i = load_ivt_u(pd.to_datetime(i_time), types=types)
            elif var == 'IVT_v':
                s_i = load_ivt_v(pd.to_datetime(i_time), types=types)
            elif var == 'tcwv':
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                         'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour', types=types)
            elif var == 'esiv':
                s_i = load_esiv(pd.to_datetime(i_time), contour)
            elif var == 'heff':
                s_i = load_heff(pd.to_datetime(i_time), contour)
            elif var == 'sic':
                s_i = load_sic(pd.to_datetime(i_time), contour)
            else:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                         var_LongName[var],
                                                         var, types=types)
            return s_i

        arg = []

        '''
        ####################
        loop time
        ####################
        '''
        time_loop_before = pd.date_range(pd.to_datetime(time_AR_new) -np.timedelta64(day_before, 'D'),
                                  pd.to_datetime(time_AR_new),
                                  freq='6H')

        time_loop_after = pd.date_range(pd.to_datetime(time_AR_new),
                                  pd.to_datetime(time_AR_new) +np.timedelta64(day_after, 'D'),
                                  freq='6H')

        date_era5_begin = pd.to_datetime('1979-01-02 00:00')
        date_era5_end = pd.to_datetime('2021-01-01 00:00')
        if time_loop_before[0] < date_era5_begin:
            return 'no'


        for s_I_AR, datetime_now in enumerate(time_loop_before[::-1]):
            if datetime_now >= date_era5_end:
                arg.append(np.nan)
                continue
            data_now = cc_call_date_now(datetime_now, types=types)
            print(data_now)
            if (var != 'esiv') & (var != 'heff')& (var != 'sic'):
                arg.append(area_weighted_mean(data_now[contour == 1].flatten(), weight[contour == 1].flatten()))
            # contour[int(30 / resolution), :] = 0
            else:
                arg.append(np.nansum(data_now))
        arg = arg[::-1]

        for s_I_AR, datetime_now in enumerate(time_loop_after[1:]):
            if datetime_now >= date_era5_end:
                arg.append(np.nan)
                continue
            data_now = cc_call_date_now(datetime_now, types=types)

            if (var != 'esiv') & (var != 'heff')& (var != 'sic'):
                arg.append(area_weighted_mean(data_now[contour == 1].flatten(), weight[contour == 1].flatten()))
            # contour[int(30 / resolution), :] = 0
            else:
                arg.append(np.nansum(data_now))


        return arg


    save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
    file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
    for iI, i_type in enumerate(np.unique(save_labels_bmus)):
        if (i_type != 1) & (i_type != 6):
            continue
        times_file_all = file_all[save_labels_bmus == i_type]


        fileNameSaveNow = path_MainData + 'PDF_line_new_Clustering/ARbegin%s_%s_forzenRegion' % \
        (types, var + 'DAYb%s_a%s' % (day_before, day_after) + '_'  +
        str(i_type))
        # if os.path.exists(fileNameSaveNow+'.nc4'):
        #     print('exist'+fileNameSaveNow)
        #     continue

        characteristics = []

        contour = np.zeros([361, 720])
        if i_type == 1:
            contour[((lat < 82) & (lat > 70) & (lon > 10) & (lon < 85))] = 1
        elif i_type == 6:
            contour[((lat < 75) & (lat > 60) & (lon > 165) & (lon < 215))] = 1
        else:
            continue
            # contour2 = np.zeros([361, 720])
            # contour2[((lat < 78) & (lat > 72) & (lon > 200) & (lon < 223))] = 1

        for time_file in times_file_all:
            if len(str(time_file)) < 5:
                continue

            s_ii = contour_cal(time_file, contour, var)

            if isinstance(s_ii, str):
                continue
            characteristics.append(s_ii)

        if len(characteristics) == 0:
            continue

        # pad
        characteristics = np.nanmean(characteristics, axis=0)
        nc4.save_nc4(characteristics,
                     fileNameSaveNow)


def cal_area(lon, lat):
    import math
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S = np.zeros(lon.shape)
    Dx = np.zeros(lon.shape)
    Dy = np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S


weight = cal_area(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5))


def area_weighted_mean(data, area_weight):
    area_weight = area_weight / np.sum(area_weight)
    avg_AR = np.nansum(data * area_weight)
    return avg_AR


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''

resolution = 0.5

var_LongName = {'ssr': 'surface_net_solar_radiation',
                'ssrd': 'surface_solar_radiation_downwards',
                'tsr':'top_net_solar_radiation',

                'str': 'surface_net_thermal_radiation',
                'strd': 'surface_thermal_radiation_downwards',

                'cbh': 'cloud_base_height',
                'hcc': 'high_cloud_cover',
                'lcc': 'low_cloud_cover',
                'mcc': 'medium_cloud_cover',
                'tcc': 'total_cloud_cover',

                'siconc': 'Sea_Ice_Cover',
                't2m': '2m_temperature',

                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',

                'tciw': 'total_column_cloud_ice_water',
                'tclw': 'total_column_cloud_liquid_water',

                }

plot_type = 'pdf' # for pioms and noaa ice motion. but not work in t2m.py now
types = 'climate'  # 'now'/'climate', only this contour_cal only works with now.
day_before = 10
day_after = 20
lat = np.arange(90, -90.01, -0.5)
lon = np.arange(0, 360, 0.5)
lon, lat = np.meshgrid(lon, lat)

main_linePhase_given_area('t2m', types)

# for i_var in ['ssr', 'ssrd', 'str', 'lcc', 'mcc', 'hcc', 'tcc', 'cbh']:
for i_var in ['esiv', 'heff', 'sic']:
    # main_linePhase_AR_centre(i_var, types)
    main_linePhase_given_area(i_var, types)


exit(0)

"""
=====================================
for anomaly compared with two days before, and line plot
=====================================
"""
path_SecondData = r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering/'

path_singleData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
                 'MS', 'GE', 'useless2']
type_name_num = ['0', '2', '3', '4', '5', '6']

type_name = ['greenland_east', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE']
colors = ['#000000', '#7FADB2', '#C2AE72', '#e7837a', 'tab:brown', 'tab:gray']
plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
             'MED', 'ARC']
infig_fontsize = 5

resolution = 0.5

delay_time = 5
time_step = 5

select_percentile = 0
AR_series = 5
savename = '%i' % AR_series
types = 'acceleration'
types = 'now'
day_before = 3

var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
                'hcc': 'high_cloud_cover',
                'lcc': 'low_cloud_cover',
                'mcc': 'medium_cloud_cover',
                'tcc': 'total_cloud_cover',
                'tciw': 'total_column_cloud_ice_water',
                'tclw': 'total_column_cloud_liquid_water',

                }
plot_param = {'cbh': [['T2Mmean=', ' K'], 'CBH', [0, 120], 1],
              'hcc': [['SSHFmean=', ' W/$m^{2}$'], 'HCC', [0, 120], 1],
              'lcc': [['SLHFmea=', ' W/$m^{2}$'], 'LCC', [0, 120], 1],
              'mcc': [['ADVmean=', ' K/6h'], 'MCC', [0, 120], 1],
              'tcc': [['ABmean=', ' K/6h'], 'TCC', [0, 120], 1],
              'tciw': [['IVTmean=', ' kg/(m*s)'], 'TCIW', [0, 120], 1],
              'tclw': [['TCWVmean=', ' kg/$m^{2}$'], 'TCLW', [0, 120], 1, ],

              't2m': [['T2Mmean=', ' K'], 'T2M', [0, 120], 1],
              'sshf': [['SSHFmean=', ' W/$m^{2}$'], 'SSHF', [0, 120], 1 / (6 * 3600)],
              'slhf': [['SLHFmea=', ' W/$m^{2}$'], 'SLHF', [0, 120], 1 / (6 * 3600)],
              'advection': [['ADVmean=', ' K/6h'], 'advection', [0, 120], 6 * 3600],
              'adibatic': [['ABmean=', ' K/6h'], 'adiabatic', [0, 120], 6 * 3600],
              'IVT': [['IVTmean=', ' kg/(m*s)'], 'IVT', [0, 120], 1],
              'tcwv': [['TCWVmean=', ' kg/$m^{2}$'], 'TCWV', [0, 120], 1, ],
              'ssr': [['TCWVmean=', ' W/$m^{2}$'], 'TCWVano', [0, 120], 1 / (6 * 3600)],
              'str': [['STRmean=', ' W/$m^{2}$'], 'STR', [0, 120], 1 / (6 * 3600)],
              'siconc': [['SICmean=', ' %'], 'SIC', [0, 120], 100],
              }
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
plt.style.use('bmh')
SMALL_SIZE = 7

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

var = 'hcc'
aar = nc4.read_nc4(
    r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\now_%sDAYb10_a20_1"%var)
par = nc4.read_nc4(
    r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\now_%sDAYb10_a20_6"%var)
plt.plot(np.arange(40), aar, label='aar')
plt.plot(np.arange(40), par, label='par')
plt.legend()
plt.show()
exit()

frequency = []
for var in vars:

    def get_data():
        data_mean = []

        for s_type_SOM in type_name:
            data_mean.append(
                nc4.read_nc4(path_SecondData + '%s_all_%s' % (types,
                                                              var + 'DAY%s_' % day_before + '_' +
                                                              str(s_type_SOM)) *
                             plot_param[var][-1]))

        return np.array(data_mean)


    frequency.append(get_data())


def monte_carlo_in_PDF(data1, data2, test_range=20):
    import random
    # divided into space range and test
    if isinstance(test_range, int) | isinstance(test_range, float):
        test_range = np.linspace(np.nanmin(np.append(data1, data2)), np.nanmax(np.append(data1, data2)), test_range)
    print(test_range)

    def monte_carlo(data1, data2):
        data = np.append(data1, data2)
        div = []
        for i in range(1000):
            s_data1 = random.sample(set(data), len(data1))
            s_data2 = np.setdiff1d(data, s_data1)
            div.append(np.nanmean(s_data1) - np.nanmean(s_data2))

        return div, np.nanmean(data1) - np.nanmean(data2)

    for i in range(len(test_range))[:-1]:
        print(test_range[i], test_range[i + 1])
        div_all, div = monte_carlo(data1[(data1 >= test_range[i]) & (data1 < test_range[i + 1])],
                                   data2[(data2 >= test_range[i]) & (data2 < test_range[i + 1])])
        p = np.percentile(div_all, [99, 95, 90])

        print(p, div)


def plots(ax, data, color='black', mean_label=['', '']):
    # def pdf_kernel(X, x_range=''):
    #     from sklearn.neighbors import KernelDensity
    #     if x_range == '':
    #         x_range = [np.nanmin(X), np.nanmax(X)]
    #     X_plot = np.linspace(x_range[0], x_range[1], 100)[:, np.newaxis]
    #     kde = KernelDensity(kernel="gaussian", bandwidth=0.8).fit(X.reshape(-1, 1))
    #     log_dens = np.exp(kde.score_samples(X_plot))
    #     return [X_plot, log_dens]
    #
    # percentile_label = ['Q1', 'Q2', 'Q3']
    # percentile = np.percentile(data, [25, 50, 75])
    # mean = np.nanmean(data)
    # HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
    # # ax.boxplot(data)
    #
    # # _, _, bar_container = ax.hist(data,bins=HIST_BINS,density=True, lw=1,
    # #                           ec="black", fc="gray",zorder=2)
    #
    # data = data[~np.isnan(data)]
    # pdf_other_axes_1 = pdf_kernel(data)
    ax.plot(np.arange(data.shape[0]), data, color=color, linewidth=1, label=mean_label)

    # yticks = ax.get_yticks()
    # ax.set_yticks(yticks)
    # ax.set_yticklabels((yticks*len(data)).astype('int'))

    # ylim_max = ax.get_ylim()[1]
    # trans = ax.get_xaxis_transform()
    # for i_percentile in range(3):
    #     ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color=color,zorder=1)
    #     ax.text(percentile[i_percentile], 0.89, percentile_label[i_percentile],
    #             transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
    #             bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
    #     )
    #     ax.text(percentile[i_percentile], 1.01, '%0.1f'%percentile[i_percentile], transform=trans,
    #             horizontalalignment='center', fontsize=infig_fontsize)
    #
    # ax.axvline(mean, linestyle='-', linewidth=0.5, color=color, zorder=1)
    # ax.text(0.85, 0.89, mean_label[0]+'\n%0.2f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)

    return [ax.get_ylim()[1], ax.get_xlim()[1]]


# print(monte_carlo_in_PDF(frequency[0,1], frequency[3,1], np.array([0,10,20])))
# exit(0)


fig, axs = plt.subplots(6, 1)
plt.subplots_adjust(top=0.975,
                    bottom=0.07,
                    left=0.095,
                    right=0.98,
                    hspace=0.4,
                    wspace=0.2)

for i in range(6):

    ax = axs[i]

    for iI_region, i_region in enumerate(type_name):
        '''选取数值'''
        data = frequency[i][iI_region][:, select_percentile]
        plots(ax, data, color=colors[iI_region], mean_label=plot_name[iI_region])

    # ax.annotate(figlabelstr[3 * i + j], [0.01, 0.85], xycoords='axes fraction')

    axs[i].set_ylabel(plot_param[vars[i]][1] + '\n' + plot_param[vars[i]][0][1])
    ax.axvline(delay_time * 4)
    ax.set_xticks(np.arange(0, data.shape[0] + 1, 4))

axs[-1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
               fancybox=True, ncol=6)
# axs[:][0].set_ylabel(plot_param[var][1]+plot_param[var][0][1])

plt.show()
# save_path = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\pic\\'
plt.title(savename)
plt.savefig('%s.png' % savename, dpi=400)
plt.close()

"""
=====================================
for anomaly compared with climately, and separated into 3 phase
=====================================
"""
# path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\\'
#
# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
# time_Seaon_divide = [[12, 1, 2]]
# time_Seaon_divide_Name = ['DJF']
# type_name_row = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
#                  'MS', 'GE', 'useless2']
# type_name_num = ['0', '2', '3', '4', '5', '6']
#
# type_name = ['greenland_east', 'greenland_west', 'pacific_east', 'pacific_west',
#              'MS', 'GE']
# colors = ['#000000', '#7FADB2', '#C2AE72', '#e7837a', '#6893C2', '#E3ECC4']
# plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
#              'MED', 'ARC']
# infig_fontsize = 5
#
# resolution = 0.5
#
# plot_param = {'t2m': [['T2Mmean=', ' K'], 'T2M', [-5, 30], 1],
#               'sshf': [['SSHFmean=', ' W/$m^{2}$'], 'SSHF', [-10, 25], 1 / (6 * 3600)],
#               'slhf': [['SLHFmea=', ' W/$m^{2}$'], 'SLHF', [-10, 25], 1 / (6 * 3600)],
#               'advection': [['ADVmean=', ' K/6h'], 'advection', [-5, 10], 6 * 3600],
#               'adibatic': [['ABmean=', ' K/6h'], 'adiabatic', [-5, 5], 6 * 3600],
#               'IVT': [['IVTmean=', ' kg/(m*s)'], 'IVT', [0, 700], 1],
#               'tcwv': [['TCWVmean=', ' kg/$m^{2}$'], 'TCWV', [0, 20], 1, ],
#               'ssr': [['TCWVmean=', ' W/$m^{2}$'], 'TCWVano', [0, 20], 1 / (6 * 3600)],
#               'str': [['STRmean=', ' W/$m^{2}$'], 'STR', [-5, 15], 1 / (6 * 3600)],
#               'sic': [['SICmean=', ' %'], 'SIC', [-10, 10], 100]
#               }
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
# plt.style.use('bmh')
# SMALL_SIZE = 7
#
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
# plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
# plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
#
# phase = ['0', '1', '2']
# phase_name = ['1', '2', '3']
# # vars = ['sshf','slhf']
# vars = ['t2m', 'advection', 'sshf', 'slhf', 'str', 'sic']
#
# frequency = []
# for var in vars:
#
#     def get_data():
#         data_mean = []
#         for i_phase in phase:
#
#             s_data_mean = []
#             # for var in ['IVT', 'tcwv']:
#             #     s_data_mean.append(nc4.read_nc4(path_SecondData + '%s0_%s_%s' % (var, season, type_SOM)))
#
#             # for var in ['advection','adibatic']:
#             #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))*6*3600)
#             # for var in ['sshf','slhf']:
#             #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))/100000)
#             for s_type_SOM in type_name:
#                 s_data_mean.append(
#                     nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', s_type_SOM)) *
#                     plot_param[var][-1])
#
#             data_mean.append(s_data_mean)
#
#         return np.array(data_mean)
#
#
#     frequency.append(get_data())
#
#
# def monte_carlo_in_PDF(data1, data2, test_range=20):
#     import random
#     # divided into space range and test
#     if isinstance(test_range, int) | isinstance(test_range, float):
#         test_range = np.linspace(np.nanmin(np.append(data1, data2)), np.nanmax(np.append(data1, data2)), test_range)
#     print(test_range)
#
#     def monte_carlo(data1, data2):
#         data = np.append(data1, data2)
#         div = []
#         for i in range(1000):
#             s_data1 = random.sample(set(data), len(data1))
#             s_data2 = np.setdiff1d(data, s_data1)
#             div.append(np.nanmean(s_data1) - np.nanmean(s_data2))
#
#         return div, np.nanmean(data1) - np.nanmean(data2)
#
#     for i in range(len(test_range))[:-1]:
#         print(test_range[i], test_range[i + 1])
#         div_all, div = monte_carlo(data1[(data1 >= test_range[i]) & (data1 < test_range[i + 1])],
#                                    data2[(data2 >= test_range[i]) & (data2 < test_range[i + 1])])
#         p = np.percentile(div_all, [99, 95, 90])
#
#         print(p, div)
#
#
# def plots(ax, data, color='black', mean_label=['', '']):
#     def pdf_kernel(X, x_range=''):
#         from sklearn.neighbors import KernelDensity
#         if x_range == '':
#             x_range = [np.nanmin(X), np.nanmax(X)]
#         X_plot = np.linspace(x_range[0], x_range[1], 100)[:, np.newaxis]
#         kde = KernelDensity(kernel="gaussian", bandwidth=0.8).fit(X.reshape(-1, 1))
#         log_dens = np.exp(kde.score_samples(X_plot))
#         return [X_plot, log_dens]
#
#     percentile_label = ['Q1', 'Q2', 'Q3']
#     percentile = np.percentile(data, [25, 50, 75])
#     mean = np.nanmean(data)
#     HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
#     # ax.boxplot(data)
#
#     # _, _, bar_container = ax.hist(data,bins=HIST_BINS,density=True, lw=1,
#     #                           ec="black", fc="gray",zorder=2)
#
#     data = data[~np.isnan(data)]
#     pdf_other_axes_1 = pdf_kernel(data)
#     ax.plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color=color, linewidth=1, label=mean_label)
#
#     # yticks = ax.get_yticks()
#     # ax.set_yticks(yticks)
#     # ax.set_yticklabels((yticks*len(data)).astype('int'))
#
#     # ylim_max = ax.get_ylim()[1]
#     # trans = ax.get_xaxis_transform()
#     # for i_percentile in range(3):
#     #     ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color=color,zorder=1)
#     #     ax.text(percentile[i_percentile], 0.89, percentile_label[i_percentile],
#     #             transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
#     #             bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
#     #     )
#     #     ax.text(percentile[i_percentile], 1.01, '%0.1f'%percentile[i_percentile], transform=trans,
#     #             horizontalalignment='center', fontsize=infig_fontsize)
#     #
#     # ax.axvline(mean, linestyle='-', linewidth=0.5, color=color, zorder=1)
#     # ax.text(0.85, 0.89, mean_label[0]+'\n%0.2f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)
#
#     return [ax.get_ylim()[1], ax.get_xlim()[1]]
#
#
# # print(monte_carlo_in_PDF(frequency[0,1], frequency[3,1], np.array([0,10,20])))
# # exit(0)
#
#
# fig, axs = plt.subplots(6, 3, figsize=[5.5, 6])
# plt.subplots_adjust(top=0.975,
#                     bottom=0.07,
#                     left=0.095,
#                     right=0.98,
#                     hspace=0.4,
#                     wspace=0.2)
#
# for i in range(6):
#     for j in range(3):
#
#         ax = axs[i][j]
#
#         for iI_region, i_region in enumerate(type_name):
#             data = frequency[i][j][iI_region]
#             plots(ax, data, color=colors[iI_region], mean_label=plot_name[iI_region])
#         ax.set_xlim(plot_param[vars[i]][-2])
#         ax.annotate(figlabelstr[3 * i + j], [0.01, 0.85], xycoords='axes fraction')
#     axs[i][0].set_ylabel(plot_param[vars[i]][1] + '\n' + plot_param[vars[i]][0][1])
#
#     # axs[i].axvline(0)
#
# # for i in range(3):
# #     # lim = np.zeros([len(vars), 1])
# #
# #     for i_region in range(len(type_name_compare)):
# #         data = frequency[i_region][i]
# #         ax = axs[i]
# #         plots(ax, data, color=colors[i_region], mean_label=type_name_compare[i_region])
# #         # plt.show()
# #     axs[i].set_xlim(plot_param[var][-2])
# #     axs[i].set_title('phase %s'%phase[i])
# #     axs[i].axvline(0)
#
#
# # ax.set_xlabel(plot_param[vars[i]][1])
#
# # maxy = np.max(lim[:,0])
# # maxx = np.max(lim[:, 1])
# # axs[i][0].set_ylim(top=maxy)
# # axs[i][1].set_ylim(top=maxy)
# # axs[i][0].set_xlim(plot_param[vars[i]][2])
# # axs[i][1].set_xlim(plot_param[vars[i]][2])
#
# # axs[i][0].set_xlim([-20, 20])
# # axs[i][1].set_xlim([-20,20])
#
# # if i != 0:
# #     axs[i][0].set_xlim([0,20])
# #     axs[i][1].set_xlim([0,20])
# # else:
# #     axs[0][0].set_xlim([0,700])
# #     axs[0][1].set_xlim([0,700])
#
# # pdf_other_axes_1 = pdf(frequency[0][i])
# # pdf_other_axes_2 = pdf(frequency[1][i])
# # axs[i][1].plot(pdf_other_axes_2[0], pdf_other_axes_2[1], color='red',linewidth=1)
# # axs[i][1].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='red',linestyle='--',linewidth=1.5)
# # axs[i][0].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='red',linewidth=1)
# axs[-1][1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
#                   fancybox=True, ncol=6)
# # axs[:][0].set_ylabel(plot_param[var][1]+plot_param[var][0][1])
# for j in range(3):
#     axs[0][j].set_title('phase %s' % (phase_name[j]))
# # plt.show()
# save_path = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\pic\\'
# plt.savefig('figure5.png', dpi=400)
# plt.close()
