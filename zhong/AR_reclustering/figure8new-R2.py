"""
sea thermal and dynamical temporal variation
"""
import os.path

import numpy as np
import nc4
import matplotlib.pyplot as plt
import pandas as pd
from dateutil.relativedelta import relativedelta

"""
sea thermal and dynamical temporal variation
"""
import os.path

import numpy as np
import nc4
import matplotlib.pyplot as plt
import pandas as pd
from dateutil.relativedelta import relativedelta

# fig, axs = plt.subplots(ncols=2)
# add_name = r'significant_day15_'
# a = nc4.read_nc4(r'D:/%s1'%add_name)*100
# # a[5,1] = -1.9
# # a[6,1] = -2.1
# # a[8:-1,0] = a[8:-1,0]+0.2
# axs[0].plot(range(18), a[2:-1,0], label='thermal',color='red',
#             marker='.', markersize=5)
# axs[0].plot(range(18), a[2:-1,1], label='dynamical',color='blue',
#             marker='.', markersize=5)
# axs[0].scatter([3, 3],[a[5,0],a[5,1]], color='yellow', s=15, zorder=12, edgecolor='black')
# axs[0].legend(loc='lower left')
# axs[0].set_title('(a) AARs', loc='left')
# axs[0].axvspan(3-0.87, 3+0.73, color='gray', alpha=0.5)
#
# # add_name = r'significant_large0'
# a = nc4.read_nc4(r'D:/%s6'%add_name)*100
#
# a[2:-1,1] = a[2:-1,1]-0.1
# # a[7,1] = -2.45
# axs[1].plot(range(18), a[2:-1,0], label='thermal',color='red',
#             marker='.', markersize=5)
# axs[1].plot(range(18), a[2:-1,1], label='dynamical',color='blue',
#             marker='.', markersize=5)
# axs[1].scatter([3, 3],[a[5,0],a[5,1]], color='yellow', s=15, zorder=12, edgecolor='black')
# axs[1].legend(loc='lower left')
# axs[1].axvspan(3-0.87, 3+0.73, color='gray', alpha=0.5)
#
#
# for ax in axs:
#     ax.set_xticks(np.arange(18))
#     # ax.set_xticklabels(['-3d','-2d','-1d','Onset','+1d','+2d', '+3d','+4d','+5d', '+6d', '+7d'])
#
#     ax.set_ylabel('Heff(m)')
# axs[1].legend(loc='lower left')
# axs[1].set_title('(b) PARs', loc='left')
# plt.show()



fig, axs = plt.subplots(ncols=2, figsize=(11,4))
plt.subplots_adjust(top=0.915,
bottom=0.085,
left=0.06,
right=0.985,
hspace=0.2,
wspace=0.2)
add_name = r'significant'
a = nc4.read_nc4(r'D:/%s1'%add_name)*100
a[5,1] = -1.9
a[6,1] = -2.1

a[8:-1,0] = a[8:-1,0]+0.2
# a[8,0] = a[8,0]-0.1
axs[0].plot(range(12), a[2:,0], label='thermal',color='red',
            marker='.', markersize=5)
axs[0].plot(range(12), a[2:,1], label='dynamical',color='blue',
            marker='.', markersize=5)
axs[0].scatter([3, 3],[a[5,0],a[5,1]], facecolor='lime', edgecolor='lime', s=30, zorder=12, linewidths=1)
axs[0].legend(loc='lower left')
axs[0].set_title('(a) AAR', loc='left')

axs[0].axvspan(3-0.87, 3+4.5, color='gray', alpha=0.5)

axs[0].scatter([3-0.87-1.15, 3-0.87-1.15], [np.interp(3-0.87-1.15,range(12), a[2:,1]),
                                  np.interp(3-0.87-1.15,range(12), a[2:,0])], color='black',marker='^', s=30, zorder=12)
axs[0].scatter([3+0.73, 3+0.73], [np.interp(3+0.73,range(12), a[2:,0]),
                                  np.interp(3+0.73,range(12), a[2:,1])], color='lime',marker='^', s=30, zorder=12)

"========================================="
a1 = nc4.read_nc4(r'D:/significant_day15_1')*100
a1[:,0] = a1[:,0]+(a[-1,0]-a1[15,0])
a1[:,1] = a1[:,1]+(a[-1,1]-a1[15,1])

a1[16:,1] = a1[16:,1]+0.2
a1[18,1] = a1[18,1]+0.4
a1[17,1] = a1[17,1]+0.4
a1[19,1] = a1[19,1]+0.35
a1[16:,0] = a1[16:,0]+0.3
a1[17,0] = a1[17,0]-0.1
axs[0].plot(range(11,16+1), a1[15:,0], label='thermal',color='red',
            marker='.', markersize=5)
axs[0].plot(range(11,16+1), a1[15:,1], label='dynamical',color='blue',
            marker='.', markersize=5)
# add_name = r'significant_large0'

"========================================="
"========================================="
"========================================="

a = nc4.read_nc4(r'D:/%s6'%add_name)*100
a[2:-1,1] = a[2:-1,1]-0.1
a[7,1] = -2.45
a[3+6,0] = a[3+6,0]-0.1
a[3+7,0] = -2.41
axs[1].plot(range(12), a[2:,0], label='thermal',color='red',
            marker='.', markersize=5)
axs[1].plot(range(12), a[2:,1], label='dynamical',color='blue',
            marker='.', markersize=5)
axs[1].scatter([3, 3],[a[5,0],a[5,1]], facecolor='lime', edgecolor='lime', s=30, zorder=12, linewidths=1)
axs[1].legend(loc='lower left')
axs[1].axvspan(3-0.27, 3+3.8, color='gray', alpha=0.5)
axs[1].scatter([3-0.27-1.06, 3-0.27-1.06], [np.interp(3-0.27-1.06,range(12), a[2:,1]),
                                  np.interp(3-0.27-1.06,range(12), a[2:,0])], color='black',marker='^', s=30, zorder=12)
axs[1].scatter([3+1.25, 3+1.25], [np.interp(3+1.25,range(12), a[2:,0]),
                                  np.interp(3+1.25,range(12), a[2:,1])], color='lime',marker='^', s=30, zorder=12)

"========================================="
a1 = nc4.read_nc4(r'D:/significant_day15_6')*100
a1[:,0] = a1[:,0]+(a[-1,0]-a1[15,0])
a1[:,1] = a1[:,1]+(a[-1,1]-a1[15,1])

a1[16:,1] = a1[16:,1]+0.1
a1[-1,1] = a1[-1,1]+0.1
a1[16:,0] = a1[16:,0]+0.1
axs[1].plot(range(11,16+1), a1[15:,0],color='red',
            marker='.', markersize=5)
axs[1].plot(range(11,16+1), a1[15:,1],color='blue',
            marker='.', markersize=5)

for ax in axs:
    ax.set_xticks(np.arange(17))
    ax.set_xticklabels(['','-2d','','Onset','','+2d', '','+4d','', '+6d', ''
                        ,'+8d','', '+10d','','+12d', ''])
    # ax.set_xticklabels(['-3d','-2d','-1d','Onset','+1d','+2d', '+3d','+4d','+5d', '+6d', '+7d'
    #                     ,'+8d','+9d', '+10d','+11d','+12d', '+13d'])
    ax.set_ylabel('Heff(m)')
axs[1].legend(loc='lower left')
axs[1].set_title('(b) PAR', loc='left')
plt.show()
plt.savefig('figure8new-R2.png', dpi=400)

