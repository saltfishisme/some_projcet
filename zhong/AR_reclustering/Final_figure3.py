import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

pioms_grid = xr.open_dataset(r"D:/aiday.H1979.nc")
points_lon = pioms_grid.x.values
points_lat = pioms_grid.y.values
from function_shared_Main import cal_area_differentLL_1d

weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)


heff = []
for i_type in range(1,6+1):
    # sih = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, 'heff'))
    #
    # sic = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, 'sic'))
    # # sih[sih>0] = np.nan
    # # sic[sic>0] = np.nan
    # ar = sih
    #
    # # ar[points_lat<66] = np.nan
    # ar[abs(ar) < 0.1] = np.nan
    # # ar[ar> -0.0001] = np.nan
    # print(len(ar[~np.isnan(ar)]))
    # s_weighted_PIOMS = weighted_PIOMS.copy()
    # s_weighted_PIOMS[np.isnan(ar)] = np.nan
    # print(np.nansum(s_weighted_PIOMS))

    # s_weighted_PIOMS = s_weighted_PIOMS / np.nansum(s_weighted_PIOMS)
    # ar = np.nansum(ar*s_weighted_PIOMS)
    sih = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pdf' % (i_type, 'esiv'))
    ar = sih
    heff.append(np.nanmean(ar))
print(heff)
import matplotlib
# matplotlib.style.use('ggplot')
df = pd.DataFrame(index=['SOM1', 'SOM2','SOM3', 'SOM4', 'SOM5', 'SOM6'])
df['$\mathrm{H_{eff}}$'] = heff
df.plot(kind='bar', rot=0,color='black')
ax = plt.gca()
ax.set_ylabel('cm')
# plt.show()
plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/figure3.png', dpi=400)
exit()