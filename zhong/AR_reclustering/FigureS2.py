import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import numpy as np
import scipy.io as scio
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

import numpy as np

# a = nc4.read_nc4(r'D:/1')
# print(a.shape)
# plt.plot(range(14), a[:,0], label='thermal')
# plt.plot(range(14), a[:,1], label='dynamical')
# plt.legend()
# plt.show()
import scipy.interpolate as interp
from matplotlib import patches
# a = xr.open_dataset(r'D:/Wwnd.20181231.nc')
# levels = a.level.values
# levels = dict(zip(levels, np.arange(len(levels))))
# print(levels)
#
# levels = [14,21,30,36]
# exit()


import matplotlib.pyplot as plt
import matplotlib
# matplotlib.rcparams['contour.negative linestyle'] = 'solid'
import cartopy.crs as ccrs
fig, axs = plt.subplots(ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()}
                       , figsize=[8, 4])
plt.subplots_adjust(top=0.84,
bottom=0.115,
left=0.07,
right=0.99,
hspace=0.2,
wspace=0.2)

ax = axs[0]
ranges = [10,85,65,85]

lon1 = np.linspace(ranges[0], ranges[1], 100)
lat1 = np.full_like(lon1, ranges[2])
lon2 = np.linspace(ranges[1], ranges[1], 100)
lat2 = np.linspace(ranges[2], ranges[3], 100)
lon3 = np.linspace(ranges[1], ranges[0], 100)
lat3 = np.full_like(lon3, ranges[3])
lon4 = np.linspace(ranges[0], ranges[0], 100)
lat4 = np.linspace(ranges[3], ranges[2], 100)

# 合并所有点
lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
ax.plot(lons,
        lats,
        color='red', transform=ccrs.PlateCarree(), linewidth=1)
lat = np.arange(90, -90.01, -0.5)
lon = np.arange(0, 360, 0.5)
lon, lat = np.meshgrid(lon, lat)
# contour = np.zeros([361, 720])
#
# contour[((lat < 82) & (lat > 70) & (lon > 10) & (lon < 85))] = 1
# ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5), contour, transform=ccrs.PlateCarree())
ax.coastlines(zorder=4,linewidth=0.1)
# ax.gridlines(draw_labels=True)
# ax.set_global()
ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
ax.set_title('(a) AAR')
data = nc4.read_nc4(r'D:/centroidOfAR_1')
data = data[:,:2]
ax.scatter(data[:,1], data[:,0], c='red', s =2,edgecolor='black', transform=ccrs.PlateCarree(), zorder=100)

ax = axs[1]

# ranges = [165,215,60,75]
ranges = [150,220,55,85]
lon1 = np.linspace(ranges[0], ranges[1], 100)
lat1 = np.full_like(lon1, ranges[2])
lon2 = np.linspace(ranges[1], ranges[1], 100)
lat2 = np.linspace(ranges[2], ranges[3], 100)
lon3 = np.linspace(ranges[1], ranges[0], 100)
lat3 = np.full_like(lon3, ranges[3])
lon4 = np.linspace(ranges[0], ranges[0], 100)
lat4 = np.linspace(ranges[3], ranges[2], 100)

# 合并所有点
lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
ax.plot(lons,
        lats,
        color='red', transform=ccrs.PlateCarree(), linewidth=1)

ax.coastlines(zorder=4,linewidth=0.1)
# ax.gridlines(draw_labels=True)
# ax.set_global()
ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
ax.set_title('(b) PAR')

data = nc4.read_nc4(r'D:/centroidOfAR_6')
data = data[:,:2]
ax.scatter(data[:,1], data[:,0], c='red', s =2,edgecolor='black',
           transform=ccrs.PlateCarree(), zorder=100)

# plt.show()
def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1, colors_shape[0]])
    return ListedColormap(np.concatenate((colors, ones.T), axis=1))
def plot_trajectory(ax, pos_int):


    ################################# prepare data ################################

    from sklearn.preprocessing import MinMaxScaler
    import numpy as np

    data = np.arange(pos_int.shape[-1])  # 给定数据
    scaler = MinMaxScaler()
    scaled_data = scaler.fit_transform(data.reshape(-1, 1))

    rr_int = scaled_data.flatten()

    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/rainbow200.mat', 'rainbow200')

    def plot_colored(ax, x, y, data, cmap, steps=1):
        '''
        :param ax: axes
        :param x: (N,) np.array
        :param y: (N,) np.array
        :param data: (N,) np.array
        :param cmap:
        :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
        :return:
        '''
        ''' from matplotlib import cm
        cmpa should be    cm.get_cmap(cmpas_zhong, 100)

         $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
         before plot all data, please normalized all this data.'''
        data = np.asarray(data)
        it = 0
        while it < (data.size - steps):
            x_segm = x[it:it + steps + 1]
            y_segm = y[it:it + steps + 1]

            c_segm = cmap(data[it + steps // 2])
            ax.scatter(x_segm, y_segm, s=0.4, linewidths=0.4,c=c_segm, transform=ccrs.Geodetic(),
                       alpha=0.4,marker='.')
            it += steps

    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len + 1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :] = np.arange(0, 101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb

    ####### plot colorbar in given cmap  ##########
    # cb_ax = fig.add_axes([0.1, 0.02, 0.8, 0.01])
    # from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    # ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    # cb_ax.set_axes_locator(ip)
    # cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
    # cbar.ax.annotate('%', xy=(0.45, -7.5), xycoords='axes fraction')
    # cbar.set_ticks(np.arange(0, 101, 25))
    ####### plot trajectories by colored lines   ##########
    from matplotlib import cm
    # viridis = cm.get_cmap(cmpas_zhong, 500)
    for i in range(pos_int.shape[0]):
        # for i in range(200):
        plot_colored(ax, pos_int[i, 1, :], pos_int[i, 0, :], rr_int, cmap=cmpas_zhong)

    ####### plot end of trajectories with black dot  ##########
    ax.scatter(pos_int[ :,1, -1,], pos_int[ :, 0, -1,], c='k', s=1, marker='.', linewidths=0.01,
               transform=ccrs.PlateCarree())

    ################################# add some feature #################################

    ax.coastlines(linewidth=0.2)
    # ax.gridlines(linewidth=0.5, colors='k')
    ################################# plot ERA5 TOPO ###################################

    cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    lat25 = topo['lat25']
    lon25 = topo['lon25']
    topo25 = topo['topo25']
    ax.pcolormesh(lon25, lat25, topo25, shading='nearest', vmin=-6000, vmax=6000, cmap=cmap_topo,
                  transform=ccrs.PlateCarree(), zorder=0)
def plot_colorbar(cmap):
    date_len = 10
    date_len = date_len+1
    import numpy as np
    import matplotlib.pyplot as plt
    Z = np.zeros([6, date_len])
    Z[0, :]=np.arange(0,101, 10)
    x = np.arange(-0.5, date_len, 1)  # len = 21
    y = np.arange(4.5, 11, 1)  # len = 7

    ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
    cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

    ax0.set_visible(False)
    return cb

"""
previous step is downscale_1H_centroid.py to create centroid_all_2000.mat
"""
def projection_transform(lon, lat, central_latitude, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    central_latitude_abs = abs(central_latitude)
    if (central_latitude_abs <= 60) & (central_latitude_abs >= 30):
        if central_latitude > 0:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': 30, 'lat_2': 60}
        else:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': -30, 'lat_2': -60}

    elif central_latitude_abs > 60:
        proj_Out = {'proj': 'ups'}

    else:
        proj_Out = {'proj': 'merc', 'lon_0': 120}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)
a = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\SOM/centroid_deinf_2000.mat')

longpath = a['centroid']
save_labels_bmus = scio.loadmat(r"G:\OneDrive\a_matlab\a_test\test_BMUS.mat")['BMUS'][:, -1].flatten()
longpath =longpath.reshape([longpath.shape[0], 2,int(longpath.shape[1]/2)])
lon,lat = projection_transform(longpath[:,1], longpath[:,0], 80, xy2ll=True)

longpath[:,1] = lon
longpath[:,0] = lat

plot_trajectory(axs[0], longpath[save_labels_bmus == 1])
plot_trajectory(axs[1], longpath[save_labels_bmus == 6])

cb_ax = fig.add_axes([0.2, 0.08, 0.6, 0.01])
cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/rainbow200.mat', 'rainbow200')
cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
# cbar.ax.annotate('%', xy=(0.45, -7.5), xycoords='axes fraction')
cbar.set_ticks([0, 100])
cbar.set_ticklabels(['origin', 'decay'])
plt.savefig(r'figureS2_R2.png', dpi=400)
plt.show()
plt.close()
exit()