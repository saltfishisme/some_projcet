import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import os
import numpy as np
import xarray as xr
import nc4
import scipy.io as scio
import pandas as pd


"""
plot composite analysis result
"""
# def add_longitude_latitude_labels_givenloc(fig, ax, xloc=[], yloc=0):
#     import cartopy.crs as ccrs
#     import numpy as np
#
#     xlocs = [0, 60, 120, 179.9999, -60, -120]
#     gl = ax.gridlines(ylocs=[66], xlocs=xlocs,
#                       linewidth=0.3, color='black', crs=ccrs.PlateCarree(),
#                       draw_labels=True,
#                       x_inline=False, y_inline=True)
#
#     gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
#     gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
#
#     fig.canvas.draw()
#     for ea in gl.ylabel_artists:
#         ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
#         ea.set_position([yloc, 66])
#
#
#     map_boundary_path = gl.axes.spines["geo"].get_path().vertices
#     from matplotlib.path import Path
#     import matplotlib.patches as patches
#     for i_xloc in  xlocs:
#
#     points = [(0, 0), (1, 1), (2, 0), (3, 2)]
#     path = Path(points)
#
#     map_boundary_path.intersects_path(
#         this_path, filled=padding > 0)
#     map_boundary = sgeom.Polygon(map_boundary_path.vertices)
#     map_boundary_path = ccrs.PlateCarree().transform_points(x=map_boundary_path[:, 1], y=map_boundary_path[:, 0],
#                                                             src_crs=ax.projection)
#
#
#     def get_nearst_loc_lingress(a, b):
#         closest_value = a[np.abs(a[:, 0] - b).argmin(), 0]
#
#         index = np.abs(a[:, 0] - b).argmin()
#         if index == 0:
#             second_col_value = a[0, 1]
#         elif index == len(a) - 1:
#             second_col_value = a[-1, 1]
#         else:
#             x0, x1 = a[index - 1, 0], a[index + 1, 0]
#             y0, y1 = a[index - 1, 1], a[index + 1, 1]
#             second_col_value = y0 + (y1 - y0) * (b - x0) / (x1 - x0)
#
#         return [b, second_col_value]
#     yloc_forX = [get_nearst_loc_lingress(map_boundary_path[:, 0:2], i) for i in xlocs]
#     yloc_forX = dict(zip(xlocs, yloc_forX))
#
#     print(yloc_forX)
#     exit()
#     for ea in gl.xlabel_artists:
#         # ea.set_visible(True)
#         ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
#         # pos = ea.get_position()
#         # new_pos = yloc_forX[pos[0]]
#         # ea.set_position([new_pos[0], new_pos[1] + -2e+06])


budget_load = ['intensification', 'advection', 'divergence',
               'residual', 'dynamics',
               'ice_drift_x', 'ice_drift_y',
               'thickness','concentration',
               'Qtd_r', 'Qtd_d']

plus = dict(zip(budget_load, [24*60*60,24*60*60,24*60*60,
        24*60*60,24*60*60,
        1,1,
        1,1,
        1,1]))

vrange = dict(zip(budget_load, [-0.1,-0.1,-0.1,
        -0.1,-0.1,
        1,1,
        -0.4,-0.2,
        -1,-1]))

unit = dict(zip(budget_load, ['$\mathrm{m\,d^{-1}}$','$\mathrm{m\,d^{-1}}$','$\mathrm{m\,d^{-1}}$',
        '$\mathrm{m\,d^{-1}}$','$\mathrm{m\,d^{-1}}$',
        1,1,
        '$\mathrm{m\,d^{-1}}$','%',
        '','']))

central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]
xlocs = [[['0°', 0,66],['60°E', 60, 66],['120°E', 120, 75],['180°', 179.99, 82],['120°W', -120, 82],['60°W', -60, 75]],
         [['0°', 0,82],['60°E', 60, 75],['120°E', 120, 66],['180°', 179.99, 66],['120°W', -120, 75],['60°W', -60, 82]],
         [['120°E', 120, 80],['180°', 179.99, 55],['120°W', -120, 80]],
         [['0°', 0,66],['60°E', 60, 66],['120°E', 120, 75],['180°', 179.99, 82],['120°W', -120, 82],['60°W', -60, 75]],
         [['0°', 0,80],['120°W', -120, 80],['60°W', -60,55]],
         [['120°E', 120, 80],['180°', 179.99, 55],['120°W', -120, 80]]]

SMALL_SIZE = 7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance_r
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])
def plot_pcolormeshP_onetype(data, p_values):


    fig, axs = plt.subplots(2,4, subplot_kw={'projection':ccrs.NorthPolarStereo()}
                            ,figsize=[10,6])

    for i_i, i in enumerate([0,1,2,3,4,7,8]):
        ax = axs[int(i_i/4), i_i%4]
        ax.set_title('%s anomaly for Type%s' %(budget_load[i], i_type))

        import cmaps
        import matplotlib.colors as colors
        a = xr.open_dataset(r"D:\out_test\budfields_20120729.nc")
        print(a)
        exit()
        lon = a.lon.values[0]
        lat = a.lat.values[0]
        cb = ax.pcolormesh(lon, lat, data[i]*plus[i],
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(), vmin=-0.2,vmax=0.2)
        # ax.pcolormesh(lon, lat, np.ma.masked_greater(p_values[i], 0.1),
        #
        #             colors='none', levels=[0, 0.1],
        #             hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        ax.coastlines(linewidth=0.3, zorder=10)
        # ax.stock_img()
        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
        # plt.show()
        ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
        # cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
        cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)

        # cb_ax.set_xlabel('K')
    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/AR%i_%s.png' % (i_type, time_use_forAR_name), dpi=400)
    plt.close()

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']


def plot_pcolormeshP_onevar(data, p_values, vartype):


    fig = plt.figure(figsize=[5, 3.8])
    plt.subplots_adjust(top=0.93,
bottom=0.07,
left=0.02,
right=0.98,
hspace=0.05,
wspace=0.155)

    for i_i, i in enumerate([1,2,3,4,5,6]):
        central_lonlat = central_lonlat_all[i_i]
        ax = fig.add_subplot(2, 3, i,
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

        ax.set_title('%s SOM%s' %(figlabelstr[i_i], i))
        plt.suptitle('$\mathrm{H_{eff}}$ change rate anomaly')
        import cmaps
        import matplotlib.colors as colors
        a = xr.open_dataset(r"D:\budfields_20200204.nc")
        lon = a.lon.values[0]
        lat = a.lat.values[0]
        # cb = ax.pcolormesh(lon, lat, data[i_i]*plus[vartype],
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree())
        p_pass = np.where(p_values[i_i] <= 0.1)

        cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))


        ax.coastlines(linewidth=0.3)
        ax.set_extent([-2e+06, 2.5e+06, -2e+06, 2e+06],ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        # from function_shared_Main import add_longitude_latitude_labels_givenloc
        # add_longitude_latitude_labels_givenloc(fig, ax, xlocs[i_i], yloc=central_lonlat[0])

        if i == 1:
            ranges = [10, 85, 65, 85]
            # 使用NumPy的linspace函数进行插值
            lon1 = np.linspace(ranges[0], ranges[1], 100)
            lat1 = np.full_like(lon1, ranges[2])
            lon2 = np.linspace(ranges[1], ranges[1], 100)
            lat2 = np.linspace(ranges[2], ranges[3], 100)
            lon3 = np.linspace(ranges[1], ranges[0], 100)
            lat3 = np.full_like(lon3, ranges[3])
            lon4 = np.linspace(ranges[0], ranges[0], 100)
            lat4 = np.linspace(ranges[3], ranges[2], 100)

            # 合并所有点
            lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
            lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
            ax.plot(lons,
                    lats,
                    color='red', transform=ccrs.PlateCarree(), linewidth=1)
            ax.set_extent([-2e+06, 2.5e+06, -2e+06, 2e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

        elif i == 6:
            ranges = [150, 220, 55, 85]
            # 使用NumPy的linspace函数进行插值
            lon1 = np.linspace(ranges[0], ranges[1], 100)
            lat1 = np.full_like(lon1, ranges[2])
            lon2 = np.linspace(ranges[1], ranges[1], 100)
            lat2 = np.linspace(ranges[2], ranges[3], 100)
            lon3 = np.linspace(ranges[1], ranges[0], 100)
            lat3 = np.full_like(lon3, ranges[3])
            lon4 = np.linspace(ranges[0], ranges[0], 100)
            lat4 = np.linspace(ranges[3], ranges[2], 100)

            # 合并所有点
            lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
            lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
            ax.plot(lons,
                    lats,
                    color='red', transform=ccrs.PlateCarree(), linewidth=1)
            ax.set_extent([-2e+06, 2.5e+06, -2e+06, 2e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

        else:
            ax.set_extent([-2e+06, 2.5e+06, -2e+06, 2e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

        # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')


    cb_ax = fig.add_axes([0.15, 0.065, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cb_ax.set_xlabel(unit[vartype])

    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    # plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-4.png', dpi=400)
    plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\figure4_R2.png', dpi=400)
    plt.show()
    plt.close()

def plot_pcolormeshP_ax(ax, data, p_values, vartype):

    a = xr.open_dataset(r"D:\budfields_20200204.nc")
    lon = a.lon.values[0]
    lat = a.lat.values[0]
    # cb = ax.pcolormesh(lon, lat, data[i-1]*plus[vartype],
    #                    cmap=cmap_use,
    #                    transform=ccrs.PlateCarree())
    # p_pass = np.where(p_values <= 0.1)
    from scipy.ndimage import gaussian_filter
    # p_values = gaussian_filter(p_values, sigma=1)

    data_mask = np.ma.masked_where(p_values>0.1, data*plus[vartype])
    mask = np.ma.getmask(data_mask)
    p_values[~mask] = 999
    p_values[p_values < 1] = 0
    from skimage import measure
    import cv2
    thresh = cv2.threshold(p_values, 1, 255, cv2.THRESH_BINARY)[1]
    labels = measure.label(thresh, connectivity=1, background=0)
    #######
    Mask_onePic = np.zeros(data.shape, dtype='uint8')
    boundary_onePic = np.zeros(data.shape, dtype='uint8')
    for label in np.unique(labels):
        if label == 0:
            continue

        #######  prepare labelMask, labelMask is a narray contains one contour
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)


        if (numPixels > 200):
            Mask_onePic = cv2.add(Mask_onePic, labelMask)
            # print(numPixels)
            if vartype == 'residual':

                kernel = np.ones((2, 2), dtype=np.uint8)
                boundary = cv2.add(boundary_onePic,
                                   cv2.morphologyEx(labelMask, cv2.MORPH_GRADIENT, kernel))


    if (vartype == 'residual'):
        import scipy.io as scio
        scio.savemat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM%i.mat'%i_som,
                     {'lon':lon, 'lat':lat, 'boundary':boundary})
    # cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values> 0.1, data*plus[vartype]),
    #                    cmap=cmap_use,
    #                    transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))


    cb = ax.pcolormesh(lon, lat, np.ma.masked_where(Mask_onePic<=5, data*plus[vartype]),
                       cmap=cmap_use,
                       transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))
    # ax.scatter(lon[p_pass], lat[p_pass],s=0.1,c='black',marker='*',linewidth=0.1,
    #           transform=ccrs.PlateCarree(), zorder=20)

    ax.coastlines(linewidth=0.3)
    # from function_shared_Main import add_longitude_latitude_labels_givenloc
    # add_longitude_latitude_labels_givenloc(fig, ax, xlocs_i_i, yloc=central_lonlat[0])
    # plt.show()
    ax.set_extent([-2e+06, 2.5e+06, -2e+06, 2e+06],ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
    return cb

for time_use_forAR_name in ['AR']:
    ar = []
    cli = []
    p = []
    for i_type in range(1,6+1):
        ar.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/AR_%i_%s'%(i_type,time_use_forAR_name)))
        cli.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/cli_%i_%s'%(i_type,time_use_forAR_name)))
        p.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/p_%i_%s'%(i_type,time_use_forAR_name)))
    ar = np.transpose(np.array(ar), [1,0,2,3])
    cli = np.transpose(np.array(cli), [1, 0,2,3])
    p = np.transpose(np.array(p), [1, 0,2,3])

    # ######## FOR FIGURE 4
    # print(ar.shape)
    # plot_pcolormeshP_onevar(ar[0]-cli[0], p[0], 'intensification')
    # # plt.show()
    # exit()

    ########### FOR FIGURE 6,7
    xlocs = [[['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['180°', 179.99, 85], ['120°W', -120, 85],
              ['60°W', -60, 75]],
             [['0°', 0, 82], ['60°E', 60, 75], ['120°E', 120, 66], ['180°', 179.99, 66], ['120°W', -120, 75],
              ['60°W', -60, 82]],
             [['120°E', 120, 80], ['180°', 179.99, 55], ['120°W', -120, 80]],
             [['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['180°', 179.99, 82], ['120°W', -120, 82],
              ['60°W', -60, 75]],
             [['0°', 0, 80], ['120°W', -120, 80], ['60°W', -60, 55]],
             [['120°E', 120, 78], ['180°', 179.99, 55], ['120°W', -120, 78]]]

    fig = plt.figure(figsize=[4, 4.5])
    plt.subplots_adjust(top=0.975,
bottom=0.07,
left=0.02,
right=0.98,
hspace=0.0,
wspace=0.045)


    ax_num = 0
    for i_som in [1, 6]:
        # for i_var in [1,2]: # fig 7
        for i_var in [4, 3]: # fig 6
            ax_num += 1
            xlocs_i_i = xlocs[i_som - 1]
            central_lonlat = central_lonlat_all[i_som - 1]
            ax = fig.add_subplot(2, 2, ax_num,
                                 projection=ccrs.Orthographic(central_longitude=central_lonlat[0],
                                                              central_latitude=central_lonlat[1]))
            cb = plot_pcolormeshP_ax(ax, ar[i_var, i_som-1]-cli[i_var, i_som-1], p[i_var, i_som-1], budget_load[i_var])

            if i_som == 1:
                ranges = [10, 85, 65, 85]
                # 使用NumPy的linspace函数进行插值
                lon1 = np.linspace(ranges[0], ranges[1], 100)
                lat1 = np.full_like(lon1, ranges[2])
                lon2 = np.linspace(ranges[1], ranges[1], 100)
                lat2 = np.linspace(ranges[2], ranges[3], 100)
                lon3 = np.linspace(ranges[1], ranges[0], 100)
                lat3 = np.full_like(lon3, ranges[3])
                lon4 = np.linspace(ranges[0], ranges[0], 100)
                lat4 = np.linspace(ranges[3], ranges[2], 100)

                # 合并所有点
                lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
                lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
                ax.plot(lons,
                        lats,
                        color='red', transform=ccrs.PlateCarree(), linewidth=1)
            elif i_som == 6:
                ranges = [150, 220, 55, 85]
                # 使用NumPy的linspace函数进行插值
                lon1 = np.linspace(ranges[0], ranges[1], 100)
                lat1 = np.full_like(lon1, ranges[2])
                lon2 = np.linspace(ranges[1], ranges[1], 100)
                lat2 = np.linspace(ranges[2], ranges[3], 100)
                lon3 = np.linspace(ranges[1], ranges[0], 100)
                lat3 = np.full_like(lon3, ranges[3])
                lon4 = np.linspace(ranges[0], ranges[0], 100)
                lat4 = np.linspace(ranges[3], ranges[2], 100)

                # 合并所有点
                lons = np.concatenate([lon1, lon2, lon3, lon4, [ranges[0]]])
                lats = np.concatenate([lat1, lat2, lat3, lat4, [ranges[2]]])
                ax.plot(lons,
                        lats,
                        color='red', transform=ccrs.PlateCarree(), linewidth=1)

            if budget_load[i_var] == 'residual':
                ax.set_title('%s %s anomaly for SOM%s' %(figlabelstr[ax_num-1], 'thermodynamics', i_som))

            else:
                ax.set_title('%s %s anomaly for SOM%s' %(figlabelstr[ax_num-1], budget_load[i_var], i_som))

    cb_ax = fig.add_axes([0.15, 0.06, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cb_ax.set_xlabel(unit[budget_load[i_var]])

    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    # plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-7.png', dpi=400)
    plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/figure6_r2.png', dpi=400)
    plt.show()
    plt.close()

