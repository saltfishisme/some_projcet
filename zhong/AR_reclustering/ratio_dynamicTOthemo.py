import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import os
import numpy as np
import xarray as xr
import nc4
import scipy.io as scio
import pandas as pd
"""
show pathfinderv4 motion and ERA5 IVT direction
in windows
"""

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# path_finderv4 = r'D:\Pathfinderv4/'
# def load_pathfinderv4(s_time):
#     data = xr.open_dataset(path_finderv4 + r'icemotion_daily_nh_25km_%s0101_%s1231_v4.1.nc'%(s_time.strftime('%Y'), s_time.strftime('%Y')))
#
#     loc = 40
#     if loc ==366-1:
#         loc=364
#     return [data.x.values, data.y.values, data.u.values[loc], data.v.values[loc]]
#
# def plot_icemotion_ivt(s_time, contour='no'):
#     pf4_x, pf4_y, pf4_u, pf4_v = load_pathfinderv4(s_time)
#
#     import cartopy.crs as ccrs
#     fig, axs = plt.subplots(1,2, subplot_kw={'projection':ccrs.NorthPolarStereo()},figsize=[10,4])
#     delta_uv_era5 = 10
#     delta_uv_pf4 = 5
#     # long_2d, lat_2d, era5_u, era5_v = [i[::delta_uv_era5, ::delta_uv_era5] for i in [long_2d, lat_2d, era5_u, era5_v]]
#     # pf4_x, pf4_y, pf4_u, pf4_v = [i[::delta_uv_era5, ::delta_uv_era5] for i in [pf4_x, pf4_y, pf4_u, pf4_v]]
#     # def delta_var(var):
#     #     return var[::delta_uv_era5, ::delta_uv_era5]
#
#     pf4_x, pf4_y = [i[::delta_uv_pf4] for i in [pf4_x, pf4_y]]
#     pf4_u, pf4_v = [i[::delta_uv_pf4, ::delta_uv_pf4] for i in [pf4_u, pf4_v]]
#
#     axs[1].quiver(pf4_x, pf4_y,
#               pf4_u, pf4_v,
#               transform=ccrs.LambertAzimuthalEqualArea(central_latitude=90), scale=500)
#     axs[1].coastlines()
#     axs[1].set_extent([0, 359, 60, 90], crs=ccrs.PlateCarree())
#     # plt.title(s_time.strftime('%Y%m%d %H')+' type%i'%i_type+' '+AR_in_ARALLBUTar)
#     plt.show()
#     # path_save = r'/home/linhaozhong/work/AR_NP85/budget/pic_icemotion_IVT/'+i_file+'/'
#     # os.makedirs(path_save,exist_ok=True)
#     # plt.savefig(path_save+s_time.strftime('%Y%m%d_%H')+' type%i.png'%i_type, dpi=400)
#     plt.close()
#     print('I am cut!')
#
# plot_icemotion_ivt(pd.to_datetime('2008-02-10 00:00'))
#
# exit()


"""
show pathfinderv4 motion and ERA5 IVT direction
"""
# path_finderv4 = r'/home/linhaozhong/work/Data/Pathfinderv4/'
# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# def load_pathfinderv4(s_time):
#     data = xr.open_dataset(path_finderv4 + r'icemotion_daily_nh_25km_%s0101_%s1231_v4.1.nc'%(s_time.strftime('%Y'), s_time.strftime('%Y')))
#     period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
#     loc = period.day_of_year-1
#     if loc >=364:
#         loc=364
#     return [data.x.values, data.y.values, data.u.values[loc], data.v.values[loc]]
# def load_ivt_total(s_time):
#     '''
#     :param s_time: datetime64
#     :return: ivt_total
#     '''
#     s_time = pd.to_datetime(s_time)
#
#     s_time_str = s_time.strftime('%Y%m%d')
#     index_vivt = xr.open_dataset(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time_str[:4], s_time_str))
#     index_uivt = xr.open_dataset(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time_str[:4], s_time_str))
#     # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
#     # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))
#
#     ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
#     ivt_east = index_uivt.sel(time=s_time)['p71.162'].values
#
#     return [index_uivt.longitude.values, index_vivt.latitude.values, ivt_east, ivt_north]
#
# def plot_icemotion_ivt(s_time, contour='no'):
#     pf4_x, pf4_y, pf4_u, pf4_v = load_pathfinderv4(s_time)
#     era_x, era_y, era5_u, era5_v = load_ivt_total(s_time)
#
#     long_2d, lat_2d = np.meshgrid(era_x, era_y)
#     import cartopy.crs as ccrs
#     fig, axs = plt.subplots(1,2, subplot_kw={'projection':ccrs.NorthPolarStereo()},figsize=[10,4])
#     delta_uv_era5 = 10
#     delta_uv_pf4 = 5
#     # long_2d, lat_2d, era5_u, era5_v = [i[::delta_uv_era5, ::delta_uv_era5] for i in [long_2d, lat_2d, era5_u, era5_v]]
#     # pf4_x, pf4_y, pf4_u, pf4_v = [i[::delta_uv_era5, ::delta_uv_era5] for i in [pf4_x, pf4_y, pf4_u, pf4_v]]
#     # def delta_var(var):
#     #     return var[::delta_uv_era5, ::delta_uv_era5]
#
#     long_2d, lat_2d, era5_u, era5_v = [i[::delta_uv_era5, ::delta_uv_era5] for i in [long_2d, lat_2d, era5_u, era5_v]]
#     pf4_x, pf4_y = [i[::delta_uv_pf4] for i in [pf4_x, pf4_y]]
#     pf4_u, pf4_v = [i[::delta_uv_pf4, ::delta_uv_pf4] for i in [pf4_u, pf4_v]]
#
#     if not isinstance(contour, str):
#         contour = contour[::delta_uv_era5, ::delta_uv_era5]
#         axs[0].quiver(long_2d[contour>0], lat_2d[contour>0],
#                   era5_u[contour>0], era5_v[contour>0],
#                   transform=ccrs.PlateCarree(),color='red', scale=2000)
#         axs[0].quiver(long_2d[contour<=0], lat_2d[contour<=0],
#                   era5_u[contour<=0], era5_v[contour<=0],
#                   transform=ccrs.PlateCarree(),color='black', scale=1500)
#     else:
#         axs[0].quiver(long_2d, lat_2d,
#                   era5_u, era5_v,
#                   transform=ccrs.PlateCarree(),color='black', scale=1000)
#     axs[0].coastlines()
#     axs[0].set_extent([0, 359, 60, 90], crs=ccrs.PlateCarree())
#     axs[1].quiver(pf4_x, pf4_y,
#               pf4_u, pf4_v,
#               transform=ccrs.LambertAzimuthalEqualArea(central_latitude=90), scale=500)
#     axs[1].coastlines()
#     axs[1].set_extent([0, 359, 60, 90], crs=ccrs.PlateCarree())
#     plt.title(s_time.strftime('%Y%m%d %H')+' type%i'%i_type+' '+AR_in_ARALLBUTar)
#     path_save = r'/home/linhaozhong/work/AR_NP85/budget/pic_icemotion_IVT/'+i_file+'/'
#     os.makedirs(path_save,exist_ok=True)
#     plt.savefig(path_save+s_time.strftime('%Y%m%d_%H')+' type%i.png'%i_type, dpi=400)
#     plt.close()
#     print('I am cut!')
#
# import os
# import numpy as np
# import pandas as pd
# import xarray as xr
# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# import nc4
# import scipy.io as scio
# from datetime import timedelta
#
# main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# path_main_Save = r'/home/linhaozhong/work/AR_NP85/budget/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
# for iI, i_type in enumerate(np.unique(save_labels_bmus)):
#     if i_type == 0:
#         continue
#     file = file_all[save_labels_bmus==i_type]
#
#     for i_file in file:
#
#         info_AR_row = xr.open_dataset(main_path+i_file[:-1])
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#         time_all_butAR = info_AR_row.time_all_butAR.values
#
#         time_use = time_all
#
#         for s_time in time_use:
#             if s_time in time_AR:
#                 AR_in_ARALLBUTar = 'ar'
#             elif s_time in time_all_butAR:
#                 AR_in_ARALLBUTar = 'allbut'
#             plot_icemotion_ivt(pd.to_datetime(s_time), info_AR_row['contour_all'].sel(time_all=s_time).values)
#
#
# exit()
#
#
# budget_load = ['intensification', 'advection', 'divergence', 'residual', 'dynamics',
#                'ice_drift_x', 'ice_drift_y',
#                'thickness','concentration'
#                ]
# path_main = r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget\AR/'
# files = os.listdir(path_main)
# for i_files in files:
#     a_files = os.listdir(path_main+i_files)
#
#     def load_budget_inGivenFile(path):
#         var_pattern = []
#         file_here = os.listdir(path)
#         for i_file_here in file_here:
#             if not len(i_file_here) == 21:
#                 continue
#             budget = xr.open_dataset(path + i_file_here)
#             var_in_single_file = []
#             for var in budget_load:
#                 var_in_single_file.append(budget[var].values[0])
#             var_pattern.append(var_in_single_file)
#         return np.nanmean(np.array(var_pattern), axis=0)
#
#     a = load_budget_inGivenFile(path_main+i_files+'/')
#     cli = load_budget_inGivenFile(path_main+i_files+'/anomaly/')
#
#     ass = xr.open_dataset(r"D:\out_test\budfields_20120729.nc")
#     lon = ass.lon.values[0]
#     lat = ass.lat.values[0]
#     fig, axs = plt.subplots(2, 4, subplot_kw={'projection': ccrs.NorthPolarStereo()},figsize=[10,6])
#     for i_i, i in enumerate([0, 1, 2, 3, 4, 7, 8]):
#         ax = axs[int(i_i / 4), i_i % 4]
#         ax.set_title('%s anomaly for Type' % (budget_load[i]))
#         data = a[i]-cli[i]
#         import cartopy.crs as ccrs
#         import matplotlib.colors as colors
#
#         data[data==0]=np.nan
#         cb = ax.pcolormesh(lon, lat, data*24*60*60, transform=ccrs.PlateCarree(), cmap='bwr', norm=colors.CenteredNorm())
#         plt.colorbar(cb, ax=ax)
#         ax.coastlines()
#         ax.set_extent([0, 359, 70, 90], crs=ccrs.PlateCarree())
#
#     stab = 8
#     u = a[5,::stab,::stab]
#     v = a[6,::stab,::stab]
#     cb = axs[1][3].quiver(lon[::stab,::stab], lat[::stab,::stab], u, v, transform=ccrs.PlateCarree())
#     axs[1][3].set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
#     axs[1][3].coastlines()
#     # plt.show()
#     plt.savefig(path_main+i_files+'div.png', dpi=400)
# exit()

"""
load AR-contour information from budget result
"""
# import os
# import numpy as np
# import pandas as pd
# import xarray as xr
# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# import nc4
# import scipy.io as scio
# from datetime import timedelta
#
# main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# path_main_Save = r'/home/linhaozhong/work/AR_NP85/budget/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
# resolution = 0.5
#
#
# def load_budgetDT(path_nc, contour):
#     import cv2
#     def query_in(contour, tree):
#         # ================================================================================================ #
#         contour = np.array(contour, dtype='uint8')
#         thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
#         contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
#         from shapely.geometry import Polygon
#         res = []
#         for i_contour in contours:
#             i_contour = np.squeeze(i_contour)
#             if len(i_contour) <5:
#                 continue
#             # trans to row
#             polygon = Polygon(i_contour)
#
#             # print('hi')
#             # import matplotlib.pyplot as plt
#             # # plt.plot(*polygon.exterior.xy)
#             # plt.plot(*polygon.exterior.xy)
#             # plt.show()
#             res_now = tree.query(polygon)
#             res.extend(res_now.tolist())
#         return res
#
#         # cv2.imshow('now', bitwiseand)
#         # cv2.waitKey(0)
#         # plt.show()
#
#     index = query_in(contour, tree)
#     file_here = os.listdir(path_nc)
#     dynamic = []
#     residual = []
#     for i_file_here in file_here:
#         if len(i_file_here) == 21:
#
#             budget = xr.open_dataset(path_nc+i_file_here)
#             dynamic.append(budget['dynamics'].values[0]*weight*24*60*60)
#             residual.append(budget['residual'].values[0]*weight*24*60*60)
#     dynamic = np.nansum(dynamic, axis=0).flatten()
#     residual = np.nansum(residual, axis=0).flatten()
#
#     return dynamic[index], residual[index]
#
# data = xr.open_dataset(r"/home/linhaozhong/work/AR_NP85/budget/all_butAR/2017012623_1.nc4/budfields_20170123.nc")
#
# from function_shared_Main import roll_longitude_from_359to_negative_180, cal_area_differentLL
# weight = cal_area_differentLL(data.lon.values[0], data.lat.values[0])
#
# points_lat = data.lat.values
#
# points_lon =roll_longitude_from_359to_negative_180(data.lon.values, reverse=True)
#
# # transform into 0,1,2,3 relative coordination, only works in north hemisphere
# # !!!!!!!!!!!!!!!!!!!!!!!#
# points_lat_relaCoord = (90 - points_lat) / resolution
# points_lon_relaCoord = points_lon / resolution
#
# # to Stree
# from shapely.strtree import STRtree
# from shapely.geometry import Point
# print(points_lat_relaCoord, points_lon_relaCoord)
# points = [Point(x, y) for x, y in zip(points_lon_relaCoord.flatten(), points_lat_relaCoord.flatten())]
# tree = STRtree(points)  # create a 'database' of points
#
# all_info = []
# for iI, i in enumerate(np.unique(save_labels_bmus)):
#     if i == 0:
#         continue
#     file = file_all[save_labels_bmus==i]
#     mean_now = []
#     mean_now_all = []
#     mean_now_allbuAR = []
#     for i_file in file:
#         info_now = []
#         info_AR_row = xr.open_dataset(main_path+i_file[:-1])
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#         time_all_butAR = info_AR_row.time_all_butAR.values
#         print('%%%%%%%%%%%%%%%%%%%%%%%')
#         print(time_all)
#         print(time_all_butAR)
#         continue
#
#
#         path_SaveAR = path_main_Save+'AR/'+i_file+'/'
#         time_here = time_AR
#         contour = np.nansum(info_AR_row['contour_all'].sel(time_all=time_here).values, axis=0)
#         dynamic, residual = load_budgetDT(path_SaveAR, contour)
#         dynamic[dynamic == 0] = np.nan
#         residual[residual==0]= np.nan
#         Qtd = (residual**2-dynamic**2)/(residual**2+dynamic**2)
#         nc4.save_nc4(np.array([dynamic, residual, Qtd]), path_SaveAR+'qdr.nc')
#         info_now.append([i_file, i, np.nanmean(dynamic), np.nanmean(residual), np.nanmean(Qtd)])
#         mean_now.append([np.nanmean(dynamic), np.nanmean(residual), np.nanmean(Qtd)])
#
#         path_SaveAR = path_main_Save + 'all/' + i_file + '/'
#         time_here = time_all
#         contour = np.nansum(info_AR_row['contour_all'].sel(time_all=time_here).values, axis=0)
#         dynamic, residual = load_budgetDT(path_SaveAR, contour)
#         dynamic[dynamic == 0] = np.nan
#         residual[residual==0]= np.nan
#         Qtd = (residual**2-dynamic**2)/(residual**2+dynamic**2)
#         nc4.save_nc4(np.array([dynamic, residual, Qtd]), path_SaveAR+'qdr.nc')
#         info_now.append([i_file, i, np.nanmean(dynamic), np.nanmean(residual), np.nanmean(Qtd)])
#         mean_now_all.append([np.nanmean(dynamic), np.nanmean(residual), np.nanmean(Qtd)])
#
#         path_SaveAR = path_main_Save + 'all_butAR/' + i_file + '/'
#         time_here = time_all_butAR
#         contour = np.nansum(info_AR_row['contour_all'].sel(time_all=time_here).values, axis=0)
#         dynamic, residual = load_budgetDT(path_SaveAR, contour)
#         dynamic[dynamic == 0] = np.nan
#         residual[residual==0]= np.nan
#         Qtd = (residual**2-dynamic**2)/(residual**2+dynamic**2)
#         nc4.save_nc4(np.array([dynamic, residual, Qtd]), path_SaveAR+'qdr.nc')
#         info_now.append([i_file, i, np.nanmean(dynamic), np.nanmean(residual), np.nanmean(Qtd)])
#         mean_now_allbuAR.append([np.nanmean(dynamic), np.nanmean(residual), np.nanmean(Qtd)])
#
#         all_info.append(info_now)
#
#     mean_now = np.array(mean_now)
#     mean_now = np.nanmean(mean_now, axis=0)
#     print('type%i'%i, mean_now)
#
#     mean_now = np.array(mean_now_all)
#     mean_now = np.nanmean(mean_now, axis=0)
#     print('type%i'%i, mean_now)
#
#     mean_now = np.array(mean_now_allbuAR)
#     mean_now = np.nanmean(mean_now, axis=0)
#     print('type%i'%i, mean_now)
#
# df = pd.DataFrame(all_info)
# df.to_csv(path_main_Save + 'test.csv')

"""
plot every event analysis result
"""
# main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# path_main_Save = r'/home/linhaozhong/work/AR_NP85/budget/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
# budget_load = ['intensification', 'advection', 'divergence', 'residual', 'dynamics',
#                'ice_drift_x', 'ice_drift_y',
#                'thickness','concentration'
#                ]
# plus = [24*60*60,24*60*60,24*60*60,
#         24*60*60,24*60*60,
#         1,1,
#         1,1]
# # plus = dict(zip(budget_load, plus))
# path_main = r'/home/linhaozhong/work/AR_NP85/budget/'
#
# for time_use_forAR_name in ['AR', 'after', 'before']:
#     for iI, i_type in enumerate(np.unique(save_labels_bmus)):
#         if i_type == 0:
#             continue
#         file = file_all[save_labels_bmus == i_type]
#
#         for i_file in file:
#             if not os.path.exists(path_main+time_use_forAR_name+'/'+i_file+'/'):
#                 continue
#
#             def load_budget_inGivenFile(path):
#                 var_pattern = []
#                 file_here = os.listdir(path)
#                 for i_file_here in file_here:
#                     if not len(i_file_here) == 21:
#                         continue
#                     budget = xr.open_dataset(path + i_file_here)
#                     var_in_single_file = []
#                     for var in budget_load:
#                         var_in_single_file.append(budget[var].values[0])
#                     var_pattern.append(var_in_single_file)
#                 return np.nanmean(np.array(var_pattern), axis=0)
#
#             a = load_budget_inGivenFile(path_main+time_use_forAR_name+'/'+i_file+'/')
#             cli = load_budget_inGivenFile(path_main+time_use_forAR_name+'/'+i_file+'/anomaly/')
#
#             ass = xr.open_dataset(r"/home/linhaozhong/work/AR_NP85/budget_row/all/2000010917_2.nc4/budfields_19991229.nc")
#             lon = ass.lon.values[0]
#             lat = ass.lat.values[0]
#             fig, axs = plt.subplots(2, 4, subplot_kw={'projection': ccrs.NorthPolarStereo()},figsize=[10,6])
#             for i_i, i in enumerate([0, 1, 2, 3, 4, 7, 8]):
#                 ax = axs[int(i_i / 4), i_i % 4]
#                 ax.set_title('%s anomaly for Type %i' % (budget_load[i], i_type))
#                 data = a[i]-cli[i]
#                 import cartopy.crs as ccrs
#                 import matplotlib.colors as colors
#
#
#                 data[data==0]=np.nan
#                 cb = ax.pcolormesh(lon, lat, data*plus[i], transform=ccrs.PlateCarree(),
#                                    cmap='bwr', vmin=-0.2,vmax=0.2)
#                 plt.colorbar(cb, ax=ax)
#                 ax.coastlines()
#                 ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
#
#             stab = 8
#             u = a[5, ::stab, ::stab]
#             v = a[6, ::stab, ::stab]
#             cb = axs[1][3].quiver(lon[::stab, ::stab], lat[::stab, ::stab], u, v, transform=ccrs.PlateCarree())
#             axs[1][3].set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
#             axs[1][3].coastlines()
#
#
#             # plt.show()
#             plt.savefig(path_main+time_use_forAR_name+'/'+i_file+'div_type%i.png'%i_type, dpi=400)
#             print(path_main+time_use_forAR_name+'/'+i_file+'div_type%i.png'%i_type)
#             plt.close()
# exit()



"""
save composite analysis result
"""
# main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
# path_main_Save = r'/home/linhaozhong/work/AR_NP85/budget/'
# save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
# file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
# budget_load = ['intensification', 'advection', 'divergence', 'residual', 'dynamics',
#                'ice_drift_x', 'ice_drift_y',
#                'thickness','concentration'
#                ]
#
# for time_use_forAR_name in ['AR', 'before', 'after']:
#     for iI, i_type in enumerate(np.unique(save_labels_bmus)):
#         if i_type == 0:
#             continue
#         file = file_all[save_labels_bmus == i_type]
#
#         var_AR_pattern = []
#         var_cli_pattern = []
#
#         for i_file in file:
#
#             path_SaveAR = path_main_Save+'%s/'%time_use_forAR_name+i_file+'/'
#             if not os.path.exists(path_SaveAR):
#                 continue
#             def load_budget_inGivenFile(path):
#                 var_pattern = []
#                 file_here = os.listdir(path)
#                 for i_file_here in file_here:
#                     if not len(i_file_here) == 21:
#                         continue
#                     budget = xr.open_dataset(path_SaveAR + i_file_here)
#                     var_in_single_file = []
#                     for var in budget_load:
#                         var_in_single_file.append(budget[var].values[0])
#                     var_pattern.append(var_in_single_file)
#                 return np.nanmean(np.array(var_pattern), axis=0)
#             var_AR_pattern.append(load_budget_inGivenFile(path_SaveAR))
#
#             path_SaveAR = path_SaveAR + 'anomaly/'
#             var_cli_pattern.append(load_budget_inGivenFile(path_SaveAR))
#
#         var_AR_pattern = np.array(var_AR_pattern)
#         var_cli_pattern = np.array(var_cli_pattern)
#         from scipy import stats
#         p_values = np.zeros(var_cli_pattern[0].shape)
#         for i_i in range(len(budget_load)):
#
#             _, p_values[i_i] = stats.ttest_ind(var_cli_pattern[:,i_i], var_AR_pattern[:,i_i],
#                                                axis=0, equal_var=False)
#         nc4.save_nc4(np.nanmean(var_AR_pattern,axis=0),
#                      r'/home/linhaozhong/work/AR_NP85/budget/composite/AR_%i_%s'%(i_type,time_use_forAR_name))
#         nc4.save_nc4(np.nanmean(var_cli_pattern,axis=0),
#                      r'/home/linhaozhong/work/AR_NP85/budget/composite/cli_%i_%s'%(i_type,time_use_forAR_name))
#
#         nc4.save_nc4(p_values,
#                      r'/home/linhaozhong/work/AR_NP85/budget/composite/p_%i_%s'%(i_type,time_use_forAR_name))
#
# exit()


"""
plot composite analysis result
"""

budget_load = ['intensification', 'advection', 'divergence',
               'residual', 'dynamics',
               'ice_drift_x', 'ice_drift_y',
               'thickness','concentration',
               'Qtd_r', 'Qtd_d']

plus = dict(zip(budget_load, [24*60*60,24*60*60,24*60*60,
        24*60*60,24*60*60,
        1,1,
        1,1,
        1,1]))

vrange = dict(zip(budget_load, [-0.1,-0.1,-0.1,
        -0.1,-0.1,
        1,1,
        -0.4,-0.2,
        -1,-1]))

unit = dict(zip(budget_load, ['m/day','m/day','m/day',
        'm/day','m/day',
        1,1,
        'm/day','%',
        '','']))

central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]

SMALL_SIZE = 7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance_r
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])
def plot_pcolormeshP_onetype(data, p_values):


    fig, axs = plt.subplots(2,4, subplot_kw={'projection':ccrs.NorthPolarStereo()}
                            ,figsize=[10,6])

    for i_i, i in enumerate([0,1,2,3,4,7,8]):
        ax = axs[int(i_i/4), i_i%4]
        ax.set_title('%s anomaly for Type%s' %(budget_load[i], i_type))

        import cmaps
        import matplotlib.colors as colors
        a = xr.open_dataset(r"D:\out_test\budfields_20120729.nc")
        print(a)
        exit()
        lon = a.lon.values[0]
        lat = a.lat.values[0]
        cb = ax.pcolormesh(lon, lat, data[i]*plus[i],
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(), vmin=-0.2,vmax=0.2)
        # ax.pcolormesh(lon, lat, np.ma.masked_greater(p_values[i], 0.1),
        #
        #             colors='none', levels=[0, 0.1],
        #             hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        ax.coastlines(linewidth=0.3, zorder=10)
        # ax.stock_img()
        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
        # plt.show()
        ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
        # cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
        cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)

        # cb_ax.set_xlabel('K')
    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/AR%i_%s.png' % (i_type, time_use_forAR_name), dpi=400)
    plt.close()

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']


def plot_pcolormeshP_onevar(data, p_values, vartype):


    fig = plt.figure(figsize=[5, 3.8])
    plt.subplots_adjust(top=0.93,
bottom=0.07,
left=0.02,
right=0.98,
hspace=0.05,
wspace=0.155)

    for i_i, i in enumerate([1,2,3,4,5,6]):
        central_lonlat = central_lonlat_all[i_i]
        ax = fig.add_subplot(2, 3, i,
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

        ax.set_title('%s SOM%s' %(figlabelstr[i_i], i))
        plt.suptitle('Intensification Anomaly')
        import cmaps
        import matplotlib.colors as colors
        a = xr.open_dataset(r"D:\budfields_20200204.nc")
        lon = a.lon.values[0]
        lat = a.lat.values[0]
        # cb = ax.pcolormesh(lon, lat, data[i_i]*plus[vartype],
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree())
        p_pass = np.where(p_values[i_i] <= 0.1)

        cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
                           cmap=cmap_use,
                           transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))


        ax.coastlines(linewidth=0.3, zorder=10)

        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')

        ax.set_extent([-2e+06, 2e+06, -2e+06, 2e+06],ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

    cb_ax = fig.add_axes([0.15, 0.065, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cb_ax.set_xlabel(unit[vartype])

    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/figure4.png', dpi=400)
    plt.close()


def plot_pcolormeshP_ax(ax, data, p_values, vartype):

    a = xr.open_dataset(r"D:\budfields_20200204.nc")
    lon = a.lon.values[0]
    lat = a.lat.values[0]
    # cb = ax.pcolormesh(lon, lat, data[i-1]*plus[vartype],
    #                    cmap=cmap_use,
    #                    transform=ccrs.PlateCarree())
    # p_pass = np.where(p_values <= 0.1)
    from scipy.ndimage import gaussian_filter
    # p_values = gaussian_filter(p_values, sigma=1)

    data_mask = np.ma.masked_where(p_values>0.1, data*plus[vartype])
    mask = np.ma.getmask(data_mask)
    p_values[~mask] = 999
    p_values[p_values < 1] = 0
    from skimage import measure
    import cv2
    thresh = cv2.threshold(p_values, 1, 255, cv2.THRESH_BINARY)[1]
    labels = measure.label(thresh, connectivity=1, background=0)
    #######
    Mask_onePic = np.zeros(data.shape, dtype='uint8')
    boundary_onePic = np.zeros(data.shape, dtype='uint8')
    for label in np.unique(labels):
        if label == 0:
            continue

        #######  prepare labelMask, labelMask is a narray contains one contour
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)


        if (numPixels > 200):
            Mask_onePic = cv2.add(Mask_onePic, labelMask)
            # print(numPixels)
            if vartype == 'residual':
                print(numPixels)
                kernel = np.ones((2, 2), dtype=np.uint8)
                boundary = cv2.add(boundary_onePic,
                                   cv2.morphologyEx(labelMask, cv2.MORPH_GRADIENT, kernel))

    data = data*plus[vartype]
    data[Mask_onePic<=5] = np.nan
    return data


for time_use_forAR_name in ['AR']:
    ar = []
    cli = []
    p = []
    for i_type in range(1,6+1):
        ar.append(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/AR_%i_%s'%(i_type,time_use_forAR_name)))
        cli.append(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/cli_%i_%s'%(i_type,time_use_forAR_name)))
        p.append(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\budget/composite/p_%i_%s'%(i_type,time_use_forAR_name)))
    ar = np.transpose(np.array(ar), [1,0,2,3])
    cli = np.transpose(np.array(cli), [1, 0,2,3])
    p = np.transpose(np.array(p), [1, 0,2,3])


    fig = plt.figure(figsize=[4, 4.5])
    plt.subplots_adjust(top=0.975,
bottom=0.07,
left=0.02,
right=0.98,
hspace=0.0,
wspace=0.045)


    ax_num = 0

    cal_ratio = []
    for i_som in [1, 6]:
        for i_var in [4,3]:

            ax_num += 1
            central_lonlat = central_lonlat_all[i_som - 1]
            ax = fig.add_subplot(2, 2, ax_num,
                                 projection=ccrs.Orthographic(central_longitude=central_lonlat[0],
                                                              central_latitude=central_lonlat[1]))
            final_contour = plot_pcolormeshP_ax(ax, ar[i_var, i_som-1]-cli[i_var, i_som-1], p[i_var, i_som-1], budget_load[i_var])

            # plt.imshow(final_contour)
            # plt.show()
            a = xr.open_dataset(r"D:\budfields_20200204.nc")
            lon = a.lon.values[0]
            lat = a.lat.values[0]
            print('%%%%%')
            print(i_som, i_var)
            if i_som == 1:
                contour = final_contour[((lat < 82) & (lat > 70) & (lon > 10) & (lon < 85))]
            elif i_som == 6:
                contour = final_contour[((lat < 90) & (lat > 60) & ((lon > 160) | (lon < -150)))]
            cal_ratio.append(np.nansum(contour[contour<0]))
    print(cal_ratio[0]/(cal_ratio[0]+cal_ratio[1]))
    print(cal_ratio[2] / (cal_ratio[2] + cal_ratio[3]))



