import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5,3.2])

plt.subplots_adjust(top=0.87,
bottom=0.0,
left=0.005,
right=0.995,
hspace=0.14,
wspace=0.0)


cmpas_here = cmaps.cmocean_balance
trend = nc4.read_nc4(
    r'G:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/trend_ar_pattern')[:120, ]
p = nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\arTypeContribute/p_ar_pattern')[
    :120, ]

trend_1979 = nc4.read_nc4(
    r'G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\arTypeContribute/trend_ar_pattern_1979')[:120, ]
p_1979 = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\arTypeContribute/p_ar_pattern_1979')[
    :120, ]

# trend[np.isnan(trend)] = 0
# trend[trend < 0.00001] = np.nan
import matplotlib.colors as colors


# cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend
#                  , vmin=-5, vmax=5, extend='both',
#                  cmap=cmaps.cmocean_balance,
#                  transform=ccrs.PlateCarree())
ax = fig.add_subplot(1, 2, 1, projection=ccrs.Orthographic(0, 90))
cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend_1979*10
                 , vmin=-0.3, vmax=0.3,
                 cmap=cmpas_here,
                 transform=ccrs.PlateCarree())
cbax = plt.colorbar(cb, orientation="horizontal", ax=ax,shrink=0.6, pad=0.07)
cbax.ax.set_xlabel('% decad$e^{-1}$')

ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:120], np.ma.masked_greater(p_1979, 0.1),

            colors='none', levels=[0, 0.1],
            hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
ax.coastlines(linewidth=0.3)
from function_shared_Main import add_longitude_latitude_labels
add_longitude_latitude_labels(fig, ax, 45, yloc=350)
ax.set_title('(a) 1979-2020')



ax = fig.add_subplot(1, 2, 2, projection=ccrs.Orthographic(0, 90))
cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend*10
                 , vmin=-1, vmax=1,
                 cmap=cmpas_here,
                 transform=ccrs.PlateCarree())
cbax = plt.colorbar(cb, orientation="horizontal", ax=ax,shrink=0.6, pad=0.07)
cbax.ax.set_xlabel('% decad$e^{-1}$')
ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:120], np.ma.masked_greater(p, 0.1),

            colors='none', levels=[0, 0.1],
            hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
ax.coastlines(linewidth=0.3)
ax.set_title('(b) 2000-2020')

# plt.suptitle('AR frequency trend')
# cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#
# cb_ax.set_xlabel('trend')
# plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)
from function_shared_Main import add_longitude_latitude_labels
add_longitude_latitude_labels(fig, ax, 45, yloc=350)

plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_Arctic\figure1.png' ,
            dpi=400)
plt.close()
exit()

exit(0)
