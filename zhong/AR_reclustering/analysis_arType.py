import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

# df = pd.read_csv(r'/home/linhaozhong/work/AR_NP85/new_Clustering_2000/type_filename_0.2.csv')

central_ll_all = [[20, 70],[70,70],[180,70], [20,70],[300,70],[180,70]]
year_begin = 1940
year_end = 2020

save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
for iI, i_arType_unique in enumerate(np.unique(save_labels_bmus)):
    if i_arType_unique == 0:
        continue
    arType_name = str(i_arType_unique)
    central_ll = central_ll_all[iI]  # central longitude and latitude for othr projection
    path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
    arType_file = file_all[save_labels_bmus==i_arType_unique]
    arType_file = [i[:-1] for i in arType_file]

# for i_arType_unique in np.arange(7):
#     arType_name = str(i_arType_unique)
#     central_ll = central_ll_all[i_arType_unique]  # central longitude and latitude for othr projection
#     path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
#     arType_file = np.array(df[arType_name].dropna())
    path_uvivt = r'/home/linhaozhong/work/AR_NP85/new_Clustering/'
    path_SaveData = r'/home/linhaozhong/work/AR_NP85/new_Clustering/%s/'%arType_name
    os.makedirs(path_SaveData, exist_ok=True)

    """
    ==========================================================
    """

    long = np.arange(0, 360, 0.5)
    lat = np.arange(90, -90.01, -0.5)
    path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
    bu_addition_Name = ''
    path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
    path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
    path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
    resolution = 0.5
    path_PIOMS = r'/home/linhaozhong/work/Data/PIOMS_nc/'

    """
    ===========================      create  u_vivt_yearlyPattern.mat  DJF  ==========================
    """

    if not os.path.exists(path_uvivt+'u_vivt_yearlyPattern.mat'):
        # from 1979 - 2021
        ds = xr.open_dataset(r'/home/linhaozhong/u_vivt_DJF.nc')

        # yearly series

        # Wrap it into a simple function
        def weighted_temporal_mean(ds, var):
            """
            weight by days in each month
            """
            # Determine the month length
            month_length = ds.time.dt.days_in_month

            # Calculate the weights
            wgts = month_length.groupby("time.year") / month_length.groupby("time.year").sum()

            # Make sure the weights in each year add up to 1
            np.testing.assert_allclose(wgts.groupby("time.year").sum(xr.ALL_DIMS), 1.0)

            # Subset our dataset for our variable
            obs = ds[var]
            year = obs['time.year']
            month = obs['time.month']
            year[month == 12] = year[month == 12] + 1
            obs['time.year'] = year

            # Setup our masking for nan values
            cond = obs.isnull()
            ones = xr.where(cond, 0.0, 1.0)

            # Calculate the numerator
            obs_sum = (obs * wgts).groupby("time.year").sum(dim="time")

            # Calculate the denominator
            ones_out = (ones * wgts).groupby("time.year").sum(dim="time")

            # Return the weighted average
            return obs_sum / ones_out, obs_sum['time.year'].values

        u, year_uv = weighted_temporal_mean(ds, 'p71.162')
        v,_ = weighted_temporal_mean(ds, 'p72.162')

        scio.savemat(path_uvivt+'u_vivt_yearlyPattern.mat',
                     {'uivt': np.array(u.values, dtype='double'),
                      'vivt': np.array(v.values, dtype='double'),
                      'year':year_uv
                      })

    """
    ===========================    moisture_into_Arctic_ratio  DJF ==========================
    """


    def load_ivt_total_anomaly(s_time):
        '''
        :param s_time: datetime64
        :return: ivt_total
        '''
        s_time = pd.to_datetime(s_time)

        s_time_str = s_time.strftime('%Y%m%d')
        index_vivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
                s_time_str[:4], s_time_str))
        index_uivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
                s_time_str[:4], s_time_str))

        import scipy.io as scio
        vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                    '%s.mat' % s_time.strftime('%m%d'))[
            s_time.strftime('%m%d')][int(s_time.strftime('%H'))]

        uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                    '%s.mat' % s_time.strftime('%m%d'))[
            s_time.strftime('%m%d')][int(s_time.strftime('%H'))]

        ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
        ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

        ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
        return ivt_north
    avg = np.zeros([year_end-year_begin+1])
    avg_add1 = np.zeros([year_end-year_begin+1])
    for i_file in arType_file:
        info_AR_row = xr.open_dataset(path_contour+i_file)
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        time_use = int(len(time_all[AR_loc:]) / 2)

        contour_all = np.zeros([len(lat),len(long)])

        for i_time in time_all:
            year_loc = int(pd.to_datetime(i_time).strftime('%Y'))-year_begin
            if pd.to_datetime(i_time).strftime('%m') == '12':
                year_loc += 1
                if year_loc == year_end - year_begin+1:
                    continue

            # sum for all year
            contour = info_AR_row['contour_all'].sel(time_all=i_time).values

            ivt = load_ivt_total_anomaly(pd.to_datetime(i_time))
            ivt[(ivt<0) | (contour<0.5)] = np.nan
            ivt_entry_arctic = np.nansum(ivt[46,:])
            ivt_entry_arctic = ivt_entry_arctic

            avg[year_loc] += ivt_entry_arctic
            avg_add1[year_loc] +=1

        # # sum for all year
        # avg += np.nansum(info_AR_row['contour_all'].values, axis=0)
    # avg = avg/avg_add1
    nc4.save_nc4(avg, path_SaveData+'AR%s_vivt_trans_Arctic'%arType_name)

    avg = nc4.read_nc4(path_SaveData+'AR%s_vivt_trans_Arctic'%arType_name)
    ds = scio.loadmat(path_uvivt+'u_vivt_yearlyPattern.mat')
    year_uv = np.squeeze(ds['year'])
    loc = np.where((year_uv>=year_begin)&(year_uv<=year_end))
    vivt = ds['vivt'][loc]

    # plus
    def february_days(year):
        if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
            return 29
        else:
            return 28
    months_daynum = []
    for i_year in range(year_begin, year_end+1):
        months_daynum.append((31+31+february_days(i_year))*4)
    print(months_daynum)
    vivt[vivt<0] = np.nan

    for ivivt in range(len(vivt)):
        vivt[ivivt] = vivt[ivivt]*months_daynum[ivivt]

    ivt_entry_arctic = np.nansum(vivt[:,46, :], axis=1)
    print(ivt_entry_arctic)
    avg = avg
    print(avg)
    ratio_yearly = list(avg/ivt_entry_arctic)
    ratio_yearly.extend([np.nansum(avg)/np.nansum(ivt_entry_arctic)])
    df = pd.DataFrame(index=list(np.arange(year_begin, year_end+1))+['total'])

    df['moisture_into_Arctic_ratio'] = ratio_yearly
    df.to_csv(path_SaveData+'moisture_into_Arctic_ratio.csv')


    """
    ===========================      create  freq density   ==========================
    """


    def load_ivt_total(s_time):
        '''
        :param s_time: datetime64
        :return: ivt_total
        '''
        s_time = pd.to_datetime(s_time)

        s_time_str = s_time.strftime('%Y%m%d')
        index_vivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
                s_time_str[:4], s_time_str))
        index_uivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
                s_time_str[:4], s_time_str))
        # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
        # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

        ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
        ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

        ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
        return ivt_total

    density = np.zeros([year_end - year_begin + 1, len(lat), len(long)])
    avg_freq = np.zeros(density.shape)
    ivt_all = np.zeros(density.shape)

    for i_file in arType_file:
        ################# !!!!!!!!!!!!!!!!!!! ##############

        info_AR_row = xr.open_dataset(path_contour + i_file)
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        time_use = int(len(time_all[AR_loc:]) / 2)

        contour_all = np.zeros([len(lat), len(long)])

        for i_time in time_AR:
            year_loc = int(pd.to_datetime(i_time).strftime('%Y')) - year_begin
            if pd.to_datetime(i_time).strftime('%m') == '12':
                year_loc = year_loc + 1
            if year_loc == year_end - year_begin + 1:
                continue
            # sum for all year

            contour = info_AR_row['contour_all'].sel(time_all=i_time).values

            ivt = load_ivt_total(pd.to_datetime(i_time))
            ivt[contour < 0.5] = 0
            ivt_all[year_loc] += ivt

            density[year_loc] += contour

            contour_all += contour
        contour_all[contour_all > 0] = 1
        if year_loc == year_end - year_begin + 1:
            continue
        avg_freq[year_loc] += contour_all

    import scipy.io as scio

    ivt_all = ivt_all / density
    ivt_all[np.isnan(ivt_all)] = 0
    scio.savemat(path_SaveData + '%s_ivt_ar_yearly.mat' % arType_name,
                 {'ar_type': np.array(density)})
    # scio.savemat(path_SaveData + '%s_freq_ar_yearly.mat' % arType_name,
    #              {'ar_type': np.array(avg_freq)})
    # scio.savemat(path_SaveData + '%s_density_ar_yearly.mat' % arType_name,
    #              {'ar_type': np.array(density)})

    def plot_columns(df_row):
        # x = df_row.values  # returns a numpy array
        # min_max_scaler = preprocessing.MinMaxScaler()
        # x_scaled = min_max_scaler.fit_transform(x)
        # df = pd.DataFrame(x_scaled, columns=df_row.columns, index=period)

        df = df_row.copy()
        # df['year'] = np.arange(1979,2020+1)

        lens = 0
        fig, ax = plt.subplots()
        colors = ['#FF0000', '#00FF00', '#0000FF', '#FFA500', '#9400D3', '#FFC0CB', '#00FFFF', '#FFFF00']

        for i_columns in df.columns:
            # plot raw line
            ax.plot(df.index, df[i_columns], '-', label=i_columns, color=colors[lens], linewidth=0.3)

            # fit 1979-2020

            from scipy.stats import linregress

            notnan_loc = np.argwhere(~np.isnan(np.array(df[i_columns])))
            x = np.array(df.index)[notnan_loc].flatten()
            y = np.array(df[i_columns])[notnan_loc].flatten()
            print(x, y)
            slope, intercept, r_value, p_value, std_err = linregress(x, y)
            f = x * slope + intercept
            if p_value < 0.1:
                ax.plot(x, f, '-', color=colors[lens])
            else:
                ax.plot(x, f, '-.', color=colors[lens], linewidth=0.8)

            lens += 1
        plt.legend()


    def cal_area(lon, lat):
        import math
        lon, lat = np.meshgrid(lon, lat)

        R = 6378.137
        dlon = lon[0, 1] - lon[0, 0]
        delta = dlon * math.pi / 180.
        S = np.zeros(lon.shape)
        Dx = np.zeros(lon.shape)
        Dy = np.ones(lon.shape)

        Dx = R * np.cos(lat * math.pi / 180.) * delta
        Dy = Dy * delta * R

        S = Dx * Dy
        return S


    def area_weighted_mean(data, area_weight, maskvalue=''):

        if not isinstance(maskvalue, str):
            area_weight[data == maskvalue] = np.nan
        area_weight[np.isnan(data)] = np.nan

        area_weight = area_weight / np.nansum(area_weight)
        avg_AR = np.nansum(data * area_weight)

        return avg_AR


    data_row = scio.loadmat(param_var[var][1][0])[param_var[var][1][1]]
    if param_var[var][2]:
        if ar_type:
            data_row = data_row[:, 1:-1, :]  # cut 1979, 2020
        else:
            data_row = data_row[1:-1, :, :]  # cut 1979, 2020

    weight = cal_area(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5))

    """
        ===========================      affected area/duration    ==========================
        """
    df_aa_Du = pd.DataFrame()

    result = []
    for time_file in arType_file:
        def contour_cal(time_file):

            info_AR_row = xr.open_dataset(path_contour + time_file)

            time_AR = info_AR_row.time_AR.values
            time_all = info_AR_row.time_all.values
            AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))

            def area_of_AR(contour):
                """
                :param contour:
                :return: the ratio of the maximum area compared with the Arctic
                """
                contour = contour[:-1]
                area_weight = scio.loadmat(path_GlobalArea + 'GridAL_ERA0_5degGridC.mat')['S']

                # mask exceed the Arctic
                lat = int(30 / resolution)
                contour[lat:, :] = 0
                area_weight[lat:, ] = 0
                area_weight_total = np.sum(area_weight)

                # mask exceed contour
                area_weight[contour == 0] = 0
                area_weight = np.sum(area_weight)
                return area_weight / area_weight_total


            avg = []
            for i_contour in info_AR_row['contour_all'].values:
                s_avg = area_of_AR(i_contour)
                avg.append(s_avg)
            avg = np.max(avg)

            ar_all = len(time_all)

            result = [avg*10, AR_loc/4, len(time_AR)/4, (ar_all-AR_loc-len(time_AR))/4, ar_all/4]

            return result


        s_mean = contour_cal(time_file)
        result.append(s_mean)

    result = np.array(result)
    for iI_columns, i_columns in enumerate(['affected area', 'duration Lag-', 'duration AR',
                                            'duration Lag+', 'duration']):
        df_aa_Du[i_columns] = result[:, iI_columns]

    df_aa_Du.boxplot()
    plt.title(arType_name)
    plt.savefig(path_SaveData+'duration_aa_%s.png' % (arType_name), dpi=400)
    plt.close()
    df_aa_Du.to_csv(path_SaveData + 'duration_aa_%s.csv' % (arType_name))


    """
    ===========================      PDF    ==========================
    """

    def load_ivt_total(s_time):
        '''
        :param s_time: datetime64
        :return: ivt_total
        '''
        s_time = pd.to_datetime(s_time)

        s_time_str = s_time.strftime('%Y%m%d')
        index_vivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
                s_time_str[:4], s_time_str))
        index_uivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
                s_time_str[:4], s_time_str))

        ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
        ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

        ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
        return ivt_total

    def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='anomaly'):
        '''date containing Hour information'''
        time = pd.to_datetime(s_time)

        if var_dir=='':
            var_dir=var
        # load hourly anomaly data
        data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                           time.strftime('%m%d')][int(time.strftime('%H')), :, :]

        # load present data
        data_present = xr.open_dataset(
            path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
        data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

        if types == 'anomaly':
            return data_present - data_anomaly
        else:
            return [data_present, data_anomaly]
    def load_esiv(s_time, contour):
        def query_in(contour, tree):
            # ================================================================================================ #
            contour = np.array(contour, dtype='uint8')
            thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
            contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
            from shapely.geometry import Polygon
            res = []
            for i_contour in contours:
                i_contour = np.squeeze(i_contour)
                if len(i_contour) < 5:
                    continue
                # trans to row
                polygon = Polygon(i_contour)

                # print('hi')
                # import matplotlib.pyplot as plt
                # # plt.plot(*polygon.exterior.xy)
                # plt.plot(*polygon.exterior.xy)
                # plt.show()
                res_now = tree.query(polygon)
                res.extend(res_now.tolist())
            return res

            # cv2.imshow('now', bitwiseand)
            # cv2.waitKey(0)
            # plt.show()

        s_time = pd.to_datetime(s_time)
        period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
        loc = period.day_of_year-1
        if loc >= 365:
            loc = 364
        index = query_in(contour, tree)

        pioms_ai = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
                       0, loc]
        pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
                       0, loc]
        pioms_ah = np.array(pioms_hi,dtype='double')*np.array(pioms_ai,dtype='double')*100
        pioms_ah_ano = pioms_ah-ah_anomaly[loc]

        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano*weighted_PIOMS_contour


    """prepare for esiv, load pioms grid and trans into stree"""
    import cv2

    # load pioms grid
    ai_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/aiday_1979_2020')
    hi_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/hiday_1979_2020')
    ah_anomaly = ai_anomaly*hi_anomaly*100
    pioms_grid = xr.open_dataset(path_PIOMS + r"aiday.H1985.nc")
    points_lon = pioms_grid.x.values
    points_lat = pioms_grid.y.values
    from function_shared_Main import cal_area_differentLL_1d

    weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)

    # transform into 0,1,2,3 relative coordination, only works in north hemisphere
    # !!!!!!!!!!!!!!!!!!!!!!!#
    points_lat_relaCoord = (90 - points_lat) / resolution
    points_lon_relaCoord = points_lon / resolution

    # to Stree
    from shapely.strtree import STRtree
    from shapely.geometry import Point

    points = [Point(x, y) for x, y in zip(points_lon_relaCoord, points_lat_relaCoord)]
    tree = STRtree(points)  # create a 'database' of points

    for var in ['IVT', 'tcwv', 't2m', 'sic', 'esiv']:
        phase1 = []
        phase2 = []
        phase3 = []
        phase4 = []
        phase5 = []
        for time_file in arType_file:
            def contour_cal(time_file, var='IVT'):

                info_AR_row = xr.open_dataset(path_contour + time_file)

                time_AR = info_AR_row.time_AR.values
                time_all = info_AR_row.time_all.values
                AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))

                time_use = [time_all[0],
                            time_all[int(AR_loc/2)],
                            time_all[AR_loc],
                            time_all[int(AR_loc+(len(time_all)-AR_loc)/2)],
                            time_all[-1]]
                if var == 't2m':
                    var_all = []
                    for i_time in time_use:
                        s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                                '2m_temperature',
                                                                't2m')
                        contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                        contour[int(30 / resolution), :] = 0
                        s_i[contour != 1] = np.nan
                        var_all.append(s_i[~np.isnan(s_i)].flatten())


                elif var == 'IVT':
                    var_all = []
                    for i_time in time_use:
                        s_i = load_ivt_total(pd.to_datetime(i_time))
                        contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                        contour[int(30 / resolution), :] = 0
                        s_i[contour != 1] = np.nan
                        var_all.append(s_i[~np.isnan(s_i)].flatten())

                elif var == 'tcwv':
                    var_all = []
                    for i_time in time_use:
                        s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                                'WaterVapor',
                                                                 'tcwv', var_dir='Total_column_water_vapour')
                        contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                        contour[int(30 / resolution), :] = 0
                        s_i[contour != 1] = np.nan
                        var_all.append(s_i[~np.isnan(s_i)].flatten())

                elif var == 'sic':
                    var_all = []
                    for i_time in time_use:
                        s_i_present, s_i_climatly = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                                 'Sea_Ice_Cover',
                                                                 'siconc', types='now')
                        from function_shared_Main import cal_area
                        weight = cal_area(info_AR_row.lon.values, info_AR_row.lat.values)
                        s_i_present = s_i_present*weight
                        s_i_climatly = s_i_climatly*weight
                        s_i = s_i_present-s_i_climatly

                        contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                        contour[int(30 / resolution), :] = 0
                        s_i[contour != 1] = np.nan
                        var_all.append(s_i[~np.isnan(s_i)].flatten())
                elif var == 'esiv':
                    var_all = []
                    for i_time in time_use:
                        contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                        s_i_present = load_esiv(i_time, contour)

                        var_all.append(s_i_present)
                return var_all

            s_mean = contour_cal(time_file, var)
            phase1.extend(s_mean[0])
            phase2.extend(s_mean[1])
            phase3.extend(s_mean[2])
            phase4.extend(s_mean[3])
            phase5.extend(s_mean[4])
        phase = [phase1, phase2, phase3, phase4, phase5]
        for iI_phase, i_phase in enumerate(phase):
            nc4.save_nc4(np.array(i_phase),
                         path_SaveData + 'pdf_%s_phase%i' % (arType_name+'_'+var, iI_phase))



    SMALL_SIZE = 7
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

    infig_fontsize = 5

    plot_param = {
                  'IVT':[['IVTmean=\n', ' kg/(m*s)'],'IVT',[-10,360], [0,0.03],0,1],
                  'tcwv':[['TCWVmean=\n', ' kg/(m**2)'],'TCWVano',[-1.5,12], [0,0.4],1,1,],
                  't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-1, 18], [0, 0.25], 2, 1],
                    'sic':[['SICmean=\n', ''],'SICano',[-10,5], [0,0.5],2,100],
                    'esiv': [['SIVmean=\n', 'cm'], 'SIVano', [-10, 5], [0, 0.5], 2, 100]
                  }

    for var in ['IVT', 'tcwv', 't2m', 'sic', 'esiv']:

        def plots(ax, data):
            percentile_label = ['Q1', 'Q2', 'Q3']
            percentile = np.percentile(data, [25,50,75])
            mean = np.nanmean(data)
            HIST_BINS = np.linspace(np.min(data), np.max(data), 100)


            _, _, bar_container = ax.hist(data,bins=100, density=True, lw=1,
                                      ec="black", fc="gray",zorder=2)

            trans = ax.get_xaxis_transform()
            for i_percentile in range(3):
                ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
                ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
                        transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
                        bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
                )
                if plot_param[var][4] == 0:
                    values =percentile[i_percentile].astype('int')
                else:
                    values = np.round(percentile[i_percentile], plot_param[var][4])

                ax.text(percentile[i_percentile], 1.01, values, transform=trans,
                        horizontalalignment='center', fontsize=infig_fontsize)

            return mean, [ax.get_xlim()[0], ax.get_xlim()[1]]


        fig, axs = plt.subplots(5, 1, figsize=[4, 5])
        plt.subplots_adjust(top=0.91,
    bottom=0.04,
    left=0.125,
    right=0.98,
    hspace=0.4,
    wspace=0.2)
        lim = np.zeros([5, 3])
        for i in range(5):
            data = nc4.read_nc4(path_SaveData + 'pdf_%s_phase%i' % (arType_name+'_'+var, i))
            ax = axs[i]
            mean,  lim[i,:2]= plots(ax, data)

            if var != 'IVT':
                ax.axvline(0, color='black',zorder=0)

            mean_label = plot_param[var][0]
            if plot_param[var][4] == 0:
                mean = int(mean)
            else:
                mean = np.round(mean, plot_param[var][4])
            lim[i,2] = mean
            if var != 'sic':
                x = 0.85

            else:
                x=0.15

            ax.text(x, 0.6, mean_label[0]+str(mean)+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)

        maxmean = np.max(lim[:, 2])
        maxx = np.max(lim[:, 1])
        maxleft = np.min(lim[:, 0])
        if maxmean>=0:
            maxx = np.min([maxmean*4, maxx])
        else:
            maxmean = np.min(lim[:, 2])
            maxleft = np.max([maxmean*4, maxleft])

        for i in range(5):
            axs[i].set_xlim(right=maxx, left=maxleft)
            if var == 'sic':
                axs[i].set_xlim(-200, 200)
        axs[0].set_title(arType_name)
        plt.savefig(path_SaveData + 'pdf_%s.png' % (arType_name+'_'+var), dpi=300)
        plt.close()
    continue

    """
    ===================== HGT =========================================
    """

    import scipy.io as scio
    import numpy as np


    def contour_cal(time_file):
        info_AR_row = xr.open_dataset(path_contour + time_file)
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        time_use = [time_all[0],
                    time_all[int(AR_loc / 2)],
                    time_all[AR_loc],
                    time_all[int(AR_loc + (len(time_all) - AR_loc) / 2)],
                    time_all[-1]]

        cross = []
        for i_time in time_use:
            i_time = pd.to_datetime(i_time)

            def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, level=[1000, 850, 500, 200],
                                                            type='daily'):
                """date containing Hour information
                type: daily/monthly/monthly_now
                """
                import scipy
                time = pd.to_datetime(date)

                # load hourly anomaly data
                def load_anomaly(type):
                    if type == 'daily':
                        data_anomaly = \
                        scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                            time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
                    elif type == 'monthly':
                        data_anomaly = \
                        scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m'))[
                            time.strftime('%m')][:, int(time.strftime('%H')), :, :]
                    elif type == 'monthly_now':

                        def monthly_mean(var_dir, var_save, var_ShortName, time):
                            path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir

                            if os.path.exists(path_SaveData + '%s.mat' % time.strftime('%Y%m')):
                                return scio.loadmat(path_SaveData + '%s.mat' % time.strftime('%Y%m'))[time.strftime('%Y%m')]

                            else:
                                date_montly_now = pd.date_range(time.strftime('%Y-%m-') + '01', freq='1D', periods=31)
                                dates = date_montly_now[date_montly_now.month == int(time.strftime('%m'))].strftime(
                                    '%Y%m%d')
                                year = dates[0][:4]

                                def get_data(var_dir, i_Year, var_save, i_MonDay):
                                    s_data = xr.open_dataset(
                                        path_PressureData + '%s/%s/%s.%s%s.nc' % (
                                        var_dir, i_Year, var_save, i_Year, i_MonDay))
                                    s_data = s_data.sel(level=level)
                                    s_data = s_data[var_ShortName].values
                                    return np.nanmean(s_data, axis=0)

                                data_in_MonDay = []

                                for iI_MonDay, i_MonDay in enumerate(dates):
                                    # work for 0229
                                    s_data = get_data(var_dir, year, var_save, i_MonDay[4:])
                                    data_in_MonDay.append(s_data)

                                data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
                                data_in_MonDay = np.array(data_in_MonDay, dtype='double')

                                path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir
                                os.makedirs(path_SaveData, exist_ok=True)
                                scipy.io.savemat(path_SaveData + '%s.mat' % time.strftime('%Y%m'),
                                                 {time.strftime('%Y%m'): data_in_MonDay})

                                return data_in_MonDay

                        data_anomaly = monthly_mean('Hgt', 'Hgt', 'z', time)

                    return data_anomaly

                data_anomaly = load_anomaly(type)

                # load present data
                data_present = xr.open_dataset(
                    path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
                    var, time.strftime('%Y%m%d')))
                data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

                return [data_present, data_anomaly]

            cross.append(get_circulation_data_based_on_pressure_date(i_time, 'Hgt', 'z'))

        return np.array(cross)


    composite_result_z = []
    composite_result_z_ano = []

    for time_file in arType_file:

        a_all = contour_cal(time_file)
        print(a_all.shape)
        composite_result_z.append(a_all[:,0,:])
        composite_result_z_ano.append(a_all[:,1,:])

    composite_result_z = np.array(composite_result_z, dtype='double')
    composite_result_z = np.nanmean(composite_result_z, axis=0)
    composite_result_z_ano = np.array(composite_result_z_ano, dtype='double')
    composite_result_z_ano = np.nanmean(composite_result_z_ano, axis=0)

    scio.savemat(path_SaveData + 'composite_%s_global.mat' % (arType_name),
                 {'data': [composite_result_z, composite_result_z_ano]})




    import cartopy.crs as ccrs
    import cartopy.feature as cfeature
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    import xarray as xr
    import matplotlib
    SMALL_SIZE = 6
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
    plt.rcParams['hatch.color'] = 'darkred'
    import scipy.io as scio


    hgt_levels = [1000, 850,500,200]
    data= scio.loadmat(path_SaveData + 'composite_%s_global.mat' % (arType_name))['data']
    print(data.shape)
    # hgt_now, 5*4*lat*lon,1000,850,500,200
    # hgt_climately, 5*4*lat*lon,1000,850,500,200

    for i_level in range(4):
        fig, axs = plt.subplots(5,1,subplot_kw={'projection': ccrs.Orthographic(central_ll[0],central_ll[1])}
                               ,figsize=[2,8])
        # plt.subplots_adjust(top=0.84,bottom=0.08,left=0.035,right=0.99,hspace=0.2,wspace=0.2)

        for i_phase in range(5):
            ax = axs[i_phase]
            # i_contour_cut, z, z_ano= data
            z=data[0, i_phase, i_level]
            z_ano = data[1, i_phase, i_level]

            import cmaps
            import matplotlib.colors as colors
            cb = ax.contourf(long, lat,
                             np.array((z-z_ano),dtype='double')/98,cmap=cmaps.cmp_b2r,
                            transform=ccrs.PlateCarree(), extend='both', norm=colors.CenteredNorm())

            plt.colorbar(cb, ax=ax)

            ax.set_title(arType_name+' %i phase%i'%(hgt_levels[i_level], i_phase))
            ax.coastlines(zorder=9)
            # ax.gridlines(draw_labels=True)
            # ax.set_extent()

        plt.savefig(path_SaveData+
                    arType_name+'Hgt_%i.png'%(hgt_levels[i_level]), dpi=400)
        plt.close()






