import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr


budget_load = ['IVT','t2m','heff',
               'sic','tcwv',
               'str', 'sshf', 'slhf', 'strd',
               'cbh','tcc','hcc','mcc','lcc']
figlabels_diffVar = dict(zip(budget_load, [
    ['(a)', '(c)'], ['(b)', '(d)'],['(e)', '(g)'],
    ['(f)', '(h)'],['(a)', '(c)'],
['(a)', '(d)'], ['(b)', '(e)'],['(c)', '(f)'],['(c)', '(f)'],
['(a)', '(d)'], ['(b)', '(e)'],['(a)', '(d)'], ['(b)', '(e)'],['(c)', '(f)'],
]
                             ))
plus = dict(zip(budget_load, [
        1,1,1,
        1,1,
        1 / (6 * 3600),1 / (6 * 3600),1 / (6 * 3600),1 / (6 * 3600),
        1/1000,1,1,1,1,]))

vrange = dict(zip(budget_load, [
        np.arange(-100,101,10),np.arange(-10,10.01,0.5),np.arange(-100,100.01,10),
        np.arange(-100,100.01,10),np.arange(-2,2.01,0.1),
        np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),np.arange(-6,6.01,0.5),
        np.arange(-2,2.01,0.1),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01),np.arange(-0.18,0.181,0.01)]))

unit = dict(zip(budget_load, [
        'kg/(m*s)','K','cm',
        '%','kg $\mathrm{m^{-2}}$',
        'W/$m^{2}$','W/$m^{2}$','W/$m^{2}$','W/$m^{2}$',
        'km','','','','',]))

central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]
# central_lonlat_all = [[30,80],[180,70]]
SMALL_SIZE = 6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])


# def plot_pcolormeshP_onevar(data, p_values, vartype, lon,lat):
#     fig = plt.figure(figsize=[8, 5.5])
#     plt.subplots_adjust(top=0.98,
# bottom=0.07,
# left=0.02,
# right=0.98,
# hspace=0.0,
# wspace=0.2)
#
#     for i_i, i in enumerate([1,2,3,4,5,6]):
#         central_lonlat = central_lonlat_all[i_i]
#         ax = fig.add_subplot(2, 3, i,
#                              projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
#         # ax = fig.add_subplot(2, 3, i,
#         #                      projection=ccrs.NorthPolarStereo())
#
#         ax.set_title('%s anomaly for Type%s' %(vartype, i))
#
#         import cmaps
#         import matplotlib.colors as colors
#
#         # cb = ax.pcolormesh(lon, lat, data[i_i],
#         #                    cmap=cmap_use,
#         #                    transform=ccrs.PlateCarree(), vmin=-10, vmax=10)
#         p_pass = np.where(p_values[i_i] <= 0.1)
#         if (var != 'esiv') & (var != 'heff')& (var != 'sic'):
#             if var == 'IVT':
#                 if i == 2:
#                     cb = ax.contourf(lon[:120], lat[:120],
#                                      np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype])[:120],
#                                        cmap=cmap_use,levels=np.arange(-50,51, 5),
#                                        transform=ccrs.PlateCarree(), extend='both')
#                 else:
#                     cb = ax.contourf(lon[:120], lat[:120],
#                                      np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype])[:120],
#                                        cmap=cmap_use,levels=vrange[vartype],
#                                        transform=ccrs.PlateCarree(), extend='both')
#                 cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)
#
#             else:
#                 ax.contourf(lon[:120], lat[:120], np.ma.masked_greater(p[i_i][:120], 0.1),
#                                 colors='none',levels=[0,0.1],
#                                 hatches=[3*'.',3*'.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
#                 cb = ax.contourf(lon[:120], lat[:120],
#                                  data[i_i][:120]*plus[vartype],
#                                    cmap=cmap_use,levels=vrange[vartype],
#                                    transform=ccrs.PlateCarree(), extend='both')
#
#         else:
#             cb = ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
#                       transform=ccrs.PlateCarree(), zorder=20)
#
#             lons = np.reshape(lon, (120, 360))
#             lats = np.reshape(lat,(120,360))
#             cb = ax.pcolormesh(lons, lats,
#                              np.reshape(data[i_i],(120, 360)),
#                                cmap=cmap_use,
#                                transform=ccrs.PlateCarree(),vmin=-100, vmax=100)
#
#             # cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)
#         # cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
#         #                    cmap=cmap_use,
#         #                    transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))
#
#         # ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
#         #           transform=ccrs.PlateCarree(), zorder=20)
#
#         ax.coastlines(linewidth=0.3, zorder=10)
#
#         ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
#         # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
#         ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
#                       ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
#         if i == 2:
#             ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
#                           ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))
#     if vartype != 'IVT':
#         cb_ax = fig.add_axes([0.15, 0.06, 0.7, 0.01])
#         cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#         cb_ax.set_xlabel(unit[vartype])
#
#     # plt.show()
#     # save_pic_path = path_MainData + 'pic_combine_result/'
#     # os.makedirs(save_pic_path, exist_ok=True)
#     plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var), dpi=400)
#     plt.close()

def plot_pcolormeshP_onevar(data, p_values, vartype, lon,lat):
    fig = plt.figure(figsize=[2.5, 4])
    plt.subplots_adjust(top=0.98,
bottom=0.1,
left=0.02,
right=0.98,
hspace=0.08,
wspace=0.2)

    for i_i, i in enumerate([1,6]):
        central_lonlat = central_lonlat_all[i-1]
        ax = fig.add_subplot(2, 1, i_i+1,
                             projection=ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        # ax = fig.add_subplot(2, 3, i,
        #                      projection=ccrs.NorthPolarStereo())
        if vartype == 'heff':
            ax.set_title('%s SIH anomaly for SOM%s' % (figlabels_diffVar[vartype][i_i], i))
        else:
            ax.set_title("%s %s' for SOM%s" %(figlabels_diffVar[vartype][i_i], vartype.upper(), i))

        import cmaps
        import matplotlib.colors as colors

        # cb = ax.pcolormesh(lon, lat, data[i_i],
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree(), vmin=-10, vmax=10)
        p_pass = np.where(p_values[i-1] <= 0.1)
        if (var != 'esiv') & (var != 'heff')& (var != 'sic'):

            ax.contourf(lon[:120], lat[:120], np.ma.masked_greater(p[i-1][:120], 0.1),
                            colors='none',levels=[0,0.1],
                            hatches=[3*'.',3*'.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
            cb = ax.contourf(lon[:120], lat[:120],
                             data[i-1][:120]*plus[vartype],
                               cmap=cmap_use,levels=vrange[vartype],
                               transform=ccrs.PlateCarree(), extend='both')

        else:
            cb = ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
                      transform=ccrs.PlateCarree(), zorder=20)

            lons = np.reshape(lon, (120, 360))
            lats = np.reshape(lat,(120,360))
            cb = ax.pcolormesh(lons, lats,
                             np.reshape(data[i-1],(120, 360)),
                               cmap=cmap_use,
                               transform=ccrs.PlateCarree(),vmin=-100, vmax=100)

            # cbar = plt.colorbar(cb, orientation="horizontal", ax=ax)
        # cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_values[i_i]> 0.1, data[i_i]*plus[vartype]),
        #                    cmap=cmap_use,
        #                    transform=ccrs.PlateCarree(),vmin=vrange[vartype],vmax=abs(vrange[vartype]))

        # ax.scatter(lon[p_pass].flatten(), lat[p_pass].flatten(),s=0.1,c='black',marker='*',linewidth=0.1,
        #           transform=ccrs.PlateCarree(), zorder=20)
        boundary = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM6.mat')
        # plt.imshow(np.array(boundary['boundary']))
        # plt.show()
        boundary_index = np.where(np.array(boundary['boundary'], dtype='single')>3)
        ax.scatter(boundary['lon'][boundary_index], boundary['lat'][boundary_index],
                   transform=ccrs.PlateCarree(),
                   zorder=50,
                   color='black', s=0.1)
        ax.coastlines(linewidth=0.3, zorder=10)

        ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
        # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
        ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
                      ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))
        if i == 2:
            ax.set_extent([-2e+06, 2e+06, -3e+06, 1e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))

    cb_ax = fig.add_axes([0.1, 0.075, 0.8, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cb_ax.set_xlabel(unit[vartype])

    # plt.show()
    # save_pic_path = path_MainData + 'pic_combine_result/'
    # os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var), dpi=400)
    plt.close()


for var in ['tcwv', 't2m']:
    print(var)
    ar = []
    p = []
    for i_type in range(1,6+1):
        ar.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_pattern' % (i_type, var)))
        p.append(nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot/pdf_type%i_%s_P' % (i_type, var)))

    ar = np.array(ar)
    p = np.array(p)
    s_figlabels_diffVar = figlabels_diffVar[var]
    lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
    if (var != 'esiv') & (var != 'heff')& (var != 'sic'):
        plot_pcolormeshP_onevar(ar, p, var, lon_2d, lat_2d)
    else:
        xy = xr.open_dataset(r"D:\aiday.H1979.nc")
        plot_pcolormeshP_onevar(ar, p, var, xy.x.values, xy.y.values)

exit()

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4
import scipy.io as scio
# def load_ivt_total(s_time):
#     '''
#     :param s_time: datetime64
#     :return: ivt_total
#     '''
#     s_time = pd.to_datetime(s_time)
#
#     s_time_str = s_time.strftime('%Y%m%d')
#     index_vivt = xr.open_dataset(
#         path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
#             s_time_str[:4], s_time_str))
#     index_uivt = xr.open_dataset(
#         path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
#             s_time_str[:4], s_time_str))
#     # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
#     # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))
#
#     ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
#     ivt_east = index_uivt.sel(time=s_time)['p71.162'].values
#
#     ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
#     return ivt_total
#
# # df = pd.read_csv(r'/home/linhaozhong/work/AR_NP85/new_Clustering/type_filename_0.2.csv')
# main_path = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_all/'
#
# info_AR_row = xr.open_dataset(main_path + r"2008122711_1.nc")
# time_AR = info_AR_row.time_AR.values
# time_all = info_AR_row.time_all.values
# AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
# time_use = int(len(time_all[AR_loc:]) / 2)
#
# ivt_singleAR = np.zeros([361, 720])
# lens = 0
# for i_time in time_AR:
#
#     # sum for all year
#
#     contour = info_AR_row['contour_all'].sel(time_all=i_time).values
#     ivt = load_ivt_total(pd.to_datetime(i_time))
#     ivt[contour < 0.5] = np.nan
#     fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})
#     cb = plt.pcolormesh(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5)[:140], ivt[:140], transform=ccrs.PlateCarree())
#     print(np.nanmean(ivt))
#     plt.colorbar(cb)
#     plt.savefig(r'/home/linhaozhong/work/AR_NP85/new_Clustering/test/%i.png'%lens, dpi=200)
#     lens += 1
#     plt.close()
#
# #     ivt = load_ivt_total(pd.to_datetime(i_time))
# #     ivt[contour < 0.5] = 0
# #     ivt_singleAR += ivt
# # ivt_singleAR[ivt_singleAR == 0] = np.nan
# # print(np.nansum(ivt_singleAR))
# exit()

SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.cmocean_balance

for var in ['t2m', 'sic']:
    for arType_name in np.arange(1,7):
        arType_name =str(arType_name)
        data = nc4.read_nc4(r'/home/linhaozhong/work/AR_NP85/Fig2/AR/AR%s_%s_monteDiv' % (arType_name,var))
        fig = plt.figure(figsize=[4,4])
        plt.subplots_adjust(top=0.97,
        bottom=0.1,
        left=0.005,
        right=0.995,
        hspace=0.14,
        wspace=0.055)

        ax = fig.add_subplot(1,1,1, projection=ccrs.NorthPolarStereo())

        ax.set_title('%s anomaly for Type%s'%(var, arType_name))


        import cmaps
        import matplotlib.colors as colors

        cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:140], data[:140],
                         cmap=cmap_use,vmin=-5,vmax=5,
                    transform=ccrs.PlateCarree())

        ax.coastlines(linewidth=0.3,zorder=10)
        # ax.stock_img()
        ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
        # plt.show()
        ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
        cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
        cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)

        cb_ax.set_xlabel('K')
        # plt.show()
        # save_pic_path = path_MainData + 'pic_combine_result/'
        # os.makedirs(save_pic_path, exist_ok=True)
        plt.savefig(r'/home/linhaozhong/work/AR_NP85/Fig2/AR/AR%s_%s_monteDiv.png' % (arType_name,var), dpi=400)
        plt.close()

exit(0)