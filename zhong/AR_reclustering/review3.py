import numpy as np
import pandas as pd
import xarray as xr
import nc4
import scipy.io as scio
from datetime import timedelta

def load_ivt_total(s_time, types='climate'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]
    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    ivt_climate = np.sqrt(index_uivt_anomaly ** 2 + index_vivt_anomaly ** 2)
    if types == 'climate':
        return ivt_total - ivt_climate
    else:
        # return [ivt_total, ivt_climate]
        return np.sqrt((ivt_east-index_uivt_anomaly) ** 2 + (ivt_north-index_vivt_anomaly) ** 2)


def load_ivt_u(s_time, types='climate'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_uivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    if types == 'climate':
        return ivt_east - index_uivt_anomaly
    else:
        return [ivt_east, index_uivt_anomaly]


def load_ivt_v(s_time, types='climate'):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    index_vivt_anomaly = scio.loadmat(path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/anomaly/' +
                                      '%s.mat' % s_time.strftime('%m%d'))[
                             s_time.strftime('%m%d')][int(s_time.strftime('%H')), :, :]

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values

    if types == 'climate':
        return ivt_north - index_vivt_anomaly
    else:
        return [ivt_north, index_vivt_anomaly]


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='climate'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    if types == 'climate':
        return data_present - data_anomaly
    else:
        return [data_present, data_anomaly]


def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, levels=21):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    # load hourly anomaly data
    def load_anomaly():
        data_anomaly = \
            scipy.io.loadmat(
                path_PressureData + '%s/anomaly_levels/' % var + '%s.mat' % time.strftime('%m%d'))[
                time.strftime('%m%d')][levels, int(time.strftime('%H')), :, :]
        return data_anomaly

    data_anomaly = load_anomaly()

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
            var, time.strftime('%Y%m%d')))

    data_present = data_present.isel(level=levels)
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[
        var_ShortName].values

    return data_present, data_anomaly

    # return data_present


def load_esiv(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_ai = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * np.array(pioms_ai, dtype='double') * 100
    pioms_ah_ano = pioms_ah - ah_anomaly[loc]

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index] / np.nansum(weighted_PIOMS[index])
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return np.nansum(pioms_ah_ano * weighted_PIOMS_contour)
    else:
        return [pioms_ah, ah_anomaly[loc]]


def load_heff(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"hiday.H%s.nc" % s_time.strftime('%Y'))['hiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * 100
    pioms_ah_ano = pioms_ah - (hi_anomaly[loc] * 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, hi_anomaly[loc] * 100]


def load_sic(s_time, contour):
    def query_in(contour, tree):
        # ================================================================================================ #
        contour = np.array(contour, dtype='uint8')
        thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        from shapely.geometry import Polygon
        res = []
        for i_contour in contours:
            i_contour = np.squeeze(i_contour)
            if len(i_contour) < 5:
                continue
            # trans to row
            polygon = Polygon(i_contour)

            # print('hi')
            # import matplotlib.pyplot as plt
            # # plt.plot(*polygon.exterior.xy)
            # plt.plot(*polygon.exterior.xy)
            # plt.show()
            res_now = tree.query(polygon)
            res.extend(res_now.tolist())
        return res

        # cv2.imshow('now', bitwiseand)
        # cv2.waitKey(0)
        # plt.show()

    s_time = pd.to_datetime(s_time)
    period = pd.Period(s_time.strftime('%Y-%m-%d'), freq='D')
    loc = period.day_of_year - 1
    if loc >= 365:
        loc = 364

    pioms_hi = xr.open_dataset(path_PIOMS + r"aiday.H%s.nc" % s_time.strftime('%Y'))['aiday'].values[
        0, loc]
    pioms_ah = np.array(pioms_hi, dtype='double') * 100
    pioms_ah_ano = pioms_ah - (ai_anomaly[loc] * 100)

    if plot_type == 'pdf':
        index = query_in(contour, tree)
        weighted_PIOMS_contour = weighted_PIOMS[index]
        pioms_ah_ano = pioms_ah_ano[index]
        # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
        return pioms_ah_ano * weighted_PIOMS_contour
    else:
        return [pioms_ah, ai_anomaly[loc] * 100]



""" #### prepare for esiv, load pioms grid and trans into stree"""
import cv2
resolution = 0.5
long = np.arange(0, 360, 0.5)
lat = np.arange(90, -90.01, -0.5)
bu_addition_Name = ''
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
path_PIOMS = r'/home/linhaozhong/work/Data/PIOMS_nc/'

# load pioms grid
ai_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/aiday_1979_2020')
hi_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/PIOMS_nc/anomaly/hiday_1979_2020')
ah_anomaly = ai_anomaly * hi_anomaly * 100
pioms_grid = xr.open_dataset(path_PIOMS + r"aiday.H1985.nc")
points_lon = pioms_grid.x.values
points_lat = pioms_grid.y.values
from function_shared_Main import cal_area_differentLL_1d

weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)

# transform into 0,1,2,3 relative coordination, only works in north hemisphere
# !!!!!!!!!!!!!!!!!!!!!!!#
points_lat_relaCoord = (90 - points_lat) / resolution
points_lon_relaCoord = points_lon / resolution

# to Stree
from shapely.strtree import STRtree
from shapely.geometry import Point

points = [Point(x, y) for x, y in zip(points_lon_relaCoord, points_lat_relaCoord)]
tree = STRtree(points)  # create a 'database' of points
"""prepare for esiv, load pioms grid and trans into stree #### """


path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']
resolution = 0.5
path_SaveData = r'/home/linhaozhong/work/AR_NP85/new_Clustering_paper_plot/'

var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
                'hcc': 'high_cloud_cover',
                'lcc': 'low_cloud_cover',
                'mcc': 'medium_cloud_cover',
                'tcc': 'total_cloud_cover',
                'tciw': 'total_column_cloud_ice_water',
                'tclw': 'total_column_cloud_liquid_water',

                }


def contour_cal(time_file, var='IVT'):
    info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

    time_all = info_AR_row.time_all_butAR.values
    time_AR = info_AR_row.time_AR.values

    AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
    print('AR_loc', AR_loc)

    try:
        useless = len(AR_loc)
        AR_loc = AR_loc[0]
    except:
        AR_loc = AR_loc
    print(time_all)
    print(time_AR)
    print(AR_loc)
    time_use = info_AR_row['time_all_butAR'].values[:AR_loc+1]
    # time_use = info_AR_row['time_AR'].values
    print(time_use)
    var_all = []

    for i_time in time_use:
        print(i_time)
        contour = info_AR_row['contour_all'].sel(time_all=i_time).values

        if var == 'IVT':
            s_i = load_ivt_total(pd.to_datetime(i_time), types='now')
        elif var == 'IVT_u':
            s_i = load_ivt_u(pd.to_datetime(i_time), types='now')
        elif var == 'IVT_v':
            s_i = load_ivt_v(pd.to_datetime(i_time), types='now')
        elif var == 'tcwv':
            s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                     'WaterVapor',
                                                     'tcwv', var_dir='Total_column_water_vapour', types='now')
        elif var == 'centroid':
            # tcwv = get_circulation_data_based_on_date(pd.to_datetime(i_time),
            #                                          'WaterVapor',
            #                                          'tcwv', var_dir='Total_column_water_vapour', types='now')
            tcwv = load_ivt_total(pd.to_datetime(i_time), types='now')

            tcwv = tcwv

            def roll_contour(contour, centroid):
                roll_lat = [-int(centroid[0] / resolution)]
                roll_lon = [-int((180 - centroid[1]) / resolution)]
                contour = np.roll(contour, roll_lat, 0)
                contour = np.roll(contour, roll_lon, 1)
                return contour

            centroid_all = info_AR_row.centroid_all.sel(time_all=i_time).values
            if trans_lon:
                if (centroid_all[1] < 180) & (centroid_all[1] > 0):
                    centroid_all[1] = centroid_all[1]+360
            s_i = [tcwv, centroid_all]

            # s_i = [roll_contour(tcwv, centroid_all), centroid_all]

        elif var == 'w':
            s_i = get_circulation_data_based_on_pressure_date(pd.to_datetime(i_time),
                                                              'Wwnd',
                                                              'w',levels
                                                              )
        elif var == 'hgt':
            s_i = get_circulation_data_based_on_pressure_date(pd.to_datetime(i_time),
                                                              'Hgt',
                                                              'z',levels
                                                              )
        elif var == 'esiv':
            s_i = load_esiv(pd.to_datetime(i_time), contour)
        elif var == 'heff':
            s_i = load_heff(pd.to_datetime(i_time), contour)
        elif var == 'sic':
            s_i = load_sic(pd.to_datetime(i_time), contour)

        else:
            s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                     var_LongName[var],
                                                     var, types='now')
        # save
        if (plot_type == 'pdf') & (var != 'esiv'):
            s_i = s_i[0] - s_i[1]
            contour[int(30 / resolution), :] = 0
            s_i[contour != 1] = np.nan
            var_all.extend(s_i[~np.isnan(s_i)].flatten())
        else:
            var_all.append(s_i)

    if (plot_type == 'pattern') & (var != 'centroid'):
        var_all = np.nanmean(np.array(var_all), axis=0)
    if var == 'centroid':
        var_all1 = np.nanmean(np.array([i[0] for i in var_all]), axis=0)
        var_all2 = np.nanmean(np.array([i[1] for i in var_all]), axis=0)
        var_all = [var_all1, var_all2]

    return var_all

"""
change in R3:
hgt500 before AR intruded the Arctic
"""
plot_type = 'pattern'  # 'pattern'/'pdf'/'pdf_weighted'
# for var in ['IVT', 't2m', 'esiv', 'tcwv', 'str', 'slhf', 'sshf', 'tcc','cbh','hcc','lcc','mcc']:
levels_all = [30]

for levels in levels_all:
    for var in ['siconc']: #centroid

        for iI, i_type in enumerate(np.unique(save_labels_bmus)):
            if not ((i_type == 1) | (i_type==6)):
                continue
            trans_lon = False
            if i_type == 1:
                trans_lon = True

            file = file_all[save_labels_bmus == i_type]

            phase1 = []

            for i_file in file:
                aa = contour_cal(i_file, var)
                if not isinstance(aa, str):
                    if plot_type == 'pdf':
                        phase1.extend(aa)
                    else:
                        phase1.append(aa)
                        print('hihi')

            if plot_type == 'pdf':
                phase = [np.array(phase1).flatten()]
                for iI_phase, i_phase in enumerate(phase):
                    nc4.save_nc4(i_phase,
                                 path_SaveData + 'R3_pdf_type%i_%s_%s' % (i_type, var, plot_type))
            else:
                """
                additional process →
                """
                if var == 'centroid':
                    phase = phase1

                    tcwv = []
                    centroid = []
                    for iI_phase, i_phase in enumerate(phase):
                        tcwv.append(i_phase[0])
                        centroid.append(i_phase[1])
                    nc4.save_nc4(np.array(tcwv),
                                 path_SaveData + 'R3_type%i_%s_%s' % (i_type, 'IVT_anotest', plot_type))
                    nc4.save_nc4(np.array(centroid),
                                 path_SaveData + 'R3_type%i_%s_%s' % (i_type, 'centroid', plot_type))

                    continue
                """
                additional process ←
                """

                phase = [np.array(phase1)]


                for iI_phase, i_phase in enumerate(phase):
                    from scipy import stats

                    _, p_values = stats.ttest_ind(i_phase[:, 0, :], i_phase[:, 1, :], axis=0)

                    nc4.save_nc4(p_values,
                                 path_SaveData + 'R3_type%i_%s_P_%i' % (i_type, var, levels))
                    i_phase = np.nanmean(np.array(i_phase), axis=0)
                    nc4.save_nc4(i_phase[0],
                                 path_SaveData + 'R3_type%i_%s_%s_ar_%i' % (i_type, var, plot_type, levels))
                    nc4.save_nc4(i_phase[1],
                                 path_SaveData + 'R3_type%i_%s_%s_cli_%i' % (i_type, var, plot_type, levels))
                    nc4.save_nc4(i_phase[0] - i_phase[1],
                                 path_SaveData + 'R3_type%i_%s_%s_%i' % (i_type, var, plot_type, levels))


exit()
