
"""
500 flow
"""

#
import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
from matplotlib.colors import ListedColormap
import matplotlib.colors as mcolors
import cmaps
cmap_use = cmaps.cmocean_balance

def prepare_data():
    """
    param: som, var_ivt_tcwv
    :return:
    """
    ar = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot\R3_type%i_siconc_pattern_30'%som)
    ar_p = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot\R3_type%i_siconc_P_30'%som)

    tcwv = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot\R3_type%i_%s_pattern'%(som, var_ivt_tcwv))
    centroid = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot\R3_type%i_centroid_pattern'%som)
    hgt = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot\R3_type%i_hgt_pattern_30'%som)

    def roll_contour(contour, centroid, back=False):
        for i in range(len(contour)):
            roll_lat = int(centroid[i][0] / 0.5)
            roll_lon = int((180 - centroid[i][1]) / 0.5)
            if back:
                roll_lat = -roll_lat
                roll_lon = -roll_lon
            contour[i] = np.roll(contour[i], [roll_lat], 0)
            contour[i] = np.roll(contour[i], [roll_lon], 1)
        return contour
    tcwv = roll_contour(tcwv, centroid)
    tcwv = np.nanmean(tcwv, axis=0)
    centroid = np.nanmean(centroid, axis=0)
    tcwv = roll_contour([tcwv],[centroid], back=True)
    tcwv = tcwv[0]

    return hgt, tcwv, centroid, ar, ar_p

central_lonlat_all = [[30,50],[150,80], [180,70],[30,80],[300,70],[180,50]]
figlabelstr = ['(a)', '(b)', '(c)', '(d)']

lon_2d, lat_2d = np.meshgrid(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5))

varplotparam = {'1_tcwv_ano':[list(np.arange(0, 0.2, 0.2))+
                              list(np.arange(0.6, 2, 0.1))+
                              list(np.arange(3, 4.01, 0.5))],
                '6_tcwv_ano':[list(np.arange(0, 0.2, 0.2))+
                              list(np.arange(0.6, 2, 0.1))+
                              list(np.arange(3, 4.01, 0.5))],
                '1_IVT':[list(range(0, 40, 5))  +list(range(40, 50, 10))+
                         list(range(50, 150, 5)) + list(range(150, 210, 10))],
                '6_IVT':[list(range(0, 40, 5))  +list(range(40, 50, 10))+
                         list(range(50, 150, 5)) + list(range(150, 210, 10))],
                }
fig = plt.figure(figsize=[5,4.5])
plt.subplots_adjust(top=1.0,
bottom=0.08,
left=0.035,
right=0.96,
hspace=0.08,
wspace=0.2)
axnum = 1
lat_range = 180
for som, var_ivt_tcwv in [[1, 'tcwv_ano'], [6, 'tcwv_ano'],[1, 'IVT'], [6, 'IVT']]:
    if axnum%2==1:
        if axnum != 1:
            cmap = ListedColormap(cmaps.GHRSST_anomaly_r.colors[4:])
            cmap.set_over(cmap.colors[-1])
        else:
            cmap = ListedColormap(cmaps.cmocean_deep.colors[0:-2])
            cmap.set_over(cmap.colors[-1])

    varplotkey = '%i_%s' % (som, var_ivt_tcwv)

    ar, tcwv, centroid, sic, sic_p = prepare_data()

    central_lonlat = central_lonlat_all[som - 1]
    ax = fig.add_subplot(2,2, axnum,
                         projection=ccrs.Orthographic(central_longitude=central_lonlat[0],
                                                      central_latitude=central_lonlat[1])
                         )
    axnum += 1

    # === colorbar for tcwv and IVT + plot contourf  $ additional above.
    norm = mcolors.BoundaryNorm(varplotparam[varplotkey][0], cmap.N)
    #   = plot contourf

    if var_ivt_tcwv == 'IVT':
        cb = ax.contourf(lon_2d[:lat_range], lat_2d[:lat_range],
                         tcwv[:lat_range], levels=varplotparam[varplotkey][0],
                         cmap=cmap, norm=norm,
                         transform=ccrs.PlateCarree(), extend='both')
        ax.contour(lon_2d[:lat_range], lat_2d[:lat_range],
                         tcwv[:lat_range], levels=[100],
               transform=ccrs.PlateCarree(),
                        colors='black', linewidths=1.2)
    print(np.nanmax(sic), np.nanmin(sic))
    sic = np.ma.masked_where(sic_p>0.1, sic)
    ax.contour(lon_2d[:lat_range], lat_2d[:lat_range],
                     sic[:lat_range],levels=np.arange(-1,0,0.1),
               transform=ccrs.PlateCarree(),
               colors='red', linewidths=0.5, linestyles='solid')
    # === hgt
    if var_ivt_tcwv == 'IVT':
        ax.contour(lon_2d[:lat_range], lat_2d[:lat_range], ar[:lat_range] /98,
                   transform=ccrs.PlateCarree(), extend='both',
                   colors='black', linewidths=0.8, alpha=0.8)
    else:
        # cb = ax.contourf(lon_2d[:lat_range], lat_2d[:lat_range],
        #                  sic[:lat_range],
        #                  cmap=cmap, norm=norm,
        #                  transform=ccrs.PlateCarree(), extend='both')
        # ax.contour(lon_2d[:lat_range], lat_2d[:lat_range],
        #                  tcwv[:lat_range],levels=[0.5,1,1.5,2,2.5],
        #            transform=ccrs.PlateCarree(), extend='both',
        #            colors='black', linewidths=0.8, alpha=0.8)
        cb = ax.contourf(lon_2d[:lat_range], lat_2d[:lat_range],
                         tcwv[:lat_range], levels=varplotparam[varplotkey][0],
                         cmap=cmap, norm=norm,
                         transform=ccrs.PlateCarree(), extend='both')

    # === centroid
    ax.scatter(centroid[1]%360, centroid[0], s=12, c='lime', transform=ccrs.PlateCarree(),zorder=999999)

    # === coastlines + ll labels + extent + title
    ax.coastlines(linewidth=0.3, zorder=10)
    gl = ax.gridlines(ylocs=[66],
                      linewidth=0.3, color='black', crs=ccrs.PlateCarree())
    print(axnum)
    if (axnum ==4)|(axnum ==2):

        ax.set_extent([central_lonlat[0]-100, central_lonlat[0]+40, 30, 90],ccrs.PlateCarree())
    else:
        ax.set_extent([central_lonlat[0]-100, central_lonlat[0]+80, 30, 90],ccrs.PlateCarree())

    ax.set_title(figlabelstr[axnum-1-1], loc='left')

    # === colorbar only works when ax = 1,3
    if axnum%2 == 0:
        if axnum == 1+1:
            cb_ax = fig.add_axes([0.15, 0.57, 0.7, 0.01])
            ticklabel = '$\mathrm{kg\,m^{-1}s^{-1}}$'
        else:
            cb_ax = fig.add_axes([0.15, 0.1, 0.7, 0.01])
            ticklabel = '$\mathrm{kg\,m^{-1}s^{-1}}$'

        cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
        cb_ax.set_xlabel(ticklabel)
        # cb_ax.set_xticks(np.arange(510,540.1,10))

plt.show()
plt.savefig('R3_figure1.png', dpi=400)
plt.show()
exit()