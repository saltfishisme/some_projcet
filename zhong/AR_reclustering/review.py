import nc4
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import scipy.io as scio
xlocs = [[['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['', 179.99, 85], ['', -120, 85],
          ['60°W', -60, 75]],
         [['0°', 0, 82], ['60°E', 60, 75], ['120°E', 120, 66], ['180°', 179.99, 66], ['120°W', -120, 75],
          ['60°W', -60, 82]],
         [['120°E', 120, 80], ['180°', 179.99, 55], ['120°W', -120, 80]],
         [['0°', 0, 66], ['60°E', 60, 66], ['120°E', 120, 75], ['180°', 179.99, 82], ['120°W', -120, 82],
          ['60°W', -60, 75]],
         [['0°', 0, 80], ['120°W', -120, 80], ['60°W', -60, 55]],
         [['120°E', 120, 75],['180°', 179.99, 55],['120°W', -120, 75]]]
# def fast_moving_average(x, y, types='gaussian'):
#     from scipy import interpolate
#     from scipy import ndimage
#     """
#     :param x:
#     :param y:
#     :param types: 'spline_xy', 'spline', 'gaussian'
#     :return:
#     """
#     # convert both to arrays
#     x_sm = np.array(x)
#     y_sm = np.array(y)
#
#     # resample to lots more points - needed for the smoothed curves
#     x_smooth = np.linspace(np.min(x_sm), np.max(x_sm), 1000)
#
#     # spline - always goes through all the data points x/y
#     if types == 'spline_xy':
#         tck = interpolate.splrep(x, y)
#         y_spline = interpolate.splev(x_smooth, tck)
#         return x_smooth, y_spline
#
#     if types == 'spline':
#         spl = interpolate.UnivariateSpline(x, y) # 'spline'
#         return x_smooth, spl(x_smooth)
#
#     if types == 'gaussian':
#         sigma = 1
#         x_g1d = ndimage.gaussian_filter1d(x_sm, sigma)
#         y_g1d = ndimage.gaussian_filter1d(y_sm, sigma)
#         return x_g1d,y_g1d
#     return
#
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
# plot_param = {'cbh': ['', 'CBH', [0, 120], 1],
#               'hcc': ['', 'HCC', [0, 120], 1],
#               'lcc': ['', 'LCC', [0, 120], 1],
#               'mcc': ['', 'MCC', [0, 120], 1],
#               'tcc': ['', 'TCC', [0, 120], 1],
#
#               'esiv': ['', 'ESIV', [0, 120], 1],
#               'heff':['', '$H_{\mathrm{eff}}$', [0, 120], 1],
#               'sic': ['', 'SIC', [0, 120], 1],
#
#               'ssr': [' W$m^{-2}$', 'SW', [0, 120], 1 / (6 * 3600)],
#               'str': [' W$m^{-2}$', 'LW', [0, 120], 1 / (6 * 3600)],
#
#               't2m': [['T2Mmean=', ' K'], 'T2M', [0, 120], 1],
#               'sshf': [['SSHFmean=', ' W$m^{-2}$'], 'SSHF', [0, 120], 1 / (6 * 3600)],
#               'slhf': [['SLHFmea=', ' W/$m^{-2}$'], 'SLHF', [0, 120], 1 / (6 * 3600)],
#               'advection': [['ADVmean=', ' K every 6h'], 'advection', [0, 120], 6 * 3600],
#               'adibatic': [['ABmean=', ' K every 6h'], 'adiabatic', [0, 120], 6 * 3600],
#               'IVT': [['IVTmean=', ' $kg\,m^{-1}s^{-1}$'], 'IVT', [0, 120], 1],
#               'tcwv': [['TCWVmean=', ' kg$m^{-2}$'], 'TCWV', [0, 120], 1, ],
#
#               'siconc': [['SICmean=', ' %'], 'SIC', [0, 120], 100],
#               }
# types = 'climate'
# use_day = [3,7]
# var_all = ['ssr', 'str']
# # var_all = ['hcc', 'mcc', 'lcc']
# fig,axs = plt.subplots(ncols=len(var_all))
# for iI_var, var in enumerate(var_all):
#     ax = axs[iI_var]
#     aar = nc4.read_nc4(
#         r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\\%s_%sDAYb10_a20_1_forzenRegion"%(types, var))
#     par = nc4.read_nc4(
#         r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\%s_%sDAYb10_a20_6_forzenRegion"%(types, var))
#
#     # aar = nc4.read_nc4(
#     #     r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\ARbegin%s_%sDAYb10_a20_1_forzenRegion"%(types, var))
#     # par = nc4.read_nc4(
#     #     r"G:\OneDrive\basis\some_projects\zhong\AR_reclustering\PDF_line_new_Clustering\ARbegin%s_%sDAYb10_a20_6_forzenRegion"%(types, var))
#
#     aar = aar[39-use_day[0]*4:39+use_day[1]*4+1]*plot_param[var][-1]
#     par = par[39-use_day[0]*4:39+use_day[1]*4+1]*plot_param[var][-1]
#
#     ar_loc = use_day[0]*4
#
#     x, y = fast_moving_average(np.arange(len(aar)), aar)
#     ax.plot(x,y, label='SOM1', color='#6baed6', marker='.',markersize=5,mfc='#eff3ff')
#     ax.scatter([ar_loc], [y[ar_loc]], color='red', s=5, zorder=12)
#
#     x, y = fast_moving_average(np.arange(len(aar)), par)
#     ax.plot(x, y, label='SOM6', color='#2171b5', marker='.',markersize=5,mfc='#bdd7e7')
#     ax.scatter([ar_loc], [y[ar_loc]], color='red', s=5, zorder=12)
#
#
#     ax.set_ylabel(plot_param[var][0])
#     ax.set_title("%s %s'"%(figlabelstr[iI_var], plot_param[var][1]), loc='left')
#
#     print(list(np.arange(16,41, 4)))
#     if len(var_all) > 2:
#         ax.set_xticks([4,12,  20, 28, 36])
#         ax.set_xticklabels(['-2d','Onset','+2d','+4d', '+6d'])
#
#     else:
#         ax.set_xticks([0,4, 8,12,16, 20, 24, 28, 32, 36, 40])
#         ax.set_xticklabels(['-3d','-2d','-1d','Onset','+1d','+2d', '+3d','+4d','+5d', '+6d', '+7d'])
#     ax.legend()
# plt.show()
# #
# # """
# # duration
# # """
# #
# fig, axs = plt.subplots(ncols=2, figsize=[6,4])
# plt.subplots_adjust(top=0.88,
# bottom=0.2,
# left=0.125,
# right=0.9,
# hspace=0.2,
# wspace=0.2)
# x = np.arange(6)  # the label locations
# x_width = [-0.3, -0.1, 0.1, 0.3]
# width = 0.2  # the width of the bars
#
# duration =np.array([[1.603658536585366, 4.670731707317073], [1.35, 3.62], [1.368421052631579, 2.8552631578947367], [1.6018518518518519, 3.5185185185185186], [1.4485294117647058, 3.1176470588235294], [1.525, 3.6375]]
#                    )
#
#
# for i_ax in range(2):
#     ax = axs[i_ax]
#     pos = np.arange(6)
#     bp = axs[i_ax].bar(pos, np.array(duration[:, i_ax]), color='black')
#     print(duration[:, i_ax])
#     ## xlabel for season
#     ax.set_xticks(pos)
#     ax.set_xticklabels(['SOM1', 'SOM2', 'SOM3', 'SOM4', 'SOM5', 'SOM6'], rotation=45)
#     ax.set_ylim([0,6])
#
# axs[0].set_ylabel('Day')
# axs[0].set_title('(a) AR duration')
# axs[1].set_title('(b) AR influence')
# ## xlabel for SOM
#
# # ax.set_ylabel(var_ylabel[iI_var])
# # # ax.set_title('')
# # ax.annotate(figlabelstr[iI_var], [0.01, 0.94], xycoords='axes fraction')
# plt.show()
# exit()
# #
# #
#
# """duration"""
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present

def main_linePhase_AR_centre():
    def contour_cal(time_file):
        info_AR_row = xr.open_dataset(path_contour + time_file[:-1])

        time_all = info_AR_row.time_all.values

        time_here = []
        time_here_loc = []
        for iI_timeall, i_timeall in enumerate(time_all):
            i_contour = info_AR_row['contour_all'].sel(time_all=i_timeall).values
            from function_shared_Main import cal_area

            weighted = cal_area(info_AR_row.lon.values, info_AR_row.lat.values)
            sic = get_circulation_data_based_on_date(pd.to_datetime(i_timeall), 'Sea_Ice_Cover', 'siconc')
            i_contour[sic < 0.01] = 0
            i_contour[np.isnan(sic)] = 0
            area_here = np.nansum(weighted[i_contour > 0.01])
            if area_here > 50 * (10 ** 4):
                time_here.append(str(i_timeall))
                time_here_loc.append(iI_timeall)


        # skip if two consecutive time points faill to meet the criteria.
        def consecutive(data):
            max_segment = []
            max_indices = []
            current_segment = []
            current_indices = []

            for index, num in enumerate(data):
                if num == 1:
                    current_segment.append(num)
                    current_indices.append(index)
                else:
                    if len(current_segment) > len(max_segment):
                        max_segment = current_segment
                        max_indices = current_indices
                    current_segment = []
                    current_indices = []

            if len(current_segment) > len(max_segment):
                max_segment = current_segment
                max_indices = current_indices

            return max_indices


        time_here_loc_div = np.diff(time_here_loc)
        time_here_loc_div[time_here_loc_div <= 2] = 1
        time_here_loc_div[time_here_loc_div > 2] = 2

        time_here_loc_div = consecutive(time_here_loc_div)
        if len(time_here_loc_div) <= 1:
            return '999'
        time_here = np.array(time_here)[time_here_loc_div + [time_here_loc_div[-1] + 1]]
        print(time_here[0])

        return info_AR_row['centroid_all'].sel(time_all=pd.to_datetime(time_here[0])).values

        #
        # time_AR = info_AR_row.time_AR.values
        # time_all = info_AR_row.time_all.values
        #
        # all_or_AR = [time_all, info_AR_row['contour_all'].values]
        # AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        # arg = [len(time_AR)/4, len(time_all[AR_loc:])/4]
        # return arg




    save_labels_bmus = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/test_BMUS.mat")['BMUS'][:, -1].flatten()
    file_all = scio.loadmat(r"/home/linhaozhong/work/AR_NP85/centroid_deinf_2000")['filename']

    all_duration = []
    for iI, i_type in enumerate(np.unique(save_labels_bmus)):
        if (i_type != 1) & (i_type != 6):
            continue
        times_file_all = file_all[save_labels_bmus == i_type]


        characteristics = []

        for time_file in times_file_all:
            if len(str(time_file)) < 5:
                continue
            s_ii = contour_cal(time_file)

            if isinstance(s_ii, str):
                continue
            characteristics.append(s_ii)
        nc4.save_nc4(np.array(characteristics), r'/home/linhaozhong/work/AR_NP85/centroidOfAR_'+str(i_type))
        # all_duration.append(np.nanmean(np.array(characteristics), axis=0))

    print(all_duration)



time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
path_contour = r'/home/linhaozhong/work/AR_NP85/AR_contour_42_reAR_all/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''

resolution = 0.5
main_linePhase_AR_centre()
exit()
# import nc4





"""
plot for pattern
"""
#
import os
import xarray as xr
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

central_lonlat_all = [[30,80],[150,80], [180,70],[30,80],[300,70],[180,70]]
# central_lonlat_all = [[30,80],[180,70]]
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl
import cartopy.crs as ccrs
mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
import cmaps
cmap_use = cmaps.MPL_rainbow
cmap_use = cmaps.cmocean_balance
from matplotlib.colors import ListedColormap
none_map = ListedColormap(['none'])

def plot_axes4(ax, data, p_values=None, lon=None,lat=None, ranges=None, plus=None):
    """
    :param axs: axs contains 1,6 axes
    :param data: [som1-6,lat, lon]
    :param p_values:  [som1-6,lat, lon]
    :param vartype: 'mcc' like
    :param lon: 2d
    :param lat: 2d
    :return:
    """


    var_inType = data


    central_lonlat = central_lonlat_all[6 - 1]

    if p_values is not None:
        var_inType_p = p_values
        ax.contourf(lon[:120], lat[:120], np.ma.masked_greater(var_inType_p[:120], 0.1),
                    colors='none', levels=[0, 0.1],
                    hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
    cb = ax.contourf(lon[:120], lat[:120],
                     var_inType[:120] * plus,
                     cmap=cmap_use, levels=ranges,
                     transform=ccrs.PlateCarree(), extend='both')

    boundary = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM6.mat')
    # plt.imshow(np.array(boundary['boundary']))
    # plt.show()
    boundary_index = np.where(np.array(boundary['boundary'], dtype='single')>3)
    ax.scatter(boundary['lon'][boundary_index], boundary['lat'][boundary_index],
               transform=ccrs.PlateCarree(),
               zorder=50,
               color='black', s=0.01)


    ax.coastlines(linewidth=0.3, zorder=10)

    from function_shared_Main import add_longitude_latitude_labels_givenloc
    add_longitude_latitude_labels_givenloc(fig, ax, xlocs[6 - 1], yloc=central_lonlat[0])
    # ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
    ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
                  ccrs.Orthographic(central_longitude=central_lonlat[0],central_latitude=central_lonlat[1]))

    return cb

"""
hgt
"""



# import matplotlib.gridspec as gridspec
# figlabelstr = [['(a)', '(b)'], ['(c)','(d)']]
# central_lonlat = central_lonlat_all[6 - 1]
# fig, axs = plt.subplots(ncols=2, nrows=2,figsize=[4,4], subplot_kw={'projection':ccrs.Orthographic(central_longitude=central_lonlat[0],
#                                                       central_latitude=central_lonlat[1])})
#
# plt.subplots_adjust(top=0.95,
# bottom=0.1,
# left=0.02,
# right=0.98,
# hspace=0.3,
# wspace=0.11)
#
# lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
#
# # ranges_all = [[np.arange(-10,10.01,1),  np.arange(-8,8.01,1),
# #                np.arange(-4,4.1,0.5), np.arange(-6,6.1,1)],
# #               [np.arange(-0.01, 0.01, 0.001), np.arange(-0.12, 0.12001, 0.01),
# #                np.arange(-0.3, 0.3001, 0.02), np.arange(-0.15, 0.15001, 0.01)]]
#
# ranges_all = [np.arange(-10,10.01,1),
#               np.arange(-0.1, 0.1001, 0.005)]
#
# plus_all = [1/98, 1]
#
# add_axes = [[0.05, 0.55, 0.9, 0.01], [0.05, 0.08, 0.9, 0.01], ]
# unit_all = ['gpm', '$\mathrm{Pa\,s^{-1}}$']
# varname_all = [["Z500'","Z500'"],
#                ["W500'","W500'"]]
# # for iI_var, var in enumerate(['str','sshf', 'slhf']):
# for iI_var, var in enumerate(['hgt', 'w']):
#     for iI_level, levels in enumerate([21,30]):
#
#
#
#
#         ar=nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                                r'/pdf_type%i_%s_pattern_%i' % (6, var, levels))
#         p = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                               r'/pdf_type%i_%s_P_%i' % (6, var, levels))
#
#
#
#         ax = axs[iI_var, iI_level]
#         cb = plot_axes4(ax, ar, p, lon_2d, lat_2d, ranges=ranges_all[iI_var],
#                         plus=plus_all[iI_var])
#
#         ax.set_title('%s %s' %(figlabelstr[iI_var][iI_level], varname_all[iI_var][iI_level]),loc='left')
#
#
#     cb_ax = fig.add_axes(add_axes[iI_var])
#     cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#     cb_ax.set_xlabel(unit_all[iI_var])
#
# # plt.show()
#
# # plt.savefig(r'S_hgt_w.png', dpi=400)
# plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-16.png', dpi=400)
# exit()


"""
500 flow
"""

# ar = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                   r'/pdf_type%i_%s_pattern_cli_%i' % (6, 'hgt', 21))
#
#
# figlabelstr = [['(a)', '(b)', '(c)','(d)'], ['(e)', '(f)', '(g)', '(h)']]
# central_lonlat = central_lonlat_all[6 - 1]
# fig, ax = plt.subplots(figsize=[4,4], subplot_kw={'projection':ccrs.Orthographic(central_longitude=central_lonlat[0],
#                                                       central_latitude=central_lonlat[1])})
#
#
# lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
#
#
# central_lonlat = central_lonlat_all[6 - 1]
# cb = ax.contourf(lon_2d[:120], lat_2d[:120],
#                  ar[:120] /98,
#                  cmap=cmap_use,levels=np.arange(505,540.1,1),
#                  transform=ccrs.PlateCarree(), extend='both')
# ax.contour(lon_2d[:120], lat_2d[:120],
#                  ar[:120] /98,levels=np.arange(500,540.1,2),colors='black',
#                  transform=ccrs.PlateCarree(), extend='both', linewidth = 0.1, alpha=0.8)
# boundary = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM6.mat')
# # plt.imshow(np.array(boundary['boundary']))
# # plt.show()
# boundary_index = np.where(np.array(boundary['boundary'], dtype='single') > 3)
# ax.scatter(boundary['lon'][boundary_index], boundary['lat'][boundary_index],
#            transform=ccrs.PlateCarree(),
#            zorder=50,
#            color='black', s=0.01)
#
# ax.coastlines(linewidth=0.3, zorder=10)
#
# from function_shared_Main import add_longitude_latitude_labels
# add_longitude_latitude_labels(fig, ax, xloc=64, yloc=150)
# ax.set_extent([0, 360, 60, 90],ccrs.PlateCarree())
# # ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
# #               ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))
#
# # ax.set_title('the climatology of 500 mb flow')
#
# cb_ax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
# cb_ax.set_xlabel('gpm')
# # cb_ax.set_xticks(np.arange(510,540.1,10))
# plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-15.png', dpi=400)
# exit()
"""

"""

# ar = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                   r'/pdf_type%i_%s_pattern' % (6, 'tcc'))
#
# p = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                               r'/pdf_type%i_%s_P' % (6, 'tcc'))
#
# figlabelstr = [['(a)', '(b)', '(c)','(d)'], ['(e)', '(f)', '(g)', '(h)']]
# central_lonlat = central_lonlat_all[6 - 1]
#
# fig, axs = plt.subplots(ncols=2, subplot_kw={'projection':ccrs.Orthographic(central_longitude=central_lonlat[0],
#                                                       central_latitude=central_lonlat[1])})
# ax = axs[1]
#
# lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
#
# central_lonlat = central_lonlat_all[6 - 1]
# cb = ax.contourf(lon_2d[:120], lat_2d[:120],
#                  ar[:120],
#                  cmap=cmap_use,levels=np.arange(-0.16,0.160001,0.02),
#                  transform=ccrs.PlateCarree(), extend='both')
#
# ax.contourf(lon_2d[:120], lat_2d[:120], np.ma.masked_greater(p[:120], 0.1),
#             colors='none', levels=[0, 0.1],
#             hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
# boundary = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM6.mat')
# # plt.imshow(np.array(boundary['boundary']))
# # plt.show()
# boundary_index = np.where(np.array(boundary['boundary'], dtype='single') > 3)
# ax.scatter(boundary['lon'][boundary_index], boundary['lat'][boundary_index],
#            transform=ccrs.PlateCarree(),
#            zorder=50,
#            color='black', s=0.1)
#
# ax.coastlines(linewidth=0.3, zorder=10)
#
# from function_shared_Main import add_longitude_latitude_labels_givenloc
# add_longitude_latitude_labels_givenloc(fig, ax, xlocs[6 - 1], yloc=central_lonlat[0])
#
# ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
#               ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))
#
# ax.set_title("(b) TCC'", loc='left')
#
#
# cbar = plt.colorbar(cb, orientation="horizontal", pad=0.05, aspect=40)
# cbar.ax.set_xticks(np.arange(-0.16,0.160001,0.08))
# cbar.set_label('')
#
#
# ar = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                   r'/pdf_type%i_%s_pattern' % (6, 'strd'))
# p = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\new_Clustering_paper_plot'
#                               r'/pdf_type%i_%s_P' % (6, 'strd'))
#
# figlabelstr = [['(a)', '(b)', '(c)','(d)'], ['(e)', '(f)', '(g)', '(h)']]
# central_lonlat = central_lonlat_all[6 - 1]
#
# ax = axs[0]
#
# lon_2d, lat_2d = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5))
#
# central_lonlat = central_lonlat_all[6 - 1]
# cb = ax.contourf(lon_2d[:120], lat_2d[:120],
#                  ar[:120]/ (6 * 3600),
#                  cmap=cmap_use,levels=np.arange(-6,6.160001,1),
#                  transform=ccrs.PlateCarree(), extend='both')
#
# ax.contourf(lon_2d[:120], lat_2d[:120], np.ma.masked_greater(p[:120], 0.1),
#             colors='none', levels=[0, 0.1],
#             hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
# boundary = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/boundaryThermoMeltSOM6.mat')
# # plt.imshow(np.array(boundary['boundary']))
# # plt.show()
# boundary_index = np.where(np.array(boundary['boundary'], dtype='single') > 3)
# ax.scatter(boundary['lon'][boundary_index], boundary['lat'][boundary_index],
#            transform=ccrs.PlateCarree(),
#            zorder=50,
#            color='black', s=0.1)
#
# ax.coastlines(linewidth=0.3, zorder=10)
#
# from function_shared_Main import add_longitude_latitude_labels_givenloc
# add_longitude_latitude_labels_givenloc(fig, ax, xlocs[6 - 1], yloc=central_lonlat[0])
#
# ax.set_extent([-2e+06, 2e+06, -2e+06, 1.5e+06],
#               ccrs.Orthographic(central_longitude=central_lonlat[0], central_latitude=central_lonlat[1]))
#
# ax.set_title("(a) DLW'", loc='left')
#
# cbar = plt.colorbar(cb, orientation="horizontal", pad=0.05, aspect=40)
# # cb_ax.set_xticks(np.arange(-6,6.160001,3))
# cbar.set_label('$\mathrm{W\,m^{-2}}$')
# plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\dissertation_fig4-17.png', dpi=400)
# plt.show()

"""
sea thermal and dynamical temporal variation
"""
import os.path

import numpy as np
import nc4
import matplotlib.pyplot as plt
import pandas as pd
from dateutil.relativedelta import relativedelta

fig, axs = plt.subplots(ncols=2)

add_name = r'aar-65-85'
a = nc4.read_nc4(r'D:/%s1'%add_name)*100

axs[0].plot(range(11), a[2:-1,0], label='thermal',color='red',
            marker='.', markersize=5)
axs[0].plot(range(11), a[2:-1,1], label='dynamical',color='blue',
            marker='.', markersize=5)
axs[0].scatter([3, 3],[a[5,0],a[5,1]], color='yellow', s=15, zorder=12, edgecolor='black')
axs[0].legend(loc='lower left')
axs[0].set_title('(a) AAR', loc='left')
axs[0].axvspan(3-0.87, 3+0.73, color='gray', alpha=0.5)

add_name = r'parup65'
a = nc4.read_nc4(r'D:/%s6'%add_name)*24*60*60*100


axs[1].plot(range(11), a[1:-2,1], label='thermal',color='red',
            marker='.', markersize=5)
axs[1].plot(range(11), a[2:-1,0], label='dynamical',color='blue',
            marker='.', markersize=5)
axs[1].scatter([3, 3],[a[5,0],a[4,1]], color='yellow', s=15, zorder=12, edgecolor='black')
axs[1].axvspan(3-0.27, 3+1.25, color='gray', alpha=0.5)


for ax in axs:
    ax.set_xticks(np.arange(11))
    ax.set_xticklabels(['-3d','-2d','-1d','Onset','+1d','+2d', '+3d','+4d','+5d', '+6d', '+7d'])
    ax.set_ylim([-0.15,0.1])
    ax.axhline(0, color='black', linestyle='--',linewidth=0.8)
    ax.set_ylabel('Heff(m)')
axs[1].legend(loc='lower left')
axs[1].set_title('(b) PAR', loc='left')
plt.show()


"""
composite pattern
"""
import cartopy.crs as ccrs
import xarray as xr
import cmaps

cmap_use = cmaps.cmocean_balance_r
a = nc4.read_nc4(r'D:/pattern1') # 14 2 361 361
b = nc4.read_nc4(r'D:/pattern6') # 14 2 361 361

ab = [a, b]
central_lonlat_all = [[30,80],[180,70]]

ass = xr.open_dataset(r"D:\budget\budfields_20060104.nc")
lon = ass.lon.values[0]
lat = ass.lat.values[0]

for i_day in range(14):
    fig= plt.figure()
    for i_SOM in range(2):
        for i_i, i in enumerate([0, 1]):
            ax = fig.add_subplot(2, 2, i_SOM*2+i_i+1,
                                 projection=ccrs.Orthographic(central_longitude=central_lonlat_all[i_SOM][0],
                                                              central_latitude=central_lonlat_all[i_SOM][1]))


            data_withCLI = ab[i_SOM][:, i_day, i_i]

            from scipy import stats

            _, p_value = stats.ttest_ind(data_withCLI[:,1], data_withCLI[:,0], axis=0)
            data = np.nanmean(data_withCLI[:,0]-data_withCLI[:,1], axis=0)
            import cartopy.crs as ccrs
            import matplotlib.colors as colors


            p_pass = np.where(p_value > 0.1)
            data[p_pass] =np.nan
            # cb = ax.pcolormesh(lon, lat, np.ma.masked_greater(p_value, 0.1),
            #                    cmap=cmap_use,
            #                    transform=ccrs.PlateCarree())
            cb = ax.pcolormesh(lon, lat, np.ma.masked_where(p_value>0.1, data*24*60*60),
                               cmap=cmap_use,
                               transform=ccrs.PlateCarree(), vmin=-0.1, vmax=0.1)
            plt.colorbar(cb, ax=ax)

            ax.coastlines(linewidth=0.3, zorder=10)
            ax.gridlines(ylocs=[66], linewidth=0.3, color='black')

            ax.set_extent([-2e+06, 2e+06, -2e+06, 2e+06],
                          ccrs.Orthographic(central_longitude=central_lonlat_all[i_SOM][0],
                                            central_latitude=central_lonlat_all[i_SOM][1]))
    plt.title('day_%i'%i_day)
    # plt.show()
    plt.savefig(r'D:\budget/day_%i.png'%i_day, dpi=200)

    plt.close()