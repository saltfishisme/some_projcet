import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io as scio
from aostools import climate
import xarray as xr
import datetime as dt
from datetime import timedelta
import netCDF4 as nc
import nc4
from scipy.signal import detrend
from scipy.stats import zscore


year_all = np.arange(2015, 2099+1)

# aar_series_arSIH1 = zscore(detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BAF_ssp245')))
# aar_series_arSIH2 = zscore(detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BAF_ssp585')))
# par_series_arSIH1 = zscore(detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BSE_ssp245')))
# par_series_arSIH2 = zscore(detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BSE_ssp585')))
# aar_series_arSIH1 = detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BAF_ssp245'))
# aar_series_arSIH2 = detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BAF_ssp585'))
# par_series_arSIH1 = detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BSE_ssp245'))
# par_series_arSIH2 = detrend(nc4.read_nc4(r'save_avgSIV_for_field_analysis_BSE_ssp585'))
aar_series_arSIH1 =nc4.read_nc4(r'save_avgSIV_for_field_analysis_BAF_ssp245')
aar_series_arSIH2 = nc4.read_nc4(r'save_avgSIV_for_field_analysis_BAF_ssp585')
par_series_arSIH1 = nc4.read_nc4(r'save_avgSIV_for_field_analysis_BSE_ssp245')
par_series_arSIH2 = nc4.read_nc4(r'save_avgSIV_for_field_analysis_BSE_ssp585')
# aar_series_arSIH[np.isnan(aar_series_arSIH)] = 0
# par_series_arSIH[np.isnan(par_series_arSIH)] = 0

fig, axs = plt.subplots(2,1)
plt.subplots_adjust(top=0.88,
bottom=0.11,
left=0.125,
right=0.9,
hspace=0.51,
wspace=0.215)

x = year_all
axs[0].plot(x,aar_series_arSIH1, color='black', label='SSP2-4.5')
axs[0].plot(x,aar_series_arSIH2, color='red', label='SSP5-8.5')
axs[0].set_title('(a) BAF', loc='left')
axs[0].legend()
x = year_all
axs[1].plot(x,par_series_arSIH1, color='black', label='SSP2-4.5')
axs[1].plot(x,par_series_arSIH2, color='red', label='SSP5-8.5')
axs[1].set_title('(b) BSW', loc='left')
axs[1].legend()
for ax in axs:
    ax.set_xlabel('year')
    # ax.set_ylabel('m')

plt.savefig(r'dissertation_figure6-8.png', dpi=400)
plt.show()
# print(list(np.arange(16,41, 4)))
# if len(var_all) > 2:
#     ax.set_xticks([4,12,  20, 28, 36])
#     ax.set_xticklabels(['-2d','Onset','+2d','+4d', '+6d'])