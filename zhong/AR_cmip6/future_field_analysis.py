import os
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import nc4
from scipy.stats import zscore
from scipy.signal import detrend
import cmaps
from scipy.stats import linregress
import cartopy.feature as cfeature
from scipy.ndimage import gaussian_filter
"""
prepare data
=====[warning]===all DJF averaged 
"""

def regrid(sparse_grid, x, y):
    from scipy.interpolate import griddata
    if x.ravel().shape != sparse_grid.ravel().shape:
        x, y = np.meshgrid(x, y)
    x_flat = x.ravel()
    y_flat = y.ravel()
    z_flat = sparse_grid.ravel()
    min_lon = np.min(x_flat)
    max_lon = np.max(x_flat)
    if max_lon>180:
        new_x, new_y = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.001,-0.5))
    elif min_lon<0:
        from function_shared_Main import roll_longitude_from_359to_negative_180
        new_x, new_y = np.meshgrid(roll_longitude_from_359to_negative_180(np.arange(0,360,0.5)), np.arange(90,-90.001,-0.5))
    else:
        new_x, new_y = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.001,-0.5))
    return griddata((x_flat, y_flat), z_flat, (new_x, new_y), method='nearest')


years = np.arange(2015,2099+1)
months = [9,10,11]
model_name = ['BCC-CSM2-MR', 'CESM2-WACCM', 'CMCC-CM2-SR5', 'IPSL-CM6A-LR', 'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR']
ssp = 'ssp245'
#
#### 2, load model hist data averaged for year, and regrid
model_hist = []
for i_model_name in model_name:
    listfilename = os.listdir(r'G:\cmip6_seaice\siconc/')
    for iiiii in listfilename:
        if (i_model_name in iiiii) & (ssp in iiiii):
            i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\siconc/"+iiiii)
    # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
    i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.year'], years))
    i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.month'], months))
    year_New = i_model_hist.time.dt.year
    month_New = i_model_hist.time.dt.month
    year_New[month_New==12] = year_New[month_New==12]+1
    i_model_hist = i_model_hist.assign_coords(month_day=year_New)
    i_model_hist = i_model_hist.groupby("month_day").mean("time")
    print(i_model_hist)
    if 'lat' in  i_model_hist.coords:
        x = i_model_hist.lon.values
        y=i_model_hist.lat.values
    elif 'latitude' in  i_model_hist.coords:
        x = i_model_hist.longitude.values
        y=i_model_hist.latitude.values
    elif 'nav_lat' in  i_model_hist.coords:
        x = i_model_hist.nav_lon.values
        y=i_model_hist.nav_lat.values

    i_model_hist = [regrid(i, x=x, y=y) for i in i_model_hist['siconc'].values]
    model_hist.append(i_model_hist)
model_hist = np.array(model_hist)
nc4.save_nc4(model_hist, 'model_%s_siconc'%(ssp))
#
# #### 2, load model hist data averaged for year, and regrid
# model_hist = []
# for i_model_name in model_name:
#     listfilename = os.listdir(r'G:\cmip6_seaice\zg500/')
#     for iiiii in listfilename:
#         if (i_model_name in iiiii) & (ssp in iiiii):
#             i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\zg500/"+iiiii)
#     # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
#
#     if len(i_model_hist['zg'].values.shape)>=4:
#         i_model_hist = i_model_hist.sel(plev=50000)
#     print(i_model_hist.plev)
#     i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.year'], years))
#     i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.month'], months))
#     year_New = i_model_hist.time.dt.year
#     month_New = i_model_hist.time.dt.month
#     year_New[month_New==12] = year_New[month_New==12]+1
#     i_model_hist = i_model_hist.assign_coords(month_day=year_New)
#     i_model_hist = i_model_hist.groupby("month_day").mean("time")
#     print(i_model_hist)
#     if 'lat' in  i_model_hist.coords:
#         x = i_model_hist.lon.values
#         y=i_model_hist.lat.values
#     elif 'latitude' in  i_model_hist.coords:
#         x = i_model_hist.longitude.values
#         y=i_model_hist.latitude.values
#     elif 'nav_lat' in  i_model_hist.coords:
#         x = i_model_hist.nav_lon.values
#         y=i_model_hist.nav_lat.values
#
#     i_model_hist = [regrid(i, x=x, y=y) for i in i_model_hist['zg'].values]
#     model_hist.append(i_model_hist)
# model_hist = np.array(model_hist)
# nc4.save_nc4(model_hist, 'model_%s_zg500_%i'%(ssp,months[0]))

#### 2, load model hist data averaged for year, and regrid
model_hist = []
for i_model_name in model_name:
    listfilename = os.listdir(r'G:\cmip6_seaice\sivol/')
    for iiiii in listfilename:
        if (i_model_name in iiiii) & (ssp in iiiii):
            i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\sivol/"+iiiii)
    # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
    i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.year'], years))
    i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.month'], months))
    year_New = i_model_hist.time.dt.year
    month_New = i_model_hist.time.dt.month
    year_New[month_New==12] = year_New[month_New==12]+1
    i_model_hist = i_model_hist.assign_coords(month_day=year_New)
    i_model_hist = i_model_hist.groupby("month_day").mean("time")
    print(i_model_hist)
    if 'lat' in  i_model_hist.coords:
        x = i_model_hist.lon.values
        y=i_model_hist.lat.values
    elif 'latitude' in  i_model_hist.coords:
        x = i_model_hist.longitude.values
        y=i_model_hist.latitude.values
    elif 'nav_lat' in  i_model_hist.coords:
        x = i_model_hist.nav_lon.values
        y=i_model_hist.nav_lat.values

    i_model_hist = [regrid(i, x=x, y=y) for i in i_model_hist['sivol'].values]
    model_hist.append(i_model_hist)
model_hist = np.array(model_hist)
nc4.save_nc4(model_hist, 'model_%s_sivol_%i'%(ssp,months[0]))


## 2, load model hist data averaged for year, and regrid
model_hist = []
for i_model_name in model_name:
    # i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\raw\sivol_SImon_BCC-CSM2-MR_historical_r1i1p1f1_gn_185001-201412.nc")
    listfilename = os.listdir(r'G:\cmip6_seaice\tas/')
    for iiiii in listfilename:
        if (i_model_name in iiiii) & (ssp in iiiii):
            i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\tas/"+iiiii)
    # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
    i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.year'], years))
    i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.month'], months))
    year_New = i_model_hist.time.dt.year
    month_New = i_model_hist.time.dt.month
    year_New[month_New==12] = year_New[month_New==12]+1
    i_model_hist = i_model_hist.assign_coords(month_day=year_New)
    i_model_hist = i_model_hist.groupby("month_day").mean("time")
    if 'lat' in  i_model_hist.coords:
        x = i_model_hist.lon.values
        y=i_model_hist.lat.values
    elif 'latitude' in  i_model_hist.coords:
        x = i_model_hist.longitude.values
        y=i_model_hist.latitude.values
    elif 'nav_lat' in  i_model_hist.coords:
        x = i_model_hist.nav_lon.values
        y=i_model_hist.nav_lat.values

    i_model_hist = [regrid(i, x=x, y=y) for i in i_model_hist['tas'].values]
    model_hist.append(i_model_hist)
model_hist = np.array(model_hist)
nc4.save_nc4(model_hist, 'model_%s_tas_%i'%(ssp,months[0]))
exit()
# ### 2, load model hist data averaged for year, and regrid
# path_modelName = r'G:\cmip6_seaice\ar/'
#
# year_begin = years[0]
# year_end = years[-1]
# date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
# date = date[np.in1d(date.month, months)].strftime('%Y%m')
#
# model_hist = []
#
# for i_modelName in model_name:
#
#     AR_frequency_model = np.zeros([year_end-year_begin+1, 361, 720])
#
#     for i_date in date:
#         filename = r'CMIP6_%s_%s_hist_threshold_AR_01field_global_bydatetime_%s.nc'%(i_modelName, ssp, i_date)
#         print(filename)
#         data_row = xr.open_dataset(path_modelName + i_modelName+ '/'+filename)
#         data = np.sum(data_row['AR_pathway'].values, axis=0)
#         # regrid
#         data = regrid(data, data_row.longitude.values, data_row.latitude.values)
#
#         loc_year = int(i_date[:4])-year_begin
#         if i_date[4:6] == '12':
#             loc_year += 1
#         if loc_year > (year_end-year_begin):
#             continue
#         AR_frequency_model[loc_year] += data
#     model_hist.append(AR_frequency_model)
#
# nc4.save_nc4(np.array(model_hist), r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6/model_%s_ar'%ssp)
#
# exit()
"""
+++++++++++++++
"""
# for ssp in ['ssp245', 'ssp585']:
#     path_modelName = r'G:\cmip6_seaice\ar/'
#     year_begin = years[0]
#     year_end = years[-1]
#     date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
#     date = date[np.in1d(date.month, months)].strftime('%Y%m')
#     model_hist = []
#     for i_modelName in model_name:
#         AR_frequency_model = np.zeros([year_end-year_begin+1, 3, 361, 720])
#         for i_date in date:
#             filename = r'CMIP6_%s_%s_hist_threshold_AR_01field_global_bydatetime_%s.nc'%(i_modelName, ssp, i_date)
#             print(filename)
#             data_row = xr.open_dataset(path_modelName + i_modelName+ '/'+filename)
#             data = np.sum(data_row['AR_pathway'].values, axis=0)
#             # regrid
#             data = regrid(data, data_row.longitude.values, data_row.latitude.values)
#
#             loc_year = int(i_date[:4])-year_begin
#             loc_month = int(i_date[4:6])
#             if i_date[4:6] == '12':
#                 loc_year += 1
#                 loc_month = 0
#             if loc_year > (year_end-year_begin):
#                 continue
#             AR_frequency_model[loc_year, loc_month] += data
#         model_hist.append(AR_frequency_model)
#     model_hist = np.array(model_hist)
#     ar_monthly = np.nanmean(model_hist, axis=0)
#     nc4.save_nc4(model_hist, 'model_%s_ar_monthly' % (ssp))

"""
field analysis
"""
def save_standard_nc(data, var_name, times, lon=None, lat=None, ll=False):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360,0.5)
    if lat is None:
        lat = np.arange(90, -90.001, -0.5)

    ds = xr.Dataset()
    ds.coords['time'] = times
    if ll:
        ds.coords["latitude"] = lat
        ds.coords["longitude"] = lon

        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = (('time', "latitude", "longitude"), np.array(data[iI_vari]))
    else:
        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = ('time', np.array(data[iI_vari]))

    return ds

def lingress(series, field):
    times = pd.date_range('%i-01-01'%year[0], '%i-01-01'%(year[-1]+1), freq='1Y')
    times = times[np.in1d(times.year, year)]
    series = save_standard_nc([series], ['var'], times, ll=False)
    field = save_standard_nc([field], ['var'], times, ll=True)

    def lag_linregress_3D(x, y, lagx=0, lagy=0):
        """
        Input: Two xr.Datarrays of any dimensions with the first dim being time.
        Thus the input data could be a 1D time series, or for example, have three
        dimensions (time, lat, lon).
        Datasets can be provided in any order, but note that the regression slope
        and intercept will be calculated for y with respect to x.
        Output: Covariance, correlation, regression slope and intercept, p-value,
        and standard error on regression between the two datasets along their
        aligned time dimension.
        Lag values can be assigned to either of the data, with lagx shifting x, and
        lagy shifting y, with the specified lag amount.
        """

        # 1. Ensure that the data are properly alinged to each other.
        x, y = xr.align(x, y)

        # 2. Add lag information if any, and shift the data accordingly
        if lagx != 0:
            # If x lags y by 1, x must be shifted 1 step backwards.
            # But as the 'zero-th' value is nonexistant, xr assigns it as invalid
            # (nan). Hence it needs to be dropped
            x = x.shift(time=-lagx).dropna(dim='time')

            # Next important step is to re-align the two datasets so that y adjusts
            # to the changed coordinates of x
            x, y = xr.align(x, y)

        if lagy != 0:
            y = y.shift(time=-lagy).dropna(dim='time')
            x, y = xr.align(x, y)

        # 3. Compute data length, mean and standard deviation along time axis:
        n = y.notnull().sum(dim='time')
        xmean = x.mean(dim='time')
        ymean = y.mean(dim='time')
        xstd = x.std(dim='time')
        ystd = y.std(dim='time')

        # 4. Compute covariance along time axis
        cov = ((x - xmean) * (y - ymean)).sum(dim='time') / n

        # 5. Compute correlation along time axis
        cor = cov / (xstd * ystd)

        # 6. Compute regression slope and intercept:
        slope = cov / (xstd ** 2)
        intercept = ymean - xmean * slope

        # 7. Compute P-value and standard error
        # Compute t-statistics
        tstats = cor * np.sqrt(n - 2) / np.sqrt(1 - cor ** 2)
        stderr = slope / tstats

        from scipy.stats import t

        pval = t.sf(np.abs(tstats['var'].values), n['var'].values - 2) * 2

        return cov['var'].values, cor['var'].values, slope['var'].values, intercept['var'].values, pval, stderr['var'].values

    cov, cor, slope, intercept, pval, stderr = lag_linregress_3D(series, field)
    return cov, cor, slope, intercept, pval, stderr



# year = np.arange(2015, 2099+1)
#
# sivol_tas = 'sivol'
# external_slope= {'ssp245':0.0225, 'ssp585':0.4722}
#
# data = []
# data_p = []
#
# for ssp in ['ssp245', 'ssp585']:
#
#     series_ar = nc4.read_nc4('save_avg_for_field_analysis') # in cmip6_ar.py, area-averaged.
#     if ssp == 'ssp245':
#         series_ar = series_ar[0]
#     else:
#         series_ar = series_ar[1]
#
#
#     field = nc4.read_nc4('model_%s_%s'%(ssp, sivol_tas))
#     field = np.nanmean(field, axis=0)[:85,:]
#
#     field[np.isnan(field)] = 0
#     from scipy.signal import detrend
#     _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(field))
#     data.append(slope)
#     data_p.append(pval)
#
#
# data = np.array(data)
# data_p = np.array(data_p)
#
#
# SMALL_SIZE=6
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
# plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
# plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize
#
# lon = np.arange(0,360,0.5)
# lat  = np.arange(90,-90.001,-0.5)
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# def plot_model_index(axs, data, levels=None, cbar_unit='', oneside=False):
#     if oneside:
#         cmaps_here = 'Oranges'
#     else:
#         cmaps_here = 'bwr'
#
#     for i in range(2):
#         ax = axs[i]
#         if levels is None:
#             cb = ax.contourf(lon, lat, data[0][i]
#                              , cmap=cmaps_here,extend='both',
#                              transform=ccrs.PlateCarree())
#             plt.colorbar(cb, ax=ax)
#
#         else:
#             # cb = ax.contourf(lon, lat, data[i*3+j]
#             #                  , cmap=cmaps_here,extend='both',levels=levels,
#             #                  transform=ccrs.PlateCarree())
#             cb = ax.pcolormesh(lon, lat, data[0][i]
#                              , cmap=cmaps_here,vmin=-1, vmax=1,
#                              transform=ccrs.PlateCarree())
#         ax.contourf(lon, lat, np.ma.masked_greater(data[1][i], 0.1),
#
#                     colors='none', levels=[0, 0.1],
#                     hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
#         ax.coastlines(linewidth=0.3, zorder=10)
#
#     cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
#     cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
#     cb_ax.set_xlabel(cbar_unit)
#
#
# fig, axs = plt.subplots(1, 2, subplot_kw={'projection': ccrs.NorthPolarStereo()}, figsize=(5, 3))
# plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)
#
# title = ['(a) SSP2-4.5',
#          '(b) SSP5-8.5']
# plot_model_index(axs, [data, data_p], levels=np.arange(-1.6,1.61,0.2), cbar_unit='')
# for i in range(2):
#     ax = axs[i]
#     ax.set_title(title[i], loc='left', fontdict={'size':8})
#     ax.add_feature(cfeature.LAND,zorder=100)
#     ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
#     from function_shared_Main import add_longitude_latitude_labels
#     add_longitude_latitude_labels(fig, ax, xloc=60, yloc=90)
#
# plt.savefig('dissertation_fig6-7.png', dpi=400)
# plt.show()
# plt.close()


"""
zg500 and tas

"""
year_all = np.arange(2015, 2099+1)
sivol_tas = 'tas'

data = []
data_p = []
type_SOM = 'BSE'
pic_name = '9'
select_year = True

for ssp in ['ssp245', 'ssp585']:
    ## create series_siovl
    def get_series_sivol_ar(var):
        field = nc4.read_nc4('model_%s_%s' % (ssp, var))[:,:85,:]
        data = np.nanmean(field,axis=0)

        # data[ar == 0] = np.nan
        data[:, 80:, :] = np.nan

        if type_SOM == 'BAF':
            # data[:,:, 630:] = np.nan #BAF 360-45
            data[:, :, :480] = np.nan #BAF 360-120
        elif type_SOM == 'BSE':
            data[:, :, 400:] = np.nan  # 200
            data[:, :, :280] = np.nan  # 140
        # plt.imshow(data[0])
        # plt.show()
        from function_shared_Main import cal_area
        def area_weighted_mean(data, area_weight):
            area_weight = area_weight.copy()
            data = data.copy()
            area_weight[np.isnan(data)] = 0
            area_weight = area_weight / np.sum(area_weight)
            return np.nansum(data * area_weight)
        area_weighted = cal_area(np.arange(-180, 180, 0.5), np.arange(90, -90.001, -0.5))
        return np.array([area_weighted_mean(i_data, area_weighted) for i_data in data])
    # if not os.path.exists(r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6\save_avgSIV_for_field_analysis_%s_%s.nc4'%(type_SOM, ssp)):
    #     series_ar = get_series_sivol()
    #     nc4.save_nc4(series_ar, 'save_avgSIV_for_field_analysis_%s_%s'%(type_SOM, ssp))
    # else:
    #     series_ar = nc4.read_nc4('save_avgSIV_for_field_analysis_%s_%s'%(type_SOM, ssp))
    #     print(series_ar)
    series_ar = get_series_sivol_ar('ar')
    # series_ar = nc4.read_nc4('save_avg_for_field_analysis_%s'%type_SOM) # in cmip6_ar.py, area-averaged.
    # if ssp == 'ssp245':
    #     series_ar = series_ar[0]
    # else:
    #     series_ar = series_ar[1]

    if select_year:
        # indices = np.argwhere(zscore(series_ar) <-1).flatten()
        # indices_low = np.argwhere(zscore(series_ar) < -1).flatten()
        indices = np.argpartition(detrend(series_ar), -15)[-15:]
        print(series_ar[indices])
        print(series_ar)
    else:
        indices = np.arange(len(year_all))
        # indices = np.argpartition(detrend(series_ar), 84)[:84]
    indices = list(sorted(indices))
    year = year_all[indices]
    print(list(year))

    series_ar = get_series_sivol_ar('sivol')[indices]

    field = nc4.read_nc4('model_%s_%s' % (ssp, sivol_tas))
    field = np.nanmean(field, axis=0)[indices]



    _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(field))

    # _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(detrend(field, axis=0,type='linear')))
    data.append(slope)
    data_p.append(pval)

data = np.array(data)
data_p = np.array(data_p)


SMALL_SIZE=6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

lon = np.arange(0,360,0.5)
lat  = np.arange(90,-90.001,-0.5)
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

def plot_model_index32(axs, data, levels=None, cbar_unit='', oneside=False):
    if oneside:
        cmaps_here = 'Oranges'
    else:
        cmaps_here = 'bwr'

    for i in range(2):
            ax = axs[i]
            if levels is None:
                cb = ax.contourf(lon, lat, data[0][i]
                                 , cmap=cmaps_here,extend='both',
                                 transform=ccrs.PlateCarree())
                cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                                    shrink=0.6, pad=0.08, aspect=40)

            else:
                cb = ax.contourf(lon, lat, data[0][i]
                                 , cmap=cmaps_here,extend='both',levels=levels,
                                 transform=ccrs.PlateCarree())
                # cb = ax.pcolormesh(lon, lat, data[i*3+j]
                #                  , cmap='bwr_r',vmin=-2, vmax=2,
                #                  transform=ccrs.PlateCarree())
                cb_ax = fig.add_axes([0.2, 0.07, 0.6, 0.01])
                cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
                cb_ax.set_xlabel(cbar_unit)
            ax.contourf(lon, lat, np.ma.masked_greater(data[1][i], 0.1),

                        colors='none', levels=[0, 0.1],
                        hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
            ax.coastlines(linewidth=0.3, zorder=10)



fig, axs = plt.subplots(2, 1, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)}, figsize=(6, 4))
plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)

title = ['(a) SSP2-4.5','(b) SSP5-8.5']
plot_model_index32(axs, [data, data_p],levels=np.arange(-1,1.001,0.1), cbar_unit='')


for i in range(2):

    ax = axs[i]
    ax.set_title(title[i], loc='left', fontdict={'size': 8})

    ax.set_extent([0, 359, 0, 90], crs=ccrs.PlateCarree())
    ax.gridlines(ylocs=[10,30, 50, 70, 90], xlocs=[0, 60, 120, 179.9999,-0.00001 -60, -120],
                 draw_labels={"bottom": "x", "left": "y"},
                 linewidth=0.3, color='black', xpadding=3)

plt.savefig('dissertation_fig6-%s.png'%pic_name, dpi=400)
plt.show()
plt.close()


# year = np.arange(2015, 2099+1)
# ssp = 'ssp585'
# sivol_tas = 'sivol'

# external_slope= {'ssp245':0.0225, 'ssp585':0.4722}
#
# data = []
# data_p = []
# for ssp in ['ssp245', 'ssp585']:
#     series_ar = nc4.read_nc4('save_avg_for_field_analysis') # in cmip6_ar.py, area-averaged.
#     if ssp == 'ssp245':
#         series_ar = series_ar[0]
#     else:
#         series_ar = series_ar[1]
#
#     field = nc4.read_nc4('model_%s_%s'%(ssp, sivol_tas))
#     field = np.nanmean(field, axis=0)[:85,:]
#
#     field[np.isnan(field)] = 0
#     from scipy.signal import detrend
#     _, _, slope, _, pval, _ = lingress(series_ar, field)
#     data.append(slope)
#     data_p.append(pval)
#
#     slope_t2m = external_slope[ssp]
#     external_forcing = slope_t2m*field
#     internal_variation = field-slope_t2m*field
#
#     _, _, slope, _, pval, _ = lingress(series_ar, external_forcing)
#     data.append(slope)
#     data_p.append(pval)
#
#     _, _, slope, _, pval, _ = lingress(series_ar, internal_variation)
#     data.append(slope)
#     data_p.append(pval)
#
# data = np.array(data)
# data_p = np.array(data_p)
#
#
# SMALL_SIZE=6
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
# plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
# plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize
#
# lon = np.arange(0,360,0.5)
# lat  = np.arange(90,-90.001,-0.5)
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# def plot_model_index(axs, data, levels=None, cbar_unit='', oneside=False):
#     if oneside:
#         cmaps_here = 'Oranges'
#     else:
#         cmaps_here = 'bwr'
#
#     for i in range(2):
#         for j in range(3):
#             ax = axs[i][j]
#             if levels is None:
#                 cb = ax.contourf(lon, lat, data[0][i*3+j]
#                                  , cmap=cmaps_here,extend='both',
#                                  transform=ccrs.PlateCarree())
#                 plt.colorbar(cb, ax=ax)
#
#             else:
#                 # cb = ax.contourf(lon, lat, data[i*3+j]
#                 #                  , cmap=cmaps_here,extend='both',levels=levels,
#                 #                  transform=ccrs.PlateCarree())
#                 cb = ax.pcolormesh(lon, lat, data[0][i*3+j]
#                                  , cmap=cmaps_here,vmin=-1, vmax=1,
#                                  transform=ccrs.PlateCarree())
#             ax.contourf(lon, lat, np.ma.masked_greater(data[1][i*3+j], 0.1),
#
#                         colors='none', levels=[0, 0.1],
#                         hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
#             ax.coastlines(linewidth=0.3, zorder=10)
#
#     cb_ax = fig.add_axes([0.1, 0.07, 0.8, 0.02])
#     cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
#     cb_ax.set_xlabel(cbar_unit)
#
#
# fig, axs = plt.subplots(2, 3, subplot_kw={'projection': ccrs.NorthPolarStereo()}, figsize=(7, 5))
# plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)
#
# title = ['(a) SSP2-4.5', '(b) External forcing SSP2-4.5', '(c) Internal forcing SSP2-4.5',
#          '(d) SSP5-8.5', '(e) External forcing SSP5-8.5', '(f) Internal forcing SSP5-8.5']
# plot_model_index(axs, [data, data_p], levels=np.arange(-1.6,1.61,0.2), cbar_unit='m')
# for i in range(2):
#     for j in range(3):
#         ax = axs[i][j]
#         ax.set_title(title[i*3+j], loc='left', fontdict={'size':8})
#         ax.add_feature(cfeature.LAND,zorder=100)
#         ax.set_extent([0, 359, 55, 90], crs=ccrs.PlateCarree())
#         from function_shared_Main import add_longitude_latitude_labels
#         add_longitude_latitude_labels(fig, ax, xloc=60, yloc=90)
#
#
# plt.savefig('dissertation_fig6-7_slopeTotalEarth.png', dpi=400)
# plt.show()
# plt.close()


# """
# zg500 and tas
#
# """
# year = np.arange(2015, 2099+1)
# sivol_tas = 'zg500'
#
# external_slope= {'ssp245':0.0225, 'ssp585':0.4722}
#
# data = []
# data_p = []
# for ssp in ['ssp245', 'ssp585']:
#     series_ar = nc4.read_nc4('save_avg_for_field_analysis') # in cmip6_ar.py, area-averaged.
#     if ssp == 'ssp245':
#         series_ar = series_ar[0]
#     else:
#         series_ar = series_ar[1]
#
#     field = nc4.read_nc4('model_%s_%s'%(ssp, sivol_tas))
#     field = np.nanmean(field, axis=0)[:85,:]
#
#     field[np.isnan(field)] = 0
#     from scipy.signal import detrend
#
#     _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(field))
#     data.append(slope)
#     data_p.append(pval)
#
#
#     _, _, slope_t2m, _,_, _ = lingress(zscore(nc4.read_nc4('warming_tred_global_%s'%ssp)), zscore(field))
#     print(slope_t2m)
#     external_forcing = slope_t2m*field
#     internal_variation = field-slope_t2m*field
#
#     _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(detrend(field)))
#     data.append(slope)
#     data_p.append(pval)
#
#     _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(internal_variation))
#     data.append(slope)
#     data_p.append(pval)
#
# data = np.array(data)
# data_p = np.array(data_p)
#
#
# SMALL_SIZE=6
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
# plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
# plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize
#
# lon = np.arange(0,360,0.5)
# lat  = np.arange(90,-90.001,-0.5)
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# def plot_model_index32(axs, data, levels=None, cbar_unit='', oneside=False):
#     if oneside:
#         cmaps_here = 'Oranges'
#     else:
#         cmaps_here = 'bwr'
#
#     for i in range(3):
#         for j in range(2):
#             ax = axs[i][j]
#             if levels is None:
#                 cb = ax.contourf(lon, lat, data[0][i*2+j]
#                                  , cmap=cmaps_here,extend='both',
#                                  transform=ccrs.PlateCarree())
#                 plt.colorbar(cb, ax=ax)
#
#             else:
#                 cb = ax.contourf(lon, lat, data[0][i*2+j]
#                                  , cmap=cmaps_here,extend='both',levels=levels,
#                                  transform=ccrs.PlateCarree())
#                 # cb = ax.pcolormesh(lon, lat, data[i*3+j]
#                 #                  , cmap='bwr_r',vmin=-2, vmax=2,
#                 #                  transform=ccrs.PlateCarree())
#             ax.contourf(lon, lat, np.ma.masked_greater(data[1][i*2+j], 0.05),
#
#                         colors='none', levels=[0, 0.05],
#                         hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
#             ax.coastlines(linewidth=0.3, zorder=10)
#
#
#     cb_ax = fig.add_axes([0.2, 0.07, 0.6, 0.01])
#     cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
#     cb_ax.set_xlabel(cbar_unit)
#
#
# fig, axs = plt.subplots(3, 2, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)}, figsize=(7, 4))
# plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)
#
# title = ['(a) SSP2-4.5', '(b) External forcing SSP2-4.5', '(c) Internal forcing SSP2-4.5',
#          '(d) SSP5-8.5', '(e) External forcing SSP5-8.5', '(f) Internal forcing SSP5-8.5']
# plot_model_index32(axs, [data, data_p], cbar_unit='m')
#
#
# for i in range(3):
#     for j in range(2):
#         ax = axs[i][j]
#         ax.set_title(title[i*2+j], loc='left', fontdict={'size': 8})
#         ax.gridlines(ylocs=[-60, -30, -0, 30, 60, 90], xlocs=[0, 60, 120, -60, -120],
#                      draw_labels={"bottom": "x", "left": "y"},
#                      linewidth=0.3, color='black', xpadding=3)
#
# plt.savefig('dissertation_fig6-8_slopeTotalEarth.png', dpi=400)
# plt.show()
# plt.close()
"""
single plot
"""

year = np.arange(2015, 2099+1)
ssp = 'ssp245'
sivol_tas = 'zg500'

series_ar = nc4.read_nc4('save_avg_for_field_analysis') # in cmip6_ar.py, area-averaged.
if ssp == 'ssp245':
    series_ar = series_ar[0]
else:
    series_ar = series_ar[1]

field = nc4.read_nc4('model_%s_%s'%(ssp, sivol_tas))
field = np.nanmean(field, axis=0)[:85,:]

field[np.isnan(field)] = 0
from scipy.signal import detrend
# series_ar = detrend(series_ar, type='linear')

# field = detrend(field,axis=0, type='linear')
cov, cor, slope, intercept, pval, stderr = lingress(series_ar, field)

def plot_linregress(trend, p_values, filename):
    fig = plt.figure(figsize=[5, 3.9])
    plt.subplots_adjust(top=0.97,
                        bottom=0.1,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))


    # trend[np.isnan(trend)] = 0
    trend[trend == 0] = np.nan
    import matplotlib.colors as colors

    # X, Y, masked_drm = z_masked_overlap(
    #     ax, np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :],
    #     source_projection=ccrs.Geodetic())
    # cb = ax.contourf(X, Y, masked_drm, 51,
    #             cmap='seismic')
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  ,levels=[-0.1,-0.05,-0.025, -0.0001, 0.0001, 0.025,0.05,0.1], extend='both', cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    trend[np.abs(trend) < 1.e-3] = 0.
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())
    lon = np.arange(0, 360, 0.5)
    lat = np.arange(90, -90.01, -0.5)
    # cb = ax.pcolormesh(lon, lat, trend,vmin=-1, vmax=1
    #                  , cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    cb = ax.pcolormesh(lon, lat, trend
                     , cmap='bwr',
                     transform=ccrs.PlateCarree())
    plt.colorbar(cb)
    ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.05),

                colors='none', levels=[0, 0.05],
                hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    plt.title(filename)
    plt.show()
    # plt.savefig(path_Save + '%s.png'%filename)
    # plt.close()
plot_linregress(slope, pval, 'test')


exit()

"""
external forcing scale
"""
year = np.arange(2015, 2099+1)
ssp = 'ssp585'
sivol_tas = 'tas'

field = nc4.read_nc4('model_%s_%s'%(ssp, sivol_tas))
field = np.nanmean(field, axis=0)[:85,50:-50,:] #a.sel(latitude=slice(75, -60))

from function_shared_Main import cal_area
area_weight = cal_area(np.arange(0,360,0.5), np.arange(90,-90.001,-0.5)[50:-50])
area_weight = area_weight / np.sum(area_weight)
mean_t2m = np.nansum(field * area_weight, axis=(1,2))

from scipy.stats import linregress
slope, _, _, p_value, _ = linregress(year, mean_t2m)
print(slope)
exit()