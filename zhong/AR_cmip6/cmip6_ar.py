import os
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import nc4
import cmaps
from scipy.stats import linregress

def regrid(sparse_grid, x, y, meshgrid=False):
    from scipy.interpolate import griddata
    if meshgrid:
        x, y = np.meshgrid(x, y)
    x_flat = x.ravel()
    y_flat = y.ravel()
    z_flat = sparse_grid.ravel()

    new_x, new_y = np.meshgrid(np.arange(-180,180,0.5), np.arange(-90,90.001,0.5))
    return griddata((x_flat, y_flat), z_flat, (new_x, new_y), method='nearest')
#
# path_modelName = r'G:\Atmospheric River Database\CMIP6/'
# modelName = os.listdir(path_modelName)
# ssp = ['ssp245', 'ssp585']
# year_begin = 2015
# year_end = 2099
# date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
# date = date[np.in1d(date.month, [12, 1, 2])].strftime('%Y%m')
#
# for i_ssp in ['hist']:
#     # AR_frequency_ssp_model = np.zeros([len(date), 144,192])
#     for i_modelName in modelName:
#
#         AR_frequency_model = np.zeros([year_end-year_begin+1, 361, 720])
#
#         for i_date in date:
#             filename = r'CMIP6_%s_%s_hist_threshold_AR_01field_global_bydatetime_%s.nc'%(i_modelName, i_ssp, i_date)
#             print(filename)
#             data_row = xr.open_dataset(path_modelName + i_modelName+ '/'+filename)
#             data = np.sum(data_row['AR_pathway'].values, axis=0)
#             # regrid
#             data = regrid(data, data_row.longitude.values, data_row.latitude.values)
#
#             loc_year = int(i_date[:4])-year_begin
#             if i_date[4:6] == '12':
#                 loc_year += 1
#             if loc_year > (year_end-year_begin):
#                 continue
#             AR_frequency_model[loc_year] += data
#         nc4.save_nc4(AR_frequency_model, 'G:\Atmospheric River Database\cmip6_frequency/%s_%s'%(i_modelName, i_ssp))
# exit()



def lingress(series, field):
    def save_standard_nc(data, var_name, times, lon=None, lat=None, ll=False):
        """
        :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
        :param var_name: ['t2m', 'ivt', 'uivt']
        :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
        :param lon: list, 720
        :param lat: list, 360
        :param path_save: r'D://'
        :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
        :return:
        """
        import xarray as xr
        import numpy as np

        if lon is None:
            lon = np.arange(-180, 180, 0.5)
        if lat is None:
            lat = np.arange(-90, 90.001, 0.5)

        ds = xr.Dataset()
        ds.coords['time'] = times
        if ll:
            ds.coords["latitude"] = lat
            ds.coords["longitude"] = lon

            for iI_vari, ivari in enumerate(var_name):
                ds[ivari] = (('time', "latitude", "longitude"), np.array(data[iI_vari]))
        else:
            for iI_vari, ivari in enumerate(var_name):
                ds[ivari] = ('time', np.array(data[iI_vari]))

        return ds

    times = pd.date_range('%i-01-01'%year[0], '%i-01-01'%(year[-1]+1), freq='1Y')
    series = save_standard_nc([series], ['var'], times, ll=False)
    field = save_standard_nc([field], ['var'], times, ll=True)

    def lag_linregress_3D(x, y, lagx=0, lagy=0):
        """
        Input: Two xr.Datarrays of any dimensions with the first dim being time.
        Thus the input data could be a 1D time series, or for example, have three
        dimensions (time, lat, lon).
        Datasets can be provided in any order, but note that the regression slope
        and intercept will be calculated for y with respect to x.
        Output: Covariance, correlation, regression slope and intercept, p-value,
        and standard error on regression between the two datasets along their
        aligned time dimension.
        Lag values can be assigned to either of the data, with lagx shifting x, and
        lagy shifting y, with the specified lag amount.
        """

        # 1. Ensure that the data are properly alinged to each other.
        x, y = xr.align(x, y)

        # 2. Add lag information if any, and shift the data accordingly
        if lagx != 0:
            # If x lags y by 1, x must be shifted 1 step backwards.
            # But as the 'zero-th' value is nonexistant, xr assigns it as invalid
            # (nan). Hence it needs to be dropped
            x = x.shift(time=-lagx).dropna(dim='time')

            # Next important step is to re-align the two datasets so that y adjusts
            # to the changed coordinates of x
            x, y = xr.align(x, y)

        if lagy != 0:
            y = y.shift(time=-lagy).dropna(dim='time')
            x, y = xr.align(x, y)

        # 3. Compute data length, mean and standard deviation along time axis:
        n = y.notnull().sum(dim='time')
        xmean = x.mean(dim='time')
        ymean = y.mean(dim='time')
        xstd = x.std(dim='time')
        ystd = y.std(dim='time')

        # 4. Compute covariance along time axis
        cov = ((x - xmean) * (y - ymean)).sum(dim='time') / n

        # 5. Compute correlation along time axis
        cor = cov / (xstd * ystd)

        # 6. Compute regression slope and intercept:
        slope = cov / (xstd ** 2)
        intercept = ymean - xmean * slope

        # 7. Compute P-value and standard error
        # Compute t-statistics
        tstats = cor * np.sqrt(n - 2) / np.sqrt(1 - cor ** 2)
        stderr = slope / tstats

        from scipy.stats import t

        pval = t.sf(np.abs(tstats['var'].values), n['var'].values - 2) * 2


        return cov, cor, slope, intercept, pval, stderr

    cov, cor, slope, intercept, pval, stderr = lag_linregress_3D(series, field)
    return cov['var'].values, cor['var'].values, slope['var'].values, intercept['var'].values, pval, stderr['var'].values

def plot_linregress(trend, filename, p_values=None):
    fig = plt.figure(figsize=[4,5])
    plt.subplots_adjust(top=0.91,
                        bottom=0.15,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=0,central_latitude=90))


    trend[trend == 0] = np.nan
    import matplotlib.colors as colors

    # X, Y, masked_drm = z_masked_overlap(
    #     ax, np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :],
    #     source_projection=ccrs.Geodetic())
    # cb = ax.contourf(X, Y, masked_drm, 51,
    #             cmap='seismic')
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  ,levels=[-0.1,-0.05,-0.025, -0.0001, 0.0001, 0.025,0.05,0.1], extend='both', cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    if p_values is not None:
        p_values[np.abs(trend) < 0.01] = np.nan
    trend[np.abs(trend) < 0.01] = np.nan

    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())

    cb = ax.contourf(lon, lat[-120:], trend[-120:]
                     , cmap='bwr_r', norm=colors.CenteredNorm(),levels=np.arange(-1,1.001,0.1),extend='both',
                     transform=ccrs.PlateCarree())
    cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal',extend='both')
    cbar.set_label('times decades${^-1}$')
    from function_shared_Main import add_longitude_latitude_labels
    add_longitude_latitude_labels(fig, ax, xloc=55,yloc=90)
    cb_ax.set_xticks(np.arange(-1,1.001,0.5))
    if p_values is not None:
        ax.contourf(lon, lat[-120:], np.ma.masked_greater(p_values, 0.01)[-120:],

                    colors='none', levels=[0, 0.01],
                    hatches=[1 * '/', 1 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    ax.stock_img()
    ax.set_title(filename,loc='left')
    plt.show()
    # plt.savefig(path_Save + '%s.png'%filename)
    plt.close()

def plot_total(trend, filename, p_values=None):
    fig = plt.figure(figsize=[4,5])
    plt.subplots_adjust(top=0.91,
                        bottom=0.15,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=0,central_latitude=90))

    import matplotlib.colors as colors

    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())

    cb = ax.contourf(lon, lat[-120:], trend[-120:]
                     , cmap=cmaps.cmocean_deep,levels=np.arange(0,2001,100),extend='both',
                     transform=ccrs.PlateCarree())
    cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal',extend='both')
    cbar.set_label('times')
    from function_shared_Main import add_longitude_latitude_labels
    ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    ax.stock_img()
    # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    add_longitude_latitude_labels(fig, ax, xloc=55,yloc=90)
    ax.set_title(filename, loc='left')
    plt.show()
    plt.savefig('%s.png'%filename)
    plt.close()
# modelName = ['BCC-CSM2-MR', 'CESM2-WACCM', 'CMCC-CM2-SR5', 'IPSL-CM6A-LR', 'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR']
# ssp = ['ssp245', 'ssp585']
# year_begin = 2015
# year_end = 2099
# date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
# date = date[np.in1d(date.month, [12, 1, 2])].strftime('%Y%m')
# lon = np.arange(-180,180,0.5)
# lat = np.arange(-90,90.001,0.5)
# year = np.arange(year_begin, year_end+1)
# # plot_total(nc4.read_nc4('cmip6_Frequency_585'), '(a)')
# # plot_total(nc4.read_nc4('cmip6_Frequency_585')-nc4.read_nc4('cmip6_Frequency'), '(b)')
# for i_ssp in ['ssp585']:
#     data_collection = []
#     for i_modelName in modelName:
#         data = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s'%(i_modelName, i_ssp))[:85]
#         data_collection.append(data)
#         # cov, cor, slope, intercept, pval, stderr = lingress(year, data)
#         # plot_linregress(slope, pval, 'test')
#     # nc4.save_nc4(np.nansum(np.nanmean(data_collection, axis=0), axis=0), 'cmip6_Frequency_585')
#     # data = np.nansum(np.nanmean(data_collection, axis=0), axis=0)
#     # plot_total(data-nc4.read_nc4('cmip6_Frequency'), 'test')
#     cov, cor, slope, intercept, pval, stderr = lingress(year, np.nanmean(data_collection, axis=0))
#     plot_linregress(slope*10, '(d)',pval)
#     exit()




"""
figure6-4
"""
# modelName = ['BCC-CSM2-MR', 'CESM2-WACCM', 'CMCC-CM2-SR5', 'IPSL-CM6A-LR', 'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR']
# ssp = ['ssp245', 'ssp585']
# year_begin = 2015
# year_end = 2099
# date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
# date = date[np.in1d(date.month, [12, 1, 2])].strftime('%Y%m')
# lon = np.arange(-180,180,0.5)
# lat = np.arange(-90,90.001,0.5)
# from function_shared_Main import cal_area
# def area_weighted_mean(data, area_weight):
#     area_weight = area_weight.copy()
#     data = data.copy()
#     area_weight[np.isnan(data)] = 0
#     area_weight = area_weight / np.sum(area_weight)
#     return np.nansum(data * area_weight)
#
# area_weighted = cal_area(np.arange(-180,180,0.5), np.arange(-90,90.001,0.5))
# year = np.arange(year_begin, year_end+1)
# labels = ['SSP2-4.5${^*}$${^*}$${^*}$: slope=0.007, p<0.01',
#           'SSP5-8.5${^*}$${^*}$${^*}$: slope=0.038, p<0.01']
# colors = ['tab:blue', 'tab:red']
# fig, ax = plt.subplots()
#
# type_SOM = ''
# avg_forSSP = [0,0.025]
# save_avg_for_field_analysis = []
# for iI_ssp, i_ssp in enumerate(ssp):
#     data_collection = []
#     for i_modelName in modelName:
#         data = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s'%(i_modelName, i_ssp))[:85]
#         data[:,:-48,:] = np.nan
#         if type_SOM == 'BAF':
#             data[:,:, :360-90] = np.nan #BAF 360-45
#             data[:, :, 360 + 60:] = np.nan  # BAF 360-45
#         elif type_SOM == 'BSW':
#             data[:,:, 40:360+280] = np.nan  # 200
#         data_collection.append([area_weighted_mean(i_data, area_weighted) for i_data in data])
#
#     data_collection = np.array(data_collection)
#     y1, y2 = np.percentile(data_collection, [5,95], axis=0)
#     avg = np.nanmean(data_collection, axis=0)
#     print(avg[0])
#     x = year
#
#
#     y1 = y1+avg_forSSP[iI_ssp]
#     y2 = y2+avg_forSSP[iI_ssp]
#     avg = avg+avg_forSSP[iI_ssp]
#     # _, y1 = fast_moving_average(x_row, y1)
#     # _, y2 = fast_moving_average(x_row, y2)
#     # x, avg = fast_moving_average(x_row, avg)
#
#     ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[iI_ssp])
#     ax.plot(x, avg, label=labels[iI_ssp], color=colors[iI_ssp],linewidth=0.8)
#     save_avg_for_field_analysis.append(avg)
#     # plt.show()
#     slope, intercept, _, p_here, _ = linregress(year, avg)
#     print('hi, s and p', slope, p_here)
#     ax.plot(x, slope*x+intercept, linestyle='--', color=colors[iI_ssp], linewidth=0.8)
#
# # nc4.save_nc4(np.array(save_avg_for_field_analysis), 'save_avg_for_field_analysis_BSW')
# # exit()
# year = np.arange(1980, 2014+1+1)
# colors = ['black']
#
# for iI_ssp, i_ssp in enumerate(['hist']):
#     data_collection = []
#     for i_modelName in modelName:
#         data = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s'%(i_modelName, i_ssp))[:85]
#         data2 = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s' % (i_modelName, 'ssp245'))[0]
#
#         data = np.append(data, [data2], axis=0)
#         data[:,:-48,:] = np.nan
#         if type_SOM == 'BAF':
#             data[:,:, :360-90] = np.nan #BAF 360-45
#             data[:, :, 360 + 60:] = np.nan  # BAF 360-45
#         elif type_SOM == 'BSW':
#             data[:,:, 40:360+280] = np.nan  # 200
#
#         data_collection.append([area_weighted_mean(i_data, area_weighted) for i_data in data])
#
#     data_collection = np.array(data_collection)
#     y1, y2 = np.percentile(data_collection, [5,95], axis=0)
#     avg = np.nanmean(data_collection, axis=0)
#     x = year
#
#     # _, y1 = fast_moving_average(x_row, y1)
#     # _, y2 = fast_moving_average(x_row, y2)
#     # x, avg = fast_moving_average(x_row, avg)
#
#     ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[iI_ssp])
#     ax.plot(x, avg, color=colors[iI_ssp],linewidth=0.8, label='historical: slope=0.0067, p=0.032')
#     print(avg[-1])
#     # plt.show()
#     slope, intercept, _, p_here, _ = linregress(year, avg)
#     print('hi, s and p', slope, p_here)
#     ax.plot(x, (slope*x+intercept), linestyle='--', color=colors[iI_ssp], linewidth=0.8)
# ax.set_ylabel('times km${^-}$${^2}$')
# ax.set_xlabel('year')
# plt.legend(loc='upper left')
# plt.savefig('dissertation_fig6-5.png', dpi=400)
# plt.show()

modelName = ['BCC-CSM2-MR', 'CESM2-WACCM', 'CMCC-CM2-SR5', 'IPSL-CM6A-LR', 'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR']
ssp = ['ssp245', 'ssp585']
year_begin = 2015
year_end = 2099
date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
date = date[np.in1d(date.month, [12, 1, 2])].strftime('%Y%m')
lon = np.arange(-180,180,0.5)
lat = np.arange(-90,90.001,0.5)
from function_shared_Main import cal_area
def area_weighted_mean(data, area_weight):
    area_weight = area_weight.copy()
    data = data.copy()
    area_weight[np.isnan(data)] = 0
    area_weight = area_weight / np.sum(area_weight)
    return np.nansum(data * area_weight)

area_weighted = cal_area(np.arange(-180,180,0.5), np.arange(-90,90.001,0.5))

labels_twotype = [['SSP2-4.5: slope=0.02, p<0.05', 'SSP5-8.5: slope=0.1, p<0.01'],
          ['SSP2-4.5: slope=0.007, p<0.01', 'SSP5-8.5: slope=0.04, p<0.01']]

labels_hist = ['historical: slope=0.027, p=0.04',
               'historical: slope=0.001, p=0.73']


figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

fig, axs = plt.subplots(2,1)
plt.subplots_adjust(top=0.945,
bottom=0.09,
left=0.12,
right=0.98,
hspace=0.335,
wspace=0.22)
type_SOM = 'BAF'
avg_forSSP = [0,0.025]
save_avg_for_field_analysis = []
for iI_type, type_SOM in enumerate(['BAF', 'BSW']):
    labels = labels_twotype[iI_type]
    ax = axs[iI_type]
    for iI_ssp, i_ssp in enumerate(ssp):
        data_collection = []
        for i_modelName in modelName:
            data = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s'%(i_modelName, i_ssp))[:85]
            data[:,:-48,:] = np.nan
            if type_SOM == 'BAF':
                data[:,:, :360-90] = np.nan #BAF 360-45
                data[:, :, 360 + 60:] = np.nan  # BAF 360-45
            elif type_SOM == 'BSW':
                data[:,:, 40:360+280] = np.nan  # 200
            data_collection.append([area_weighted_mean(i_data, area_weighted) for i_data in data])

        year = np.arange(2015, 2099 + 1)
        colors = ['tab:blue', 'tab:red']
        data_collection = np.array(data_collection)
        y1, y2 = np.percentile(data_collection, [5,95], axis=0)
        avg = np.nanmean(data_collection, axis=0)
        print(avg[0])
        x = year


        y1 = y1+avg_forSSP[iI_ssp]
        y2 = y2+avg_forSSP[iI_ssp]
        avg = avg+avg_forSSP[iI_ssp]
        # _, y1 = fast_moving_average(x_row, y1)
        # _, y2 = fast_moving_average(x_row, y2)
        # x, avg = fast_moving_average(x_row, avg)

        ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[iI_ssp])
        ax.plot(x, avg, label=labels[iI_ssp], color=colors[iI_ssp],linewidth=0.8)
        save_avg_for_field_analysis.append(avg)
        # plt.show()
        slope, intercept, _, p_here, _ = linregress(year, avg)
        print('hi, s and p', i_ssp, '  ', slope, p_here)
        ax.plot(x, slope*x+intercept, linestyle='--', color=colors[iI_ssp], linewidth=0.8)

    # nc4.save_nc4(np.array(save_avg_for_field_analysis), 'save_avg_for_field_analysis_BSW')
    # exit()
    year = np.arange(1980, 2014+1+1)
    colors = ['black']

    for iI_ssp, i_ssp in enumerate(['hist']):
        data_collection = []
        for i_modelName in modelName:
            data = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s'%(i_modelName, i_ssp))[:85]
            data2 = nc4.read_nc4('G:\Atmospheric River Database\cmip6_frequency/%s_%s' % (i_modelName, 'ssp245'))[0]

            data = np.append(data, [data2], axis=0)
            data[:,:-48,:] = np.nan
            if type_SOM == 'BAF':
                data[:,:, :360-90] = np.nan #BAF 360-45
                data[:, :, 360 + 60:] = np.nan  # BAF 360-45
            elif type_SOM == 'BSW':
                data[:,:, 40:360+280] = np.nan  # 200

            data_collection.append([area_weighted_mean(i_data, area_weighted) for i_data in data])

        data_collection = np.array(data_collection)
        y1, y2 = np.percentile(data_collection, [5,95], axis=0)
        avg = np.nanmean(data_collection, axis=0)
        x = year

        # _, y1 = fast_moving_average(x_row, y1)
        # _, y2 = fast_moving_average(x_row, y2)
        # x, avg = fast_moving_average(x_row, avg)

        ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[iI_ssp])

        ax.plot(x, avg, color=colors[iI_ssp],linewidth=0.8, label=labels_hist[iI_type])
        print(avg[-1])
        # plt.show()
        slope, intercept, _, p_here, _ = linregress(year, avg)
        print('hi, s and p', 'hist', '  ', slope, p_here)
        ax.plot(x, (slope*x+intercept), linestyle='--', color=colors[iI_ssp], linewidth=0.8)
    ax.set_ylabel('times km${^-}$${^2}$')
    ax.set_xlabel('year')
    ax.legend(loc='upper left')
    ax.set_title(figlabelstr[iI_type], loc='left')
# plt.show()
plt.savefig('dissertation_fig6-5_twotype.png', dpi=400)
plt.show()


