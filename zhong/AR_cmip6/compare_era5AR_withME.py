import os
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import nc4
import cmaps
from scipy.stats import linregress

path_modelName = r'G:\Atmospheric River Database\ERA5/'
modelName = os.listdir(path_modelName)
year_begin = years[0]
year_end = years[-1]
date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
date = date[np.in1d(date.month, months)].strftime('%Y%m')


AR_frequency_model = np.zeros([year_end-year_begin+1, 181, 360])

for i_date in date:
    filename = r'ERA5_hist_hist_threshold_AR_01field_global_bydatetime_%s.nc'%(i_date)
    print(filename)
    data_row = xr.open_dataset(path_modelName + '/'+filename)
    data = np.sum(data_row['AR_pathway'].values, axis=0)

    loc_year = int(i_date[:4])-year_begin
    if i_date[4:6] == '12':
        loc_year += 1
    if loc_year > (year_end-year_begin):
        continue
    AR_frequency_model[loc_year] += data
nc4.save_nc4(AR_frequency_model, r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6\ERA5/frequency_yearly')
exit()

"""
density
"""

import numpy as np
import cartopy.crs as ccrs
import scipy.io as scio
import nc4
import matplotlib.pyplot as plt

SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[5,3.2])

plt.subplots_adjust(top=0.87,
bottom=0.0,
left=0.005,
right=0.995,
hspace=0.14,
wspace=0.0)

ax = fig.add_subplot(1, 2, 1, projection=ccrs.NorthPolarStereo())

# a = scio.loadmat(r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering\SOM/centroid_deinf_1979.mat')
#
# longpath = a['centroid']
# save_labels_bmus = scio.loadmat(r"G:\OneDrive\a_matlab\a_test\test1979_BMUS.mat")['BMUS'][:, -1].flatten()
# longpath =longpath.reshape([longpath.shape[0], 2,int(longpath.shape[1]/2)])
# lon,lat = projection_transform(longpath[:,1], longpath[:,0], 80, xy2ll=True)
#
# longpath[:,1] = lon
# longpath[:,0] = lat
#
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# save_labels_bmus[:] = 1
# plot_centroid(ax, save_labels_bmus, longpath)

def add_longitude_latitude_labels(ax, xloc):
    gl = ax.gridlines(ylocs=[66], xlocs=[0, 60, 120, 179.9999, -60, -120],
                      linewidth=0.3, color='black',crs=ccrs.PlateCarree(),
                      draw_labels=True,
                      x_inline=True, y_inline=True)


    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}


    fig.canvas.draw()
    for ea in gl.ylabel_artists:
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        ea.set_position([165, 66])

    for ea in gl.xlabel_artists:
        ea.set_visible(True)
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        pos = ea.get_position()
        ea.set_position([pos[0], xloc])

add_longitude_latitude_labels(ax, 22)
title = ax.set_title('(a', x=0.05, y=0.94)
title.set_bbox(dict(facecolor='white', edgecolor='white', pad=0))


from matplotlib.colors import ListedColormap
import matplotlib.colors as mcolors
import cmaps

ax = fig.add_subplot(1, 2, 2, projection=ccrs.PlateCarree())

cmap = ListedColormap(cmaps.WhiteBlue.colors[0:-2])
cmap.set_over(cmap.colors[-1])
# lev = [1,5,10,15,20,25,30,35,40,50,60,70,80,90,100,120,140,160,180]
lev = list(np.arange(1,10,0.5))+list(range(10,20,1))+list(range(20,50,5))
lev = list(np.arange(1,20,0.5))
lev = list(list(range(100,400,20))+list(range(400,700,50))+list(range(700,1000,100)))
norm = mcolors.BoundaryNorm(lev, cmap.N)

# density = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\freq_all")
density = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_cmip6\ERA5\frequency_yearly")
density = np.nansum(density, axis=0)[::-1,]

data = density
cb = ax.contourf(np.arange(-180, 180, 1), np.arange(90, -90.001, -1), data,levels=np.arange(0,50,4), extend='max',
            transform=ccrs.PlateCarree())

ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
ax.coastlines()
# cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", ax=ax, shrink=0.6, pad=0.07)
# cbar.set_ticks([100, 220,340,550,950])
# cbar.set_ticklabels(['100','220','340','550','950'])
cbar.set_label('counts')

add_longitude_latitude_labels(ax, 42)
title = ax.set_title('(b)', x=0.05, y=0.94)
title.set_bbox(dict(facecolor='white', edgecolor='white', pad=0))


plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)
plt.savefig('dissertion_figure1.png', dpi=400)
plt.close()
exit()



import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from scipy.stats import linregress
import nc4
# ar = scipy.io.loadmat(r"D:\ar_yearly_YY.mat")['ar_type']

# import nc4

freq = nc4.read_nc4(r"G:\OneDrive\basis\some_projects\zhong\AR_cmip6\ERA5\frequency_yearly")

# for iloop in [['ivt', ivt/freq],
#               ['duration', density/freq],
#               ['freq', freq],
#               ['freq_begin', density_begin/freq]]:

period = np.arange(1980,2020+1)
mutation_year = 2000-1979

data_row = freq

skip_0 = False
data_row[np.isnan(data_row)] = 0

n_lats = data_row.shape[1]
n_lons = data_row.shape[2]

save_name = ['%i_%i' % (period[0], period[-1]), '%i_%i' % (period[0], period[mutation_year]),
             '%i_%i' % (period[mutation_year], period[-1])]
# for iI, data in enumerate([data_row, data_row[:mutation_year], data_row[mutation_year:]]):
for iI, data in enumerate([data_row[:]]):
    trend = np.zeros((n_lats, n_lons))
    p_values = np.ones((n_lats, n_lons))

    for i in range(n_lats):
        for j in range(n_lons):
            s_data = data[:, i, j]
            ######
            # if (density[:,i,j] == 0).any():
            #     print('hi')
            if skip_0:
                s_data = s_data[s_data != 0]
            if len(s_data) != 0:
                slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
                trend[i, j] = slope
                p_values[i, j] = p_value
    # #
    nc4.save_nc4(trend, 'trend')
    nc4.save_nc4(p_values, 'p')
    ################################# plot #########################################
    # trend = nc4.read_nc4(r'trend_YY')
    # plt.imshow(trend)
    # plt.show()
    # p_values = nc4.read_nc4(r'p_YY')

    import cartopy.crs as ccrs
    import cmaps

    fig = plt.figure(figsize=[5, 3.9])
    plt.subplots_adjust(top=0.97,
                        bottom=0.1,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

    import matplotlib.pyplot as plt
    import numpy as np
    import numpy.ma as ma
    import scipy.special as sp
    import cartopy.crs as ccrs


    # trend[np.isnan(trend)] = 0
    trend[trend == 0] = np.nan
    import matplotlib.colors as colors

    # X, Y, masked_drm = z_masked_overlap(
    #     ax, np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :],
    #     source_projection=ccrs.Geodetic())
    # cb = ax.contourf(X, Y, masked_drm, 51,
    #             cmap='seismic')
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  ,levels=[-0.1,-0.05,-0.025, -0.0001, 0.0001, 0.025,0.05,0.1], extend='both', cmap='bwr', norm=colors.CenteredNorm(),
    #                  transform=ccrs.PlateCarree())
    trend[np.abs(trend) < 1.e-3] = 0.
    # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
    #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
    #                  transform=ccrs.PlateCarree())
    cb = ax.pcolormesh(np.arange(0, 360, 1), np.arange(-90, 90.01, 1), trend
                     , cmap='bwr', norm=colors.CenteredNorm(),
                     transform=ccrs.PlateCarree())
    plt.colorbar(cb)
    ax.contourf(np.arange(0, 360, 1), np.arange(-90, 90.01, 1), np.ma.masked_greater(p_values, 0.1),

                colors='none', levels=[0, 0.1],
                hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66], linewidth=0.3, color='black')

    plt.show()
    plt.close()