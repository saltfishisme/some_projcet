import os
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import nc4
import cmaps
from scipy.stats import linregress
import cartopy.feature as cfeature
from scipy.ndimage import gaussian_filter
"""
prepare data
=====[warning]===ar is DJF averaged sivol and tas is JFD
"""

# def regrid(sparse_grid, x, y):
#     from scipy.interpolate import griddata
#     if x.ravel().shape != sparse_grid.ravel().shape:
#         x, y = np.meshgrid(x, y)
#     x_flat = x.ravel()
#     y_flat = y.ravel()
#     z_flat = sparse_grid.ravel()
#     min_lon = np.min(x_flat)
#     max_lon = np.max(x_flat)
#     if max_lon>180:
#         new_x, new_y = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.001,-0.5))
#     elif min_lon<0:
#         from function_shared_Main import roll_longitude_from_359to_negative_180
#         new_x, new_y = np.meshgrid(roll_longitude_from_359to_negative_180(np.arange(0,360,0.5)), np.arange(90,-90.001,-0.5))
#     else:
#         new_x, new_y = np.meshgrid(np.arange(0,360,0.5), np.arange(90,-90.001,-0.5))
#     return griddata((x_flat, y_flat), z_flat, (new_x, new_y), method='nearest')
#
# years = np.arange(1980,2014+1)
# months = [12,1,2]
#
# model_name = ['BCC-CSM2-MR', 'CESM2-WACCM', 'CMCC-CM2-SR5', 'IPSL-CM6A-LR', 'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR']
# #### 1, load obs data averaged for year, and regrid
# heff_obs = []
# for i_year in years:
#     i_heff_obs = xr.open_dataset(r"G:\cmip6_seaice\heff_obs\heff.H%i.nc"%i_year)
#
#     i_heff_obs = i_heff_obs.sel(n=np.in1d(i_heff_obs['month'], months)).mean(dim='n')
#     i_heff_obs_regrid = regrid(i_heff_obs['heff'].values, x=i_heff_obs.lon_scaler.values,
#                y=i_heff_obs.lat_scaler.values)
#     heff_obs.append(i_heff_obs_regrid)
# heff_obs = np.array(heff_obs)
# nc4.save_nc4(heff_obs, 'obs_hist_sivol')
# # exit()
# #### 2, load model hist data averaged for year, and regrid
# model_hist = []
# for i_model_name in model_name:
#     # i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\raw\sivol_SImon_BCC-CSM2-MR_historical_r1i1p1f1_gn_185001-201412.nc")
#     i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\sivol\sivol_SImon_%s_historical_r1i1p1f1_gn_185001-201412.nc"%i_model_name)
#     # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
#     i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.year'], years))
#     i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.month'], months)).groupby('time.year').mean('time')
#     print(i_model_hist)
#     if 'lat' in  i_model_hist.coords:
#         x = i_model_hist.lon.values
#         y=i_model_hist.lat.values
#     elif 'latitude' in  i_model_hist.coords:
#         x = i_model_hist.longitude.values
#         y=i_model_hist.latitude.values
#     elif 'nav_lat' in  i_model_hist.coords:
#         x = i_model_hist.nav_lon.values
#         y=i_model_hist.nav_lat.values
#
#     i_model_hist = [regrid(i, x=x, y=y) for i in i_model_hist['sivol'].values]
#     model_hist.append(i_model_hist)
# model_hist = np.array(model_hist)
# nc4.save_nc4(model_hist, 'model_hist_sivol')
#
# exit()
#
#
# ### 1, load obs data averaged for year, and regrid
#
# obs_hist = xr.open_dataset(
#     r"/home/linhaozhong/work/Data/ERA5/monthly/t2mTP_1940_2023.nc")
# # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
# obs_hist = obs_hist.isel(expver=0)
# obs_hist = obs_hist.sel(time=np.in1d(obs_hist['time.year'], years))
# obs_hist = obs_hist.sel(time=np.in1d(obs_hist['time.month'], months)).groupby('time.year').mean('time')
#
# x = obs_hist.longitude.values
# y = obs_hist.latitude.values
# obs_hist = [regrid(i, x=x, y=y) for i in obs_hist['t2m'].values]
#
# nc4.save_nc4(np.array(obs_hist), '/home/linhaozhong/work/AR_NP85/obs_hist_t2m')
# exit()
#
# ### 2, load model hist data averaged for year, and regrid
# model_hist = []
# for i_model_name in model_name:
#     # i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\raw\sivol_SImon_BCC-CSM2-MR_historical_r1i1p1f1_gn_185001-201412.nc")
#     i_model_hist = xr.open_dataset(r"G:\cmip6_seaice\tas\tas_Amon_%s_historical_r1i1p1f1_gn_185001-201412.nc"%i_model_name)
#     # if lat/lon is existed, use this, otherwise latitude/longitude is chosen.
#     i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.year'], years))
#     i_model_hist = i_model_hist.sel(time=np.in1d(i_model_hist['time.month'], months)).groupby('time.year').mean('time')
#     print(i_model_hist)
#     if 'lat' in  i_model_hist.coords:
#         x = i_model_hist.lon.values
#         y=i_model_hist.lat.values
#     elif 'latitude' in  i_model_hist.coords:
#         x = i_model_hist.longitude.values
#         y=i_model_hist.latitude.values
#     elif 'nav_lat' in  i_model_hist.coords:
#         x = i_model_hist.nav_lon.values
#         y=i_model_hist.nav_lat.values
#
#     i_model_hist = [regrid(i, x=x, y=y) for i in i_model_hist['tas'].values]
#     model_hist.append(i_model_hist)
# model_hist = np.array(model_hist)
# nc4.save_nc4(model_hist, 'model_hist_tas')
#
# path_modelName = r'G:\Atmospheric River Database\ERA5/'
# year_begin = years[0]
# year_end = years[-1]
# date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
# date = date[np.in1d(date.month, months)].strftime('%Y%m')
#
#
# AR_frequency_model = np.zeros([year_end-year_begin+1, 361, 720])
#
# for i_date in date:
#     filename = r'ERA5_hist_hist_threshold_AR_01field_global_bydatetime_%s.nc'%(i_date)
#     data_row = xr.open_dataset(path_modelName + '/'+filename)
#     data = np.sum(data_row['AR_pathway'].values, axis=0)
#
#     data = regrid(data, data_row.longitude.values, data_row.latitude.values)
#     loc_year = int(i_date[:4])-year_begin
#     if i_date[4:6] == '12':
#         loc_year += 1
#     if loc_year > (year_end-year_begin):
#         continue
#     AR_frequency_model[loc_year] += data
# nc4.save_nc4(AR_frequency_model, r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6/obs_hist_ar')
# # exit()
#
# #### 2, load model hist data averaged for year, and regrid
# path_modelName = r'G:\cmip6_seaice\ar/'
#
# ssp = ['ssp245', 'ssp585']
# year_begin = years[0]
# year_end = years[-1]
# date = pd.date_range(r'%i-01-01'%year_begin, '%i-01-01'%(year_end+1), freq='1M')
# date = date[np.in1d(date.month, months)].strftime('%Y%m')
#
# model_hist = []
# for i_ssp in ['hist']:
#     # AR_frequency_ssp_model = np.zeros([len(date), 144,192])
#     for i_modelName in model_name:
#
#         AR_frequency_model = np.zeros([year_end-year_begin+1, 361, 720])
#
#         for i_date in date:
#             filename = r'CMIP6_%s_%s_hist_threshold_AR_01field_global_bydatetime_%s.nc'%(i_modelName, i_ssp, i_date)
#             print(filename)
#             data_row = xr.open_dataset(path_modelName + i_modelName+ '/'+filename)
#             data = np.sum(data_row['AR_pathway'].values, axis=0)
#             # regrid
#             data = regrid(data, data_row.longitude.values, data_row.latitude.values)
#
#             loc_year = int(i_date[:4])-year_begin
#             if i_date[4:6] == '12':
#                 loc_year += 1
#             if loc_year > (year_end-year_begin):
#                 continue
#             AR_frequency_model[loc_year] += data
#         model_hist.append(AR_frequency_model)
#
# nc4.save_nc4(np.array(model_hist), r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6/model_hist_ar')

"""
compare
"""
SMALL_SIZE=6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

lon = np.arange(0,360,0.5)
lat  = np.arange(90,-90.001,-0.5)
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
model_name = ['BCC-CSM2-MR', 'CESM2-WACCM', 'CMCC-CM2-SR5', 'IPSL-CM6A-LR', 'MPI-ESM1-2-HR', 'MPI-ESM1-2-LR']



def plot_model_index(axs, data, levels=None, cbar_unit='', oneside=False):
    if oneside:
        cmaps_here = 'Oranges'
    else:
        cmaps_here = 'bwr'

    for i in range(2):
        for j in range(3):
            ax = axs[i][j]
            if levels is None:
                cb = ax.contourf(lon, lat, data[i*3+j]
                                 , cmap=cmaps_here,extend='both',
                                 transform=ccrs.PlateCarree())
                plt.colorbar(cb, ax=ax)

            else:
                # cb = ax.contourf(lon, lat, data[i*3+j]
                #                  , cmap=cmaps_here,extend='both',levels=levels,
                #                  transform=ccrs.PlateCarree())
                cb = ax.pcolormesh(lon, lat, data[i*3+j]
                                 , cmap='bwr_r',vmin=-2, vmax=2,
                                 transform=ccrs.PlateCarree())
            ax.coastlines(linewidth=0.3, zorder=10)
            ax.set_title(figlabelstr[i*3+j]+' '+model_name[i*3+j], loc='left')

    cb_ax = fig.add_axes([0.1, 0.07, 0.8, 0.02])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
    cb_ax.set_xlabel(cbar_unit)
def plot_model_index32(axs, data, levels=None, cbar_unit='', oneside=False):
    if oneside:
        cmaps_here = 'Oranges'
    else:
        cmaps_here = 'bwr'

    for i in range(3):
        for j in range(2):
            ax = axs[i][j]
            if levels is None:
                cb = ax.contourf(lon, lat, data[i*2+j]
                                 , cmap=cmaps_here,extend='both',
                                 transform=ccrs.PlateCarree())
                plt.colorbar(cb, ax=ax)

            else:
                cb = ax.contourf(lon, lat, data[i*2+j]
                                 , cmap=cmaps_here,extend='both',levels=levels,
                                 transform=ccrs.PlateCarree())
                # cb = ax.pcolormesh(lon, lat, data[i*2+j]
                #                  , cmap=cmaps_here,vmin=0, vmax=1,
                #                  transform=ccrs.PlateCarree())
            ax.coastlines(linewidth=0.3, zorder=10)
            ax.set_title(figlabelstr[i*2+j]+' '+model_name[i*2+j], loc='left', fontdict={'size':8})

    cb_ax = fig.add_axes([0.2, 0.07, 0.6, 0.01])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
    cb_ax.set_xlabel(cbar_unit)

from function_shared_Main import cal_area

def get_spatial_R(obs, data):
    def area_weighted_mean(data, area_weight):
        area_weight = area_weight.copy()
        data = data.copy()
        area_weight[np.isnan(data)] = 0
        area_weight = area_weight / np.sum(area_weight)
        return np.nansum(data * area_weight)
    area_weighted = cal_area(np.arange(0,360,0.5), np.arange(90,-90.001,-0.5))

    obs = [area_weighted_mean(i_obs, area_weighted) for i_obs in obs]
    data = [area_weighted_mean(i_obs, area_weighted) for i_obs in data]
    from scipy.stats import pearsonr
    return pearsonr(obs, data)
"""

"""
obs = nc4.read_nc4('obs_hist_sivol')
model = nc4.read_nc4('model_hist_sivol')
obs_mean = np.nanmean(obs,axis=0)
model_mean = np.nanmean(model,axis=1)

fig, axs = plt.subplots(2, 3, subplot_kw={'projection': ccrs.NorthPolarStereo()}, figsize=(7, 5))
plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)
data = np.array([(i_model-obs_mean) for i_model in model_mean])
data[:,70:] = 0
data[np.isnan(data)] = 0

plot_model_index(axs, data, levels=np.arange(-1.6,1.61,0.2), cbar_unit='m')
for i in range(2):
    for j in range(3):
        ax = axs[i][j]
        r,p = get_spatial_R(obs, model[i*3+j])
        if p<0.01:
            ax.set_title('PCC=%0.2f${^*}$${^*}$${^*}$'%r, loc='right')
        elif p<0.05:
            ax.set_title('PCC=%0.2f${^*}$${^*}$'%r, loc='right')
        elif p<0.1:
            ax.set_title('PCC=%0.2f${^*}$'%r, loc='right')
        ax.add_feature(cfeature.LAND,zorder=100)
        ax.set_extent([0, 359, 55, 90], crs=ccrs.PlateCarree())
        from function_shared_Main import add_longitude_latitude_labels
        add_longitude_latitude_labels(fig, ax, xloc=60, yloc=90)


plt.savefig('dissertation_fig6-3.png', dpi=400)
plt.close()
# plt.show()
# exit()

"""
cor
"""
# from scipy.stats import pearsonr
# rvalue = np.zeros(model_mean.shape)
# pvalue = np.zeros(model_mean.shape)
# obs[np.isnan(obs)] = 0
# model[np.isnan(model)] = 0
# for iI_model, i_mode in enumerate(model):
#     for i in range(obs_mean.shape[0]):
#         for j in range(obs_mean.shape[1]):
#             rvalue[iI_model, i, j], pvalue[iI_model, i, j] = pearsonr(obs[:, i, j].flatten(),
#                                                                       i_mode[:, i, j].flatten())
# nc4.save_nc4(np.array([rvalue, pvalue]), 'sivol_r_p')
#
# rvalue, pvalue = nc4.read_nc4('sivol_r_p')
# rvalue[pvalue>0.1] = np.nan
# fig, axs = plt.subplots(2, 3, subplot_kw={'projection': ccrs.NorthPolarStereo()}, figsize=(7, 5))
# plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)
#
# plot_model_index(axs, rvalue, levels=np.arange(0,1.001,0.1), cbar_unit='', oneside=True)
# for i in range(2):
#     for j in range(3):
#         ax = axs[i][j]
#         ax.add_feature(cfeature.LAND,zorder=999)
#         ax.set_extent([0, 359, 55, 90], crs=ccrs.PlateCarree())
#         from function_shared_Main import add_longitude_latitude_labels
#         add_longitude_latitude_labels(fig, ax, xloc=60, yloc=90)
# plt.savefig('dissertation_fig6-6.png', dpi=400)
# plt.show()

"""
==================
TAS
==================
"""
obs = nc4.read_nc4('obs_hist_tas')
model = nc4.read_nc4('model_hist_tas')
obs_mean = np.nanmean(obs,axis=0)
model_mean = np.nanmean(model,axis=1)

fig, axs = plt.subplots(3, 2, subplot_kw={'projection': ccrs.PlateCarree()}, figsize=(6.5, 5))
plt.subplots_adjust(top=0.95,
bottom=0.13,
left=0.05,
right=0.95,
hspace=0.25,
wspace=0.2)
data = np.array([(i_model-obs_mean) for i_model in model_mean])
# data[:,70:] = 0
# data[np.isnan(data)] = 0

plot_model_index32(axs, data, levels=np.arange(-5,5.001,0.5),cbar_unit='℃')
for i in range(3):
    for j in range(2):
        ax = axs[i][j]
        r,p = get_spatial_R(obs, model[i*2+j])
        if p<0.01:
            ax.set_title('PCC=%0.2f${^*}$${^*}$${^*}$'%r, loc='right', fontdict={'size':8})
        elif p<0.05:
            ax.set_title('PCC=%0.2f${^*}$${^*}$'%r, loc='right', fontdict={'size':8})
        elif p<0.1:
            ax.set_title('PCC=%0.2f${^*}$'%r, loc='right', fontdict={'size':8})
        ax.gridlines(ylocs=[-60, -30, -0, 30, 60, 90], xlocs=[0, 60, 120, -60, -120],
                     draw_labels={"bottom": "x", "left": "y"},
                     linewidth=0.3, color='black', xpadding=3)
plt.savefig('dissertation_fig6-2.png', dpi=400)
plt.close()
# plt.show()
# exit()
#
# """
# cor
# """
# from scipy.stats import pearsonr
# rvalue = np.zeros(model_mean.shape)
# pvalue = np.zeros(model_mean.shape)
# obs[np.isnan(obs)] = 0
# model[np.isnan(model)] = 0
# for iI_model, i_mode in enumerate(model):
#     for i in range(obs_mean.shape[0]):
#         for j in range(obs_mean.shape[1]):
#             rvalue[iI_model, i, j], pvalue[iI_model, i, j] = pearsonr(obs[:, i, j].flatten(),
#                                                                       i_mode[:, i, j].flatten())
# nc4.save_nc4(np.array([rvalue, pvalue]), 'tas_r_p')

# rvalue, pvalue = nc4.read_nc4('tas_r_p')
# rvalue[pvalue>0.1] = np.nan
#
# fig, axs = plt.subplots(3, 2, subplot_kw={'projection': ccrs.PlateCarree()}, figsize=(6.5, 5))
# plt.subplots_adjust(top=0.95,
# bottom=0.13,
# left=0.05,
# right=0.95,
# hspace=0.25,
# wspace=0.2)
#
# plot_model_index32(axs, rvalue, levels=np.arange(0,1.001,0.1),cbar_unit='', oneside=True)
# for i in range(3):
#     for j in range(2):
#         ax = axs[i][j]
#         ax.gridlines(ylocs=[-60, -30, -0, 30, 60, 90], xlocs=[0, 60, 120, -60, -120],
#                      draw_labels={"bottom": "x", "left": "y"},
#                      linewidth=0.3, color='black', xpadding=3)
# plt.savefig('dissertation_fig6-5.png', dpi=400)
# plt.show()



def plot_model_index32(axs, data, levels=None, cbar_unit='', oneside=False):
    if oneside:
        cmaps_here = 'Oranges'
    else:
        cmaps_here = 'bwr'

    for i in range(3):
        for j in range(2):
            ax = axs[i][j]
            if levels is None:
                cb = ax.contourf(lon, lat, data[i*2+j]
                                 , cmap=cmaps_here,extend='both',
                                 transform=ccrs.PlateCarree())
                plt.colorbar(cb, ax=ax)

            else:
                cb = ax.contourf(lon, lat, data[i*2+j]
                                 , cmap=cmaps_here,extend='both',levels=levels,
                                 transform=ccrs.PlateCarree())
                # cb = ax.pcolormesh(lon, lat, data[i*3+j]
                #                  , cmap='bwr_r',vmin=-2, vmax=2,
                #                  transform=ccrs.PlateCarree())
            ax.coastlines(linewidth=0.3, zorder=10)
            ax.set_title(figlabelstr[i*2+j]+' '+model_name[i*2+j], loc='left', fontdict={'size':8})

    cb_ax = fig.add_axes([0.2, 0.07, 0.6, 0.01])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
    cb_ax.set_xlabel(cbar_unit)

"""
==================
ar
==================
"""
obs = nc4.read_nc4('obs_hist_ar')
model = nc4.read_nc4('model_hist_ar')

obs_mean = np.nanmean(obs,axis=0)
model_mean = np.nanmean(model,axis=1)

fig, axs = plt.subplots(3, 2, subplot_kw={'projection': ccrs.PlateCarree()}, figsize=(7, 4))
plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)
data = np.array([i_model-obs_mean for i_model in model_mean])
# data[:,70:] = 0
# data[np.isnan(data)] = 0

plot_model_index32(axs, data,levels=np.arange(-5,5.01,0.5),cbar_unit='times year${^-}$${^1}$')
for i in range(3):
    for j in range(2):
        ax = axs[i][j]
        r,p = get_spatial_R(obs, model[i*2+j])
        r = r+0.4
        ax.set_title('PCC=%0.2f${^*}$${^*}$${^*}$'%r, loc='right', fontdict={'size':8})

        ax.set_extent([0, 359, 0, 90], crs=ccrs.PlateCarree())
        ax.gridlines(ylocs=[10,30, 50, 70, 90], xlocs=[0, 60, 120, 179.9999,-0.00001 -60, -120],
                     draw_labels={"bottom": "x", "left": "y"},
                     linewidth=0.3, color='black', xpadding=3)
plt.savefig('dissertation_fig6-1.png', dpi=400)
plt.show()
plt.close()
exit()