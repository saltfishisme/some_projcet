import os
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import nc4
from scipy.stats import zscore
from scipy.signal import detrend
import cmaps
from scipy.stats import linregress
import cartopy.feature as cfeature
from scipy.ndimage import gaussian_filter


"""
field analysis
"""
def save_standard_nc(data, var_name, times, lon=None, lat=None, ll=False):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360,0.5)
    if lat is None:
        lat = np.arange(90, -90.001, -0.5)

    ds = xr.Dataset()
    ds.coords['time'] = times
    if ll:
        ds.coords["latitude"] = lat
        ds.coords["longitude"] = lon

        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = (('time', "latitude", "longitude"), np.array(data[iI_vari]))
    else:
        for iI_vari, ivari in enumerate(var_name):
            ds[ivari] = ('time', np.array(data[iI_vari]))

    return ds

def lingress(series, field):
    times = pd.date_range('%i-01-01'%year[0], '%i-01-01'%(year[-1]+1), freq='1Y')
    times = times[np.in1d(times.year, year)]
    series = save_standard_nc([series], ['var'], times, ll=False)
    field = save_standard_nc([field], ['var'], times, ll=True)

    def lag_linregress_3D(x, y, lagx=0, lagy=0):
        """
        Input: Two xr.Datarrays of any dimensions with the first dim being time.
        Thus the input data could be a 1D time series, or for example, have three
        dimensions (time, lat, lon).
        Datasets can be provided in any order, but note that the regression slope
        and intercept will be calculated for y with respect to x.
        Output: Covariance, correlation, regression slope and intercept, p-value,
        and standard error on regression between the two datasets along their
        aligned time dimension.
        Lag values can be assigned to either of the data, with lagx shifting x, and
        lagy shifting y, with the specified lag amount.
        """

        # 1. Ensure that the data are properly alinged to each other.
        x, y = xr.align(x, y)

        # 2. Add lag information if any, and shift the data accordingly
        if lagx != 0:
            # If x lags y by 1, x must be shifted 1 step backwards.
            # But as the 'zero-th' value is nonexistant, xr assigns it as invalid
            # (nan). Hence it needs to be dropped
            x = x.shift(time=-lagx).dropna(dim='time')

            # Next important step is to re-align the two datasets so that y adjusts
            # to the changed coordinates of x
            x, y = xr.align(x, y)

        if lagy != 0:
            y = y.shift(time=-lagy).dropna(dim='time')
            x, y = xr.align(x, y)

        # 3. Compute data length, mean and standard deviation along time axis:
        n = y.notnull().sum(dim='time')
        xmean = x.mean(dim='time')
        ymean = y.mean(dim='time')
        xstd = x.std(dim='time')
        ystd = y.std(dim='time')

        # 4. Compute covariance along time axis
        cov = ((x - xmean) * (y - ymean)).sum(dim='time') / n

        # 5. Compute correlation along time axis
        cor = cov / (xstd * ystd)

        # 6. Compute regression slope and intercept:
        slope = cov / (xstd ** 2)
        intercept = ymean - xmean * slope

        # 7. Compute P-value and standard error
        # Compute t-statistics
        tstats = cor * np.sqrt(n - 2) / np.sqrt(1 - cor ** 2)
        stderr = slope / tstats

        from scipy.stats import t

        pval = t.sf(np.abs(tstats['var'].values), n['var'].values - 2) * 2

        return cov['var'].values, cor['var'].values, slope['var'].values, intercept['var'].values, pval, stderr['var'].values

    cov, cor, slope, intercept, pval, stderr = lag_linregress_3D(series, field)
    return cov, cor, slope, intercept, pval, stderr

"""
zg500 and tas

"""
year_all = np.arange(2015, 2099+1)
# year_all = np.arange(1980, 2014+1)
sivol_tas = 'tas'

data = []
data_p = []
type_SOM = 'BAF'
pic_name = '10'
select_year = False

for ssp in ['ssp245', 'ssp585']:
    ## create series_siovl
    def get_series_sivol_ar():
        ar = nc4.read_nc4('model_%s_%s' % (ssp, 'ar'))
        ar = np.nanmean(ar,axis=0)

        field = nc4.read_nc4('model_%s_%s' % (ssp, 'siconc'))[:,:85,:]
        field_1 = nc4.read_nc4('model_%s_%s_9' % (ssp, 'sivol'))[:, :85, :]
        field = field - field_1
        data = np.nanmean(field,axis=0)

        data[ar == 0] = np.nan
        data[:, 80:, :] = np.nan

        if type_SOM == 'BAF':
            data[:,:, 630:] = np.nan #BAF 360-45
            data[:, :, 60:480] = np.nan #BAF 360-120
        elif type_SOM == 'BSW':
            data[:, :, 400:] = np.nan  # 200
            data[:, :, :280] = np.nan  # 140
        # plt.imshow(data[0])
        # plt.show()
        from function_shared_Main import cal_area
        def area_weighted_mean(data, area_weight):
            area_weight = area_weight.copy()
            data = data.copy()
            area_weight[np.isnan(data)] = 0
            area_weight = area_weight / np.sum(area_weight)
            return np.nansum(data * area_weight)
        area_weighted = cal_area(np.arange(0, 360, 0.5), np.arange(90, -90.001, -0.5))
        return np.array([area_weighted_mean(i_data, area_weighted) for i_data in data])

        # return np.nanmean(data, axis=(1,2))
    # if not os.path.exists(r'G:\OneDrive\basis\some_projects\zhong\AR_cmip6\save_avgSIV_for_field_analysis_%s_%s.nc4'%(type_SOM, ssp)):
    #     series_ar = get_series_sivol()
    #     nc4.save_nc4(series_ar, 'save_avgSIV_for_field_analysis_%s_%s'%(type_SOM, ssp))
    # else:
    #     series_ar = nc4.read_nc4('save_avgSIV_for_field_analysis_%s_%s'%(type_SOM, ssp))
    #     print(series_ar)
    series_ar = get_series_sivol_ar()
    nc4.save_nc4(series_ar, 'save_avgSIV_for_field_analysis_%s_%s' % (type_SOM, ssp))
    # series_ar = nc4.read_nc4('save_avg_for_field_analysis_%s'%type_SOM) # in cmip6_ar.py, area-averaged.
    # if ssp == 'ssp245':
    #     series_ar = series_ar[0]
    # else:
    #     series_ar = series_ar[1]
    series_ar = zscore(series_ar)
    if select_year:
        # indices_HIGH = np.argwhere(series_ar>1).flatten()
        # indices = np.argwhere(series_ar <-1).flatten()
        indices_HIGH = np.argpartition((series_ar), -15)[-15:]
        indices = np.argpartition((series_ar), 15)[:15]
        print(series_ar[indices])
        print(series_ar)
    else:
        indices = np.arange(len(year_all))

        # indices = np.argpartition(detrend(series_ar), 84)[:84]
    indices = list(sorted(indices))
    year = year_all[indices]
    print(list(year))

    series_ar = series_ar[indices]

    if type_SOM == 'BAF':
        field = nc4.read_nc4('model_%s_%s_3' % (ssp, sivol_tas))
    else:
        field = nc4.read_nc4('model_%s_%s' % (ssp, sivol_tas))

    field = np.nanmean(field, axis=0)

    # field = detrend(field, axis=0,type='linear')
    # field_low = field[indices]
    # field_high = field[indices_HIGH]
    #
    # from scipy.stats import ttest_ind
    # _,pval=ttest_ind(field_low, field_high, axis=0)
    # field_low = np.nanmean(field_low, axis=0)
    # field_high = np.nanmean(field_high, axis=0)
    # slope = field-field_high

    # _, _, slope, _, pval, _ = lingress(zscore(series_ar), zscore(field))
    # nc4.save_nc4(series_ar, 'D:/test_for_series')
    # nc4.save_nc4( zscore(detrend(field[indices], axis=0,type='linear')), 'D:/test_for_field')
    # exit()
    _, _, slope, _, pval, _ = lingress(series_ar, zscore(detrend(field[indices], axis=0,type='linear')))
    data.append(slope)
    data_p.append(pval)

data = np.array(data)
data_p = np.array(data_p)


SMALL_SIZE=6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize

lon = np.arange(0,360,0.5)
lat  = np.arange(90,-90.001,-0.5)
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

def plot_model_index32(axs, data, levels=None, cbar_unit='', oneside=False):
    if oneside:
        cmaps_here = 'Oranges'
    else:
        cmaps_here = 'bwr_r'

    for i in range(2):
            ax = axs[i]
            if levels is None:
                cb = ax.contourf(lon, lat, data[0][i]
                                 , cmap=cmaps_here,extend='both',
                                 transform=ccrs.PlateCarree())
                cbar = fig.colorbar(cb, ax=ax, orientation='horizontal', extend='both',
                                    shrink=0.6, pad=0.08, aspect=40)

            else:
                cb = ax.contourf(lon, lat, data[0][i]
                                 , cmap=cmaps_here,extend='both',levels=levels,
                                 transform=ccrs.PlateCarree())
                # cb = ax.pcolormesh(lon, lat, data[i*3+j]
                #                  , cmap='bwr_r',vmin=-2, vmax=2,
                #                  transform=ccrs.PlateCarree())
                cb_ax = fig.add_axes([0.2, 0.07, 0.6, 0.01])
                cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal', extend='both')
                cb_ax.set_xlabel(cbar_unit)
            ax.contourf(lon, lat, np.ma.masked_greater(data[1][i], 0.1),

                        colors='none', levels=[0, 0.1],
                        hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
            ax.coastlines(linewidth=0.3, zorder=10)



fig, axs = plt.subplots(2, 1, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)}, figsize=(6, 4))
plt.subplots_adjust(top=0.95,bottom=0.11,left=0.05,right=0.95,hspace=0.2,wspace=0.2)

title = ['(a) SSP2-4.5','(b) SSP5-8.5']
plot_model_index32(axs, [data, data_p],levels=np.arange(-1,1.001,0.1), cbar_unit='')


for i in range(2):

    ax = axs[i]
    ax.set_title(title[i], loc='left', fontdict={'size': 8})

    ax.set_extent([0, 359, 0, 90], crs=ccrs.PlateCarree())
    ax.gridlines(ylocs=[10,30, 50, 70, 90], xlocs=[0, 60, 120, 179.9999,-0.00001 -60, -120],
                 draw_labels={"bottom": "x", "left": "y"},
                 linewidth=0.3, color='black', xpadding=3)

plt.savefig('dissertation_fig6-%s.png'%pic_name, dpi=400)
plt.show()
plt.close()
exit()
