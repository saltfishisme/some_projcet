from scipy.ndimage import gaussian_filter
from shapely.geometry import Polygon
from geopy.distance import distance
from fil_finder import FilFinder2D
from skimage import measure
import matplotlib.pyplot as plt
import astropy.units as u
import logging.handlers
import scipy.io as scio
import pandas as pd
import xarray as xr
import numpy as np
import traceback
import cv2, os
import nc4


de_quantile_field_Name = 'daily_85'     # [4, lat, lon]    ######
path_IVT_Data  = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_Proj_Data = r'/home/linhaozhong/work/AR_NP85_1940/'  ######

os.makedirs(path_Proj_Data, exist_ok=True)
path_daily_AR_save = path_Proj_Data + 'daily_AR/'
os.makedirs(path_daily_AR_save, exist_ok=True)

bu_sI_Season = [0]  ###### needed to re-check
bu_time_Seaon_divide = [[12, 1, 2]]  ######
bu_time_Seaon_divide_Name = ['winter']  #####


bu_study_box = [np.arange(0, 360, 0.5), np.arange(70, 90.4, 0.5)]


resolution = 0.5
de_branch_ratio  = 0.7
de_branch_number = 5
de_Temporal_resolution = 6
de_the_number_of_grids = 400  # to remove the AR_detection pathway with the number of grids is smaller 400
time_loop = pd.date_range('1940-01-01', '2022-12-31', freq='1D')

bu_Data_begin_year = '1939'  # if ERA5 is from 1979, then set it as '1978'
bu_main_period_length_all = [4]  # a AR_detection event should last at least 3 times (Temporal_resolution is 6h
bu_lack_period_length_all = [2]
#  In an ARS, their temporal discontinuity is allowed up to 2 times (12 hr), as long as two AR_detection pathways partially overlap.
bu_Area_Protion_for_connective_contour = 0.4  # two AR_detection pathways partially overlap
bu_the_number_of_grids = 400  # to remove the AR_detection pathway with the number of grids is smaller 400

slong = np.arange(0, 180, 0.5)
IVT_long = np.append(slong, np.arange(-180, 0, 0.5))
IVT_lat  = np.arange(90, -90.25, -0.5)
bu_roll_num = int(len(IVT_long) / 2)


#######################################################################################################
def daily_AR_detection(row_data, IVT_lat, IVT_long, given_threshold):
    '''
    :param data: np.array, 2D data.
    :param lat, lon, resolution: lat(list or array-like), lon(list or array-like) and resolution(float) of the data.
    :param given_threshold: float, the minimum value which can be marked as 'high_value'.
    :param numll_min: float, the points in one high_value region should be more than numll_min, otherwise, this region will be filtered.
    :return: array, shape is [the number of high_value area, 7],
            the first five indexes are  x_centre, y_centre, width, height, angle,
            The sixth and seventh indices are area of fitted ellipse and high_value region.
            The eighth and ninth indices are averaged wind speed of fitted ellipse and high_value region
    '''

    data = row_data - given_threshold
    data = gaussian_filter(data, sigma=1)

    def cal_contour(data, roll=False):
        ####### prepare data
        s_IVT_long = IVT_long.copy()
        if roll != False:
            data = np.roll(data, roll, axis=1)
            s_IVT_long = np.roll(IVT_long, roll)
        thresh = cv2.threshold(data, 1, 255, cv2.THRESH_BINARY)[1]
        labels = measure.label(thresh, connectivity=1, background=0)
        #######
        Mask_onePic = np.zeros(data.shape, dtype='uint8')
        lineMask_onePic = np.zeros(data.shape, dtype='uint8')
        contourMask = []
        lineMask = []
        s_df_result = pd.DataFrame()
        if roll != False:
            num_AR = 1000
        else:
            num_AR = 0

        ''' #########################   loop for every contour in this image   ###########################'''
        for label in np.unique(labels):
            if label == 0:
                continue

            #######  prepare labelMask, labelMask is a narray contains one contour
            labelMask = np.zeros(thresh.shape, dtype="uint8")
            labelMask[labels == label] = 255
            numPixels = cv2.countNonZero(labelMask)
            ####### get index of this contour
            ar_pathway_index = np.argwhere(labelMask != 0)

            '''' ####### numPixels , longitude and latitude that a AR_detection event must spread across '''
            if roll != False:
                if (np.in1d(np.unique(ar_pathway_index[:, 1]), [roll])).any() == False:
                    continue
            else:
                if (np.in1d(np.unique(ar_pathway_index[:, 1]), [0, thresh.shape[1] - 1])).any():
                    continue

            if not ((np.in1d(s_IVT_long[np.unique(ar_pathway_index[:, 1])], bu_study_box[0]).any()) & (
                    np.in1d(IVT_lat[np.unique(ar_pathway_index[:, 0])], bu_study_box[1]).any())):
                continue

            if (numPixels > de_the_number_of_grids):
                simple = labelMask
                simple[simple == 255] = 1
                ######## sketetons
                fil = FilFinder2D(simple, distance=1 * u.pix, mask=simple)
                fil.preprocess_image(skip_flatten=True)
                fil.create_mask(border_masking=True, verbose=False,
                                use_existing_mask=True)
                fil.medskel(verbose=False)
                result = fil.analyze_skeletons(branch_thresh=8 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')

                # print(fil.lengths())
                # print(fil.lengths()/np.sum(fil.branch_properties['length']))
                # # print(fil.branch_properties)
                # exit(0)
                # print(fil.curvature_branches())
                # plt.imshow(simple, cmap='gray')
                # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                # plt.axis('off')
                # plt.show()

                '''###################  criteria ################################### '''

                #######
                if (fil.branch_properties['number'] > de_branch_number):
                    # print('kill by shape')
                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()
                    continue
                ######
                if ((fil.lengths() / np.sum(fil.branch_properties['length'])) < de_branch_ratio * u.pix):
                    # print('kill by ratio')
                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()
                    continue

                # plt.imshow(labelMask, cmap='gray')
                # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                # plt.axis('off')
                # plt.show()

                #######
                sslabels = measure.label(labelMask, connectivity=1, background=0)
                regions = measure.regionprops(sslabels, intensity_image=row_data)
                length = regions[0].axis_major_length
                width = regions[0].axis_minor_length
                if length / width < 2:
                    # plt.imshow(labelMask)
                    # plt.title('kill by length/width')
                    # plt.show()
                    continue

                #######
                try:
                    fil.analyze_skeletons(branch_thresh=40 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')
                except:
                    continue
                ll_loc = np.array(fil.end_pts)[0]
                lat_loc = IVT_lat[ll_loc[:, 0]]
                lon_loc = s_IVT_long[ll_loc[:, 1]]
                try:
                    length = distance((lat_loc[0], lon_loc[0]), (lat_loc[1], lon_loc[1])).km
                    if length < 2000:
                        continue
                except:
                    print('error')

                contourMask.append(np.array(labelMask))
                lineMask.append(np.array(fil.skeleton_longpath, dtype="uint8"))
                Mask_onePic = cv2.add(Mask_onePic, labelMask)
                lineMask_onePic = cv2.add(lineMask_onePic, np.array(fil.skeleton_longpath, dtype='uint8'))

                num_AR += 1
                contours = measure.find_contours(labelMask)[0]
                s_df = pd.DataFrame()

                def array_to_str(array):
                    saveStr = ''
                    for i in array:
                        saveStr += ',' + str(i)
                    return saveStr[1:]

                s_df['y'] = [array_to_str(contours[:, 0])]
                s_df['x'] = [array_to_str(contours[:, 1])]

                s_df['time'] = df_time
                s_df['num'] = num_AR
                s_df_result = s_df_result.append(s_df, ignore_index=True)

        if roll != False:
            thresh = np.roll(thresh, -roll, axis=1)
            lineMask_onePic = np.roll(lineMask_onePic, -roll, axis=1)
            Mask_onePic = np.roll(Mask_onePic, -roll, axis=1)
            if num_AR > 1000:
                contourMask = np.array(contourMask)
                lineMask = np.array(lineMask)
                for s_lineMask in range(len(lineMask)):
                    contourMask[s_lineMask] = np.roll(contourMask[s_lineMask], -roll, axis=1)
                    lineMask[s_lineMask] = np.roll(lineMask[s_lineMask], -roll, axis=1)
        return num_AR, s_df_result, contourMask, lineMask, thresh, lineMask_onePic

    # cal contour for row map
    num_AR, s_df_result, mask_ss, lineMask, thresh, lineMask_M = cal_contour(data)

    # cal contour for rolled map
    s_roll_number = int(data.shape[1] / 2)
    num_AR_cross, s_df_result_cross, mask_ss_cross, lineMask_cross, thresh_cross, lineMask_M_cross = cal_contour(data,
                                                                                                                 roll=s_roll_number)

    if (num_AR > 0) & (num_AR_cross > 1000):
        mask_ss.extend(mask_ss_cross)
        nc4.save_nc4(np.array(mask_ss), path_daily_AR_save + s_time + '_%02i' % timestep)
    elif (num_AR > 0) & (num_AR_cross <= 1000):
        nc4.save_nc4(np.array(mask_ss), path_daily_AR_save + s_time + '_%02i' % timestep)
    elif (num_AR <= 0) & (num_AR_cross > 1000):
        nc4.save_nc4(np.array(mask_ss_cross), path_daily_AR_save + s_time + '_%02i' % timestep)
    if num_AR_cross > 1000:
        s_df_result = s_df_result.append(s_df_result_cross, ignore_index=True)
        num_AR = num_AR + num_AR_cross - 1000
    if num_AR != 0:
        # fig, ax = plt.subplots(ncols=2, nrows=1, figsize=(5,1.8))
        # plt.subplots_adjust(top=0.96,
        #                     bottom=0.11,
        #                     left=0.01,
        #                     right=0.99,
        #                     hspace=0.2,
        #                     wspace=0.17)
        # ax[0].imshow(thresh, vmin=0)
        # ax[1].imshow(lineMask_M)
        # plt.title(s_time+' '+str(timestep))
        # ax[0].axhline(40, linewidth=0.4)
        # ax[0].axhline(60, linewidth=0.4)
        # # ax[2].imshow(lineMask)
        # #plt.show()
        # plt.savefig(path_SaveData+'pic/'+s_time+'_'+str(timestep)+'.png', dpi=500)
        # plt.close()
        return s_df_result


########################################################################################################
''' ####### logger '''
# logger = logging.getLogger('mylogger')
# logger.setLevel(logging.DEBUG)
# f_handler = logging.FileHandler('%sOF_log_step01.log' % path_Proj_Data, mode='w')
# f_handler.setLevel(logging.DEBUG)
# f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))
# logger.addHandler(f_handler)

# for sIloc_Season in range(len(bu_time_Seaon_divide)):
#     s_Season = bu_time_Seaon_divide[sIloc_Season]
#     df_result = pd.DataFrame(columns=['time', 'num', 'y', 'x'])
#     s_time_loop = time_loop[np.in1d(time_loop.month, s_Season)]
#     dual_threshold = nc4.read_nc4(path_Proj_Data + de_quantile_field_Name)[bu_sI_Season[sIloc_Season]]
#
#     for s_time in s_time_loop.strftime('%Y%m%d'):
#         print(s_time)
#         index_vivt = xr.open_dataset(
#             path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
#                 s_time[:4], s_time))
#         index_uivt = xr.open_dataset(
#             path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
#                 s_time[:4], s_time))
#         print(index_uivt['p71.162'])
#         # index_vivt = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data/NorthWater.%s.nc'%( s_time))
#         # index_uivt = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data/EastWaterVapor.%s.nc'%(s_time))
#
#         for s_time_daily in range(int(24 / de_Temporal_resolution)):
#             timestep = (s_time_daily + 1) * de_Temporal_resolution - 1
#             df_time = index_vivt['p72.162'].isel(time=timestep).time.values
#             ivt_north = index_vivt['p72.162'].values[timestep]
#             ivt_east = index_uivt['p71.162'].values[timestep]
#             ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
#             IVT_long = index_uivt.longitude.values
#             IVT_lat = index_uivt.latitude.values
#
#             try:
#                 s_df_result = daily_AR_detection(ivt_total, IVT_lat, IVT_long, dual_threshold)
#             except Exception as e:
#                 logger.error(s_time)
#                 logger.error(traceback.format_exc())
#                 continue
#             df_result = df_result.append(s_df_result)
#
#     df_result.to_csv(path_Proj_Data + 'daily_AR_%s.csv' % (bu_time_Seaon_divide_Name[sIloc_Season]))


####################################################################################################


print("Step01 is finished, Step02 is started........")


#####################################################################################################

''' ####### logger '''
import logging.handlers
import traceback
logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)
f_handler = logging.FileHandler('%sOF_log_step02.log' % path_Proj_Data, mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))
logger.addHandler(f_handler)

for bu_main_period_length in bu_main_period_length_all:
    for bu_lack_period_length in bu_lack_period_length_all:

        for season in bu_time_Seaon_divide_Name:
            df_row = pd.read_csv(path_Proj_Data + r'daily_AR_%s.csv' % (season), parse_dates=[0], usecols=[1, 2, 3, 4],
                                 infer_datetime_format=True, keep_date_col=True)


            def prepare_data(df):
                df['diff'] = df['time'].diff() / np.timedelta64(6, 'h')
                df['diff'] = df['diff'].fillna(0)
                df['index'] = np.zeros(df.shape[0])
                ####### select
                df['index'][df['diff'] <= bu_lack_period_length] = 1
                return df


            def detection(df_prepared):
                main_period_length = (df_prepared['time'].iloc[[0, -1]].diff() / np.timedelta64(6, 'h') + 1).values[1]
                if (main_period_length < bu_main_period_length) | (df_prepared.shape[0] <= 3):
                    return np.zeros(df_prepared.shape[0])

                result_1000 = detection_by_timeSeries(df_prepared.copy(), roll_1000=True)
                df_prepared_Out1000 = df_prepared[(result_1000 == 0) & (df_prepared['num'] < 1000)].copy()
                # whether df_prepared_Out1000 need to be re-grouped
                df_prepared_Out1000 = prepare_data(df_prepared_Out1000)

                # regroupby from 0
                num_0 = [0]
                # print(df_prepared_Out1000)
                if np.isin(df_prepared_Out1000['index'].values, [0]).any():
                    # print(np.argwhere(df_prepared_Out1000['index'].values == 0))
                    num_0 = list(np.argwhere(df_prepared_Out1000['index'].values == 0)[0])

                num_0 = np.unique([0] + num_0 + [len(df_prepared_Out1000)])
                num_start = np.max(result_1000)
                result_less1000 = []
                for i_num_0 in range(len(num_0))[:-1]:
                    s_result = detection_by_timeSeries(
                        df_prepared_Out1000.iloc[num_0[i_num_0]:num_0[i_num_0 + 1]].copy())
                    s_result[s_result > 0] = s_result[s_result > 0] + num_start
                    num_start = np.max(s_result)
                    result_less1000.extend(s_result.copy())
                result = np.array(result_1000)
                result_less1000 = np.array(result_less1000)
                result[(result_1000 == 0) & (df_prepared['num'] < 1000)] = result_less1000
                return result


            def detection_by_timeSeries(df, roll_1000=False):
                def create_polygon(p1_x, p1_y, p2_x, p2_y, num_12=[0, 0], given_p2xy=5):
                    '''
                    :param p1_x: String,
                    :param p1_y:
                    :param p2_x:
                    :param p2_y:
                    :param given_p2xy: if p2_x and p2_y dont need to pass function to_float()
                    :return: Spatial overlap, 0-1
                    '''

                    def to_float(String):
                        sdata = (String.split(','))
                        sdata = [float(item) for item in sdata]
                        return np.array(sdata)

                    def cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y):
                        if (len(p1_x) < 3) | (len(p2_x) < 3):
                            return 0
                        p1 = Polygon(zip(p1_x, p1_y));
                        p2 = Polygon(zip(p2_x, p2_y))
                        # if NorthPolar !=False:
                        # import matplotlib.pyplot as plt
                        # plt.plot(*p1.exterior.xy)
                        # plt.plot(*p2.exterior.xy)
                        # plt.show()
                        try:
                            pp = p1.intersection(p2)
                        except:
                            return 0
                        try:
                            return np.nanmax([pp.area / p1.area, pp.area / p2.area])
                        except:
                            return 0

                    p1_x = to_float(p1_x)
                    p1_y = to_float(p1_y)
                    if given_p2xy == 5:
                        p2_x = to_float(p2_x)
                        p2_y = to_float(p2_y)
                    if roll_1000:
                        # if num is bigger than 1000, then need to roll. for a num_1000 AR, split it into two x,y,
                        # one is in left of figure, and the other is in the right.
                        if (num_12[0] > 1000) & (num_12[1] > 1000):
                            return cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y)
                        elif num_12[0] > 1000:
                            p1_x = p1_x - bu_roll_num
                            p1_x[p1_x < 0] = p1_x[p1_x < 0] + (bu_roll_num * 2)
                            p1_x_1 = p1_x[p1_x < bu_roll_num]
                            p1_y_1 = p1_y[p1_x < bu_roll_num]
                            p1_x_2 = p1_x[p1_x > bu_roll_num]
                            p1_y_2 = p1_y[p1_x > bu_roll_num]
                            ratio_1 = cal_overlap_ratio(p1_x_1, p1_y_1, p2_x, p2_y)
                            ratio_2 = cal_overlap_ratio(p1_x_2, p1_y_2, p2_x, p2_y)
                            return ratio_1 + ratio_2
                        elif num_12[1] > 1000:
                            p2_x = p2_x - bu_roll_num
                            p2_x[p2_x < 0] = p2_x[p2_x < 0] + (bu_roll_num * 2)
                            p2_x_1 = p2_x[p2_x < bu_roll_num]
                            p2_y_1 = p2_y[p2_x < bu_roll_num]
                            p2_x_2 = p2_x[p2_x > bu_roll_num]
                            p2_y_2 = p2_y[p2_x > bu_roll_num]
                            ratio_1 = cal_overlap_ratio(p2_x_1, p2_y_1, p1_x, p1_y)
                            ratio_2 = cal_overlap_ratio(p2_x_2, p2_y_2, p1_x, p1_y)
                            return ratio_1 + ratio_2
                        else:
                            return cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y)
                    else:
                        return cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y)

                df['AR_detection'] = np.zeros(df.shape[0])
                time_unique = df['time'].unique()
                diff_unique = df['diff'][df['num'] == 1]

                ############### **************************************
                result = np.empty([int(len(df['num'].unique())), len(time_unique)], dtype='int') * np.nan

                for s_i in range(len(time_unique))[:-1]:
                    # print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
                    # print('s_i', time_unique[s_i])
                    polygon_list_Now = df[['x', 'y']][df['time'] == time_unique[s_i]].values
                    polygon_list_Next = df[['x', 'y']][df['time'] == time_unique[s_i + 1]].values
                    num_list_now = df['num'][df['time'] == time_unique[s_i]].values.astype('int')
                    num_list_Next = df['num'][df['time'] == time_unique[s_i + 1]].values.astype('int')
                    s_List = []
                    for sI_xy_now, s_xy_now in enumerate(polygon_list_Now):
                        for sI_xy_next, s_xy_next in enumerate(polygon_list_Next):
                            ######## pass error with xy

                            s_index = create_polygon(s_xy_now[0], s_xy_now[1], s_xy_next[0], s_xy_next[1],
                                                     [num_list_now[sI_xy_now], num_list_Next[sI_xy_next]])

                            ''' ##########################   calculation #############################'''
                            if s_index > 0.1:
                                loc = 0
                                for s_result_nan in range(result.shape[0]):
                                    if result[s_result_nan, s_i] == (num_list_now[sI_xy_now]):
                                        # print('1')
                                        result[s_result_nan, s_i + 1] = num_list_Next[sI_xy_next]
                                        loc = 1
                                        break
                                if loc == 0:
                                    for s_result_nan in range(result.shape[0]):
                                        # # print('???', result[s_result_nan, s_i])
                                        if np.nansum(result[s_result_nan]) != 0:
                                            # # print('are you here?')
                                            # if too much nan contains ,skip.
                                            new_iloc_xy = np.argwhere([~np.isnan(result[s_result_nan])])[-1][-1]
                                            # # print(int(result[s_result_nan, new_iloc_xy]), 'new')
                                            new_iloc_xy_values = df[['x', 'y']][
                                                (df['time'] == time_unique[new_iloc_xy]) & (df['num'] == int(
                                                    result[s_result_nan, new_iloc_xy]))].values[0]
                                            new_iloc_num = df['num'][(df['time'] == time_unique[new_iloc_xy]) & (
                                                    df['num'] == int(result[s_result_nan, new_iloc_xy]))]
                                            s_diff = diff_unique.iloc[new_iloc_xy + 1:s_i + 1].values
                                            # # print('diff', diff_unique.iloc[new_iloc_xy+1:s_i])
                                            # # print('diff', diff_unique_test.iloc[new_iloc_xy+1:s_i])
                                            # # print('sum_diff', np.sum(s_diff))
                                            if np.sum(s_diff) > bu_lack_period_length:
                                                continue

                                            s_index = create_polygon(new_iloc_xy_values[0], new_iloc_xy_values[1],
                                                                     s_xy_now[0], s_xy_now[1], [new_iloc_num.values[0],
                                                                                                num_list_now[
                                                                                                    sI_xy_now]])
                                            if s_index > 0.1:
                                                # print('2',s_index)
                                                result[s_result_nan, s_i] = num_list_now[sI_xy_now]
                                                result[s_result_nan, s_i + 1] = num_list_Next[sI_xy_next]
                                                loc = 1
                                                break
                                if loc == 0:
                                    for s_result_nan in range(result.shape[0]):
                                        if np.nansum(result[s_result_nan]) == 0:
                                            # # print(result[s_result_nan])
                                            # print('3')
                                            result[s_result_nan, s_i] = num_list_now[sI_xy_now]
                                            result[s_result_nan, s_i + 1] = num_list_Next[sI_xy_next]
                                            loc = 1
                                            break
                                if loc == 0:
                                    result = np.append(result, [np.ones(result.shape[1]) * np.nan], axis=0)
                                    # print('4')
                                    result[-1, s_i] = num_list_now[sI_xy_now]
                                    result[-1, s_i + 1] = num_list_Next[sI_xy_next]
                                    loc = 1
                                    break

                ####### useless
                if bu_NorthPolar_protion != False:
                    for s_result_nan in range(result.shape[0]):
                        area_protion = []
                        if np.nansum(result[s_result_nan]) != 0:
                            s_result = result[s_result_nan]
                            for new_iloc_xy in range(len(s_result)):
                                if np.isnan(s_result[new_iloc_xy]) != 1:
                                    new_iloc_xy_values = df[['x', 'y']][(df['time'] == time_unique[new_iloc_xy]) & (
                                            df['num'] == int(result[s_result_nan, new_iloc_xy]))].values[0]
                                    s_index = create_polygon(new_iloc_xy_values[0], new_iloc_xy_values[1],
                                                             [146, 270, 270, 146], [174, 174, 74, 74], given_p2xy=True)
                                    area_protion.append(s_index)

                            area_protion = np.array(area_protion)
                            if len(area_protion[area_protion > 0.3]) < 2:
                                result[s_result_nan] = np.zeros(result[s_result_nan].shape)

                for s_result_nan in range(result.shape[0]):
                    s_result = result[s_result_nan]
                    new_iloc_xy = np.argwhere([~np.isnan(s_result.flatten())])

                    if len(new_iloc_xy) >= 2:
                        begin = new_iloc_xy[0][-1];
                        end = new_iloc_xy[-1][-1]
                        main_period_length = (time_unique[end] - time_unique[begin]) / np.timedelta64(6, 'h') + 1
                        if roll_1000:
                            if (len(s_result[s_result > 1000]) == 0):
                                continue
                        if (main_period_length >= bu_main_period_length):
                            min_lat = []
                            for iI_loc, iloc in enumerate(s_result):
                                if np.isnan(iloc):
                                    a = 0
                                else:
                                    time_loc = time_unique[iI_loc]
                                    df.loc[(df['time'] == time_loc) & (
                                            df['num'] == iloc), 'AR_detection'] = s_result_nan + 1
                        else:
                            result[s_result_nan] = np.zeros(result[s_result_nan].shape)

                return df['AR_detection'].values


            bu_NorthPolar_protion = False
            df_prepared = prepare_data(df_row.copy())
            a = df_prepared.groupby(df_prepared['index'].eq(0).cumsum()).apply(lambda x: detection(x)).reindex()
            a = a.values.flatten()

            ##########
            b = []
            for i in a:
                b.extend(i)
            b = np.array(b)
            df_prepared['AR'] = b
            df_prepared = df_prepared[['time', 'num', 'index', 'AR']]
            df_prepared.to_csv(
                path_Proj_Data + 'AR_timeSeries_%s%i%i.csv' % (season, bu_main_period_length, bu_lack_period_length),
                index=False)

        '''#################################  find all AR_detection ############################################'''


        def centroid(charater_AR, s_time):
            def direction(u, v):

                a = np.arctan2(v, u) * 180 / np.pi
                if a < 0:
                    a = 360 + a
                a0 = 0
                if u > 0:
                    a0 = 180
                return 90 - a + a0

            def load_ivt_total(s_time):
                '''
                :param s_time: datetime64
                :return: ivt_total
                '''
                s_time = pd.to_datetime(s_time)
                s_time_str = s_time.strftime('%Y%m%d')
                index_vivt = xr.open_dataset(
                    path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
                    s_time_str[:4], s_time_str))
                index_uivt = xr.open_dataset(
                    path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
                    s_time_str[:4], s_time_str))
                # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
                # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

                ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
                ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

                ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
                return ivt_total, ivt_east, ivt_north

            ivt_total, u, v = load_ivt_total(s_time)
            ivt_total[charater_AR == 0] = np.nan
            s_area = area.copy()
            s_area[charater_AR == 0] = np.nan
            long, lat = np.meshgrid(np.arange(0, 360, resolution), IVT_lat)
            long = np.array(long, dtype='double')
            lat = np.array(lat, dtype='double')

            long_AR_index = np.argwhere(charater_AR != 0)[:, 1]
            long_AR_index = sorted(long_AR_index)
            b = sorted(set(range(long_AR_index[0], long_AR_index[-1] + 1)) - set(long_AR_index))

            # if AR across longitude 0, and long_AR_index is not ascend
            if (np.isin(long_AR_index, [0]).any()) & (len(b) != 0):
                long_min = b[0];
                long_max = b[-1]

                long_row = np.zeros(charater_AR.shape[1] - long_max + long_min + 1)
                long_row[:charater_AR.shape[1] - long_max] = long[0, long_max:].copy()
                long_row[charater_AR.shape[1] - long_max:charater_AR.shape[1] - long_max + long_min + 1] = long[0,
                                                                                                           :long_min + 1].copy()

                long[:, long_max:] = np.arange(charater_AR.shape[1] - long_max)
                long[:, :long_min + 1] = np.arange(charater_AR.shape[1] - long_max,
                                                   charater_AR.shape[1] - long_max + long_min + 1)
            else:
                long_min = np.min(long_AR_index);
                long_max = np.max(long_AR_index)
                long_row = long[0, long_min:long_max + 1].copy()
                long[:, long_min:long_max + 1] = np.arange(long_max - long_min + 1)

            long[charater_AR == 0] = np.nan
            lat[charater_AR == 0] = np.nan

            protion = ivt_total * (s_area / np.nansum(s_area))
            centroid_long_int = np.nansum(long * (protion / np.nansum(protion)))

            centroid_long = long_row[int(centroid_long_int)]

            centroid_index = [np.argwhere(IVT_lat == int(np.nansum(lat * protion) / np.nansum(protion))).flatten(),
                              np.argwhere(np.arange(0, 360, resolution) == int(centroid_long)).flatten()]
            centroid_direction = direction(u[centroid_index[0], centroid_index[1]],
                                           v[centroid_index[0], centroid_index[1]])
            return [np.nansum(lat * protion) / np.nansum(protion), centroid_long, centroid_direction]


        def character_mainaxis_orientation_width(simple):
            long = np.arange(0, 360, resolution)
            lat = IVT_lat
            # cross 0 index or not?
            roll = 0
            if (np.in1d(np.unique(np.argwhere(simple != 0)[:, 1]), [0])).any():
                simple = np.roll(simple, int(180 / resolution), axis=1)
                roll = 1
            ######## sketetons
            fil = FilFinder2D(simple, distance=1 * u.pix, mask=simple)
            fil.preprocess_image(skip_flatten=True)
            fil.create_mask(border_masking=True, verbose=False,
                            use_existing_mask=True)
            fil.medskel(verbose=False)
            sslabels = measure.label(simple, connectivity=1, background=0)
            regions = measure.regionprops(sslabels)
            longth = regions[0].axis_major_length
            width = regions[0].axis_minor_length

            fil.analyze_skeletons(branch_thresh=8 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')
            fil.exec_rht()
            orientation = np.degrees(fil.orientation)
            longpath = [fil.filaments[0].longpath_pixel_coords[0], fil.filaments[0].longpath_pixel_coords[1]]
            if roll:
                longpath = np.array([longpath[0], longpath[1] - (180 / resolution)])
                longpath[1][longpath[1] < 0] = longpath[1][longpath[1] < 0] + (360 / resolution)
            longpath[0] = np.array([lat[int(i)] for i in longpath[0]])
            longpath[1] = np.array([long[int(i)] for i in longpath[1]])
            return longpath, orientation[0].value, width * resolution, longth * resolution


        def save_standard_nc(data1, data2, times_1, times_2, shape,
                             centroid1, centroid2,
                             file_vari_name, lon=None, lat=None, path_save=""):
            shape_longpath, shape_orientation, shape_width, shape_longth = shape
            if lon is None:
                lon = np.arange(0, 360, resolution)
            if lat is None:
                lat = np.arange(90, -90.5, -resolution)

            def get_dataarray(data, times, time_name):
                foo = xr.DataArray(data, coords=[times, lat, lon], dims=[time_name, "lat", 'lon'])
                return foo

            def get_centroidarray(data, times, time_name):
                foo = xr.DataArray(data, coords=[times, ['y', 'x', 'a']], dims=[time_name, 'yxa'])
                return foo

            longpath_shape = np.arange(len(shape_longpath[0][0]))
            ds = xr.Dataset(
                {
                    'contour_all': get_dataarray(data1, times_1, 'time_all'),
                    'contour_AR': get_dataarray(data2, times_2, 'time_AR'),
                    'centroid_all': get_centroidarray(centroid1, times_1, 'time_all'),
                    'centroid_AR': get_centroidarray(centroid2, times_2, 'time_AR'),
                    'longpath': xr.DataArray(np.array(shape_longpath), coords=[times_2, ['y', 'x'], longpath_shape],
                                             dims=['time_AR', 'yx', 'lp']),
                    'orientation': xr.DataArray(shape_orientation, coords=[times_2], dims=['time_AR']),
                    'width': xr.DataArray(shape_width, coords=[times_2], dims=['time_AR']),
                    'longth': xr.DataArray(shape_longth, coords=[times_2], dims=['time_AR']),
                }
            )
            print(ds)
            ds.to_netcdf(path_save + '%s.nc' % file_vari_name)


        def Overlap(row_data, given_threshold, exist_contour):
            contourMask = []
            num = 0
            data = row_data - given_threshold
            data = gaussian_filter(data, sigma=1)
            s_IVT_long = IVT_long
            area_Protion = []
            exist_contour = np.array(exist_contour, dtype='uint8')
            p_area_exist_contour = len(np.argwhere(exist_contour != 0).flatten())
            thresh = cv2.threshold(data, 1, 255, cv2.THRESH_BINARY)[1]
            labels = measure.label(thresh, connectivity=1, background=0)
            ################### get pair-contour ###############################
            labels_unique = np.unique(labels)[np.unique(labels) != 0]
            labels_Mask_num = []
            for label in labels_unique:
                s_label_index = np.argwhere(labels == label)
                s_lon = s_label_index[:, 1]
                if np.isin(s_lon, [0]).any():
                    labels_Mask_num.append(2)
                elif np.isin(s_lon, [thresh.shape[1] - 1]).any():
                    labels_Mask_num.append(3)
                else:
                    labels_Mask_num.append(1)
            labels_Mask_num = np.array(labels_Mask_num)
            for label_begin in np.argwhere(labels_Mask_num == 2):
                s_lon_begin = np.argwhere(labels[:, 0] == labels_unique[label_begin]).flatten()
                # print('%%')
                # print(s_lon_begin)
                for label_end in np.argwhere(labels_Mask_num == 3):
                    s_lon_end = np.argwhere(labels[:, -1] == labels_unique[label_end]).flatten()
                    # print()
                    # print(s_lon_end)
                    if len(s_lon_end) == 0:
                        continue
                    protion1 = len(s_lon_begin[np.isin(s_lon_begin, s_lon_end)]) / len(s_lon_begin)
                    protion2 = len(s_lon_end[np.isin(s_lon_end, s_lon_begin)]) / len(s_lon_end)
                    protion = np.max([protion1, protion2])
                    # print(protion, 'protion')
                    if protion > 0.3:
                        labels[labels == labels_unique[label_end]] = labels_unique[label_begin]

            ################### loop in every contour ###############################
            Area_Protion = []
            for label in np.unique(labels)[np.unique(labels) != 0]:

                #######  prepare labelMask, labelMask is a narray contains one contour
                labelMask = np.zeros(thresh.shape, dtype="uint8")
                labelMask[labels == label] = 1.0
                numPixels = cv2.countNonZero(labelMask)

                # plt.imshow(labelMask)
                # plt.show()

                if (numPixels > bu_the_number_of_grids):
                    ar = cv2.bitwise_and(labelMask, exist_contour)
                    if cv2.countNonZero(ar) != 0:
                        # p_area_labelMask = len(np.argwhere(labelMask!=0).flatten())
                        p_area = len(np.argwhere(ar != 0).flatten())
                        s_Area_Protion = p_area / p_area_exist_contour
                        if s_Area_Protion > bu_Area_Protion_for_connective_contour:
                            num += 1
                            Area_Protion.append(s_Area_Protion)
                            contourMask.append(labelMask)
            if num > 0:
                s_selected = np.argmax(np.array(Area_Protion))
                # plt.imshow(np.array(contourMask[s_selected]))
                # plt.show()
                return np.array(contourMask[s_selected])
            else:
                return -999


        for sIloc_Season, season in enumerate(bu_time_Seaon_divide_Name):
            df = pd.read_csv(
                path_Proj_Data + 'AR_timeSeries_%s%i%i.csv' % (season, bu_main_period_length, bu_lack_period_length),
                index_col=0, parse_dates=[0])
            # df = pd.read_csv(path_ProjData + 'result_%s%i%i%s.csv' % (season, bu_main_period_length, bu_lack_period_length), index_col=0, parse_dates=[0])
            df['num_loc'] = np.ones(df.shape[0])
            df['num_loc'] = df['num_loc'].groupby(df.index).cumsum()
            time_be_all = []
            num_ARloc_all = []
            time_AR_all = []
            num_AR_name_all = []


            def cals_AR_num(df):
                num = len(np.argwhere(df['AR'].unique() != 0))
                for i in df['AR'].unique():
                    if i == 0:
                        continue
                    s_df = df[df['AR'] == i]
                    time_be_all.append([s_df.index[0], s_df.index[-1]])
                    num_ARloc_all.append(s_df['num_loc'].values)
                    time_AR_all.append(s_df.index.values)
                    num_AR_name_all.append(i)
                return


            def load_ivt_total(s_time):
                '''
                :param s_time: datetime64
                :return: ivt_total
                '''
                s_time = pd.to_datetime(s_time)

                s_time_str = s_time.strftime('%Y%m%d')
                index_vivt = xr.open_dataset(
                    path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
                    s_time_str[:4], s_time_str))
                index_uivt = xr.open_dataset(
                    path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
                    s_time_str[:4], s_time_str))
                # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
                # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

                ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
                ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

                ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
                return ivt_total


            def load_ivt_now(timeAR, num):
                '''
                load all AR_detection shape
                :param timeAR: list ,datetime64s, ARs exist time
                :param num: list, int, ARs num in accurate time
                :return: exist_contour_all
                '''
                exist_contour_all = []
                for sI_loc, s_time in enumerate(timeAR):
                    s_time = pd.to_datetime(s_time)
                    path_AR = path_Proj_Data + 'daily_AR/%s' % (s_time.strftime('%Y%m%d_%H'))
                    exist_contour = nc4.read_nc4(path_AR)[int(num[sI_loc] - 1)]
                    exist_contour_all.append(exist_contour)
                return exist_contour_all


            a = df.groupby(df['index'].eq(0).cumsum()).apply(lambda x: cals_AR_num(x))
            thresh = nc4.read_nc4(path_Proj_Data + de_quantile_field_Name)[bu_sI_Season[sIloc_Season]]
            area_S = scio.loadmat(path_Proj_Data + 'GridAL_ERA0_5degGridC.mat')['S']
            area = np.zeros([area_S.shape[0] + 1, area_S.shape[1]])
            area[:-1, :] = area_S
            area[-1, :] = area_S[-1, :]
            for sI_time_all, s_time_be_all in enumerate(time_be_all):
                warnings = 0
                next_AR_for_error = 0
                s_num_ARloc = num_ARloc_all[sI_time_all]
                s_time_AR_all = time_AR_all[sI_time_all]
                exist_contour_all = load_ivt_now(s_time_AR_all, s_num_ARloc)
                ######## fill
                s_time_loop = pd.date_range(s_time_be_all[0], s_time_be_all[1], freq='6h')
                s_timeAR_str = [pd.to_datetime(i).strftime('%Y%m%d%H') for i in s_time_AR_all]
                new_exist_contour_all = []
                dirs_path = path_Proj_Data + 'AR_contour_%i%i/' % (bu_main_period_length, bu_lack_period_length)

                if os.path.exists(dirs_path + '%s_%i.nc' % (s_timeAR_str[0], num_AR_name_all[sI_time_all])):
                    continue

                num_sI_time = 0
                for sI_time, s_time in enumerate(s_time_loop):
                    if np.isin(s_timeAR_str, pd.to_datetime(s_time).strftime('%Y%m%d%H')).any() == False:
                        s_exist_contour = new_exist_contour_all[sI_time - 1]
                        ivt_total = load_ivt_total(s_time)
                        contour_new = Overlap(ivt_total, thresh, s_exist_contour)
                        if isinstance(contour_new, np.ndarray):
                            new_exist_contour_all.append(contour_new)
                        else:
                            print('warning: no AR_detection find between all AR_detection events', s_time)
                            # plt.imshow(s_exist_contour)
                            # plt.show()
                            # plt.imshow(np.ma.masked_less(ivt_total-thresh, 0))
                            # plt.show()
                            warnings = 1
                            break
                    else:
                        new_exist_contour_all.append(exist_contour_all[num_sI_time])
                        num_sI_time += 1

                if warnings == 1:
                    continue

                ####### backward
                back_bu = 0
                s_time_loop = pd.date_range(s_time_be_all[0] - np.timedelta64(10, 'D'), s_time_be_all[0], freq='6h')[
                              :-1]
                if s_time_loop[0].strftime('%Y') == bu_Data_begin_year:
                    s_time_loop = pd.date_range('%i0101 05:00'%(int(bu_Data_begin_year)+1), s_time_be_all[0], freq='6h')[:-1]
                back_contour_all = []
                for sI_time, s_time in enumerate(s_time_loop[::-1]):
                    if sI_time == 0:
                        s_exist_contour = exist_contour_all[0]
                    ivt_total = load_ivt_total(s_time)
                    # plt.imshow(ivt_total-thresh)
                    # plt.show()
                    contour_new = Overlap(ivt_total, thresh, s_exist_contour)

                    if isinstance(contour_new, np.ndarray):
                        back_contour_all.append(contour_new)
                        s_exist_contour = contour_new
                    else:
                        back_bu = 1
                        back_time = s_time
                        break
                if back_bu != 1:
                    print('bu != 1',s_time)
                    back_time = s_time - np.timedelta64(6, 'h')
                print(s_time)
                ####### upward
                up_bu = 0
                s_time_loop = pd.date_range(s_time_be_all[1], s_time_be_all[1] + np.timedelta64(10, 'D'), freq='6h')[1:]
                up_contour_all = []
                for sI_time, s_time in enumerate(s_time_loop):
                    if sI_time == 0:
                        s_exist_contour = exist_contour_all[-1]
                    ivt_total = load_ivt_total(s_time)
                    contour_new = Overlap(ivt_total, thresh, s_exist_contour)
                    if isinstance(contour_new, np.ndarray):
                        up_contour_all.append(contour_new)
                        s_exist_contour = contour_new
                    else:
                        up_bu = 1
                        up_time = s_time
                        break

                if up_bu != 1:
                    up_time = s_time + np.timedelta64(6, 'h')

                final_contour = back_contour_all[::-1] + new_exist_contour_all + up_contour_all
                final_time = pd.date_range(back_time, up_time, freq='6H')[1:-1]
                AR_time = pd.date_range(s_time_be_all[0], s_time_be_all[1], freq='6h')

                ######## """get centroid""" ############
                centroid_all = []
                for i_centroid in range(len(final_time)):
                    centroid_all.append(centroid(final_contour[i_centroid], final_time[i_centroid]))

                centroid_AR = []
                for i_centroid in range(len(AR_time)):
                    centroid_AR.append(centroid(new_exist_contour_all[i_centroid], AR_time[i_centroid]))

                shape_longpath_all = pd.DataFrame()
                shape_orientation = []
                shape_width = []
                shape_longth = []
                for i_centroid in range(len(AR_time)):
                    try:
                        longpath, orientation, width, longth = character_mainaxis_orientation_width(
                            new_exist_contour_all[i_centroid])
                    except Exception as e:
                        logger.error(s_time)
                        logger.error(traceback.format_exc())
                        next_AR_for_error = 1
                        print(e)
                        break
                    shape_longpath = pd.DataFrame()
                    shape_longpath[str(i_centroid)] = longpath[0]
                    shape_longpath[str(i_centroid) + str(i_centroid)] = longpath[1]
                    shape_longpath_all = pd.concat([shape_longpath_all, shape_longpath], ignore_index=True, axis=1)
                    shape_orientation.append(orientation)
                    shape_width.append(width)
                    shape_longth.append(longth)

                if next_AR_for_error == 1:
                    print(dirs_path + '%s_%i.nc' % (s_timeAR_str[0], num_AR_name_all[sI_time_all]))
                    print('break')
                    continue

                shape_longpath_all = shape_longpath_all.T.values
                shape_longpath = []
                for i_lp in range(0, shape_longpath_all.shape[0], 2):
                    shape_longpath.append([shape_longpath_all[i_lp], shape_longpath_all[i_lp + 1]])

                dirs_path = path_Proj_Data + 'AR_contour_%i%i/' % (bu_main_period_length, bu_lack_period_length)
                os.makedirs(dirs_path, exist_ok=True)
                print(len(final_contour))
                print(len(new_exist_contour_all))
                print(len(final_time))
                print(len(AR_time))
                print(final_time, AR_time)
                save_standard_nc(final_contour, new_exist_contour_all,
                                 final_time, AR_time,
                                 [shape_longpath, shape_orientation, shape_width, shape_longth],
                                 np.array(centroid_all), np.array(centroid_AR),
                                 file_vari_name='%s_%i' % (s_timeAR_str[0], num_AR_name_all[sI_time_all]),
                                 path_save=dirs_path, )

