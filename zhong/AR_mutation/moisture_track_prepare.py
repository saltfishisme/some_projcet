import os
import numpy as np
import pandas as pd
import scipy.io as scio


# a = os.listdir(r'/home/linhaozhong/work/AR_NP85_1940/moisture_track/')
# b = os.listdir(r'/home/linhaozhong/work/AR_NP85_1940/AR_contour_42/')
# b = [i[:-3] for i in b]
# file_AR_all = list(set(b)-set(a))
# np.savetxt(r'/home/linhaozhong/work/AR_NP85_1940/contour_lack.txt', np.array(file_AR_all), fmt='%s')
# exit()
import xarray as xr
import matlab.engine

do_muti_process = '_2'
do_muti_process_matlab = {'_1':'MATLAB_15434', '_2':'MATLAB_15281'}
eng = matlab.engine.connect_matlab(do_muti_process_matlab[do_muti_process])

def create_Matlab_param_and_run(AR_contour, AR_time, AR_file):
    if os.path.exists(
            path_mat_AR+AR_file+'/%s_1-level-int_trajN.mat'%AR_time.strftime('%Y%m%d')) & os.path.exists(
            path_mat_AR+AR_file+'/%sres_6hr.mat'%AR_time.strftime('%Y%m%d')):
        return
    print('are you here?')
    ds = xr.Dataset()
    ds.coords["latitude"] = np.arange(90, -90.01, -0.5)
    ds.coords["longitude"] = np.arange(0, 360, 0.5)

    ds['tp'] = (("latitude", "longitude"), AR_contour)
    ds.to_netcdf(path_mat + 'AR.nc')

    AR_time_s = AR_time - np.timedelta64(16, 'D')
    if int(AR_time_s.strftime('%Y')) < 1940:
        return

    time_AR = {'year_s':float(AR_time_s.strftime('%Y')),'month_s':float(AR_time_s.strftime('%m')),'day_s':float(AR_time_s.strftime('%d')),
               'hour_s':0,
               'year_e':float(AR_time.strftime('%Y')),'month_e':float(AR_time.strftime('%m')),'day_e':float(AR_time.strftime('%d')),
               'hour_e':1
              }
    scio.savemat(path_mat+'AR_time.mat', time_AR)
    print('begin run')
    # run

    eng.cd(path_mat, nargout=0)
    print('cd')
    eng.Step01_TrackAreaSelection_shp(nargout=0)
    eng.Step02_Track_time_mode(nargout=0)
    print('step 03')
    eng.Step03_Tracmain_prepcyc(nargout=0)
    print('track over')
    eng.Step04_TimeInterpolation_EulerProj(nargout=0)


    dirs_1 = r'output_NP-5deg'
    dirs_2 = r'output_int6hr_NP-5deg'
    import shutil
    os.makedirs(path_mat_AR+AR_file, exist_ok=True)
    shutil.copy(path_mat+dirs_1+'/%s_1-level-int_trajN.mat'%AR_time.strftime('%Y%m%d'),
              path_mat_AR+AR_file)
    shutil.copy(path_mat+dirs_2+'/%sres_6hr.mat'%AR_time.strftime('%Y%m%d'),
              path_mat_AR+AR_file)

path_AR_contour = '/home/linhaozhong/work/AR_NP85_1940/AR_contour_42/'
path_mat_AR = r'/home/linhaozhong/work/AR_NP85_1940/moisture_track/'

file_AR_all = np.loadtxt(r'/home/linhaozhong/work/AR_NP85_1940/contour_lack.txt', dtype=str)

do_muti_process_dict = {'_1':file_AR_all[0:300], '_2':file_AR_all[300:]}

path_mat = r'/home/linhaozhong/work/AR_NP85_1940/NP_daily%s/'%do_muti_process

for i_AR_file in do_muti_process_dict[do_muti_process]:
    data_AR = xr.open_dataset(path_AR_contour+i_AR_file+'.nc')
    time_AR = data_AR.time_AR.values
    contour_AR = data_AR.contour_AR
    next_one_time = time_AR[0]
    next_one_contour = np.zeros(contour_AR.values[0].shape)

    # to daily resolution and create mat param for moisture track
    for i_time_AR in time_AR:
        if pd.to_datetime(i_time_AR).strftime('%Y%m%d') == pd.to_datetime(next_one_time).strftime('%Y%m%d'):
            next_one_contour += contour_AR.sel(time_AR=i_time_AR).values
        else:
            create_Matlab_param_and_run(next_one_contour, pd.to_datetime(next_one_time), i_AR_file)
            next_one_time = i_time_AR
            next_one_contour = contour_AR.sel(time_AR=i_time_AR).values

# eng.quit()