import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================

=============================
"""



def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='anomaly'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    if types == 'anomaly':
        return data_present - data_anomaly
    else:
        return [data_present, data_anomaly]


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


def main_3phase(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'AR_contour_42/%s' % (time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))
        print(AR_loc)

        if var == 't2m':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                        '2m_temperature',
                                                        't2m')
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())


        elif var == 'IVT':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = load_ivt_total(pd.to_datetime(i_time))
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'tcwv':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                        'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour')
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'sic':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i_present, s_i_climatly = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                         'Sea_Ice_Cover',
                                                         'siconc', types='now')
                s_i = s_i_present-s_i_climatly

                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                # s_i[s_i_present<0.0001]=np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        return avg

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            phase1 = []
            phase2 = []
            phase3 = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean = contour_cal(time_file, var)
                phase1.extend(s_mean[0])
                phase2.extend(s_mean[1])
                phase3.extend(s_mean[2])

            phase = [phase1, phase2, phase3]
            for iI_phase, i_phase in enumerate(phase):
                nc4.save_nc4(np.array(i_phase),
                             path_SaveData + 'PDF/all_%s' % (var + '%i_'%iI_phase + season + '_' + type_name[iI]))


# # main('area')
# # main('duration')
# main_3phase('IVT')
# main_3phase('t2m')
# main_3phase('tcwv')
#
# # main('lat')
# # main('lon')
# # main_3phase('str')
# main_3phase('sic')
# # # main_3phase('advection')
# # # main_3phase('adibatic')
# # # main_3phase('ssr')
# # # main_3phase('slhf')
# # # main_3phase('sshf')
# #
# # # main_3phase('t2m')
# exit(0)


path_SecondData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\PDF_for_ARbegin_3phase/'

path_MainData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE', 'BAF','BSE', 'BSW',
             'MED', 'ARC']

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

infig_fontsize = 4

resolution=0.5
var_all = ['IVT', 'tcwv']
X_new_for_var = [np.linspace(-10, 600, 1000), np.linspace(-5, 30, 1000)]
units = ['kg m${^{-1}}$ s${^{-1}}$','kg m${^{-2}}}$']
var_all_plot = ['IVT', 'TCWV']

# var_all = ['t2m', 'sic']
# X_new_for_var = [np.linspace(-6, 26, 1000), np.linspace(-0.08, 0.05, 1000)]
# units = ['℃','']
# var_all_plot = ['T2m', 'SIC']



SMALL_SIZE = 6
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize


for season in time_Seaon_divide_Name:

    def get_data():
        data_mean = []
        for s_type_name in type_name:
            s_data_mean = []

            for var in var_all:

                s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%i_%s_%s' % (var, phase, season, s_type_name)))

            data_mean.append(s_data_mean)

        return np.array(data_mean)


    def plots(ax, data, X_new, bin_width='scott'):

        def plot_pdf(X,X_new=None, bandwidth='scott'):
            from scipy import stats
            kde = stats.gaussian_kde(np.squeeze(X), bw_method=bandwidth)
            f = kde.covariance_factor()
            bw = f * X.std()
            print(bw)
            if X_new is None:
                xmin = np.nanmin(X)
                xmax = np.nanmax(X)
                X_new = np.linspace(xmin, xmax, 1000)

            log_dens = kde.evaluate(X_new)
            return X_new, log_dens
        data = np.array(data)
        x,y  = plot_pdf(data, X_new, bandwidth=bin_width)
        ax.plot(x, y, color=color, label=phase_name, linewidth=1)
        # index_max = np.argmax(y)
        # ax.plot([x[index_max], x[index_max]], [0, y[index_max]], color='black',
        #         linestyle='--', linewidth=1)

        index_max = np.argmin(abs(x-np.nanmean(data)))
        ax.plot([x[index_max], x[index_max]], [0, y[index_max]], color=color,
                linestyle='--', linewidth=1)
        return


    fig, axs = plt.subplots(6, 2, figsize=[5, 7])
    plt.subplots_adjust(top=0.95,
bottom=0.05,
left=0.1,
right=0.98,
hspace=0.4,
wspace=0.2)

    colors = ['blue', 'red', 'black', ]
    phase_names = ['t=0', r't=$\frac{1}{2}$T', 't=T']

    for phase in [0,1,2]:
        color = colors[phase]
        phase_name = phase_names[phase]
        frequency = get_data()

        for i in range(6):
            for j in range(2):
                data = frequency[i][j]
                ax = axs[i][j]
                var = var_all[j]
                ax.set_title(figlabelstr[i*2+j],loc='left')

                if var_all[0] == 't2m':
                    if j == 1:
                        plots(ax, data, X_new_for_var[j], 0.1)
                        ax.set_xlim([-0.06,0.04])
                        ax.set_ylim([0, 30])
                    else:
                        plots(ax, data, X_new_for_var[j], 0.8)
                        ax.set_xlim([-5,25])
                        ax.set_ylim([0, 0.09])

                else:
                    if j == 1:
                        plots(ax, data, X_new_for_var[j], 'scott')
                        ax.set_xlim([-2, 15])
                        # ax.set_ylim([0,0.04])
                    else:

                        plots(ax, data, X_new_for_var[j], 'scott')
                        if i == 0:
                            ax.set_xlim([0,300])
                            # ax.set_ylim([0, 0.09])
                        else:
                            ax.set_xlim([0,200])
                            # ax.set_ylim([0, 0.09])
axs[-1][1].legend(loc='upper center', bbox_to_anchor=(-0.1, -0.15), ncol=3)

for i_col in range(2):
    axs[0][i_col].set_title(var_all_plot[i_col],fontdict={'size':7})

for i_row in range(6):
    axs[i_row][0].set_ylabel(plot_name[i_row]+'\nPDF',fontdict={'size':6})


for i_col in range(2):
    axs[-1][i_col].set_xlabel(units[i_col])

plt.savefig('dissertation_fig3-7.png', dpi=400)
plt.show()
plt.close()







