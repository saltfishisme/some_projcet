# import cartopy.crs as ccrs
# import matplotlib.pyplot as plt
# import matplotlib.path as mpath
# import numpy as np
#
# # Value for r_extent is obtained by trial and error
# # get it with `ax.get_ylim()` after running this code
# r_extent = 4651194.319
# r_extent *= 1.005       #increase a bit for better result
#
# # Projection settings
# lonlat_proj = ccrs.PlateCarree()
# use_proj = ccrs.NorthPolarStereo(central_longitude=0)
# fig = plt.figure(figsize=[7, 7])
# ax = plt.subplot(1, 1, 1, projection=use_proj)
# ax.set_extent([-180, 180, 50, 90], lonlat_proj)
#
# #ax.stock_img() # add bluemarble image
# ax.coastlines(lw=0.5, color="black", zorder=20) # add coastlines
#
# # draw graticule (meridian and parallel lines)
# gl = ax.gridlines(draw_labels=True, crs=lonlat_proj, lw=1, color="gray",
#         y_inline=True, xlocs=range(-180,180,30), ylocs=range(0,90,10))
#
# # set the plot limits
# ax.set_xlim(-r_extent, r_extent)
# ax.set_ylim(-r_extent, r_extent)
#
# # Prep circular boundary
# circle_path = mpath.Path.unit_circle()
# circle_path = mpath.Path(circle_path.vertices.copy() * r_extent,
#                            circle_path.codes.copy())
#
# #set circular boundary
# #this method will not interfere with the gridlines' labels
# ax.set_boundary(circle_path)
# ax.set_frame_on(False)  #hide the boundary frame
#
# fig.canvas.draw()  # Enable the use of `gl._labels`
# print(help(gl))
# print(gl.y_inline_label_artists)
# print(gl.ylabel_artists)
# print(gl.label_artists)
# # Reposition the tick labels
# # Labels at 150d meridian will be moved to 180d
# for ea in gl.label_artists:
#     pos = ea.get_position()
#     if pos[0] == 150:
#         print("Position:", pos, ea.get_text() )
#         ea.set_position([180, pos[1]])
# plt.draw()
# plt.show()
#
# plt.show()



import numpy as np
import os
import scipy.io as scio
import pandas as pd

"""
when calculate the area-weighted mean, we will ## mask 0 ## to avoid error.
"""

ar_type = True
file_exist = True
path_Main = r'G:\OneDrive\basis\some_projects\zhong\AR_reclustering/'
VAR = 'freq'
PARAM_VAR = {'ivt':[True, [path_Main + 'ivt_ar_yearly', 'ar_type'], False, [np.arange(1979, 2020 + 1), 2000]],
             'freq':[False, [path_Main+'freq_ar_yearly', 'ar_type'], True, [np.arange(1980,2019+1), 2002]],
             'density':[True, [path_Main+'density_ar_yearly', 'ar_type'], True, [np.arange(1980,2019+1), 2000]]
             }

# for Trend, keep 0 value or not?
# file path and mat file var name
# cut 1979 and 2020 or not?

period_VAR = PARAM_VAR[VAR][3][0]
mutation_year_VAR = int(PARAM_VAR[VAR][3][1] - period_VAR[0])
arType_shortName = ['GRE', '1', 'BAF', 'BSE' , 'BSW', 'MED', 'ARC', '7']

import matplotlib.pyplot as plt

def plot_columns(ax, df_row, linregress_2000=[]):
    df = df_row.copy()

    df_mutation = df.iloc[mutation_year_VAR:].copy()

    lens = 0

    colors = ['#FF0000', '#00FF00', '#0000FF', '#FFA500', '#9400D3', '#FFC0CB', '#00FFFF', '#FFFF00']


    for i_columns in df.columns:
        # plot raw line
        ax.plot(df.index, df[i_columns], '-', color='black', linewidth=0.5)

        # fit 1979-2020

        from scipy.stats import linregress

        notnan_loc = np.argwhere(~np.isnan(np.array(df[i_columns])))
        x = np.array(df.index)[notnan_loc].flatten()
        y = np.array(df[i_columns])[notnan_loc].flatten()

        slope, intercept, r_value, p_value, std_err = linregress(x,y)

        f = x * slope + intercept

        ax.plot(x, f, '--', color='black', label='slope=%.2f/decade'%(slope*10), linewidth=1.2)


        # fit 1979-2020
        if i_columns in linregress_2000:
            from scipy.stats import linregress
            notnan_loc = np.argwhere(~np.isnan(np.array(df_mutation[i_columns])))
            x = np.array(df_mutation.index)[notnan_loc].flatten()
            y = np.array(df_mutation[i_columns])[notnan_loc].flatten()
            print(x,y)
            slope, intercept, r_value, p_value, std_err = linregress(x, y)
            print( p_value)
            f = x * slope + intercept

            ax.plot(x, f, '-', linewidth=1.2, color='black', label='slope=%.2f/decad$e^{*}$'%(slope*10))

        lens += 1

def cal_area(lon, lat):
    import math
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S

def area_weighted_mean(data, area_weight, maskvalue=''):

    if not isinstance(maskvalue, str):
        area_weight[data==maskvalue] = np.nan
    area_weight[np.isnan(data)] = np.nan

    area_weight = area_weight / np.nansum(area_weight)
    avg_AR = np.nansum(data * area_weight)

    return avg_AR


data_row = scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\Trend_new\freq_type_ar_yearly.mat")['ar_type']

if PARAM_VAR[VAR][2]:
    if ar_type:
        data_row = data_row[:, 1:-1, :]  # cut 1979, 2020
    else:
        data_row = data_row[1:-1, :,:]  # cut 1979, 2020

weight = cal_area(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5))


data = np.nansum(data_row, axis=0)

avg = []

for ii in range(data.shape[0]):
    avg.append(area_weighted_mean(data[ii], weight.copy(), maskvalue=0))

avg = np.array(avg)
if PARAM_VAR[VAR][0]:
    avg[avg == 0] = np.nan
df = pd.DataFrame(index=period_VAR)
df[VAR] = avg


import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs
import cmaps
import nc4

# data_circulation[0][data_circulation[0]<20] = 0
SMALL_SIZE = 8
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
# fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                         figsize=[4, 5])
fig = plt.figure(figsize=[7,2.8])

plt.subplots_adjust(top=0.83,
bottom=0.19,
left=0.085,
right=0.88,
hspace=0.14,
wspace=0.2)


cmpas_here = cmaps.cmocean_balance

trend_1979 = nc4.read_nc4(
    r'G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\arTypeContribute/trend_ar_pattern_1979')[:180, ]
p_1979 = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\AR_PatternTrend\arTypeContribute/p_ar_pattern_1979')[
    :180, ]

# trend[np.isnan(trend)] = 0
# trend[trend < 0.00001] = np.nan
import matplotlib.colors as colors

import matplotlib.gridspec as gridspec
gs = gridspec.GridSpec(1,3)

ax = fig.add_subplot(gs[0,:2])
plot_columns(ax, df, df.columns)
title = ax.set_title('(a)', x=0.03, y=0.93)
title.set_bbox(dict(facecolor='white',edgecolor='white', pad=0))
ax.set_xlabel('year')
ax.set_ylabel('times')
plt.legend()

ax = fig.add_subplot(gs[0,-1], projection=ccrs.Orthographic(0,90))


cb = ax.pcolormesh(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:180], trend_1979*10
                 , vmin=-0.3, vmax=0.3,
                 cmap=cmpas_here,
                 transform=ccrs.PlateCarree())
cb_ax = fig.add_axes([0.92, 0.2, 0.01, 0.6])
cbar = plt.colorbar(cb, cax=cb_ax)

cb_ax.set_ylabel('% decad$e^{-1}$')

ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5)[:180], np.ma.masked_greater(p_1979, 0.1),

            colors='none', levels=[0, 0.1],
            hatches=[3 * '+', 3 * '+'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
ax.coastlines(linewidth=0.3, zorder=10)
gl = ax.gridlines(ylocs=[66], xlocs=[0,60,120,180,-60,-120],
             linewidth=0.3, color='black',
             draw_labels={'bottom':'x', 'left':'x', 'right':'x', 'top':'x'},
             x_inline=False, y_inline=True)

gl.xpadding = 2

# exit()
gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}

fig.canvas.draw()

for ea in gl.ylabel_artists:
    ea.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='none', pad=0))
    pos = ea.get_position()

    if pos[1] == 66:
        print("Position:", pos, ea.get_text() )
        ea.set_position([165, 66])
title = ax.set_title('(b)',x=0.03, y=0.93)
title.set_bbox(dict(facecolor='white',edgecolor='white', pad=0))
# cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
#
# cb_ax.set_xlabel('trend')
# plt.show()
# save_pic_path = path_MainData + 'pic_combine_result/'
# os.makedirs(save_pic_path, exist_ok=True)

plt.savefig(r'figure1.png',
            dpi=400)
plt.close()
exit()

exit(0)
