# import cartopy.crs as ccrs
# import cartopy.feature as cfeature
# import matplotlib.pyplot as plt
# import numpy as np
# import pandas as pd
# import xarray as xr
#
#
#
# def call_colors_in_mat(Path, var_name):
#     '''
#     mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
#     which represents alpha of those colors.
#
#     :param Path: mat file Path
#     :param var_name: var in mat file
#     :return: colormap
#     '''
#     from matplotlib.colors import ListedColormap
#     import scipy.io as scio
#     import numpy as np
#
#     colors = scio.loadmat(Path)[var_name]
#     colors_shape = colors.shape
#     ones = np.ones([1, colors_shape[0]])
#     return ListedColormap(np.concatenate((colors, ones.T), axis=1))
# cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')
# var_with_shortname = {'Hgt':'z', 'Tem':'t', 'Uwnd':'u', 'Vwnd':'v', 'RH':'r'}
# central_lon = [320,300,180,180,0,320]
# import nc4
# import scipy.io as scio
#
# long = np.arange(0.5, 360, 1)
# lat = np.arange(89.5, -90, -1)
#
# import matplotlib
#
# SMALL_SIZE = 6
#
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
# plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
# plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
#
# fig = plt.figure(figsize=[5,12.5])
# plt.subplots_adjust(top=0.95,
# bottom=0.08,
# left=0.05,
# right=0.99,
# hspace=0.2,
# wspace=0.2)
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
#              'MS', 'GE']
# plot_name = ['G\nR\nE','B\nA\nF','B\nS\nE' , 'B\nS\nW', 'M\nE\nD', 'A\nR\nC']
# type_name_num = ['0','2','3','4','5','6']
#
# subplot_number = 1
# for i in [0,1,2,3,4,5]:
#
#     # centroid = a['centroid']
#     # long_c, lat_c = centroid[0][:2]
#     # print(centroid)
#     # long = np.arange(long_c-45, long_c+45,0.5)
#     # lat = np.arange(lat_c-40, lat_c+40, 0.5)
#
#     def plot_quiver():
#         plt.rcParams['hatch.color'] = 'darkred'
#         ax = fig.add_subplot(6,3, subplot_number,  projection=ccrs.Orthographic(central_lon[i], 45))
#
#         # i_contour_cut, z, z_ano= data
#         z = data1
#
#         import cmaps
#         import matplotlib.colors as colors
#         cb = ax.contourf(long, lat,
#                          np.array((z),dtype='double'),levels=np.arange(0,3.5,0.2),extend='max',cmap=cmpas_zhong,
#                         transform=ccrs.PlateCarree())
#         # plt.colorbar(cb, orientation="horizontal")
#         # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
#         #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
#         #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
#         import cmaps
#         # cb = ax.contourf(long, lat,
#         #                  uv_in_AR(u, v, i_contour_cut), extend='both', levels=np.arange(0, 15.1, 0.5),
#         #                  transform=ccrs.PlateCarree(), cmap=cmaps.cmocean_matter, zorder=3)
#         # plt.colorbar(cb)
#         # ax.quiver(long[::delta], lat[::delta_lat],
#         #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
#         #           transform=ccrs.PlateCarree(), scale=200, zorder=7)
#         ax.set_title(figlabelstr[subplot_number-1],fontsize=7, loc='left')
#
#         ax.coastlines()
#         ax.gridlines()
#         from function_shared_Main import add_longitude_latitude_labels
#         add_longitude_latitude_labels(fig, ax, 'auto', yloc=central_lon[i])
#         # plt.show()
#         # ax.set_extent()
#         return cb
#
#     phase = 0
#     a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
#     data1 =a['data']
#     plot_quiver()
#     subplot_number += 1
#     phase = 1
#     a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
#     data1 =a['data']
#     cb = plot_quiver()
#     subplot_number += 1
#
#     phase = 2
#     a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
#     data1 =a['data']
#     cb = plot_quiver()
#     subplot_number += 1
#     # plt.show()
#
# axs = plt.gcf().axes
# phase_names = ['t=0', r't=$\frac{1}{2}$T', 't=T']
# for i_col in range(3):
#     axs[i_col].set_title(phase_names[i_col],fontdict={'size':8}, pad=10)
#
# for iI, i_row in enumerate([0,3,6,9,12,15]):
#     # axs[i_row].set_ylabel(plot_name[iI])
#     axs[i_row].text(-0.12, 0.5, plot_name[iI], transform=axs[i_row].transAxes,fontdict={'size':8},
#             verticalalignment='center', bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1})
#
#
# cb_ax = fig.add_axes([0.25, 0.05, 0.5, 0.01])
# cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
# cb_ax.set_xlabel('mm/day')
# plt.savefig('dissertation_fig3-10.png',dpi=500)
# plt.show()


import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr



def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1, colors_shape[0]])
    return ListedColormap(np.concatenate((colors, ones.T), axis=1))
cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')
var_with_shortname = {'Hgt':'z', 'Tem':'t', 'Uwnd':'u', 'Vwnd':'v', 'RH':'r'}
central_lon = [320,300,180,180,0,320]
import nc4
import scipy.io as scio

long = np.arange(0.5, 360, 1)
lat = np.arange(89.5, -90, -1)

import matplotlib

SMALL_SIZE = 6

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

fig = plt.figure(figsize=[6.5,5])
plt.subplots_adjust(top=0.95,
bottom=0.08,
left=0.035,
right=0.99,
hspace=0.2,
wspace=0.2)
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE','BAF','BSE' , 'BSW', 'MED', 'ARC']
type_name_num = ['0','2','3','4','5','6']
phase_names = ['t=0', r't=$\frac{1}{2}$T', 't=T']
subplot_number = 1
for i in [0,1,2,3,4,5]:

    # centroid = a['centroid']
    # long_c, lat_c = centroid[0][:2]
    # print(centroid)
    # long = np.arange(long_c-45, long_c+45,0.5)
    # lat = np.arange(lat_c-40, lat_c+40, 0.5)

    def plot_quiver():
        plt.rcParams['hatch.color'] = 'darkred'
        ax = fig.add_subplot(3,4, subplot_number,  projection=ccrs.Orthographic(central_lon[i], 45))

        # i_contour_cut, z, z_ano= data
        z = data1

        import cmaps
        import matplotlib.colors as colors
        cb = ax.contourf(long, lat,
                         np.array((z),dtype='double'),levels=np.arange(0,3.5,0.2),extend='max',cmap=cmpas_zhong,
                        transform=ccrs.PlateCarree())
        # plt.colorbar(cb, orientation="horizontal")
        # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
        #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
        #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
        import cmaps
        # cb = ax.contourf(long, lat,
        #                  uv_in_AR(u, v, i_contour_cut), extend='both', levels=np.arange(0, 15.1, 0.5),
        #                  transform=ccrs.PlateCarree(), cmap=cmaps.cmocean_matter, zorder=3)
        # plt.colorbar(cb)
        # ax.quiver(long[::delta], lat[::delta_lat],
        #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
        #           transform=ccrs.PlateCarree(), scale=200, zorder=7)
        plt.title(figlabelstr[subplot_number-1]+plot_name[i]+' %s'%(phase_names[phase]), fontdict={'size':SMALL_SIZE+1})


        ax.coastlines()
        ax.gridlines()
        from function_shared_Main import add_longitude_latitude_labels
        add_longitude_latitude_labels(fig, ax, 'auto', yloc=central_lon[i])
        # plt.show()
        # ax.set_extent()
        return cb

    phase = 0
    a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
    data1 =a['data']
    plot_quiver()
    subplot_number += 1
    phase = 1
    a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
    data1 =a['data']
    cb = plot_quiver()
    subplot_number += 1


    # plt.show()

# axs = plt.gcf().axes
# phase_names = ['t=0', r't=$\frac{1}{2}$T', 't=T']
# for i_col in range(3):
#     axs[i_col].set_title(phase_names[i_col],fontdict={'size':8}, pad=10)

# for iI, i_row in enumerate([0,3,6,9,12,15]):
#     # axs[i_row].set_ylabel(plot_name[iI])
#     axs[i_row].text(-0.12, 0.5, plot_name[iI], transform=axs[i_row].transAxes,fontdict={'size':8},
#             verticalalignment='center', bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1})


cb_ax = fig.add_axes([0.25, 0.05, 0.5, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
cb_ax.set_xlabel('mm/day')
plt.savefig('dissertation_fig3-10.png',dpi=500)
plt.show()
