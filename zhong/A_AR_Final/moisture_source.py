import os

import numpy as np
import cartopy.crs as ccrs
import scipy.io as scio
import nc4
import matplotlib.pyplot as plt
import cmaps
import cartopy.feature as cfeature
import sys
import cmaps
from cartopy.util import add_cyclic_point


def plot_centroid_eul(save_labels, longpath, savename=''):
    def flatten_trajectorytoEuler(data, lon, lat):
        """
        :param data: [event,2(lon,lat),trajectory],
        :param lon:
        :param lat:
        :return:
        """
        print(data.shape)
        data_eul = np.zeros([len(lat), len(lon)])
        for i_event in range(data.shape[0]):
            for i_date in range(data.shape[2]):
                lon_now = data[i_event, 1, i_date]
                lat_now = data[i_event, 0, i_date]
                print(lon_now, lat_now)
                index = [0, 0]
                index[1] = np.argmin(abs(lon - lon_now))
                index[0] = np.argmin(abs(lat - lat_now))
                data_eul[index[0], index[1]] += 1
        return data_eul

    size = int(np.ceil(len(np.unique(save_labels)) / 3))
    fig = plt.figure()

    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            ax = fig.add_subplot(size, 3, iI + 1, projection=ccrs.NorthPolarStereo())
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
            lon = np.arange(-180, 180, 0.5)
            lat = np.arange(90, -90.1, -0.5)
            centroid_eul = flatten_trajectorytoEuler(longpath[save_labels == i], lon, lat)

            ax.pcolormesh(lon, lat, centroid_eul, transform=ccrs.PlateCarree())
            ax.set_title('Type %i %i, %0.0f%%' % (
            i, longpath[save_labels == i].shape[0], 100 * longpath[save_labels == i].shape[0] / longpath.shape[0]))
            ax.coastlines()
    plt.tight_layout()
    plt.show()
    # plt.savefig(path_pic+'%s_%s.png'%(season, savename), dpi=500)
    plt.close()
    # # # plot classification result
    # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    # for i in np.unique(save_labels[save_labels == 0]):
    #     ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    # ax.coastlines()
    # plt.title(season)
    # plt.show()
    # plt.close()


def plot_trajectory_with_rr(ax, pos_int, rr_int):
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1, colors_shape[0]])
        return ListedColormap(np.concatenate((colors, ones.T), axis=1))

    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    path_color_zhong = 'D:/MATLAB/colormapdata/rainbow200.mat'
    cmpas_zhong = call_colors_in_mat(path_color_zhong, 'rainbow200')

    def plot_colored(ax, x, y, data, cmap, steps=1):
        '''
        :param ax: axes
        :param x: (N,) np.array
        :param y: (N,) np.array
        :param data: (N,) np.array
        :param cmap:
        :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
        :return:
        '''
        ''' from matplotlib import cm
        cmpa should be    cm.get_cmap(cmpas_zhong, 100)

         $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
         before plot all data, please normalized all this data.'''
        data = np.asarray(data)
        it = 0
        while it < (data.size - steps):
            x_segm = x[it:it + steps + 1]
            y_segm = y[it:it + steps + 1]

            c_segm = cmap(data[it + steps // 2])
            ax.plot(x_segm, y_segm, c=c_segm, transform=ccrs.Geodetic(), linewidth=0.2)
            it += steps

    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len + 1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :] = np.arange(0, 101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb

    ####### plot colorbar in given cmap  ##########
    cb_ax = fig.add_axes([0.1, 0.02, 0.8, 0.01])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    cb_ax.set_axes_locator(ip)
    cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
    cbar.set_label('%')
    cbar.set_ticks(np.arange(0, 101, 25))
    ####### plot trajectories by colored lines   ##########
    from matplotlib import cm
    viridis = cm.get_cmap(cmpas_zhong, 500)
    for i in range(pos_int.shape[0])[::lineplot]:
        # for i in range(200):
        plot_colored(ax, pos_int[i, 0, :], pos_int[i, 1, :], rr_int[i, :], cmap=cmpas_zhong)
        ####### plot end of trajectories with black dot  ##########
        ax.scatter(pos_int[i, 0, 0], pos_int[i, 1, 0], c='k', s=1, marker='.', linewidths=0.5,
                   transform=ccrs.PlateCarree())

    ################################# add some feature #################################

    ax.coastlines(linewidth=0.5)

    ################################# plot ERA5 TOPO ###################################

    # cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    # topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    # lat25 = topo['lat25']; lon25=topo['lon25']; topo25=topo['topo25']
    # ax.pcolormesh(lon25, lat25, topo25, shading='nearest',vmin=-6000, vmax=6000,cmap=cmap_topo, transform=ccrs.PlateCarree(), zorder=0)


# print("???????////////")
# # load data from all AR moisture mat files
# path_MoisturematFile = r'/home/linhaozhong/work/AR_NP85_1940/moisture_track/'
# pos_int_all = []
# rr_int_all = []
# for i_file in os.listdir(path_MoisturematFile):
#     i_file = os.path.join(path_MoisturematFile, i_file)
#     matfile = os.listdir(i_file)
#     print(matfile)
#     for i_matfile in matfile:
#         if 'res_6hr' in i_matfile:
#             data = mat73.loadmat(i_file+'/'+i_matfile)
#
#             pos_int_all.extend(np.transpose(data['pos_int'], [2,0,1]))
#             rr_int_all.extend(np.transpose(data['rr_int'], [1,0]))
# nc4.save_nc4(np.array(pos_int_all), r'/home/linhaozhong/work/AR_NP85_1940/pos_int')
# nc4.save_nc4(np.array(rr_int_all), r'/home/linhaozhong/work/AR_NP85_1940/rr_int')
# exit()

lineplot = 1000
fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
plt.subplots_adjust(top=0.98, bottom=0.13,left=0.125,right=0.9,hspace=0.2,wspace=0.2)
pos_int_all = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\moisture_track/pos_int')
rr_int_all = nc4.read_nc4(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\moisture_track/rr_int')
print(pos_int_all.shape)
plot_trajectory_with_rr(ax, pos_int_all, rr_int_all)


def add_longitude_latitude_labels(ax, xloc):
    gl = ax.gridlines(ylocs=[66], xlocs=[0, 60, 120, 179.9999, -60, -120],
                      linewidth=0.3, color='black', crs=ccrs.PlateCarree(),
                      draw_labels=True,
                      x_inline=True, y_inline=True)

    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}

    fig.canvas.draw()
    for ea in gl.ylabel_artists:
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        ea.set_position([165, 66])

    for ea in gl.xlabel_artists:
        ea.set_visible(True)
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        pos = ea.get_position()
        ea.set_position([pos[0], xloc])


add_longitude_latitude_labels(ax, 20)
ax.set_extent([0, 359, 10, 90], crs=ccrs.PlateCarree())

plt.savefig(r'G:\OneDrive\basis\some_projects\zhong\A_AR_Final\moisture_source.png', dpi=400)
plt.show()
