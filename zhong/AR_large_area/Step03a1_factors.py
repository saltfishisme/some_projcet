import xarray as xr
import numpy as np
import pandas as pd
import nc4
import scipy.io as scio

def reduce_expver(data, axis='dim'):
    result = np.nansum(data,axis=axis)
    result[np.all(np.isnan(data), axis=axis)] = np.nan
    return result
def get_climately(path_data, variName_inNC, path_save='', time_divide_type='seasonal', Save_vari_name='vari'):
    '''
    calculates the variables' monthly/seasonal climately during 1979-2020,
    :return: saved into nc file
    '''
    da_row = xr.open_dataset(path_data).sel(time=slice('1979-01', '2020-12'))

    try:
        da_row = da_row.reduce(reduce_expver, 'expver')
    except:
        a = 0

    if time_divide_type == 'seasonal':
        time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
    elif time_divide_type == 'monthly':
        time_Seaon_divide = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    result_allVari = []
    for iI_vari, i_vari in enumerate(variName_inNC):
        da_s_row = da_row[i_vari]
        result = []
        for sI_Season, s_Season in enumerate(time_Seaon_divide):
            da = da_s_row.sel(time=np.in1d(da_s_row['time.month'], s_Season))
            da = da.mean(dim='time').values
            result.append(da)

        result_allVari.append(np.array(result))
    nc4.save_standard_nc(result_allVari, variName_inNC, np.arange(1, len(time_Seaon_divide) + 1), path_save=path_save,
                         lon=da_row.longitude.values, lat=da_row.latitude.values, file_name=Save_vari_name)


variName_inNC = ['siconc']
variLongName_all = ['Sea_Ice_Cover']
# get_climately("/home/linhaozhong/download.nc",
#               variName_inNC,
#               '/home/linhaozhong/work/AR_detection/',
#               time_divide_type='monthly',
#               Save_vari_name='Climately')

import nc4


def load_ivt_total(s_time):
    """
    :param s_time: datetime64
    :return: ivt_total
    """
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


def load_ivt_now(timeAR, num):
    """
    load all AR_detection shape
    :param timeAR: list ,datetime64s, ARs exist time
    :param num: list, int, ARs num in accurate time
    :return: exist_contour_all
    """
    exist_contour_all = []
    for sI_loc, s_time in enumerate(timeAR):
        s_time = pd.to_datetime(s_time)
        path_AR = path_SaveData + 'daily_AR/%s' % (s_time.strftime('%Y%m%d_%H'))
        exist_contour = nc4.read_nc4(path_AR)[int(num[sI_loc] - 1)]
        exist_contour_all.append(exist_contour)
    return exist_contour_all


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/1X1/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'

# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\\'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'


bu_main_period_length = 4
bu_lack_period_length = 2


def main_index(variLongName, variShortName):
    for impact_area in range(1, 7):
        tem_index_mean_all = []
        tem_index_max_all = []
        tem_index_mean_70 = []
        tem_index_max_70 = []
        tem_index_mean_70_diff = []

        time_all = []
        bu_length_lack = '%i%i' % (bu_main_period_length, bu_lack_period_length)
        # times_file_all = np.array(os.listdir(path_SaveData+'AR_contour_%s/'%bu_length_lack))

        data = pd.read_csv(path_SaveData + 'impact_area.csv', index_col=0)
        times_file_all = data[data['impact_area'] == impact_area]['fileName'].values

        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData + 'AR_contour_%s/%s' % (bu_length_lack, s_time_all))

            s_time_loop = info_AR.time.values

            s_tem_index_mean_all = []
            s_tem_index_max_all = []
            s_tem_index_mean_70 = []
            s_tem_index_max_70 = []
            s_tem_index_mean_70_diff = []

            for s_time in s_time_loop:
                # print('s_time', s_time)
                s_time = pd.to_datetime(s_time)
                # sI_Season = np.argwhere(np.isin(time_Seaon_divide, s_time.month))[0][0]
                # print(dual_threshold.sel(time=s_time.month)[variShortName])

                charater_AR = info_AR.sel(time=s_time)['vari'].values[::2, ::2]

                s_time = pd.to_datetime(s_time)

                tem = xr.open_dataset(path_MainData + '%s/%i/%s.%s.nc' % (
                    variLongName, s_time.year, variLongName, s_time.strftime('%Y%m%d')))
                print(tem)
                tem_before = xr.open_dataset(path_MainData + '%s/%i/%s.%s.nc' % (
                    variLongName, (s_time - np.timedelta64(3, 'D')).year, variLongName,
                    (s_time - np.timedelta64(3, 'D')).strftime('%Y%m%d')))

                tem_index_used_grid = tem.sel(time=s_time)[variShortName].values
                tem_index_used_grid_before = tem_before.sel(time=s_time - np.timedelta64(3, 'D'))[variShortName].values

                tem_diff = tem_index_used_grid - tem_index_used_grid_before


                tem_index_used_grid = np.ma.masked_where(charater_AR == 0, tem_index_used_grid)
                tem_diff = np.ma.masked_where(charater_AR == 0, tem_diff)

                tem_index_used_grid[tem['latitude'].values < 70, :] = np.nan
                tem_diff[tem['latitude'].values < 70, :] = np.nan


                s_tem_index_mean_70_diff.append(np.nanmean(tem_diff))


            tem_index_mean_70_diff.append('%'.join(str(i) for i in s_tem_index_mean_70_diff))

            time_all.append(s_time_all[:-3])

        df = pd.DataFrame()

        df['%s_mean_70_diff' % variShortName] = tem_index_mean_70_diff
        df['time'] = time_all

        df.to_csv(path_SaveData + 'basis_analysis/%s_index_%s%s.csv' % (variShortName, bu_length_lack, impact_area))


for iI, i_var in enumerate(variName_inNC):
    main_index(variLongName_all[iI], i_var)
