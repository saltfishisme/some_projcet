import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys

''' get hourly anomaly of circulation factor '''


##########################
def save_standard_nc(data, file_vari_name, times, lon=None, lat=None, path_save="", Save_name='Vari'):
    '''
    :param data: list, containing [varis, time, lat, lon]
    :param file_vari_name: list, [varis]
    :param times:
    :param lon:
    :param lat:
    :param path_save:
    :param Save_name:
    :return:
    '''
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times

    for iI_vari, ivari in enumerate(file_vari_name):
        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + '%s.nc' % Save_name)


def nc_anomaly():
    da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc').sel(lat=60)
    da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
    result = da.groupby("year_month") - da.groupby("year_month").mean("time")
    return result


def anomaly_for_separate_file(var, var_ShortName):
    dates = pd.date_range('2000-01-01', '2000-12-31', freq='1D').strftime('%m%d')
    years = np.arange(1979, 2020 + 1)
    # containing 2/29

    anomaly = np.empty([len(level_isobaric), 366 * 24, len(lat), len(lon)])
    for iI_MonDay, i_MonDay in enumerate(dates):
        data_in_MonDay = []
        for i_Year in years:
            try:
                print(path_MainData + '%s/%i/%s.%i%s.nc' % (var, i_Year, var, i_Year, i_MonDay))
                if level_isobaric == -999:
                    s_data = xr.open_dataset(path_MainData + '%s/%i/%s.%i%s.nc' % (var, i_Year, var, i_Year, i_MonDay))[
                        var_ShortName].values
                else:
                    s_data = \
                    xr.open_dataset(path_MainData + '%s/%i/%s.%i%s.nc' % (var, i_Year, var, i_Year, i_MonDay)).sel(
                        level=level_isobaric)[var_ShortName].values
                    s_data = np.transpose(s_data, [1, 0, 2, 3])
                print(s_data.shape)
                data_in_MonDay.append(s_data)
            except:
                print(i_Year, i_MonDay)
        data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
        path_SaveData = path_MainData + '%s/anomaly/' % var
        os.makedirs(path_SaveData, exist_ok=True)
        scipy.io.savemat(path_SaveData + '%s.mat' % i_MonDay,
                         {'%s' % i_MonDay: np.array(data_in_MonDay, dtype='double')})
        print(data_in_MonDay.shape)
        anomaly[:, iI_MonDay * 24:(iI_MonDay + 1) * 24, :, :] = data_in_MonDay

    for iI_level, i_level in enumerate(level_isobaric):
        save_standard_nc(anomaly[iI_level], ['%s_%i' % (var, i_level)],
                         pd.date_range('1000-01-01 00:00', '1000-12-31 23:00', freq='1H'),
                         lon=lon, lat=lat, path_save=path_SaveData, Save_name='%s_%i_dailyMean' % (var, i_level))
    # scipy.io.savemat(path_SaveData + 'anomaly_%s.mat'%var, {var:np.array(anomaly, dtype='double')})


# path_MainData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# level_isobaric = [1000, 850, 500, 200]
# vari_SaveName_all=['Tem', 'Uwnd', 'Vwnd', 'Wwnd']
# var_ShortName = ['t', 'u', 'v', 'w']
# lon = np.arange(0,360,0.5)
# lat = np.arange(90,-90.2,-0.5)
#
#
# for iI_var in range(len(var_ShortName)):
#     anomaly_for_separate_file(vari_SaveName_all[iI_var], var_ShortName[iI_var])

''' get circulation data'''

path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_MainData = r'/home/linhaozhong/work/AR_detection/'
path_SaveData = '/home/linhaozhong/work/AR_detection/'


def get_circulation_data_based_on_date(date, var, var_ShortName, level=[1000, 850, 500, 200]):
    '''date containing Hour information'''
    time = pd.to_datetime(date)

    # load hourly anomaly data
    data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

    return data_present - data_anomaly


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']

data_SOM = pd.read_csv(path_MainData + 'impact_area_SOM.csv', index_col=0)
bu_length_lack = '42'
long = np.arange(0, 360, 0.5)

vari_SaveName_all = ['Tem', 'Uwnd', 'Vwnd', 'Wwnd']

vari_ShortName = ['t', 'u', 'v', 'w']
for iI_varName in range(len(vari_ShortName)):
    for Seaon in time_Seaon_divide_Name:
        for impact_area in range(1, 7):
            for type_SOM in range(0, 5):
                save_name = '%s_ia%s_som%s_%s' % (vari_SaveName_all[iI_varName], impact_area, type_SOM, Seaon)

                if os.path.exists(path_SaveData + 'circulation_data/%s.mat' % save_name):
                    continue

                times_file_all = data_SOM[(data_SOM['impact_area'] == impact_area) &
                                          (data_SOM['SOM_type'] == type_SOM) &
                                          (data_SOM['season'] == Seaon)]['fileName'].values
                anomaly = []
                for sI_time, s_time_all in enumerate(times_file_all):
                    info_AR = xr.open_dataset(path_MainData + 'AR_contour_%s/%s' % (bu_length_lack, s_time_all))

                    s_time_loop = info_AR.time.values

                    for s_time in s_time_loop:
                        s_time = pd.to_datetime(s_time)
                        charater_AR = info_AR.sel(time=s_time, lat=70)['vari'].values

                        long_past = long[np.argwhere(charater_AR != 0).flatten()]
                        if len(long_past) != 0:
                            anomaly.append(get_circulation_data_based_on_date(s_time, vari_SaveName_all[iI_varName],
                                                                              vari_ShortName[iI_varName]))
                            break

                anomaly = np.nanmean(anomaly, axis=0)
                scipy.io.savemat(path_SaveData + 'circulation_data/%s.mat' % save_name,
                                 {'vari': np.array(anomaly, dtype='double')})
