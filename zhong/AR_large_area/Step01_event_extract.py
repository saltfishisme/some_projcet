import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import numpy as np
import nc4
import os
import cv2

def yearly_duration_across_70(times_file_all):
    df = pd.DataFrame()
    df['year'] = [i[:4] for i in times_file_all]
    df['month'] = [i[4:6] for i in times_file_all]
    df['day'] = [i[6:8] for i in times_file_all]
    df = df.astype(int)
    month_to_season_dct = {
        1: 'DJF', 2: 'DJF',
        3: 'MAM', 4: 'MAM', 5: 'MAM',
        6: 'JJA', 7: 'JJA', 8: 'JJA',
        9: 'SON', 10: 'SON', 11: 'SON',
        12: 'DJF'
    }
    df['season'] = [month_to_season_dct.get(i) for i in df['month']]

    p_area_exist_contour = len(np.argwhere(exist_contour!=0).flatten()) # exist_contour: North polar
    impact_area_Protion = []
    # loop of all AR events
    for sI_time, s_time_all in enumerate(times_file_all):
        info_AR = xr.open_dataset(config_AR.path_SaveData+'AR_contour_%s%s/%s'%(config_AR.bu_length_lack, config_AR.de_projName, s_time_all))

        s_Area_Protion_one_AR = []
        # loop of an AR event
        for s_contour in info_AR['vari'].values:

            ar = cv2.bitwise_and(np.array(s_contour, dtype='uint8'), np.array(exist_contour, dtype='uint8'))
            if cv2.countNonZero(ar)!=0:
                p_area = len(np.argwhere(ar!=0).flatten())
                s_Area_Protion = p_area/p_area_exist_contour

                s_Area_Protion_one_AR.append(s_Area_Protion*10)
        s_Area_Protion_one_AR = np.array(s_Area_Protion_one_AR)

        impact_area_Protion.append('%i'%(int(np.ceil(np.max(s_Area_Protion_one_AR)))))
    df['impact_area'] = impact_area_Protion
    df['fileName'] = times_file_all
    return df

bu_Area_Protion_for_connective_contour = 0.5
exist_contour = np.zeros([361, 720])
exist_contour[:40, :] = 1

import sys
# sys.path.append(r'D:\OneDrive\basis\some_projects\zhong\\')
import config_AR
times_file_all = np.array(os.listdir(config_AR.path_SaveData+'AR_contour_%s%s/'%(config_AR.bu_length_lack, config_AR.de_projName)))

df = yearly_duration_across_70(times_file_all)
df.to_csv(config_AR.path_SaveData + 'impact_area.csv')