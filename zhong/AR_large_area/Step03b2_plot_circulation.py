import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs


class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps

        self.ax = ax
        self.ranges = ranges

        def del_ll(range, lat, lon):
            """
            cut lon and lat, and then return the index to cut data used later.
            :param range:
            :param lat:
            :param lon:
            :return: self.lat, self.lon, self.del_area
            """

            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range[0])

            if del_area[0] > del_area[1]:  # sort
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        if self.ranges[0] is not False:
            self.lat, self.lon, self.del_area = del_ll(ranges, lat, lon)
        else:
            self.lat, self.lon, self.del_area = [lat, lon, False]

        self.crs = ccrs.PlateCarree()
        self.MidPointNorm = False
        self.print_min_max = False

        self.colorbar_fmt = '%.1f'
        self.colorbar = True
        self.colorbar_orientation = 'h'
        self.ranges_reconstrcution_button_contourf = False
        self.contourf_arg = {}

        self.contour_fmt = '%.1f'
        self.ranges_reconstrcution_button_contour = False
        self.contour_clabel_color = 'white'
        self.contour_arg = {'colors': 'black'}
        self.colorbar_clabel_size = 5
        self.contour_clabel= True


        self.quiver_spacewind = [1, 1]
        self.quiverkey_arg = {'X': 0.85, 'Y': 1.05, 'U': 10, 'label': r'$10 \frac{m}{s}$'}
        self.quiver_arg = {}


    def del_data(self, data):
        """
        link with del_ll, use self.del_area from function del_ll
        :param data:
        :return: cut data
        """
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1] + 1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        """
        :param data:
        :param ranges_reconstrcution_button:
        :return: self.levels, self.MidPointNorm
        """
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize

        def ranges_create(begin, end, inter):
            class MidPointNorm(Normalize):
                def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                    Normalize.__init__(self, vmin, vmax, clip)
                    self.midpoint = midpoint

                def __call__(self, value, clip=None):
                    if clip is None:
                        clip = self.clip

                    result, is_scalar = self.process_value(value)

                    self.autoscale_None(result)
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if not (vmin < midpoint < vmax):
                        raise ValueError("midpoint must be between maxvalue and minvalue.")
                    elif vmin == vmax:
                        result.fill(0)  # Or should it be all masked? Or 0.5?
                    elif vmin > vmax:
                        raise ValueError("maxvalue must be bigger than minvalue")
                    else:
                        vmin = float(vmin)
                        vmax = float(vmax)
                        if clip:
                            mask = ma.getmask(result)
                            result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                              mask=mask)

                        # ma division is very slow; we can take a shortcut
                        resdat = result.data

                        # First scale to -1 to 1 range, than to from 0 to 1.
                        resdat -= midpoint
                        resdat[resdat > 0] /= abs(vmax - midpoint)
                        resdat[resdat < 0] /= abs(vmin - midpoint)

                        resdat /= 2.
                        resdat += 0.5
                        result = ma.array(resdat, mask=result.mask, copy=False)

                    if is_scalar:
                        result = result[0]
                    return result

                def inverse(self, value):
                    if not self.scaled():
                        raise ValueError("Not invertible until scaled")
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if cbook.iterable(value):
                        val = ma.asarray(value)
                        val = 2 * (val - 0.5)
                        val[val > 0] *= abs(vmax - midpoint)
                        val[val < 0] *= abs(vmin - midpoint)
                        val += midpoint
                        return val
                    else:
                        val = 2 * (value - 0.5)
                        if val < 0:
                            return val * abs(vmin - midpoint) + midpoint
                        else:
                            return val * abs(vmax - midpoint) + midpoint

            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0 + inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))

        # if range is given
        if isinstance(ranges_reconstrcution_button, list) or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button


        else:
            ranges_min = np.nanmin(data)
            ranges_max = np.nanmax(data)

            if ranges_reconstrcution_button is False:  # default if False
                if ranges_min > 0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                else:
                    ticks = (ranges_max - ranges_min) / 30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

        if self.MidPointNorm is not False:
            # only when min < 0

            print(self.levels)
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       norm=self.MidPointNorm, **self.contourf_arg)

        else:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       **self.contourf_arg)

        if self.colorbar:
            if self.colorbar_orientation == 'h':
                orientation = 'horizontal'
            else:
                orientation = 'vertical'

            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.colorbar_fmt)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)

        if self.contour_clabel:
            cbs = self.ax.clabel(
                cb, fontsize=self.colorbar_clabel_size,  # Typically best results when labelling line contours.
                colors=['black'],
                inline=True,  # Cut the line where the label will be placed.
                fmt=self.contour_fmt,  # Labes as integers, with some extra space.
            )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.quiver_spacewind[0]], self.lat[::self.quiver_spacewind[1]],
                             u[::self.quiver_spacewind[1], ::self.quiver_spacewind[0]], v[::self.quiver_spacewind[1], ::self.quiver_spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)
def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1,colors_shape[0]])
    return  ListedColormap(np.concatenate((colors, ones.T), axis=1))

path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\\'
path_SaveData = '/home/linhaozhong/work/AR_detection/'

time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
divided_area = [
    [15, 150],[90, 210], [150, 300],
    [0, 90, 300, 360], [210, 360]
]

var_SaveName_all = ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']

var_ShortName = ['z', 't', 'u', 'v', 'w']



for Season in time_Seaon_divide_Name:
    for type_SOM in range(0, 5):

        def get_data(var, type_SOM, Season):
            # combine impact_area index of 1,2
            num = 0
            data_circulation_12 = []
            for impact_area in range(1, 2 + 1):
                save_name = '%s_ia%s_som%s_%s' % (var,
                                                  impact_area, type_SOM, Season)
                s_data_circulation = scipy.io.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area'
                                                      r'\circulation_data\%s.mat' % save_name)['vari']
                if s_data_circulation.shape[-1] < 10:
                    continue

                num += 1
                data_circulation_12.append(s_data_circulation)

            if num > 1:
                data_circulation_12 = np.nanmean(data_circulation_12, axis=0)
            elif num == 1:
                data_circulation_12 = data_circulation_12[0]

            # combine impact_area index of 3-6
            num = 0
            data_circulation_36 = []
            for impact_area in range(3, 6 + 1):
                save_name = '%s_ia%s_som%s_%s' % (var,
                                                  impact_area, type_SOM, Season)
                s_data_circulation = scipy.io.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area'
                                                      r'\circulation_data\%s.mat' % save_name)['vari']
                print(s_data_circulation.shape)
                if s_data_circulation.shape[-1] < 10:
                    continue

                num += 1
                data_circulation_36.append(s_data_circulation)

            if num > 1:
                data_circulation_36 = np.nanmean(data_circulation_36, axis=0)
            elif num == 1:
                data_circulation_36 = data_circulation_36[0]
            return data_circulation_12, data_circulation_36


        def cal_quiver_var_plot(ax, data, quiver):
            from cartopy.util import add_cyclic_point

            data, lon = add_cyclic_point(data, coord=np.arange(0, 360, 0.5))
            u, lon = add_cyclic_point(quiver[0], coord=np.arange(0, 360, 0.5))
            v, lon = add_cyclic_point(quiver[1], coord=np.arange(0, 360, 0.5))
            base_ax = circulation(ax[0], lon, np.arange(90, -90.001, -0.5),
                                  [[0, 360, 30, 90], np.arange(0, 360, 90), [50, 60, 70, 80]])
            base_ax.colorbar_orientation = 'h'
            base_ax.contourf_arg = {'cmap':call_colors_in_mat(r"D:\MATLAB\colormapdata\Tano.mat", 'Tano'),
                                    }
            base_ax.colorbar_fmt='%i'
            data = data/98
            base_ax.p_contourf(data)
            print(quiver[0].shape, quiver[1].shape)
            base_ax.quiver_spacewind = [7,7]
            base_ax.p_quiver(u, v)
            base_ax.ax.coastlines(linewidth=0.8)
            ######## change!!!!!
            divided_area = [
                [15, 150],[90, 210], [150, 300],
                [300, 90], [210, 359]
            ]
            s_divided_area = divided_area[type_SOM]
            x = s_divided_area
            y = np.ones(len(x))*70
            ax[0].plot(x, y, color='red', transform=ccrs.PlateCarree())

            ###################################################
            from mpl_toolkits.axes_grid1 import make_axes_locatable
            # create new axes on the right and on the top of the current axes

            base_ax = circulation(ax[1], lon, np.arange(90, -90.001, -0.5),
                                  [[0, 360, 70, 90], np.arange(0, 360, 90), [70, 80]])
            base_ax.colorbar_orientation = 'h'
            base_ax.contourf_arg = {'cmap':call_colors_in_mat(r"D:\MATLAB\colormapdata\Tano.mat", 'Tano'),
                                    }
            base_ax.colorbar_fmt='%i'
            data = data/98
            base_ax.p_contourf(data)
            print(quiver[0].shape, quiver[1].shape)
            base_ax.quiver_spacewind = [7,7]
            base_ax.p_quiver(u, v)
            base_ax.ax.coastlines(linewidth=0.8)
            ######## change!!!!!
            divided_area = [
                [15, 150],[90, 210], [150, 300],
                [300, 90], [210, 359]
            ]
            s_divided_area = divided_area[type_SOM]
            x = s_divided_area
            y = np.ones(len(x))*70
            ax[1].plot(x, y, color='red', transform=ccrs.PlateCarree())

        data_circulation_12, data_circulation_36 = get_data('Hgt', type_SOM, Season)
        u_12, u_36 = get_data('Uwnd', type_SOM, Season)
        v_12, v_36 = get_data('Vwnd', type_SOM, Season)
        levels = [1000, 850, 500, 200]
        for level in range(4):
            fig, axs = plt.subplots(nrows=2, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
                                    figsize=[4,5])
            plt.subplots_adjust(top=0.915,
                                bottom=0.055,
                                left=0.01,
                                right=0.98,
                                hspace=0.09,
                                wspace=0.11)

            cal_quiver_var_plot([axs[0][0], axs[1][0]], data_circulation_12[level], [u_12[level],v_12[level]])
            if len(data_circulation_36) != 0:
                cal_quiver_var_plot([axs[0][1], axs[1][1]], data_circulation_36[level], [u_36[level],v_36[level]])
                # base_ax.ax.gridlines(alpha=)
            fig.suptitle('%s SOM:%i (%s in %ihPa)' % (Season, type_SOM+1, 'combine', levels[level]))
            # plt.show()
            save_pic_path = path_MainData+'pic/%s/%i/'%( 'combine', levels[level])
            os.makedirs(save_pic_path, exist_ok=True)
            plt.savefig(save_pic_path + '%s SOM %i (%s in %ihPa).png' % (Season, type_SOM+1, var_SaveName_all[iI_varName], levels[level]), dpi=300)
            plt.close()

# for iI_varName in range(len(var_ShortName)):
#     for Season in time_Seaon_divide_Name:
#         for type_SOM in range(0, 5):
#
#             # combine impact_area index of 1,2
#             num = 0
#             data_circulation_12 = []
#             for impact_area in range(1, 2 + 1):
#                 save_name = '%s_ia%s_som%s_%s' % (var_SaveName_all[iI_varName],
#                                                   impact_area, type_SOM, Season)
#                 s_data_circulation = scipy.io.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area'
#                                                       r'\circulation_data\%s.mat' % save_name)['vari']
#                 if s_data_circulation.shape[-1] < 10:
#                     continue
#
#                 num += 1
#                 data_circulation_12.append(s_data_circulation)
#
#             if num > 1:
#                 data_circulation_12 = np.nanmean(data_circulation_12, axis=0)
#             elif num == 1:
#                 data_circulation_12 = data_circulation_12[0]
#
#             # combine impact_area index of 3-6
#             num = 0
#             data_circulation_36 = []
#             for impact_area in range(3, 6 + 1):
#                 save_name = '%s_ia%s_som%s_%s' % (var_SaveName_all[iI_varName],
#                                                   impact_area, type_SOM, Season)
#                 s_data_circulation = scipy.io.loadmat(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area'
#                                                       r'\circulation_data\%s.mat' % save_name)['vari']
#                 print(s_data_circulation.shape)
#                 if s_data_circulation.shape[-1] < 10:
#                     continue
#
#                 num += 1
#                 data_circulation_36.append(s_data_circulation)
#
#             if num > 1:
#                 data_circulation_36 = np.nanmean(data_circulation_36, axis=0)
#             elif num == 1:
#                 data_circulation_36 = data_circulation_36[0]
#
#
#
#             def cal_plot(ax, data):
#                 from cartopy.util import add_cyclic_point
#
#                 data, lon = add_cyclic_point(data, coord=np.arange(0, 360, 0.5))
#                 base_ax = circulation(ax, lon, np.arange(90, -90.001, -0.5),
#                                       [[0, 360, 30, 90], np.arange(0, 360, 90), [50, 60, 70, 80]])
#                 base_ax.colorbar_orientation = 'h'
#                 base_ax.contourf_arg = {'cmap':call_colors_in_mat(r"D:\MATLAB\colormapdata\Tano.mat", 'Tano'),
#                                         }
#                 base_ax.colorbar_fmt='%i'
#                 if var_SaveName_all[iI_varName] == 'Hgt':
#                     data = data/98
#                 base_ax.p_contourf(data)
#                 base_ax.contour_clabel = False
#                 base_ax.contour_arg = {'linewidths':0.3, 'colors':'black'}
#                 base_ax.p_contour(data)
#                 base_ax.ax.coastlines(linewidth=0.8)
#                 ######## change!!!!!
#                 divided_area = [
#                     [15, 150],[90, 210], [150, 300],
#                     [300, 90], [210, 359]
#                 ]
#                 s_divided_area = divided_area[type_SOM]
#                 x = s_divided_area
#                 y = np.ones(len(x))*70
#                 ax.plot(x, y, color='red', transform=ccrs.PlateCarree())
#                 # ax.fill(x, y, color='red', transform=ccrs.PlateCarree(), alpha=0.4)
#
#             levels = [1000, 850, 500, 200]
#             for level in range(4):
#                 fig, axs = plt.subplots(nrows=1, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
#                                         figsize=[4,3])
#                 plt.subplots_adjust(top=1.0,
#                                     bottom=0.055,
#                                     left=0.075,
#                                     right=0.975,
#                                     hspace=0.2,
#                                     wspace=0.2)
#
#                 cal_plot(axs[0], data_circulation_12[level])
#                 if len(data_circulation_36) != 0:
#                     cal_plot(axs[1], data_circulation_36[level])
#                     # base_ax.ax.gridlines(alpha=)
#                 fig.suptitle('%s SOM:%i (%s in %ihPa)' % (Season, type_SOM+1, var_SaveName_all[iI_varName], levels[level]))
#                 # plt.show()
#                 save_pic_path = path_MainData+'pic/%s/%i/'%(var_SaveName_all[iI_varName], levels[level])
#                 os.makedirs(save_pic_path, exist_ok=True)
#                 plt.savefig(save_pic_path + '%s SOM %i (%s in %ihPa).png' % (Season, type_SOM+1, var_SaveName_all[iI_varName], levels[level]), dpi=300)
#                 plt.close()


