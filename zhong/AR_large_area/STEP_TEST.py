import pandas as pd
import scipy.io
import xarray as xr
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\\')
import config_AR

def to_float(df):
    I = []
    for sI in df:
        sI = sI.split('%')
        sI = [float(i) for i in sI]
        I.append(np.array(sI))

    return np.array(I)


variName_inNC = ['IVT']
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
data_SOM = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\impact_area_SOM.csv', index_col=0)
data_SOM = data_SOM.set_index(data_SOM['fileName'].str[:-3])

for Season in time_Seaon_divide_Name:
    for s_variName_inNC in variName_inNC:
        df_plot_SOM  = pd.DataFrame()
        for used_variName_indf
        for type_SOM in range(0, 5):
            s_data_SOM = data_SOM[(data_SOM['SOM_type'] == type_SOM) & (data_SOM['season'] == Season)]

            columns_all = ['%s_mean' % s_variName_inNC, '%s_max' % s_variName_inNC, '%s_mean_70' % s_variName_inNC, '%s_max_70' % s_variName_inNC, '%s_mean_70_diff' % s_variName_inNC]
            if s_variName_inNC == 'IVT':
                columns_all = ['%s_mean' % s_variName_inNC, '%s_max' % s_variName_inNC, '%s_mean_70' % s_variName_inNC, '%s_max_70' % s_variName_inNC]
            if s_variName_inNC == 'siconc':
                columns_all = [ '%s_mean_70_diff' % s_variName_inNC]


            for impact_area in range(1, 2+1):
                df  = pd.read_csv(config_AR.path_MainData +'basis_analysis/%s_index_%s%s.csv' % (s_variName_inNC, config_AR.bu_length_lack, impact_area),
                                  index_col=0)
                for i_columns in columns_all:
                    df[i_columns] = to_float(df[i_columns].values)
                    # df[i_columns] = df[i_columns].apply(lambda x: np.nanmean(np.array(x)))
                df = df.set_index(df['time'])
                df = df.drop(['time'], axis=1)
                if impact_area == 2:
                    df_12 = pd.concat([df_12, df], axis=0)
                else:
                    df_12 = df

            df_12 = df_12.loc[df_12.index.intersection(s_data_SOM.index)]


            def plot(ax, data_all):
                data_all.plot.kde(ax=ax)

                ticks = []
                for i_mean in range(len(data_all)):
                    x = ax.get_children()[i_mean]._x
                    y = ax.get_children()[i_mean]._y
                    mean_val = x[np.argmax(y)]
                    ticks.append(mean_val)
                    ax.plot([mean_val, mean_val], [0, np.max(y)], color='black', linestyle='dashed', linewidth=0.5)
                ax.set_xlim([-0.25,0.25])
                ax.set_xticks(ticks, minor=True)
                from matplotlib.ticker import MultipleLocator, FormatStrFormatter
                # yminorLocator   = MultipleLocator(2) #将y轴次刻度标签设置为1的倍数
                # ax.xaxis.set_major_locator(yminorLocator)

            fig, axs = plt.subplots(nrows=1, ncols=1)
            plt.subplots_adjust(top=0.945,
                                bottom=0.05,
                                left=0.13,
                                right=0.9,
                                hspace=0.3,
                                wspace=0.3)
            # reshape df_12
            df_12_values = df_12.values
            data_all = []
            for iI_data in range(df_12_values.shape[1]):
                s_data = []
                for iI in df_12_values[:, iI_data]:
                    s_data.extend(iI)
                data_all.append(s_data)

            data_all = pd.DataFrame(data_all).T
            data_all.columns = df_12.columns

            xlim = [np.nanmin(data_all.values), np.nanmax(data_all.values)]



            plot(axs, data_all)



            axs.set_title('(a) slightly affected')

            plt.show()
            plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\pic\pdf\five_SOM\fig4_%s.png'%s_variName_inNC, dpi=300)
            plt.close()
            # exit(0)





