import numpy as np
import pandas as pd
import xarray as xr
import nc4


divided_area = [
    [15, 150],[90, 210], [150, 300],
    [0, 90, 300, 360], [210, 360]
 ]
divided_area_index = [
    [0],[0], [0],
    [0,2], [0]
]
resolution = 0.5

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
    return ivt_total

def load_ivt_now(timeAR, num):
    '''
    load all AR_detection shape
    :param timeAR: list ,datetime64s, ARs exist time
    :param num: list, int, ARs num in accurate time
    :return: exist_contour_all
    '''
    exist_contour_all = []
    for sI_loc, s_time in enumerate(timeAR):
        s_time = pd.to_datetime(s_time)
        path_AR = path_SaveData+'daily_AR/%s'%(s_time.strftime('%Y%m%d_%H'))
        exist_contour = nc4.read_nc4(path_AR)[int(num[sI_loc] - 1)]
        exist_contour_all.append(exist_contour)
    return exist_contour_all


time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5x0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
long = np.arange(0, 360, resolution)
# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\\'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'


for bu_main_period_length in [4]:
    for bu_lack_period_length in [2]:

        time_all = []
        bu_length_lack ='%i%i'%(bu_main_period_length, bu_lack_period_length)
        # times_file_all = np.array(os.listdir(path_SaveData+'AR_contour_%s/'%bu_length_lack))

        data = pd.read_csv(path_SaveData+'impact_area.csv', index_col=0)
        times_file_all = data['fileName'].values
        final_type = []
        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s/%s'%(bu_length_lack, s_time_all))

            s_time_loop = info_AR.time.values

            type_SOM_All = []
            for s_time in s_time_loop:
                s_time = pd.to_datetime(s_time)
                charater_AR = info_AR.sel(time=s_time, lat=70)['vari'].values

                long_past = long[np.argwhere(charater_AR != 0).flatten()]
                if len(long_past) == 0:
                    continue
                classification_protion_All = []
                for iI, i_divided_area in enumerate(divided_area):
                    classification = pd.cut(long_past, i_divided_area, labels=np.arange(len(i_divided_area) - 1))
                    classification = classification[~np.isnan(np.array(classification))]
                    classification_len = np.bincount(classification, minlength=len(i_divided_area) - 1)
                    classification_protion = np.sum(classification_len[divided_area_index[iI]])/np.sum((np.diff(i_divided_area)[divided_area_index[iI]])/resolution)
                    classification_protion_All.append(classification_protion)

                type_SOM_All.append(np.argmax(classification_protion_All))

            final_type.append(np.argmax(np.bincount(type_SOM_All, minlength=len(divided_area)-1)))
        data['SOM_type'] = final_type
        print(data)
        data.to_csv(path_SaveData+'impact_area_SOM.csv')













