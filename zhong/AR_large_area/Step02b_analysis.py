import pandas as pd
import scipy.io
import xarray as xr
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\\')
import config_AR

def to_float(df, columns):
    I = []
    for sI in df:
        sI = sI.split('%')
        sI = np.array([float(i) for i in sI])
        sI = sI[~np.isnan(sI)]

        if 'mean_70' in columns:
            print(columns)
            I.append(list((sI)))
        else:
            I.append(np.array(sI))

    return I


# variName_inNC = ['siconc', 'IVT', 'slhf', 'sshf', 't2m', 'alnid', 'alnip', 'ssr', 'str', 'sp', 'ttr']
variName_inNC = [ 'IVT']
data_SOM = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\impact_area_SOM.csv', index_col=0)
data_SOM = data_SOM.set_index(data_SOM['fileName'].str[:-3])
SOM_type = 1
season_type = 'MAM'
s_data_SOM = data_SOM[(data_SOM['SOM_type'] ==SOM_type) & (data_SOM['season'] == season_type)]

for s_variName_inNC in variName_inNC:
    columns_all = ['%s_mean' % s_variName_inNC, '%s_max' % s_variName_inNC, '%s_mean_70' % s_variName_inNC, '%s_max_70' % s_variName_inNC, '%s_mean_70_diff' % s_variName_inNC]
    if s_variName_inNC == 'IVT':
        columns_all = ['%s_mean_70' % s_variName_inNC, '%s_max_70' % s_variName_inNC]
    if s_variName_inNC == 'siconc':
        columns_all = [ '%s_mean_70_diff' % s_variName_inNC]

    for impact_area in range(1, 2+1):
        df  = pd.read_csv(config_AR.path_MainData +'basis_analysis/%s_index_%s%s.csv' % (s_variName_inNC, config_AR.bu_length_lack, impact_area),
                          index_col=0)

        for i_columns in columns_all:

            df[i_columns] = to_float(df[i_columns].values, i_columns)
            # df[i_columns] = df[i_columns].apply(lambda x: np.nanmean(np.array(x)))
        df = df.set_index(df['time'])
        df = df.drop(['time'], axis=1)
        df = df[columns_all]
        if impact_area == 2:
            df_12 = pd.concat([df_12, df], axis=0)
        else:
            df_12 = df

    df_12 = df_12.loc[df_12.index.intersection(s_data_SOM.index)]


    def plot(ax, data_all, style):
        data_all.plot.kde(ax=ax, style=style)


        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        # yminorLocator   = MultipleLocator(2) #将y轴次刻度标签设置为1的倍数
        # ax.xaxis.set_major_locator(yminorLocator)
    SMALL_SIZE=6

    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
    plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
    plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
    plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
    # plt.rc('xtick.major', size=0.5, width=0.2)
    # plt.rc('xtick.minor', size=0.1, width=0.1)
    # plt.rc('ytick.major', size=0.5, width=0.2)
    # plt.rc('ytick.minor', size=0.1, width=0.1)
    # plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
    plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize
    fig, axs = plt.subplots(nrows=1, ncols=1, figsize=[3,2])
    import matplotlib


    plt.subplots_adjust(top=0.945,
                        bottom=0.15,
                        left=0.15,
                        right=0.96,
                        hspace=0.3,
                        wspace=0.3)
    # reshape df_12
    df_12_values = df_12.values
    data_all = []
    for iI_data in range(df_12_values.shape[1]):
        s_data = []
        for iI in df_12_values[:, iI_data]:
            s_data.extend(iI)
        data_all.append(s_data)

    data_all = pd.DataFrame(data_all).T
    data_all.columns = df_12.columns

    xlim = [np.nanmin(data_all.values), np.nanmax(data_all.values)]
    data_all.columns = ['slightly affected (average)', 'slightly affected (maximum)']
    plot(axs, data_all, ['k--', 'r--'])

    for impact_area in range(3, 6+1):
        df  = pd.read_csv(config_AR.path_MainData +'basis_analysis/%s_index_%s%s.csv' % (s_variName_inNC, config_AR.bu_length_lack, impact_area),
                          index_col=0)
        for i_columns in columns_all:
            df[i_columns] = to_float(df[i_columns].values, i_columns)
            # df[i_columns] = df[i_columns].apply(lambda x: np.nanmean(np.array(x)))
        df = df.set_index(df['time'])
        df = df.drop(['time'], axis=1)
        df = df[columns_all]
        if impact_area > 3:
            df_12 = pd.concat([df_12, df], axis=0)
        else:
            df_12 = df
    df_12 = df_12.loc[df_12.index.intersection(s_data_SOM.index)]

    # reshape df_12
    df_12_values = df_12.values
    data_all = []
    for iI_data in range(df_12_values.shape[1]):
        s_data = []
        for iI in df_12_values[:, iI_data]:
            s_data.extend(iI)
        data_all.append(s_data)

    data_all = pd.DataFrame(data_all).T
    data_all.columns = df_12.columns
    data_all.columns = ['heavily affected (average)', 'heavily affected (maximum)']
    plot(axs, data_all, ['k-', 'r-'])

    # axs[0].set_title('(a) slightly affected SOM%i %s'%(SOM_type+1, season_type))
    ticks = []
    colors = ['k', 'r', 'k', 'r']
    for i_mean in range(4):
        x = axs.get_children()[i_mean]._x
        y = axs.get_children()[i_mean]._y
        mean_val = x[np.argmax(y)]
        ticks.append(mean_val)
        axs.plot([mean_val, mean_val], [0, np.max(y)], color=colors[i_mean], linestyle='dashed', linewidth=0.5)
    axs.set_xlim([0,500])
    axs.set_xticks(ticks, minor=True)
    axs.set_xlabel(r'IVT (kg*$m^{-1}$*$s^{-1}$)')
    axs.set_title('(c) IVT across 70°N in %s' %season_type)
    # plt.show()
    plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\pic\pdf\fig4_%s_SOM%i_%s.png'%(s_variName_inNC, SOM_type, season_type), dpi=300)

    # plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\pic\pdf\five_SOM\fig4_%s_SOM%i_%s.png'%(s_variName_inNC, SOM_type, season_type), dpi=300)
    plt.close()
    exit(0)





