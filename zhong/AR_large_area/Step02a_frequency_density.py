import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys

import nc4


# path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_large_area\\'
# path_MainData = r'/home/linhaozhong/work/AR_detection/'
# path_SaveData = '/home/linhaozhong/work/AR_detection/'

time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']

data_SOM = pd.read_csv(path_MainData + 'impact_area_SOM.csv', index_col=0)
bu_length_lack = '42'
long = np.arange(0, 360, 0.5)

df_frequency_density = pd.DataFrame(index=time_Seaon_divide_Name, columns=np.arange(5))
for Seaon in time_Seaon_divide_Name:
    for type_SOM in range(0, 5):
        s_frequency = np.zeros(2)
        for impact_area in range(1, 7):

            times_file_all = data_SOM[(data_SOM['impact_area'] == impact_area) &
                                      (data_SOM['SOM_type'] == type_SOM) &
                                      (data_SOM['season'] == Seaon)]['fileName'].values
            if impact_area <= 2:
                s_frequency[0] += len(times_file_all)
            else:
                s_frequency[1] += len(times_file_all)
        df_frequency_density[type_SOM].loc[Seaon] = s_frequency
import numpy as np

labels = ['SOM1', 'SOM2', 'SOM3', 'SOM4', 'SOM5']

frequency = df_frequency_density.values

x = np.arange(len(labels))  # the label locations
x_width = [-0.3, -0.1, 0.1, 0.3]
width = 0.2  # the width of the bars

fig, ax = plt.subplots()
plt.subplots_adjust(top=0.97,
                    bottom=0.135,
                    left=0.08,
                    right=0.985,
                    hspace=0.2,
                    wspace=0.2)
pos = []
lab = []
for j_som in range(5):
    for i in range(4):
        s_frequency = frequency[i, j_som]
        print(s_frequency)
        rects1 = ax.bar(x[j_som] +x_width[i], s_frequency[0], width, label='slightly affected',
                        facecolor='#2c7fb8', edgecolor='black', linewidth=0.7)
        rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1], width, label='heavily affected',
                        bottom=s_frequency[0],
                        facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7)
        ax.bar_label(rects1, padding=-14)

        ax.bar_label(rects2, labels=[int(s_frequency[1])], padding=0, color='#1a9850')
        pos.append(x[j_som] + x_width[i])
        lab.append(time_Seaon_divide_Name[i])


ax.set_xticks(pos,minor=True)
ax.set_xticklabels(lab,minor=True,rotation = 90)

ax.tick_params(axis='x', which='major', pad=15, size=0)
plt.setp(ax.get_xticklabels(), rotation=0)

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:2], labels[:2])

ax.set_xticklabels(['', 'SOM1', 'SOM2', 'SOM3', 'SOM4', 'SOM5'])
ax.tick_params(axis='x', which='major', pad=35)

ax.set_ylabel('AR occurrence number (units: times)')
# ax.set_title('')
plt.show()
plt.savefig('figure2.png', dpi=300)
exit(0)
#####################################################################


time_Seaon_divide_Color = ['#98141b','#00385f', '#739D00','#006A55', 'black']
ax_tick_label = []
ax_tick_color = []
for j in range(5):
    for z in range(4):
        ax_tick_label.append(time_Seaon_divide_Name[z])
        ax_tick_color.append(time_Seaon_divide_Color[j])


data = nc4.read_nc4('step02a_density')

data = np.transpose(data, [2,1,0,3])

data = data[0].reshape(20, 720)
fig, axs = plt.subplots(20, 1, sharex=True)
# Remove horizontal space between axes
fig.subplots_adjust(top=0.94,
                    bottom=0.08,
                    left=0.08,
                    right=0.84,
                    hspace=0.0,
                    wspace=0.2)
handles = []
labels = []
for i_ax, ax in enumerate(axs):
    ax.bar(np.arange(720), data[i_ax], color=ax_tick_color[i_ax], label='SOM%i' % (int(i_ax/4)+1))

    ax.set_ylim([0,np.max(data)])
    ax_c = ax.twinx()
    ax_c.set_ylim([0,np.max(data)])
    ax_c.set_yticks([50,150,250])
    # ax_c.set_yticks([25,50])
    plt.setp(ax_c.get_yticklabels(), fontsize=5)
    ax.set_yticks([np.max(data)/2])
    ax.set_yticklabels([ax_tick_label[i_ax]])
    ax.set_xlim([0,720])
    if i_ax % 4 == 0:
        s_handles, s_labels = ax.get_legend_handles_labels()
        handles += s_handles
        labels += s_labels
axs[9].legend(handles, labels,loc='center left', bbox_to_anchor=(1.04, 0.5), fontsize=8,
          fancybox=True)
ax.set_xticks(np.arange(60,720,60))
ax.set_xticklabels(np.arange(30,360,30))
ax.set_xlabel('Longitude', labelpad=0.5)
fig.suptitle('(a) AR occurrence (slightly affected)')
# fig.suptitle('(b) AR occurrence (heavily affected)')
# plt.show()
plt.savefig('fig3a.png', dpi=300)
exit(0)




density = np.zeros([4, 5, 2, len(long)])
for iI_season, Seaon in enumerate(time_Seaon_divide_Name):
    for type_SOM in range(5):
        for impact_area in range(1, 7):

            times_file_all = data_SOM[(data_SOM['impact_area'] == impact_area) &
                                      (data_SOM['SOM_type'] == type_SOM) &
                                      (data_SOM['season'] == Seaon)]['fileName'].values
            print(times_file_all)
            for sI_time, s_time_all in enumerate(times_file_all):
                info_AR = xr.open_dataset(path_MainData + 'AR_contour_%s/%s' % (bu_length_lack, s_time_all))

                s_time_loop = info_AR.time.values
                print('??????')
                for s_time in s_time_loop:
                    s_time = pd.to_datetime(s_time)
                    charater_AR = info_AR.sel(time=s_time, lat=70)['vari'].values
                    if np.max(charater_AR) == 1:
                        if impact_area <= 2:
                            density[iI_season, type_SOM, 0, :] += charater_AR
                        else:
                            density[iI_season, type_SOM, 1, :] += charater_AR
                        # break
                    print(np.max(density))
nc4.save_nc4(density, path_SaveData+ 'step02a_density')

exit(0)


