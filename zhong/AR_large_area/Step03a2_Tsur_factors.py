"""
=============================
1, 1000 hPa Temperature Advection,
2, adiabatic cooling and warming
reference:
Gong T, Feldstein S, Lee S. The Role of Downward Infrared Radiation in the Recent Arctic Winter Warming Trend. Journal of Climate. 2017;30(13):4937-4949. doi:10.1175/JCLI-D-16-0180.1
Lee, S., Gong, T., Johnson, N., Feldstein, S. B., & Pollard, D. (2011). On the Possible Link between Tropical Convection and the Northern Hemisphere Arctic Surface Air Temperature Change between 1958 and 2001, Journal of Climate, 24(16), 4350-4367. Retrieved Feb 27, 2022, from https://journals.ametsoc.org/view/journals/clim/24/16/2011jcli4003.1.x
=============================
"""

###############################
# Imports
from datetime import datetime

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import metpy.calc as mpcalc
import pandas as pd
from metpy.units import units
import numpy as np
import scipy.ndimage as ndimage
import xarray as xr
import nc4
import os


def create_save_adv_adi(time):
    import metpy.calc as mpcalc
    from metpy.units import units
    import numpy as np
    import xarray as xr
    import os

    def get_data(time, var, level_isobaric):
        '''
        :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
        :param level_isobaric:
        :return:
        '''

        def get_svar_data(var):
            data = xr.open_dataset(
                path_MainData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
            return data

        result = []
        if np.isin(var, 'Hgt').any():
            hght_var = get_svar_data('Hgt').sel(time=time, level=level_isobaric)['z']
            result.append(hght_var)
        if np.isin(var, 'Tem').any():
            temp_var = get_svar_data('Tem').sel(time=time, level=level_isobaric)['t']
            result.append(temp_var)
        if np.isin(var, 'Uwnd').any():
            u_wind_var = get_svar_data('Uwnd').sel(time=time, level=level_isobaric)['u']
            result.append(u_wind_var)
        if np.isin(var, 'Vwnd').any():
            v_wind_var = get_svar_data('Vwnd').sel(time=time, level=level_isobaric)['v']
            result.append(v_wind_var)
        if np.isin(var, 'Wwnd').any():
            w_wind_var = get_svar_data('Wwnd').sel(time=time, level=level_isobaric)['w']
            result.append(w_wind_var)

        lat_var = result[0]['latitude']
        lon_var = result[0]['longitude']
        result.append(lat_var)
        result.append(lon_var)
        return result

    def tem_advection_1000(time):
        temp_var, u_wind_var, v_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Uwnd', 'Vwnd'],
                                                                      level_isobaric=1000)

        # between lat/lon grid points
        dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)

        # Calculate temperature advection using metpy function
        adv = mpcalc.advection(temp_var * units.kelvin, u=u_wind_var * units('m/s'), v=v_wind_var * units('m/s'),
                               dx=dx, dy=dy)
        return adv

    def adiabatic(time, level_isobaric=[1000, 850, 500, 200]):
        temp_var, w_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Wwnd'], level_isobaric=level_isobaric)

        pressure_var = np.ones([len(level_isobaric), len(lat_var), len(lon_var)])
        for i in range(len(level_isobaric)):
            # transform hPa into Pa
            pressure_var[i] = pressure_var[i] * level_isobaric[i] * 100
        pressure_var = pressure_var * units.Pa

        # Calculate temperature advection using metpy function
        adv = mpcalc.static_stability(pressure_var, temp_var * units.kelvin)

        Rd = units.Quantity(8.314462618, 'J / mol / K') / units.Quantity(28.96546e-3, 'kg / mol')
        adiabatic_warming = adv * (w_wind_var * units('Pa/s')) * pressure_var / Rd
        return adiabatic_warming


    s_advection = tem_advection_1000(time)
    s_adiabatic = adiabatic(time)

    s_year = int(time.strftime('%Y'))
    os.makedirs(path_SaveData + 'adibatic_cli/%i' % s_year, exist_ok=True)
    os.makedirs(path_SaveData + 'advection_cli/%i' % s_year, exist_ok=True)
    s_adiabatic.to_netcdf(path_SaveData + 'adibatic_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))
    s_advection.to_netcdf(path_SaveData + 'advection_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))


path_MainData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_SaveData = '/home/linhaozhong/work/AR_analysis/'
path_ARData = '/home/linhaozhong/work/AR_detection/new1_AR_contour_42/'
time_file = os.listdir(path_ARData)

# for i_time in time_file:
#
#     AR_info = xr.open_dataset(path_ARData+i_time)
#     for s_time in AR_info.time_all.values:
#
#         s_time = pd.to_datetime(s_time)
#         s_advection = tem_advection_1000(s_time)
#         s_adiabatic = adiabatic(s_time)
#
#         s_adiabatic.to_netcdf(path_SaveData+'adibatic/%s.nc'%s_time.strftime('%Y%m%d%H'))
#         s_advection.to_netcdf(path_SaveData + 'advection/%s.nc'%s_time.strftime('%Y%m%d%H'))

for i_time in time_file:

    AR_info = xr.open_dataset(path_ARData+i_time)
    for new_time in AR_info.time_all.values:
        new_time = pd.to_datetime(new_time)
        for s_year in range(1979,2020+1):
            s_time = pd.to_datetime('%i-%s' % (s_year, new_time.strftime('%m-%d %H:00')))
            if os.path.exists(path_SaveData+'adibatic_cli/%i/%s.nc' % (s_year,s_time.strftime('%Y%m%d%H'))):
                continue

            create_save_adv_adi(s_time)

# """
# =============================
# 1, 1000 hPa Temperature Advection,
# 2, adiabatic cooling and warming
# reference:
# Gong T, Feldstein S, Lee S. The Role of Downward Infrared Radiation in the Recent Arctic Winter Warming Trend. Journal of Climate. 2017;30(13):4937-4949. doi:10.1175/JCLI-D-16-0180.1
# Lee, S., Gong, T., Johnson, N., Feldstein, S. B., & Pollard, D. (2011). On the Possible Link between Tropical Convection and the Northern Hemisphere Arctic Surface Air Temperature Change between 1958 and 2001, Journal of Climate, 24(16), 4350-4367. Retrieved Feb 27, 2022, from https://journals.ametsoc.org/view/journals/clim/24/16/2011jcli4003.1.x
# =============================
# """
#
# ###############################
# # Imports
# from datetime import datetime
#
# import cartopy.crs as ccrs
# import cartopy.feature as cfeature
# import matplotlib.gridspec as gridspec
# import matplotlib.pyplot as plt
# import metpy.calc as mpcalc
# import pandas as pd
# from metpy.units import units
# import numpy as np
# import scipy.ndimage as ndimage
# import xarray as xr
# import nc4
# def get_data(var, level_isobaric):
#     '''
#     :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
#     :param level_isobaric:
#     :return:
#     '''
#     def get_svar_data(var):
#         data = xr.open_dataset(path_MainData+'%s/%s/%s.%s.nc'%(var, time[:4], var, time))
#         return data
#
#     result = []
#     if np.isin(var, 'Hgt').any():
#         hght_var = get_svar_data('Hgt').sel(level=level_isobaric)['z']
#         result.append(hght_var)
#     if np.isin(var, 'Tem').any():
#         temp_var = get_svar_data('Tem').sel(level=level_isobaric)['t']
#         result.append(temp_var)
#     if np.isin(var, 'Uwnd').any():
#         u_wind_var = get_svar_data('Uwnd').sel(level=level_isobaric)['u']
#         result.append(u_wind_var)
#     if np.isin(var, 'Vwnd').any():
#         v_wind_var = get_svar_data('Vwnd').sel(level=level_isobaric)['v']
#         result.append(v_wind_var)
#     if np.isin(var, 'Wwnd').any():
#         w_wind_var = get_svar_data('Wwnd').sel(level=level_isobaric)['w']
#         result.append(w_wind_var)
#
#     lat_var = result[0]['latitude']
#     lon_var = result[0]['longitude']
#     result.append(lat_var)
#     result.append(lon_var)
#     return result
#
#
# path_MainData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_SaveData = '/home/linhaozhong/work/AR_analysis/advection/'
#
# level_isobaric = 1000
# de_Temporal_resolution = 6
# time_all = pd.date_range('1979-01-01', '2020-12-31', freq='1D').strftime('%Y%m%d')
#
# for time in time_all:
#     # get daily data contains 24 hours
#     temp_var_row,  u_wind_var_row, v_wind_var_row, lat_var, lon_var = get_data(['Tem', 'Uwnd', 'Vwnd'], level_isobaric=1000)
#     daily_adv = []
#
#
#     for s_time_daily in range(int(24/de_Temporal_resolution)):
#         timestep = (s_time_daily+1)*de_Temporal_resolution-1
#         temp_var = temp_var_row.isel(time=timestep)
#         u_wind_var = u_wind_var_row.isel(time=timestep)
#         v_wind_var = v_wind_var_row.isel(time=timestep)
#
#         # between lat/lon grid points
#         dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)
#
#         # Calculate temperature advection using metpy function
#         adv = mpcalc.advection(temp_var * units.kelvin, u=u_wind_var*units('m/s'), v =v_wind_var*units('m/s'),
#                                dx=dx, dy=dy)
#         print(adv)
#         exit(0)
#         adv.to_netcdf(path_SaveData+'%s_%i.nc'%(time, timestep))
#         # daily_adv.append(adv)
#
#     # nc4.save_standard_nc(daily_adv, ['temp_advection'], pd.date_range('%s 05:00'%time, '%s 23:00'%time, freq='6H'),
#     #                      lon=None, lat=None, path_save=path_SaveData, Save_name='temp_adv_%s'%time)
#     # exit(0)
# exit(0)
# path_SaveData = '/home/linhaozhong/work/AR_analysis/adibatic/'
# level_isobaric = [1000, 850, 500, 200]
# pressure_var = np.ones([4,361,720])
# for i in range(len(level_isobaric)):
#     # transform hPa into Pa
#     pressure_var[i] = pressure_var[i] * level_isobaric[i]*100
# pressure_var = pressure_var*units.Pa
#
# for time in time_all:
#     # get daily data contains 24 hours
#     temp_var_row, w_wind_var_row, lat_var, lon_var = get_data(['Tem', 'Wwnd'], level_isobaric=level_isobaric)
#     daily_adv = []
#
#
#     for s_time_daily in range(int(24/de_Temporal_resolution)):
#         timestep = (s_time_daily+1)*de_Temporal_resolution-1
#         temp_var = temp_var_row.isel(time=timestep)
#         w_wind_var = w_wind_var_row.isel(time=timestep)
#
#         # between lat/lon grid points
#         dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)
#
#         # Calculate temperature advection using metpy function
#         adv = mpcalc.static_stability(pressure_var, temp_var * units.kelvin)
#
#         Rd = units.Quantity(8.314462618, 'J / mol / K')/units.Quantity(28.96546e-3, 'kg / mol')
#         adiabatic_warming = adv*(w_wind_var*units('Pa/s'))*pressure_var/Rd
#         print(w_wind_var*units('Pa/s').to(units('hPa/s')))
#         print(np.max(adiabatic_warming), np.min(adiabatic_warming))
#
#         exit(0)
#         adv.to_netcdf(path_SaveData+'%s_%i.nc'%(time, timestep))
#         # daily_adv.append(adv)







