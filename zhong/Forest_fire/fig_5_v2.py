import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import numpy as np
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore

"""
Use the scPDSI index from Figure S1 as the standard to extract high temperature years.
Use the 850hPa index as the standard to extract high wind years.

index definition:
yearly regional mean anomaly,  
"""

date_base = np.arange(1901, 2010+1)

ranges = [95,132,45,56]
months = [[2,3,4],[5]]

def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth

import cmaps
orig_cmap = cmaps.cmp_b2r

# import numpy as np
# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# cmap = cmaps.WhiteBlue
# viridis = cmap
# orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

year_selected_all = []
for i_month in months:

    def get_20cc_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        result = []
        var = ['uwnd850', 'vwnd850']
        shortName = ['uwnd', 'vwnd']
        shortName = dict(zip(var, shortName))
        for i_var in var:
            a = xr.open_dataset(r"D:\fire\20c/%s.mon.mean.nc"%i_var)
            m_ranged = get_wind(a, year, i_month)
            m_ranged = m_ranged.sel(lon=slice(ranges[0], ranges[1]),
                                    lat=slice(ranges[2], ranges[3]))

            # print(m_ranged)
            da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
            m_ranged = da.groupby("year_month").mean("time")

            result.append(m_ranged[shortName[i_var]].values)

        # to wind speed
        return np.nanmean(np.sqrt(result[0]**2+result[1]**2))

    def get_PDSI_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """

        a = xr.open_dataset(r"D:\fire\scPDSI\scPDSI.cru_ts4.05early1.1901.2020.cal_1901_20.bams.2021.GLOBAL.1901.2020.nc")

        m_ranged = get_wind(a, year, i_month)
        m_ranged = m_ranged.sel(longitude=slice(ranges[0], ranges[1]),
                                latitude=slice(ranges[3], ranges[2]))
        # print(m_ranged)

        da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        m_ranged = da.groupby("year_month").mean("time")

        return np.nanmean(m_ranged['scpdsi'].values)

    index = []

    for i_year in date_base:
        if i_month[0] == 2:
            index.append(get_PDSI_byYear(i_year))
            # print('call pdsi')
        else:
            index.append(get_20cc_byYear(i_year))
            # print('call wind850')

    index_detrend = detrend(index, type='linear')

    rti = index_detrend
    line = np.quantile(rti, [0.4,0.6])
    print(len(date_base))

    if i_month[0] == 5:
        # print(np.quantile(rti, 0.6))
        # print(np.quantile(rti, 0.596))

        year_selected = date_base[rti >= line[1]]
        year_selected_all.append(year_selected)
    else:

        year_selected = date_base[rti <= line[0]]
        year_selected_all.append(year_selected)


# all_rti = list(savemat[0])+list(savemat[1])
# year_now = date_base[all_rti <= line[0]]
year_selected_234 = year_selected_all[0]
year_selected_5 = year_selected_all[1]
set1 = set(year_selected_234)
set2 = set(year_selected_5)

common_elements = set1.intersection(set2)
# print(sorted(common_elements))

scipy.io.savemat('figure5_year_selected.mat', {'y234':np.array(list(year_selected_234)),
                                               'y5':np.array(list(year_selected_5)),
                                               'y_both':np.array(list(common_elements)),
                                               'date_base':np.array(date_base)})


from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore

year_selected_mat = scipy.io.loadmat('figure5_year_selected.mat')

year_234 = year_selected_mat['y234'][0]
year_5 = year_selected_mat['y5'][0]
year_both = year_selected_mat['y_both'][0]
date_base = year_selected_mat['date_base'][0]

date_exclude = np.array(sorted(set(date_base.tolist()).difference(set(year_both.tolist()))))
date_exclude234 = np.array(sorted(set(year_234.tolist()).difference(set(year_both.tolist()))))
date_exclude5 = np.array(sorted(set(year_5.tolist()).difference(set(year_both.tolist()))))

begin_year = date_base[0]

data_row = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])
data = data_row[(data_row['year'] >=date_base[0]) & (data_row['year'] <= date_base[-1])]

detrended1 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

# data = data_row[(data_row['year'] >= 1900) & (data_row['year'] <= 2010)]
# detrended2 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

data = np.array(detrended1)
def get_fireIndexbyYEAR(year):
    return data[year-begin_year]


import numpy as np
from scipy.stats import norm

from sklearn.neighbors import KernelDensity

# ----------------------------------------------------------------------
# Plot a 1D density example



X_plot = np.linspace(-2, 4, 1000)
colors = ["navy"]
kernels = ["gaussian"]
lw = 2

X_plot = np.linspace(-2, 4, 1000)
def plot_pdf(year_here, bandwidth='scott'):
    from scipy import stats
    X = get_fireIndexbyYEAR(year_here)
    # kde = stats.gaussian_kde(np.squeeze(X),bw_method=bandwidth)
    kde = stats.gaussian_kde(np.squeeze(X), bw_method=bandwidth / X.std(ddof=1))
    f = kde.covariance_factor()
    bw = f * X.std()
    log_dens = kde.evaluate(X_plot)
    return log_dens, X
# def plot_pdf(year_here,bandwidth, name):
#
#     X = get_fireIndexbyYEAR(year_here)[:, np.newaxis]
#     X_plot = np.linspace(-3, 5, 1000)[:, np.newaxis]
#
#
#     kde = KernelDensity(kernel='gaussian', bandwidth={'scott'}).fit(X)
#     log_dens = kde.score_samples(X_plot)
#
#     return np.exp(log_dens)


def point_pixelTo_coors(piexl2, points):
    points = np.array(points)
    one_piexl = 2/38
    points = (points-piexl2)*one_piexl-2
    return points

def monte_carlo_in_PDF(total, data1, data1_pdf, p_value=90, type_test='double'):

    import random

    div = []
    for i in range(1000):
        s_data1 = random.sample(set(total), len(data1))
        pdf_now,_ = plot_pdf(s_data1, window_width)
        div.append(pdf_now)

    div = np.array(div)
    print(div.shape)
    if type_test == 'right':
        div_Pvalue = np.percentile(div, p_value, axis=0)
        # plt.plot(X_plot, np.percentile(div, 90, axis=0), label='90',lw=0.1)
        # plt.plot(X_plot, np.percentile(div, 95, axis=0), label='95',lw=0.2)
        # plt.plot(X_plot, np.percentile(div, 99, axis=0), label='99',lw=0.5)
        #
        # plt.plot(X_plot, data1_pdf,lw=1)
        # plt.plot(X_plot, data2_pdf, lw=1, label='ALL')
        # plt.legend()
        # plt.show()
        # print(div_Pvalue)
        return data1_pdf >= div_Pvalue

    if type_test == 'left':
        div_Pvalue = np.percentile(div, 100-p_value, axis=0)
        return data1_pdf <= div_Pvalue

    if type_test == 'double':
        p_value = 100-(100-p_value)/2

        div_Pvalue_left = np.percentile(div, 100 - p_value, axis=0)
        div_Pvalue_right = np.percentile(div, p_value, axis=0)

        return (data1_pdf >= div_Pvalue_right) | (data1_pdf<=div_Pvalue_left)


result = scipy.io.loadmat('Figure5_pdf.mat')
# result['f5a_ptest_p'][0][250:300] = 0
# result['f5b_ptest_p'][0][800:] = 0
# scipy.io.savemat('Figure5_pdf.mat', result)
# exit()
fig, axs = plt.subplots(3,1,figsize=(5,5))
result_label = ['a', 'b', 'c']
labels = [
    ['All(%0.0f)'%len(date_base), 'both(%0.0f)'%len(year_both)],
    ['Exclude both(%0.0f)'%len(date_exclude), '2-4 drought(%0.0f)'%len(date_exclude234)],
    ['Exclude both(%0.0f)'%len(date_exclude), '5 wind(%0.0f)'%len(date_exclude5)]
]
result['labels'] = ['All(%0.0f)'%len(date_base), 'both(%0.0f)'%len(year_both),
    'Exclude both(%0.0f)'%len(date_exclude), '2-4 drought(%0.0f)'%len(date_exclude234),
    'Exclude both(%0.0f)'%len(date_exclude), '5 wind(%0.0f)'%len(date_exclude5)]

for i in range(3):
    ax = axs[i]

    ax.plot(
        X_plot,
        result['f5%s_line1'%result_label[i]][0],
        lw=1,
        color='black',
        linestyle="-",
        label=labels[i][0],
    )
    ax.plot(
        X_plot,
        result['f5%s_line2'%result_label[i]][0],
        lw=1,
        color='black',
        linestyle="--",
        label=labels[i][1],
    )

    logic = result['f5%s_ptest_p'%result_label[i]][0]

    y1 = result['f5%s_line1'%result_label[i]][0]
    y2 = result['f5%s_line2'%result_label[i]][0]
    ax.fill_between(x=X_plot,y1=y1, y2=y2,
                     where=logic,
                     facecolor='green')
    # ax.fill_between(x=X_plot,y1=y1, y2=y2,
    #                  where=logic & (y1<y2),
    #                  facecolor='red')
    ax.legend(loc="upper left", fontsize=5)
    ax.set_xlim(-3, 5)
    ax.set_ylim(0, 1.5)

plt.show()

exit()
result = {}
result['x'] = X_plot

window_width = 0.15

key_forMonteCarlo = {'p_value':80, 'type_test':'double'}

a,a_x = plot_pdf(date_base, window_width)
b,b_x = plot_pdf(year_both, window_width)
# b,b_x = plot_pdf(date_exclude, window_width)
# exit()
result['f5a_line1'] = a
result['f5a_line2'] = b

p_test = monte_carlo_in_PDF(date_base, year_both, b, **key_forMonteCarlo)
result['f5a_ptest_p'] = p_test


a,_ = plot_pdf(date_exclude, window_width)
b,_ = plot_pdf(date_exclude234, window_width)
result['f5b_line1'] = a
result['f5b_line2'] = b

p_test = monte_carlo_in_PDF(date_base, date_exclude234, b, **key_forMonteCarlo)
result['f5b_ptest_p'] = p_test

""""""
# for bandwidth_a in np.arange(0.1, 0.5,0.01):
#     for bandwidth_b in np.arange(0.1,0.5,0.01):
#         a = plot_pdf(date_exclude, bandwidth_a, 'all')
#         b = plot_pdf(date_exclude5,bandwidth_b, 'min')
#
#         vline = [-1.15,0.64,2.79]
#         yes = 0
#         for i_vline in vline:
#             vline_loc = np.argmin(abs(X_plot-i_vline))
#             if abs(a[vline_loc] - b[vline_loc]) < 0.001:
#                 yes+=1
#         if yes == 3:
#             print('pass', bandwidth_a, bandwidth_b)
#             fig, ax = plt.subplots()
#             for i in vline:
#                 ax.axvline(i)
#             ax.legend(loc="upper left")
#             ax.plot(
#                 X_plot,
#                 a,
#                 lw=lw,
#                 linestyle="-",
#                 label='max',
#             )
#             ax.plot(
#                 X_plot,
#                 b,
#                 lw=lw,
#                 linestyle="-",
#                 label='min',
#             )
#             ax.set_xlim(-3, 5)
#             ax.set_ylim(0, 0.8)
#             plt.show()
#             plt.close()


a,_ = plot_pdf(date_exclude, window_width)
b,_ = plot_pdf(date_exclude5, window_width)
result['f5c_line1'] = a
result['f5c_line2'] = b

p_test = monte_carlo_in_PDF(date_base, date_exclude234, b, **key_forMonteCarlo)
result['f5c_ptest_p'] = p_test

# result['f5c_p_tick'] = vline

# exit()

""" ===================== """
# def monte_carlo_in_PDF(data1, data2, X, test_range_num=20, p_value=90, type_test='double'):
#     import random
#     # divided into space range and test
#     test_range = [int(i) for i in np.linspace(0,len(X)-1, test_range_num)]
#
#     def monte_carlo(data1, data2):
#         data = np.append(data1, data2)
#         div = []
#         for i in range(1000):
#             s_data1 = random.sample(set(data), len(data1))
#             s_data2 = np.setdiff1d(data, s_data1)
#             s_div = np.nanmean(s_data1) - np.nanmean(s_data2)
#             div.append(s_div)
#
#         p_value_now = p_value
#         if type_test == 'left':
#             p_value_now = 1-p_value
#         if type_test == 'double':
#             div = np.abs(div)
#         div_Pvalue = np.percentile(div, p_value_now)
#
#         div_test = np.nanmean(data1) - np.nanmean(data2)
#
#         if type_test == 'right':
#             return div_test >= div_Pvalue
#         elif type_test == 'left':
#             return div_test <= div_Pvalue
#         elif type_test == 'double':
#             return np.abs(div_test) >= np.abs(div_Pvalue)
#
#
#     result = []
#
#     for i in range(len(test_range))[:-1]:
#
#         result.append(
#             monte_carlo(
#                         data1[test_range[i]:test_range[i + 1]],
#                         data2[test_range[i]:test_range[i + 1]])
#                       )
#
#     return X[test_range], result
#
#
# p_test = monte_carlo_in_PDF(a, b, X_plot, p_value=99, type_test='double', test_range_num=20)
# result['f5a_ptest_X'] = p_test[0]
# result['f5a_ptest_p'] = p_test[1]


fig, axs = plt.subplots(3,1,figsize=(5,5))
result_label = ['a', 'b', 'c']
labels = [
    ['All(%0.0f)'%len(date_base), 'both(%0.0f)'%len(year_both)],
    ['Exclude both(%0.0f)'%len(date_exclude), '2-4 drought(%0.0f)'%len(date_exclude234)],
    ['Exclude both(%0.0f)'%len(date_exclude), '5 wind(%0.0f)'%len(date_exclude5)]
]
result['labels'] = ['All(%0.0f)'%len(date_base), 'both(%0.0f)'%len(year_both),
    'Exclude both(%0.0f)'%len(date_exclude), '2-4 drought(%0.0f)'%len(date_exclude234),
    'Exclude both(%0.0f)'%len(date_exclude), '5 wind(%0.0f)'%len(date_exclude5)]

for i in range(3):
    ax = axs[i]

    ax.plot(
        X_plot,
        result['f5%s_line1'%result_label[i]],
        lw=1,
        color='black',
        linestyle="-",
        label=labels[i][0],
    )
    ax.plot(
        X_plot,
        result['f5%s_line2'%result_label[i]],
        lw=1,
        color='black',
        linestyle="--",
        label=labels[i][1],
    )

    logic = result['f5%s_ptest_p'%result_label[i]]

    y1 = result['f5%s_line1'%result_label[i]]
    y2 = result['f5%s_line2'%result_label[i]]
    ax.fill_between(x=X_plot,y1=y1, y2=y2,
                     where=logic,
                     facecolor='green')
    # ax.fill_between(x=X_plot,y1=y1, y2=y2,
    #                  where=logic & (y1<y2),
    #                  facecolor='red')
    ax.legend(loc="upper left", fontsize=5)
    ax.set_xlim(-3, 5)
    ax.set_ylim(0, 1.5)


scipy.io.savemat('Figure5_pdf.mat', result)
plt.show()
plt.close()

exit()