"""downlod """
import os.path
import xarray as xr
import numpy as np
import pandas as pd
# year_b = 2007
# year_e = 2007
# # https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/20070701-ESACCI-L3S_FIRE-BA-MODIS-AREA_2-fv5.1.tar.gz
# str_begin = r'start https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/'
# str_begin = r'start https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/'
#
# time_all = pd.date_range('%i-01'%year_b, '%i-12'%year_e, freq='1M').strftime('%Y%m')
# str_all = []
# i = 0
# for i_time in time_all:
#     month = int(i_time[4:6])
#     for i_v in range(1,6+1):
#         str_end = '01-ESACCI-L3S_FIRE-BA-MODIS-AREA_%i-fv5.1.tar.gz'%i_v
#         str_now = i_time[:4]+'/'+i_time+str_end
#         # print(r'C:\FFOutput/'+str_now)
#         if i_time[:4] == '2007':
#             continue
#         if not os.path.exists(r'C:\FFOutput/'+i_time+str_end):
#             str_all.append(str_begin+str_now)
#             print(str_now)
#             i += 1
#         if i>100:
#             break
# print(i)
# np.savetxt('ESA_CCIFire51.bat', str_all, fmt='%s')
# exit()
# year_b = 2007
# year_e = 2008
# # https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/20070701-ESACCI-L3S_FIRE-BA-MODIS-AREA_2-fv5.1.tar.gz
# str_begin = r'start https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/'
#
# time_all = pd.date_range('%i-01'%year_b, '%i-01'%year_e, freq='1M').strftime('%Y%m')
# str_all = []
# i = 0
# for i_time in time_all:
#     month = int(i_time[4:6])
#     for i_v in range(1,6+1):
#         str_end = '01-ESACCI-L3S_FIRE-BA-MODIS-AREA_%i-fv5.1.tar.gz'%i_v
#         str_now = '/'+i_time+str_end
#         # print(r'C:\FFOutput/'+str_now)
#         if not os.path.exists(r'C:\FFOutput/'+i_time+str_end):
#             str_all.append(str_begin+str_now)
#             print(str_now)
#             i += 1
#         if i>100:
#             break
# print(i)
# np.savetxt('ESA_CCIFire51.bat', str_all, fmt='%s')
# exit()
"""
FIG2
FIG3
"""
types = 'EKF' # EKF / 20C
if types == 'EKF':
    date_combine_high_freq_year =[1854, 1846, 1874, 1899, 1866, 1751, 1766, 1898, 1883, 1863, 1843, 1815, 1885, 1858, 1758]
    date_low_freq_year  = [1886, 1840, 1897, 1860, 1777, 1820, 1757, 1892, 1891, 1861, 1888, 1882, 1848, 1781, 1810]
    date_base = np.arange(1750,1899+1)
elif types == '20C':
    date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
    date_low_freq_year = [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
    date_base = np.arange(1900, 2010+1)

ranges = [97,126,45,54]
months = [[2,3,4],[5]]

def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth

import cmaps
orig_cmap = cmaps.cmp_b2r

# import numpy as np
# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# cmap = cmaps.WhiteBlue
# viridis = cmap
# orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

savemat = []
for i_month in months:

    def get_EKF400_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        hgt500 = []
        t2m = []
        u850 = []
        v850 = []
        for i_year in year:
            a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_%i_v1.1.nc"%i_year)
            m_ranged = get_wind(a, i_year, i_month)
            hgt500.append(m_ranged['geopotential_height'].sel(pressure_level_gph=500).mean(dim='time').values)
            t2m.append(m_ranged['air_temperature'].sel(level_temp=2).mean(dim='time').values)
            u850.append(m_ranged['eastward_wind'].sel(pressure_level_wind=850).mean(dim='time').values)
            v850.append(m_ranged['northward_wind'].sel(pressure_level_wind=850).mean(dim='time').values)

            # hgt500.extend(m_ranged['geopotential_height'].sel(pressure_level_gph=500).values)
            # t2m.extend(m_ranged['air_temperature'].sel(level_temp=2).values)
            # u850.extend(m_ranged['eastward_wind'].sel(pressure_level_wind=850).values)
            # v850.extend(m_ranged['northward_wind'].sel(pressure_level_wind=850).values)

        return [np.array(hgt500), np.array(t2m), np.sqrt(np.array(u850)**2+np.array(v850)**2)]

    def get_20cc_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        result = []
        var = ['hgt500', 'air.2m', 'uwnd850', 'vwnd850', 'qu', 'qv']
        shortName = ['hgt', 'air', 'uwnd', 'vwnd', 'qu', 'qv']
        shortName = dict(zip(var, shortName))
        for i_var in var:
            a = xr.open_dataset(r"D:\fire\20c/%s.mon.mean.nc"%i_var)
            m_ranged = get_wind(a, year, i_month)
            da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
            m_ranged = da.groupby("year_month").mean("time")
            if i_var == 'air':
                m_ranged = m_ranged.sel(level=1000)
                print(m_ranged[shortName[i_var]])
            result.append(m_ranged[shortName[i_var]].values)
        result_new = [result[0], result[1],
                      np.sqrt(np.array(result[2])**2+np.array(result[3])**2),
                      result[4], result[5]]

        return result_new

    if types == 'EKF':
        high = get_EKF400_byYear(date_combine_high_freq_year)
        lows = get_EKF400_byYear(date_low_freq_year)
        a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean_1970_v1.nc")
        lon = a.longitude.values
        lat = a.latitude.values

    elif types == '20C':
        high = get_20cc_byYear(date_combine_high_freq_year)
        lows = get_20cc_byYear(date_low_freq_year)

        a = xr.open_dataset(r"D:\fire\20c\hgt500.mon.mean.nc")
        lon = a.lon.values
        lat = a.lat.values

    from scipy.stats import ttest_ind
    p = []
    for i in range(len(high)):
        _,pvalues = ttest_ind(high[i], lows[i], axis=0, equal_var=False)
        p.append(pvalues)


    high = np.nanmean(high, axis=1)
    lows = np.nanmean(lows, axis=1)

    data = high-lows
    savemat.append([data,p])

import scipy.io as scio
m234, m234P = savemat[0]
m5, m5P = savemat[1]

if types == '20C':
    scio.savemat('figure2.mat', {
        'f2a_M234_Hgt500':m234[0],
        'f2a_M234_Hgt500_p':m234P[0],
        'f2a_M234_t2m':m234[1],

        'f2a_M5_Hgt500': m5[0],
        'f2a_M5_Hgt500_p': m5P[0],
        'f2a_M5_t2m': m5[1],


        'f2b_M234_wind850': m234[2],
        'f2b_M234_wind850_p': m234P[2],

        'f2b_M5_wind850': m5[2],
        'f2b_M5_wind850_p': m5P[2],


        'f2c_M234_IVT': np.sqrt(m234[3]**2+m234[4]**2),
        'f2c_M234_IVT_p': np.minimum(m234P[3], m234P[4]),
        'f2c_M234_qu': m234[3],
        'f2c_M234_qv': m234[4],

        'f2c_M5_IVT': np.sqrt(m5[3]**2+m5[4]**2),
        'f2c_M5_IVT_p': np.minimum(m5P[3], m5P[4]),
        'f2c_M5_qu': m5[3],
        'f2c_M5_qv': m5[4],

        'lon':lon,
        'lat':lat
        })

elif types == 'EKF':
    scio.savemat('figure3.mat', {
        'f3a_M234_Hgt500': m234[0],
        'f3a_M234_Hgt500_p': m234P[0],
        'f3a_M234_t2m': m234[1],

        'f3a_M5_Hgt500': m5[0],
        'f3a_M5_Hgt500_p': m5P[0],
        'f3a_M5_t2m': m5[1],

        'f3b_M234_wind850': m234[2],
        'f3b_M234_wind850_p': m234P[2],

        'f3b_M5_wind850': m5[2],
        'f3b_M5_wind850_p': m5P[2],
        'lon': lon,
        'lat': lat

    })



exit()



from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
import matplotlib.pyplot as plt
data_row = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])
data = data_row[(data_row['year'] >= 1750) & (data_row['year'] <= 1899)]
from scipy.stats import linregress
slope, intercept, r_value, p_value, std_err = linregress(data['year'], data['nechinamogoliaSiberia'])
line1 = np.array(data['year'])* slope + intercept
detrended1 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

data = data_row[(data_row['year'] >= 1900) & (data_row['year'] <= 2010)]
slope, intercept, r_value, p_value, std_err = linregress(data['year'], data['nechinamogoliaSiberia'])
line2 = np.array(data['year'])* slope + intercept
detrended2 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

import scipy.io as scio
scio.savemat('figure1.mat', {'f1c':np.array(data_row['nechinamogoliaSiberia'][(data_row['year'] >= 1750) & (data_row['year'] <= 2010)]),
                             'f1d':np.array(zscore(detrended1+detrended2)),
                             'year':range(1750, 2010+1),
                             'line1y':line1,
                             'line2y':line2,
                             'line1x': range(1750, 1899+1),
                             'line2x':range(1900, 2010+1),
                             },
             )
detrended = pd.Series(zscore(detrended1+detrended2), index=range(1750, 2010+1))
detrended.plot()
plt.show()