import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import numpy as np
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore

types = 'EKF' # EKF / 20C

date_base_low = np.arange(1836,1899+1)

date_base_high = np.arange(1900, 2010+1)
date_base = np.arange(1836,2010+1)

ranges = [97,126,45,54]
months = [[2,3,4],[5]]

def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth



import cmaps
orig_cmap = cmaps.cmp_b2r

# import numpy as np
# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# cmap = cmaps.WhiteBlue
# viridis = cmap
# orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

savemat = []
year_selected_all = []
for i_month in months:

    def get_EKF400_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        t2m = []

        for i_year in year:
            a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean_%i_v1.nc"%i_year)
            m_ranged = get_wind(a, i_year, i_month)
            m_ranged =  m_ranged['air_temperature'].sel(level_temp=2).mean(dim='time').sel(longitude=slice(97,126))
            lat = m_ranged.latitude.values
            loc54 = np.argmin(abs(lat - 53.67))
            # print(lat[loc54])
            loc45 = np.argmin(abs(lat - 46.76))
            # print(lat)
            m_ranged_north = np.nanmean(m_ranged.isel(latitude=loc54, ).values)
            m_ranged_south = np.nanmean(m_ranged.isel(latitude=loc45).values)
            tem_grad = (m_ranged_south - m_ranged_north) / 7.45935
            t2m.append(tem_grad)

        return np.array(t2m)

    def get_20cc_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        result = []
        var = ['air.2m']
        shortName = [ 'air']
        shortName = dict(zip(var, shortName))
        for i_var in var:
            a = xr.open_dataset(r"D:\fire\20c/%s.mon.mean.nc"%i_var)
            m_ranged = get_wind(a, year, i_month)
            da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
            m_ranged = da.groupby("year_month").mean("time").sel(lon=slice(97, 125))
            m_ranged = m_ranged['air']
            lat = m_ranged.lat.values
            loc54 = np.argmin(abs(lat - 53.67))
            loc45 = np.argmin(abs(lat - 46.76))
            m_ranged_north = m_ranged.isel(lat=loc54).mean(dim='lon').values
            m_ranged_south = m_ranged.isel(lat=loc45).mean(dim='lon').values

            tem_grad = (m_ranged_south - m_ranged_north) / 7.45935


        return tem_grad

    high = get_20cc_byYear(date_base_high)
    lows = get_20cc_byYear(date_base_low)
    # detrended = pd.Series(high)
    # ax = detrended.plot()
    # # ax.axhline(line[0])
    # plt.show()
    high = detrend(high, type='linear')
    lows = detrend(lows, type='linear')
    # high = high
    # lows = lows

    rti = np.array(list(lows)+list(high))
    line = np.quantile(rti, [0.4,0.6])
    print(len(date_base))

    if i_month[0] == 5:
        # print(np.quantile(rti, 0.6))
        # print(np.quantile(rti, 0.596))

        year_selected = date_base[rti >= line[1]]
        year_selected_all.append(year_selected)
    else:

        year_selected = date_base[rti <= line[0]]
        year_selected_all.append(year_selected)


    savemat.append([rti, line])

# all_rti = list(savemat[0])+list(savemat[1])
# year_now = date_base[all_rti <= line[0]]
year_selected_234 = year_selected_all[0]
year_selected_5 = year_selected_all[1]
print(list(year_selected_234))
print(list(year_selected_5))
set1 = set(year_selected_234)
set2 = set(year_selected_5)

common_elements = set1.intersection(set2)
# print(sorted(common_elements))
print(common_elements)

# print([1850, 1857, 1859, 1881, 1889, 1905, 1906, 1907, 1931, 1932, 1963, 1972, 1983, 1986, 2000, 2003])
# exit()
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
data_row = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])

data = data_row[(data_row['year'] >= 1836) & (data_row['year'] <= 1899)]
detrended1 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

data = data_row[(data_row['year'] >= 1900) & (data_row['year'] <= 2010)]
detrended2 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))


import scipy.io as scio
m234, m234P = savemat[0]
m5, m5P = savemat[1]

scio.savemat('figureS5.mat', {
    's5a':zscore(detrended1+detrended2),

    's5b':m234,
    's5b_quantile':m234P[0],

    's5c': m5,
    's5c_quantile': m5P[1],

    'year':date_base
    })
exit()

from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore

year_234 = np.array([1836, 1838, 1840, 1841, 1846, 1850, 1852, 1854, 1857, 1859, 1860, 1863, 1866, 1875, 1878, 1881, 1882, 1883, 1884, 1885, 1886, 1889, 1891, 1893, 1896, 1897, 1900, 1903, 1905, 1906, 1907, 1915, 1916, 1917, 1921, 1925, 1931, 1932, 1934, 1936, 1938, 1939, 1940, 1943, 1944, 1945, 1947, 1949, 1950, 1951, 1953, 1957, 1963, 1967, 1968, 1972, 1975, 1983, 1986, 1988, 1989, 1990, 1993, 1995, 1997, 2000, 2003, 2005, 2008, 2010])
year_5 = np.array([1837, 1839, 1842, 1843, 1844, 1847, 1848, 1849, 1850, 1851, 1853, 1857, 1867, 1871, 1872, 1877, 1881, 1887, 1888, 1889, 1890, 1891, 1894, 1898, 1899, 1902, 1905, 1906, 1907, 1909, 1910, 1911, 1913, 1914, 1919, 1920, 1923, 1924, 1925, 1926, 1928, 1930, 1931, 1932, 1933, 1935, 1956, 1958, 1959, 1963, 1969, 1972, 1974, 1978, 1980, 1983, 1984, 1985, 1986, 1987, 1991, 1994, 1996, 1998, 2000, 2003, 2004, 2006, 2007, 2009])
year_both = np.array(sorted([1857, 1986, 1889, 1891, 1925, 1963, 1931, 1932, 2000, 1905, 1906, 2003, 1972, 1907, 1881, 1850, 1983]))
date_base = np.arange(1836,2010+1)
date_exclude = np.array(sorted(set(date_base).difference(set(year_both))))
date_exclude234 = np.array(sorted(set(year_234).difference(set(year_both))))
date_exclude5 = np.array(sorted(set(year_5).difference(set(year_both))))
print(date_exclude.shape, date_exclude234.shape, date_exclude5.shape)
begin_year = 1836

data_row = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])
data = data_row[(data_row['year'] >= 1836) & (data_row['year'] <= 1899)]

detrended1 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

data = data_row[(data_row['year'] >= 1900) & (data_row['year'] <= 2010)]
detrended2 = list(zscore(detrend(data['nechinamogoliaSiberia'], type='linear')))

data = np.array(detrended1+detrended2)
print(data.shape)
def get_fireIndexbyYEAR(year):
    return data[year-begin_year]


import numpy as np
from scipy.stats import norm

from sklearn.neighbors import KernelDensity

# ----------------------------------------------------------------------
# Plot a 1D density example



X_plot = np.linspace(-3, 5, 1000)
colors = ["navy"]
kernels = ["gaussian"]
lw = 2
# def plot_pdf(year_here,bandwidth, name):
#
#     X = get_fireIndexbyYEAR(year_here)[:, np.newaxis]
#     X_plot = np.linspace(-3, 5, 1000)[:, np.newaxis]
#
#
#     kde = KernelDensity(kernel='gaussian', bandwidth={'scott'}).fit(X)
#     log_dens = kde.score_samples(X_plot)
#
#     return np.exp(log_dens)
def plot_pdf(year_here, bandwidth='scott'):
    from scipy import stats
    X = get_fireIndexbyYEAR(year_here)
    kde = stats.gaussian_kde(np.squeeze(X),bw_method=bandwidth)
    f = kde.covariance_factor()
    bw = f * X.std()
    print(bw)
    log_dens = kde.evaluate(X_plot)
    return log_dens

def point_pixelTo_coors(piexl2, points):
    points = np.array(points)
    one_piexl = 2/38
    points = (points-piexl2)*one_piexl-2
    return points

result = {}
a = plot_pdf(date_base, 0.36)
b = plot_pdf(year_both, 0.30)
vline = [-0.62,0.78,3.17]
vline = point_pixelTo_coors(70, [83,93,99,115,130,155,171,182])
result['f5a_line1'] = a
result['f5a_line2'] = b
result['x'] = X_plot
result['f5a_p_tick'] = vline









a = plot_pdf(date_exclude,0.3625522934180812)
b = plot_pdf(date_exclude234,0.38)
# vline = [-1.45,0.6,2.63,3.68]
vline = point_pixelTo_coors(277, [280,293,297,331,337])
result['f5b_line1'] = a
result['f5b_line2'] = b
result['f5b_p_tick'] = vline
""""""
# for bandwidth_a in np.arange(0.1, 0.5,0.01):
#     for bandwidth_b in np.arange(0.1,0.5,0.01):
#         a = plot_pdf(date_exclude, bandwidth_a, 'all')
#         b = plot_pdf(date_exclude5,bandwidth_b, 'min')
#
#         vline = [-1.15,0.64,2.79]
#         yes = 0
#         for i_vline in vline:
#             vline_loc = np.argmin(abs(X_plot-i_vline))
#             if abs(a[vline_loc] - b[vline_loc]) < 0.001:
#                 yes+=1
#         if yes == 3:
#             print('pass', bandwidth_a, bandwidth_b)
#             fig, ax = plt.subplots()
#             for i in vline:
#                 ax.axvline(i)
#             ax.legend(loc="upper left")
#             ax.plot(
#                 X_plot,
#                 a,
#                 lw=lw,
#                 linestyle="-",
#                 label='max',
#             )
#             ax.plot(
#                 X_plot,
#                 b,
#                 lw=lw,
#                 linestyle="-",
#                 label='min',
#             )
#             ax.set_xlim(-3, 5)
#             ax.set_ylim(0, 0.8)
#             plt.show()
#             plt.close()



a = plot_pdf(date_exclude, 0.36255)
b = plot_pdf(date_exclude5, 0.36)
vline = [-1.15,0.64,2.79]
vline = point_pixelTo_coors(481, [476,488])
result['f5c_line1'] = a
result['f5c_line2'] = b
result['f5c_p_tick'] = vline




def monte_carlo_in_PDF(data1, data2, X, test_range_num=20, p_value=90, type_test='double'):
    import random
    # divided into space range and test
    test_range = [int(i) for i in np.linspace(0,len(X)-1, test_range_num)]

    def monte_carlo(data1, data2):
        data = np.append(data1, data2)
        div = []
        for i in range(1000):
            s_data1 = random.sample(set(data), len(data1))
            s_data2 = np.setdiff1d(data, s_data1)
            s_div = np.nanmean(s_data1) - np.nanmean(s_data2)
            div.append(s_div)

        p_value_now = p_value
        if type_test == 'left':
            p_value_now = 1-p_value
        if type_test == 'double':
            div = np.abs(div)
        div_Pvalue = np.percentile(div, p_value_now)
        div_test = np.nanmean(data1) - np.nanmean(data2)

        if type_test == 'right':
            return div_test >= div_Pvalue
        elif type_test == 'left':
            return div_test <= div_Pvalue
        elif type_test == 'double':
            return np.abs(div_test) >= np.abs(div_Pvalue)


    result = []

    for i in range(len(test_range))[:-1]:

        # result.append(
        #     monte_carlo(
        #                 data1[test_range[i]:test_range[i + 1]],
        #                 data2[test_range[i]:test_range[i + 1]])
        #               )
        from scipy import stats
        _, p_values = stats.ttest_ind(data1[test_range[i]:test_range[i + 1]],
                        data2[test_range[i]:test_range[i + 1]], equal_var=False)
        result.append(
            p_values
                      )
    return X[test_range], result
a = monte_carlo_in_PDF(a*100, b*100, X_plot, p_value=90, type_test='double', test_range_num=20)
print(a)
exit()
# scipy.io.savemat('Figure5_pdf.mat', result)
exit()
fig, ax = plt.subplots(figsize=(5,5))
for i in vline:
    ax.axvline(i)
ax.legend(loc="upper left")
ax.plot(
    X_plot,
    a,
    lw=lw,
    linestyle="-",
    label='max',
)
ax.plot(
    X_plot,
    b,
    lw=lw,
    linestyle="-",
    label='min',
)
ax.set_xlim(-3, 5)
ax.set_ylim(0, 0.8)
plt.show()
plt.close()

# def plots(ax, data, bin_width, mean_label=['','']):
#     percentile_label = ['Q1', 'Q2', 'Q3']
#     percentile = np.percentile(data, [25,50,75])
#     mean = np.nanmean(data)
#     HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
#     # ax.boxplot(data)
#
#     _, _, bar_container = ax.hist(data,bins=bin_width,density=True, lw=1,
#                               ec="black", fc="gray",zorder=2)
#
#     # yticks = ax.get_yticks()
#     # ax.set_yticks(yticks)
#     # ax.set_yticklabels((yticks*len(data)).astype('int'))
#
#     ylim_max = ax.get_ylim()[1]
#     trans = ax.get_xaxis_transform()
#     for i_percentile in range(3):
#         ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
#         ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
#                 transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
#                 bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
#         )
#         ax.text(percentile[i_percentile], 1.01, '%0.2f'%percentile[i_percentile], transform=trans,
#                 horizontalalignment='center', fontsize=infig_fontsize)
#
#     ax.text(0.9, 0.89, mean_label[0]+'%0.1f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)
#
#     return [ax.get_ylim()[1], ax.get_xlim()[1]]




exit()