"""downlod """
import os.path
import xarray as xr
import numpy as np
import pandas as pd
"""
The database of EKF and 20C has been changed to cover the whole period of both datasets.
The high and low fire years have been limited to a length of 10 years before and after 1900.
"""

# from scipy.signal import detrend
# import pandas as pd
# from scipy.stats import zscore
# import matplotlib.pyplot as plt
#
# fig, axs = plt.subplots(nrows=3,ncols=1)
# plt.subplots_adjust(top=0.95,
# bottom=0.05,
# left=0.125,
# right=0.9,
# hspace=0.5,
# wspace=0.5)
# usecol_all = [3,6,7]
# colname_all = ['NEChina','Mongo', 'BaikalRURom']
#
# year_high_all = []
# year_low_all = []
# for i_col in range(3):
#     usecol=[0,usecol_all[i_col]]
#     colname=colname_all[i_col]
#     data_row = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=usecol)
#     data = data_row[(data_row['year'] >= 1750) & (data_row['year'] <= 1899)]
#     from scipy.stats import linregress
#     slope, intercept, r_value, p_value, std_err = linregress(data['year'], data[colname])
#     line1 = np.array(data['year'])* slope + intercept
#     detrended1 = list(zscore(detrend(data[colname], type='linear')))
#
#     data = data_row[(data_row['year'] >= 1900) & (data_row['year'] <= 2010)]
#     slope, intercept, r_value, p_value, std_err = linregress(data['year'], data[colname])
#     line2 = np.array(data['year'])* slope + intercept
#     detrended2 = list(zscore(detrend(data[colname], type='linear')))
#     f1d = np.array(zscore(detrended1+detrended2))
#     std_f1d = np.std(zscore(detrended1 + detrended2))
#     axs[i_col].plot(np.arange(1750,2010+1),f1d)
#     axs[i_col].set_title(colname)
#     axs[i_col].set_ylim(-2.5,5)
#     axs[i_col].axhline(2*std_f1d,linewidth=0.5, linestyle='--',color='black')
#     axs[i_col].axhline(1.75*std_f1d, linewidth=0.5, linestyle='--', color='black')
#
#     std_f1d = np.std(zscore(detrended1 + detrended2))
#
#     # f1d = np.array(zscore(detrended1+detrended2))[1836-1750:1900-1750+1]
#     # year = np.arange(1750,2010+1)[1836-1750:1900-1750+1]
#
#     f1d = np.array(zscore(detrended1 + detrended2))[1900 - 1750:]
#     year = np.arange(1750, 2010 + 1)[1900 - 1750:]
#
#     # f1d = np.array(zscore(detrended1+detrended2))[:1899-1750+1]
#     # year = np.arange(1750,2010+1)[:1899-1750+1]
#     year_high = year[f1d > (2 * std_f1d)]
#
#     year_high_all.append(list(year[np.argsort(f1d)[-10:]]))
#     year_low_all.append(list(year[np.argsort(f1d)[:10]]))
# print(list(year_high_all))
# print(list(year_low_all))
# exit()
# plt.show()
#
# loc_1900 = 1900-1750
# std_f1d = np.std(zscore(detrended1+detrended2))
#
# # f1d = np.array(zscore(detrended1+detrended2))[1836-1750:1900-1750+1]
# # year = np.arange(1750,2010+1)[1836-1750:1900-1750+1]
#
# f1d = np.array(zscore(detrended1+detrended2))[1900-1750:]
# year = np.arange(1750,2010+1)[1900-1750:]
#
# # f1d = np.array(zscore(detrended1+detrended2))[:1899-1750+1]
# # year = np.arange(1750,2010+1)[:1899-1750+1]
# year_high = year[f1d>(2*std_f1d)]
#
# year_high = year[np.argsort(f1d)[-10:]]
# year_low = year[np.argsort(f1d)[:10]]
# print(list(year_high))
# print(list(year_low))
# exit()


# year_b = 2007
# year_e = 2007
# # https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/20070701-ESACCI-L3S_FIRE-BA-MODIS-AREA_2-fv5.1.tar.gz
# str_begin = r'start https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/'
# str_begin = r'start https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/'
#
# time_all = pd.date_range('%i-01'%year_b, '%i-12'%year_e, freq='1M').strftime('%Y%m')
# str_all = []
# i = 0
# for i_time in time_all:
#     month = int(i_time[4:6])
#     for i_v in range(1,6+1):
#         str_end = '01-ESACCI-L3S_FIRE-BA-MODIS-AREA_%i-fv5.1.tar.gz'%i_v
#         str_now = i_time[:4]+'/'+i_time+str_end
#         # print(r'C:\FFOutput/'+str_now)
#         if i_time[:4] == '2007':
#             continue
#         if not os.path.exists(r'C:\FFOutput/'+i_time+str_end):
#             str_all.append(str_begin+str_now)
#             print(str_now)
#             i += 1
#         if i>100:
#             break
# print(i)
# np.savetxt('ESA_CCIFire51.bat', str_all, fmt='%s')
# exit()
# year_b = 2007
# year_e = 2008
# # https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/20070701-ESACCI-L3S_FIRE-BA-MODIS-AREA_2-fv5.1.tar.gz
# str_begin = r'start https://data.cci.ceda.ac.uk/thredds/fileServer/esacci/fire/data/burned_area/MODIS/pixel/v5.1/compressed/2007/old-redacted/'
#
# time_all = pd.date_range('%i-01'%year_b, '%i-01'%year_e, freq='1M').strftime('%Y%m')
# str_all = []
# i = 0
# for i_time in time_all:
#     month = int(i_time[4:6])
#     for i_v in range(1,6+1):
#         str_end = '01-ESACCI-L3S_FIRE-BA-MODIS-AREA_%i-fv5.1.tar.gz'%i_v
#         str_now = '/'+i_time+str_end
#         # print(r'C:\FFOutput/'+str_now)
#         if not os.path.exists(r'C:\FFOutput/'+i_time+str_end):
#             str_all.append(str_begin+str_now)
#             print(str_now)
#             i += 1
#         if i>100:
#             break
# print(i)
# np.savetxt('ESA_CCIFire51.bat', str_all, fmt='%s')
# exit()

"""
FIG2
FIG3
"""
year_high_all = [[1974, 1917, 1933, 1947, 1901, 1911, 1913, 1926, 1944, 1987], [1969, 1981, 1914, 1918, 1994, 1925, 1944, 1996, 2008, 1997], [1989, 1904, 1976, 1912, 1965, 1911, 1922, 1987, 2003, 1943]]
year_low_all = [[1900, 1902, 1904, 1906, 1909, 1912, 1914, 1916, 1918, 1919], [2010, 1908, 1950, 1960, 1964, 1975, 1984, 1987, 1989, 1990], [1900, 1901, 1907, 1910, 1914, 1935, 1936, 1938, 1941, 1950]]

types = '20C' # EKF / 20C
loc_1900 = 'after' # before/ after
highTOlow = True


colname_all = ['NEChina','Mongo', 'BaikalRURom']

for i_col in range(3):
    addtianl_fileName = r'_1900-2010_%s'%colname_all[i_col]

    if types == 'EKF':
        # date_combine_high_freq_year = [1846, 1874, 1899, 1866, 1898, 1883, 1863, 1843, 1885, 1858,1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
        # date_low_freq_year = [1886, 1840, 1897, 1860, 1892, 1891, 1861, 1888, 1882, 1848,1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
        # date_base = np.arange(1750, 2003+1)

        if loc_1900 == 'before':
            date_combine_high_freq_year = [1854, 1846, 1874, 1899, 1866, 1751, 1766, 1898, 1883, 1863, 1843, 1815, 1885, 1858, 1758]
            date_low_freq_year = [1886, 1840, 1897, 1860, 1777, 1820, 1757, 1892, 1891, 1861, 1888, 1882, 1848, 1781, 1810]
        if loc_1900 == 'after':
            date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]
            date_low_freq_year = [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]

        # date_base = np.arange(1750, 2003 + 1)
        date_base = np.arange(1750, 2003 + 1)

    elif types == '20C':

        # date_combine_high_freq_year = [1846, 1874, 1899, 1866, 1898, 1883, 1863, 1843, 1885, 1858,1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
        # date_low_freq_year = [1886, 1840, 1897, 1860, 1892, 1891, 1861, 1888, 1882, 1848,1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]

        # date_combine_high_freq_year = [1911, 1922, 1943, 1987, 2003]  # high1
        # date_low_freq_year = [1960, 1908, 1955, 1984, 1910]
        if loc_1900 == 'before':
            # date_combine_high_freq_year =co
            date_low_freq_year = [1886, 1840, 1897, 1860, 1892, 1891, 1861, 1888, 1882, 1848]

        if loc_1900 == 'after':
            date_combine_high_freq_year = year_high_all[i_col]
            date_low_freq_year = year_low_all[i_col]


        # date_combine_high_freq_year_1 = [1846, 1874, 1899, 1866, 1898, 1883, 1863, 1843, 1885, 1858,1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
        # date_low_freq_year_1 = [1886, 1840, 1897, 1860, 1892, 1891, 1861, 1888, 1882, 1848,1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
        #
        # date_combine_high_freq_year_2 = [1911, 1922, 1943, 1987, 2003]  # high1
        # date_low_freq_year_2 = [1960,1908,1955,1984,1910]

        # date_combine_high_freq_year = set(date_combine_high_freq_year_1) - set(date_combine_high_freq_year_2)
        # date_low_freq_year = set(date_low_freq_year_1) - set(date_low_freq_year_2)
        #
        # date_combine_high_freq_year = list(date_combine_high_freq_year)
        # date_low_freq_year = list(date_low_freq_year)

        date_base = np.arange(1836, 2010+1)


    months = [[2,3,4],[5]]

    def get_wind(var, var_year, var_month):
        wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
        wind_inmonth = wind_inYear.sel(
                                    time=np.in1d(wind_inYear['time.month'], var_month))
        return wind_inmonth

    import cmaps
    # orig_cmap = cmaps.cmp_b2r

    # import numpy as np
    # from matplotlib.colors import ListedColormap, LinearSegmentedColormap
    # cmap = cmaps.WhiteBlue
    # viridis = cmap
    # orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

    savemat = []
    for i_month in months:

        def get_EKF400_byYear(year):
            """
            get EKF400 data in given year, month, and range.
            :param year:
            :return:
            """
            hgt500 = []
            t2m = []
            u850 = []
            v850 = []
            for i_year in year:
                a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_%i_v1.1.nc"%i_year)
                m_ranged = get_wind(a, i_year, i_month)
                hgt500.append(m_ranged['geopotential_height'].sel(pressure_level_gph=500).mean(dim='time').values)
                t2m.append(m_ranged['air_temperature'].sel(level_temp=2).mean(dim='time').values)
                u850.append(m_ranged['eastward_wind'].sel(pressure_level_wind=850).mean(dim='time').values)
                v850.append(m_ranged['northward_wind'].sel(pressure_level_wind=850).mean(dim='time').values)

                # hgt500.extend(m_ranged['geopotential_height'].sel(pressure_level_gph=500).values)
                # t2m.extend(m_ranged['air_temperature'].sel(level_temp=2).values)
                # u850.extend(m_ranged['eastward_wind'].sel(pressure_level_wind=850).values)
                # v850.extend(m_ranged['northward_wind'].sel(pressure_level_wind=850).values)

            return [np.array(hgt500), np.array(t2m), np.sqrt(np.array(u850)**2+np.array(v850)**2)]

        def get_20cc_byYear(year):
            """
            get EKF400 data in given year, month, and range.
            :param year:
            :return:
            """
            result = []
            var = ['hgt500', 'air.2m', 'uwnd850', 'vwnd850', 'qu', 'qv']
            shortName = ['hgt', 'air', 'uwnd', 'vwnd', 'qu', 'qv']
            shortName = dict(zip(var, shortName))
            for i_var in var:
                a = xr.open_dataset(r"D:\fire\20c/%s.mon.mean.nc"%i_var)
                m_ranged = get_wind(a, year, i_month)
                da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
                m_ranged = da.groupby("year_month").mean("time")
                if i_var == 'air':
                    m_ranged = m_ranged.sel(level=1000)
                    print(m_ranged[shortName[i_var]])
                result.append(m_ranged[shortName[i_var]].values)
            result_new = [result[0], result[1],
                          np.sqrt(np.array(result[2])**2+np.array(result[3])**2),
                          result[4], result[5]]

            return result_new

        if types == 'EKF':
            climate = get_EKF400_byYear(date_base)
            from scipy.signal import detrend

            period = np.arange(1750, 2003 + 1)
            mutation_year = 2000 - 1979

            data_row = climate[0]

            skip_0 = False
            data_row[np.isnan(data_row)] = 0
            import xarray as xr
            from scipy.stats import linregress

            # a = xr.open_dataset(r"E:\Guan_DJF.nc")
            # lon = a.lon.values
            # lat = a.lat.values

            n_lats = data_row.shape[1]
            n_lons = data_row.shape[2]

            save_name = ['%i_%i' % (period[0], period[-1]), '%i_%i' % (period[0], period[mutation_year]),
                         '%i_%i' % (period[mutation_year], period[-1])]
            # for iI, data in enumerate([data_row, data_row[:mutation_year], data_row[mutation_year:]]):
            for iI, data in enumerate([data_row[:]]):
                trend = np.zeros((n_lats, n_lons))
                p_values = np.ones((n_lats, n_lons))

                for i in range(n_lats):
                    for j in range(n_lons):
                        s_data = data[:, i, j]
                        ######
                        # if (density[:,i,j] == 0).any():
                        #     print('hi')
                        if skip_0:
                            s_data = s_data[s_data != 0]
                        if len(s_data) != 0:
                            slope, intercept, r_value, p_value, std_err = linregress(np.arange(len(s_data)), s_data)
                            trend[i, j] = slope
                            p_values[i, j] = p_value
                # #

                ################################# plot #########################################
                # trend = nc4.read_nc4(r'trend_YY')
                # plt.imshow(trend)
                # plt.show()
                # p_values = nc4.read_nc4(r'p_YY')
                import matplotlib.pyplot as plt
                import cartopy.crs as ccrs
                import cmaps

                fig = plt.figure(figsize=[5, 3.9])
                plt.subplots_adjust(top=0.97,
                                    bottom=0.1,
                                    left=0.005,
                                    right=0.995,
                                    hspace=0.14,
                                    wspace=0.055)
                ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

                import matplotlib.pyplot as plt
                import numpy as np
                import numpy.ma as ma
                import scipy.special as sp
                import cartopy.crs as ccrs

                # trend[np.isnan(trend)] = 0
                trend[trend == 0] = np.nan
                import matplotlib.colors as colors

                a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean_1970_v1.nc")
                lon = a.longitude.values
                lat = a.latitude.values

                trend[np.abs(trend) < 1.e-3] = 0.
                # cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)[:120], trend[:120, :]
                #                  , cmap='bwr', norm=colors.CenteredNorm(), extend='both',
                #                  transform=ccrs.PlateCarree())
                cb = ax.pcolormesh(lon, lat, trend
                                   , cmap='bwr', norm=colors.CenteredNorm(),
                                   transform=ccrs.PlateCarree())
                plt.colorbar(cb)
                ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                            colors='none', levels=[0, 0.1],
                            hatches=[1 * '.', 1 * '.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

                # ax.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
                # ax.set_global()
                ax.coastlines(linewidth=0.3, zorder=10)
                # ax.stock_img()
                ax.gridlines(ylocs=[66], linewidth=0.3, color='black')

                plt.show()
                plt.close()



            climate = detrend(climate, axis=1)

            # high = get_EKF400_byYear(date_combine_high_freq_year)
            # lows = get_EKF400_byYear(date_low_freq_year)
            high = climate[:, date_combine_high_freq_year-date_base[0]]
            lows = climate[:, date_low_freq_year-date_base[0]]
            a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean_1970_v1.nc")
            lon = a.longitude.values
            lat = a.latitude.values


        elif types == '20C':
            climate = get_20cc_byYear(date_base)
            from scipy.signal import detrend
            climate = detrend(climate, axis=1)

            # high = get_EKF400_byYear(date_combine_high_freq_year)
            # lows = get_EKF400_byYear(date_low_freq_year)
            high = climate[:, date_combine_high_freq_year-date_base[0]]
            lows = climate[:, date_low_freq_year-date_base[0]]

            a = xr.open_dataset(r"D:\fire\20c\hgt500.mon.mean.nc")
            lon = a.lon.values
            lat = a.lat.values

        from scipy.stats import ttest_ind
        p = []
        for i in range(len(high)):
            if highTOlow:
                _,pvalues = ttest_ind(high[i], lows[i], axis=0, equal_var=False)
            else:
                _, pvalues = ttest_ind(high[i], climate[i], axis=0, equal_var=False)


            p.append(pvalues)


        ######################!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#######################
        if not highTOlow:
            for i_high in np.arange(high.shape[1]):
                high[:,i_high] = high[:,i_high] - np.nanmean(climate, axis=1)

        high = np.nanmean(high, axis=1)
        lows = np.nanmean(lows, axis=1)

        if highTOlow:
            data = high-lows
        else:
            data = high

        savemat.append([data,p])

    import scipy.io as scio
    m234, m234P = savemat[0]
    m5, m5P = savemat[1]

    coords = pd.read_csv('Coordinates_%s.csv' % colname_all[i_col])
    lat_station = coords['lat'].values
    lon_station = coords['lon'].values

    if types == '20C':
        scio.savemat('figure2%s.mat'%addtianl_fileName, {
            'f2a_M234_Hgt500':m234[0],
            'f2a_M234_Hgt500_p':m234P[0],
            'f2a_M234_t2m':m234[1],
            'f2a_M234_t2m_p': m234P[1],

            'f2a_M5_Hgt500': m5[0],
            'f2a_M5_Hgt500_p': m5P[0],
            'f2a_M5_t2m': m5[1],
            'f2a_M5_t2m_p': m5P[1],


            'f2b_M234_wind850': m234[2],
            'f2b_M234_wind850_p': m234P[2],

            'f2b_M5_wind850': m5[2],
            'f2b_M5_wind850_p': m5P[2],


            'f2c_M234_IVT': np.sqrt(m234[3]**2+m234[4]**2),
            'f2c_M234_IVT_p': np.minimum(m234P[3], m234P[4]),
            'f2c_M234_qu': m234[3],
            'f2c_M234_qv': m234[4],

            'f2c_M5_IVT': np.sqrt(m5[3]**2+m5[4]**2),
            'f2c_M5_IVT_p': np.minimum(m5P[3], m5P[4]),
            'f2c_M5_qu': m5[3],
            'f2c_M5_qv': m5[4],

            'lon':lon,
            'lat':lat,

            'lon_station':lon_station,
            'lat_station':lat_station
            })

    elif types == 'EKF':
        scio.savemat('figure3%s.mat'%addtianl_fileName, {
            'f3a_M234_Hgt500': m234[0],
            'f3a_M234_Hgt500_p': m234P[0],
            'f3a_M234_t2m': m234[1],
            'f3a_M234_t2m_p': m234P[1],

            'f3a_M5_Hgt500': m5[0],
            'f3a_M5_Hgt500_p': m5P[0],
            'f3a_M5_t2m': m5[1],
            'f3a_M5_t2m_p': m5P[1],

            'f3b_M234_wind850': m234[2],
            'f3b_M234_wind850_p': m234P[2],

            'f3b_M5_wind850': m5[2],
            'f3b_M5_wind850_p': m5P[2],
            'lon': lon,
            'lat': lat

        })

exit()



from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
import matplotlib.pyplot as plt
data_row = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,3])
data = data_row[(data_row['year'] >= 1750) & (data_row['year'] <= 1899)]
from scipy.stats import linregress
slope, intercept, r_value, p_value, std_err = linregress(data['year'], data[colname])
line1 = np.array(data['year'])* slope + intercept
detrended1 = list(zscore(detrend(data[colname], type='linear')))

data = data_row[(data_row['year'] >= 1900) & (data_row['year'] <= 2010)]
slope, intercept, r_value, p_value, std_err = linregress(data['year'], data[colname])
line2 = np.array(data['year'])* slope + intercept
detrended2 = list(zscore(detrend(data[colname], type='linear')))

import scipy.io as scio
scio.savemat('figure1.mat', {'f1c':np.array(data_row[colname][(data_row['year'] >= 1750) & (data_row['year'] <= 2010)]),
                             'f1d':np.array(zscore(detrended1+detrended2)),
                             'year':range(1750, 2010+1),
                             'line1y':line1,
                             'line2y':line2,
                             'line1x': range(1750, 1899+1),
                             'line2x':range(1900, 2010+1),
                             },
             )
detrended = pd.Series(zscore(detrended1+detrended2), index=range(1750, 2010+1))
detrended.plot()
plt.show()