import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

def get_pre_data(freqType):
    # file_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/plot/'
    file_Path = r'D:\OneDrive\a_matlab\Blocking/'

    index_BI = scio.loadmat(file_Path+r'BI_%s.mat'%freqType)['BI']

    if freqType == 'high':
        years = [1987,2003]
    else:
        years = [1990,2013]

    varName = index_BI.dtype.names


    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s0101'%iyear; date_e = '%s0331'%iyear
            sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
            dates.extend(sdate)
        return dates
    dates_all = date_ranges(years)

    return index_BI, dates_all

####################################### plot TM in all year ###################################
# import numpy as np
# import matplotlib
# import matplotlib.pyplot as plt
#
# date_P = dates_all
# lon_P = np.arange(0,360)
#
# index_TMs = []
# for time in range(len(dates_all)):
#     index_TM = index_BI['TM'][0][time][0]
#     index_TMs.append(index_TM)
# index_TMs = np.array(index_TMs)
#
# fig, ax = plt.subplots()
# im = ax.imshow(index_TMs)
#
#
# ax.set_yticks(np.arange(len(date_P))[::31])
#
# ax.set_yticklabels(date_P[::31])
# for vline in [100, 110,125]:
#     ax.axvline(vline, color='lightyellow', linestyle='--', linewidth=1)
# ax.set_title('%s-frequency year (TM)'%(freqType))
# ax.set_xlabel('Longitude')
#
# fig.tight_layout()
# plt.savefig('all_year_%s_frequency.png'%freqType, dpi=600)
# plt.close()


####################################### plot TM in given time  ###################################

def get_TM_accum_in_given_time(time_given, freqType):
    index_BI, dates_all = get_pre_data(freqType)
    index_TM_accum = np.zeros(360)
    for time in range(len(dates_all)):
        if np.isin(time_given, int(dates_all[time][5])).any():
            index_TM = index_BI['TM'][0][time][0]
            index_TM_accum += index_TM
    return index_TM_accum


# for time_given in [[3,4,5],[4,5],[4,5,6], [3,4,5,6]]:
#     index_TM_accum_high = get_TM_accum_in_given_time(time_given, 'high')
#     index_TM_accum_low = get_TM_accum_in_given_time(time_given, 'low')
#     index_TM_accum =(index_TM_accum_high-index_TM_accum_low) /(index_TM_accum_high+index_TM_accum_low)
#     fig, ax = plt.subplots(1,1)
#     ax.plot(range(360), index_TM_accum)
#     for vline in [100, 110,125]:
#         ax.axvline(vline, color='k', linestyle='--', linewidth=1)
#     ax.axhline(0, color='k', linewidth=1)
#     Month = ''
#     for imonth in time_given:
#         Month = Month+str(imonth)+','
#     ax.set_title(' difference (TM_high - TM_low) in Month %s'%(Month[:-1]))
#     ax.set_xlabel('Longitude')
#     # plt.show()
#     plt.savefig('difference_M_%s.png'%(Month[:-1]), dpi=600)
#     plt.close()


# exit(0)
for freqType in ['high', 'low']:
    for time_given in [[3,4,5],[4,5],[4,5,6], [3,4,5,6]]:
        index_TM_accum = get_TM_accum_in_given_time(time_given, freqType)
        fig, ax = plt.subplots(1,1)
        ax.plot(range(360), index_TM_accum)
        for vline in [100, 110,125]:
            ax.axvline(vline, color='k', linestyle='--', linewidth=1)

        Month = ''
        for imonth in time_given:
            Month = Month+str(imonth)+','
        ax.set_title('%s-frequency year (TM in Month %s)'%(freqType, Month[:-1]))
        ax.set_xlabel('Longitude')
        plt.show()
        plt.savefig('%s_frequency_M_%s.png'%(freqType,Month[:-1]), dpi=600)
        plt.close()

####################################### plot GHGS in given time  ###################################
# colors = ['red', 'blue', 'black']
# legends = ['+5', '0', '-5']
# def get_TM_accum_in_given_time(time_given, freqType):
#     index_BI, dates_all = get_pre_data(freqType)
#     index_GHGS_accum = np.zeros([3,360])
#
#     for time in range(len(dates_all)):
#         if np.isin(time_given, int(dates_all[time][5])).any():
#             for i in range(3):
#                 index_GHGS = index_BI['GHGS'][0][time][:,i]
#                 if time == 0:
#                     index_GHGS_accum[i] = index_GHGS
#                 else:
#                     index_GHGS_accum[i] = index_GHGS + index_GHGS_accum[i]
#     return index_GHGS_accum/len(dates_all)
#
#
# for time_given in [[4,5]]:
#     index_GHGS_accum_high = get_TM_accum_in_given_time(time_given, 'high')
#     index_GHGS_accum_low = get_TM_accum_in_given_time(time_given, 'low')
#
#     print(np.mean(index_GHGS_accum_high[0,100:125]))
#     index_GHGS_accum = (index_GHGS_accum_high - index_GHGS_accum_low)
#     fig, ax = plt.subplots(1,1)
#
#     for i in range(3):
#         ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
#     plt.legend()
#     for vline in [100, 110,125]:
#         ax.axvline(vline, color='k', linestyle='--', linewidth=1)
#     ax.axhline(0, color='k', linewidth=1)
#     Month = ''
#     for imonth in time_given:
#         Month = Month+str(imonth)+','
#     ax.set_title('difference (GHGS in Month %s)'%(Month[:-1]))
#     ax.set_xlabel('Longitude')
#
#     plt.show()
#     plt.savefig('difference_GHGS_M_%s.png'%(Month[:-1]), dpi=600)
#     plt.close()
#
# exit(0)


# for freqType in ['high', 'low']:
#     for time_given in [[3,4,5,6]]:
#         index_GHGS_accum = get_TM_accum_in_given_time(time_given, freqType)
#         fig, ax = plt.subplots(1,1)
#
#         for i in range(3):
#             ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
#         plt.legend()
#         for vline in [100, 110,125]:
#             ax.axvline(vline, color='k', linestyle='--', linewidth=1)
#
#         Month = ''
#         for imonth in time_given:
#             Month = Month+str(imonth)+','
#         ax.set_title('%s-frequency year (GHGS in Month %s)'%(freqType, Month[:-1]))
#         ax.set_xlabel('Longitude')
#         # plt.show()
#         plt.savefig('%s_frequency_GHGS_M_%s.png'%(freqType,Month[:-1]), dpi=600)
#         plt.close()
#
#


################################### plot TEM in longitude ####################################
# colors = ['red', 'blue', 'black'][::-1]
# legends = ['35-45°N', '45-65°N', '65-85°N']
# a = scio.loadmat('tem_longitudehigh_m123.mat', mat_dtype=True)
# t5_add = a['t5add'][0]; t5 = a['t5'][0]; t5div = a['t5div'][0]
# print(t5div)
# index_t5s_high = [t5_add, t5, t5div]
#
# a = scio.loadmat('tem_longitudelow_m123.mat', mat_dtype=True)
# t5_add = a['t5add'][0]; t5 = a['t5'][0]; t5div = a['t5div'][0]
# index_t5s_low = [t5_add, t5, t5div]
#
# fig, ax = plt.subplots(1,1)
#
# for i in range(3):
#     ax.plot(range(360), index_t5s_high[i]-index_t5s_low[i], color=colors[i], label=legends[i])
# plt.legend()
# for vline in [100, 110,125]:
#     ax.axvline(vline, color='k', linestyle='--', linewidth=1)
#
# ax.axhline(0, color='k', linestyle='-', linewidth=1)
#
# ax.set_title('difference (extreme high - low) (TEM in Month 4,5)')
# ax.set_xlabel('Longitude')
# plt.show()
# plt.savefig('%s_frequency_GHGS_M_4_5.png'%(freqType), dpi=600)
# plt.close()


