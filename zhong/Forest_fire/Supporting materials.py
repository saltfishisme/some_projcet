import xarray as xr
import numpy as np

types = 'EKF' # EKF / 20C
if types == 'EKF':
    # date_combine_high_freq_year =[1854, 1846, 1874, 1899, 1866, 1751, 1766, 1898, 1883, 1863, 1843, 1815, 1885, 1858, 1758]
    # date_low_freq_year = [1886, 1840, 1897, 1860, 1777, 1820, 1757, 1892, 1891, 1861, 1888, 1882, 1848, 1781, 1810]
    # date_base = np.arange(1750,1899+1)

    date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]
    date_low_freq_year = [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
    date_base = np.arange(1900, 2003 + 1)

elif types == '20C':
    date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
    date_low_freq_year = [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
    #
    # date_combine_high_freq_year = [1846, 1874, 1899, 1866, 1898, 1883, 1863, 1843, 1885, 1858]
    # date_low_freq_year = [1886, 1840, 1897, 1860, 1892, 1891, 1861, 1888, 1882, 1848]

    date_base = np.arange(1836, 2010+1)

ranges = [97,126,45,54]
months = [[2,3,4],[5]]

def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth

import cmaps
orig_cmap = cmaps.cmp_b2r

# import numpy as np
# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# cmap = cmaps.WhiteBlue
# viridis = cmap
# orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

savemat = []
for i_month in months:

    def get_EKF400_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        u200 = []

        for i_year in year:
            a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_%i_v1.1.nc"%i_year)
            m_ranged = get_wind(a, i_year, i_month)

            u200.append(m_ranged['eastward_wind'].sel(pressure_level_wind=200).mean(dim='time').values)

            # hgt500.extend(m_ranged['geopotential_height'].sel(pressure_level_gph=500).values)
            # t2m.extend(m_ranged['air_temperature'].sel(level_temp=2).values)
            # u850.extend(m_ranged['eastward_wind'].sel(pressure_level_wind=850).values)
            # v850.extend(m_ranged['northward_wind'].sel(pressure_level_wind=850).values)

        return np.array(u200)

    def get_20cc_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        result = []
        var = ['uwnd200']
        shortName = [ 'uwnd']
        shortName = dict(zip(var, shortName))
        for i_var in var:
            a = xr.open_dataset(r"D:\fire\20c/%s.mon.mean.nc"%i_var)
            m_ranged = get_wind(a, year, i_month)
            da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
            m_ranged = da.groupby("year_month").mean("time")
            if i_var == 'air':
                m_ranged = m_ranged.sel(level=1000)
                print(m_ranged[shortName[i_var]])
            result.append(m_ranged[shortName[i_var]].values)

        return result[0]

    def get_PHYDA_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        a = xr.open_dataset(r"D:\fire\PHYDA\da_hydro_DecFeb_r.1-2000_d.05-Jan-2018.nc")
        m_ranged = a['pdsi_mn'].values[np.array(year)-1]
        return m_ranged, a.lon.values, a.lat.values

    def get_PDSI_byYear(year):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """

        a = xr.open_dataset(r"D:\fire\scPDSI\scPDSI.cru_ts4.05early1.1901.2020.cal_1901_20.bams.2021.GLOBAL.1901.2020.nc")

        m_ranged = get_wind(a, year, i_month)
        print(m_ranged)
        da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        m_ranged = da.groupby("year_month").mean("time")

        return m_ranged['scpdsi'].values,a.longitude.values, a.latitude.values

    if types == 'EKF':

        high = get_EKF400_byYear(date_combine_high_freq_year)
        lows = get_EKF400_byYear(date_low_freq_year)
        a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean_1970_v1.nc")
        lon = a.longitude.values
        lat = a.latitude.values

        # pdsi_high, lon_pdsi, lat_pdsi = get_PHYDA_byYear(date_combine_high_freq_year)
        # pdsi_low,_,_ = get_PHYDA_byYear(date_low_freq_year)

    elif types == '20C':
        high = get_20cc_byYear(date_combine_high_freq_year)
        lows = get_20cc_byYear(date_low_freq_year)

        a = xr.open_dataset(r"D:\fire\20c\hgt500.mon.mean.nc")
        lon = a.lon.values
        lat = a.lat.values

        pdsi_high, lon_pdsi, lat_pdsi = get_PDSI_byYear(date_combine_high_freq_year)
        pdsi_low, _, _ = get_PDSI_byYear(date_low_freq_year)
    high_all = [high, high]
    lows_all = [lows, lows]

    # high_all = [high, pdsi_high]
    # lows_all = [lows, pdsi_low]

    from scipy.stats import ttest_ind
    p = []
    data = []
    for i in range(len(high_all)):
        _,pvalues = ttest_ind(high_all[i], lows_all[i], axis=0, equal_var=False)
        p.append(pvalues)
        data.append(np.nanmean(high_all[i],axis=0) - np.nanmean(lows_all[i],axis=0))


    savemat.append([data,p])


import scipy.io as scio
m234, m234P = savemat[0]
m5, m5P = savemat[1]

if types == '20C':
    scio.savemat('figureS1_1836-1899.mat', {
        's1a_M234_Hgt500':m234[0],
        's1a_M234_Hgt500_p':m234P[0],

        's1b_M5_Hgt500': m5[0],
        's1b_M5_Hgt500_p': m5P[0],

        'lon': lon,
        'lat': lat,

        # 's1c_M234_pdsi': m234[1],
        # 's1c_M234_pdsi_p': m234P[1],
        #
        # 's1d_M5_pdsi': m5[1],
        # 's1d_M5_pdsi_p': m5P[1],
        #
        # 'lon_pdsi': lon_pdsi,
        # 'lat_pdsi': lat_pdsi,
        })

elif types == 'EKF':
    scio.savemat('figureS2_1900-2003.mat', {
        's2a_M234_Hgt500':m234[0],
        's2a_M234_Hgt500_p':m234P[0],

        's2b_M5_Hgt500': m5[0],
        's2b_M5_Hgt500_p': m5P[0],

        'lon': lon,
        'lat': lat,

        # 's2c_M_pdsi': m234[1],
        # 's2c_M_pdsi_p': m234P[1],
        #
        # 'lon_pdsi': lon_pdsi,
        # 'lat_pdsi': lat_pdsi,
        }
)



exit()