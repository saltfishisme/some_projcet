import xarray as xr
import numpy as np
a = xr.open_dataset(r"D:\fire\PHYDA\da_hydro_DecFeb_r.1-2000_d.05-Jan-2018.nc")
print(a['pdsi_mn'])
exit()

types = 'EKF' # EKF / 20C

date_base_low = np.arange(1750,1899+1)

date_base_high = np.arange(1900, 2003+1)
date_base = np.arange(1836,2010+1)

ranges = [97,126,45,54]
months = [[2,3,4],[5]]

def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth

def get_tem_grad(data):
    m_ranged_north = np.roll(data, 1, axis=0)
    m_ranged_south = np.roll(data, -1, axis=0)
    tem_grad = (m_ranged_north - m_ranged_south) / 2
    return tem_grad

def get_regional_tem_grad(data):
    m_ranged_north = np.nanmean(data[0,:])
    m_ranged_south = np.nanmean(data[-1,:])
    tem_grad = (m_ranged_south - m_ranged_north) / 7.45935
    return tem_grad

import cmaps
orig_cmap = cmaps.cmp_b2r

# import numpy as np
# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# cmap = cmaps.WhiteBlue
# viridis = cmap
# orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

savemat = []
for i_month in months:

    def get_EKF400_byYear(year, types='tg'):
        """
        get EKF400 data in given year, month, and range.
        :param year:
        :return:
        """
        t2m = []

        for i_year in year:
            a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_%i_v1.1.nc"%i_year)
            m_ranged = get_wind(a, i_year, i_month)
            m_ranged =  m_ranged['air_temperature'].sel(level_temp=2).mean(dim='time')
            # t2m gradient
            t2m.append(get_tem_grad(m_ranged.values))
        return np.array(t2m)


    high = get_EKF400_byYear(date_base_high)
    lows = get_EKF400_byYear(date_base_low)


    a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_1970_v1.1.nc")
    lon = a.longitude.values
    lat = a.latitude.values



    from scipy.stats import ttest_ind

    _,p = ttest_ind(high, lows, axis=0, equal_var=False)
    # import scipy.io as scio
    # scio.savemat('test.mat', {'high':high, 'low':lows})
    # exit()
    high = np.nanmean(high, axis=0)
    print(high.shape)
    lows = np.nanmean(lows, axis=0)
    savemat.append([lows-high,p])


import scipy.io as scio
m234, m234P = savemat[0]
m5, m5P = savemat[1]

scio.savemat('figureS3.mat', {
    's3a_M234':m234,
    's3a_M234_p':m234P,

    's3b_M5': m5,
    's3b_M5_p': m5P,

    'lon':lon,
    'lat':lat,

    }
)



exit()