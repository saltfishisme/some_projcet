import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import nc4
import pandas as pd
import numpy as np



def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth

def get_EKF400_byYear(year, i_month):
    """
    get EKF400 data in given year, month, and range.
    :param year:
    :return:
    """
    hgt500 = []

    for i_year in year:
        a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_%i_v1.1.nc" % i_year)
        m_ranged = get_wind(a, i_year, i_month)
        hgt500.extend(m_ranged['geopotential_height'].sel(pressure_level_gph=500).values)
    return np.array(hgt500)

def create_nc(data, vari, path_save="/home/linhaozhong/work/Data/ERA5/pressure_level/plot/"):
    import xarray as xr
    import numpy as np
    import pandas as pd

    ################################# create date based on years  #################################


    times =  pd.date_range('%i-01-01'%date_year_s, '%i-12-31'%date_year_e, freq='1M')
    times = times[np.in1d(times.month,months)]
    print(len(times))


    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            vari: get_dataarray(data),

        }
    )
    print(ds)
    ds.to_netcdf(path_save + '%s_EKFV1.1.nc' % vari)

months = [2,3,4,5]

date_year_s = 1750
date_year_e = 2003

a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_2000_v1.1.nc")
lat = a.lat.values
lon = a.lon.values
hgt = get_EKF400_byYear(np.arange(1750,2003+1,1), [2,3,4,5])
print(hgt.shape)
create_nc(hgt, 'z500', 'D:/')
exit()
'''

'''

def nc_anomaly_monthly():
    da = xr.open_dataset(r"D:\z500_EKFV1.1.nc")
    print(da)
    da = da.assign_coords(month_day=da.time.dt.strftime("%m"))
    result = da.groupby("month_day") - da.groupby("month_day").mean("time")
    print(result)
    return result

def nc_anomaly_daily():

    da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_1836_2015_20c.nc')
    da = da.sel(time=np.in1d(da['time.year'], np.arange(1836,2010+1,1)))
    da = da.assign_coords(month_day=da.time.dt.strftime("%m-%d"))
    result = da.groupby("month_day") - da.groupby("month_day").mean("time")
    return result

# data_Global_sta = nc_anomaly_monthly()
#
# data_Global_sta_234 = data_Global_sta['z500'].sel(time=np.in1d(data_Global_sta['time.month'], [2,3,4])).values
# data_Global_sta_5 = data_Global_sta['z500'].sel(time=np.in1d(data_Global_sta['time.month'], [5])).values
#
# target = scipy.io.loadmat('figure3.mat')
# data_target234 = target['f3a_M234_Hgt500']
# data_target5 = target['f3a_M5_Hgt500']


# data_Global_sta = nc_anomaly_daily()
# data_Global_sta.to_netcdf('hgt500_20c_anomaly_1836_2010.nc')
data_Global_sta = xr.open_dataset('hgt500_20c_anomaly_1836_2010.nc')
print(data_Global_sta)
data_Global_sta_234 = data_Global_sta['hgt500_1836_2015'].sel(time=np.in1d(data_Global_sta['time.month'], [2,3,4])).values
data_Global_sta_5 = data_Global_sta['hgt500_1836_2015'].sel(time=np.in1d(data_Global_sta['time.month'], [5])).values

target = scipy.io.loadmat('figure2.mat')
data_target234 = target['f2a_M234_Hgt500']
data_target5 = target['f2a_M5_Hgt500']


lon = target['lon'][0]
lat = target['lat'][0]


""" additianl """
data = {}

ranges = [30,170,35,80]

loc_north = max([np.argmin(abs(lat - ranges[3])), np.argmin(abs(lat - ranges[2]))])
loc_south = min([np.argmin(abs(lat - ranges[3])), np.argmin(abs(lat - ranges[2]))])
loc_west = min([np.argmin(abs(lon - ranges[0])), np.argmin(abs(lon - ranges[1]))])
loc_east = max([np.argmin(abs(lon - ranges[0])), np.argmin(abs(lon - ranges[1]))])

data['data_234'] = np.transpose(data_Global_sta_234[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
data['target_234'] = data_target234[loc_south:loc_north+1, loc_west:loc_east+1]

lon_234, lat_234 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
data['lat_234'] = lat_234
data['lon_234'] = lon_234

data['data_5'] = np.transpose(data_Global_sta_5[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
data['target_5'] = data_target5[loc_south:loc_north+1, loc_west:loc_east+1]

lon_5, lat_5 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
data['lat_5'] = lat_5
data['lon_5'] = lon_5
scipy.io.savemat('figureA_parCor_20c.mat', data)
exit()


"""
pdf EKF
"""

date_base = np.arange(1750, 2003 + 1)

cor = scipy.io.loadmat('pattern_Cor.mat')
month_name = ['234', '5']
result = {}
result_rawdata = {}

for method01 in ['1']:
    cor234 = cor['yearly_cor234_method%s'%method01]
    cor5 = cor['yearly_cor5_method%s'%method01]

    date = pd.date_range('1750-01-01', '2003-12-31', freq='1M')

    cor234 = pd.DataFrame(cor234, index=date[np.in1d(date.month, [2,3,4])])
    cor5 = pd.DataFrame(cor5, index=date[np.in1d(date.month, [5])])

    merge_daily = []
    merge_yearly = []
    for i_year in date_base:
        list_now = list(cor234[np.in1d(cor234.index.year, i_year)].values)+list(cor5[np.in1d(cor5.index.year, i_year)].values)
        merge_daily.extend(list_now)

        mean_234 = np.mean(cor234[np.in1d(cor234.index.year, i_year)].values)
        mean_5 = np.mean(cor5[np.in1d(cor5.index.year, i_year)].values)
        merge_yearly.append((mean_234+mean_5)/2)

    merge_daily = pd.DataFrame(merge_daily, index=date[np.in1d(date.month, [2,3,4,5])])
    result_rawdata['monthly_m2345'] = merge_daily.values

    # result_rawdata['yearly_m2345'] = merge_daily.resample('1Y').mean().values
    result_rawdata['yearly_m2345'] = merge_yearly
    result_rawdata['year'] = date_base
    for i_month, data_row in enumerate([cor234, cor5]):

        data = data_row.resample('1Y').mean()

        result_rawdata['yearly_m%s' % month_name[i_month]] =data.values

        from scipy.stats import zscore
        from scipy.stats import linregress
        x = data.index.year.values
        y = zscore(np.squeeze(data[0].values))

        from scipy.signal import detrend

        from scipy.stats import pearsonr
        data_row_fireindex = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])

        fireindex = data_row_fireindex[(data_row_fireindex['year'] >= 1750) & (data_row_fireindex['year'] <= 2003)]

        detrended2 = list(zscore(fireindex['nechinamogoliaSiberia']))
        result_rawdata['fireIndex_zscore' ] = detrended2
        r,p = pearsonr(y, detrended2)
        # print(pearsonr(np.squeeze(merge_daily.resample('1Y').mean().values)[:150], detrended2[:150]))
        print(pearsonr(np.squeeze(merge_yearly)[:150], detrended2[:150]))
        #
        # print('1900')
        # print(pearsonr(np.squeeze(merge_daily.resample('1Y').mean().values)[150:], detrended2[150:]))
        print(pearsonr(np.squeeze(merge_yearly)[150:], detrended2[150:]))
        result_rawdata['monthly_m%s' % month_name[i_month]] = data_row.values
        # exit()

        # slope, intercept, r_value, p_value, std_err = linregress(x, merge_yearly)
        # print(slope, p_value)
        # slope, intercept, r_value, p_value, std_err = linregress(x[:150], merge_yearly[:150])
        # print(slope, p_value)
        # slope, intercept, r_value, p_value, std_err = linregress(x[150:], merge_yearly[150:])
        # print(slope, p_value)

        """pdf for 1900"""

        X_plot = np.linspace(-3.5,3.5,1000)
        # def plot_pdf(X,bandwidth):
        #     from sklearn.neighbors import KernelDensity
        #     X_plots = X_plot[:, np.newaxis]
        #     kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(X)
        #     log_dens = kde.score_samples(X_plots)
        #     return np.exp(log_dens)
        def plot_pdf(X, bandwidth='scott'):
            from scipy import stats
            kde = stats.gaussian_kde(np.squeeze(X), bw_method=bandwidth)
            f = kde.covariance_factor()
            bw = f * X.std()
            log_dens = kde.evaluate(X_plot)
            return log_dens
        import matplotlib.pyplot as plt

        # result_rawdata['monthly_m%s' % month_name[i_month]] = data_row.values

        # # data1 = data_row[np.in1d(data_row.index.year, np.arange(1750,1899+1))].values
        # # print(data_row[np.in1d(data_row.index.year, np.arange(1750,1899+1))])
        # # data2 = data_row[np.in1d(data_row.index.year, np.arange(1900,2003+1))].values
        #
        # data1 = np.array(merge_daily[:150*4])
        # data2 = np.array(merge_daily[150*4:])
        # a = plot_pdf(data1)
        # b = plot_pdf(data2)
        #
        # # result['highFire_method%s_m%s'%(method01, month_name[i_month])] = a
        # # result['lowFire_method%s_m%s' % (method01, month_name[i_month])] = b
        #
        # fig, ax = plt.subplots(figsize=(5,5))
        #
        # ax.plot(
        #     X_plot,
        #     a,
        #     linestyle="-",
        #     label='1750-1899',color='red',
        # )
        # ax.plot(
        #     X_plot,
        #     b,
        #     linestyle="-",
        #     label='1900-2003',color='blue',
        # )
        #
        # ax.legend(loc="upper left")
        # ax.set_xlim(-2, 2)
        # # ax.set_ylim(0, 0.5)
        # plt.show()
        # plt.close()


# scipy.io.savemat('Figure_patternCor_EKF.mat', result)

scipy.io.savemat('patternCor_rawData_EKF.mat', result_rawdata)


''' '''
date_base = np.arange(1836, 2010 + 1)

cor = scipy.io.loadmat('pattern_Cor.mat')
month_name = ['234', '5']
result = {}
result_rawdata = {}

for method01 in ['1']:
    cor234 = cor['yearly_cor234_method%s'%method01]
    cor5 = cor['yearly_cor5_method%s'%method01]

    date = pd.date_range('1836-01-01', '2010-12-31', freq='1D')
    print(date[np.in1d(date.month, [2,3,4])])
    cor234 = pd.DataFrame(cor234, index=date[np.in1d(date.month, [2,3,4])])
    cor5 = pd.DataFrame(cor5, index=date[np.in1d(date.month, [5])])

    merge_daily = []
    merge_yearly = []
    for i_year in date_base:
        list_now = list(cor234[np.in1d(cor234.index.year, i_year)].values)+list(cor5[np.in1d(cor5.index.year, i_year)].values)
        merge_daily.extend(list_now)

        mean_234 = np.mean(cor234[np.in1d(cor234.index.year, i_year)].values)
        mean_5 = np.mean(cor5[np.in1d(cor5.index.year, i_year)].values)
        merge_yearly.append((mean_234+mean_5)/2)

    merge_daily = pd.DataFrame(merge_daily, index=date[np.in1d(date.month, [2,3,4,5])])
    result_rawdata['daily_m2345'] = merge_daily.values

    # result_rawdata['yearly_m2345'] = merge_daily.resample('1Y').mean().values
    result_rawdata['yearly_m2345'] = merge_yearly
    result_rawdata['year'] = date_base
    for i_month, data_row in enumerate([cor234, cor5]):

        data = data_row.resample('1Y').mean()

        result_rawdata['yearly_m%s' % month_name[i_month]] =data.values

        from scipy.stats import zscore
        from scipy.stats import linregress
        x = data.index.year.values
        y = zscore(np.squeeze(data[0].values))

        from scipy.signal import detrend

        from scipy.stats import pearsonr
        data_row_fireindex = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])

        fireindex = data_row_fireindex[(data_row_fireindex['year'] >= 1836) & (data_row_fireindex['year'] <= 2010)]

        detrended2 = list(zscore(fireindex['nechinamogoliaSiberia']))
        result_rawdata['fireIndex_zscore' ] = detrended2
        r,p = pearsonr(y, detrended2)

        print(pearsonr(np.squeeze(merge_yearly)[:64], detrended2[:64]))

        print('1900')

        print(pearsonr(np.squeeze(merge_yearly)[64:], detrended2[64:]))



        # slope, intercept, r_value, p_value, std_err = linregress(x, merge_yearly)
        # print(slope, p_value)
        # slope, intercept, r_value, p_value, std_err = linregress(x[:150], merge_yearly[:150])
        # print(slope, p_value)
        # slope, intercept, r_value, p_value, std_err = linregress(x[150:], merge_yearly[150:])
        # print(slope, p_value)

        """pdf for 1900"""

        X_plot = np.linspace(-3.5,3.5,1000)
        # def plot_pdf(X,bandwidth):
        #     from sklearn.neighbors import KernelDensity
        #     X_plots = X_plot[:, np.newaxis]
        #     kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(X)
        #     log_dens = kde.score_samples(X_plots)
        #     return np.exp(log_dens)
        def plot_pdf(X, bandwidth='scott'):
            from scipy import stats
            kde = stats.gaussian_kde(np.squeeze(X), bw_method=bandwidth)
            f = kde.covariance_factor()
            bw = f * X.std()
            log_dens = kde.evaluate(X_plot)
            return log_dens
        import matplotlib.pyplot as plt

        result_rawdata['daily_m%s' % month_name[i_month]] = data_row.values

        # data1 = data_row[np.in1d(data_row.index.year, np.arange(1750,1899+1))].values
        # print(data_row[np.in1d(data_row.index.year, np.arange(1750,1899+1))])
        # data2 = data_row[np.in1d(data_row.index.year, np.arange(1900,2003+1))].values

        # data1 = np.array(merge_daily[:64*4])
        # data2 = np.array(merge_daily[64*4:])
        # a = plot_pdf(data1)
        # b = plot_pdf(data2)
        #
        # # result['highFire_method%s_m%s'%(method01, month_name[i_month])] = a
        # # result['lowFire_method%s_m%s' % (method01, month_name[i_month])] = b
        #
        # fig, ax = plt.subplots(figsize=(5,5))
        #
        # ax.plot(
        #     X_plot,
        #     a,
        #     linestyle="-",
        #     label='1750-1899',color='red',
        # )
        # ax.plot(
        #     X_plot,
        #     b,
        #     linestyle="-",
        #     label='1900-2003',color='blue',
        # )
        #
        # ax.legend(loc="upper left")
        # ax.set_xlim(-2, 2)
        # # ax.set_ylim(0, 0.5)
        # plt.show()
        # plt.close()

scipy.io.savemat('patternCor_rawData_20c.mat', result_rawdata)


import scipy.io
#
a = scipy.io.loadmat('patternCor_rawData_20c.mat')
b = scipy.io.loadmat('patternCor_rawData_20c_monthly.mat')

a['yearly_m234'] = b['yearly_m234'][:-5]
a['yearly_m5'] = b['yearly_m5'][:-5]
a['yearly_m2345'] = b['yearly_m2345'][0][:-5]

scipy.io.savemat('patternCor_rawData_20c_remonth.mat', a)


