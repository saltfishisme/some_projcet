import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

#%% calculate TM, GHGS, GHGN
def nc_anomaly():
    da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc').sel(lat=60)
    da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
    result = da.groupby("year_month") - da.groupby("year_month").mean("time")
    return result

data_hgt_Global_sta = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc')
de_latitude_range = [30, 70]; de_reversal_range = 15

index_TM = np.zeros([data_hgt_Global_sta.time.values.shape[0],
                     de_latitude_range[1]-de_latitude_range[0]+1,
                     data_hgt_Global_sta.lon.values.shape[0]])
index_GHGS = np.zeros(index_TM.shape)
index_GHGN = np.zeros(index_TM.shape)

for s_i, s_latitude_central in  enumerate(np.arange(de_latitude_range[0], de_latitude_range[1]+1)):
    s_hgt_in_latitude_south = data_hgt_Global_sta.sel(lat=s_latitude_central-de_reversal_range)['hgt500'].values
    s_hgt_in_latitude_south_low = data_hgt_Global_sta.sel(lat=s_latitude_central-de_reversal_range-15)['hgt500'].values
    s_hgt_in_latitude_centre = data_hgt_Global_sta.sel(lat=s_latitude_central)['hgt500'].values
    s_hgt_in_latitude_north = data_hgt_Global_sta.sel(lat=s_latitude_central+de_reversal_range)['hgt500'].values

    s_GHGS = (s_hgt_in_latitude_centre - s_hgt_in_latitude_south)/de_reversal_range
    s_GHGS_low = (s_hgt_in_latitude_centre - s_hgt_in_latitude_south_low)/de_reversal_range
    s_GHGN = (s_hgt_in_latitude_north-s_hgt_in_latitude_centre)/de_reversal_range

    s_TM = np.zeros(s_GHGN.shape)
    s_TM[(s_GHGS>0) & (s_GHGN<-10) & (s_GHGS_low<-5)] = 1

    index_TM[:, s_i] = s_TM
    index_GHGS[:, s_i] = s_GHGS
    index_GHGN[:, s_i] = s_GHGN

times = data_hgt_Global_sta.time
import nc4
nc4.save_standard_nc(index_TM, 'index_TM', times, lat=np.arange(de_latitude_range[0], de_latitude_range[1]+1))
nc4.save_standard_nc(index_GHGS, 'index_GHGS', times, lat=np.arange(de_latitude_range[0], de_latitude_range[1]+1))
nc4.save_standard_nc(index_GHGN, 'index_GHGN', times, lat=np.arange(de_latitude_range[0], de_latitude_range[1]+1))

#%% test
data_index_TM = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\Forest_fire\index_TM.nc')
data_index_GHGS = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\Forest_fire\index_GHGS.nc')
data_index_GHGN = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\Forest_fire\index_GHGN.nc')

bu_use_month = [4,5]
bu_use_year = np.arange(1970, 2002) # high1
index_TM_in_used_month = data_index_TM.sel(time=(np.in1d(data_index_TM['time.month'], bu_use_month)) & (np.in1d(data_index_TM['time.year'], bu_use_year)))
print(index_TM_in_used_month.lat)
index_TM_in_used_month = index_TM_in_used_month.sum(dim='time')['index_TM'].values
fig, ax = plt.subplots(1,1)
ax.imshow(index_TM_in_used_month)
ax = plt.gca()
ax.set_yticks(np.arange(0,41,5))
ax.set_yticklabels(np.arange(30,71, 5))
plt.show()






