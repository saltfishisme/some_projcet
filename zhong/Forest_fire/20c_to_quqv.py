import matplotlib.pyplot as plt
import xarray as xr
from metpy import calc
from metpy.units import units
import numpy as np




# import nc4
# from nc4 import save_nc4, read_nc4
# import time
# def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
#     """
#     :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
#     :param var_name: ['t2m', 'ivt', 'uivt']
#     :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
#     :param lon: list, 720
#     :param lat: list, 360
#     :param path_save: r'D://'
#     :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
#     :return:
#     """
#     import xarray as xr
#     import numpy as np
#
#     if lon is None:
#         lon = np.arange(0, 360)
#     if lat is None:
#         lat = np.arange(90, -91, -1)
#
#     # def get_dataarray(data):
#     #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
#     #     return foo
#
#     ds = xr.Dataset()
#     ds.coords["lat"] = lat
#     ds.coords["lon"] = lon
#     ds.coords["time"] = times
#
#     for iI_vari, ivari in enumerate(var_name):
#
#         ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))
#
#     print(ds)
#     ds.to_netcdf(path_save + '%s.nc' % file_name)
#
#
# qu = xr.open_dataset(r'D:/qu.mon.mean.nc')
# qv = xr.open_dataset(r'D:/qv.mon.mean.nc')
#
# save_standard_nc([-qu['qu'].values], ['qu'], qu.time.values, lon=qu.lon.values, lat=qu.lat.values,
#                  path_save=r'D:/qu.nc',
#                  file_name='qu')
# save_standard_nc([-qv['qv'].values], ['qv'], qv.time.values, lon=qv.lon.values, lat=qv.lat.values,
#                  path_save=r'D:/qv.nc',
#                  file_name='qv')


# additionStr = ''
# uwnd = xr.open_dataset(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/uwnd.mon.mean%s.nc'%additionStr)
# hgt = xr.open_dataset(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/hgt.mon.mean%s.nc'%additionStr)
#
# uwnd.sel(level=200).to_netcdf(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/uwnd200.mon.mean.nc')
# hgt.sel(level=500).to_netcdf(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/hgt500.mon.mean.nc')
# exit()
#
# def to_uwnd_vwnd(shum, uwnd, vwnd, levels):
#     from metpy.calc import get_layer
#     import numpy as np
#     def precipitable_water(pressure, dewpt, bottom=None, top=None):
#         # print(pressure)
#         # Sort pressure and dewpoint to be in decreasing pressure order (increasing height)
#         sort_inds = np.argsort(pressure)[::-1]
#         pressure = pressure[sort_inds]
#         dewpt = dewpt[sort_inds]
#
#         if top is None:
#             top = np.nanmin(pressure.magnitude) * pressure.units
#
#         if bottom is None:
#             bottom = np.nanmax(pressure.magnitude) * pressure.units
#         # print(pressure, dewpt)
#         print(bottom)
#         pres_layer, w = get_layer(pressure, dewpt, bottom=bottom, depth=bottom - top)
#         # print(pres_layer, w)
#         # exit()
#         # Since pressure is in decreasing order, pw will be the opposite sign of that expected.
#         pw = np.trapz(w.magnitude, pres_layer.magnitude) * (w.units * pres_layer.units)
#         return -pw
#
#     hgt = np.ones(shum.shape)
#     for i in range(shum.shape[0]):
#         hgt[i] = hgt[i] * levels[i]
#
#     from metpy.units import units
#     qu = uwnd * shum * 9.8
#     qv = vwnd * shum * 9.8
#     qu = qu * units('kg/(s*hPa*cm)')
#     qv = qv * units('kg/(s*hPa*cm)')
#     hgt = hgt * units.hPa
#
#     uresult = np.trapz(qu.magnitude, hgt.magnitude, axis=0)
#     vresult = np.trapz(qv.magnitude, hgt.magnitude, axis=0)
#     # nlat, nlon = hgt.shape[1:3]
#     # uresult = np.empty([nlat, nlon])
#     # vresult = np.empty([nlat, nlon])
#     # import numpy as np
#     # for ilat in range(nlat):
#     #     for ilon in range(nlon):
#     #         uresult[ilat, ilon] = np.array(precipitable_water(hgt[:, ilat, ilon],
#     #                                                          qu[:, ilat, ilon], bottom=slp[ilat, ilon]))
#     #         vresult[ilat, ilon] = np.array(precipitable_water(hgt[:, ilat, ilon],
#     #                                                           qv[:, ilat, ilon], bottom=slp[ilat, ilon]))
#     return uresult, vresult
# #
# additionStr = ''
# shum = xr.open_dataset(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/shum.mon.mean%s.nc'%additionStr)
# uwnd = xr.open_dataset(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/uwnd.mon.mean%s.nc'%additionStr).sel(level=shum.level)
# vwnd = xr.open_dataset(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/vwnd.mon.mean%s.nc'%additionStr).sel(level=shum.level)
# hgt = xr.open_dataset(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/hgt.mon.mean%s.nc'%additionStr).sel(level=shum.level)
#
# # shum.isel(time=[-2,-1]).to_netcdf(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/shum.mon.mean_1.nc')
# # uwnd.isel(time=[-2,-1]).to_netcdf(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/uwnd.mon.mean_1.nc')
# # vwnd.isel(time=[-2,-1]).to_netcdf(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/vwnd.mon.mean_1.nc')
# # hgt.isel(time=[-2,-1]).to_netcdf(r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/hgt.mon.mean_1.nc')
# # exit()
# qu = np.zeros(uwnd['uwnd'].values[:,0,:,:].shape)
# qv = np.zeros(qu.shape)
#
# for i_I, si in enumerate(shum.time.values):
#     i = str(si)
#     qu[i_I], qv[i_I] = to_uwnd_vwnd(shum.sel(time=si).variables['shum'].values,
#                  uwnd.sel(time=si).variables['uwnd'].values,
#                  vwnd.sel(time=si).variables['vwnd'].values,
#                  hgt.level.values)
#
# # nc4.save_nc4(qu, '/home/linhaozhong/work/AR_NP85/Budget_tool-main/quqv/qu')
# # nc4.save_nc4(qv, '/home/linhaozhong/work/AR_NP85/Budget_tool-main/quqv/qv')
#
# def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
#     """
#     :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
#     :param var_name: ['t2m', 'ivt', 'uivt']
#     :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
#     :param lon: list, 720
#     :param lat: list, 360
#     :param path_save: r'D://'
#     :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
#     :return:
#     """
#     import xarray as xr
#     import numpy as np
#
#     if lon is None:
#         lon = np.arange(0, 360)
#     if lat is None:
#         lat = np.arange(90, -91, -1)
#
#     # def get_dataarray(data):
#     #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
#     #     return foo
#
#     ds = xr.Dataset()
#     ds.coords["lat"] = lat
#     ds.coords["lon"] = lon
#     ds.coords["time"] = times
#
#     for iI_vari, ivari in enumerate(var_name):
#
#         ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))
#
#     print(ds)
#     ds.to_netcdf(path_save + '%s.nc' % file_name)
#
# save_standard_nc([qu], ['qu'], shum.time.values, lon=shum.lon.values, lat=shum.lat.values,
#                  path_save=r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/quqv/',
#                  file_name='qu')
# save_standard_nc([qv], ['qv'], shum.time.values, lon=shum.lon.values, lat=shum.lat.values,
#                  path_save=r'/home/linhaozhong/work/AR_NP85/Budget_tool-main/quqv/',
#                  file_name='qv')
#
# exit()



a =xr.open_dataset(r'download.nc')
print(a)
qv = a['p72.162'].values
qu = a['p71.162'].values

b = xr.open_dataset(r'D:/qv.mon.mean.nc')['qv'].sel(time='2015-11').values

# c = xr.open_dataset(r'D:/qv.nc')['qv'].values
fig, ax = plt.subplots(1,3)


cb = ax[0].imshow(b[0,::-1]-qv[0])
plt.colorbar(cb, ax=ax[0])
cb = ax[1].imshow(qv[0],vmin=-400,vmax=400)
plt.colorbar(cb, ax=ax[1])
cb = ax[2].imshow(b[0,::-1],vmin=-400,vmax=400)
plt.colorbar(cb, ax=ax[2])
plt.show()


# cb = ax[0].imshow(-b[1][::-1]-qu[1])
# plt.colorbar(cb, ax=ax[0])
# cb = ax[1].imshow(qu[1])
# plt.colorbar(cb, ax=ax[1])
# cb = ax[2].imshow(-b[1][::-1])
# plt.colorbar(cb, ax=ax[2])
# plt.show()



