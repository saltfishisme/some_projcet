import os
import shutil

import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import numpy as np
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
import nc4


def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    # def get_dataarray(data):
    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
    #     return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times

    for iI_vari, ivari in enumerate(var_name):
        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + file_name)

# filename = r''
# path_cmip6 = r'G:\cmip6_tas/'
#
# cmip6_filename = os.listdir(path_cmip6)
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     if len(sp) == 7:
#         cmip6_filename_splite.append(sp)
#
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
# print(cmip6_filename_splite)
# cmip6_filename_missing = []
#
# def find_file_name(x):
#     file_titile =  ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], ['historical'], x['4'], x['5'], ['185001-201412.hdf'])]
#     file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
#     # get period of cmip6 file
#     if list(x['3'])[0] == 'historical':
#         return
#     print(path_cmip6+file_titile[0])
#     a = xr.open_dataset(path_cmip6+file_titile[0])
#
#     b = xr.open_dataset(path_cmip6 + file[0], use_cftime=True)
#     a = xr.concat([a, b], dim='time')
#
#     a.to_netcdf(r'G:\%s_185001-210012.nc' % (['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'])][0]))
#
#
# cmip6_filename_splite.groupby([
#                                cmip6_filename_splite['3'],
#                                cmip6_filename_splite['4']]).apply(
#     lambda x: find_file_name(x))
#
# exit()

"""
===============================
find tas models based on z500 models
===============================
"""

# path_cmip6 = r'G:\cmip6_FireIndex/cmip6_oneModel/'
#
# cmip6_filename = os.listdir(path_cmip6)
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     cmip6_filename_splite.append(sp)
#
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
#
# x = pd.read_csv('G:/cmip6_z500_split.csv', index_col=0)
#
# # file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
# x = x.fillna('-999')
# model_z500 = np.unique(cmip6_filename_splite['2'])
#
# file_tas = []
# for i in x.index:
#     if np.array(x['5'].loc[i])=='-999':
#         file = x.loc[i]["0"]+'_'+x.loc[i]["1"]+'_'+x.loc[i]['2']+'_'+x.loc[i]['3']+'_'+x.loc[i]['4']
#         if np.isin(x.loc[i]['2'],model_z500,):
#             file_tas.append(x.loc[i]['2'])
# print(np.unique(file_tas))
# exit()
# np.savetxt('G:\cmip6_FireIndex/file_tas.txt', file_tas, fmt='%s')
# exit()

"""
===============================
combine different run number into one
===============================
"""

# path_z500_save = r'/home/linhaozhong/work/Data/cmip6_oneModel/'
# path_cmip6 = r'/home/linhaozhong/work/Data/CMIP6/'
#
# cmip6_filename = np.loadtxt('/home/linhaozhong/work/Data/file_tas.txt', dtype=str)
# # cmip6_filename = os.listdir(path_cmip6)
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     cmip6_filename_splite.append(sp)
#
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
#
# cmip6_filename_use = cmip6_filename_splite[(cmip6_filename_splite['0'] == 'tas')
#                                            & (cmip6_filename_splite['1'] == 'mon')]
# cmip6_filename_missing = []
# print(cmip6_filename_use)
# def find_file_name(x):
#     print('whiy not me')
#     print(x)
#     file_titile =  ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'])]
#     file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'])]
#     # get period of cmip6 file
#     a_all = []
#     for i_file in file:
#         data = xr.open_dataset(path_cmip6+i_file)
#         a = data['tas'].values
#
#         a_all.append(a)
#     a_all = np.nanmean(a_all, axis=0)
#
#     save_standard_nc([a_all], ['tas'], times=data.time, lon=data.lon, lat=data.lat,
#                      path_save=path_z500_save, file_name=file_titile[0]+'.nc')
#
# cmip6_filename_use.groupby([cmip6_filename_use['2'],
#                             cmip6_filename_use['3']], group_keys=True).apply(
#     lambda x: find_file_name(x))
# exit()
"""
===============================
global surface temperature mean
===============================
"""

# path_cmip6 = r'/home/linhaozhong/work/Data/cmip6_oneModel/'
# model = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
#
# def cal_area_differentLL(lon, lat):
#     """
#     work for
#     :param lon:
#     :param lat:
#     :return:
#     """
#     import math
#     import numpy as np
#     lon, lat = np.meshgrid(lon, lat)
#
#     R = 6378.137
#
#     dlon = np.zeros(lon.shape)
#     dlon[:,:-1] = list(np.diff(lon))
#     dlon[:,-1] = dlon[:,-2]
#
#     delta = dlon * math.pi / 180.
#
#     Dx = R * np.cos(lat * math.pi / 180.) * delta
#     Dy = delta * R
#
#     S = Dx * Dy
#     return S
#
#
# # ERA5 t2m, for climate adjust.
# a = xr.open_dataset(r"/home/linhaozhong/t2mTP_1940_2023.nc")
# # ranged
# a = a.sel(time=np.in1d(a['time.year'], np.arange(1940,2022+1)))
# def reduce_expver(data, axis='dim'):
#     result = np.nansum(data,axis=axis)
#     result[np.all(np.isnan(data), axis=axis)] = np.nan
#     return result
# a = a.reduce(reduce_expver, 'expver')
#
# a_ranged = a.sel(latitude=slice(75, -60))
#
# area_weight = cal_area_differentLL(a_ranged.longitude, a_ranged.latitude)
# area_weight = area_weight / np.sum(area_weight)
# mean_t2m = np.nansum(a_ranged['t2m'].values * area_weight, axis=(1,2))
# import nc4
# nc4.save_nc4(np.array(mean_t2m), '/home/linhaozhong/work/Data/era5')
# exit()
#
#
# for i_model in model:
#     line_t2m = []
#     for i_filename in os.listdir(path_cmip6):
#         if not i_model in i_filename:
#             continue
#         a = xr.open_dataset(path_cmip6+i_filename)
#         # ranged
#
#         a_ranged = a.sel(lat=slice(-60, 75))
#
#         if a_ranged['tas'].values.shape[0] == 0:
#             a_ranged = a.sel(lat=slice(75, -60))
#
#         area_weight = cal_area_differentLL(a_ranged.lon, a_ranged.lat)
#         area_weight = area_weight / np.sum(area_weight)
#         mean_t2m = np.nansum(a_ranged['tas'].values * area_weight, axis=(1,2))
#         line_t2m.append(mean_t2m)
#
#     import nc4
#     nc4.save_nc4(np.array(line_t2m), '/home/linhaozhong/work/Data/%s'%i_model)
#
#
# exit()

"""
=================
get internal variation of every models
and then make a boxplot.
=================
"""

# loc_2015 = 2015-1850
# loc_2015 = 0
# fig, ax = plt.subplots(1, figsize=[9,4])
# labels = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
# x_row = np.arange(1850,2100+1)[loc_2015:]
# colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red']
# month_selected = [2,3,4,5]
# month_name = '_merge'
# internal_variations = []
# for iI_ssp, i_model in enumerate(model):
#     t2m_line = np.mean(nc4.read_nc4(path_t2m+i_model), axis=0)
#     date = pd.date_range('1850-01-01', '2100-12-31', freq='1M')
#     t2m_line = pd.DataFrame(t2m_line, index=date)
#     def select_months(year_data):
#         x = year_data.loc[year_data.index.month.isin(month_selected)]
#         x = x.mean()
#         return x
#     selected_data = t2m_line.groupby(t2m_line.index.year).apply(select_months)
#
#     # begin
#     t2m_line = np.squeeze(np.array(selected_data))
#     z500_line = nc4.read_nc4('line_collection%s'%month_name)[:,iI_ssp]
#
#     y1, y2 = np.percentile(z500_line, [5,95], axis=0)
#     avg = np.mean(z500_line, axis=0)
#     x = x_row
#
#     # collection = []
#     # for y in [y1, y2, avg]:
#     #     slope, intercept, _, p_here, _ = linregress(zscore(y), zscore(t2m_line))
#     #
#     #     # forcing
#     #     external_forcing = slope*y
#     #     internal_variation = y-slope*y
#     #     collection.append(external_forcing)
#     # print(slope, p_here)
#     #
#     # y1,y2, avg = collection
#
#     # x, y1 = fast_moving_average(x_row, y1)
#     # x, y2 = fast_moving_average(x_row, y2)
#     # x, avg = fast_moving_average(x_row, avg)
#
#     if iI_ssp == 0:
#         ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[iI_ssp])
#     ax.plot(x, avg, label=labels[iI_ssp], color=colors[iI_ssp])
#
#     # internal_variations.append(collection)
#
# plt.legend(loc='upper left')
# plt.title('M%s'%month_name[1:])
# plt.show()



"""
=================
figureA_parCor_20c_monthly.mat
=================
"""

import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import nc4
import pandas as pd
import numpy as np


# def nc_anomaly_monthly():
#     da = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\Forest_fire\z500_ERA5_1940-2022.nc")
#     print(da)
#     da = da.assign_coords(month_day=da.time.dt.strftime("%m"))
#     result = da.groupby("month_day") - da.groupby("month_day").mean("time")
#     print(result)
#     return result
#
# def nc_anomaly_monthly_20c():
#     da = xr.open_dataset(r"G:\OneDrive\basis\some_projects\zhong\Forest_fire\hgt.mon.mean.nc")
#
#     da = da.sel(level=500)
#     da = da.assign_coords(month_day=da.time.dt.strftime("%m"))
#     result = da.groupby("month_day") - da.groupby("month_day").mean("time")
#     print(result)
#     return result
#
# data_Global_sta = nc_anomaly_monthly()
# data_Global_sta_234 = data_Global_sta['z'].sel(time=np.in1d(data_Global_sta['time.month'], [2,3,4])).values
# data_Global_sta_5 = data_Global_sta['z'].sel(time=np.in1d(data_Global_sta['time.month'], [5])).values
#
# # for era5!
# data_Global_sta_234 = data_Global_sta_234[:, ::-1, :]
# data_Global_sta_5 = data_Global_sta_5[:, ::-1, :]
# #
# #
#
# # data_Global_sta = nc_anomaly_monthly_20c()
# # data_Global_sta.to_netcdf(r'monthly_20c.nc')
#
# # data_Global_sta = xr.open_dataset(r'monthly_20c.nc')
# # # data_Global_sta = data_Global_sta.sel(time=np.in1d(data_Global_sta['time.year'], np.arange(1900,2015+1)))
# # data_Global_sta_234 = data_Global_sta['hgt'].sel(time=np.in1d(data_Global_sta['time.month'], [2,3,4])).values
# # data_Global_sta_5 = data_Global_sta['hgt'].sel(time=np.in1d(data_Global_sta['time.month'], [5])).values
#
#
#
# target = scipy.io.loadmat('figure2_1900-2010.mat')
# data_target234 = target['f2a_M234_Hgt500']
# data_target5 = target['f2a_M5_Hgt500']
#
#
# lon = target['lon'][0]
# lat = target['lat'][0]
#
# """ additianl """
# data = {}
#
# ranges = [30,170,35,80]
#
# loc_north = max([np.argmin(abs(lat - ranges[3])), np.argmin(abs(lat - ranges[2]))])
# loc_south = min([np.argmin(abs(lat - ranges[3])), np.argmin(abs(lat - ranges[2]))])
# loc_west = min([np.argmin(abs(lon - ranges[0])), np.argmin(abs(lon - ranges[1]))])
# loc_east = max([np.argmin(abs(lon - ranges[0])), np.argmin(abs(lon - ranges[1]))])
#
# data['data_234'] = np.transpose(data_Global_sta_234[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
# data['target_234'] = data_target234[loc_south:loc_north+1, loc_west:loc_east+1]
#
# lon_234, lat_234 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
# data['lat_234'] = lat_234
# data['lon_234'] = lon_234
#
# data['data_5'] = np.transpose(data_Global_sta_5[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
# data['target_5'] = data_target5[loc_south:loc_north+1, loc_west:loc_east+1]
#
# lon_5, lat_5 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
# data['lat_5'] = lat_5
# data['lon_5'] = lon_5
# scipy.io.savemat('figureA_parCor_20c_monthly.mat', data)
# exit()


# date_base = np.arange(1940, 2022 + 1)
# # date_base = np.arange(1836, 2015 + 1)
# cor = scipy.io.loadmat('pattern_Cor.mat')
# month_name = ['234', '5']
# result = {}
# result_rawdata = {}
#
# for method01 in ['1']:
#     cor234 = cor['yearly_cor234_method%s'%method01]
#     cor5 = cor['yearly_cor5_method%s'%method01]
#
#     date = pd.date_range('%i-01-01'%date_base[0], '%i-12-31'%date_base[-1], freq='1M')
#
#     cor234 = pd.DataFrame(cor234, index=date[np.in1d(date.month, [2,3,4])])
#     cor5 = pd.DataFrame(cor5, index=date[np.in1d(date.month, [5])])
#
#     merge_daily = []
#     merge_yearly = []
#     for i_year in date_base:
#         list_now = list(cor234[np.in1d(cor234.index.year, i_year)].values)+list(cor5[np.in1d(cor5.index.year, i_year)].values)
#         merge_daily.extend(list_now)
#
#         mean_234 = np.mean(cor234[np.in1d(cor234.index.year, i_year)].values)
#         mean_5 = np.mean(cor5[np.in1d(cor5.index.year, i_year)].values)
#         merge_yearly.append((mean_234+mean_5)/2)
#
#     merge_daily = pd.DataFrame(merge_daily, index=date[np.in1d(date.month, [2,3,4,5])])
#     result_rawdata['monthly_m2345'] = merge_daily.values
#
#     # result_rawdata['yearly_m2345'] = merge_daily.resample('1Y').mean().values
#     result_rawdata['yearly_m2345'] = merge_yearly
#     result_rawdata['year'] = date_base
#     for i_month, data_row in enumerate([cor234, cor5]):
#         data = data_row.resample('1Y').mean()
#         result_rawdata['yearly_m%s' % month_name[i_month]] =data.values
#         from scipy.stats import zscore
#         from scipy.stats import linregress
#         x = data.index.year.values
#         y = zscore(np.squeeze(data[0].values))
#
#         from scipy.signal import detrend
#
#         from scipy.stats import pearsonr
#         data_row_fireindex = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])
#
#         fireindex = data_row_fireindex[(data_row_fireindex['year'] >= date_base[0]) & (data_row_fireindex['year'] <= date_base[-1])]
#
#         detrended2 = list(zscore(fireindex['nechinamogoliaSiberia']))
#         # result_rawdata['fireIndex_zscore' ] = detrended2
#         # r,p = pearsonr(y, detrended2)
#         # print(pearsonr(np.squeeze(merge_daily.resample('1Y').mean().values)[:150], detrended2[:150]))
#         # print(pearsonr(np.squeeze(merge_yearly)[:64], detrended2[:64]))
#         #
#         # print('1900')
#         # print(pearsonr(np.squeeze(merge_daily.resample('1Y').mean().values)[150:], detrended2[150:]))
#         # print(pearsonr(np.squeeze(merge_yearly)[64:], detrended2[64:]))
#         ddd = np.squeeze(merge_yearly)[64:]
#         # slope, intercept, _, p_here, _ = linregress(np.arange(len(ddd)), ddd)
#         # print(linregress(np.arange(len(ddd)), ddd))
#
#
# scipy.io.savemat('patternCor_rawData_ERA5.mat', result_rawdata)
# exit()

# import scipy.io
# #
# a = scipy.io.loadmat('patternCor_rawData_20c_monthly_megafire.mat')
# b = scipy.io.loadmat('patternCor_rawData_20c_monthly.mat')
#
# megafire = a['yearly_m2345'][0]
# lowfire = b['yearly_m2345'][0]
# year = np.arange(1836, 2015+1)
# plt.plot(year, megafire, label='megafire')
# plt.plot(year, lowfire, label='fire')
# plt.legend()
# plt.show()
#
# ddd = lowfire[64:]
# print(linregress(np.arange(len(ddd)), ddd))
#
# ddd = megafire[64:]
# print(linregress(np.arange(len(ddd)), ddd))
# exit()

"""
=================
get internal variation of every models
and then make a lineplot.
=================
"""

def t2m_toGivenMonth(t2m_line, time):
    date = pd.date_range('%i-01-01'%time[0], '%i-12-31'%time[1], freq='1M')
    t2m_line = pd.DataFrame(t2m_line, index=date)

    def monthly_mean(year_data):
        x_mean = year_data.mean()
        return year_data - x_mean

    t2m_line = t2m_line.groupby(t2m_line.index.month).apply(monthly_mean)

    def select_months(year_data):
        x = year_data.loc[year_data.index.month.isin(month_selected)]
        x = x.mean()
        return x

    selected_data = t2m_line.groupby(t2m_line.index.year).apply(select_months)
    return np.squeeze(np.array(selected_data))

def climate_adjust(data, loc):
    """
    :param data: 2d, time+model
    :param loc: loc position
    :return:
    """

    if len(data.shape) == 2:
        avg1 = np.nanmean(data[:, :loc], axis=1)
        avg2 = np.nanmean(data[:, loc:], axis=1)
        data[:, loc:] = data[:, loc:] + np.array(avg1 - avg2).reshape(-1, 1)
    if len(data.shape) == 1:
        avg1 = np.nanmean(data[:loc])
        avg2 = np.nanmean(data[loc:])
        data[loc:] = data[loc:] + np.array(avg1 - avg2)
    return data

def climate_adjust_byERA5(data, loc):
    """
    :param data: 2d, time+model
    :param loc: loc position
    :return:
    """
    era5 = nc4.read_nc4(path_t2m + 'era5')
    era5 = t2m_toGivenMonth(era5, [1940, 2022])
    avg1_era5 = np.nanmean(era5[:2014-1940+1])
    avg2_era5 = np.nanmean(era5[2015-1940:])
    avg1 = np.nanmean(data[1940-1850:2014-1850+1])
    avg2 = np.nanmean(data[2015-1850:2022-1850+1])

    data[:loc] = data[:loc] + np.array(avg1_era5 - avg1)
    data[loc:] = data[loc:] + np.array(avg2_era5 - avg2)

    return data

def climate_adjust_byfireIndex_row(data, loc, extent=None):
    """
    :param data: 2d, time+model
    :param loc: loc position
    :return:
    """
    era5 = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\patternCor_rawData_20c_monthly.mat')
    era5_2 = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\patternCor_rawData_ERA5.mat')

    year = [int(i) for i in era5['year'][0]]
    year = [year[0], 2022]

    # plt.plot(np.arange(year[0],2010+1), era5['yearly_m%s'%month_name[1:]][:,0])
    # plt.plot(np.arange(1940,2022+1), era5_2['yearly_m%s'%month_name[1:]][:,0])
    # plt.show()

    if month_name == '_merge':
        era5 = era5['yearly_m2345'][0][1850-2010:]
        era5_2 = era5_2['yearly_m2345'][0][2010-1940:]
    else:
        era5 = era5['yearly_m%s'%month_name[1:]][1850-2010:]
        era5_2 = era5_2['yearly_m%s'%month_name[1:]][2010-1940:]
    era5 = np.squeeze(np.array(list(era5)+list(era5_2)))

    # get external forcing
    z500_line = nc4.read_nc4('line_collection%s' % month_name)[:, 1] # ssp245 as usual

    # get 20c year
    z500_line = np.nanmean(z500_line[:, :year[-1]-1850+1], axis=0)

    # external forcing

    slope, intercept, _, p_here, _ = linregress(zscore(z500_line), zscore(era5))
    era5 = abs(slope)*era5

    avg1_era5 = np.nanmean(era5[:2014-1850+1])
    avg2_era5 = np.nanmean(era5[2015-1850:])

    avg1 = np.nanmean(data[:2014-1850+1])
    avg2 = np.nanmean(data[2015-1850:2022-1850+1])

    print('ca1', np.array(avg1_era5 - avg1))
    print('ca2', np.array(avg2_era5 - avg2))
    # data[:loc] = data[:loc] + np.array(avg1_era5 - avg1)
    # data[loc:] = data[loc:] + np.array(avg2_era5 - avg2)

    avg2_add = avg1*(avg2_era5/avg1_era5)-avg2
    # data[:loc] = data[:loc] + np.array(avg1_era5 - avg1)
    data[loc:] = data[loc:] + np.array(avg2_add)
    if extent is not None:
        yy = []
        for i_y in extent:
            # i_y[:loc] = i_y[:loc] + np.array(avg1_era5 - avg1)
            # i_y[loc:] = i_y[loc:] + np.array(avg2_era5 - avg2)

            i_y[loc:] = i_y[loc:] + np.array(avg2_add)

            yy.append(i_y)
        return data, yy
    return data

def climate_adjust_byfireIndex(data, loc, extent=None):
    """
    :param data: 2d, time+model
    :param loc: loc position
    :return:
    """
    era5 = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\patternCor_rawData_20c_monthly.mat')
    era5_2 = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\patternCor_rawData_ERA5.mat')

    year_20c = [era5['year'][0], era5['year'][-1]]
    year_era5 = [era5_2['year'][0][0], era5_2['year'][0][-1]]

    use_year_begin = 1940
    use_year_end = 2022
    # plt.plot(np.arange(year[0],2010+1), era5['yearly_m%s'%month_name[1:]][:,0])
    # plt.plot(np.arange(1940,2022+1), era5_2['yearly_m%s'%month_name[1:]][:,0])
    # plt.show()

    if month_name == '_merge':
        era5 = era5['yearly_m2345'][0]
        era5_2 = era5_2['yearly_m2345'][0]
    else:
        era5 = era5['yearly_m%s'%month_name[1:]]
        era5_2 = era5_2['yearly_m%s'%month_name[1:]]

    era5 = np.squeeze(era5_2[use_year_begin-1940:])
    # plt.plot(range(use_year_begin, use_year_end+1), era5)
    # plt.show()
    # get external forcing
    z500_line = nc4.read_nc4('line_collection%s' % month_name)[:, 1] # ssp245 as usual

    # get 20c year
    z500_line = np.nanmean(z500_line[:, use_year_begin-1850:year_era5[-1]-1850+1], axis=0)

    # external forcing

    slope, intercept, _, p_here, _ = linregress(zscore(z500_line), zscore(era5))
    era5 = abs(slope)*era5

    avg1_era5 = np.nanmean(era5[:2014-use_year_begin+1])
    avg2_era5 = np.nanmean(era5[2015-use_year_begin:])

    avg1 = np.nanmean(data[:2014-use_year_begin+1])
    avg2 = np.nanmean(data[2015-use_year_begin:2022-use_year_begin+1])

    print('ca1', avg1_era5, avg1)
    print('ca2', avg2_era5, avg2)
    avg1 = 0
    avg2 = data[:loc][-1]-data[loc:][0]
    data[:loc] = data[:loc] + avg1
    data[loc:] = data[loc:] + avg2

    # avg2_add = avg1*(avg2_era5/avg1_era5)-avg2
    # # data[:loc] = data[:loc] + np.array(avg1_era5 - avg1)
    # data[loc:] = data[loc:] + np.array(avg2_add)
    if extent is not None:
        yy = []
        for i_y in extent:
            i_y[:loc] = i_y[:loc] + avg1
            i_y[loc:] = i_y[loc:] + avg2

            # i_y[loc:] = i_y[loc:] + np.array(avg2_add)

            yy.append(i_y)
        return data, yy
    return data

def fast_moving_average(x, y, types='gaussian'):
    from scipy import interpolate
    from scipy import ndimage
    """
    :param x:
    :param y:
    :param types: 'spline_xy', 'spline', 'gaussian'
    :return:
    """
    # convert both to arrays
    x_sm = np.array(x)
    y_sm = np.array(y)

    # resample to lots more points - needed for the smoothed curves
    x_smooth = np.linspace(np.min(x_sm), np.max(x_sm), 1000)

    # spline - always goes through all the data points x/y
    if types == 'spline_xy':
        tck = interpolate.splrep(x, y)
        y_spline = interpolate.splev(x_smooth, tck)
        return x_smooth, y_spline

    if types == 'spline':
        spl = interpolate.UnivariateSpline(x, y) # 'spline'
        return x_smooth, spl(x_smooth)

    if types == 'gaussian':
        sigma = 3
        x_g1d = ndimage.gaussian_filter1d(x_sm, sigma)
        y_g1d = ndimage.gaussian_filter1d(y_sm, sigma)
        return x_g1d,y_g1d
    return
from scipy.stats import linregress
path_t2m = r'G:\cmip6_FireIndex/'
path_z500 = r''
model = ['ssp126', 'ssp245', 'ssp370', 'ssp585']

loc_2015 = 2015-1850
loc_2015 = 0

labels = ['SSP1-2.6', 'SSP2-4.5', 'SSP3-7.0', 'SSP5-8.5']
labels_matVar = ['SSP1', 'SSP2', 'SSP3', 'SSP5']
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red']
x_row = np.arange(1850,2100+1)[loc_2015:]

month_selected_all = [[2,3,4], [5], [2,3,4,5]]
month_name_all = ['_234', '_5', '_merge']

# external_interanl = '_ex'
# #
# for iI_ssp, i_model in enumerate(model):
#     t2m_line = nc4.read_nc4(path_t2m + i_model)
#     # y = np.nanmean(t2m_line, axis=0)
#     # # y = t2m_line[2]
#     # y = y.reshape(-1, 12)
#     # plt.plot(np.arange(1850, 2100 + 1), y[:,1:4+1].mean(axis=1), label=i_model)
#     # print(t2m_line.shape)
#     t2m_line_new = []
#     for i_t2m in np.arange(8):
#         t2m_line_new.append(t2m_toGivenMonth(t2m_line[i_t2m],[1850, 2100]))
#     plt.plot(np.arange(1850, 2100 + 1), np.nanmean(t2m_line_new, axis=0), label=i_model)
# plt.legend()
# plt.show( )
# plt.close()
# exit()

collection_loop = {}
line_collection_loop = {}


external_interanl = '_ex'
titles = ['FEB-APR', 'MAY', 'FEB-MAY']
for iI_month_selected in np.arange(3):
    month_selected = month_selected_all[iI_month_selected]
    month_name = month_name_all[iI_month_selected]

# month_selected = [2,3,4,5]
# month_name = '_merge'
# titles = ['the externally-forced signal', 'the  internal climate variability signal', 'Model (CMIP6)']
# for iI_month_selected, external_interanl in enumerate(['_ex', '_in', '_total']):

    fig, ax = plt.subplots(1, figsize=[9, 4])
    internal_variations = []
    collection_t2m = []

    for iI_ssp, i_model in enumerate(model):
        # begin
        t2m_line = np.nanmean(nc4.read_nc4(path_t2m + i_model),axis=0)
        t2m = t2m_toGivenMonth(t2m_line, [1850, 2100])
        collection_t2m.append(t2m)
        z500_line = nc4.read_nc4('line_collection%s'%month_name)[:,iI_ssp]

        # climate adjust

        collection = []
        for iI_y in range(8):
            y = z500_line[iI_y]
            # t2m = climate_adjust_byERA5(t2m, 2015 - 1850)
            # y = climate_adjust(y, 2015 - 1850)
            loc = 2015-1850
            loc_1900 = 1900-1850
            slope, intercept, _, p_here, _ = linregress(zscore(t2m[loc_1900:loc]), zscore(y[loc_1900:loc]))
            # slope, intercept, _, p_here, _ = linregress(zscore(t2m[:loc]), zscore(y[:loc]))
            # forcing
            if external_interanl == '_ex':
                external_forcing_1 = abs(slope)*y[:loc]
            elif external_interanl == '_total':
                external_forcing_1 = y[:loc]
            elif external_interanl == '_in':
                external_forcing_1 = y[:loc]-abs(slope)*y[:loc]

            # slope, intercept, _, p_here, _ = linregress(zscore(t2m[loc:]), zscore(y[loc:]))
            # forcing
            if external_interanl == '_ex':
                external_forcing_2 = abs(slope)*y[loc:]
            elif external_interanl == '_total':
                external_forcing_2 = y[loc:]
            elif external_interanl == '_in':
                external_forcing_2 = y[loc:]-abs(slope)*y[loc:]

            external_forcing = list(external_forcing_1)+list(external_forcing_2)
            collection.append(external_forcing)
        internal_variations.append(collection)
    nc4.save_nc4(np.array(internal_variations).transpose([1,0,2]), 'line_collection_external%s'%month_name)
    collection_loop['line_t2m'+str(iI_month_selected+1)] = np.array(collection_t2m, dtype='double')


    collection = []
    collection_linregress = []
    line_collection = []
    for iI_ssp, i_model in enumerate(model):

        z500_line = nc4.read_nc4('line_collection_external%s'%month_name)[:,iI_ssp]

        y1, y2 = np.percentile(z500_line, [5,95], axis=0)
        avg = np.mean(z500_line, axis=0)
        x = x_row

        # climate_adjust_byfireIndex(avg, 2015 - 1850, [y1, y2])
        avg, [y1,y2] = climate_adjust_byfireIndex(avg, 2015-1850, [y1,y2])

        # _, y1 = fast_moving_average(x_row, y1)
        # _, y2 = fast_moving_average(x_row, y2)
        # x, avg = fast_moving_average(x_row, avg)

        ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[iI_ssp])
        ax.plot(x, avg, label=labels[iI_ssp], color=colors[iI_ssp],linewidth=0.8)
        # ax.axvline(2014)
        collection.append(avg)
        line_collection.append([y1,avg,y2])

        loc_2015_lingress = 2015-1850
        slope, intercept, _, p_here, _ = linregress(x[loc_2015_lingress:], avg[loc_2015_lingress:])
        ax.plot(x[loc_2015_lingress:], slope*x[loc_2015_lingress:]+intercept, linestyle='--', color=colors[iI_ssp], linewidth=0.8)
        collection_linregress.append([slope, p_here, slope*x[loc_2015_lingress:]+intercept])
        # plt.show()

    collection_loop['line'+str(iI_month_selected+1)] = np.array(collection, dtype='double')

    line_collection_loop['M'+str(month_name)] =  np.array(line_collection, dtype='double')
    line_collection_loop['M'+str(month_name)+'_linregress'] = np.array([i[2] for i in collection_linregress])
    line_collection_loop['M'+str(month_name)+'_slope'] = np.array([[i[0],i[1]] for i in collection_linregress])

    # nc4.save_nc4(np.array(line_collection), 'Rsquared%s'%external_interanl)
    # exit()
    # mat_collection = {'external_forcing':np.array(line_collection, dtype='double'),
    #                                                  'year':np.arange(1850,2100+1)}
    #

    plt.legend(loc='upper left')
    plt.title(titles[iI_month_selected])
    ax.set_ylim([-1,1])
    ax.set_xticks(np.arange(1850,2100+1,25))

    # ======================================================
    c20_raw = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\patternCor_rawData_20c_monthly.mat')
    era5_raw = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\patternCor_rawData_ERA5.mat')
    if month_name == '_merge':
        c20 = c20_raw['yearly_m2345'][0]
        era5 = era5_raw['yearly_m2345'][0]
    else:
        c20 = c20_raw['yearly_m%s'%month_name[1:]]
        era5 = era5_raw['yearly_m%s'%month_name[1:]]
    print(c20_raw['year'])
    year_hist = (c20_raw['year'][0]>=1850)&(c20_raw['year'][0]<=2014)
    ax.plot(c20_raw['year'][0][year_hist], c20[year_hist])

    year_hist = (era5_raw['year'][0]>=1850)&(era5_raw['year'][0]<=2014)
    ax.plot(era5_raw['year'][0][year_hist], era5[year_hist])
    plt.show()
    # plt.savefig('cmip6_%s.png'%titles[iI_month_selected], dpi=400)
    plt.close()
exit()

# scipy.io.savemat(r'collection_loop_forTrendContri.mat', collection_loop)



tc = scipy.io.loadmat(r'G:\OneDrive\a_matlab/TrendContri.mat')
line_collection_loop['trendContri_abs'] = tc['D']

tc = scipy.io.loadmat(r'G:\OneDrive\a_matlab/TrendContri_rela.mat')
line_collection_loop['trendContri_relative'] = tc['D']
line_collection_loop['year'] = np.arange(1850,2100+1)
line_collection_loop['year_linregress'] = np.arange(2015,2100+1)


scipy.io.savemat('cmip6_external_forcing.mat', line_collection_loop)
exit()

import numpy as np
import statsmodels.api as sm

# 假设你有以下三条线的数据：
loc_2015 = [2015-1850, 2100-1850+1]
# loc_2015 = [0, 2100-1850+1]
line1 = nc4.read_nc4('Rsquared_ex')[:,loc_2015[0]:loc_2015[1]]
line2 = nc4.read_nc4('Rsquared_in')[:,loc_2015[0]:loc_2015[1]]
line3 = nc4.read_nc4('Rsquared_total')[:,loc_2015[0]:loc_2015[1]]

# line1 = np.array([line1,line1]).transpose([2,1,0])
# line2 = np.array([line2,line2]).transpose([2,1,0])
# line3 = np.array([line3,line3]).transpose([2,1,0])
# print(line3.shape)
# scipy.io.savemat(r'Rsquared.mat', {'line1':np.array(line1), 'line2':line2, 'line3':line3})
# exit()
for i in np.arange(4):
    def line_trend_contribution(x, y):
        # detrend anomaly series.
        # x = detrend(x)
        # y = detrend(y)
        trend_x, intercept, _, p_here, _ = linregress(np.arange(x.shape[0]), x)
        print(trend_x, p_here)
        slope, intercept, _, p_here, _ = linregress(zscore(x), zscore(y))
        return slope*trend_x
    print(line_trend_contribution(line1[i], line3[i]))
    print(line_trend_contribution(line2[i], line3[i]))
exit()
# line_all = [line1, line2, line3]
# for iI, line in enumerate(line_all):
#     plt.plot(np.arange(line[3].shape[0]), line[3], label=iI)
# plt.legend()
# plt.show()
# print(np.var(line1), np.var(line2), np.var(line3))
# exit()
# line1 = nc4.read_nc4('Rsquared_ex')
# print(line1.shape)
# line2 = nc4.read_nc4('Rsquared_in')
# line3 = nc4.read_nc4('Rsquared_total')
#
# a = pd.DataFrame()
# a['Y'] = line3[0]
# a['X1'] = line1[0]
# a['X2'] = line2[0]
#
# a.to_excel('test.xlsx')
# exit()


for i in range(4):
    import numpy as np
    from factor_analyzer import FactorAnalyzer

    # Generating some sample data for the demonstration

    factor1 = line1[i]
    factor2 = line2[i]

    # Y is a dependent variable influenced by both factors and some residual noise
    Y = line3[i]

    df = pd.DataFrame(np.array([factor1, factor2]).T,columns = ['x1','x2'])
    df['y'] = Y

    from statsmodels.formula.api import ols
    from statsmodels.stats.anova import anova_lm

    lm = ols('y ~ x2+x1', df).fit()
    # print(lm.rsquared)
    # exit()
    anova_table = anova_lm(lm)
    print('%%%%%%%%%%%%')
    print(anova_table)
    print(anova_table.loc['x1', 'sum_sq'] / anova_table['sum_sq'].sum())
    exit()
exit()
a = np.array([
    [0.664,0.160,0.28],
    [0.656,0.029,0.40],
    [0.666,0.360,0.37],
    [0.661,0.667,0.61]
])
mat_collection['variation_explained'] = a
scipy.io.savemat(r'cmip6_external_forcing.mat', mat_collection)
exit()




"""
=================
merge boxplot
=================
"""
periods = np.array([[2024,2050],[2074,2100]])
loc_periods = periods-1850

fig, ax = plt.subplots(1, figsize=[9,4])
plt.subplots_adjust(top=0.88,
bottom=0.295,
left=0.11,
right=0.9,
hspace=0.2,
wspace=0.2)

labels = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
periods_name = [' near future', ' far future']
colors = ['tab:blue', 'tab:orange', 'tab:green']

xtick = []
xticklabels = []
loc_box = [1,2,3,4]
loc_box_dict = {'234':[1,2,3,4],
                '5':[1.2,2.2,3.2,4.2],
                'merge':[1.4,2.4,3.4,4.4]}

collection_for_legend = []
for iI, merge_type in enumerate(['234', '5', 'merge']):
    loc_box = loc_box_dict[merge_type]
    line_collection = nc4.read_nc4('line_collection_interVar_%s'%merge_type)
    print(line_collection.shape)
    periods_1 = line_collection[:,:,loc_periods[0,0]:loc_periods[0,1]+1]
    periods_2 = line_collection[:,:,loc_periods[1,0]:loc_periods[1,1]+1]
    periods = [periods_1, periods_2]
    collection_for_legend.append(periods[0][:,0].flatten())
    for iI_boxplot in range(2):
        for i_box in range(4):
            xtick.append(loc_box[i_box]+iI_boxplot*4)
            xticklabels.append(labels[i_box])
            bp = ax.boxplot(periods[iI_boxplot][:,i_box].flatten(), positions=[loc_box[i_box]+iI_boxplot*4],
                       medianprops={'color':'black'},patch_artist=True)

            for patch, color in zip(bp['boxes'], [colors[iI],colors[iI],colors[iI],colors[iI]]):
                patch.set_facecolor(color)
        loc_box = np.array(loc_box)+0.5


#
bp = ax.boxplot(collection_for_legend, positions=[1,1.2,1.4],
                medianprops={'color': 'black'}, patch_artist=True, labels=['234', '5', 'merge'])
for patch, color in zip(bp['boxes'], colors):
    patch.set_facecolor(color)
ax.legend(bp["boxes"], ['234', '5', 'merge'], loc='upper center', bbox_to_anchor=(0.5, 1.15), ncol=3)



ax.set_xticks(xtick)
ax.set_xticklabels(xticklabels,rotation=90)
ax.set_ylim([-3,3])

plt.title('near future                                                                  far future')
plt.show()
exit()


"""
=================
single boxplot
=================
"""
merge_type = 'merge'
periods = np.array([[2024,2050],[2074,2100]])
loc_periods = periods-1850
line_collection = nc4.read_nc4('line_collection_interVar_%s'%merge_type)

periods_1 = line_collection[:,:,loc_periods[0,0]:loc_periods[0,1]+1]
periods_2 = line_collection[:,:,loc_periods[1,0]:loc_periods[1,1]+1]
periods = [periods_1, periods_2]
fig, ax = plt.subplots(1, figsize=[5,4])
plt.subplots_adjust(top=0.88,
bottom=0.295,
left=0.11,
right=0.9,
hspace=0.2,
wspace=0.2)

labels = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
periods_name = [' near future', ' far future']
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red']

xtick = []
xticklabels = []
loc_box = [[1,1.2,1.4,1.6],[2,2.2,2.4,2.6]]
for iI_boxplot in range(2):

    for i_box in range(4):
        xtick.append(loc_box[iI_boxplot][i_box])
        xticklabels.append(labels[i_box])
        ax.boxplot(periods[iI_boxplot][:,i_box].flatten(), positions=[loc_box[iI_boxplot][i_box]], whis=2.5)

ax.set_xticks(xtick)
ax.set_xticklabels(xticklabels,rotation=45)
ax.set_ylim([-3,3])
plt.suptitle('M'+merge_type)
plt.title('near future                       far future')
plt.show()
exit()





