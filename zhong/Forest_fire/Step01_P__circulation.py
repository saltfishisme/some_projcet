import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr
class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cmaps
        self.ax = ax
        self.crs = ccrs.PlateCarree()
        self.cmaps = ''
        self.colorbar=True
        self.orientation='h'
        self.ranges = ranges
        self.fmt_contourf='%.1f'
        self.fmt_contour='%.1f'
        self.ranges_reconstrcution_button_contourf = False
        self.ranges_reconstrcution_button_contour = False
        self.linewidths=1
        self.clabel_size=5
        self.cmaps=cmaps.WhiteGreen
        self.colorbar=True
        self.orientation='h'
        self.spacewind=[1,1]
        self.colors=False
        self.print_min_max=False
        self.contour_arg={'colors':'black'}
        self.contourf_arg={}
        self.quiverkey_arg={'X':0.85, 'Y':1.05, 'U':10, 'label': r'$10 \frac{m}{s}$'}
        self.contour_clabel_color = 'white'
        self.MidPointNorm = False
        def del_ll(range, lat, lon):
            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]
            del_area = ll_del(lat, lon, range[0])

            if del_area[0] > del_area[1]:
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1]+1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area
        if self.ranges[0] is not False:
            self.lat,  self.lon, self.del_area=del_ll(ranges, lat, lon)
        else:
            self.lat,  self.lon, self.del_area = [lat, lon, False]

    def del_data(self, data):
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1]+1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        def ranges_create(begin, end, inter):
            class MidPointNorm(Normalize):
                def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                    Normalize.__init__(self, vmin, vmax, clip)
                    self.midpoint = midpoint

                def __call__(self, value, clip=None):
                    if clip is None:
                        clip = self.clip

                    result, is_scalar = self.process_value(value)

                    self.autoscale_None(result)
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if not (vmin < midpoint < vmax):
                        raise ValueError("midpoint must be between maxvalue and minvalue.")
                    elif vmin == vmax:
                        result.fill(0)  # Or should it be all masked? Or 0.5?
                    elif vmin > vmax:
                        raise ValueError("maxvalue must be bigger than minvalue")
                    else:
                        vmin = float(vmin)
                        vmax = float(vmax)
                        if clip:
                            mask = ma.getmask(result)
                            result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                              mask=mask)

                        # ma division is very slow; we can take a shortcut
                        resdat = result.data

                        # First scale to -1 to 1 range, than to from 0 to 1.
                        resdat -= midpoint
                        resdat[resdat > 0] /= abs(vmax - midpoint)
                        resdat[resdat < 0] /= abs(vmin - midpoint)

                        resdat /= 2.
                        resdat += 0.5
                        result = ma.array(resdat, mask=result.mask, copy=False)

                    if is_scalar:
                        result = result[0]
                    return result

                def inverse(self, value):
                    if not self.scaled():
                        raise ValueError("Not invertible until scaled")
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if cbook.iterable(value):
                        val = ma.asarray(value)
                        val = 2 * (val - 0.5)
                        val[val > 0] *= abs(vmax - midpoint)
                        val[val < 0] *= abs(vmin - midpoint)
                        val += midpoint
                        return val
                    else:
                        val = 2 * (value - 0.5)
                        if val < 0:
                            return val * abs(vmin - midpoint) + midpoint
                        else:
                            return val * abs(vmax - midpoint) + midpoint
            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0+inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if isinstance(ranges_reconstrcution_button, list)  or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button
        else:

            ranges_min = np.nanmin(data); ranges_max = np.nanmax(data)
            if ranges_reconstrcution_button is False:
                if ranges_min>0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                ticks = (ranges_max-ranges_min)/30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

        if self.MidPointNorm is not False:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', norm=self.MidPointNorm, **self.contourf_arg)

        else:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', **self.contourf_arg)



        if self.colorbar:
            if self.orientation == 'h':
                orientation='horizontal'
            else:
                orientation='vertical'
            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.fmt_contourf)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)
        cbs = self.ax.clabel(
            cb,  fontsize=self.clabel_size,# Typically best results when labelling line contours.
            colors=['black'],
            inline=True,  # Cut the line where the label will be placed.
            fmt=self.fmt_contour,  # Labes as integers, with some extra space.
        )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)


        qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                ccrs.PlateCarree(),
        #                                edgecolor='k', facecolor='none', linewidths=0.55)
        # self.ax.add_feature(shape_feature, alpha=0.5)


freqType = 'high'
file_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/plot/'
# file_Path = r'D:\OneDrive\a_matlab\Blocking/'

BI = scio.loadmat(file_Path+r'BI_%s.mat'%freqType)['BI']
hgt_Z500 = scio.loadmat(file_Path+r'z500_%s_freq_year.mat'%freqType)['z500']
tem_t2m = scio.loadmat(file_Path+r't2m_%s_freq_year.mat'%freqType)['tem']

if freqType == 'high':
    years = [1987,2003]
else:
    years = [1990,2013]

varName = BI.dtype.names

############# work for hgt to mat ###################
hgt_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/Hgt/'


################################# create date based on years  #################################

def date_ranges(years):
    dates = []
    for iyear in years:
        date_s = '%s0315'%iyear; date_e = '%s0615'%iyear
        sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
        dates.extend(sdate)
    return dates

dates_all = date_ranges(years)
coordinates = pd.read_csv(file_Path+r'Coordinates.csv')
lat = coordinates['lat']
lon = coordinates['lon']

################################# get hgt to mat ########################################

# z500 = []
# for time in range(len(dates_all)):
#     hgt = xr.open_dataset(hgt_Path+dates_all[time][:4]+'/Hgt_%s.nc'%dates_all[time]).sel(level=500).mean(dim='time')['z'].values
#     z500.append(hgt/9.8)
# z500 =np.array(z500, dtype='double')
# z500 = np.transpose(z500, [1,2,0])
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/z500_%s_freq_year.mat'%freqType, {'z500':z500[::2, ::2, ]})
# sys.exit(0)


# tS = []
# for time in range(len(dates_all)):
#     print(time)
#     t = xr.open_dataset('/home/linhaozhong/work/Data/ERA5/single_level/2m_temperature/'+'/2m_temperature%s.nc'%dates_all[time]).mean(dim='time')['t2m'].values
#
#     tS.append(t)
# tS =np.array(tS, dtype='double')
# tS = np.transpose(tS, [1,2,0])
# scio.savemat('/home/linhaozhong/work/Data/ERA5/single_level/t2m_%s_freq_year.mat'%freqType, {'tem':tS})
# sys.exit(0)


for time in range(len(dates_all)):
    # plot = True
    # if np.isin( BI[varName[2]][0][time].flatten()[49:250], 1).any():
    # if plot:
    print(time)
    # print(time)
    fig= plt.figure()
    ax = fig.add_subplot(111, projection= ccrs.LambertConformal(central_longitude=120.0, central_latitude=30))
    # ax = fig.add_subplot(111, projection= ccrs.PlateCarree(central_longitude=105))
    ranges = [False, [90,120], [0,30]]


    ################################### circulation #######################################

    base_ax = circulation(ax, np.arange(0,360), np.arange(90,-91,-1), ranges)
    base_ax.fmt_contourf = '%.0f'
    base_ax.ranges_reconstrcution_button_contourf = np.arange(230,310,10)

    base_ax.fmt_contour = '%.0f'
    base_ax.print_min_max=True
    base_ax.ranges_reconstrcution_button_contour = np.arange(380,680,5)
    base_ax.contour_arg = {'linewidths':0.5, 'colors':'k'}

    base_ax.p_contourf(tem_t2m[:,:,time])
    base_ax.p_contour(hgt_Z500[:,:,time]/10)

    # hgt_now = scio.loadmat(r'D:\OneDrive\a_matlab\Blocking\matlab.mat')['HGT']
    # base_ax.p_contourf(hgt_now)
    # base_ax.p_contour(hgt_now)


    ################################### Blocking index #######################################

    ax.scatter(lon, lat, s=1, c='purple',transform=ccrs.PlateCarree(), zorder=999)
    cm = ax.scatter(np.arange(0, 360), np.array([60]).repeat(360),BI[varName[2]][0][time], c='red',transform=ccrs.PlateCarree(), zorder=999)

    # ax.text([np.arange(0, 360), np.arange(0, 360), np.arange(0, 360)], [np.array([50]).repeat(360),np.array([60]).repeat(360),np.array([70]).repeat(360)], c=BI[varName[4]][0][time], s=1,vmin=0.00001,transform=ccrs.PlateCarree(), zorder=999)
    # ax.scatter(np.arange(0, 360), np.array([ilat]).repeat(360), np.ones([360]), transform=ccrs.PlateCarree(), zorder=999)


    ###################################### feature ##########################################

    ax.set_extent([50, 250, 0, 90], crs=ccrs.PlateCarree())
    # ax.plot([90,90,115,115,90],[54,43,43,54,54], transform=ccrs.PlateCarree(), color='red')
    # ax.axvline(60, transfrom=ccrs.PlateCarree())
    # ax.axvline(130, transfrom=ccrs.PlateCarree())
    ax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False,linewidth=0.3, linestyle='--',xlocs=[60,90,110,150,180],ylocs=[0,40,60,80])
    # ax.stock_img()
    ax.coastlines()
    ax.set_title(dates_all[time])
    plt.show()
    plt.savefig(file_Path+r'%s/%i.png'%(freqType, time), dpi=600)
    plt.close()
