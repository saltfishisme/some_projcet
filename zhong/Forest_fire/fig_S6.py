import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
types = 'EKF' # EKF / 20C

date_base_low = np.arange(1836,1899+1)

date_base_high = np.arange(1900, 2010+1)
date_base = np.arange(1836,2010+1)

ranges = [97,126,45,54]
months = [[2,3,4],[5]]

def get_wind(var, var_year, var_month):
    wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
    wind_inmonth = wind_inYear.sel(
                                time=np.in1d(wind_inYear['time.month'], var_month))
    return wind_inmonth



import cmaps
orig_cmap = cmaps.cmp_b2r

# import numpy as np
# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# cmap = cmaps.WhiteBlue
# viridis = cmap
# orig_cmap = ListedColormap(viridis(np.linspace(0, 0.65, 128)))

savemat = []
for date_begin in [1750,1850]:
    for i_month in months:

        def get_EKF400_byYear(year):
            """
            get EKF400 data in given year, month, and range.
            :param year:
            :return:
            """
            t2m = []

            for i_year in year:
                a = xr.open_dataset(r"D:\fire\EKF400\EKF400_ensmean\EKF400_ensmean_%i_v1.1.nc"%i_year)
                # print(a['air_temperature'].sel(level_temp=2).isel(time=0))
                m_ranged = get_wind(a, i_year, i_month)
                m_ranged =  m_ranged['air_temperature'].sel(level_temp=2).sel(longitude=slice(97,125))
                iprint = 0
                def cal_tem_grad(m_ranged):
                    lat = m_ranged.latitude.values
                    loc54 = np.argmin(abs(lat - 53.67))
                    loc45 = np.argmin(abs(lat - 46.76))
                    # loc54 = np.argmin(abs(lat - 54))
                    # loc45 = np.argmin(abs(lat - 45))

                    # print(lat[loc54], lat[loc45], lat[loc54]-lat[loc45])


                    m_ranged_north = np.nanmean(m_ranged.isel(latitude=loc54).values)
                    m_ranged_south = np.nanmean(m_ranged.isel(latitude=loc45).values)
                    # print(m_ranged_north, m_ranged_south)
                    # exit()
                    tem_grad = (m_ranged_south - m_ranged_north) / 7.45935
                    # print(tem_grad)
                    return tem_grad

                s_tg = []
                for iii in i_month:

                    s_tg.append(cal_tem_grad(m_ranged.sel(
                                    time=np.in1d(m_ranged['time.month'], [iii]))))

                t2m.extend(np.array(s_tg))

            return np.array(t2m)

        # def get_20cc_byYear(year):
        #     """
        #     get EKF400 data in given year, month, and range.
        #     :param year:
        #     :return:
        #     """
        #
        #     var = ['air.2m']
        #     shortName = [ 'air']
        #
        #     for i_var in var:
        #         a = xr.open_dataset(r"D:\fire\20c/%s.mon.mean.nc"%i_var)
        #         print(a)
        #         m_ranged = get_wind(a, year, i_month)
        #         print(m_ranged)
        #         da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        #         m_ranged = da.groupby("year_month").mean("time").sel(lon=slice(97, 125))
        #         print(m_ranged)
        #         m_ranged = m_ranged['air']
        #         lat = m_ranged.lat.values
        #         loc54 = np.argmin(abs(lat - 53.67))
        #         print(lat[loc54])
        #         loc45 = np.argmin(abs(lat - 46.76))
        #
        #         print(lat[loc45])
        #         m_ranged_north = m_ranged.isel(lat=loc54).mean(dim='lon').values
        #         m_ranged_south = m_ranged.isel(lat=loc45).mean(dim='lon').values
        #         tem_grad = (m_ranged_south - m_ranged_north) / 7.45935
        #
        #     exit()
        #     return tem_grad

        a1 = get_EKF400_byYear(np.arange(date_begin,1899+1))
        a2 = get_EKF400_byYear(np.arange(1900,2003+1))
        a3 = get_EKF400_byYear(np.arange(date_begin,2003+1))

        savemat.append([a1,a2, a3])

# fig, ax = plt.subplots()
# for iI, [a1, a2, a3] in enumerate(savemat):
#
#     ax.boxplot(a1, positions=[1+3*iI], showmeans=True, meanline=True)
#     ax.boxplot(a2, positions=[2+3*iI], showmeans=True, meanline=True)
#     ax.boxplot(a3, positions=[3+3*iI], showmeans=True, meanline=True)
#     # ax.set_ylim([0.1,1.2])
#     ax.set_title('%i-2003'%date_begin)
#     # # ax.axhline(line[0])
# plt.show()

result = {}
import scipy.io as scio
a1, a2, a3 = savemat[0]
b1,b2,b3 = savemat[1]

result['s6a_1'] = a1
result['s6a_2'] = a2
result['s6a_3'] = a3
result['s6a_4'] = b1
result['s6a_5'] = b2
result['s6a_6'] = b3

a1, a2, a3 = savemat[2]
b1,b2,b3 = savemat[3]
result['s6b_1'] = a1
result['s6b_2'] = a2
result['s6b_3'] = a3
result['s6b_4'] = b1
result['s6b_5'] = b2
result['s6b_6'] = b3

scio.savemat('figureS6.mat', result)






exit()