import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

type_era5_or_20c = '20c'

if type_era5_or_20c == 'era5':
    date_extre_high_freq_year = [1987, 2003]
    date_high_freq_year =[1997, 2008 ]
    date_low_freq_year =[1984, 1990, 2013]

elif type_era5_or_20c == '20c':
    date_extre_high_freq_year = [1911,1922,1943,1987, 2003] # high2
    date_combine_high_freq_year = [1943, 2003,1911,1987,1922,1944,1976,1913,1912,1965] # high1
    date_high_freq_year = [1912,1913,1933,1944,1965,1976, 2008] # high12
    date_low_freq_year  =           [1908, 1910, 1941, 1955, 1960, 1970, 1971, 1984, 1990]
    date_base = np.arange(1900,2011)

####################################### plot TEM in given time  ###################################


colors = ['red', 'blue', 'black'][::-1]
#
wind = xr.open_dataset(r'D:\OneDrive\basis\some_projects\t2m_20c.nc')
vari = 'air'
label = 'TEM (K)'
legends = ['35-45°N', '45-55°N', '65-85°N']

####### 改动下面的范围！！！
# wind = xr.open_dataset(r'D:\OneDrive\basis\some_projects\wind_20c.nc')
# vari = 'wind'
# label = 'Wind speed (200hPa)'
# legends = ['35-45°N', '45-65°N', '65-85°N']

for month_used in [[1,2,3],[2,3],[4,5]]:
    for type_high in ['high1']:
        def get_wind(wind_year):
            wind_inYear = wind.sel(time=np.in1d(wind['time.year'],wind_year))
            index_GHGS_accum = []

            if type_era5_or_20c == 'era5':
                index_GHGS_accum.append(wind_inYear.sel(lat=slice(45,35), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['time', 'lat'])[vari].values)
                index_GHGS_accum.append(wind_inYear.sel(lat=slice(65,45), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['time', 'lat'])[vari].values)
                index_GHGS_accum.append(wind_inYear.sel(lat=slice(85,65), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['time', 'lat'])[vari].values)
            elif type_era5_or_20c == '20c':
                index_GHGS_accum.append(wind_inYear.sel(lat=slice(35,45), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['time', 'lat'])[vari].values)
                index_GHGS_accum.append(wind_inYear.sel(lat=slice(45,55), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['time', 'lat'])[vari].values)
                index_GHGS_accum.append(wind_inYear.sel(lat=slice(65,85), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['time', 'lat'])[vari].values)

            return np.array(index_GHGS_accum)

        if type_high == 'high2':
            date_high = date_extre_high_freq_year
        elif type_high == 'high12':
            date_high = date_high_freq_year
        elif type_high == 'high1':
            date_high = date_combine_high_freq_year
        elif type_high == 'low':
            date_high = date_low_freq_year
        high = get_wind(date_high)
        low = get_wind(date_base)
        index_GHGS_accum = high-low

        fig, ax = plt.subplots(1,1)

        for i in range(3):
            ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
        plt.legend()
        for vline in [100, 110,125]:
            ax.axvline(vline, color='k', linestyle='--', linewidth=1)

        ax.axhline(0, color='k', linewidth=1)
        Month = ''
        for imonth in month_used:
            Month = Month+str(imonth)+','
        ax.set_title('%s - Climately in Month %s (%s) 20C'%(type_high, Month[:-1], label))
        ax.set_xlabel('Longitude')

        # plt.show()
        plt.savefig('%s - Climately in Month %s (%s) 20C'%(type_high, Month[:-1], label), dpi=600)
        plt.close()


