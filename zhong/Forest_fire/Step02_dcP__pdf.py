import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

date_year_s = 1979
date_year_e = 2015
date_high_freq_year =[1997, 2008 ]
date_extre_high_freq_year = [1987, 2003]
date_low_freq_year =[1984,1990, 2013]

path_Wind = r'/home/linhaozhong/work/Data/ERA5/pressure_level/'


################################# create date based on years  #################################

def date_ranges(years):
    dates = []
    for iyear in years:
        date_s = '%s0401'%iyear; date_e = '%s0531'%iyear
        sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
        dates.extend(sdate)
    return dates

years = np.arange(date_year_s, date_year_e+1)
dates_all = date_ranges(years)



################################## wind data ##############################################

# z500 = []
# for time in range(len(dates_all)):
#     print(xr.open_dataset(path_Wind+'Hgt/'+dates_all[time][:4]+'/Hgt_%s.nc'%dates_all[time]).sel(level=500))
#     hgt = xr.open_dataset(path_Wind+'Hgt/'+dates_all[time][:4]+'/Hgt_%s.nc'%dates_all[time]).sel(level=500).mean(dim='time')['z'].values
#     z500.append(hgt/9.8)
# z500 =np.array(z500, dtype='double')
# z500 = np.transpose(z500, [1,2,0])
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/z500.mat', {'z500':z500[::2, ::2, ]})
# sys.exit(0)




# windS = []
#
# for time in range(len(dates_all)):
#     u = xr.open_dataset(path_Wind+'Uwnd/'+dates_all[time][:4]+'/Uwnd_%s.nc'%dates_all[time]).sel(level=200, latitude=slice(60,42), longitude=slice(60,130)).mean()['u'].values
#     v = xr.open_dataset(path_Wind+'Vwnd/1/'+dates_all[time][:4]+'/Vwnd_%s.nc'%dates_all[time]).sel(level=200, latitude=slice(60,42), longitude=slice(60,130)).mean()['v'].values
#
#     # u = xr.open_dataset(path_Wind+'Uwnd/'+dates_all[time][:4]+'/Uwnd_%s.nc'%dates_all[time]).sel(level=200, latitude=slice(54,40), longitude=slice(119,140)).mean()['u'].values
#     # v = xr.open_dataset(path_Wind+'Vwnd/1/'+dates_all[time][:4]+'/Vwnd_%s.nc'%dates_all[time]).sel(level=200, latitude=slice(54,40), longitude=slice(119,140)).mean()['v'].values
#
#
#     wind = np.sqrt(u**2+v**2)
#     windS.append(wind)
# wind =np.array(windS, dtype='double')
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/wind_1979.mat', {'wind':windS})

# sys.exit(0)

# tS = []
#
# for time in range(len(dates_all)):
#     t = xr.open_dataset('/home/linhaozhong/work/Data/ERA5/single_level/2m_temperature/'+'/2m_temperature%s.nc'%dates_all[time]).sel(latitude=slice(45,35), longitude=slice(100,115)).mean()['t2m'].values
#     tS.append(t)
# wind =np.array(tS, dtype='double')
# scio.savemat('/home/linhaozhong/work/Data/ERA5/single_level/tem.mat', {'tem':tS})
# sys.exit(0)


################  for average along longitude ################
# tS = []
#
# for time in range(len(dates_all)):
#     t = xr.open_dataset('/home/linhaozhong/work/Data/ERA5/single_level/2m_temperature/'+'/2m_temperature%s.nc'%dates_all[time])
#     if time == 0:
#         t5add = t.sel(latitude=slice(45,35)).mean(dim=['time', 'latitude'])['t2m'].values/len(dates_all)
#         t5 = t.sel(latitude=slice(65,55)).mean(dim=['time', 'latitude'])['t2m'].values/len(dates_all)
#         t5div = t.sel(latitude=slice(85,65)).mean(dim=['time', 'latitude'])['t2m'].values/len(dates_all)
#     else:
#         t5add += t.sel(latitude=slice(45,35)).mean(dim=['time', 'latitude'])['t2m'].values/len(dates_all)
#         t5 += t.sel(latitude=slice(65,55)).mean(dim=['time', 'latitude'])['t2m'].values/len(dates_all)
#         t5div += t.sel(latitude=slice(85,65)).mean(dim=['time', 'latitude'])['t2m'].values/len(dates_all)
#
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/tem_longitude.mat', {'t5add':t5add, 't5':t5, 't5div':t5div})
# sys.exit(0)


####################################### plot wind in given time  ###################################

index_wind1 = scio.loadmat('wind.mat')['wind'][0]
index_wind2 = scio.loadmat('wind2.mat')['wind'][0]

index_wind = np.append(index_wind1, index_wind2)

from scipy import stats
index_wind = stats.zscore(index_wind)

####### get different month index ##########
dates_m = np.array([int(i[5]) for i in dates_all])
loc_m = []
for iloc in [3,4,5,6]:
    loc_m.append(np.where(dates_m==iloc))

####### get high, low frequency  year index ##########
dates_y = np.array([int(i[:4]) for i in dates_all])
loc_y = []
for iyear in [date_extre_high_freq_year, date_high_freq_year, date_low_freq_year]:
    loc_sy = []
    for i in iyear:
        loc_sy.append([np.where(dates_y==i)[0][0], np.where(dates_y==i)[0][-1]])
    loc_y.append(loc_sy)
print(loc_y)
############### plot ##################
fig, ax = plt.subplots(1,1, figsize=[12,3])
plt.subplots_adjust(top=0.88,
                    bottom=0.25,
                    left=0.03,
                    right=0.85,
                    hspace=0.2,
                    wspace=0.2)
# plt.contourf(range(len(dates_all)), [5], [index_wind])
# plt.show()
labels = ['Mar.','Apr.','May.','Jun.']
for iloc in range(4):
    ax.bar(loc_m[iloc][0], index_wind[loc_m[iloc]], label=labels[iloc])
# for iloc in range(date_year_e+1-date_year_s):
#     ax.axvline()
colors = ['red', 'yellow', 'blue', 'black']
labels = ['extreme high', 'high', 'low']
for iyear in range(3):
    color = colors[iyear]
    label_button = True
    for isyear in loc_y[iyear]:
        if label_button:
            ax.axvspan(isyear[0], isyear[1], facecolor=color, alpha=0.2, label=labels[iyear])
            label_button = False
        else:
            ax.axvspan(isyear[0], isyear[1], facecolor=color, alpha=0.2)
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
ax.set_title('Wind')
ax.set_xticks(np.arange(len(dates_all))[::93])
ax.set_xticklabels(dates_all[::93])
plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")
ax.grid()
plt.show()
plt.savefig('wind.png', dpi=900)
plt.close()

####################################### plot tem in given time  ###################################

# index_wind = scio.loadmat('tem.mat')['tem'][0]
# print(index_wind.shape)
# print(len(dates_all))
#
# # from scipy import stats
# # index_wind = stats.zscore(index_wind)
#
# ####### get different month index ##########
# dates_m = np.array([int(i[5]) for i in dates_all])
# loc_m = []
# for iloc in [3,4,5,6]:
#     loc_m.extend(np.where(dates_m==iloc))
#
# print(loc_m)
# ####### get high, low frequency  year index ##########
# dates_y = np.array([int(i[:4]) for i in dates_all])
# loc_y = []
# for iyear in [date_extre_high_freq_year, date_high_freq_year, date_low_freq_year]:
#     loc_sy = []
#     for i in iyear:
#         loc_sy.extend(np.where(dates_y==i)[0])
#     loc_y.append(loc_sy)
#
# loc_45 = np.append(loc_m[1], loc_m[2])
#
#
# ############### pdf ###################
# from numpy import mean
# from numpy import std
# from scipy.stats import norm
#
#
# labels = ['extreme high', 'high', 'low']
# colors = ['red', 'yellow', 'blue', 'black']
#
# fig, ax = plt.subplots(1,1)
#
# data_df = pd.DataFrame()
# for i in range(3):
#     import seaborn as sns
#     from scipy import stats
#     from scipy.stats import norm
#     loc_my = np.intersect1d(loc_45, loc_y[i])
#     print(np.array(dates_all)[loc_my])
#     x, y  = sns.distplot(index_wind[loc_my], fit=norm, kde=False, hist=False, ax=ax, fit_kws={'label':labels[i], 'color':colors[i]})
#
#     data_df['%s_x'%labels[i]] = x
#     data_df['%s_y'%labels[i]] = y
#
# data_df.to_csv('tem.csv')
# plt.ylabel('PDF')
# plt.xlabel('tem (k)')
# plt.legend()
# plt.title('PDF of tem')
# plt.show()
# plt.savefig('pdf_tem.png', dpi=600)
# plt.close()


################################ plot GHGS ##################################################
#
index_wind = scio.loadmat(r'D:\OneDrive\a_matlab\Blocking\BI_m123.mat')['BI']['GHGS'][0]


# from scipy import stats
# index_wind = stats.zscore(index_wind)

####### get different month index ##########
dates_m = np.array([int(i[5]) for i in dates_all])
loc_m = []
for iloc in [1,2,3]:
    loc_m.extend(np.where(dates_m==iloc))


####### get high, low frequency  year index ##########
dates_y = np.array([int(i[:4]) for i in dates_all])
loc_y = []
for iyear in [date_extre_high_freq_year, date_high_freq_year, date_low_freq_year]:
    loc_sy = []
    for i in iyear:
        loc_sy.extend(np.where(dates_y==i)[0])
    loc_y.append(loc_sy)

loc_45 = np.append(loc_m[1], loc_m[2])


############### pdf ###################
from numpy import mean
from numpy import std
from scipy.stats import norm


labels = ['extreme high', 'high', 'low']
colors = ['red', 'yellow', 'blue', 'black']

fig, ax = plt.subplots(1,1)

data_df = pd.DataFrame()
for i in range(3):
    import seaborn as sns
    from scipy import stats
    from scipy.stats import norm
    loc_my = np.intersect1d(loc_45, loc_y[i])
    print(np.array(dates_all)[loc_my])

    index_wind_use_here = np.array([i[100:125,1] for i in index_wind[loc_my]]).flatten()
    x, y  = sns.distplot(index_wind_use_here, fit=norm, kde=False, hist=False, ax=ax, fit_kws={'label':labels[i], 'color':colors[i]})

    data_df['%s_x'%labels[i]] = x
    data_df['%s_y'%labels[i]] = y

data_df.to_csv('GHGS.csv')
plt.ylabel('PDF')
plt.xlabel('GHGS')
plt.legend()
plt.title('PDF of GHGS between 60-130°E')
plt.show()
plt.savefig('pdf_GHGS.png', dpi=600)
plt.close()
