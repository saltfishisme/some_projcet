import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

date_high_freq_year =[1997, 2008 ]
date_extre_high_freq_year = [1987, 2003]
date_low_freq_year =[1984, 1990, 2013]
def get_pre_data_with_freqType(freqType):
    # file_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/plot/'
    file_Path = r'D:\OneDrive\a_matlab\Blocking/'

    index_BI = scio.loadmat(file_Path+r'BI_%s.mat'%freqType)['BI']

    if freqType == 'high':
        years = [1987,2003]
    else:
        years = [1984,1990,2013]

    varName = index_BI.dtype.names


    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s0101'%iyear; date_e = '%s0331'%iyear
            sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
            dates.extend(sdate)
        return dates
    dates_all = date_ranges(years)

    return index_BI, dates_all


def get_pre_data_with_all_year(data):
    '''
    add a datetimeIndex to data
    :param data: ( number * TimeIndex_length), number at least 1
    :return: data with DatetimeIndex
    '''
    date_year_s = 1979
    date_year_e = 2015

    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s1201'%iyear; date_e = '%s1231'%iyear
            sdate = pd.date_range(date_s, date_e, freq='1D')
            dates.extend(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e+1)
    dates_all = date_ranges(years)

    data_TimeIndex = pd.DataFrame(index=dates_all)
    for i, idata in enumerate(data):
        data_TimeIndex['%i'%i] = idata
    return data_TimeIndex

####################################### plot TM in all year ###################################
# import numpy as np
# import matplotlib
# import matplotlib.pyplot as plt
#
# date_P = dates_all
# lon_P = np.arange(0,360)
#
# index_TMs = []
# for time in range(len(dates_all)):
#     index_TM = index_BI['TM'][0][time][0]
#     index_TMs.append(index_TM)
# index_TMs = np.array(index_TMs)
#
# fig, ax = plt.subplots()
# im = ax.imshow(index_TMs)
#
#
# ax.set_yticks(np.arange(len(date_P))[::31])
#
# ax.set_yticklabels(date_P[::31])
# for vline in [100, 110,125]:
#     ax.axvline(vline, color='lightyellow', linestyle='--', linewidth=1)
# ax.set_title('%s-frequency year (TM)'%(freqType))
# ax.set_xlabel('Longitude')
#
# fig.tight_layout()
# plt.savefig('all_year_%s_frequency.png'%freqType, dpi=600)
# plt.close()


####################################### plot TM in given time  ###################################


def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
    data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]
    return np.sum(np.array(data_MandY))

file_Path = r'D:\OneDrive\a_matlab\Blocking/'

data = scio.loadmat(file_Path+r'BI_m12.mat',mat_dtype=True)['BI']['TM'][0]
index_GHGS = get_pre_data_with_all_year([data])


for time_given in [[12]]:
    index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_extre_high_freq_year)-1)
    index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_low_freq_year)-1)
    index_GHGS_accum = np.array(index_GHGS_accum_high[0]) - np.array(index_GHGS_accum_low[0])
    fig, ax = plt.subplots(1,1)

    ax.plot(range(360), index_GHGS_accum)

    for vline in [100, 110,125]:
        ax.axvline(vline, color='k', linestyle='--', linewidth=1)
    ax.axhline(0, color='k', linewidth=1)
    Month = ''
    for imonth in time_given:
        Month = Month+str(imonth)+','
    ax.set_title('difference (TM in Month %s)'%(Month[:-1]))
    ax.set_xlabel('Longitude')

    # plt.show()
    plt.savefig('difference_TM_M_%s.png'%(Month[:-1]), dpi=600)
    plt.close()

# exit(0)

####################################### plot GHGS in given time  ###################################
colors = ['red', 'blue', 'black']
legends = ['+5', '0', '-5']

def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
    data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]

    return np.transpose(np.mean(np.array(data_MandY)), [1,0])

file_Path = r'D:\OneDrive\a_matlab\Blocking/'

data = scio.loadmat(file_Path+r'BI_m12.mat',mat_dtype=True)['BI']['GHGS'][0]

index_GHGS = get_pre_data_with_all_year([data])


for time_given in [[12]]:
    index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_extre_high_freq_year)-1)
    index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_low_freq_year)-1)

    index_GHGS_accum = (index_GHGS_accum_high - index_GHGS_accum_low)
    fig, ax = plt.subplots(1,1)

    for i in range(3):
        ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
    plt.legend()
    for vline in [100, 110,125]:
        ax.axvline(vline, color='k', linestyle='--', linewidth=1)
    ax.axhline(0, color='k', linewidth=1)
    Month = ''
    for imonth in time_given:
        Month = Month+str(imonth)+','
    ax.set_title('difference (GHGS in Month %s)'%(Month[:-1]))
    ax.set_xlabel('Longitude')

    # plt.show()
    plt.savefig('difference_GHGS_M_%s.png'%(Month[:-1]), dpi=600)
    plt.close()

# exit(0)


####################################### plot TEM in given time  ###################################


colors = ['red', 'blue', 'black'][::-1]
legends = ['35-45°N', '45-65°N', '65-85°N']


def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
    data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]
    print(data_MandY)
    return np.transpose(np.mean(np.array(data_MandY)), [1,0])




a = scio.loadmat('tem_longitude_m12.mat', mat_dtype=True)
t5_add = a['t5add']; t5 = a['t5']; t5div = a['t5div']
index_t5s_high = np.transpose(np.array([t5_add, t5, t5div]), [1,2,0])

index_t5s = []
for i in range(index_t5s_high.shape[0]):
    index_t5s.append(np.array(index_t5s_high[i]))
index_GHGS = get_pre_data_with_all_year([index_t5s])

for time_given in [[12]]:
    index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_extre_high_freq_year)-1)
    index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_low_freq_year)-1)

    index_GHGS_accum = (index_GHGS_accum_high - index_GHGS_accum_low)
    fig, ax = plt.subplots(1,1)

    for i in range(3):
        ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
    plt.legend()
    for vline in [100, 110,125]:
        ax.axvline(vline, color='k', linestyle='--', linewidth=1)
    ax.axhline(0, color='k', linewidth=1)
    Month = ''
    for imonth in time_given:
        Month = Month+str(imonth)+','
    ax.set_title('difference (TEM in Month %s)'%(Month[:-1]))
    ax.set_xlabel('Longitude')

    # plt.show()
    plt.savefig('difference_TEM_M_%s.png'%(Month[:-1]), dpi=600)
    plt.close()

exit(0)

