import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

'''
1, get year-month-day DatetimeIndex List, param: month1, month2, 1979-2015
2, load data from era5, and average into daily scale
3, saved as nc file (except hgt, which will be saved as mat to calculate BI and GHGS 
'''


month1 = '0101'
month2 = '0531'

date_year_s = 1979
date_year_e = 2015

def create_nc(data, vari, path_save="/home/linhaozhong/work/Data/ERA5/pressure_level/plot/"):
    import xarray as xr
    import numpy as np
    import pandas as pd

    lon = np.arange(0, 360)
    lat = np.arange(90, -91, -1)

    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s%s' % (iyear, month1);
            date_e = '%s%s' % (iyear, month2)
            sdate = pd.date_range(date_s, date_e, freq='1D')
            if iyear == years[0]:
                dates = sdate
            else:
                dates = dates.append(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e + 1)
    times = date_ranges(years)

    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            vari: get_dataarray(data),

        }
    )
    print(ds)
    ds.to_netcdf(path_save + '%s_era5.nc' % vari)


################################# create date based on years  #################################
''' step '''


def date_ranges(years):
    dates = []
    for iyear in years:
        # $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ #
        date_s = '%s%s' % (iyear, month1);
        date_e = '%s%s' % (iyear, month2)
        sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
        dates.extend(sdate)
    return dates


years = np.arange(date_year_s, date_year_e + 1)
dates_all = date_ranges(years)

################################## tem data ##############################################


t5 = []

for time in range(len(dates_all)):
    t = xr.open_dataset(
        '/home/linhaozhong/work/Data/ERA5/single_level/2m_temperature/' + '/2m_temperature%s.nc' % dates_all[time])

    t5.append(t.mean(dim='time')['t2m'].values)

create_nc(t5, 'tem', '/home/linhaozhong/work/Data/ERA5/single_level/')
# scio.savemat('/home/linhaozhong/work/Data/ERA5/single_level/tem_longitude_m12345.mat', {'t5add':np.array(t5add, dtype='double'), 't5':np.array(t5, dtype='double'), 't5div':np.array(t5div, dtype='double')})
sys.exit(0)


################################## wind data ##############################################


################ ERA5
windS = []
for time in range(len(dates_all)):
    if dates_all[time][:4] == dates_all[0][:4]:
        u = xr.open_dataset(path_Wind + 'Uwnd/' + dates_all[time][:4] + '/Uwnd_%s.nc' % dates_all[time]).sel(
            level=200).mean(dim=['time'])['u'].values
        v = xr.open_dataset(path_Wind + 'Vwnd/1/' + dates_all[time][:4] + '/Vwnd_%s.nc' % dates_all[time]).sel(
            level=200).mean(dim=['time'])['v'].values

    else:
        u = xr.open_dataset(path_Wind + 'Uwnd/' + dates_all[time][:4] + '/Uwnd_%s.nc' % dates_all[time]).sel(
            level=200).mean(dim=['time'])['u'].values
        v = xr.open_dataset(path_Wind + 'Vwnd/' + dates_all[time][:4] + '/Vwnd_%s.nc' % dates_all[time]).sel(
            level=200).mean(dim=['time'])['v'].values
    if u.shape[0] == 361:
        u = u[::2, ::2]
    if v.shape[0] == 361:
        v = v[::2, ::2]
    wind = np.sqrt(u ** 2 + v ** 2)
    windS.append(wind)
wind = np.array(windS, dtype='double')
create_nc(wind, 'wind')
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/wind_era5_12.mat', {'wind':wind})
exit(0)


################################## hgt data  for BI calculation ##############################################

z500 = []
for time in range(len(dates_all)):
    hgt = \
    xr.open_dataset(path_Wind + 'Hgt/' + dates_all[time][:4] + '/Hgt_%s.nc' % dates_all[time]).sel(level=500).mean(
        dim='time')['z'].values
    z500.append(hgt / 9.8)
z500 = np.array(z500, dtype='double')
z500 = np.transpose(z500, [1, 2, 0])
scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/z500_m12345.mat', {'z500': z500[::2, ::2, ]})
sys.exit(0)


# dont know how to do, may be for average!
################  for average along longitude ################
# tS = []
# print(len(dates_all))
# for time in range(len(dates_all)):
#     t = xr.open_dataset(
#         '/home/linhaozhong/work/Data/ERA5/single_level/2m_temperature/' + '/2m_temperature%s.nc' % dates_all[time])
#     if time == 0:
#         print(type(t.sel(latitude=slice(45, 35)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')[0]))
#         t5add = t.sel(latitude=slice(45, 35)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')
#         t5 = t.sel(latitude=slice(65, 55)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')
#         t5div = t.sel(latitude=slice(85, 65)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')
#     else:
#         t5add += t.sel(latitude=slice(45, 35)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')
#         t5 += t.sel(latitude=slice(65, 55)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')
#         t5div += t.sel(latitude=slice(85, 65)).mean(dim=['time', 'latitude'])['t2m'].values.astype('float64')
#
# scio.savemat('/home/linhaozhong/work/Data/ERA5/single_level/tem_longitude%s_m123.mat' % freqType,
#              {'t5add': np.array(t5add / len(dates_all), dtype='double'),
#               't5': np.array(t5 / len(dates_all), dtype='double'),
#               't5div': np.array(t5div / len(dates_all), dtype='double')})
# sys.exit(0)
