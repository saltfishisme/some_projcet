import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

freqType = 'difference'

type_era5_or_20c = '20c'



####################  load BI file ########
pathIndex_BI = '20c'

month1 = '0101'
month2 = '0531'

date_year_s = 1900
date_year_e = 2015

year_add = 0


date_combine_high_freq_year = [1943, 2003,1911,1987,1922,1944,1976,1913,1912,1965] # high1
date_low_freq_year  =           [1975,1971,1970,1941,1990,1910,1984,1955,1908,1960]
date_base = np.arange(1900,2010)

def get_pre_data_with_all_year(data):
    '''
    add a datetimeIndex to data
    :param data: ( number * TimeIndex_length), number at least 1
    :return: data with DatetimeIndex
    '''
    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s%s'%(iyear, month1); date_e = '%s%s'%(iyear, month2)
            sdate = pd.date_range(date_s, date_e, freq='1D')
            dates.extend(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e+1)
    dates_all = date_ranges(years)

    data_TimeIndex = pd.DataFrame(index=dates_all)
    for i, idata in enumerate(data):
        data_TimeIndex['%i'%i] = idata
    return data_TimeIndex
def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
    year = year + year_add
    data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]
    return data_MandY

def nc_anomaly():
    da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc').sel(lat=60)
    da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
    result = da.groupby("year_month") - da.groupby("year_month").mean("time")
    return result

longitude_used = [90,120]

month_use = [[4], [5]]

for bu_max_min in [   'East',    'West', 'centre']:
    for type_high in [ 'high1', 'low']: #  'high1';
        for month_given in month_use:
            if type_high == 'high1':
                date_use_year = date_combine_high_freq_year
            elif type_high == 'low':
                date_use_year = date_low_freq_year
            #  取值
            file_Path = r'D:\OneDrive\a_matlab\Blocking/'
            data = scio.loadmat(file_Path+r'BI_%s.mat'%pathIndex_BI,mat_dtype=True)
            index_TM = get_pre_data_with_all_year([data['BI']['TM'][0]])
            index_GHGS = get_pre_data_with_all_year([data['BI']['GHGS'][0]])

            index_TM = get_data_dateIndex_in_MandY_and_mean(index_TM, month_given, np.array(date_use_year))
            index_GHGS = get_data_dateIndex_in_MandY_and_mean(index_GHGS, month_given, np.array(date_use_year))
            index_GHGS_now = np.array([si[0][:,-1] for si in index_GHGS.values])


            label_time = index_TM.index
            index_TM = np.array([i[0][0] for i in index_TM.values])

    #%% find blocking last 5 longitude and 3 days
            def running_mean_uniform_filter1d(x, N):
                from scipy import ndimage
                return ndimage.uniform_filter(x, N, mode='constant', origin=-1)[:-(N[0]-1), :-(N[1]-1)]

            index_TM_filter = running_mean_uniform_filter1d(index_TM, [3, 5])
            argindex = np.argwhere(index_TM_filter==1)


            index_TM_time = []
            index_TM_loc = []

            from skimage import measure
            import numpy as np
            import cv2
            thresh = cv2.threshold(index_TM, 0.5, 255, cv2.THRESH_BINARY)[1]
            labels = measure.label(thresh, connectivity=1, background=0)
            mask = np.zeros(thresh.shape, dtype="uint8")
            for label in np.unique(labels):
                if label == 0:
                    continue
                labelMask = np.zeros(thresh.shape, dtype="uint8")
                labelMask[labels == label] = 255
                arg_now = np.argwhere(labelMask==255)

                if ((arg_now[:,None] == argindex).all(2).any(1)).any():

                    a = np.ones(index_GHGS_now.shape)*-999
                    a[labels == label] = index_GHGS_now[labels == label]
                    max_TM = np.unravel_index(a.argmax(), a.shape)
                    index_TM_time.append(max_TM[0])

                    s_argwhere = np.argwhere(labels==label)

#%%

                    if np.isin(s_argwhere[:,1], np.arange(longitude_used[0], longitude_used[1]+1)).any():
                        s_max = np.max(s_argwhere[:,1]); s_min = np.min(s_argwhere[:,1])
                        s_centre = int((s_max-s_min)/2) +np.min(s_argwhere[:,1])
                        print(s_centre)
                        print('i am here')
                        if bu_max_min ==    'East':
                            if (s_centre>longitude_used[1]):
                                print('?')
                                index_TM_loc.append(s_centre)
                            else:
                                index_TM_loc.append(np.nan)

                        elif bu_max_min ==    'West':
                            if (s_centre<longitude_used[0]):
                                index_TM_loc.append(s_centre)
                            else:
                                index_TM_loc.append(np.nan)

                        elif bu_max_min == 'centre':
                            if (s_centre>=longitude_used[0]) & (s_centre<=longitude_used[1]):
                                index_TM_loc.append(s_centre)
                            else:
                                index_TM_loc.append(np.nan)

                    else:
                        index_TM_loc.append(np.nan)

#%%
                    mask = cv2.add(mask, labelMask)
            mask[mask==255] = 1

    #%%
            # plt.imshow(mask)
            # plt.xlabel('Longitude')
            # plt.title('TM index in 60°N')
            # ax = plt.gca()
            # ax.set_yticks(np.arange(len(label_time))[::30])
            # ax.set_yticklabels(label_time.strftime('%Y-%m-%d')[::30])
            # plt.show()
            # index_TM_time =[72, 74, 81, YY_identification.py, 128, 129, 130, 131, 132, 133, 155, 156, 211, 212, 213, 214, 283, 284, 285]
            # hgt = nc_anomaly()


            result = []
            # now = hgt['hgt500'].values

            now = index_GHGS_now
            gHGS_21 = []

            trys = []

            if bu_max_min ==    'East':
                sub = 60; add=61
            if bu_max_min ==    'West':
                sub = 60; add=61
            if bu_max_min == 'centre':
                sub = 60; add=61
            for s_i, idate in enumerate(index_TM_time):

                if (now[idate-10:idate+11, :].shape[0] == 21) & (np.isnan(index_TM_loc[s_i])==False):
                    print(index_TM_loc[s_i])
                    trys.append(index_TM_loc[s_i])
                    gHGS_21.append(now[idate-10:idate+11, index_TM_loc[s_i]-sub:index_TM_loc[s_i]+add])
            if len(gHGS_21) == 0:
                continue
            result = np.nanmean(gHGS_21, axis=0)
            print(result.shape)
            index_mean_longitude = int(np.mean(trys))
            print('mean', index_mean_longitude)

    #%%
            legends = ['65°N-45°N', '60°N-40°N', '55°N-35°N']
            fig, ax = plt.subplots(1,1, figsize=[4,4])
            import matplotlib

            SMALL_SIZE = 7
            matplotlib.rc('font', size=SMALL_SIZE)
            matplotlib.rc('axes', titlesize=SMALL_SIZE)
            plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
            plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
            plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
            plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
            plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
            plt.subplots_adjust(top=0.94,
                                bottom=0.19,
                                left=0.12,
                                right=0.95,
                                hspace=0.645,
                                wspace=0.21)
            import matplotlib.pyplot as plt
            import cmaps
            import matplotlib.colors as colors


            cb = ax.contourf(range(result.shape[1]), range(0, result.shape[0]), result,
                             cmap=cmaps.cmp_b2r,extend='both')

            Month = ''
            for imonth in month_given:
                Month = Month+str(imonth)+','

            ax.set_title('%s M%s  GHGS %s' %(type_high, Month[:-1], bu_max_min ))
            ax.set_ylabel('day', fontsize=7)

            ax.set_yticks(np.arange(0,21)[::2])
            ax.set_yticklabels(np.arange(-10,11,1)[::2], fontsize=7)

            ax.set_xticks(np.arange(result.shape[1])[::20])
            ax.set_xticklabels(np.arange(index_mean_longitude-sub,index_mean_longitude+add,1)[::20], fontsize=7)
            for vline in [100, 110,125]:
                ax.axvline(vline-index_mean_longitude+sub, color='k', linestyle='--', linewidth=1)

            # ax.set_xlim(longitude_used[0]-20, longitude_used[1]+20)
            for label in (ax.get_xticklabels() + ax.get_yticklabels()):
                label.set_fontsize(7)
            ax.set_xlabel('Longitude', fontsize=7)
            cb_ax = fig.add_axes([0.1, 0.05, 0.8, 0.02])
            cbar = fig.colorbar(cb, cax=cb_ax,  extend='both', orientation='horizontal')

            plt.rcParams.update({'font.size': 7})

            # plt.show()
            plt.savefig('%s M%s  GHGS %s.png' %(type_high, Month[:-1], bu_max_min), dpi=800)
            plt.close()
