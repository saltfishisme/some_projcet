import pandas as pd
import numpy as np
import scipy.io
import xarray as xr
import os
import pandas as pd
import numpy as np
import xarray as xr
import scipy.io as scio
from metpy.interpolate import cross_section
import nc4

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import matplotlib.colors as colors
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
import nc4

def plot_linregress_som3(trend, filename, p_values=None):
    fig = plt.figure(figsize=[5,3.5])
    plt.subplots_adjust(top=0.91,
                        bottom=0.15,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

    trend[trend == 0] = np.nan

    # trend[trend > 1.8] = 1.8
    import matplotlib.colors as colors

    if p_values is not None:
        p_values[np.abs(trend) < 0.01] = np.nan
    # trend[np.abs(trend) < 0.01] = np.nan


    # divnorm = colors.TwoSlopeNorm(vmin=-0.6, vcenter=0, vmax=0.6)

    if filename == 'May':
        cb = ax.contourf(lon, lat, trend
                         , cmap=cmap_here,
                         extend='both',
                         transform=ccrs.PlateCarree(), levels=np.arange(520,600,1))
    else:
        cb = ax.contourf(lon, lat, trend
                         , cmap=cmap_here,
                         extend='both',
                         transform=ccrs.PlateCarree(), levels=np.arange(500,600,5))

    cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal',extend='both')
    # cb_ax.set_xticks(np.arange(-1,1.001,0.5))
    coords = pd.read_csv('Coordinates.csv')
    # coords = coords.loc[coords['lon']<117]
    # coords = coords.loc[coords['lat'] > 51]
    # # coords = coords.
    # coords.to_csv('Coordinates_BaikalRURom.csv',index=False)
    lat_coor = coords['lat']
    lon_coor = coords['lon']

    ax.scatter(lon_coor, lat_coor, s=0.5, c='red', transform=ccrs.PlateCarree())

    if p_values is not None:
        ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                    colors='none', levels=[0, 0.1],
                    hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([50, 150+100, 0, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    ax.set_title(filename)
    # plt.show()
    plt.savefig(r'D:/%s.png'%filename)
    plt.close()
def plot_linregress(trend, filename, p_values=None):
    fig = plt.figure(figsize=[5,3.5])
    plt.subplots_adjust(top=0.91,
                        bottom=0.15,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

    trend[trend == 0] = np.nan

    # trend[trend > 1.8] = 1.8
    import matplotlib.colors as colors

    if p_values is not None:
        p_values[np.abs(trend) < 0.01] = np.nan
    # trend[np.abs(trend) < 0.01] = np.nan


    # divnorm = colors.TwoSlopeNorm(vmin=-0.6, vcenter=0, vmax=0.6)


    cb = ax.contourf(lon, lat, trend
                     , cmap=cmap_here,
                     extend='both',norm=colors.CenteredNorm(),
                     transform=ccrs.PlateCarree())

    cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
    cbar = fig.colorbar(cb, cax=cb_ax, orientation='horizontal',extend='both')
    # cb_ax.set_xticks(np.arange(-1,1.001,0.5))
    coords = pd.read_csv('Coordinates.csv')
    # coords = coords.loc[coords['lon']<117]
    # coords = coords.loc[coords['lat'] > 51]
    # # coords = coords.
    # coords.to_csv('Coordinates_BaikalRURom.csv',index=False)
    lat_coor = coords['lat']
    lon_coor = coords['lon']

    ax.scatter(lon_coor, lat_coor, s=0.5, c='red', transform=ccrs.PlateCarree())

    if p_values is not None:
        ax.contourf(lon, lat, np.ma.masked_greater(p_values, 0.1),

                    colors='none', levels=[0, 0.1],
                    hatches=[5 * '/', 5 * '/'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    # ax.set_extent([50, 150+100, 0, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    ax.set_title(filename)
    # plt.show()
    plt.savefig(r'D:/%s.png'%filename)
    plt.close()

def quiver(u, u_p, v, v_p,filename):
    fig = plt.figure(figsize=[5, 3.5])
    plt.subplots_adjust(top=0.91,
                        bottom=0.15,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)
    # ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=220, central_latitude=60))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

    u[((u_p>0.1)&(v_p > 0.1))] = np.nan
    v[((u_p>0.1)&(v_p > 0.1))] = np.nan

    delta = 3
    delta_lat = 2
    qui = ax.quiver(lon[::delta], lat[::delta_lat],
              u[::delta_lat, ::delta], v[::delta_lat, ::delta],
              transform=ccrs.PlateCarree(), scale=200, zorder=7)
    qk = ax.quiverkey(qui, 1, 1.05, 5, r'$5 \frac{m}{s}$', labelpos='E',
                      coordinates='axes')

    coords = pd.read_csv('Coordinates.csv')
    lat_coor = coords['lat']
    lon_coor = coords['lon']

    ax.scatter(lon_coor, lat_coor, s=0.5, c='red', transform=ccrs.PlateCarree())


    # ax.set_extent([50, 150+100, 0, 90], crs=ccrs.PlateCarree())
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    # ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
    ax.set_title(filename)
    # plt.show()
    plt.savefig(r'D:/%s.png'%filename)
    plt.close()


import cmaps
cmap_here  =cmaps.cmp_b2r
ranges = [30,170,35,80]
lon = np.arange(0,360,1)[30:170+1]
lat = np.arange(-90,90.001,1)[-80:]

lon = np.arange(0,360,1)
lat = np.arange(-90,90.001,1)

mat_collectin = {}

mat_collectin['lat'] = lat
mat_collectin['lon'] = lon
var = 'hgt'
a = nc4.read_nc4(r'D:/%s_ano_std1.64_234'%var)
a_p = nc4.read_nc4(r'D:/%s_p_std1.64_234'%var)
b = nc4.read_nc4(r'D:/%s_ano_std1.64_5'%var)
b_p = nc4.read_nc4(r'D:/%s_p_std1.64_5'%var)

mat_collectin['hgt_ano_m234'] = a
mat_collectin['hgt_ano_m234_p'] = a_p
mat_collectin['hgt_ano_m5'] = b
mat_collectin['hgt_ano_m5_p'] = b_p
# plot_linregress(a[-80:, 30:170 + 1], 'February-April ano std1.64', p_values=a_p[-80:, 30:170 + 1])
# plot_linregress(b[-80:, 30:170 + 1], 'May ano std1.64' , p_values=b_p[-80:, 30:170 + 1])
# exit()

def load_data(var, month):
    a = nc4.read_nc4(r'D:/%s_ano_std1.64_%s'%(var, month))
    a_p = nc4.read_nc4(r'D:/%s_p_std1.64_%s'%(var, month))
    return a, a_p

mat_collectin['uwnd_ano_m234'] = load_data('uwnd', '234')[0]
mat_collectin['uwnd_ano_m234_p'] = load_data('uwnd', '234')[1]
mat_collectin['uwnd_ano_m5'] = load_data('uwnd', '5')[0]
mat_collectin['uwnd_ano_m5_p'] = load_data('uwnd', '5')[1]

mat_collectin['vwnd_ano_m234'] = load_data('vwnd', '234')[0]
mat_collectin['vwnd_ano_m234_p'] = load_data('vwnd', '234')[1]
mat_collectin['vwnd_ano_m5'] = load_data('vwnd', '5')[0]
mat_collectin['vwnd_ano_m5_p'] = load_data('vwnd', '5')[1]

a = nc4.read_nc4(r'D:/std1.64_234')
b = nc4.read_nc4(r'D:/std1.64_5')
a = np.nanmean(a,axis=0)
b = np.nanmean(b,axis=0)
mat_collectin['hgt_m234'] = a*9.8
mat_collectin['hgt_m5'] = b*9.8

scipy.io.savemat(r'daily_circulation_pattern.mat', mat_collectin)

exit()

month = '234'
quiver(load_data('uwnd', month)[0], load_data('uwnd', month)[1],
       load_data('vwnd', month)[0], load_data('vwnd', month)[1],
       'February-April wnd_ano std1.64')

month = '5'
quiver(load_data('uwnd', month)[0], load_data('uwnd', month)[1],
       load_data('vwnd', month)[0], load_data('vwnd', month)[1],
       'May wnd_ano std1.64')
# a = nc4.read_nc4(r'D:/std1.64_234')
# b = nc4.read_nc4(r'D:/std1.64_5')
# a = np.nanmean(a,axis=0)
# b = np.nanmean(b,axis=0)
# plot_linregress_som3(a[-80:, 30:170 + 1] * 0.98, 'February-April std1.64')
# plot_linregress_som3(b[-80:, 30:170 + 1] * 0.98, 'May std1.64' )
exit()
# for i in range(5):
#     a = nc4.read_nc4(r'D:/std1.75_234')[-i]
#     b = nc4.read_nc4(r'D:/std1.75_5')[-i]
#     plot_linregress_som3(a[-80:, 30:170+1]*0.98, 'February-April std1.75 %i'%i)
#     plot_linregress_som3(b[-80:, 30:170 + 1] * 0.98, 'May std1.75 %i'%i)
# exit()


def wnd_20c_1sample(date, var, level, anomaly=False):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    v_present = xr.open_dataset(
        path_PressureData + '%s.%s.nc' % (var,time.strftime('%Y')))
    v_present = v_present.sel(time=time.strftime('%Y-%m-%d'), level=level)[
        var].values

    if anomaly:
        v_mean = xr.open_dataset(r'/home/linhaozhong/work/Data/20_cc/%s.mon.mean.%i.nc'%(var,level))
        v_mean = v_mean.sel(time=np.in1d(v_mean.time.month, [int(time.strftime('%m'))]))[
            var].values

        from scipy import stats
        _,p=stats.ttest_1samp(v_mean, v_present, axis=0)

        return v_present-np.nanmean(v_mean, axis=0), p
    else:
        return v_present


def wnd_20c(date, var, level, anomaly=False):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    v_present = xr.open_dataset(
        path_PressureData + '%s.%s.nc' % (var,time.strftime('%Y')))
    v_present = v_present.sel(time=time.strftime('%Y-%m-%d'), level=level)[
        var].values

    if anomaly:
        print(nc4.read_nc4(r'/home/linhaozhong/work/Data/20_cc/%s.12monthMean'%var).shape)
        v_mean = nc4.read_nc4(r'/home/linhaozhong/work/Data/20_cc/%s.12monthMean'%var)[int(time.strftime('%m'))-1]
        return v_present, v_mean

    else:
        return v_present

# get wnd 20c anomaly
def nc_anomaly_monthly_20c(var, level):
    da = xr.open_dataset(r"/home/linhaozhong/work/Data/20_cc/%s.mon.mean.nc"%var)
    da = da.sel(level=level)
    # da = da.assign_coords(month_day=da.time.dt.strftime("%m"))
    # print(da)
    # result = da.groupby("month_day").mean("time")
    return da

# var = 'uwnd'
# a = nc_anomaly_monthly_20c(var, 850)
# a.to_netcdf( r'/home/linhaozhong/work/Data/20_cc/%s.mon.mean.850.nc'%var)
#
# var = 'vwnd'
# a = nc_anomaly_monthly_20c(var, 850)
# a.to_netcdf( r'/home/linhaozhong/work/Data/20_cc/%s.mon.mean.850.nc'%var)
#
# var = 'hgt'
# a = nc_anomaly_monthly_20c(var, 500)
# a.to_netcdf( r'/home/linhaozhong/work/Data/20_cc/%s.mon.mean.500.nc'%var)
# exit()


pattern_R = scipy.io.loadmat(r"/home/linhaozhong/work/fire/patternCor_rawData_20c.mat")
# pattern_R = scipy.io.loadmat(r"G:\OneDrive\桌面\林火数据重建_cmip6\patternCor_rawData_20c.mat")

high_year = pattern_R['year'][0][pattern_R['fireIndex_zscore'][0]>2]
high_year = high_year[high_year>=1900]
print(high_year)

daily_R = pattern_R['daily_m2345']
year = pattern_R['year'][0]
date = pd.date_range('%i-01-01'%year[0], '%i-12-01'%year[-1], freq='1D')

date = date[np.in1d(date.month, [2,3,4,5])]
daily_R = pd.DataFrame(daily_R, index=date)
daily_R = daily_R.loc[daily_R.index.year.isin([1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943])]
print(daily_R)
# find index higher than 2
daily_R = daily_R.loc[daily_R[0]>1.64]

month_name = ['234','5']
for iI_month, i_month in enumerate([[2,3,4],[5]]):


    daily_R_month = daily_R.loc[daily_R.index.month.isin(i_month)]
    print(daily_R_month.shape)
    # continue
    # plt.figure()
    # plt.bar(np.arange(daily_R_month.shape[0]), daily_R_month[0])  # 'o'代表点
    # ax = plt.gca()
    # if iI_month == 0:
    #     for i_axvline in [89,89*2,89*3,89*4]:
    #         ax.axvline(i_axvline, linewidth=0.5,linestyle='--',color='black')
    # if iI_month == 1:
    #     for i_axvline in [31,31*2,31*3,31*4]:
    #         ax.axvline(i_axvline, linewidth=0.5,linestyle='--',color='black')
    # # 显示图表
    # plt.show()
    # plt.close()
    # continue
    # print(daily_R_month.shape)
    # continue
    daily_R_month = daily_R_month.sort_values(by=0)

    var_param = {'hgt':[r'/home/linhaozhong/work/Data/20C-NOAA/', 500],
                 'uwnd': [r'/home/linhaozhong/work/Data/20_cc/wind/', 850],
                 'vwnd': [r'/home/linhaozhong/work/Data/20_cc/wind/', 850],
                 }

    for i_var in ['hgt', 'uwnd', 'vwnd']:
        var = []
        var_ano = []
        for i_date in daily_R_month.index:
            path_PressureData = var_param[i_var][0]
            i_hgt, i_hgt_ano = wnd_20c(i_date, i_var, var_param[i_var][1], anomaly=True)
            var.append(i_hgt)
            var_ano.append(i_hgt_ano)

        var = np.array(var)
        var_ano = np.array(var_ano)

        from scipy.stats import ttest_ind
        _, p = ttest_ind(var, var_ano, axis=0)

        nc4.save_nc4(np.array(np.nanmean(var-var_ano,axis=0)), r'/home/linhaozhong/work/fire/%s_ano_std1.64_%s'%(i_var, month_name[iI_month]))
        nc4.save_nc4(p, r'/home/linhaozhong/work/fire/%s_p_std1.64_%s'%(i_var, month_name[iI_month]))

exit()
