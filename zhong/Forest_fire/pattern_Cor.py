import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import nc4
import pandas as pd
import numpy as np

'''

'''
# def nc_anomaly():
#     da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc')
#     da = da.sel(time=np.in1d(da['time.year'], np.arange(1900,2010+1,1)))
#     print(da)
#     da = da.assign_coords(month_day=da.time.dt.strftime("%m-%d"))
#     result = da.groupby("month_day") - da.groupby("month_day").mean("time")
#     print(result)
#     return result
#
#
#
# # data_Global_sta = nc_anomaly()
# # data_Global_sta.to_netcdf(r'hgt_anomaly_20c')
# data_Global_sta = xr.open_dataset('hgt_anomaly_20c')
# data_Global_sta_234 = data_Global_sta['hgt500'].sel(time=np.in1d(data_Global_sta['time.month'], [2,3,4])).values
# data_Global_sta_5 = data_Global_sta['hgt500'].sel(time=np.in1d(data_Global_sta['time.month'], [5])).values
#
# target = scipy.io.loadmat('figure2.mat')
# data_target234 = target['f2a_M234_Hgt500']
# data_target5 = target['f2a_M5_Hgt500']
#
# lon = target['lon'][0]
# lat = target['lat'][0]
#
# # import matplotlib.pyplot as plt
# # import matplotlib
# # # matplotlib.rcparams['contour.negative linestyle'] = 'solid'
# # import cartopy.crs as ccrs
# # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)}
# #                        , figsize=[4.5, 4])
# # plt.subplots_adjust(top=0.84,
# #                     bottom=0.08,
# #                     left=0.07,
# #                     right=0.99,
# #                     hspace=0.2,
# #                     wspace=0.2)
# #
# # import cmaps
# # import matplotlib.colors as colors
# #
# # import cmaps
# # orig_cmap = cmaps.cmp_b2r
# #
# # ranges = [20,170,35,90]
# # ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]], [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]], transform=ccrs.PlateCarree())
# # # ax.scatter(ranges[0:2], ranges[2:4], transform=ccrs.PlateCarree())
# # cb = ax.contourf(lon, lat,
# #                  data_target234, cmap=orig_cmap,
# #                  transform=ccrs.PlateCarree(), norm=colors.CenteredNorm(),extend='both')
# #
# # # plt.colorbar(cb)
# #
# #
# # ax.coastlines(zorder=9)
# # ax.gridlines(draw_labels=True)
# # # ax.set_extent()
# # plt.show()
# # # plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\pic_combine_result/' +
# # #             plot_name[i] + ' phase %i.png' % phase, dpi=400)
# # plt.close()
#
# """ additianl """
# data = {}
#
# # ranges = [30,170,35,80]
# ranges = [40,160,35,80]
# loc_north = np.argmin(abs(lat - ranges[3]))
# loc_south = np.argmin(abs(lat - ranges[2]))
# loc_west = np.argmin(abs(lon - ranges[0]))
# loc_east = np.argmin(abs(lon - ranges[1]))
#
# data['data_234'] = np.transpose(data_Global_sta_234[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
# data['target_234'] = data_target234[loc_south:loc_north+1, loc_west:loc_east+1]
#
# lon_234, lat_234 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
# data['lat_234'] = lat_234
# data['lon_234'] = lon_234
#
#
# loc_north = np.argmin(abs(lat - ranges[3]))
# loc_south = np.argmin(abs(lat - ranges[2]))
# loc_west = np.argmin(abs(lon - ranges[0]))
# loc_east = np.argmin(abs(lon - ranges[1]))
#
# data['data_5'] = np.transpose(data_Global_sta_5[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
# data['target_5'] = data_target5[loc_south:loc_north+1, loc_west:loc_east+1]
#
# lon_5, lat_5 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
# data['lat_5'] = lat_5
# data['lon_5'] = lon_5
# scipy.io.savemat('figureA_parCor.mat', data)
# exit()
"""
pdf
"""
date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
date_low_freq_year = [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
date_base = np.arange(1900, 2010 + 1)

cor = scipy.io.loadmat('pattern_Cor.mat')
month_name = ['234', '5']
result = {}
result_rawdata = {}

for method01 in ['1']:
    cor234 = cor['yearly_cor234_method%s'%method01]
    cor5 = cor['yearly_cor5_method%s'%method01]

    date = pd.date_range('1900-01-01', '2010-12-31', freq='1D')

    cor234 = pd.DataFrame(cor234, index=date[np.in1d(date.month, [2,3,4])])
    cor5 = pd.DataFrame(cor5, index=date[np.in1d(date.month, [5])])

    print(cor234)
    merge_daily = []
    merge_yearly = []
    for i_year in np.arange(1900, 2010+1):
        list_now = list(cor234[np.in1d(cor234.index.year, i_year)].values)+list(cor5[np.in1d(cor5.index.year, i_year)].values)
        merge_daily.extend(list_now)

        mean_234 = np.mean(cor234[np.in1d(cor234.index.year, i_year)].values)
        mean_5 = np.mean(cor5[np.in1d(cor5.index.year, i_year)].values)
        merge_yearly.append((mean_234+mean_5)/2)

    merge_daily = pd.DataFrame(merge_daily, index=date[np.in1d(date.month, [2,3,4,5])])
    result_rawdata['daily_m2345'] = merge_daily.values

    # result_rawdata['yearly_m2345'] = merge_daily.resample('1Y').mean().values
    result_rawdata['yearly_m2345'] = merge_yearly

    for i_month, data_row in enumerate([cor234, cor5]):

        data = data_row.resample('1Y').mean()

        result_rawdata['yearly_m%s' % month_name[i_month]] =data.values

        from scipy.stats import zscore
        from scipy.stats import linregress
        x = data.index.year.values
        y = zscore(np.squeeze(data[0].values))

        from scipy.signal import detrend

        from scipy.stats import pearsonr
        data_row_fireindex = pd.read_csv(r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0,1])

        fireindex = data_row_fireindex[(data_row_fireindex['year'] >= 1900) & (data_row_fireindex['year'] <= 2010)]

        detrended2 = list(zscore(fireindex['nechinamogoliaSiberia']))
        result_rawdata['fireIndex_zscore' ] = detrended2
        r,p = pearsonr(y, detrended2)
        print(pearsonr(np.squeeze(merge_daily.resample('1Y').mean().values), detrended2))
        print(pearsonr(np.squeeze(merge_yearly), detrended2))

        result['R_method%s_m%s'%(method01, month_name[i_month])] = r
        result['P_method%s_m%s' % (method01, month_name[i_month])] = p
        # slope, intercept, r_value, p_value, std_err = linregress(x, y)
        # f = x * slope + intercept
        # fig, ax = plt.subplots()
        # ax.plot(x, detrended2, label='fire index')
        # ax.plot(x, y, label='patternCor')
        # ax.plot(x, f, linestyle='--')
        # plt.legend()
        # plt.show()

        """pdf for high low fire year"""

        X_plot = np.linspace(-3.5,3.5,1000)
        # def plot_pdf(X,bandwidth):
        #     from sklearn.neighbors import KernelDensity
        #     X_plots = X_plot[:, np.newaxis]
        #     kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(X)
        #     log_dens = kde.score_samples(X_plots)
        #     return np.exp(log_dens)
        def plot_pdf(X, bandwidth='scott'):
            from scipy import stats
            kde = stats.gaussian_kde(np.squeeze(X), bw_method=bandwidth)
            f = kde.covariance_factor()
            bw = f * X.std()
            log_dens = kde.evaluate(X_plot)
            return log_dens
        import matplotlib.pyplot as plt

        result_rawdata['daily_m%s' % month_name[i_month]] = data_row.values

        data1 = data_row[np.in1d(data_row.index.year, date_combine_high_freq_year)].values
        data2 = data_row[np.in1d(data_row.index.year, date_low_freq_year)]
        a = plot_pdf(data1,0.5)
        b = plot_pdf(data2,0.5)

        result['highFire_method%s_m%s'%(method01, month_name[i_month])] = a
        result['lowFire_method%s_m%s' % (method01, month_name[i_month])] = b

        # fig, ax = plt.subplots(figsize=(5,5))
        #
        # ax.plot(
        #     X_plot,
        #     a,
        #     linestyle="-",
        #     label='high fire year',color='red',
        # )
        # ax.plot(
        #     X_plot,
        #     b,
        #     linestyle="-",
        #     label='low fire year',color='blue',
        # )
        #
        # ax.legend(loc="upper left")
        # ax.set_xlim(-3.5, 3.5)
        # ax.set_ylim(0, 0.5)
        # plt.show()
        # plt.close()
result['x'] = X_plot
scipy.io.savemat('Figure_patternCor.mat', result)

scipy.io.savemat('Figure_patternCor_rawData.mat', result_rawdata)