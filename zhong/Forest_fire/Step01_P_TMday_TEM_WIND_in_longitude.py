import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr


#######################################################################

date_extre_high_freq_year = [1911,1922,1943,1987, 2003] # high2
date_combine_high_freq_year = [1943, 2003,1911,1987,1922,1944,1976,1913,1912,1965] # high1
date_high_freq_year = [1912,1913,1933,1944,1965,1976, 2008] # high12
date_low_freq_year  =           [1908, 1910, 1941, 1955, 1960, 1970, 1971, 1984, 1990]
date_base = np.arange(1900,2011)


####################################### plot TEM in given time  ###################################


colors = ['red', 'blue', 'black'][::-1]
#
wind = xr.open_dataset(r'D:\OneDrive\basis\some_projects\t2m_20c.nc')
vari = 'air'
label = 'TEM (K)'
legends = ['35-45°N', '45-55°N', '65-85°N']

######## 改动下面的范围！！！
# wind = xr.open_dataset(r'D:\OneDrive\basis\some_projects\wind1000_20c.nc')
# vari = 'wind1000'
# label = 'Wind speed (1000hPa)'
# legends = ['35-45°N', '45-55°N', '65-85°N']
# wind = wind.assign_coords(lat=np.arange(-90,91,1))

######## 改动下面的范围！！！
# wind = xr.open_dataset(r'D:\OneDrive\basis\some_projects\wind200_20c.nc')
# vari = 'wind'
# label = 'Wind speed (200hPa)'
# legends = ['35-45°N', '45-65°N', '65-85°N']

def get_TM(years, month):
    file_Path = r'D:\OneDrive\a_matlab\Blocking/'
    def get_pre_data_with_all_year(data):
        '''
        add a datetimeIndex to data
        :param data: ( number * TimeIndex_length), number at least 1
        :return: data with DatetimeIndex
        '''
        ################################# create date based on years  #################################
        month1 = '0101'
        month2 = '0531'

        date_year_s = 1900
        date_year_e = 2015
        def date_ranges(years):
            dates = []
            for iyear in years:
                date_s = '%s%s'%(iyear, month1); date_e = '%s%s'%(iyear, month2)
                sdate = pd.date_range(date_s, date_e, freq='1D')
                dates.extend(sdate)
            return dates

        years = np.arange(date_year_s, date_year_e+1)
        dates_all = date_ranges(years)

        data_TimeIndex = pd.DataFrame(index=dates_all)
        for i, idata in enumerate(data):
            data_TimeIndex['%i'%i] = idata
        return data_TimeIndex

    data = scio.loadmat(file_Path+r'BI_20c.mat',mat_dtype=True)
    index_TM = get_pre_data_with_all_year([data['BI']['TM'][0]])

    def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
        data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]
        return data_MandY
    index_TM = get_data_dateIndex_in_MandY_and_mean(index_TM, month, years).values

    index_TM = np.array([i[0][0] for i in index_TM])
    return index_TM



for month_used in [[1,2,3,4,5]]:
    for type_high in ['high1', 'low']:
        def get_wind(wind_year):
            index_TM = get_TM(wind_year, month_used)

            wind_inYear = wind.sel(time=np.in1d(wind['time.year'],wind_year))
            index_GHGS_accum = []

            def sel_time_and_mask_with_TM(lat_b, lat_e):
                print(wind_inYear.sel(lat=slice(lat_b, lat_e), time=np.in1d(wind_inYear['time.month'], month_used)))
                swind = wind_inYear.sel(lat=slice(lat_b, lat_e), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['lat'])[vari].values

                swind[np.where(index_TM !=1)] = np.nan
                return np.nanmean(swind, axis=0)

            index_GHGS_accum.append(sel_time_and_mask_with_TM(35,45))
            index_GHGS_accum.append(sel_time_and_mask_with_TM(45,55))
            index_GHGS_accum.append(sel_time_and_mask_with_TM(65,85))

            return np.array(index_GHGS_accum)

        if type_high == 'high2':
            date_high = date_extre_high_freq_year
        elif type_high == 'high12':
            date_high = date_high_freq_year
        elif type_high == 'high1':
            date_high = date_combine_high_freq_year
        elif type_high == 'low':
            date_high = date_low_freq_year
        high = get_wind(date_high)
        low = get_wind(date_base)
        print(month_used)
        print(low)
        index_GHGS_accum = high-low

        fig, ax = plt.subplots(1,1)

        for i in range(3):
            ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
        plt.legend()
        for vline in [100, 110,125]:
            ax.axvline(vline, color='k', linestyle='--', linewidth=1)

        ax.axhline(0, color='k', linewidth=1)
        Month = ''
        for imonth in month_used:
            Month = Month+str(imonth)+','
        ax.set_title('TM %s - Climately in Month %s (%s) 20C'%(type_high, Month[:-1], label))
        ax.set_xlabel('Longitude')

        # plt.show()
        plt.savefig('TM %s - Climately in Month %s (%s) 20C'%(type_high, Month[:-1], label), dpi=600)
        plt.close()


