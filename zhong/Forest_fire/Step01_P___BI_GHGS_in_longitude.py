import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

freqType = 'difference'
type_high = 'high1' #  'high1'
type_era5_or_20c = '20c'

month_use = [[1,2,3,4,5]]


####################  load BI file ########
pathIndex_BI = '20c'
# ONLY changes when pathIndex_BI changes
# month1 = '0401'
# month2 = '0531'
month1 = '0101'
month2 = '0531'

date_year_s = 1900
date_year_e = 2015

year_add = 0
if pathIndex_BI == 'm12':
    year_add = -1

#################### year #############
if type_era5_or_20c == 'era5':
    date_extre_high_freq_year = [1987, 2003]
    date_high_freq_year =[1997, 2008 ]
    date_low_freq_year =[1984, 1990, 2013]

elif type_era5_or_20c == '20c':
    # date_extre_high_freq_year = [1911,1922,1943,1944,1987, 2003]
    # date_combine_high_freq_year = [1911,1922,1943,1944,1987, 2003, 1912,1913,1933,1965,1976,1997, 2008]
    # date_high_freq_year =[1912,1913,1933,1965,1976,1997, 2008 ]
    # date_low_freq_year  =           [1908,1909,1910,1916,1927,1936,1941,1955,1960,1970,1971,1975,1984]
    date_extre_high_freq_year = [1911,1922,1943,1987, 2003] # high2
    # date_combine_high_freq_year = [1911,1922,1943,1987, 2003, 1912,1913,1933,1944,1965,1976, 2008] # high1
    date_combine_high_freq_year = [1943, 2003,1911,1987,1922,1944,1976,1913,1912,1965] # high1
    date_low_freq_year  =           [1975,1971,1970,1941,1990,1910,1984,1955,1908,1960]
    date_high_freq_year = [1912,1913,1933,1944,1965,1976, 2008] # high12
    # date_low_freq_year  =           [1960,1908,1955,1984,1910]
    date_low_freq_year  =           [1975,1971,1970,1941,1990,1910,1984,1955,1908,1960]
    date_base = np.arange(1900,2011)
# def get_pre_data_with_freqType(freqType):
#     # file_Path = r'/home/linhaozhong/work/Data/ERA5/pressure_level/plot/'
#     file_Path = r'D:\OneDrive\a_matlab\Blocking/'
#
#     index_BI = scio.loadmat(file_Path+r'BI_%s.mat'%freqType)['BI']
#
#     if freqType == 'high':
#         years = [1987,2003]
#     else:
#         years = [1984,1990,2013]
#
#     varName = index_BI.dtype.names
#
#
#     ################################# create date based on years  #################################
#
#     def date_ranges(years):
#         dates = []
#         for iyear in years:
#             date_s = '%s0101'%iyear; date_e = '%s0331'%iyear
#             sdate = pd.date_range(date_s, date_e, freq='1D').strftime('%Y%m%d')
#             dates.extend(sdate)
#         return dates
#     dates_all = date_ranges(years)
#
#     return index_BI, dates_all

def get_pre_data_with_all_year(data):
    '''
    add a datetimeIndex to data
    :param data: ( number * TimeIndex_length), number at least 1
    :return: data with DatetimeIndex
    '''
    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s%s'%(iyear, month1); date_e = '%s%s'%(iyear, month2)
            sdate = pd.date_range(date_s, date_e, freq='1D')
            dates.extend(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e+1)
    dates_all = date_ranges(years)

    data_TimeIndex = pd.DataFrame(index=dates_all)
    for i, idata in enumerate(data):
        data_TimeIndex['%i'%i] = idata
    return data_TimeIndex


#######################################  BI #############################################
def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
    year = year + year_add
    data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]
    return np.mean(np.array(data_MandY))

file_Path = r'D:\OneDrive\a_matlab\Blocking/'

data = scio.loadmat(file_Path+r'BI_%s.mat'%pathIndex_BI,mat_dtype=True)['BI']['TM'][0]
index_GHGS = get_pre_data_with_all_year([data])

for time_given in month_use:

    if freqType == 'difference':
        if type_high == 'high1':
            index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_combine_high_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            index_GHGS_accum = (index_GHGS_accum_high[0]-index_GHGS_accum_low[0])/abs(index_GHGS_accum_low[0])

        elif type_high == 'high2':
            index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_extre_high_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            index_GHGS_accum = (index_GHGS_accum_high[0]-index_GHGS_accum_low[0])/abs(index_GHGS_accum_high[0]+index_GHGS_accum_low[0])
        elif type_high == 'high12':
            index_GHGS_accum_median = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_high_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            index_GHGS_accum = (index_GHGS_accum_median[0] - index_GHGS_accum_low[0])/abs(index_GHGS_accum_median[0] + index_GHGS_accum_low[0])
        elif type_high == 'low':
            index_GHGS_accum_median = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_low_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            index_GHGS_accum = (index_GHGS_accum_median[0] - index_GHGS_accum_low[0])/abs(index_GHGS_accum_low[0])

    #####################################  plot ########################################
    fig, ax = plt.subplots(1,1)
    ax.plot(range(360), index_GHGS_accum)
    for vline in [100, 110,125]:
        ax.axvline(vline, color='k', linestyle='--', linewidth=1)
    ax.axhline(0, color='k', linewidth=1)
    Month = ''
    for imonth in time_given:
        Month = Month+str(imonth)+','
    ax.set_title('%s - Climately in Month %s (TM) %s'%(type_high, Month[:-1], type_era5_or_20c))
    ax.set_xlabel('Longitude')
    # plt.show()
    plt.savefig('%s - Climately in Month %s (TM) %s'%(type_high, Month[:-1], type_era5_or_20c), dpi=600)
    plt.close()

# exit(0)

####################################### plot GHGS in given time  ###################################


def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
    year = year + year_add
    data = scio.loadmat(file_Path+r'BI_%s.mat'%pathIndex_BI,mat_dtype=True)['BI']['TM'][0]
    index_TM = get_pre_data_with_all_year([data])
    index_TM = index_TM[(index_TM.index.month.isin(month))&(index_TM.index.year.isin(year))].values
    index_TM = np.array([i[0][0] for i in index_TM])
    data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))].values
    # print(data_MandY[310][0].shape)

    result = []
    for i in range(3):
        now = np.array([si[0][:,i] for si in data_MandY])
        now[np.where(index_TM !=1)] = np.nan
        result.append(np.nanmean(now, axis=0))
    # print(result)
    # exit(0)
    return np.array(result)

file_Path = r'D:\OneDrive\a_matlab\Blocking/'
data = scio.loadmat(file_Path+r'BI_%s.mat'%pathIndex_BI,mat_dtype=True)['BI']['GHGS'][0]
index_GHGS = get_pre_data_with_all_year([data])


for time_given in month_use:

    if freqType == 'difference':
        if type_high == 'high1':
            index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_combine_high_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            # index_GHGS_accum_high[np.isnan(index_GHGS_accum_high)] = 999
            index_GHGS_accum = np.true_divide(index_GHGS_accum_high-index_GHGS_accum_low, abs(index_GHGS_accum_low), where=(index_GHGS_accum_high!=999))
            index_GHGS_accum = index_GHGS_accum_high-index_GHGS_accum_low

        elif type_high == 'high2':
                index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_extre_high_freq_year))
                index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
                index_GHGS_accum = (index_GHGS_accum_high-index_GHGS_accum_low)/abs(index_GHGS_accum_high+index_GHGS_accum_low)
        elif type_high == 'high12':
            index_GHGS_accum_median = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_high_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            index_GHGS_accum = (index_GHGS_accum_median - index_GHGS_accum_low)/abs(index_GHGS_accum_median + index_GHGS_accum_low)
        elif type_high == 'low':
            index_GHGS_accum_median = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_low_freq_year))
            index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_base))
            index_GHGS_accum = index_GHGS_accum_median - index_GHGS_accum_low

    #################################### plot #########################################
    colors = ['red', 'blue', 'black']
    legends = ['+5', '0', '-5']
    fig, ax = plt.subplots(1,1)
    for i in range(3):
        ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
    plt.legend()
    for vline in [100, 110,125]:
        ax.axvline(vline, color='k', linestyle='--', linewidth=1)
    ax.axhline(0, color='k', linewidth=1)
    Month = ''
    for imonth in time_given:
        Month = Month+str(imonth)+','
    ax.set_title('%s - Climately in Month %s (GHGS) %s'%(type_high, Month[:-1], type_era5_or_20c))
    ax.set_xlabel('Longitude')
    # plt.show()
    plt.savefig('%s - Climately in Month %s (GHGS) %s'%(type_high, Month[:-1], type_era5_or_20c), dpi=600)
    plt.close()
exit(0)


####################################### plot TEM in given time  ###################################


# colors = ['red', 'blue', 'black'][::-1]
# legends = ['35-45°N', '45-65°N', '65-85°N']
#
#
# def get_data_dateIndex_in_MandY_and_mean(data_dateIndex, month, year):
#     year = year + year_add
#     data_MandY = data_dateIndex[(data_dateIndex.index.month.isin(month))&(data_dateIndex.index.year.isin(year))]
#     return np.transpose(np.mean(np.array(data_MandY)), [1,0])
#
#
# a = scio.loadmat('tem_longitude_m12.mat', mat_dtype=True)
# t5_add = a['t5add']; t5 = a['t5']; t5div = a['t5div']
# index_t5s_high = np.transpose(np.array([t5_add, t5, t5div]), [1,2,0])
#
# index_t5s = []
# for i in range(index_t5s_high.shape[0]):
#     index_t5s.append(np.array(index_t5s_high[i]))
# index_GHGS = get_pre_data_with_all_year([index_t5s])
#
# for time_given in month_use:
#     index_GHGS_accum_high = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_extre_high_freq_year))
#     index_GHGS_accum_low = get_data_dateIndex_in_MandY_and_mean(index_GHGS, time_given, np.array(date_low_freq_year))
#
#     if freqType == 'high':
#         index_GHGS_accum = index_GHGS_accum_high
#     elif freqType == 'low':
#         index_GHGS_accum = index_GHGS_accum_low
#     elif freqType == 'difference':
#         index_GHGS_accum = index_GHGS_accum_high - index_GHGS_accum_low
#
#     fig, ax = plt.subplots(1,1)
#
#     for i in range(3):
#         ax.plot(range(360), index_GHGS_accum[i], color=colors[i], label=legends[i])
#     plt.legend()
#     for vline in [100, 110,125]:
#         ax.axvline(vline, color='k', linestyle='--', linewidth=1)
#     if freqType == 'difference':
#         ax.axhline(0, color='k', linewidth=1)
#     Month = ''
#     for imonth in time_given:
#         Month = Month+str(imonth)+','
#     ax.set_title('%s - low in Month %s (Wind speed (200hPa))'%(type_high, Month[:-1]))
#     ax.set_xlabel('Longitude')
#
#     # plt.show()
#     plt.savefig('%s - low in Month %s (Wind speed (200hPa))'%(type_high, Month[:-1]), dpi=600)
#     plt.close()
#
# exit(0)
#
