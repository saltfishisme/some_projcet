import numpy as np
import xarray as xr




month1 = '0201'
month2 = '0531'

date_year_s = 1836
date_year_e = 2015

def create_nc(data, vari, path_save="/home/linhaozhong/work/Data/ERA5/pressure_level/plot/"):
    import xarray as xr
    import numpy as np
    import pandas as pd

    lon = np.arange(0, 360)
    lat = np.arange(-90, 91, 1)

    ################################# create date based on years  #################################

    def date_ranges(years):
        dates = []
        for iyear in years:
            date_s = '%s%s' % (iyear, month1);
            date_e = '%s%s' % (iyear, month2)
            sdate = pd.date_range(date_s, date_e, freq='1D')
            if iyear == years[0]:
                dates = sdate
            else:
                dates = dates.append(sdate)
        return dates

    years = np.arange(date_year_s, date_year_e + 1)
    times = date_ranges(years)

    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset(
        {
            vari: get_dataarray(data),

        }
    )
    print(ds)
    ds.to_netcdf(path_save + '%s_20c.nc' % vari)


windS = []
#
years = np.arange(date_year_s, date_year_e+1)

for iyear in years:
    u = xr.open_dataset('/home/linhaozhong/work/Data/20C-NOAA/hgt.%i.nc'%iyear)

    u = u.sel(time=np.in1d(u['time.month'],[2,3,4,5]), level=500)['hgt'].values

    windS.extend(u)
wind = np.array(windS, dtype='double')
create_nc(wind, 'hgt500', '/home/linhaozhong/work/Data/20_cc/')
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/wind_era5_12.mat', {'wind':wind})
exit()

windS = []
for iyear in years:
    u = xr.open_dataset('/home/linhaozhong/work/Data/20_cc/wind/uwnd.%i.nc'%iyear)
    v = xr.open_dataset('/home/linhaozhong/work/Data/20_cc/wind/vwnd.%i.nc'%iyear)

    u = u.sel(time=np.in1d(u['time.month'],[2,3,4,5]), level=850)['uwnd'].values
    v = v.sel(time=np.in1d(v['time.month'],[2,3,4,5]), level=850)['vwnd'].values

    wind = np.sqrt(u ** 2 + v ** 2)
    windS.extend(wind)
wind = np.array(windS, dtype='double')
create_nc(wind, 'wind850', '/home/linhaozhong/work/Data/20_cc/')
# scio.savemat('/home/linhaozhong/work/Data/ERA5/pressure_level/plot/wind_era5_12.mat', {'wind':wind})
exit(0)