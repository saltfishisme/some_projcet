import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
import nc4

# a = xr.open_dataset(r'D:/tas_mon_ACCESS-CM2_ssp126_000.nc')
# b = xr.open_dataset(r'D:/tas_mon_ACCESS-CM2_ssp126_001.nc')
# print(a)
# print(b)
# fig, axs = plt.subplots(3,1)
# axs[0].imshow(a['tas'].values[0])
# axs[1].imshow(b['tas'].values[0])
# axs[2].imshow(a['tas'].values[0]-b['tas'].values[0])
# plt.show()
# exit()

data_row = nc4.read_nc4(r'D:/TCI_new_2')

cmip6 = np.loadtxt('cmip6.txt', dtype=str)
# print(len(cmip6))
# exit()
# iii = 0
# for i in range(data_row.shape[1]):
#     if np.nanmean(data_row[1,i,:]) > np.nanmean(data_row[0,i,:]):
#         print(cmip6_output[i])
#         iii += 1
# print(iii/len(cmip6_output))
# fig, ax = plt.subplots()
#
# ax.boxplot(data_row[].flatten(), positions=[1], showmeans=True, meanline=True)
# ax.boxplot(s_data_row[cmip6_name_modeled[1]].flatten(), positions=[2], showmeans=True, meanline=True)
#
# plt.show()
# remove nan
nan_index = np.argwhere(~np.isnan(data_row).any(axis=2))
nan_index_unique = np.unique(nan_index[:,1])
data_row = data_row[:, nan_index_unique, :]
cmip6 = cmip6[nan_index_unique]

date_beginYear = 1850
date_begin = np.arange(1850, 1899 + 1)
date_end = np.arange(1900, 2003 + 1)
date_base = np.arange(1850, 2003 + 1)


period1_234 = np.nanmean(data_row[0,:, :50], axis=1)
period2_234 = np.nanmean(data_row[0,:, 50:154], axis=1)
period1_5 = np.nanmean(data_row[1,:, :50], axis=1)
period2_5 = np.nanmean(data_row[1,:, 50:154], axis=1)

period1_234_median = np.nanmedian(data_row[0,:, :50], axis=1)
period2_234_median = np.nanmedian(data_row[0,:, 50:154], axis=1)
period1_5_median = np.nanmedian(data_row[1,:, :50], axis=1)
period2_5_median = np.nanmedian(data_row[1,:, 50:154], axis=1)
# period1_234 = np.nanmean(data_row[0,:, 50:153+1], axis=1)
# period2_234 = np.nanmean(data_row[0,:, 154:250+1], axis=1)
# period1_5 = np.nanmean(data_row[1,:, 50:153+1], axis=1)
# period2_5 = np.nanmean(data_row[1,:, 154:250+1], axis=1)
# index = np.argwhere(((period1_234>period2_234)&(period1_5<period2_5)))

# index = np.argwhere(((period1_234>period2_234)&(period1_5<period2_5)&(period1_234_median>period2_234_median)&(period1_5_median<period2_5_median)))
# index = np.argwhere(
#     (period1_234_median>period2_234_median)&(period1_5_median<period2_5_median)
#                      &((period1_234_median-period2_234_median)>(0.05*period1_234_median))
#                      &((period2_5_median-period1_5_median)>(0.05*period2_5_median))
#                     )


# index = np.argwhere(((period1_234>period2_234))&((period1_234-period2_234)>0.05))
index = np.argwhere((
    ((period1_234_median-period2_234_median)>0.02)
                    &((period2_5_median-period1_5_median)>0.000)))
cmip6_output = cmip6[np.squeeze(index)]
# print(len(cmip6_output))
# coll = []
# coll_loc = []
# for iI,i in enumerate(cmip6_output):
#     if not '_192' in i[0]:
#         coll.append(i[0])
#         coll_loc.append(iI)
data_row_col = data_row[:,np.squeeze(index),:]

# print(data_row.shape)
# aa = data_row[0,:, 100:]
# print(aa.shape)
# aa = aa[81]
# detrended = pd.DataFrame()
# detrended['1'] = pd.Series(data_row[0,8,:])
# detrended['2'] = pd.Series(data_row_col[0,2,:])
# ax = detrended.plot()
# # ax.axhline(line[0])
# plt.show()
# exit()

# print(len(cmip6_output))
# exit()
nc4.save_nc4(data_row_col, 'TCI_output_test')
np.savetxt('cmip6_output_test.txt', cmip6_output, fmt='%s')
# exit()


data_row = nc4.read_nc4('TCI_output_test')
cmip6_name = np.loadtxt('cmip6_output_test.txt',dtype=str)
date_begin = 1850
date_period1 = [50, 153]
date_period2 = [154, 250]
date_period1_year = [1900,2003]
date_period2_year = [2004,2100]
date_base = np.arange(1850,2100+1)
# fig, ax = plt.subplots()
# a = data_row[:,:,date_period2-date_begin]
# print(np.argwhere(np.isnan(data_row)))
# a = a[~np.isnan(a)]
#
# # print(.shape)
# ax.boxplot(data_row[:,:,date_period1-date_begin].flatten(), positions=[1], showmeans=True, meanline=True)
# ax.boxplot(a, positions=[2], showmeans=True, meanline=True)
# # ax.boxplot(data_row[:,:,105:].flatten(), positions=[1], showmeans=True, meanline=True)
# plt.show()

model = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
cmip6_name_modeled = []
for i_model in model:
    a_save = []
    for iI,i in enumerate(cmip6_name):
        if i_model in i:
            a_save.append(iI)
    cmip6_name_modeled.append(a_save)
for i in cmip6_name_modeled:
    print(len(i))
exit()
def drought_wind_condition(data):
    m234, m5 = data
    from scipy.signal import  detrend
    # m234 = detrend(m234)
    # m5 = detrend(m5)
    # print(np.argwhere(m234<=np.quantile(m234, 0.4)))
    # print(np.quantile(m234, 0.4))
    # print(np.argwhere(m5 >= np.quantile(m5, 0.6)))
    # print(np.quantile(m5, 0.6))
    year = np.argwhere(((m234<=np.quantile(m234, 0.4)) & (m5>=np.quantile(m5, 0.6))))

    return year.shape[0]/data.shape[1]

def drought_wind_condition_allyear(data):
    m234, m5 = data
    year = date_base[np.argwhere(((m234<np.quantile(m234, 0.4)) & (m5>np.quantile(m5, 0.4))))]

    # 1900-2003
    perc1 = len(year[((year>=date_period1_year[0])&(year<=date_period1_year[1]))])/data.shape[1]
    perc2 = len(year[((year>=date_period2_year[0])&(year<=date_period2_year[1]))])/data.shape[1]

    return [perc1, perc2]
# extract ssp
drought_wind_year_percent = []
for data in [data_row[:, :, 50:164+1], data_row[:, :, 165:]]:
    s_drought_wind_year_percent = []
    # for i_data in cmip6_name_modeled[0]:
    for i_data in range(data.shape[1]):

        aaaa = drought_wind_condition(data[:, i_data, :])
        if aaaa < 0.14:
            print('i_data %%%%%%%%%%',cmip6_name[i_data])
            print(period1_234[i_data],period2_234[i_data],
                  period1_5[i_data],period2_5[i_data],
                  period1_234_median[i_data],period2_234_median[i_data],
                  period1_5_median[i_data],period2_5_median[i_data])

        s_drought_wind_year_percent.append(aaaa)
    drought_wind_year_percent.append(s_drought_wind_year_percent)

# drought_wind_year_percent = []
# for data in [data_row[:, :, :]]:
#
#     for i_data in range(data.shape[1]):
#         drought_wind_year_percent.append(drought_wind_condition_allyear(data[:, i_data, :]))
# drought_wind_year_percent = np.array(drought_wind_year_percent).T


# detrended = pd.Series(np.array(drought_wind_year_percent[1])[cmip6_name_modeled[2]].flatten())
# ax = detrended.plot()
# # ax.axhline(line[0])
# plt.show()
fig, ax = plt.subplots()

result = {}
for iI_boxplot, s_data_row in enumerate([np.array(drought_wind_year_percent[0]), np.array(drought_wind_year_percent[1])]):
    print(s_data_row[cmip6_name_modeled[0]])
    print(cmip6_name[cmip6_name_modeled[0]])
    print('%%%%%%%%% 5 month %%%%%%%%%%%%%%%%')
    def selected_25_75(data):
        p25, p75 = np.percentile(data, [25,75])
        print(p25, p75)
        s_data = data[(data>p25)&(data<p75)]
        print(np.percentile(s_data, [25,50,75]))
        print(s_data)
        plt.boxplot(s_data)
        plt.show()
        return s_data
    for iiiii in range(4):
        result['s5_%i'%(1+iI_boxplot+iiiii*2)] = s_data_row[cmip6_name_modeled[iiiii]].flatten()
    ax.boxplot(s_data_row[cmip6_name_modeled[0]].flatten(), positions=[1+iI_boxplot], showmeans=True, meanline=True, whis=2.5)
    ax.boxplot(s_data_row[cmip6_name_modeled[1]].flatten(), positions=[4+iI_boxplot], showmeans=True, meanline=True, whis=2.5)
    ax.boxplot(s_data_row[cmip6_name_modeled[2]].flatten(), positions=[7+iI_boxplot], showmeans=True, meanline=True, whis=2.5)
    ax.boxplot(s_data_row[cmip6_name_modeled[3]].flatten(), positions=[10+iI_boxplot], showmeans=True, meanline=True, whis=2.5)
import scipy.io as scio
scio.savemat('figure5_cmip6.mat', result)
# ax.set_ylim([0,0.3])
# # ax.axhline(line[0])
plt.show()
exit()


path_Save = r'/home/linhaozhong/work/Data/'
path_cmip6 = r'/home/linhaozhong/work/Data/CMIP6/'
cmip6_File = np.loadtxt(path_Save+'cmip6.txt', dtype=str)

date_base = np.arange(1850,2100+1)
ranges = [97,126,45,54]
months = [[2,3,4],[5]]

def get_wind(var, var_month):
    wind_inmonth = var.sel(time=np.in1d(var['time.month'], var_month))
    return wind_inmonth


def get_CMIP6_byYear(fileName):
    """
    get CMIP6 data in given year, month, and range.
    :param year:
    :return:
    """
    a = xr.open_dataset(fileName)

    def ll_now(m_ranged):
        da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        m_ranged = da.groupby("year_month").mean("time").sel(lon=slice(97, 126))
        m_ranged = m_ranged['tas']
        lat = m_ranged.lat.values
        loc54 = np.argmin(abs(lat - 53.67))
        loc45 = np.argmin(abs(lat - 46.76))

        m_ranged_north = m_ranged.isel(lat=loc54).mean(dim='lon').values
        m_ranged_south = m_ranged.isel(lat=loc45).mean(dim='lon').values
        tem_grad = (m_ranged_south - m_ranged_north) / (lat[loc54]-lat[loc45])
        return tem_grad
    m234 = ll_now(get_wind(a, [2,3,4]))
    m5 = ll_now(get_wind(a, [5]))
    return m234, m5

m234 = []
m5 = []
for i_file in cmip6_File:
    sm234, sm5 = get_CMIP6_byYear(path_cmip6+i_file)
    m234.append(sm234)
    m5.append(sm5)

nc4.save_nc4(np.array([m234,m5]), path_Save+'TCI_lon126')


def get_CMIP6_byYear(fileName):
    """
    get CMIP6 data in given year, month, and range.
    :param year:
    :return:
    """
    a = xr.open_dataset(fileName)

    def ll_now(m_ranged):
        da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        m_ranged = da.groupby("year_month").mean("time").sel(lon=slice(98.52, 125.82))
        m_ranged = m_ranged['tas']
        lat = m_ranged.lat.values
        loc54 = np.argmin(abs(lat - 53.67))
        loc45 = np.argmin(abs(lat - 46.76))

        m_ranged_north = m_ranged.isel(lat=loc54).mean(dim='lon').values
        m_ranged_south = m_ranged.isel(lat=loc45).mean(dim='lon').values
        tem_grad = (m_ranged_south - m_ranged_north) / (lat[loc54]-lat[loc45])
        return tem_grad
    m234 = ll_now(get_wind(a, [2,3,4]))
    m5 = ll_now(get_wind(a, [5]))
    return m234, m5

m234 = []
m5 = []
for i_file in cmip6_File:
    sm234, sm5 = get_CMIP6_byYear(path_cmip6+i_file)
    m234.append(sm234)
    m5.append(sm5)

nc4.save_nc4(np.array([m234,m5]), path_Save+'TCI_lonSTATION')

def get_CMIP6_byYear(fileName):
    """
    get CMIP6 data in given year, month, and range.
    :param year:
    :return:
    """
    a = xr.open_dataset(fileName)

    def ll_now(m_ranged):
        da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        m_ranged = da.groupby("year_month").mean("time").sel(lon=slice(97, 126))
        m_ranged = m_ranged['tas']
        lat = m_ranged.lat.values
        loc54 = np.argmin(abs(lat - 53.67))
        loc45 = np.argmin(abs(lat - 46.76))

        m_ranged_north = m_ranged.isel(lat=loc54).mean(dim='lon').values
        m_ranged_south = m_ranged.isel(lat=loc45).mean(dim='lon').values
        tem_grad = (m_ranged_south - m_ranged_north) / 7.45935
        return tem_grad
    m234 = ll_now(get_wind(a, [2,3,4]))
    m5 = ll_now(get_wind(a, [5]))
    return m234, m5

m234 = []
m5 = []
for i_file in cmip6_File:
    sm234, sm5 = get_CMIP6_byYear(path_cmip6+i_file)
    m234.append(sm234)
    m5.append(sm5)

nc4.save_nc4(np.array([m234,m5]), path_Save+'TCI_lon126_frozen')


def get_CMIP6_byYear(fileName):
    """
    get CMIP6 data in given year, month, and range.
    :param year:
    :return:
    """
    a = xr.open_dataset(fileName)

    def ll_now(m_ranged):
        da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
        m_ranged = da.groupby("year_month").mean("time").sel(lon=slice(98.52, 125.82))
        m_ranged = m_ranged['tas']
        lat = m_ranged.lat.values
        loc54 = np.argmin(abs(lat - 53.67))
        loc45 = np.argmin(abs(lat - 46.76))

        m_ranged_north = m_ranged.isel(lat=loc54).mean(dim='lon').values
        m_ranged_south = m_ranged.isel(lat=loc45).mean(dim='lon').values
        tem_grad = (m_ranged_south - m_ranged_north) / 7.45935
        return tem_grad
    m234 = ll_now(get_wind(a, [2,3,4]))
    m5 = ll_now(get_wind(a, [5]))
    return m234, m5

m234 = []
m5 = []
for i_file in cmip6_File:
    sm234, sm5 = get_CMIP6_byYear(path_cmip6+i_file)
    m234.append(sm234)
    m5.append(sm5)

nc4.save_nc4(np.array([m234,m5]), path_Save+'TCI_lonSTATION_frozen')
exit()

