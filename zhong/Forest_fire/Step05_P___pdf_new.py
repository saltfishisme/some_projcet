import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.io as scio
import cartopy.crs as ccrs
import cmaps
import sys
import xarray as xr

###############################################################
import nc4

# date_combine_high_freq_year = [1943, 2003,1911,1987,1922,1944,1976,1913,1912,1965] # high1
# date_low_freq_year  =  [1908, 1910, 1941, 1955, 1960, 1970, 1971, 1984, 1990]

date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943] # high1
date_low_freq_year  =  [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
date_base = np.arange(1900,2011)


####################################### plot TEM in given time  ###################################


colors = ['red', 'blue', 'black'][::-1]
#
# wind = xr.open_dataset(r'D:\OneDrive\basis\some_projects\t2m_20c.nc')
# vari = 'air'
# label = 'TEM (K)'
# legends = ['35-45°N', '45-55°N', '65-85°N']
# def nc_anomaly_SpatianalRange(varFileName):
#     lon_b, lon_e, lat_b, lat_e = ranges1
#     da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\%s.nc'%varFileName)
#     print(da)
#     da = da.sel(lat=slice(lat_b, lat_e), lon=slice(lon_b, lon_e))
#     da = da.sel(time=np.in1d(da['time.year'], np.arange(1900,2010+1,1)))
#     da = da.sel(time=np.in1d(da['time.month'], [2,3,4,5]))
#     da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
#     print(da)
#     result = da.groupby("year_month") - da.groupby("year_month").mean("time")
#     print('???so')
#     return result

def nc_anomaly_SpatianalRange(varFileName, cli_year):
    lon_b, lon_e, lat_b, lat_e = ranges1
    da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\%s.nc'%varFileName)
    print(da)
    da = da.sel(lat=slice(lat_b, lat_e), lon=slice(lon_b, lon_e))
    da = da.sel(time=np.in1d(da['time.year'], np.arange(1900,2010+1,1)))
    da = da.sel(time=np.in1d(da['time.month'], [2,3,4,5]))
    da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
    da_cli =  da.sel(time=np.in1d(da['time.year'], cli_year))
    da_cli = da_cli.assign_coords(year_month=da_cli.time.dt.strftime("%m-%d"))
    result = da.groupby("year_month") - da_cli.groupby("year_month").mean("time")
    print('???so')
    return result
# def nc_anomaly():
#     da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc').sel(lat=60)
#     da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
#     result = da.groupby("year_month") - da.groupby("year_month").mean("time")
#     return result
######## 改动下面的范围！！！
data_collection_for_txt_save = []

fig, axs = plt.subplots(2, 3, figsize=[6,4])
plt.subplots_adjust(top=0.955,
bottom=0.08,
left=0.08,
right=0.97,
hspace=0.32,
wspace=0.285)

import matplotlib.pyplot as plt

SMALL_SIZE = 6

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
plt.rc('figure', titlesize=SMALL_SIZE)  # fontsize of the figure titleim
import matplotlib

matplotlib.rc('axes', titlesize=SMALL_SIZE)

def plots(axs):
    def p_plot_pdf(ax, data, xlabel, title):
        from numpy import mean
        from numpy import std
        from scipy.stats import norm


        labels = ['high', 'low', 'climatology']
        colors = ['red', 'blue', 'black']

        for i in range(2):

            from scipy import stats
            from scipy.stats import norm

            def plot_pdf(X, bandwidth='scott'):
                from scipy import stats
                kde = stats.gaussian_kde(np.squeeze(X), bw_method=bandwidth)
                f = kde.covariance_factor()
                bw = f * X.std()
                log_dens = kde.evaluate(X_plot)
                return log_dens


            pdf_xy = [X_plot, plot_pdf(data[i][0])]
            data_collection_for_txt_save.append(pdf_xy)
            ax.plot(pdf_xy[0], pdf_xy[1],color=colors[i], label=labels[i])
            index_max = np.argmax(pdf_xy[1])
            ax.plot([pdf_xy[0][index_max],pdf_xy[0][index_max]], [0,pdf_xy[1][index_max]], color='black',
                    linestyle='--', linewidth=1)

        ax.legend()
        # ax.set_title(title)
        # plt.show()

    for smonth_now, month_used in enumerate(months):
        ranges = ranges_all[smonth_now]

        def get_wind(wind, wind_year, vari):
            wind_inYear = wind.sel(time=np.in1d(wind['time.year'],wind_year))
            index_GHGS_accum = []

            def sel_time_and_mask_with_TM(lon_b, lon_e, lat_b, lat_e):
                print(wind_inYear.sel(lat=slice(lat_b, lat_e), lon=slice(lon_b, lon_e), time=np.in1d(wind_inYear['time.month'], month_used)))
                swind = wind_inYear.sel(lat=slice(lat_b, lat_e), lon=slice(lon_b, lon_e), time=np.in1d(wind_inYear['time.month'], month_used)).mean(dim=['lat', 'lon'])[vari].values
                return swind

            index_GHGS_accum.append(sel_time_and_mask_with_TM(ranges[0], ranges[1], ranges[2], ranges[3]))
            return np.array(index_GHGS_accum)


        data = []
        data.append(get_wind(data_Global_sta, date_combine_high_freq_year, vari))
        data.append(get_wind(data_Global_sta, date_low_freq_year, vari))
        # data.append(get_wind(data_Global_sta, date_base, vari))


        Month = ''
        for imonth in month_used:
            Month = Month+str(imonth)+','

        title = '%s M%s'%(label, Month[:-1])
        p_plot_pdf(axs[smonth_now], data, label, title)
        axs[smonth_now].tick_params(axis='x', labelsize=SMALL_SIZE)
        axs[smonth_now].tick_params(axis='y', labelsize=SMALL_SIZE)

    axs[smonth_now].set_xlabel(label, fontdict={'size':SMALL_SIZE})


ranges1 = [97,126,45,54] # m1
ranges2 = [97,126,45,54] # M2
ranges4 = [97,126,45,54] # M4
ranges5 = [97,126,45,54] # M5
ranges_all = [ranges1, ranges2, ranges4, ranges5]

months = [[2,3,4],[5]]
titles = [['(a) 500hPa Geopotential Height,anomalies,Feb-April',
           '(b) T2m, anomalies,Feb-April',
           '(c) 850hPa Wind speed,anomalies,Feb-April'],
          ['(d) 500hPa Geopotential Height,anomalies,May',
           '(e) T2m,anomalies,May',
           '(f)850hPa Wind speed,anomalies,May']]


data_Global_sta = nc_anomaly_SpatianalRange('t2m_20c')
data_Global_sta.to_netcdf('t2m_20c_anomaly.nc')
data_Global_sta = nc_anomaly_SpatianalRange(r'wind850_20c')
data_Global_sta.to_netcdf('wind850_20c_anomaly.nc')
data_Global_sta = nc_anomaly_SpatianalRange('hgt500_20c')
data_Global_sta.to_netcdf('hgt500_20c_anomaly.nc')

X_plot = np.linspace(-10,10,1000)
# data_Global_sta['air'].values = data_Global_sta['air'].values-274.15
data_Global_sta = xr.open_dataset(r't2m_20c_anomaly.nc')
print('slowlsow?')
vari = 'air'
label = 'T2m (K)'
plots([axs[0,1], axs[1,1]])

X_plot = np.linspace(-5,5,1000)
data_Global_sta = xr.open_dataset(r'wind850_20c_anomaly.nc')

vari = 'wind850'
label = 'wind speed (m*$s^{-1}$)'
plots([axs[0,2], axs[1,2]])


X_plot = np.linspace(-150,150,1000)
data_Global_sta = xr.open_dataset(r'hgt500_20c_anomaly.nc')

vari = 'hgt500'
label = 'Z500'
plots([axs[0,0], axs[1,0]])

for i_ax in range(2):
    for j_ax in range(3):
        axs[i_ax, j_ax].set_title(np.array(titles)[i_ax, j_ax], loc='center')
# axs[1][1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
#           fancybox=True, shadow=True, ncol=2)
# axs[0,0].set_yticks([0,0.001,0.002,0.003,0.004])
# axs[0,0].set_xticks([-200,-100,0,100,200])
# axs[1,0].set_yticks([0,0.001,0.002,0.003,0.004])
# axs[0,0].set_ylabel('PDF', fontdict={'size':SMALL_SIZE})
# axs[1,0].set_ylabel('PDF', fontdict={'size':SMALL_SIZE})
plt.show()
print(data_collection_for_txt_save.shape)
nc4.save_nc4(np.array(data_collection_for_txt_save), 'data_collection')
import scipy.io as scio
import numpy as np
import nc4
a = nc4.read_nc4('data_collection')
a = a[[8,9,0,1,4,5,10,11,2,3,6,7],:,:]
scio.savemat('figure4.mat',{'pdf':a})

plt.show()
plt.savefig('figure1_pdf.png', dpi=400)
plt.close()