import os
import shutil

import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
from scipy.signal import detrend
import pandas as pd
from scipy.stats import zscore
def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    # def get_dataarray(data):
    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
    #     return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times

    for iI_vari, ivari in enumerate(var_name):
        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + file_name)

"""
===============================
use z500 to constrain cmip6
===============================
"""

"""
===============================
1, filter CMIP6 data from .sh file.
===============================
"""
# import os
# import xarray as xr
# import numpy as np
# import pandas as pd
# # 1.1 get all file names and addresses from wget.sh files.
#
# path_wget_sh_file = r'G:\cmip6_sh/'
# keyword_cmip6FileName = ['']  # skip if keyword is not in fileName
#
# file_name = []
# file_address = []
# for i_wget_file in os.listdir(path_wget_sh_file):
#     if not '.sh' in i_wget_file:
#         continue
#     with open(path_wget_sh_file+i_wget_file, 'r', encoding='utf-8') as file:
#         wget_sh_File = file.read()
#     for i_findNC in wget_sh_File.split("'"):
#         if not all(substring in i_findNC for substring in keyword_cmip6FileName):
#             continue
#         if '.nc' in i_findNC[-3:]:
#             if 'http' in i_findNC:
#                 file_address.append(i_findNC)
#             else:
#                 file_name.append(i_findNC)
# cmip6_file_address = pd.DataFrame(file_address, index=file_name)
#
# cmip6_filename = file_name
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     if len(sp) == 7:
#         cmip6_filename_splite.append(sp)
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_split.csv', index_col=0)
# print(np.unique(cmip6_filename_splite['2']).shape)
#
# cmip6_filename_use = []
#
# labels = ['historical','ssp126', 'ssp245', 'ssp370', 'ssp585']
#
# def find_file_name(x):
#     scenio = x['3']
#
#     if not ((np.isin(labels[2], scenio)) & (np.isin(labels[3], scenio))& (np.isin(labels[4], scenio))):
#         return
#     # if not len(np.unique(scenio))==5:
#     #     return
#
#     period = x['6']
#     period_year = []
#     for iI_period, i_period in enumerate(period):
#         i_period_year_begin = int(i_period[:4])
#         if (x['3'].iloc[iI_period] == 'historical'):
#             if not i_period_year_begin > 2014:
#                 period_year.append(iI_period)
#         else:
#             if not i_period_year_begin > 2100:
#                 period_year.append(iI_period)
#     x = x.iloc[period_year]
#     cmip6_filename_use.extend(x.index.values)
#     # print(x)
#     # print(cmip6_filename_use)
#
# cmip6_filename_splite.groupby([cmip6_filename_splite['2'],cmip6_filename_splite['4']]).apply(
#     lambda x: find_file_name(x))
#
# a = cmip6_filename_splite.iloc[np.unique(cmip6_filename_use)]
# print(a)
# print(np.unique(a['2']).shape)
#
# # a = cmip6_file_address.iloc[np.unique(cmip6_filename_use)]
#
# exit()
# path_exist_File = r'G:\cmip6/'
# exist_File = os.listdir(path_exist_File)
# for i in exist_File:
#     if '_2.nc' in i:
#         print('?????')
#         print(i.replace('_2.nc', '.nc'))
#         os.rename(path_exist_File+i, path_exist_File+i.replace('_2.nc', '.nc'))
#     if '_2.hdf' in i:
#         print('?????')
#         print(i.replace('_2.nc', '.nc'))
#         os.rename(path_exist_File+i, path_exist_File+i.replace('_2.hdf', '.hdf'))
# exist_File = [i.replace('.hdf','.nc') for i in exist_File]
#
# missingData2 = list(set(a.index.values)-set(exist_File))
#
# missingData = a.loc[list(dict.fromkeys(list(missingData2)))]
# # a.to_csv('Address.csv')
# print(missingData)
# index_exAceess = []
# for i in missingData.index.values:
#     if '' in i:
#         index_exAceess.append(i)
#         print(i)
# missingData = missingData.loc[index_exAceess]
# print(missingData)
# np.savetxt('Address_vpn.txt', missingData[0],fmt='%s')
# exit()

"""
===============================
find all nocomplicated nc file
===============================
"""

# fileAll = os.listdir(path_wget_sh_file)
# missingData = []
# cmip6_z500 = []
# for i_file in fileAll:
#     if not all(substring in i_file for substring in keyword_cmip6FileName):
#         continue
#     if not '.nc' in i_file:
#         continue
#     try:
#         xr.open_dataset(path_wget_sh_file + i_file)
#         cmip6_z500.append(i_file)
#     except:
#         missingData.append(i_file)
#
# # missingData = np.loadtxt('missingData_cimp6Z500.txt', dtype='str')[-15:]
# # cmip6_z500 =np.loadtxt('cimp6_Z500.txt', dtype='str')
#
# missingData2 = list(set(file_name)-set(cmip6_z500))
# missingData = cmip6_file_address.loc[list(dict.fromkeys(list(missingData)+missingData2))]
#
# np.savetxt(path_wget_sh_file+'missingData_Address.txt', missingData[0],fmt='%s')
# np.savetxt(path_wget_sh_file+r'cimp6_download.txt', cmip6_z500, fmt='%s')
#
# exit()

"""
===============================
combine cmip6 data
model-ssp126-r1i1p1f1-**time**.nc
===============================
"""

# path_cmip6 = r'G:\cmip6_seaice\gong/'
# path_concat_cmip6 = r'G:\cmip6_seaice\new/'
# cmip6_filename = os.listdir(path_cmip6)
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     if len(sp) == 7:
#         cmip6_filename_splite.append(sp)
#
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
#
# cmip6_filename_use = cmip6_filename_splite[(cmip6_filename_splite['0'] == 'sivoln')
#                                            & (cmip6_filename_splite['1'] == 'Amon')]
# # cmip6_filename_use = cmip6_filename_splite[(cmip6_filename_splite['0'] == 'sivoln')
# #                                            & (cmip6_filename_splite['1'] == 'Amon')]
# cmip6_filename_missing = []
# print('begin')
# def find_file_name(x):
#     file_titile =  ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'])]
#     file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
#     # get period of cmip6 file
#
#     period = x['6']
#     period_year = []
#     period_sort = []
#     for i_period in period:
#         i_period_year_begin = int(i_period[:4])
#         period_sort.append(i_period_year_begin)
#         i_period_year_end = int(i_period[7:11])
#         period_year.extend(np.arange(i_period_year_begin, i_period_year_end + 1))
#
#     period_year_pd = np.arange(np.min(period_year), np.max(period_year) + 1)
#
#     if len(period_year_pd) != len(period_year):
#         print(period_year_pd)
#         print(period_year)
#         print(len(period_year_pd), len(period_year))
#         print(file)
#         cmip6_filename_missing.append(file)
#     print(file_titile[0])
#     if os.path.exists(path_concat_cmip6 +r'%s_'
#                       r'%i01-%i12.nc' %
#                       (file_titile[0],
#                        np.min(period_year),
#                        np.max(period_year))) | os.path.exists(path_concat_cmip6 +r'%s_'
#         r'%i01-%i12.hdf' % (
#             file_titile[0], np.min(period_year), np.max(period_year))):
#         print('continue')
#         return
#     if len(file) == 1:
#         import shutil
#         shutil.copy(path_cmip6 + file[0], path_concat_cmip6)
#         return
#     period_sort = sorted(range(len(period_sort)), key=lambda k: period_sort[k])
#     a = xr.open_dataset(path_cmip6 + file[period_sort[0]], use_cftime=True)
#     for i in period_sort[1:]:
#         b = xr.open_dataset(path_cmip6 + file[i], use_cftime=True)
#         a = xr.concat([a, b], dim='time')
#
#     a.to_netcdf(path_concat_cmip6 + r'%s_%i01-%i12.nc' % (file_titile[0], np.min(period_year),
#                                                           np.max(period_year)))
#
#
# cmip6_filename_use.groupby([cmip6_filename_use['2'],
#                             cmip6_filename_use['3'],
#                             cmip6_filename_use['4']]).apply(
#     lambda x: find_file_name(x))
#
# exit()


"""
===============================
reorganize the required cmip6 data
containing historical, ssp126, 245, ....
===============================
"""

# cmip6_filename = os.listdir(r'G:\cmip6_concat/')
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     if len(sp) == 7:
#         cmip6_filename_splite.append(sp)
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_split.csv', index_col=0)
#
# cmip6_filename_use = []
# cmip6_filename_use_loc = []
#
# def find_file_name(x):
#     scenio = x['3']
#     if not len(np.unique(scenio)) == 5:
#         return
#
#     cmip6_filename_use.extend(['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])])
#     cmip6_filename_use_loc.extend(x.index.values)
#
# cmip6_filename_splite.groupby([cmip6_filename_splite['2'],cmip6_filename_splite['4']]).apply(
#     lambda x: find_file_name(x))
#
# print(cmip6_filename_splite.iloc[cmip6_filename_use_loc])
# np.savetxt('final_cmip6.txt', cmip6_filename_use,fmt='%s') # delete some items by myself!


"""
===============================
Pressure to Pressure Interpolation
===============================
"""

# from metpy.interpolate import log_interpolate_1d
# from metpy.units import units
#
#
# # path_z500_save = r'/home/linhaozhong/work/cmip6_z500/'
# # path_cmip6 = r'/home/linhaozhong/work/Forest_Fire/'
# path_z500_save = r'G:/cmip6_z500/'
# path_cmip6 = r'G:/cmip6/'
# plevs = [500.] * units.hPa
#
# wrong = []
# for i_fileName in os.listdir(path_cmip6):
#     if os.path.exists(path_z500_save+i_fileName.replace('.hdf', '.nc')):
#         continue
#     a = xr.open_dataset(path_cmip6+i_fileName)
#     print(i_fileName)
#     # a  = xr.open_dataset(r"G:\cmip6\zg_Amon_UKESM1-0-LL_ssp585_r3i1p1f2_gn_201501-204912.hdf")
#     try:
#         hgt = a.sel(plev=slice(50000 + 100, 50000 - 100))['zg'].values
#         save_standard_nc([np.squeeze(hgt)], ['z500'], times=a.time, lon=a.lon, lat=a.lat,
#                          path_save=path_z500_save, file_name=i_fileName.replace('.hdf', '.nc'))
#         continue
#     except:
#         wrong.append(i_fileName)
#         print('nonnonononnonon', i_fileName)
#         continue
#
#     levels = a.plev.values
#     print(levels)
#     if a.plev.units in ['pa','Pa']:
#         print('i am here')
#         try:
#             hgt = a.sel(plev=slice(50000-100, 50000+100))['zg'].values
#             print(hgt.shape)
#             if np.isin(0, hgt.shape):
#                 hgt = a.sel(plev=slice(50000 + 100, 50000 - 100))['zg'].values
#                 print(hgt.shape)
#                 if np.isin(0, hgt.shape):
#                     print('so 0 in here?')
#                     raise ValueError("0 in shape")
#
#             save_standard_nc([np.squeeze(hgt)], ['z500'], times=a.time, lon=a.lon, lat=a.lat,
#                              path_save=path_z500_save, file_name=i_fileName.replace('.hdf', '.nc'))
#             continue
#         except:
#             print('no!')
#
#     elif a.plev.units in ['hpa','hPa']:
#         print('i am here')
#
#         try:
#             hgt = a.sel(plev=slice(500 - 100, 500 + 1))['zg'].values
#             if np.isin(0, hgt.shape):
#                 hgt = a.sel(plev=slice(500 + 1, 500 - 1))['zg'].values
#                 if np.isin(0, hgt.shape):
#                     raise ValueError("0 in shape")
#
#             save_standard_nc([np.squeeze(hgt)], ['z500'], times=a.time, lon=a.lon, lat=a.lat,
#                              path_save=path_z500_save, file_name=i_fileName.replace('.hdf', '.nc'))
#             continue
#
#         except:
#             print('no!')
#
#     pressure = np.ones(hgt.shape)
#     for i in range(pressure.shape[1]):
#         pressure[:,i,:,:] = pressure[:,i,:,:]*levels[i]
#     pressure = units.Quantity(pressure, a.plev.units)
#     height = log_interpolate_1d(plevs, pressure, hgt, axis=1)
#     print(i_fileName)
#
#     save_standard_nc([height[:,0,:,:]], ['z500'], times=a.time, lon=a.lon, lat=a.lat,
#                      path_save=path_z500_save, file_name=i_fileName)
# print(wrong)
# exit()





import matplotlib.pyplot as plt
import scipy.io
import xarray as xr
import nc4
import pandas as pd
import numpy as np
"""
FIG2
FIG3
"""


# date_combine_high_freq_year = [1874, 1899, 1866, 1898, 1883, 1863, 1885, 1858,
#                                1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]  # high1
# date_low_freq_year = [1886, 1897, 1860, 1892, 1891, 1861, 1888, 1882,
#                         1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970]

date_combine_high_freq_year = [1965, 1912, 1913, 1976, 1944, 1922, 1987, 1911, 2003, 1943]
date_low_freq_year = [1960, 1908, 1955, 1984, 1910, 1990, 1941, 1970, 1971, 1975]
date_base = np.arange(1850, 2014+1)


months = [[2,3,4],[5]]


# import matlab.engine
# matlab.engine.shareEngine
# matlab.engine.engineName
# eng = matlab.engine.connect_matlab('MATLAB_976')
# eng.cd(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire/', nargout=0)

def create_standard_pattern(fileName_historical, fileName_ssp):
    """
    :param fileName_historical: str
    :param fileName_ssp: list
    :return:
    """
    def step01_standard_pattern(fileName):
        def get_wind(var, var_year, var_month):
            wind_inYear = var.sel(time=np.in1d(var['time.year'], var_year))
            wind_inmonth = wind_inYear.sel(
                                        time=np.in1d(wind_inYear['time.month'], var_month))
            return wind_inmonth

        savemat = []
        for i_month in months:

            def get_CMIP6_byYear(year):
                """
                get EKF400 data in given year, month, and range.
                :param year:
                :return:
                """
                a = xr.open_dataset(path_cmip6+fileName)
                m_ranged = get_wind(a, year, i_month)

                # averaged in yearly scale
                da = m_ranged.assign_coords(year_month=m_ranged.time.dt.strftime("%Y"))
                m_ranged = da.groupby("year_month").mean("time")
                return m_ranged['z500'].values


            climate = get_CMIP6_byYear(date_base)

            from scipy.signal import detrend
            climate = detrend(climate, axis=0)

            high = climate[date_combine_high_freq_year-date_base[0]]
            lows = climate[date_low_freq_year-date_base[0]]

            a = xr.open_dataset(path_cmip6+fileName)
            lon = a.lon.values
            lat = a.lat.values


            high = np.nanmean(high, axis=0)
            lows = np.nanmean(lows, axis=0)

            data = high-lows
            savemat.append(data)

        import scipy.io as scio
        m234 = savemat[0]
        m5 = savemat[1]


        scio.savemat('standard_pattern.mat', {
            'M234_Hgt500':m234,
            'M5_Hgt500': m5,
            'lon':lon,
            'lat':lat
            })

    def stpe02_montly_pattern(fileName):
        """
        :param fileName: for nc_anomaly_montly,
                        an existed file named as standard_pattern.mat is created by step01 function.
        :return:
        """

        def nc_anomaly_monthly():
            da = xr.open_dataset(path_cmip6+fileName)
            da = da.assign_coords(month_day=da.time.dt.strftime("%m"))
            result = da.groupby("month_day") - da.groupby("month_day").mean("time")
            return result

        data_Global_sta = nc_anomaly_monthly()

        data_Global_sta_234 = data_Global_sta['z500'].sel(time=np.in1d(data_Global_sta['time.month'], [2,3,4])).values
        data_Global_sta_5 = data_Global_sta['z500'].sel(time=np.in1d(data_Global_sta['time.month'], [5])).values

        target = scipy.io.loadmat('standard_pattern.mat')
        data_target234 = target['M234_Hgt500']
        data_target5 = target['M5_Hgt500']


        lon = target['lon'][0]
        lat = target['lat'][0]


        """ additianl """
        data = {}

        ranges = [30,170,35,80]
        # ranges = [40,160,35,80]

        loc_north = max([np.argmin(abs(lat - ranges[3])), np.argmin(abs(lat - ranges[2]))])
        loc_south = min([np.argmin(abs(lat - ranges[3])), np.argmin(abs(lat - ranges[2]))])
        loc_west = min([np.argmin(abs(lon - ranges[0])), np.argmin(abs(lon - ranges[1]))])
        loc_east = max([np.argmin(abs(lon - ranges[0])), np.argmin(abs(lon - ranges[1]))])

        data['data_234'] = np.transpose(data_Global_sta_234[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
        data['target_234'] = data_target234[loc_south:loc_north+1, loc_west:loc_east+1]

        lon_234, lat_234 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
        data['lat_234'] = lat_234
        data['lon_234'] = lon_234


        data['data_5'] = np.transpose(data_Global_sta_5[:, loc_south:loc_north+1, loc_west:loc_east+1], [1,2,0])
        data['target_5'] = data_target5[loc_south:loc_north+1, loc_west:loc_east+1]

        lon_5, lat_5 = np.meshgrid(lon[loc_west:loc_east+1], lat[loc_south:loc_north+1])
        data['lat_5'] = lat_5
        data['lon_5'] = lon_5
        scipy.io.savemat('figureA_parCor.mat', data)

        "call matlab"
        eng.pattern_Cor(nargout=0)
        import time
        time.sleep(0.5)
        try:
            shutil.move(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire/pattern_Cor.mat', path_pattern_cor+'pattern_Cor.mat')
            os.rename(path_pattern_cor+'pattern_Cor.mat', path_pattern_cor+'%s.mat'%fileName.replace('.nc', ''))
        except:
            return
    step01_standard_pattern(fileName_historical)
    stpe02_montly_pattern(fileName_historical)
    for i_ssp in fileName_ssp:
        stpe02_montly_pattern(i_ssp)


def Rvalue_pattern_cor_With_fireIndex(fileName, return_merge=False, merge_type='merge'):
    """
    :param fileName:
    :param return_merge: 'montly', 'yearly'
    :return:
    """
    cor = scipy.io.loadmat(path_pattern_cor+fileName.replace('.nc', '.mat'))

    for method01 in ['1']:
        cor234 = cor['yearly_cor234_method%s' % method01]
        cor5 = cor['yearly_cor5_method%s' % method01]
        date = pd.date_range('%i-01-01'%int(date_base[0]), '%i-12-31'%int(date_base[-1]), freq='1M')

        cor234 = pd.DataFrame(cor234, index=date[np.in1d(date.month, [2, 3, 4])])
        cor5 = pd.DataFrame(cor5, index=date[np.in1d(date.month, [5])])

        merge_daily = []
        merge_yearly = []
        for i_year in date_base:
            list_now = list(cor234[np.in1d(cor234.index.year, i_year)].values) + list(
                cor5[np.in1d(cor5.index.year, i_year)].values)
            merge_daily.extend(list_now)

            mean_234 = np.mean(cor234[np.in1d(cor234.index.year, i_year)].values)
            mean_5 = np.mean(cor5[np.in1d(cor5.index.year, i_year)].values)
            if merge_type == 'merge':
                merge_yearly.append((mean_234 + mean_5) / 2)
            elif merge_type == '234':
                merge_yearly.append(mean_234)
            elif merge_type == '5':
                merge_yearly.append(mean_5)

        if return_merge == 'yearly':
            return merge_yearly
        elif return_merge == 'monthly':
            return merge_daily

        from scipy.stats import zscore

        from scipy.stats import pearsonr
        data_row_fireindex = pd.read_csv(
            r"D:\Mongolia RURom Baki Northeastern China-Merged RURom and Baikal-v2.csv", usecols=[0, 1])
        fireindex = data_row_fireindex[(data_row_fireindex['year'] >= date_base[0]) & (data_row_fireindex['year'] <= date_base[-1])]
        detrended2 = list(zscore(fireindex['nechinamogoliaSiberia']))

        loc_1900= 1900-date_base[0]
        r1, p1 = pearsonr(np.squeeze(merge_yearly)[:loc_1900], detrended2[:loc_1900])
        r2, p2 = pearsonr(np.squeeze(merge_yearly)[loc_1900:], detrended2[loc_1900:])
        if (r2 > r1) & (p2<0.1):
            return True
        else:
            return False


# path_cmip6 = r'G:\cmip6_concat/'
# path_pattern_cor = r'G:\cmip6_pattern_cor/'
#
#
# cmip6_filename = os.listdir(path_cmip6)
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     if len(sp) == 7:
#         cmip6_filename_splite.append(sp)
#
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
# print(np.unique(cmip6_filename_splite['2']).shape)
#
#
# file_in_model_member = []
#
# def find_file_name(x):
#     file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
#     file_in_model_member.append(file)
#
# cmip6_filename_splite.groupby([cmip6_filename_splite['2'],
#                             cmip6_filename_splite['4']]).apply(
#     lambda x: find_file_name(x))
#
#
# for i_File in file_in_model_member:
#     fileName_historical=[i for i in i_File if '_historical_' in i][0]
#     fileName_ssp=[i for i in i_File if '_ssp' in i]
#     exist_orNO = True
#     for ii_file in i_File:
#         exist_orNO = exist_orNO & os.path.exists(path_pattern_cor+ii_file.replace('.nc', '.mat'))
#     if exist_orNO:
#         print('continue')
#         continue
#     print(fileName_historical)
#     create_standard_pattern(fileName_historical, fileName_ssp)
#
# model_here = []
# for i_File in file_in_model_member:
#     fileName_historical=[i for i in i_File if '_historical_' in i][0]
#     fileName_ssp=[i for i in i_File if '_ssp' in i]
#
#     if Rvalue_pattern_cor_With_fireIndex(fileName_historical):
#         model_here.extend(i_File)
# print(model_here)
# exit()

"""
===============================
combine different run number into one
===============================
"""
#
path_cmip6 = r'G:\cmip6_FireIndex\cmip6_concat/'
path_z500_save = r'G:\cmip6_FireIndex/cmip6_oneModel/'
# #
# # cmip6_filename = ['zg_Amon_ACCESS-CM2_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-CM2_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r15i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r16i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r17i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r19i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r20i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r26i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r27i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r28i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r31i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r35i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r7i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r8i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r9i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_historical_r4i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-ESM2-1_ssp126_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp245_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp370_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp585_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CanESM5_historical_r10i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r15i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r21i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r22i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r3i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r6i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r7i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-1_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_historical_r3i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-CanOE_ssp126_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp245_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp370_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp585_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_historical_r1i1p5f1_gn_185001-201412.nc', 'zg_Amon_GISS-E2-1-G_ssp126_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_ssp245_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_ssp370_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_ssp585_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_historical_r2i1p1f2_gn_185001-201412.nc', 'zg_Amon_UKESM1-0-LL_ssp126_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp245_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp370_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp585_r2i1p1f2_gn_201501-210012.nc']
# cmip6_filename = ['zg_Amon_ACCESS-CM2_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-CM2_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_historical_r2i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-CM2_ssp126_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp245_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp370_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp585_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_historical_r3i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-CM2_ssp126_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp245_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp370_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp585_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_historical_r4i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-CM2_ssp126_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp245_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp370_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp585_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_historical_r5i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-CM2_ssp126_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp245_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp370_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-CM2_ssp585_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r11i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r12i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r13i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r14i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r15i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r16i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r16i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r17i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r18i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r19i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r19i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r20i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r21i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r23i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r24i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r25i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r25i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r25i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r25i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r25i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r26i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r26i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r27i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r27i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r28i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r28i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r29i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r29i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r29i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r29i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r29i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r2i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r30i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r30i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r30i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r30i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r30i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r31i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r31i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r32i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r32i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r32i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r32i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r32i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r33i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r33i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r33i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r33i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r33i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r34i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r34i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r34i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r34i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r34i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r35i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r35i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r36i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r36i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r36i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r36i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r36i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r37i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r37i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r37i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r37i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r37i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r38i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r38i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r38i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r38i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r38i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r39i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r39i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r39i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r39i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r39i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r4i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r4i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r5i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r6i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r6i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r6i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r6i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r6i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r7i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r8i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_historical_r9i1p1f1_gn_185001-201412.nc', 'zg_Amon_ACCESS-ESM1-5_ssp126_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp245_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp370_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_ACCESS-ESM1-5_ssp585_r9i1p1f1_gn_201501-210012.nc', 'zg_Amon_BCC-CSM2-MR_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_BCC-CSM2-MR_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_BCC-CSM2-MR_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_BCC-CSM2-MR_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_BCC-CSM2-MR_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_historical_r1i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-CM6-1_ssp126_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp245_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp370_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp585_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_historical_r3i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-CM6-1_ssp126_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp245_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp370_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp585_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_historical_r4i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-CM6-1_ssp126_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp245_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp370_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp585_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_historical_r5i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-CM6-1_ssp126_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp245_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp370_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp585_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_historical_r6i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-CM6-1_ssp126_r6i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp245_r6i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp370_r6i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-CM6-1_ssp585_r6i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_historical_r1i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-ESM2-1_ssp126_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp245_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp370_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp585_r1i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_historical_r2i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-ESM2-1_ssp126_r2i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp245_r2i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp370_r2i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp585_r2i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_historical_r3i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-ESM2-1_ssp126_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp245_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp370_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp585_r3i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_historical_r4i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-ESM2-1_ssp126_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp245_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp370_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp585_r4i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_historical_r5i1p1f2_gr_185001-201412.nc', 'zg_Amon_CNRM-ESM2-1_ssp126_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp245_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp370_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CNRM-ESM2-1_ssp585_r5i1p1f2_gr_201501-210012.nc', 'zg_Amon_CanESM5_historical_r10i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r10i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r10i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r10i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r10i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r10i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r10i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r11i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r11i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r11i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r11i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r11i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r11i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r11i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r12i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r12i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r12i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r12i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r12i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r12i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r12i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r13i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r13i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r13i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r13i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r13i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r13i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r13i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r14i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r14i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r14i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r14i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r14i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r14i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r14i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r15i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r15i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r16i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r16i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r16i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r16i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r16i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r17i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r17i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r17i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r17i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r17i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r17i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r17i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r18i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r18i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r1i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r20i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r20i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r20i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r20i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r20i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r20i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r20i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r21i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r21i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r21i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r21i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r22i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r22i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r23i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r23i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r23i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r23i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r23i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r23i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r23i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r24i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r24i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r24i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r24i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r24i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r24i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r24i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r25i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r25i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r25i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r25i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r25i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r2i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r2i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r3i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r3i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r5i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r5i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r6i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r6i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r7i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r7i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r7i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r7i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_historical_r8i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5_ssp126_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp245_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp370_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5_ssp585_r8i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-1_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_historical_r1i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-1_ssp126_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp245_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp370_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-1_ssp585_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_historical_r1i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-CanOE_ssp126_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp245_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp370_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp585_r1i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_historical_r2i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-CanOE_ssp126_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp245_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp370_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp585_r2i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_historical_r3i1p2f1_gn_185001-201412.nc', 'zg_Amon_CanESM5-CanOE_ssp126_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp245_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp370_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_CanESM5-CanOE_ssp585_r3i1p2f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-f3-L_historical_r1i1p1f1_gr_185001-201412.nc', 'zg_Amon_FGOALS-f3-L_ssp126_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_FGOALS-f3-L_ssp245_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_FGOALS-f3-L_ssp370_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_FGOALS-f3-L_ssp585_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_FGOALS-g3_historical_r1i1p1f1_gn_185001-201412.nc', 'zg_Amon_FGOALS-g3_ssp126_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp245_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp370_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp585_r1i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_historical_r2i1p1f1_gn_185001-201412.nc', 'zg_Amon_FGOALS-g3_ssp126_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp245_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp370_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp585_r2i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_historical_r3i1p1f1_gn_185001-201412.nc', 'zg_Amon_FGOALS-g3_ssp126_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp245_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp370_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_FGOALS-g3_ssp585_r3i1p1f1_gn_201501-210012.nc', 'zg_Amon_GFDL-ESM4_historical_r1i1p1f1_gr1_185001-201412.nc', 'zg_Amon_GFDL-ESM4_ssp126_r1i1p1f1_gr1_201501-210012.nc', 'zg_Amon_GFDL-ESM4_ssp245_r1i1p1f1_gr1_201501-210012.nc', 'zg_Amon_GFDL-ESM4_ssp370_r1i1p1f1_gr1_201501-210012.nc', 'zg_Amon_GFDL-ESM4_ssp585_r1i1p1f1_gr1_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_historical_r1i1p5f1_gn_185001-201412.nc', 'zg_Amon_GISS-E2-1-G_ssp126_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_ssp245_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_ssp370_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_GISS-E2-1-G_ssp585_r1i1p5f1_gn_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_historical_r1i1p1f1_gr_185001-201412.nc', 'zg_Amon_IPSL-CM6A-LR_ssp126_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp245_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp370_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp585_r1i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_historical_r2i1p1f1_gr_185001-201412.nc', 'zg_Amon_IPSL-CM6A-LR_ssp126_r2i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp245_r2i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp370_r2i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp585_r2i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_historical_r3i1p1f1_gr_185001-201412.nc', 'zg_Amon_IPSL-CM6A-LR_ssp126_r3i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp245_r3i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp370_r3i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp585_r3i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_historical_r6i1p1f1_gr_185001-201412.nc', 'zg_Amon_IPSL-CM6A-LR_ssp126_r6i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp245_r6i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp370_r6i1p1f1_gr_201501-210012.nc', 'zg_Amon_IPSL-CM6A-LR_ssp585_r6i1p1f1_gr_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_historical_r1i1p1f2_gn_185001-201412.nc', 'zg_Amon_UKESM1-0-LL_ssp126_r1i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp245_r1i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp370_r1i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp585_r1i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_historical_r2i1p1f2_gn_185001-201412.nc', 'zg_Amon_UKESM1-0-LL_ssp126_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp245_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp370_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp585_r2i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_historical_r3i1p1f2_gn_185001-201412.nc', 'zg_Amon_UKESM1-0-LL_ssp126_r3i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp245_r3i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp370_r3i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp585_r3i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_historical_r4i1p1f2_gn_185001-201412.nc', 'zg_Amon_UKESM1-0-LL_ssp126_r4i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp245_r4i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp370_r4i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_historical_r8i1p1f2_gn_185001-201412.nc', 'zg_Amon_UKESM1-0-LL_ssp126_r8i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp245_r8i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp370_r8i1p1f2_gn_201501-210012.nc', 'zg_Amon_UKESM1-0-LL_ssp585_r8i1p1f2_gn_201501-210012.nc']

cmip6_filename = os.listdir(path_cmip6)
cmip6_filename_splite = []
for i in cmip6_filename:
    sp = i.split('_')
    if len(sp) == 7:
        cmip6_filename_splite.append(sp)


cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)

# print(list(np.unique(cmip6_filename_splite['2'])))
cmip6_filename_splite = cmip6_filename_splite[(cmip6_filename_splite['3'] == 'ssp126')]

# def find_file_name(x):
#     print(x['2'].iloc[0])
#     print(x.shape)
#
#
# cmip6_filename_splite.groupby([cmip6_filename_splite['2'],
#                             cmip6_filename_splite['3']]).apply(
#     lambda x: find_file_name(x))
# exit()
# # a = ['ACCESS-CM2','ACCESS-ESM1-5','BCC-CSM2-MR','CNRM-CM6-1','CNRM-ESM2-1',
# #  'CanESM5', 'CanESM5-CanOE', 'FGOALS-f3-L', 'FGOALS-g3', 'GFDL-ESM4',
# #  'GISS-E2-1-G', 'IPSL-CM6A-LR', 'UKESM1-0-LL']
# # for i in np.unique(cmip6_filename_splite['2']):
# #     if not i in a:
# #         print(i)
# # exit()

# #
# #
cmip6_filename_use = cmip6_filename_splite[(cmip6_filename_splite['0'] == 'zg')
                                           & (cmip6_filename_splite['1'] == 'Amon')]
cmip6_filename_missing = []

# def find_file_name(x):
#     file_titile =  ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['5'], x['6'])]
#     file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
#     # get period of cmip6 file
#     a_all = []
#     for i_file in file:
#         data = xr.open_dataset(path_cmip6+i_file)
#         a = data['z500'].values
#
#         a_all.append(a)
#     a_all = np.nanmean(a_all, axis=0)
#     os.makedirs(path_z500_save, exist_ok=True)
#     save_standard_nc([a_all], ['z500'], times=data.time, lon=data.lon, lat=data.lat,
#                      path_save=path_z500_save, file_name=file_titile[0])

def find_file_name(x):
    file_titile =  ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['5'], x['6'])]
    file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'], x['6'])]
    # get period of cmip6 file
    a_all = []
    print(x['2'].iloc[0])
    for i_file in file:
        data = xr.open_dataset(path_cmip6+i_file)
        print(data.dims)
        return


cmip6_filename_use.groupby([cmip6_filename_use['2'],
                            cmip6_filename_use['3']]).apply(
    lambda x: find_file_name(x))
exit()

"""
===============================
rerun pattern_cor
===============================
"""

# path_cmip6 = r'G:\cmip6_FireIndex/cmip6_oneModel/'
# path_pattern_cor = r'G:\cmip6_FireIndex/cmip6_final_pattern_cor/'
# cmip6_filename = os.listdir(path_cmip6)
#
# cmip6_filename_splite = []
# for i in cmip6_filename:
#     sp = i.split('_')
#     cmip6_filename_splite.append(sp)
# cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
# cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
# cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)
#
#
# file_in_model_member = []
# def find_file_name(x):
#     file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'])]
#     file_in_model_member.append(file)
#
# cmip6_filename_splite.groupby([cmip6_filename_splite['2']]).apply(
#     lambda x: find_file_name(x))
#
#
# for i_File in file_in_model_member:
#     fileName_historical=[i for i in i_File if '_historical_' in i][0]
#     fileName_ssp=[i for i in i_File if '_ssp' in i]
#     exist_orNO = True
#     for ii_file in i_File:
#         exist_orNO = exist_orNO & os.path.exists(path_pattern_cor+ii_file.replace('.nc', '.mat'))
#     if exist_orNO:
#         print('continue')
#         continue
#     print(fileName_historical)
#     create_standard_pattern(fileName_historical, fileName_ssp)
#
# exit()
#


path_cmip6 = r'G:\cmip6_FireIndex/cmip6_oneModel/'
path_pattern_cor = r'G:\cmip6_FireIndex/cmip6_final_pattern_cor/'
cmip6_filename = os.listdir(path_cmip6)

cmip6_filename_splite = []
for i in cmip6_filename:
    sp = i.split('_')
    cmip6_filename_splite.append(sp)
cmip6_filename_splite = pd.DataFrame(cmip6_filename_splite)
cmip6_filename_splite.to_csv('cmip6_z500_split.csv')
cmip6_filename_splite = pd.read_csv('cmip6_z500_split.csv', index_col=0)


file_in_model_member = []
def find_file_name(x):
    file = ['_'.join(i) for i in zip(x["0"], x["1"], x['2'], x['3'], x['4'], x['5'])]
    file_in_model_member.append(file)
cmip6_filename_splite.groupby([cmip6_filename_splite['2']]).apply(
    lambda x: find_file_name(x))

merge_type = 'merge'

line_collection = []
for i_File in file_in_model_member:
    fileName_historical=[i for i in i_File if '_historical_' in i][0]
    fileName_ssp=[i for i in i_File if '_ssp' in i]
    print(fileName_ssp)
    s_line_collection = []
    date_base = np.arange(1850,2014+1)
    historical = list(Rvalue_pattern_cor_With_fireIndex(fileName_historical, return_merge='yearly', merge_type=merge_type))
    for i_ssp in fileName_ssp:
        date_base = np.arange(2015,2100+1)
        ssp = list(Rvalue_pattern_cor_With_fireIndex(i_ssp, return_merge='yearly', merge_type=merge_type))
        s_line_collection.append(historical+ssp)
    line_collection.append(s_line_collection)
line_collection = np.array(line_collection)
nc4.save_nc4(line_collection, 'line_collection_%s'%merge_type)
exit()

periods = np.array([[2024,2050],[2074,2100]])
loc_periods = periods-1850

fig, ax = plt.subplots(1, figsize=[9,4])
plt.subplots_adjust(top=0.88,
bottom=0.295,
left=0.11,
right=0.9,
hspace=0.2,
wspace=0.2)

labels = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
periods_name = [' near future', ' far future']
colors = ['tab:blue', 'tab:orange', 'tab:green']

xtick = []
xticklabels = []
loc_box = [1,2,3,4]
loc_box_dict = {'234':[1,2,3,4],
                '5':[1.2,2.2,3.2,4.2],
                'merge':[1.4,2.4,3.4,4.4]}

collection_for_legend = []
for iI, merge_type in enumerate(['234', '5', 'merge']):
    loc_box = loc_box_dict[merge_type]
    line_collection = nc4.read_nc4('line_collection_%s' % merge_type)
    periods_1 = line_collection[:,:,loc_periods[0,0]:loc_periods[0,1]+1]
    periods_2 = line_collection[:,:,loc_periods[1,0]:loc_periods[1,1]+1]
    periods = [periods_1, periods_2]
    collection_for_legend.append(periods[0][:,0].flatten())
    for iI_boxplot in range(2):
        for i_box in range(4):
            xtick.append(loc_box[i_box]+iI_boxplot*4)
            xticklabels.append(labels[i_box])
            bp = ax.boxplot(periods[iI_boxplot][:,i_box].flatten(), positions=[loc_box[i_box]+iI_boxplot*4],
                       medianprops={'color':'black'},patch_artist=True)

            for patch, color in zip(bp['boxes'], [colors[iI],colors[iI],colors[iI],colors[iI]]):
                patch.set_facecolor(color)
        loc_box = np.array(loc_box)+0.5


#
bp = ax.boxplot(collection_for_legend, positions=[1,1.2,1.4],
                medianprops={'color': 'black'}, patch_artist=True, labels=['234', '5', 'merge'])
for patch, color in zip(bp['boxes'], colors):
    patch.set_facecolor(color)
ax.legend(bp["boxes"], ['234', '5', 'merge'], loc='upper center', bbox_to_anchor=(0.5, 1.15), ncol=3)



ax.set_xticks(xtick)
ax.set_xticklabels(xticklabels,rotation=90)
ax.set_ylim([-3,3])

plt.title('near future                                                                  far future')
plt.show()
exit()

merge_type = 'merge'
periods = np.array([[2024,2050],[2074,2100]])
loc_periods = periods-1850
line_collection = nc4.read_nc4('line_collection_%s'%merge_type)

periods_1 = line_collection[:,:,loc_periods[0,0]:loc_periods[0,1]+1]
periods_2 = line_collection[:,:,loc_periods[1,0]:loc_periods[1,1]+1]
periods = [periods_1, periods_2]
fig, ax = plt.subplots(1, figsize=[5,4])
plt.subplots_adjust(top=0.88,
bottom=0.295,
left=0.11,
right=0.9,
hspace=0.2,
wspace=0.2)

labels = ['ssp126', 'ssp245', 'ssp370', 'ssp585']
periods_name = [' near future', ' far future']
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red']

xtick = []
xticklabels = []
loc_box = [[1,1.2,1.4,1.6],[2,2.2,2.4,2.6]]
for iI_boxplot in range(2):

    for i_box in range(4):
        xtick.append(loc_box[iI_boxplot][i_box])
        xticklabels.append(labels[i_box])
        ax.boxplot(periods[iI_boxplot][:,i_box].flatten(), positions=[loc_box[iI_boxplot][i_box]], whis=2.5)

ax.set_xticks(xtick)
ax.set_xticklabels(xticklabels,rotation=45)
ax.set_ylim([-3,3])
plt.suptitle('M'+merge_type)
plt.title('near future                       far future')
plt.show()
exit()


import matplotlib.pyplot as plt
import numpy as np

from scipy import interpolate

from scipy import ndimage

def fast_moving_average(x, y, types='gaussian'):
    """
    :param x:
    :param y:
    :param types: 'spline_xy', 'spline', 'gaussian'
    :return:
    """
    # convert both to arrays
    x_sm = np.array(x)
    y_sm = np.array(y)

    # resample to lots more points - needed for the smoothed curves
    x_smooth = np.linspace(np.min(x_sm), np.max(x_sm), 1000)

    # spline - always goes through all the data points x/y
    if types == 'spline_xy':
        tck = interpolate.splrep(x, y)
        y_spline = interpolate.splev(x_smooth, tck)
        return x_smooth, y_spline

    if types == 'spline':
        spl = interpolate.UnivariateSpline(x, y) # 'spline'
        return x_smooth, spl(x_smooth)

    if types == 'gaussian':
        sigma = 3
        x_g1d = ndimage.gaussian_filter1d(x_sm, sigma)
        y_g1d = ndimage.gaussian_filter1d(y_sm, sigma)
        return x_g1d,y_g1d
    return


loc_2015 = 2015-1850
loc_2015 = 0
line_collection = nc4.read_nc4('line_collection')[:,:,loc_2015:]
fig, ax = plt.subplots(1, figsize=[9,4])
x_row = np.arange(1850,2100+1)[loc_2015:]
labels = ['ssp126', 'ssp245', 'ssp370', 'ssp585']

colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red']
for i in range(4):
    data = line_collection[:,i]
    y1, y2 = np.percentile(data, [25,75], axis=0)
    avg = np.mean(data, axis=0)
    x = x_row
    # x, y1 = fast_moving_average(x_row, y1)
    # x, y2 = fast_moving_average(x_row, y2)
    # x, avg = fast_moving_average(x_row, avg)
    ax.fill_between(x, y1, y2, alpha=.2, linewidth=0, color=colors[i])
    ax.plot(x, avg, label=labels[i], color=colors[i])
plt.legend(loc='upper left')
plt.title('8 models')
plt.show()











