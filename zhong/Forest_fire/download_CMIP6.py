import matplotlib.pyplot as plt
import xarray as xr
import os
import pandas as pd
import numpy as np

"""
===============================
get characteristics of each cmip6 file
===============================
"""
# # cmip6_filename =np.loadtxt('final_cmip6.txt', dtype=str)
# cmip6_filename =os.listdir(r'H:/')
# cmip6_var = []
# cmip6_filename_var = []
# for i_cmip6file in cmip6_filename:
#     cmip6_var_s = []
#
#     try:
#         data = xr.open_dataset(r'H:/'+i_cmip6file)
#     except:
#         continue
#     cmip6_filename_var.append(i_cmip6file)
#     coords = list(data.coords)
#     variences = list(data.keys())
#     for i_plev in coords:
#         if 'lev' in i_plev:
#             cmip6_var_s.append(i_plev)
#     for i_var in variences:
#         if not 'bnd' in i_var:
#             cmip6_var_s.append(i_var)
#     cmip6_var.append(cmip6_var_s)
#
# cmip6_var = pd.DataFrame(cmip6_var, index=cmip6_filename_var)
# cmip6_var.to_csv('cmip6_z500_var.csv')
# exit()




exit()

"""
===============================
sigma-pressure coordination interpolation
===============================
"""

# from __future__ import print_function
# import Ngl
# import numpy
# import Nio
# import os

# if lev_unit == '1':
#     var=data['zg'].values
#     hyam = data['a'].values
#     hybm = data['b'].values
#     psrf = data['ps'].values
#     p0mb = data['p0'].values
# def sigma_pressure_coord(var, hyam, hybm, psrf, p0mb):
#     pnew = [500.]
#     #  Do the interpolation.
#     Tnew = Ngl.vinth2p(var,hyam,hybm,pnew,psrf,1,p0mb,1,True)
#     return Tnew












"""
===============================
Sigma to Pressure Interpolation
===============================

By using `metpy.calc.log_interp`, data with sigma as the vertical coordinate can be
interpolated to isobaric coordinates.
"""

from metpy.interpolate import log_interpolate_1d
from metpy.plots import add_metpy_logo, add_timestamp
from metpy.units import units


plevs = [500.] * units.hPa

import numpy as np
a = xr.open_dataset(r"G:\cmip6_concat\zg_Amon_ACCESS-CM2_ssp370_r1i1p1f1_gn_201501-210012.nc")

# 1, find lev, maybe lev/plev

print(a)
print(a.lev)

hgt = units.Quantity(a['zg'].values, a['zg'].units)
# 处理hgt数据为pa
levels = a.plev.values
pressure = np.ones(hgt.shape)
for i in range(pressure.shape[1]):
    pressure[:,i,:,:] = pressure[:,i,:,:]*levels[i]
pressure = units.Quantity(hgt, a.plev.units)

height = log_interpolate_1d(plevs, pressure, hgt, axis=1)
print(height.shape)
plt.imshow(height[0,0,:])
plt.show()


exit()


import numpy as np
a = np.loadtxt(r'cmip6.txt', dtype=str)
model = ['ACCESS-CM2','ACCESS-ESM1-5','AWI-CM-1-1-MR','BCC-CSM2-MR','CAMS-CSM1-0','CanESM5-p1','CanESM5-p2','CanESM5-CanOE-p2','CESM2','CESM2-WACCM','CIESM','CMCC-CM2-SR5','CNRM-CM6-1-f2','CNRM-CM6-1-HR-f2','CNRM-ESM2-1-f2','EC-Earth3','EC-Earth3-Veg','FGOALS-f3-L','FGOALS-g3','FIO-ESM-2-0','GFDL-CM4','GFDL-ESM4','GISS-E2-1-G-p1','GISS-E2-1-G-p3','HadGEM3-GC31-LL-f3','HadGEM3-GC31-MM-f3','INM-CM4-8','INM-CM5-0','IPSL-CM6A-LR','KACE-1-0-G','MCM-UA-1-0','MIROC6','MIROC-ES2L-f2','MPI-ESM1-2-HR','MPI-ESM1-2-LR','MRI-ESM2-0','NESM3','NorESM2-LM','NorESM2-MM','UKESM1-0-LL-f2']

collection = []
for i_model in model:
    i_model = '_'+i_model+'_'
    s_colle = []
    for j in a:
        if i_model in j:
            s_colle.append(j)
    print(i_model)
    print(s_colle)
    print(len(s_colle))
    collection.extend(s_colle)
print(len(collection))
np.savetxt('cmip6_tas.txt', collection, fmt='%s')