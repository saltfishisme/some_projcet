import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import xarray as xr

import nc4

path_MainData =  r'D:\OneDrive\basis\some_projects\zhong\AR_detection\basis_analysis\\'
path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_abc_loc = ['a) ', 'b) ', 'c) ', 'd) ']

def plot_line(df, bu_varis_all, save_name):
    if df.shape[0] < 20:
        return
    ylim = np.max(df[bu_varis_all].max().values)

    time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]
    time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
    time_abc_loc = ['a) ', 'b) ', 'c) ', 'd) ']
    bu_color = ['red', 'blue', 'yellow']
    fig, axs = plt.subplots(nrows=2, ncols=2)
    plt.subplots_adjust(top=0.94,
                        bottom=0.125,
                        left=0.07,
                        right=0.96,
                        hspace=0.35,
                        wspace=0.2)
    def plot_line(ax, sdf, vari):

        for iI_vari, ivari in enumerate(vari):
            ax.plot(sdf['year'], sdf[ivari], color=bu_color[iI_vari], alpha=0.3)
            coef = np.polyfit(sdf['year'], sdf[ivari],1)
            poly1d_fn = np.poly1d(coef)
            ax.plot(sdf['year'], poly1d_fn(sdf['year']), color=bu_color[iI_vari], linestyle='-', label=ivari)

            ax.set_ylim(0, ylim)

    for iI_ax, iseason in enumerate(time_Seaon_divide_Name):
        ax = axs[int(iI_ax/2)][int(iI_ax%2)]

        df_resample = pd.DataFrame(index=np.arange(1979,2021))
        sdf = pd.concat([df_resample, df[df['season']==iseason].set_index(df[df['season']==iseason]['year'])], axis=1)
        sdf = sdf.fillna(0)
        sdf['year'] = sdf.index
        # print(sdf)
        plot_line(ax, sdf, bu_varis_all)
        ax.set_title(time_abc_loc[iI_ax]+time_Seaon_divide_Name[iI_ax]+' '+bu_period_lack, loc='left')
    plt.legend(loc='upper center', bbox_to_anchor=(-0.07, -0.15),
               fancybox=True, shadow=True, ncol=5)
    # plt.show()
    plt.savefig(path_SaveData+'pic/%s_%s.png'%(save_name, bu_period_lack), dpi=500)
    plt.close()
''' ##################################### freq #####################################'''


#
# for bu_main_period_length in [3, 4, 5]:
#     for bu_lack_period_length in [1, 2, 3]:
#         bu_period_lack = '%i%i'%(bu_main_period_length, bu_lack_period_length)
#
#         df = pd.read_csv(path_MainData+'basis_analysis_%s.csv'%bu_period_lack)
#         if df.shape[0]<20:
#             continue
#
#         df.columns = ['season', 'year', 'freq', 'duration', 'duration_70']
#         bu_varis_all = ['freq', 'duration', 'duration_70']
#         # bu_varis_all = ['freq']
#
#         plot_line(df, bu_varis_all, 'ba')
# exit(0)
''' ##################################### IVT #####################################'''

def to_float(df):
    I = []
    for sI in df:
        sI = sI.split('%')
        sI = [float(i) for i in sI]
        I.append(sI)
    return I
bu_vari = 'tem'
vari_all = ['%s_mean'%bu_vari, '%s_max'%bu_vari, '%s_mean_70'%bu_vari, '%s_max_70'%bu_vari, '%s_mean_70_diff'%bu_vari]
if bu_vari == 'IVT':
    vari_all = ['%s_mean'%bu_vari, '%s_max'%bu_vari, '%s_mean_70'%bu_vari, '%s_max_70'%bu_vari]

for bu_main_period_length in [4]:
    for bu_lack_period_length in [2]:
        for impact_area in range(1, 7):
            bu_period_lack = '%i%i'%(bu_main_period_length, bu_lack_period_length)

            df = pd.read_csv(path_MainData+'%s_index_%s%s.csv'%(bu_vari, bu_period_lack, impact_area), index_col=0)
            df['year'] = [int(i[:4]) for i in df['time'].values]
            df['month'] = [int(i[4:6]) for i in df['time'].values]
            df['day'] = [int(i[6:8]) for i in df['time'].values]
            month_to_season_dct = {
                1: 'DJF', 2: 'DJF',
                3: 'MAM', 4: 'MAM', 5: 'MAM',
                6: 'JJA', 7: 'JJA', 8: 'JJA',
                9: 'SON', 10: 'SON', 11: 'SON',
                12: 'DJF'
            }
            df['season'] = [month_to_season_dct.get(i) for i in df['month']]

            single_index = []
            for s_vari in vari_all:
                df[s_vari] = to_float(df[s_vari].values)
                df[s_vari[4:]] = df[s_vari].apply(lambda x: np.nanmean(x))
                a= df[s_vari[4:]].groupby([df['season'], df['year']]).mean()
                single_index.append(a)

            df_single_index = pd.concat(single_index, axis=1)

            plot_line(df_single_index.reset_index(), ['max', 'max_70'], '%s_max_%s'%(bu_vari, impact_area))
            plot_line(df_single_index.reset_index(), ['mean', 'mean_70'], '%s_mean_%s'%(bu_vari, impact_area))
            plot_line(df_single_index.reset_index(), ['mean_70_diff'], '%s_diff_%s'%(bu_vari, impact_area))
''' ##################################### 2D frequency #####################################'''
# import matplotlib
#
# SMALL_SIZE = 7
# matplotlib.rc('font', size=SMALL_SIZE)
# matplotlib.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
# plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
# plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
#
# for bu_main_period_length in [4]:
#     for bu_lack_period_length in [2]:
#         bu_period_lack = '%i%i'%(bu_main_period_length, bu_lack_period_length)
#
#         data = nc4.read_nc4(path_MainData+'frequency_2D_%s'%bu_period_lack)
#         fig, axs = plt.subplots(nrows=2, ncols=2, sharex=True, figsize=[6,6])
#         print(np.max(data))
#         plt.subplots_adjust(top=0.89,
#                             bottom=0.05,
#                             left=0.07,
#                             right=0.98,
#                             hspace=0.385,
#                             wspace=0.19)
#
#         # definitions for the axes
#         left, width = 0, 1
#         bottom, height = 0.5, 0.5
#         spacing = 0.0015
#
#
#         rect_scatter = [left, bottom, width, height]
#         rect_histx = [left, bottom + height + spacing, width, 0.2]
#
#
#
#
#         for i in range(4):
#             ax = axs[int(i/2)][int(i%2)]
#
#             ax_histx = fig.add_axes(rect_histx, sharex=ax, )
#
#             from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
#
#             ip = InsetPosition(ax, rect_histx)
#             ax_histx.set_axes_locator(ip)
#
#             ax_histx.plot(np.arange(0,360), np.sum(data[i][:, ::2], axis=0))
#             ax_histx.set_ylim(0,450)
#             frame = plt.gca()
#             # # y 轴不可见
#             frame.axes.get_yaxis().set_visible(False)
#             # x 轴不可见
#             frame.axes.get_xaxis().set_visible(False)
#
#             ax.contourf(np.arange(0,360), np.arange(1979,2021), data[i][:, ::2], level=np.arange(0,5), extend='both')
#             ax_histx.set_title(time_abc_loc[i]+time_Seaon_divide_Name[i]+' '+bu_period_lack, loc='left')
#         # plt.show()
#         plt.savefig(path_SaveData+'pic/2d_frequency_%s.png'%(bu_period_lack), dpi=500)
#         plt.close()
#





