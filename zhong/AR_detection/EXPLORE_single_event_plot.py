import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import metpy.calc as mpcalc
"""
=============================
select a standard event based on some critieria.
1, temperature
2, intensity

=============================
"""

"""
=============================
get circulation and other characteristics.
=============================
"""

import datetime


def load_Q(s_time, var, types='climate'):
    """
    :param s_time: datetime64
    :param var:
    :param type: climate or now
    :return:
    """

    def create_save_adv_adi(time):
        import metpy.calc as mpcalc
        from metpy.units import units
        import numpy as np
        import xarray as xr
        import os

        def get_data(time, var, level_isobaric):
            '''
            :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
            :param level_isobaric:
            :return:
            '''

            def get_svar_data(var):
                data = xr.open_dataset(
                    path_PressureData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
                return data

            result = []
            if np.isin(var, 'Hgt').any():
                hght_var = get_svar_data('Hgt').sel(time=time, level=level_isobaric)['z']
                result.append(hght_var)
            if np.isin(var, 'Tem').any():
                temp_var = get_svar_data('Tem').sel(time=time, level=level_isobaric)['t']
                result.append(temp_var)
            if np.isin(var, 'Uwnd').any():
                u_wind_var = get_svar_data('Uwnd').sel(time=time, level=level_isobaric)['u']
                result.append(u_wind_var)
            if np.isin(var, 'Vwnd').any():
                v_wind_var = get_svar_data('Vwnd').sel(time=time, level=level_isobaric)['v']
                result.append(v_wind_var)
            if np.isin(var, 'Wwnd').any():
                w_wind_var = get_svar_data('Wwnd').sel(time=time, level=level_isobaric)['w']
                result.append(w_wind_var)

            lat_var = result[0]['latitude']
            lon_var = result[0]['longitude']
            result.append(lat_var)
            result.append(lon_var)
            return result

        def tem_advection_1000(time):
            temp_var, u_wind_var, v_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Uwnd', 'Vwnd'],
                                                                          level_isobaric=1000)

            # between lat/lon grid points
            dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)

            # Calculate temperature advection using metpy function
            adv = mpcalc.advection(temp_var * units.kelvin, u=u_wind_var * units('m/s'), v=v_wind_var * units('m/s'),
                                   dx=dx, dy=dy)
            return adv

        def adiabatic(time, level_isobaric=[1000, 850, 500, 200]):
            temp_var, w_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Wwnd'], level_isobaric=level_isobaric)

            pressure_var = np.ones([len(level_isobaric), len(lat_var), len(lon_var)])
            for i in range(len(level_isobaric)):
                # transform hPa into Pa
                pressure_var[i] = pressure_var[i] * level_isobaric[i] * 100
            pressure_var = pressure_var * units.Pa

            # Calculate temperature advection using metpy function
            adv = mpcalc.static_stability(pressure_var, temp_var * units.kelvin)

            Rd = units.Quantity(8.314462618, 'J / mol / K') / units.Quantity(28.96546e-3, 'kg / mol')
            adiabatic_warming = adv * (w_wind_var * units('Pa/s')) * pressure_var / Rd
            return adiabatic_warming

        s_advection = tem_advection_1000(time)
        s_adiabatic = adiabatic(time)

        s_year = int(time.strftime('%Y'))
        path_SaveData = r'/home/linhaozhong/work/AR_analysis/'
        os.makedirs(path_SaveData + 'adibatic_cli/%i' % s_year, exist_ok=True)
        os.makedirs(path_SaveData + 'advection_cli/%i' % s_year, exist_ok=True)
        s_adiabatic.to_netcdf(path_SaveData + 'adibatic_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))
        s_advection.to_netcdf(path_SaveData + 'advection_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))

    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q_withpath(s_time_path):
        now = xr.open_dataset(path_Qdata + '%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    if types == 'climate':
        cli = []
        for i_year in range(1979, 2020 + 1):
            # judge whether this date is valid
            try:
                datetime.datetime(i_year, int(s_time.strftime('%m')), int(s_time.strftime('%d')))
            except:
                print('%s is invalid' % (str(i_year) + s_time.strftime('%m%d%H')))
                continue

            s_time_str = var + '_cli/' + str(i_year) + '/' + str(i_year) + s_time.strftime('%m%d%H')
            if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
                a = True
            else:
                create_save_adv_adi(pd.to_datetime(str(i_year) + '-' + s_time.strftime('%m-%d %H:00:00')))
            cli.append(load_Q_withpath(s_time_str))
        cli = np.nanmean(np.array(cli), axis=0)
        now = load_Q_withpath(var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H'))
        return now - cli
    elif types == 'now':
        s_time_str = var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H')
        if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
            a = True
        else:
            create_save_adv_adi(s_time)
        now = load_Q_withpath(s_time_str)
        return now


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def get_mpv(date, pvORmpv='pv'):
    """date containing Hour information"""
    import scipy
    time = pd.to_datetime(date)
    # load hourly anomaly data
    vari_SaveName_all=['RH', 'Tem', 'Uwnd', 'Vwnd']

    data_row = xr.open_dataset(
        path_PressureData + '%s/' % 'Hgt' + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % ('Hgt', time.strftime('%Y%m%d')))

    for i in vari_SaveName_all:
        iia = xr.open_dataset(
        path_PressureData + '%s/' % i + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (i, time.strftime('%Y%m%d')))
        data_row = xr.merge([data_row, iia])

    # load present data
    data = data_row.sel(time=time.strftime('%Y%m%d %H:00:00'))

    data = data.sel(level=slice(500, 1000))
    data = data.metpy.parse_cf().squeeze()

    ##############################
    # Get the cross section, and convert lat/lon to supplementary coordinates:

    cross = data.set_coords(('latitude', 'longitude'))

    ##############################
    # For this example, we will be plotting potential temperature, relative humidity, and
    # tangential/normal winds. And so, we need to calculate those, and add them to the dataset:
    cross['dewpoint'] = mpcalc.dewpoint_from_relative_humidity(cross['t'], cross['r'])

    if pvORmpv == 'mpv':
        cross['e_Potential_temperature'] = mpcalc.equivalent_potential_temperature(
            cross['level'],
            cross['t'], cross['dewpoint']
        )
    else:
        cross['e_Potential_temperature'] = mpcalc.potential_temperature(
            cross['level'],
            cross['t']
        )


    cross['mpv'] = mpcalc.potential_vorticity_baroclinic(cross['e_Potential_temperature'], cross['level'],
                                                         cross['u'], cross['v'])
    return cross

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m
def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, level=[1000, 850, 500, 200]):
    """date containing Hour information"""
    import scipy
    time = pd.to_datetime(date)
    # load hourly anomaly data
    def load_anomaly(type):
        if type == 'daily':
            data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                               time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly':
            data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m'))[
                               time.strftime('%m')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly_now':

            def monthly_mean(var_dir, var_save, var_ShortName, time):
                path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir

                if os.path.exists(path_SaveData + '%s.mat' % time.strftime('%Y%m')):
                    return scio.loadmat(path_SaveData + '%s.mat' % time.strftime('%Y%m'))[time.strftime('%Y%m')]

                else:
                    date_montly_now = pd.date_range(time.strftime('%Y-%m-') + '01', freq='1D', periods=31)
                    dates = date_montly_now[date_montly_now.month == int(time.strftime('%m'))].strftime('%Y%m%d')
                    year = dates[0][:4]

                    def get_data(var_dir, i_Year, var_save, i_MonDay):
                        s_data = xr.open_dataset(
                            path_PressureData + '%s/%s/%s.%s%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
                        s_data = s_data.sel(level=level)
                        s_data = s_data[var_ShortName].values
                        return np.nanmean(s_data, axis=0)
                    data_in_MonDay = []

                    for iI_MonDay, i_MonDay in enumerate(dates):
                        # work for 0229
                        print(path_MainData + '%s/%s/%s.%s%s.nc' % (var_dir, year, var_save, year, i_MonDay[4:]))
                        s_data = get_data(var_dir, year, var_save, i_MonDay[4:])
                        data_in_MonDay.append(s_data)

                    data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
                    data_in_MonDay = np.array(data_in_MonDay, dtype='double')

                    path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir
                    os.makedirs(path_SaveData, exist_ok=True)
                    scipy.io.savemat(path_SaveData + '%s.mat' % time.strftime('%Y%m'), {time.strftime('%Y%m'): data_in_MonDay})

                    return data_in_MonDay

            data_anomaly = monthly_mean('Hgt', 'Hgt', 'z', time)


        return data_anomaly

    data_anomaly = load_anomaly('daily')

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

    return [data_present, data_anomaly]


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='climately'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    if types == 'climately':
        data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                           time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values
    if types == 'climately':

        return data_present-data_anomaly

    elif types == 'now':
        return data_present


def plot_six_axes(data, centroid, contour, plot_name, savename=''):
    SMALL_SIZE = 8
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
    import matplotlib as mpl

    mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
    # fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
    #                         figsize=[4, 5])
    fig = plt.figure(figsize=[5, 3.9])

    plt.subplots_adjust(top=0.97,
                        bottom=0.1,
                        left=0.005,
                        right=0.995,
                        hspace=0.14,
                        wspace=0.055)

    for level in range(2):
        for i_ax in range(3):
            import cmaps
            ax = fig.add_subplot(2, 3, level * 3 + i_ax + 1,
                                 projection=ccrs.Orthographic(centroid[1], 70))

            import matplotlib
            min_val, max_val = 0.1, 1.0
            n = 10
            orig_cmap = cmaps.prcp_1
            colors = orig_cmap(np.linspace(min_val, max_val, n))
            cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

            "==================================="
            data_max = np.nanmax(data[level * 3 + i_ax][contour>0])
            data_min = np.nanmin(data[level * 3 + i_ax][contour > 0])

            cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), data[level * 3 + i_ax],
                             transform=ccrs.PlateCarree(), cmap=cmap, extend='both', zorder=2,
                             vmax=data_max, vmin=data_min)

            ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(contour, 0.1),

                        colors='none', levels=[0.1, 1.1],
                        hatches=[10 * '.', 10 * '.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

            plt.colorbar(cb, orientation="horizontal")

            "==================================="

            ax.set_title('%s' % (plot_name[level * 3 + i_ax]))
            ax.set_extent([0, 359, 30, 90], crs=ccrs.PlateCarree())
            # ax.set_global()
            ax.coastlines(linewidth=0.3, zorder=10)
            # ax.stock_img()
            ax.gridlines(ylocs=[66], linewidth=0.3, color='black')
            # plt.show()

    # plt.show()

    plt.savefig(savename, dpi=400)
    plt.close()


def plot_one_axes_mpv(data, centroid, contour, plot_name, savename=''):
    SMALL_SIZE = 8
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
    import matplotlib as mpl

    mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
    # fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
    #                         figsize=[4, 5])
    fig = plt.figure()

    import cmaps
    ax = fig.add_subplot(111,
                         projection=ccrs.Orthographic(centroid[1], 70))

    import matplotlib
    min_val, max_val = 0.1, 1.0
    n = 10
    orig_cmap = cmaps.prcp_1
    colors = orig_cmap(np.linspace(min_val, max_val, n))
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

    # choose 500hPa

    cir_hgt_now = data[0] / 98
    cir_mpv = data[1] * 1000000
    lon = data[2][1]
    lat = data[2][0]

    cb = ax.contour(lon, lat, cir_hgt_now,
                    transform=ccrs.PlateCarree(), levels=data[3], colors='black',zorder=5)
    import cmaps
    cb = ax.contourf(lon, lat, cir_mpv,
                     transform=ccrs.PlateCarree(), levels=np.arange(-8,8,0.2), cmap=cmaps.temp_diff_18lev,
                     extend='both', zorder=2)

    ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(contour, 0.1),

                colors='none', levels=[0.1, 1.1],
                hatches=[5 * '.', 5 * '.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    plt.colorbar(cb, orientation="horizontal")

    "==================================="

    ax.set_title('%s' % (plot_name[0]))
    ax.set_extent([0, 359, 30, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66], linewidth=0.3, color='black', draw_labels=True)
    # plt.show()

    plt.savefig(savename, dpi=400)
    plt.close()


def plot_one_axes(data, centroid, contour, plot_name, savename=''):
    SMALL_SIZE = 8
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
    import matplotlib as mpl

    mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
    # fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
    #                         figsize=[4, 5])
    fig = plt.figure()


    import cmaps
    ax = fig.add_subplot(111,
                         projection=ccrs.Orthographic(centroid[1], 70))

    import matplotlib
    min_val, max_val = 0.1, 1.0
    n = 10
    orig_cmap = cmaps.prcp_1
    colors = orig_cmap(np.linspace(min_val, max_val, n))
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

    # choose 500hPa

    cir_hgt_now = data[0]/98
    cir_hgt_avg = data[1]/98
    print('11')
    cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.01,-0.5), cir_hgt_now, levels=np.arange(-50,51,2),
                     transform=ccrs.PlateCarree(), colors='black',zorder=5)
    print('223')
    # cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(510,600,5),
    #                  transform=ccrs.PlateCarree(), colors='black',zorder=5)
    # cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(110,155,5),
    #                  transform=ccrs.PlateCarree(), colors='black',zorder=5)
    # cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(1050,1250,10),
    #                  transform=ccrs.PlateCarree(), colors='black',zorder=5)

    # apply t-test
    print('224')
    cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5), cir_hgt_now-cir_hgt_avg,
                     levels=np.arange(-15, 15, 1),
                     transform=ccrs.PlateCarree(),cmap=cmap, extend='both',zorder=2)

    print('225')
    ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(contour, 0.1),

                colors='none', levels=[0.1, 1.1],
                hatches=[10 * '.', 10 * '.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)

    plt.colorbar(cb, orientation="horizontal")

    "==================================="
    print('22')
    ax.set_title('%s' % (plot_name[0]))
    ax.set_extent([0, 359, 30, 90], crs=ccrs.PlateCarree())
    # ax.set_global()
    ax.coastlines(linewidth=0.3, zorder=10)
    # ax.stock_img()
    ax.gridlines(ylocs=[66], linewidth=0.3, color='black',draw_labels=True)
    # plt.show()

    # plt.show()
    print(savename)
    print('333')
    plt.savefig(savename, dpi=400)
    plt.close()
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'



# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# path_MainData = r'/home/linhaozhong/work/AR_detection/'
# file_contour_Data = 'new1_'
# bu_addition_Name = ''
# path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
# path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
# resolution = 0.5
#
# path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# # path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
# path_moisture_path = r'/home/linhaozhong/work/AR_detection/AR_moisture_path_42/'
# path_savepath = r'/home/linhaozhong/work/AR_detection/'
# path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# # path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'


use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
plot_param = {'cbh': [['T2Mmean=', ' K'], 'CBH', [0, 120], 1],
              'hcc': [['SSHFmean=', ' W/$m^{2}$'], 'HCC', [0, 120], 1 ],
              'lcc': [['SLHFmea=', ' W/$m^{2}$'], 'LCC', [0, 120], 1],
              'mcc': [['ADVmean=', ' K/6h'], 'MCC', [0, 120], 1],
              'tcc': [['ABmean=', ' K/6h'], 'TCC', [0, 120], 1],
              'tciw': [['IVTmean=', ' kg/(m*s)'], 'TCIW', [0, 120], 1],
              'tclw': [['TCWVmean=', ' kg/$m^{2}$'], 'TCLW', [0, 120], 1, ],

                't2m':[['T2Mmean=',' K'],'T2M', [0, 120], 1],
              'sshf': [['SSHFmean=', ' W/$m^{2}$'], 'SSHF', [0, 120], 1 / (6 * 3600)],
              'slhf': [['SLHFmea=', ' W/$m^{2}$'], 'SLHF', [0, 120], 1 / (6 * 3600)],
              'advection': [['ADVmean=', ' K/6h'], 'advection', [0, 120], 6 * 3600],
              'adibatic': [['ABmean=', ' K/6h'], 'adiabatic', [0, 120], 6 * 3600],
              'IVT': [['IVTmean=', ' kg/(m*s)'], 'IVT', [0, 120], 1],
              'tcwv': [['TCWVmean=', ' kg/$m^{2}$'], 'TCWV', [0, 120], 1, ],
              'ssr': [['TCWVmean=', ' W/$m^{2}$'], 'TCWVano', [0, 120], 1 / (6 * 3600)],
              'str': [['STRmean=', ' W/$m^{2}$'], 'STR', [0, 120], 1 / (6 * 3600)],
              'siconc': [['SICmean=', ' %'], 'SIC', [0, 120], 100],
                'Hgt': [['Hgtmean=', ' %'], 'Hgt', [0, 120], 1],
              }
var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
                'hcc': 'high_cloud_cover',
                'lcc': 'low_cloud_cover',
                'mcc': 'medium_cloud_cover',
                'tcc': 'total_cloud_cover',
                'tciw': 'total_column_cloud_ice_water',
                'tclw': 'total_column_cloud_liquid_water',
'Hgt': 'z',

                }


def main_linePhase(var, types='now'):
    def contour_cal(time_file, var='IVT', save_dir=''):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        # info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))
        info_AR_row = xr.open_dataset(path_MainData + '/%sAR_contour_42/%s' % (file_contour_Data, time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        if bu_all_or_AR == 'AR':
            all_or_AR = [time_AR, info_AR_row['contour_AR'].values, info_AR_row['centroid_AR'].values,
                         info_AR_row['longpath'].values]
        else:
            all_or_AR = [time_all, info_AR_row['contour_all'].values, info_AR_row['centroid_all'].values]

        def cc_call_date_now(s_time, types, s_var):
            """
            to avoid too many Duplication
            :param s_time: pd.datetime
            :param types: now, climate
            :return:
            """
            if (s_var == 'advection') | (s_var == 'adibatic'):
                data = load_Q(
                    s_time,
                    s_var, types=types)
            elif s_var == 'IVT':
                data = load_ivt_total(
                    s_time)
            elif s_var == 'Hgt':
                data = get_circulation_data_based_on_pressure_date(s_time,s_var,
                    var_LongName[s_var],
                     level=[1000])
            else:
                data = get_circulation_data_based_on_date(
                    s_time,
                    var_LongName[s_var],
                    s_var, types=types)
            return data

        '''
        ####################
        loop time
        ####################
        '''
        time_loop = all_or_AR[0]

        if var[0] == 'mpv':
            for s_I_AR, datetime_now in enumerate(time_loop):
                datetime_now = pd.to_datetime(datetime_now)
                i_contour = all_or_AR[1][s_I_AR]
                i_centroid = all_or_AR[2][s_I_AR]

                cross_mpv = get_mpv(datetime_now)
                cross_mpv = cross_mpv.reset_coords('metpy_crs', drop=True)



                save_name = save_dir + '%s' \
                            % (datetime_now.strftime('%Y%m%d_%H'))
                cross_mpv.to_netcdf(save_name+'.nc')

                # def select_rectangle_area(data, centroid, long, lat, r_lat=20, r_lon=30):
                #     def roll_longitude_from_359to_negative_180(data, long):
                #         roll_length = np.argwhere(long < 180).shape[0] - 1
                #         long = np.roll(long, roll_length)
                #         data = np.roll(data, roll_length)
                #         long = xr.where(
                #             long > 180,
                #             long - 360,
                #             long)
                #         return data, long
                #
                #     if r_lon != -999:
                #         lon_index = np.argwhere(long == centroid[1])[0]
                #         if (lon_index - r_lon < 0) | (lon_index + r_lon > long.shape[0]):
                #             data, long = roll_longitude_from_359to_negative_180(data, long)
                #             centroid[1] = (centroid[1] + 180) % 360 - 180
                #
                #     long_2d, lat_2d = np.meshgrid(long, lat)
                #     lat_index = np.argwhere(lat == centroid[0])[0]
                #     lon_index = np.argwhere(long == centroid[1])[0]
                #     result = []
                #     for i_data in data:
                #         s_data = i_data[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])),
                #                  :]
                #         lat_2d_flatten = lat_2d[
                #                          int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])),
                #                          0]
                #         if r_lon != -999:
                #             s_data = s_data[:, int(lon_index - r_lon):int(lon_index + r_lon)]
                #             long_2d_flatten = long_2d[0, int(lon_index - r_lon):int(lon_index + r_lon)]
                #         result.append(s_data)
                #     if r_lon == -999:
                #         long_2d_flatten = long
                #     return result, [lat_2d_flatten, long_2d_flatten]
                #
                # import metpy
                # mpv_k265 = metpy.interpolate.interpolate_to_isosurface(cross_mpv['e_Potential_temperature'].values,
                #                                                 cross_mpv['mpv'].values,
                #                                                 250)
                # if not os.path.exists(save_name + '_k250.jpg'):
                #     cross = cross_mpv.sel(level=1000)
                #
                #     cross_rec, [lat_rec, lon_rec] = select_rectangle_area([cross['z'].values,
                #                                                            mpv_k265],
                #                                                           [int(i_centroid[0]), int(i_centroid[1])],
                #                                                           cross['longitude'].values,
                #                                                           cross['latitude'].values,
                #                                                           r_lat=50, r_lon=-999)
                #     plot_one_axes_mpv([cross_rec[0],
                #                        cross_rec[1], [lat_rec, lon_rec], np.arange(-100,100,5)],
                #                       i_centroid, i_contour, var, save_name+'_k250.jpg')



                # if not os.path.exists(save_name + '_1000.jpg'):
                #     cross = cross_mpv.sel(level=1000)
                #
                #     cross_rec, [lat_rec, lon_rec] = select_rectangle_area([cross['z'].values,
                #                                                            cross['mpv'].values],
                #                                                           [int(i_centroid[0]), int(i_centroid[1])],
                #                                                           cross['longitude'].values,
                #                                                           cross['latitude'].values,
                #                                                           r_lat=50, r_lon=-999)
                #     plot_one_axes_mpv([cross_rec[0],
                #                        cross_rec[1], [lat_rec, lon_rec], np.arange(-100,100,5)],
                #                       i_centroid, i_contour, var, save_name+'_1000.jpg')
                #
                # if not os.path.exists(save_name + '_850.jpg'):
                #     cross = cross_mpv.sel(level=850)
                #
                #     cross_rec, [lat_rec, lon_rec] = select_rectangle_area([cross['z'].values,
                #                                                            cross['mpv'].values],
                #                                                           [int(i_centroid[0]), int(i_centroid[1])],
                #                                                           cross['longitude'].values,
                #                                                           cross['latitude'].values,
                #                                                           r_lat=25*2, r_lon=-999)
                #     plot_one_axes_mpv([cross_rec[0],
                #                        cross_rec[1], [lat_rec, lon_rec], np.arange(100,300,5)],
                #                       i_centroid, i_contour, var, save_name + '_850.jpg')

            return

        # provided climately
        if types == 'now':
            data_climately = []
            for s_var in var:
                data_climately.append(cc_call_date_now(pd.to_datetime(time_loop[0]) - np.timedelta64(delay_time, 'D'),
                                                       types=types,
                                                       s_var=s_var))

        for s_I_AR, datetime_now in enumerate(time_loop):
            datetime_now = pd.to_datetime(datetime_now)
            contour = all_or_AR[1][s_I_AR]

            data = []
            for sI_var, s_var in enumerate(var):
                # provided climately
                if types == 'acceleration':
                    data_climately = cc_call_date_now(datetime_now - np.timedelta64(3, 'D'), types='now',
                                                      s_var=s_var)
                    data_now = cc_call_date_now(datetime_now, types='now',
                                                s_var=s_var)
                    data_now = data_now-data_climately
                else:
                    data_now = cc_call_date_now(datetime_now, types=types,
                                                s_var=s_var)



                if types == 'now':
                    data_now = data_now - data_climately[sI_var]

                data.append(data_now*plot_param[s_var][-1])


            save_name = save_dir+'%s.jpg' \
                        % (datetime_now.strftime('%Y%m%d_%H'))
            # if s_var != 'Hgt':
            #     plot_six_axes(data, all_or_AR[2][s_I_AR], contour, var, save_name)
            # else:
            #     plot_one_axes([data[0][0][0], data[0][1][0]], all_or_AR[2][s_I_AR], contour, var, save_name)
            print('?')
            nc4.save_nc4(np.array([data[0][0][0], data[0][1][0]]), '/home/linhaozhong/work/AR_NP85/test_hgt')
            scio.savemat('/home/linhaozhong/work/AR_NP85/test_AR.mat', {'contour':contour, 'centroid':all_or_AR[2][s_I_AR],
                                                                        'longpath':all_or_AR[3][s_I_AR]})
            # nc4.save_nc4(contour, '/home/linhaozhong/work/AR_NP85/test_AR')
            exit(0)
            try:
                if s_var != 'Hgt':
                    plot_six_axes(data, all_or_AR[2][s_I_AR], contour, var, save_name)
                else:
                    print('?')
                    nc4.save_nc4(data[0], '/home/linhaozhong/work/AR_NP85/test_hgt')
                    nc4.save_nc4(contour, '/home/linhaozhong/work/AR_NP85/test_AR')
                    exit(0)
                    plot_one_axes([data[0][0][0], data[0][1][0]], all_or_AR[2][s_I_AR], contour, var, save_name)
            except:
                return

        return

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)


        for iI, i in enumerate(df.columns):
            if int(i)!=1:
                times_file_all = df[i].values

                counts = 10000
                counts_now = 0
                for time_file in times_file_all:
                    if counts_now > counts:
                        continue

                    if len(str(time_file)) < 5:
                        continue
                    # mat file always has some bugs with str type. skip whitespace
                    time_file_new = ''
                    for i in time_file:
                        time_file_new += i
                        if i == 'c':
                            break
                    import shutil


                    # new_file = '/home/linhaozhong/work/AR_NP85//pic_single_event_mpv_%s/%s/%s/' % (var[0], df.columns[iI], '1991020705_1.nc')
                    # os.makedirs(new_file, exist_ok=True)
                    # # s_ii = contour_cal(time_file, var, new_file)
                    # s_ii = contour_cal('1991020705_1.nc', var, new_file)
                    # exit(0)

                    new_file = '/home/linhaozhong/work/AR_NP85//pic_single_event_%s/%s/%s/' % (var[0], df.columns[iI], time_file_new)

                    os.makedirs(new_file, exist_ok=True)
                    counts_now += 1
                    s_ii = contour_cal(time_file, var, new_file)


for delay_time in [5]:
    types = 'climately'  # 'acceleration'/ 'now'/ 'climately'
    bu_all_or_AR = 'all'
    # main_linePhase('ssr', types)
    # main_linePhase('slhf', types)
    # main_linePhase('sshf', types)
    # main_linePhase(['Hgt'], types)
    main_linePhase(['mpv'], types)
    # main_linePhase(['t2m', 'IVT', 'sshf', 'slhf', 'str', 'siconc'], types)

    # main_linePhase(['t2m', 'advection', 'sshf', 'slhf', 'str', 'siconc'], types)
    # main_linePhase('ivt', types)
    # main_linePhase('advection', types)
    # main_linePhase('adibatic', types)
    # main_linePhase('str', types)
    # main_linePhase('siconc', types)
    # main_linePhase('cbh', types)
    # main_linePhase('hcc', types)
    # main_linePhase('lcc', types)
    # main_linePhase('mcc', types)
    # main_linePhase('tcc', types)
    # main_linePhase('tciw', types)
    # main_linePhase('tclw', types)
exit(0)

# # bu_all_or_AR = 'AR'
# # main_linePhase('str', types)
# # main_linePhase('advection', types)
# # main_linePhase('adibatic', types)
# # main_linePhase('ssr', types)
# # main_linePhase('slhf', types)
# # main_linePhase('sshf', types)
# # main_linePhase('t2m', types)
# # main_linePhase('sic', types)
# exit(0)
