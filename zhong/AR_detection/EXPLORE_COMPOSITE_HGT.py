import os
import scipy.io as scio
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from function_shared import get_data_output_dataarray, select_rectangle_area
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
from Setting_mpv import *


def projection_transform_test(lon, lat, centroid, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    proj_Out = {'proj': 'lcc', 'lon_0': int(centroid[1]),
                'lat_1': int(centroid[0])}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)


def projection_transform_relative_coor(lon_2d, lat_2d, points):
    lon, lat = points
    lon_2d = lon_2d[0, :] - lon
    lat_2d = lat_2d[:, 0] - lat
    arg_lon = np.argmin(np.abs(lon_2d))
    arg_lat = np.argmin(np.abs(lat_2d))
    return [arg_lon, arg_lat]


def contour_overlap_or_not(contour, mask):
    # way 1
    if np.logical_and(contour == 1, mask == 1).any():
        return True
    else:
        return False

    # way 2
    def contour_to_edge_relative_coords(should_roll_matirx, contour):
        """
        transform contour from lat ,lon coordination to relative coordination.
        if the contour pass though the edge, roll to the central part.
        :param contour: contour
        :param position:
        :param lon_2d:
        :param lat_2d:
        :return:
        """
        roll = False
        if np.isin(should_roll_matirx[:, 0], [1]).any():
            roll = True
            contour = np.roll(contour, int(180 / resolution), axis=1)
        import cv2
        im_bw = cv2.threshold(contour, 0, 255, cv2.THRESH_BINARY)[1]
        im_bw = np.array(im_bw, dtype=np.uint8)
        contours, _ = cv2.findContours(im_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        polygons = []
        for object in contours:
            if object.shape[0] > 200:
                from shapely.geometry import MultiPolygon, Polygon
                polygons.append(Polygon(np.squeeze(object)))
        return polygons

    contour_arg = contour_to_edge_relative_coords(contour, contour)

    from shapely.geometry import MultiPolygon
    p1 = MultiPolygon(contour_arg)

    # load EU continent
    mask_arg = contour_to_edge_relative_coords(contour, mask)
    p2 = MultiPolygon(mask_arg)

    try:
        return p1.intersects(p2)
    except:
        for poly in p2.geoms:
            x, y = poly.exterior.xy
            plt.plot(x, y)
        for poly in p1.geoms:
            x, y = poly.exterior.xy
            plt.plot(x, y)
        plt.savefig(path_saveData + 'test.png')
        exit(0)
    return p1.intersects(p2)


var_with_shortname = {'Hgt': 'z', 'Tem': 't', 'Uwnd': 'u', 'Vwnd': 'v', 'RH': 'r'}
path_contourData = r'/home/linhaozhong/work/AR_NP85/AR_contour_42/'
composite_prssure_level = 1000
path_saveData = r'/home/linhaozhong/work/AR_NP85/nowindzone/'
path_EU_Data = r'/home/linhaozhong/work/AR_NP85/EU_mask.mat'


def main_3phase():
    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_contourData + time_file_new)

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        time_use = np.int(len(time_all[AR_loc:])/2)
        time_use = [time_all[AR_loc],time_all[AR_loc+time_use], time_all[-1]]

        for i_time in [time_use[phase]]:
            i_contour = info_AR_row['contour_all'].sel(time_all=i_time).values

            i_centroid = info_AR_row['centroid_all'].sel(time_all=i_time).values

            i_time = pd.to_datetime(i_time)
            from function_shared import get_circulation_data_based_on_pressure_date
            cross = get_circulation_data_based_on_pressure_date(pd.to_datetime(i_time), 'Hgt', 'z')

            "###############################"
            return cross
        "###############################"
        # plot_contour(uv_in_AR(), cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values, cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values,cross['pv'].values)

        return

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            if i != '000':
                times_file_all = df[i].values

                composite_result_z = []
                composite_result_z_ano = []
                centroid = []
                print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
                for time_file in times_file_all:
                    if len(str(time_file)) < 5:
                        continue
                    a_all = contour_cal(time_file, i)

                    composite_result_z.append(a_all[0][2])
                    composite_result_z_ano.append(a_all[1][2])

                # centroid = np.array(centroid, dtype='double')
                # from function_shared import roll_longitude_from_359to_negative_180
                # if (i=='0') | (i=='2'):
                #     centroid[:, 0] = roll_longitude_from_359to_negative_180(centroid[:, 0])
                # centroid = np.mean(centroid, axis=0)
                composite_result_z = np.array(composite_result_z, dtype='double')
                composite_result_z_ano = np.array(composite_result_z_ano, dtype='double')
                from scipy import stats
                _, p_values = stats.ttest_ind(composite_result_z, composite_result_z_ano, axis=0, equal_var=False)
                composite_result_z_ano = np.nanmean(composite_result_z_ano, axis=0)
                composite_result_z = np.nanmean(composite_result_z, axis=0)


                scio.savemat(path_saveData + 'composite_%s_%i_global_500.mat' % (i, phase),
                             {'centroid': centroid, 'data': [composite_result_z, composite_result_z_ano, p_values]})
                # nc4.save_nc4(composite_result,path_saveData+'composite_%s'%i)


# for phase in [0,1,2]:
#     main_3phase()
# exit()
#
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib

SMALL_SIZE = 6

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
plt.rcParams['hatch.color'] = 'black'

fig = plt.figure(figsize=[6.5,5])
plt.subplots_adjust(top=0.95,
bottom=0.08,
left=0.035,
right=0.99,
hspace=0.2,
wspace=0.2)
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
var_with_shortname = {'Hgt':'z', 'Tem':'t', 'Uwnd':'u', 'Vwnd':'v', 'RH':'r'}
central_lon = [0,200,180,0,0,0]
central_lat = [90, 70, 70, 90, 90, 90]
import nc4
import scipy.io as scio
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE','BAF','BSE' , 'BSW', 'MED', 'ARC']
type_name_num = ['0','2','3','4','5','6']
import cmaps
cmpas_here = cmaps.cmp_b2r
long = np.arange(0, 360, 0.5)
lat = np.arange(90, -90.01, -0.5)
subplot_number = 1
for i in np.arange(6):

    # centroid = a['centroid']
    # long_c, lat_c = centroid[0][:2]
    # print(centroid)
    # long = np.arange(long_c-45, long_c+45,0.5)
    # lat = np.arange(lat_c-40, lat_c+40, 0.5)

    def plot_quiver():
        plt.rcParams['hatch.color'] = 'black'
        # ax = fig.add_subplot(3,4, subplot_number, projection=ccrs.Orthographic(central_lon[i], central_lat[i]))
        # ax = fig.add_subplot(3,4, subplot_number, projection=ccrs.NorthPolarStereo(central_longitude=0))
        ax = fig.add_subplot(3,4, subplot_number, projection=ccrs.Orthographic(0, 90))

        # i_contour_cut, z, z_ano= data
        z, z_ano,p_values = data1



        import matplotlib.colors as colors
        # cb = ax.contourf(long, lat,
        #                  np.array((z),dtype='double')/98,cmap=cmaps.cmp_b2r,
        #                 transform=ccrs.PlateCarree(), extend='both')
        cb = ax.contourf(long, lat,
                         np.array((z-z_ano),dtype='double')/98,levels=np.arange(-9, 9.1, 0.5),cmap=cmpas_here,
                        transform=ccrs.PlateCarree(), extend='both',norm=colors.CenteredNorm())
        cl = ax.contour(long, lat,
                         np.array((z),dtype='double')/98, levels=np.arange(510,600,10),colors='black',
                        transform=ccrs.PlateCarree(), norm=colors.CenteredNorm(), linewidths=0.5)
        ax.contourf(long, lat, np.ma.masked_greater(p_values, 0.1),
                    colors='none', levels=[0, 0.1],
                    hatches=[3 * '.', 3 * '.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
        # ax.clabel(cl,fmt='%i')
        # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
        #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
        #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
        import cmaps
        # cb = ax.contourf(long, lat,
        #                  uv_in_AR(u, v, i_contour_cut), extend='both', levels=np.arange(0, 15.1, 0.5),
        #                  transform=ccrs.PlateCarree(), cmap=cmaps.cmocean_matter, zorder=3)
        # plt.colorbar(cb)
        # ax.quiver(long[::delta], lat[::delta_lat],
        #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
        #           transform=ccrs.PlateCarree(), scale=200, zorder=7)
        ax.set_title(figlabelstr[subplot_number-1]+plot_name[i]+' phase %i'%(phase+1),fontsize=7)
        ax.coastlines(zorder=9, linewidth=0.5)
        ax.gridlines()
        # ax.set_extent()
        # plt.show()
        # plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\pic_combine_result/'+
        #             plot_name[i]+' phase %i.png'%phase, dpi=400)
        # plt.close()
        return cb

    phase = 0
    a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\composite_%s_%i_global_500.mat"%(type_name_num[i], phase))
    # b= scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\composite_%s_%i_global.mat"%(type_name_num[i], phase))
    data1 =a['data']
    plot_quiver()
    subplot_number += 1
    phase = 1
    a= scio.loadmat(r"G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\composite_%s_%i_global_500.mat"%(type_name_num[i], phase))
    # b= scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\composite_%s_%i_global.mat"%(type_name_num[i], phase))
    data1 =a['data']
    cb = plot_quiver()
    subplot_number += 1
    # plt.show()

cb_ax = fig.add_axes([0.25, 0.05, 0.5, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
cb_ax.set_xlabel('gpm')
# plt.show()
plt.savefig('figure_6_revised.png',dpi=500)
exit(0)
