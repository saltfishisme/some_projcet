import pandas as pd
import numpy as np
import scipy.io
import xarray as xr
import os
import matplotlib.pyplot as plt
# bu_study_box = [np.arange(80,120), np.arange(20,50)]  # longitude and latitude that a AR_detection event must spread across
bu_study_box = [np.arange(20,180), np.arange(0,50)]  # longitude and latitude that a AR_detection event must spread across
path_MainData = r'/home/linhaozhong/work/AR_detection_China/' # Path of AR_contour_32_all_ll folder
path_IVT_Data = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/' # Path of IVT folder (era5)
path_SaveData = r'/home/linhaozhong/work/AR_detection_China/'


bu_addition_Name = 'all_ll'

def basis_analysis(bu_length_lack):

    def load_ivt_total(s_time):
        '''
        :param s_time: datetime64
        :return: ivt_total
        '''
        s_time = pd.to_datetime(s_time)


        s_time_str = s_time.strftime('%Y%m%d')
        index_vivt = xr.open_dataset(path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time_str[:4], s_time_str))
        index_uivt = xr.open_dataset(path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time_str[:4], s_time_str))
        # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
        # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

        ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
        ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

        ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
        return ivt_total

    times_file_all = np.array(os.listdir(path_MainData+'AR_contour_%s_%s_row/'%(bu_length_lack, bu_addition_Name)))
    time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

    def IVT_intensity(times_file_all):
        tem_index_mean = []
        tem_index_max = []

        time_FileName = []
        time_begin = []
        time_end = []

        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s_%s/%s'%(bu_length_lack, bu_addition_Name, s_time_all))
            print(path_SaveData+'AR_contour_%s_%s/%s'%(bu_length_lack, bu_addition_Name, s_time_all))
            '''######## skips AR_detection events out of study_box '''
            IVT_long = info_AR.lon.values
            IVT_lat = info_AR.lat.values
            ar_pathway_index=np.argwhere(info_AR.vari.values !=0)
            if len(bu_study_box[0])!=0:
                if (np.in1d(IVT_long[np.unique(ar_pathway_index[:, 2])], bu_study_box[0]).any()) != True:
                    continue
            if len(bu_study_box[1])!=0:
                if (np.in1d(IVT_lat[np.unique(ar_pathway_index[:,1])], bu_study_box[1]).any()) != True:
                    continue

            '''######## get IVT mean & max from every contour '''
            s_time_loop = info_AR.time.values

            s_tem_index_mean_all = []
            s_tem_index_max_all = []

            for s_time in s_time_loop:
                s_time = pd.to_datetime(s_time)

                ivt_total = load_ivt_total(s_time)
                charater_AR = info_AR.sel(time=s_time)['vari'].values

                tem_index_used_grid = np.ma.masked_where(charater_AR==0, ivt_total)

                s_tem_index_mean_all.append(np.nanmean(tem_index_used_grid))
                s_tem_index_max_all.append(np.nanmax(tem_index_used_grid))

            tem_index_mean.append(np.nanmean(s_tem_index_mean_all))
            tem_index_max.append(np.nanmax(s_tem_index_max_all))
            time_FileName.append(s_time_all[:-3])
            time_begin.append(pd.to_datetime(s_time_loop[0]).strftime('%Y%m%d %H'))
            time_end.append(pd.to_datetime(s_time_loop[-1]).strftime('%Y%m%d %H'))

        df  = pd.DataFrame()
        df['begin'] = time_begin
        df['end'] = time_end
        df['IVT_intensity_Mean'] = tem_index_mean
        df['IVT_intensity_Max'] = tem_index_max
        df['FileName'] = time_FileName

        df = df.sort_values(by = ['begin'])
        os.makedirs(path_SaveData+'basis_analysis/')
        df.to_csv(path_SaveData+'basis_analysis/analysis.csv', index=False)

    def centroid(times_file_all):

        area_S = scipy.io.loadmat(path_MainData+'GridAL_ERA0_5degGridC.mat')['S']
        area= np.zeros([area_S.shape[0]+1, area_S.shape[1]])
        area[:-1, :] = area_S
        area[-1,:] = area_S[-1,:]

        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'new_AR_contour_%s%s/%s'%(bu_length_lack, bu_addition_Name, s_time_all))
            '''######## skips AR_detection events out of study_box '''
            IVT_long = np.arange(0,360,0.5)
            IVT_lat = info_AR.lat.values


            ar_pathway_index=np.argwhere(info_AR.vari.values !=0)
            if len(bu_study_box[0])!=0:
                if (np.in1d(IVT_long[np.unique(ar_pathway_index[:, 2])], bu_study_box[0]).any()) != True:
                    continue
            if len(bu_study_box[1])!=0:
                if (np.in1d(IVT_lat[np.unique(ar_pathway_index[:,1])], bu_study_box[1]).any()) != True:
                    continue

            '''######## get IVT mean & max from every contour '''
            s_time_loop = info_AR.time.values

            centroids = []
            times = []

            for s_time in s_time_loop:
                s_time = pd.to_datetime(s_time)

                ivt_total = load_ivt_total(s_time)
                charater_AR = info_AR.sel(time=s_time)['vari'].values

                ivt_total[charater_AR==0] = np.nan
                s_area = area.copy()
                s_area[charater_AR==0] = np.nan
                long, lat = np.meshgrid(IVT_long, IVT_lat)
                long = np.array(long,dtype='double')
                lat = np.array(lat,dtype='double')

                long_AR_index = np.argwhere(charater_AR != 0)[:,1]

                long_AR_index = sorted(long_AR_index)
                b = sorted(set(range(long_AR_index[0],long_AR_index[-1]+1))-set(long_AR_index))

                if (np.isin(np.argwhere(charater_AR != 0)[:,1], [0]).any()) & (len(b)!=0):
                    long_min = b[0]; long_max = b[-1]
                    long_row = np.zeros(charater_AR.shape[1]-long_max+long_min)
                    long_row[:charater_AR.shape[1]-long_max] = long[0, long_max:].copy()
                    long_row[charater_AR.shape[1]-long_max:charater_AR.shape[1]-long_max+long_min] = long[0, :long_min].copy()
                    long[:, long_max:] = np.arange(charater_AR.shape[1]-long_max)
                    long[:, :long_min] = np.arange(charater_AR.shape[1]-long_max, charater_AR.shape[1]-long_max+long_min)
                else:
                    long_min = np.min(long_AR_index); long_max = np.max(long_AR_index)
                    long_row = long[0, long_min:long_max].copy()
                    long[:, long_min:long_max] = np.arange(long_max-long_min)
                print('long_row', long_row)
                print('long', long[0,:])
                long[charater_AR==0] = np.nan
                lat[charater_AR==0] = np.nan



                protion = ivt_total*s_area/np.nansum(s_area)
                # print(np.nanmax(ivt_total), np.nanmax(s_area))
                # print([np.nansum(long*protion)/np.nansum(protion), np.nansum(lat*protion)/np.nansum(protion)])
                centroid_long_int = np.nansum(long*protion)/np.nansum(protion)

                centroid_long = long_row[int(centroid_long_int)] + centroid_long_int%1

                centroids.append([centroid_long, np.nansum(lat*protion)/np.nansum(protion)])
                times.append(s_time.strftime('%Y%m%d %H'))
            savename = path_SaveData+'AR_contour_%s%s_centroid/'%(bu_length_lack, bu_addition_Name)
            os.makedirs(savename, exist_ok=True)
            scipy.io.savemat(savename+'%s.mat'%(s_time_all[:-3]),
                             {'centroid':np.array(centroids, dtype='double'), 'time':times})

    def get_AR_pathway_Time_only_based_on_daily_AR(times_file_all):
        import nc4
        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s_%s_row/%s'%(bu_length_lack, bu_addition_Name, s_time_all))
            # print(path_SaveData+'AR_contour_%s_%s/%s'%(bu_length_lack, bu_addition_Name, s_time_all))


            '''######## get IVT mean & max from every contour '''
            s_time_loop = info_AR.time.values
            AR_pathway_time = []

            start_calcu = 0
            for s_time in s_time_loop:
                charater_AR = info_AR.sel(time=s_time)['vari'].values
                s_time = pd.to_datetime(s_time)

                if s_time.strftime('%Y%m%d%H') == s_time_all[:10]:
                    AR_pathway_time.append(s_time.strftime('%Y-%m-%d %H:00:00'))
                    start_calcu = 1
                    continue

                end_lack_time = 0

                if start_calcu >= 1:
                    print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
                    start_calcu += 1
                    try:
                        contour = nc4.read_nc4(path_MainData+'daily_AR_%s/%s'%(bu_addition_Name, s_time.strftime('%Y%m%d_%H')))
                        print('daily_AR_%s/%s'%(bu_addition_Name, s_time.strftime('%Y%m%d_%H')))

                    except:
                        if end_lack_time <= 1:
                            end_lack_time += 1
                            print('add1')
                            AR_pathway_time.append(s_time.strftime('%Y-%m-%d %H:00:00'))
                            continue
                        else:
                            break # quit loop

                    s_break = 0
                    for i_contour in contour:
                        if np.max(i_contour-charater_AR) == 0:
                            print('add2')
                            AR_pathway_time.append(s_time.strftime('%Y-%m-%d %H:00:00'))
                            s_break = 1
                            break
                    if s_break == 1:
                        print('have contour')
                        continue
                    # make sure the total length is longer than 3P.
                    if end_lack_time <= 1:
                        end_lack_time += 1
                        print('add3')
                        AR_pathway_time.append(s_time.strftime('%Y-%m-%d %H:00:00'))
                        continue

                    break # quit loop

            print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
            print(s_time_loop)
            print(AR_pathway_time)

            if len(AR_pathway_time) < 3:
                print(AR_pathway_time)
                print(s_time_all)
                raise ValueError('noooooooo')
            scipy.io.savemat(r'/home/linhaozhong/work/AR_detection_China/AR_contour_32_all_ll_AR_timeSeries/%s.mat'%(s_time_all),
                             {'time':AR_pathway_time})
        # IVT_intensity(times_file_all)
        centroid(times_file_all)

#
# for bu_main_period_length in [3]:
#     for bu_lack_period_length in [2]:
#         basis_analysis('%i%i'%(bu_main_period_length, bu_lack_period_length))


#########################  plot centroid #################################

a = xr.open_dataset(r"D:\OneDrive\a_matlab\cal_area\2020050917_1.nc")
b = scipy.io.loadmat(r"D:\OneDrive\a_matlab\cal_area\2020050917_1.mat")

for i, iss in enumerate(a['vari'].values):
    import matplotlib.pyplot as plt
    import cartopy.crs as ccrs
    fig, ax = plt.subplots(nrows=1, ncols=1, subplot_kw={'projection':ccrs.PlateCarree()})
    ax.contourf(np.arange(0,360,0.5), a.lat.values, iss, transform=ccrs.PlateCarree())
    print(b['centroid'][i])
    ax.scatter(b['centroid'][i][0],b['centroid'][i][1], s=1, transform=ccrs.PlateCarree())
    plt.show()





