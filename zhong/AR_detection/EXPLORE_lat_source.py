import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================

=============================
"""



def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def area_of_AR(contour):
    """
    :param contour:
    :return: the ratio of the maximum area compared with the Arctic
    """
    contour = contour[:-1]
    area_weight = scio.loadmat(path_GlobalArea + 'GridAL_ERA0_5degGridC.mat')['S']

    # mask exceed the Arctic
    lat = int(30 / resolution)
    contour[lat:, :] = 0
    area_weight[lat:, ] = 0
    area_weight_total = np.sum(area_weight)

    # mask exceed contour
    area_weight[contour < 0.01] = 0
    area_weight = np.sum(area_weight)
    return area_weight / area_weight_total

def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='anomaly'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    if types == 'anomaly':
        return data_present - data_anomaly
    else:
        return [data_present, data_anomaly]


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


def main_3phase(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'AR_contour_42/%s' % (time_file_new))
        lat = info_AR_row.lat.values
        lon = info_AR_row.lon.values
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))
        time_use = int(len(time_all[AR_loc:]) / 2)
        # time_use = [time_all[AR_loc], time_all[AR_loc + time_use], time_all[-1]]

        # for lat
        # lat_all = []
        # for i_time in time_all[int(AR_loc + time_use):]:
        #
        #     i_contour = info_AR_row['contour_all'].sel(time_all=i_time).values
        #     # lat_min = lat[np.max(np.argwhere(i_contour>0.1)[:,0])]
        #     # try lon
        #     lat_min = lon[np.argwhere(i_contour > 0.1)[:, 1]]
        #     lat_all.append(lat_min)

        contour = np.zeros(info_AR_row['contour_all'].isel(time_all=0).values.shape)
        for i_time in time_all:

            i_contour = info_AR_row['contour_all'].sel(time_all=i_time).values
            # lat_min = lat[np.max(np.argwhere(i_contour>0.1)[:,0])]
            # try lon
            contour += i_contour

        affected_area = area_of_AR(contour)
        return np.array(affected_area)

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            phase1 = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean = contour_cal(time_file, var)
                print(s_mean)
                phase1.append(s_mean)

            nc4.save_nc4(np.array(phase1),
                         path_SaveData + 'PDF/all_%s' % (var +  season + '_' + type_name[iI]))

# main_3phase('area_new')
# exit()

# path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF_line_begin_inAR0/'
path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF/'

time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name_num = ['0','2','3','4','5','6']

plot_name = ['GRE', 'BAF','BSE', 'BSW',
             'MED', 'ARC']

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

infig_fontsize = 5


var_all = ['area_new', 'area_new']
phase = 1

if phase == 0:
    plot_param = {
                  'IVT':[['IVTmean=\n', ' kg/(m*s)'],'IVT',[-10,360], [0,0.03],0,1],
                  'tcwv':[['TCWVmean=\n', ' kg/(m**2)'],'TCWVano',[-1.5,12], [0,0.4],1,1,],

                  't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-1, 18], [0, 0.25], 2, 1],
                    'sic':[['SICmean=\n', ''],'SICano',[-10,5], [0,0.5],2,100]
                  }
else:
    plot_param = {
                  'IVT':[['IVTmean=\n', ' kg/(m*s)'],'IVT',[-10,360], [0,0.03],0,1],
                  'tcwv':[['TCWVmean=\n', ' kg/(m**2)'],'TCWVano',[-1.5,12], [0,0.4],1,1,],

                  't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-1, 20], [0, 0.25], 2, 1],
                    'sic':[['SICmean=\n', ''],'SICano',[-10,5], [0,0.5],2,100],
        'lat':[['SICmean=\n', ''],'SICano',[20,80], [0,0.08],0,1],
        'area_new': [['SICmean=\n', ''], 'SICano', [0, 1], [0, 0.08], 2, 1]
                  }
SMALL_SIZE = 7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize


for season in time_Seaon_divide_Name:

    def get_data():
        data_mean = []
        for s_type_name in type_name:
            s_data_mean = []

            for var in var_all:
                aaa = nc4.read_nc4(path_SecondData + 'all_%s%s_%s' % (var, season, s_type_name))
                print(aaa)
                aaa = aaa[~np.isnan(aaa)]
                s_data_mean.append(aaa*plot_param[var][-1])

            data_mean.append(s_data_mean)

        return np.array(data_mean)


    frequency = get_data()


    def plots(ax, data, bin_width):
        percentile_label = ['Q1', 'Q2', 'Q3']
        percentile = np.percentile(data, [25,50,75])
        print(data)
        mean = np.nanmean(data)
        HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
        # ax.boxplot(data)

        _, _, bar_container = ax.hist(data,bins=bin_width,density=True, lw=1,
                                  ec="black", fc="gray",zorder=2)

        # yticks = ax.get_yticks()
        # ax.set_yticks(yticks)
        # ax.set_yticklabels((yticks*len(data)).astype('int'))

        ylim_max = ax.get_ylim()[1]
        trans = ax.get_xaxis_transform()
        for i_percentile in range(3):
            ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
            ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
                    transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
                    bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
            )
            if plot_param[var][4] == 0:
                values =percentile[i_percentile].astype('int')
            else:
                values = np.round(percentile[i_percentile], plot_param[var][4])

            ax.text(percentile[i_percentile], 1.01, values, transform=trans,
                    horizontalalignment='center', fontsize=infig_fontsize)

        return mean


    fig, axs = plt.subplots(6, 2, figsize=[4, 5])
    plt.subplots_adjust(top=0.91,
bottom=0.04,
left=0.105,
right=0.98,
hspace=0.4,
wspace=0.2)



    def flatten(l):
        l = np.array([item for sublist in l for item in sublist])
        l = l[~np.isnan(l)]
        return np.array(l)

    bin_col = [np.histogram(flatten(frequency[:,0]), bins=100)[1], np.histogram(flatten(frequency[:,1].flatten()), bins=100)[1]]
    for i in range(6):

        for j in range(2):
            data = frequency[i][j]
            ax = axs[i][j]
            var = var_all[j]
            mean = plots(ax, data, bin_col[j])
            ax.annotate(figlabelstr[2 * i + j], [0.01, 1.01], xycoords='axes fraction')

            if var != 'IVT':
                ax.axvline(0, color='black',zorder=0)
            # ax.set_xlim(plot_param[var][2])
            # ax.set_ylim(plot_param[var][3])

            mean_label = plot_param[var][0]
            if plot_param[var][4] == 0:
                mean = int(mean)
            else:
                mean = np.round(mean, plot_param[var][4])

            if var != 'sic':
                x = 0.85
            else:
                x=0.15
            ax.text(x, 0.6, mean_label[0]+str(mean)+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)


        axs[i][0].set_ylabel(plot_name[i] + '\nProbability')




for i_col in range(2):
    var = var_all[i_col]
    axs[0][i_col].set_title(plot_param[var][1], pad=10)

fig.suptitle('phase %i'%(phase+1), fontsize=8)

plt.show()
plt.savefig('figure5_%s_phase%i.png'%(var_all[0], phase), dpi=300)
plt.close()











