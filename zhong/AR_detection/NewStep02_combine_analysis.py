import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
"""
2D characteristics combine analysis of IVT, T2m anomaly, circulation.
get all AR characteristics from type_filename_SON.csv
while this file will provided the clustering result of AR and the corresponding AR fileAddress
"""

class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps
        self.ax = ax
        self.crs = ccrs.PlateCarree()
        self.cmaps = ''
        self.colorbar = True
        self.orientation = 'h'
        self.ranges = ranges
        self.fmt_contourf = '%.1f'
        self.fmt_contour = '%.1f'
        self.ranges_reconstrcution_button_contourf = False
        self.ranges_reconstrcution_button_contour = False
        self.linewidths = 1
        self.clabel_size = 5
        self.cmaps = cmaps.WhiteGreen
        self.colorbar = True
        self.orientation = 'h'
        self.spacewind = [1, 1]
        self.colors = False
        self.print_min_max = False
        self.contour_arg = {'colors': 'black'}
        self.contourf_arg = {}
        self.quiverkey_arg = {'X': 0.85, 'Y': 1.05, 'U': 10, 'label': r'$10 \frac{m}{s}$'}
        self.contour_clabel_color = 'white'
        self.MidPointNorm = False

        def del_ll(range, lat, lon):
            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range[0])
            # 人为重新排序
            if del_area[0] > del_area[1]:
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        if self.ranges[0] is not False:
            self.lat, self.lon, self.del_area = del_ll(ranges, lat, lon)
        else:
            self.lat, self.lon, self.del_area = [lat, lon, False]

    def del_data(self, data):
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1] + 1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        def ranges_create(begin, end, inter):
            class MidPointNorm(Normalize):
                def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                    Normalize.__init__(self, vmin, vmax, clip)
                    self.midpoint = midpoint

                def __call__(self, value, clip=None):
                    if clip is None:
                        clip = self.clip

                    result, is_scalar = self.process_value(value)

                    self.autoscale_None(result)
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if not (vmin < midpoint < vmax):
                        raise ValueError("midpoint must be between maxvalue and minvalue.")
                    elif vmin == vmax:
                        result.fill(0)  # Or should it be all masked? Or 0.5?
                    elif vmin > vmax:
                        raise ValueError("maxvalue must be bigger than minvalue")
                    else:
                        vmin = float(vmin)
                        vmax = float(vmax)
                        if clip:
                            mask = ma.getmask(result)
                            result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                              mask=mask)

                        # ma division is very slow; we can take a shortcut
                        resdat = result.data

                        # First scale to -1 to 1 range, than to from 0 to 1.
                        resdat -= midpoint
                        resdat[resdat > 0] /= abs(vmax - midpoint)
                        resdat[resdat < 0] /= abs(vmin - midpoint)

                        resdat /= 2.
                        resdat += 0.5
                        result = ma.array(resdat, mask=result.mask, copy=False)

                    if is_scalar:
                        result = result[0]
                    return result

                def inverse(self, value):
                    if not self.scaled():
                        raise ValueError("Not invertible until scaled")
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if cbook.iterable(value):
                        val = ma.asarray(value)
                        val = 2 * (val - 0.5)
                        val[val > 0] *= abs(vmax - midpoint)
                        val[val < 0] *= abs(vmin - midpoint)
                        val += midpoint
                        return val
                    else:
                        val = 2 * (value - 0.5)
                        if val < 0:
                            return val * abs(vmin - midpoint) + midpoint
                        else:
                            return val * abs(vmax - midpoint) + midpoint

            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0 + inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if isinstance(ranges_reconstrcution_button, list) or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button
        else:

            ranges_min = np.nanmin(data);
            ranges_max = np.nanmax(data)
            if ranges_reconstrcution_button is False:
                if ranges_min > 0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                ticks = (ranges_max - ranges_min) / 30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

        if self.MidPointNorm is not False:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       norm=self.MidPointNorm, **self.contourf_arg)

        else:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       **self.contourf_arg)

        if self.colorbar:
            if self.orientation == 'h':
                orientation = 'horizontal'
            else:
                orientation = 'vertical'
            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.fmt_contourf)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=5, transform=self.crs, **self.contour_arg)
        cbs = self.ax.clabel(
            cb, fontsize=self.clabel_size,  # Typically best results when labelling line contours.
            colors=['black'],
            inline=True,  # Cut the line where the label will be placed.
            fmt=self.fmt_contour,  # Labes as integers, with some extra space.
        )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]],
                             u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_MainData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values


    return t2m


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly
#                 s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
#                                                          'WaterVapor',
#                                                          'tcwv', var_dir='Total_column_water_vapour')


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'


def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, level=[1000, 850, 500, 200]):
    '''date containing Hour information'''
    import scipy
    time = pd.to_datetime(date)
    # load hourly anomaly data
    data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

    return data_present - data_anomaly

resolution = 0.5
def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num-1):
        percentile = np.percentile(time_len, 100*i/(phase_num-1))
        p_index = int(percentile)
        p_weighted = percentile%1
        division.append(contour[p_index]*p_weighted+contour[p_index+1]*(1-p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)

def contour_cal(time_file,var='IVT', trans_lon=False):
    def roll_contour(contour, centroid):
        """
        roll all AR variables to [0,0] position based on their centroids.
        :param contour: [N, single contour]
        :param centroid: [N, single centroid]
        :return:
        """
        for i in range(len(contour)):
            roll_lat = [int(centroid[i][0] / resolution)]
            roll_lon = [int((180 - centroid[i][1]) / resolution)]
            contour[i] = np.roll(contour[i], roll_lat, 0)
            contour[i] = np.roll(contour[i], roll_lon, 1)
        return contour

    # mat file always has some bugs with str type. skip whitespace
    time_file_new = ''
    for i in time_file:
        time_file_new += i
        if i == 'c':
            break

    info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))

    # get the shape characteristic
    if var == 'shape':
        shape_charac_name = ['width', 'longth', 'orientation']
        centroid_AR_row = info_AR_row.centroid_AR.values
        if trans_lon:
            centroid_AR = centroid_AR_row[:,1]
            centroid_AR[(centroid_AR<180) & (centroid_AR>0)] = centroid_AR[(centroid_AR<180) & (centroid_AR>0)]+360
            centroid_AR_row[:,1] = centroid_AR
        shape_charac_AR = np.array([np.mean(i, axis=0) for i in np.array_split(centroid_AR_row,3)])

        centroid_all_row = info_AR_row.centroid_all.values
        if trans_lon:
            centroid_all = centroid_all_row[:,1]
            centroid_all[(centroid_all<180) & (centroid_all>0)] = centroid_all[(centroid_all<180) & (centroid_all>0)]+360
            centroid_all_row[:,1] = centroid_all
        shape_charac_all = np.array([np.mean(i, axis=0) for i in np.array_split(centroid_all_row,3)])

        # exit(0)
        # for i_shape_charac in shape_charac_name:
        #     isc = info_AR_row[i_shape_charac].values
        #     if i_shape_charac == 'orientation':
        #         isc[isc<0] = isc[isc<0]+180
        #     isc = np.array([np.mean(i) for i in np.array_split(isc, 3)])
        #     shape_charac = np.append(shape_charac, np.expand_dims(isc, axis=1), axis=1)

        return [shape_charac_AR, shape_charac_all]


    ####################################################################
    time_all = info_AR_row.time_all.values
    time_AR = info_AR_row.time_AR.values

    contour_IVT_AR = []
    contour_IVT_all = []

    if var == 't2m':
        for i_time_AR in time_AR:
            contour_IVT_AR.append(get_circulation_data_based_on_date(pd.to_datetime(i_time_AR),'2m_temperature',
                                                                     't2m'))
        for i_time_all in time_all:
            contour_IVT_all.append(get_circulation_data_based_on_date(pd.to_datetime(i_time_all),'2m_temperature',
                                                                     't2m'))
    elif var == 'IVT':
        for i_time_AR in time_AR:
            contour_IVT_AR.append(load_ivt_total(pd.to_datetime(i_time_AR)))
        for i_time_all in time_all:
            contour_IVT_all.append(load_ivt_total(pd.to_datetime(i_time_all)))

    elif var == 'tcwv':
        for i_time_AR in time_AR:
            contour_IVT_AR.append(get_circulation_data_based_on_date(pd.to_datetime(i_time_AR),
                                                         'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour'))
        for i_time_all in time_all:
            contour_IVT_all.append(get_circulation_data_based_on_date(pd.to_datetime(i_time_all),'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour'))
    elif var == 'advection':
        for i_time_AR in time_AR:
            contour_IVT_AR.append(load_Q(pd.to_datetime(i_time_AR),
                                                         'advection'))
        for i_time_all in time_all:
            contour_IVT_all.append(load_Q(pd.to_datetime(i_time_all),
                                                         'advection'))
    elif var == 'adibatic':
        for i_time_AR in time_AR:
            contour_IVT_AR.append(load_Q(pd.to_datetime(i_time_AR),
                                                         'adibatic'))
        for i_time_all in time_all:
            contour_IVT_all.append(load_Q(pd.to_datetime(i_time_all),
                                                         'adibatic'))
    ####### for pressure level, return
    else:
        info_AR_3phase, info_all_3phase = [[], []]

        # get 4 level circulation
        for i_time_AR in time_AR:
            contour_IVT_AR.append(get_circulation_data_based_on_pressure_date(pd.to_datetime(i_time_AR),var[0],
                                                                     var[1]))
        for i_time_all in time_all:
            contour_IVT_all.append(get_circulation_data_based_on_pressure_date(pd.to_datetime(i_time_all),var[0],
                                                        var[1]))

        # roll circulation into centroid position for every levels
        for i_level in range(4):
            index = np.argwhere(time_all == time_AR[0]).flatten()[0]

            s_contour_IVT_AR = roll_contour(contour_IVT_AR[i_level], info_AR_row.centroid_AR.values)
            info_AR_3phase.append(cal_3phase(s_contour_IVT_AR, phase_num=phase_num))

            s_contour_IVT_all = roll_contour(contour_IVT_all[i_level], info_AR_row.centroid_all.values)
            info_all_3phase.append(cal_3phase(s_contour_IVT_all[index:], phase_num=phase_num))

        return [info_AR_3phase, info_all_3phase]

    index = np.argwhere(time_all==time_AR[0]).flatten()[0]
    contour_IVT_AR = roll_contour(contour_IVT_AR, info_AR_row.centroid_AR.values)
    info_AR_3phase = cal_3phase(contour_IVT_AR, phase_num=phase_num)

    contour_IVT_all = roll_contour(contour_IVT_all, info_AR_row.centroid_all.values)
    info_all_3phase = []
    info_all_3phase.extend(cal_3phase(contour_IVT_all[:index+1], phase_num=phase_num))
    info_all_3phase.extend(cal_3phase(contour_IVT_all[index:], phase_num=phase_num)[1:])
    return [info_AR_3phase, info_all_3phase]

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_detection/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_detection/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

phase_num = 3
use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']

def main(var):
    for season in ['DJF', 'MAM', 'JJA', 'SON']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):

            times_file_all = df[i].values
            info_AR_3phase = []
            info_all_3phase = []
            trans_lon = False
            if (iI == 0) & (var == 'shape'):
                trans_lon = True
            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue
                s_info_AR_3phase, s_info_all_3phase = contour_cal(time_file, var, trans_lon)
                info_all_3phase.append(s_info_all_3phase)
                info_AR_3phase.append(s_info_AR_3phase)
            info_all_3phase = np.array(info_all_3phase)

            info_AR_3phase = np.array(info_AR_3phase)
            print(info_AR_3phase.shape)
            if (iI == 0) & (var=='shape'):

                # watch out the mean longitute between 0 and 359
                info_all_3phase[:, :,1] = info_all_3phase[:, :,1] % 360
                info_AR_3phase[:,:, 1] = info_AR_3phase[:, :,1] % 360
                print(info_all_3phase)
                
                s_all = info_all_3phase[:, :,1] % 360
                s_AR = info_AR_3phase[:,:, 1]% 360
                s_all[(s_all < 180) & (s_all > 0)] = s_all[(s_all < 180) & (
                            s_all > 0)] + 360
                s_AR[(s_AR < 180) & (s_AR > 0)] = s_AR[(s_AR < 180) & (
                            s_AR > 0)] + 360

                s_AR = np.nanmean(s_AR, axis=0)%360
                s_all = np.nanmean(s_all, axis=0)%360

                info_all_3phase = np.nanmean(info_all_3phase, axis=0)
                info_AR_3phase = np.nanmean(info_AR_3phase, axis=0)

                info_AR_3phase[:,1]=s_AR
                info_all_3phase[:,1] = s_all

                print(info_all_3phase)

            else:
                info_all_3phase = np.nanmean(info_all_3phase, axis=0)
                info_AR_3phase = np.nanmean(info_AR_3phase, axis=0)

            # for sI, ssi in enumerate(info_AR_3phase):
            #     plt.imshow(ssi)
            #     plt.title(var+' '+season + ' '+type_name[iI]+' phase %i'%sI)
            #     # plt.show()
            #     plt.savefig('/home/linhaozhong/work/AR_detection/pic_combine_result/%s.png'%(var+'_'+season + '_'+type_name[iI]+'_phase %i'%sI),
            #                 dpi=400)
            #     plt.close()
            if isinstance(var, list):
                nc4.save_nc4(info_AR_3phase,
                             path_SaveData+'combine_result/AR_%s'%(var[0]+'_'+season + '_'+type_name[iI]))
                nc4.save_nc4(info_all_3phase,
                             path_SaveData+'combine_result/all_%s'%(var[0]+'_'+season + '_'+type_name[iI]))

            else:
                # nc4.save_nc4(info_AR_3phase,
                #              path_SaveData+'combine_result/AR_%s'%(var+'_'+season + '_'+type_name[iI]))
                nc4.save_nc4(info_all_3phase,
                             path_SaveData+'combine_result/5_phase_all_%s'%(var+'_'+season + '_'+type_name[iI]))

# main('shape')
# main('IVT')
# main('t2m')
main('tcwv')
# main('advection')
# main('adibatic')
# vari_SaveName_all = ['Hgt']
# vari_ShortName = ['z']
# for i in range(len(vari_ShortName)):
#     main([vari_SaveName_all[i], vari_ShortName[i]])