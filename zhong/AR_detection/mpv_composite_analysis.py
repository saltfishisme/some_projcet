import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import numpy as np
import nc4
import cartopy.crs as ccrs
from function_shared import get_circulation_data_based_on_pressure_date, load_mpv

a = nc4.read_nc4(r"D:\mpv_composite_result_500\mpv_ano_0")
# print(a.shape)
fig = plt.figure()

import cmaps
print(a.shape)
ax = fig.add_subplot(111,
                     projection=ccrs.Orthographic(180, 70))
cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5), a[2],
                transform=ccrs.PlateCarree(), zorder=5)
ax.coastlines(zorder=10)
ax.set_global()
plt.show()
exit(0)





path_mpvData = r'/home/linhaozhong/work/AR_NP85/pic_single_event_mpv/'
composite_prssure_level = 1000
composite_var = 'mpv'
type_AR_classification_all = [0,2,3,4,5,6]
path_saveData = r'/home/linhaozhong/work/AR_NP85/mpv_composite_result/'


for type_AR_classification in type_AR_classification_all:
    path_AR_dir_all_Type = os.listdir(path_mpvData+str(type_AR_classification)+'/')

    composite_result = []
    hgt_anomaly = []
    mpv_anomaly = []
    for path_AR_dir_Type in path_AR_dir_all_Type:
        if path_AR_dir_Type[-2:] == 'nc':
            ds = xr.open_dataset(path_mpvData+str(type_AR_classification)+'/'+
                                 path_AR_dir_Type+'/'+path_AR_dir_Type[:-7]+'_'+path_AR_dir_Type[-7:-5]+'.nc')

            composite_result.append(ds[composite_var].sel(level=composite_prssure_level).values)

            date_now = pd.to_datetime(path_AR_dir_Type[:-7]+' %s:00:00'%path_AR_dir_Type[-7:-5])
            print(date_now)
            hgt_anomaly.append(get_circulation_data_based_on_pressure_date(date_now, 'Hgt', 'z',level=composite_prssure_level)[1])
            mpv_anomaly.append(load_mpv(date_now, 'mpv',r'/home/linhaozhong/work/AR_NP85/'))
    # averaged
    composite_result = np.nanmean(np.array(composite_result), axis=0)
    hgt_anomaly = np.nanmean(np.array(hgt_anomaly), axis=0)
    mpv_anomaly = np.nanmean(np.array(mpv_anomaly), axis=0)

    nc4.save_nc4(composite_result, path_saveData+'mpv_%i'%type_AR_classification)
    nc4.save_nc4(hgt_anomaly, path_saveData + 'hgt_%i' % type_AR_classification)
    nc4.save_nc4(mpv_anomaly, path_saveData + 'mpv_ano_%i' % type_AR_classification)


