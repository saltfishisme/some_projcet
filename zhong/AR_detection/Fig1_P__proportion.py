import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt
import xarray as xr
plt.rcParams['font.sans-serif'] = ['Times New Roman']
# plt.rcParams['axes.unicode_minus'] = False
data_row = xr.open_dataset('vivt.nc')

bu_months_selected = [[12,1,2],[3,4,6],[6,7,8],[9,10,11]]
bu_months_labels = ['(a) DJF', '(b) MAM', '(c) JJA', '(d) SON']

fig, axs =plt.subplots(2,2, figsize=[6,4])
SMALL_SIZE = 8

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=SMALL_SIZE)  # fontsize of the figure titleim
import matplotlib
matplotlib.rc('axes', titlesize=SMALL_SIZE)
plt.subplots_adjust(top=0.94,
                    bottom=0.145,
                    left=0.12,
                    right=0.885,
                    hspace=0.33,
                    wspace=0.24)
for s_imonth, s_months_selected in enumerate(bu_months_selected):
    data = data_row.sel(time=np.in1d(data_row['time.month'], s_months_selected))
    data = data['vivt'].values
    data[data<0] = 0
    data = data.reshape([data.shape[0],  int(360/5), 5])
    data = np.sum(data, axis=2)
    data[data<0] = np.nan
    data_sta = data

    def pdf(data):
        data = data[~np.isnan(data)]
        mu, std = norm.fit(data)
        x = np.arange(0, np.max(data)+75, 75)
        p = norm.pdf(x, mu, std)

        return x, p, np.percentile(data, 95), np.percentile(data, 85)

    index_pdf = pd.DataFrame(index=np.arange(0, np.max(data_sta)+75, 75))
    index_p90 = []
    index_p50  = []
    for s_i in range(data_sta.shape[1]):
        s_x, s_p, s_p90, s_p50 = pdf(data_sta[:, s_i])
        s_data = pd.Series(s_p, index=s_x)
        index_pdf['%i'%(s_i*5)] = s_data
        index_p90.append(s_p90)
        index_p50.append(s_p50)
    import cmaps
    ax = axs[int(s_imonth/2)][int(s_imonth%2)]
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))
    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/Tano.mat', 'Tano')
    cb = ax.pcolormesh(index_pdf.columns, index_pdf.index, index_pdf.values*100, vmax=0.5, cmap=cmaps.MPL_cool)
    ax.plot(index_pdf.columns, index_p90, label='95$^{th}$', linewidth=1, color='black')
    ax.plot(index_pdf.columns, index_p50, label='85$^{th}$', linewidth=1, color='black', linestyle='--')
    ax.set_ylim([0,3500])

    ax.set_xticks(index_pdf.columns[::10])
    if s_imonth >1 :
        ax.set_xlabel('Longitude', fontdict={'size':SMALL_SIZE})
    ax.tick_params(axis='x', labelsize=SMALL_SIZE)
    ax.tick_params(axis='y', labelsize=SMALL_SIZE)
    ax.set_title(bu_months_labels[s_imonth], loc='left')

plt.legend(loc='upper center', bbox_to_anchor=(-0.125, -0.15),
           ncol=2)
cb_ax = fig.add_axes([0.9,0.12,0.01,0.8])
cbax = plt.colorbar(cb, extend='max',aspect=30, pad=0.03, cax=cb_ax)
cbax.set_label('Proportion of total poleward transport (%)')
fig.supylabel(r'Vertically intergrated flux (Tg day$^{-1}$ deg$^{-1}$)')
# plt.show()
plt.savefig('figure1_proprotion.png', dpi=400)