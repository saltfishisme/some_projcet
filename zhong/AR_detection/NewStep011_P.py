import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4


class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps

        self.ax = ax
        self.ranges = ranges

        def del_ll(range, lat, lon):
            """
            cut lon and lat, and then return the index to cut data used later.
            :param range:
            :param lat:
            :param lon:
            :return: self.lat, self.lon, self.del_area
            """

            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range[0])

            if del_area[0] > del_area[1]:  # sort
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        if self.ranges[0] is not False:
            self.lat, self.lon, self.del_area = del_ll(ranges, lat, lon)
        else:
            self.lat, self.lon, self.del_area = [lat, lon, False]

        self.crs = ccrs.PlateCarree()
        self.MidPointNorm = False
        self.print_min_max = False

        self.colorbar_fmt = '%.1f'
        self.colorbar = True
        self.colorbar_orientation = 'h'
        self.ranges_reconstrcution_button_contourf = False
        self.contourf_arg = {}

        self.contour_fmt = '%.1f'
        self.ranges_reconstrcution_button_contour = False
        self.contour_clabel_color = 'white'
        self.contour_arg = {'colors': 'black'}
        self.colorbar_clabel_size = 5
        self.contour_clabel= True


        self.quiver_spacewind = [1, 1]
        self.quiverkey_arg = {'X': 0.85, 'Y': 1.05, 'U': 10, 'label': r'$10 \frac{m}{s}$'}
        self.quiver_arg = {}


    def del_data(self, data):
        """
        link with del_ll, use self.del_area from function del_ll
        :param data:
        :return: cut data
        """
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1] + 1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        """
        :param data:
        :param ranges_reconstrcution_button:
        :return: self.levels, self.MidPointNorm
        """
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        class MidPointNorm(Normalize):

            def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                Normalize.__init__(self, vmin, vmax, clip)
                self.midpoint = midpoint

            def __call__(self, value, clip=None):
                if clip is None:
                    clip = self.clip

                result, is_scalar = self.process_value(value)

                self.autoscale_None(result)
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if not (vmin < midpoint < vmax):
                    raise ValueError("midpoint must be between maxvalue and minvalue.")
                elif vmin == vmax:
                    result.fill(0)  # Or should it be all masked? Or 0.5?
                elif vmin > vmax:
                    raise ValueError("maxvalue must be bigger than minvalue")
                else:
                    vmin = float(vmin)
                    vmax = float(vmax)
                    if clip:
                        mask = ma.getmask(result)
                        result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                          mask=mask)

                    # ma division is very slow; we can take a shortcut
                    resdat = result.data

                    # First scale to -1 to 1 range, than to from 0 to 1.
                    resdat -= midpoint
                    resdat[resdat > 0] /= abs(vmax - midpoint)
                    resdat[resdat < 0] /= abs(vmin - midpoint)

                    resdat /= 2.
                    resdat += 0.5
                    result = ma.array(resdat, mask=result.mask, copy=False)

                if is_scalar:
                    result = result[0]
                return result

            def inverse(self, value):
                if not self.scaled():
                    raise ValueError("Not invertible until scaled")
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if cbook.iterable(value):
                    val = ma.asarray(value)
                    val = 2 * (val - 0.5)
                    val[val > 0] *= abs(vmax - midpoint)
                    val[val < 0] *= abs(vmin - midpoint)
                    val += midpoint
                    return val
                else:
                    val = 2 * (value - 0.5)
                    if val < 0:
                        return val * abs(vmin - midpoint) + midpoint
                    else:
                        return val * abs(vmax - midpoint) + midpoint
        def ranges_create(begin, end, inter):


            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0 + inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))

        # if range is given
        if isinstance(ranges_reconstrcution_button, list) or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button


        else:
            ranges_min = np.nanmin(data)
            ranges_max = np.nanmax(data)

            if ranges_reconstrcution_button is False:  # default if False
                if ranges_min > 0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                else:
                    ticks = (ranges_max - ranges_min) / 30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        class MidPointNorm(Normalize):

            def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                Normalize.__init__(self, vmin, vmax, clip)
                self.midpoint = midpoint

            def __call__(self, value, clip=None):
                if clip is None:
                    clip = self.clip

                result, is_scalar = self.process_value(value)

                self.autoscale_None(result)
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if not (vmin < midpoint < vmax):
                    raise ValueError("midpoint must be between maxvalue and minvalue.")
                elif vmin == vmax:
                    result.fill(0)  # Or should it be all masked? Or 0.5?
                elif vmin > vmax:
                    raise ValueError("maxvalue must be bigger than minvalue")
                else:
                    vmin = float(vmin)
                    vmax = float(vmax)
                    if clip:
                        mask = ma.getmask(result)
                        result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                          mask=mask)

                    # ma division is very slow; we can take a shortcut
                    resdat = result.data

                    # First scale to -1 to 1 range, than to from 0 to 1.
                    resdat -= midpoint
                    resdat[resdat > 0] /= abs(vmax - midpoint)
                    resdat[resdat < 0] /= abs(vmin - midpoint)

                    resdat /= 2.
                    resdat += 0.5
                    result = ma.array(resdat, mask=result.mask, copy=False)

                if is_scalar:
                    result = result[0]
                return result

            def inverse(self, value):
                if not self.scaled():
                    raise ValueError("Not invertible until scaled")
                vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                if cbook.iterable(value):
                    val = ma.asarray(value)
                    val = 2 * (val - 0.5)
                    val[val > 0] *= abs(vmax - midpoint)
                    val[val < 0] *= abs(vmin - midpoint)
                    val += midpoint
                    return val
                else:
                    val = 2 * (value - 0.5)
                    if val < 0:
                        return val * abs(vmin - midpoint) + midpoint
                    else:
                        return val * abs(vmax - midpoint) + midpoint
        try:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       norm=MidPointNorm(0), **self.contourf_arg)

        except:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       **self.contourf_arg)

        if self.colorbar:
            if self.colorbar_orientation == 'h':
                orientation = 'horizontal'
            else:
                orientation = 'vertical'

            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.colorbar_fmt)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)

        if self.contour_clabel:
            cbs = self.ax.clabel(
                cb, fontsize=self.colorbar_clabel_size,  # Typically best results when labelling line contours.
                colors=['black'],
                inline=True,  # Cut the line where the label will be placed.
                fmt=self.contour_fmt,  # Labes as integers, with some extra space.
            )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.quiver_spacewind[0]], self.lat[::self.quiver_spacewind[1]],
                             u[::self.quiver_spacewind[1], ::self.quiver_spacewind[0]], v[::self.quiver_spacewind[1], ::self.quiver_spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)
def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1,colors_shape[0]])
    return  ListedColormap(np.concatenate((colors, ones.T), axis=1))


path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\\'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE', 'BAF','BSE', 'BSW',
             'MED', 'ARC']
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
var_fmt = ['%0.1f', '%i', '%i', '%0.1f', '%0.1f']
var_title = []
var_ylabel = ['affected region/the Arctic (units:%)', 'duration (units:days)']


resolution=0.5
path_IVTratio = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\IVT_ratio\\'


def get_IVT_ratio():
    total = nc4.read_nc4(path_IVTratio + 'IVT_totalYear')
    classification_range = [[[0, 15], [320, 359]], [270, 320], [150, 210], [150, 210], [15, 150], [15, 150]]
    var = 'IVT_ratio'
    ratio = []
    for iI_type, type_SOM in enumerate(type_name):
        s_ratio = []
        for iI_season, season in enumerate(time_Seaon_divide_Name):
            if iI_type != 0:
                lon_range = (np.array(classification_range[iI_type]) / resolution).astype('int')
                climately = np.sum(total[:, iI_season, lon_range[0]:lon_range[1]])
            else:
                lon_range = (np.array(classification_range[iI_type][0]) / resolution).astype('int')
                climately = np.sum(total[:, iI_season, lon_range[0]:lon_range[1]])
                lon_range = (np.array(classification_range[iI_type][1]) / resolution).astype('int')
                climately += np.sum(total[:, iI_season, lon_range[0]:lon_range[1]])

            a = np.sum(nc4.read_nc4(path_IVTratio + r'mean_1D_%s_%s_%s' % (var, season, type_SOM)))
            b = np.sum(nc4.read_nc4(path_IVTratio + r'max_1D_%s_%s_%s' % (var, season, type_SOM)))
            s_ratio.append([a / climately, b / climately])
        ratio.append(s_ratio)
    return np.array(ratio)


fig, axs = plt.subplots(nrows=1,ncols=2, figsize=[6.5,3])
plt.subplots_adjust(top=0.97,
bottom=0.11,
left=0.09,
right=0.98,
hspace=0.2,
wspace=0.27)
for iI_var, var in enumerate(['area', 'duration']):


    def get_data():
        if var == 'freq':
            data = []
            for season in time_Seaon_divide_Name:
                df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)[type_name_num]
                data.append(np.array(df.count()))
            data = np.transpose(np.array(data), [1,0])
            print(np.sum(data/np.sum(data), axis=1))
            return data/np.sum(data), data
        data_mean = []
        data_max = []
        for type_SOM in type_name:

            data_mean.append(nc4.read_nc4(path_SecondData + 'mean_1D_%s_%s_%s' % (var, 'DJF', type_SOM)))
            data_max.append(nc4.read_nc4(path_SecondData + 'max_1D_%s_%s_%s' % (var, 'DJF', type_SOM)))

        return np.array(data_mean), np.array(data_max)

    labels = type_name

    frequency_mean, frequency_max = get_data()
    if var == 'area':
        frequency_mean = frequency_mean*100
    if var == 'duration':
        frequency_mean = frequency_mean/4

    if var == 'area':
        frequency_max = frequency_max*100
    if var == 'duration':
        frequency_max = frequency_max/4

    x = np.arange(len(labels))  # the label locations
    x_width = [-0.3, -0.1, 0.1, 0.3]
    width = 0.2  # the width of the bars

    ax = axs[iI_var]

    pos = np.arange(len(frequency_mean)) + 1
    bp = ax.boxplot(frequency_mean, sym='k+', positions=pos,medianprops={'color':'black'})

    pos = np.arange(len(frequency_max)) + 1
    bp = ax.boxplot(frequency_max, sym='r+',capprops={"color": "red", "linewidth": 1.5}, positions=pos,medianprops={'color':'red'})

    ## xlabel for season
    ax.set_xticks(pos)
    ax.set_xticklabels(plot_name)


    ## xlabel for SOM

    ax.set_ylabel(var_ylabel[iI_var])
    # ax.set_title('')
    ax.annotate(figlabelstr[iI_var], [0.01, 0.94], xycoords='axes fraction')
plt.show()
plt.savefig('figure3.png', dpi=300)
plt.close()
    # exit(0)


