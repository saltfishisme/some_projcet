import os
from geopy.distance import distance
import nc4
import pandas as pd
from scipy.ndimage import gaussian_filter
from fil_finder import FilFinder2D
import astropy.units as u
import numpy as np
from skimage import measure
import matplotlib.pyplot as plt
import cv2
import xarray as xr
import logging.handlers
import traceback
path_IVT_Data  = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'

def AR_orNot(contour, ivt_total, s_time):
    def daily_AR_detection(data, IVT_lat, IVT_long, row_data):
        '''
        :param data: np.array, 2D data.
        :param lat, lon, resolution: lat(list or array-like), lon(list or array-like) and resolution(float) of the data.
        :param given_threshold: float, the minimum value which can be marked as 'high_value'.
        :param numll_min: float, the points in one high_value region should be more than numll_min, otherwise, this region
                                 will be filtered.

        :return: array, shape is [the number of high_value area, 7],
                the first five indexes are  x_centre, y_centre, width, height, angle,
                The sixth and seventh indices are area of fitted ellipse and high_value region.
                The eighth and ninth indices are averaged wind speed of fitted ellipse and high_value region
        '''


        def cal_contour(data, roll=False):

            ####### prepare data
            s_IVT_long = IVT_long.copy()
            if roll != False:
                data = np.roll(data, roll, axis=1)
                s_IVT_long = np.roll(IVT_long, roll)
            thresh = cv2.threshold(data, 0.1, 255, cv2.THRESH_BINARY)[1]
            labels = measure.label(thresh, connectivity=1, background=0)

            #######
            Mask_onePic = np.zeros(data.shape, dtype='uint8')
            lineMask_onePic = np.zeros(data.shape, dtype='uint8')
            contourMask = []
            lineMask = []
            s_df_result = pd.DataFrame()

            if roll != False:
                num_AR = 1000
            else:
                num_AR = 0

            ''' #########################   loop for every contour in this image   ###########################'''
            for label in np.unique(labels):
                if label == 0:
                    continue

                #######  prepare labelMask, labelMask is a narray contains one contour
                labelMask = np.zeros(thresh.shape, dtype="uint8")
                labelMask[labels == label] = 255
                numPixels = cv2.countNonZero(labelMask)

                # plt.imshow(labelMask)
                # plt.show()
                # plt.close()

                ####### get index of this contour
                ar_pathway_index = np.argwhere(labelMask != 0)

                '''' ####### numPixels , longitude and latitude that a AR_detection event must spread across '''
                if roll != False:
                    if (np.in1d(np.unique(ar_pathway_index[:, 1]), [roll])).any() == False:
                        continue
                else:
                    if (np.in1d(np.unique(ar_pathway_index[:, 1]), [0, thresh.shape[1] - 1])).any():
                        continue

                if not ((np.in1d(s_IVT_long[np.unique(ar_pathway_index[:, 1])], bu_study_box[0]).any()) & (
                        np.in1d(IVT_lat[np.unique(ar_pathway_index[:, 0])], bu_study_box[1]).any())):
                    continue


                if (numPixels > de_the_number_of_grids):
                    simple = labelMask
                    simple[simple == 255] = 1

                    ######## sketetons
                    fil = FilFinder2D(simple, distance=1 * u.pix, mask=simple)
                    fil.preprocess_image(skip_flatten=True)
                    fil.create_mask(border_masking=True, verbose=False,
                                    use_existing_mask=True)
                    fil.medskel(verbose=False)
                    result = fil.analyze_skeletons(branch_thresh=8 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')

                    # print(fil.lengths())
                    # print(fil.lengths()/np.sum(fil.branch_properties['length']))
                    # # print(fil.branch_properties)
                    # exit(0)
                    # print(fil.curvature_branches())
                    # plt.imshow(simple, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()

                    '''###################  criteria ################################### '''

                    #######
                    if (fil.branch_properties['number'] > de_branch_number):
                        # print('kill by shape')
                        # plt.imshow(labelMask, cmap='gray')
                        # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                        # plt.axis('off')
                        # plt.show()
                        continue

                    ######
                    if ((fil.lengths() / np.sum(fil.branch_properties['length'])) < de_branch_ratio * u.pix):
                        # print('kill by ratio')
                        # plt.imshow(labelMask, cmap='gray')
                        # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                        # plt.axis('off')
                        # plt.show()
                        continue

                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()

                    #######
                    sslabels = measure.label(labelMask, connectivity=1, background=0)
                    regions = measure.regionprops(sslabels, intensity_image=row_data)
                    length = regions[0].axis_major_length
                    width = regions[0].axis_minor_length
                    if width == 0:
                        continue
                    if length / width < 2:
                        # plt.imshow(labelMask)
                        # plt.title('kill by length/width')
                        # plt.show()
                        continue

                    #######
                    try:
                        fil.analyze_skeletons(branch_thresh=40 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')
                    except:
                        continue
                    ll_loc = np.array(fil.end_pts)[0]
                    print(ll_loc)
                    lat_loc = IVT_lat[ll_loc[:, 0]]
                    lon_loc = s_IVT_long[ll_loc[:, 1]]
                    try:
                        length = distance((lat_loc[0], lon_loc[0]), (lat_loc[1], lon_loc[1])).km
                        if length < 2000:
                            continue
                    except:
                        print('error')


                    contourMask.append(np.array(labelMask))
                    lineMask.append(np.array(fil.skeleton_longpath, dtype="uint8"))

                    Mask_onePic = cv2.add(Mask_onePic, labelMask)
                    lineMask_onePic = cv2.add(lineMask_onePic, np.array(fil.skeleton_longpath, dtype='uint8'))

                    num_AR += 1
                    contours = measure.find_contours(labelMask)[0]
                    s_df = pd.DataFrame()

                    def array_to_str(array):
                        saveStr = ''
                        for i in array:
                            saveStr += ',' + str(i)
                        return saveStr

                    s_df['y'] = array_to_str([contours[:, 0]])
                    s_df['x'] = array_to_str([contours[:, 1]])
                    s_df['num'] = num_AR
                    s_df_result = s_df_result.append(s_df, ignore_index=True)

            if roll != False:
                thresh = np.roll(thresh, -roll, axis=1)
                lineMask_onePic = np.roll(lineMask_onePic, -roll, axis=1)
                Mask_onePic = np.roll(Mask_onePic, -roll, axis=1)

                if num_AR > 1000:
                    contourMask = np.array(contourMask)
                    lineMask = np.array(lineMask)
                    for s_lineMask in range(len(lineMask)):
                        contourMask[s_lineMask] = np.roll(contourMask[s_lineMask], -roll, axis=1)
                        lineMask[s_lineMask] = np.roll(lineMask[s_lineMask], -roll, axis=1)
            return num_AR, s_df_result, contourMask, lineMask, thresh, lineMask_onePic

        # cal contour for row map
        num_AR, s_df_result, mask_ss, lineMask, thresh, lineMask_M = cal_contour(data)

        # cal contour for rolled map
        s_roll_number = int(data.shape[1] / 2)

        num_AR_cross, s_df_result_cross, mask_ss_cross, lineMask_cross, thresh_cross, lineMask_M_cross = cal_contour(data,
                                                                                                                     roll=s_roll_number)
        if num_AR+(num_AR_cross-1000) != 0:
            return True
        else:
            return False

    bu_study_box = [np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)]
    IVT_long = np.arange(0, 360, 0.5)
    IVT_lat = np.arange(90, -90.01, -0.5)
    de_branch_ratio = 0.7
    de_branch_number = 5
    de_the_number_of_grids = 400  # to remove the AR_detection pathway with the number of grids is smaller 400

    return daily_AR_detection(contour, IVT_lat, IVT_long, ivt_total)

import os

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total
main_path = r'/home/linhaozhong/work/AR_NP85_1940/AR_contour_42/'
file = os.listdir(main_path)

for i_file in file:
    if os.path.exists(r'/home/linhaozhong/work/AR_NP85_1940/AR_contour_42_reAR_all/'+i_file):
       continue
    info_AR_row = xr.open_dataset(main_path+i_file)
    time_all = info_AR_row.time_all.values
    time_AR = list(info_AR_row.time_AR.values)

    for i_time in time_all:
        i_time_pd = pd.to_datetime(i_time)
        # sum for all year

        contour = info_AR_row['contour_all'].sel(time_all=i_time).values

        ivt = load_ivt_total(i_time_pd)

        if AR_orNot(contour, ivt, pd.to_datetime(i_time)):
            time_AR.append(i_time)
        else:
            print('nonono')
            # exit()

    time_AR = sorted([pd.to_datetime(i) for i in time_AR])

    info_AR_row['time_all_butAR'] = time_AR

    info_AR_row.to_netcdf(r'/home/linhaozhong/work/AR_NP85_1940/AR_contour_42_reAR_all/'+i_file)

    # # sum for all year
    # avg += np.nansum(info_AR_row['contour_all'].values, axis=0)
# nc4.save_nc4(avg, '/home/linhaozhong/work/AR_NP85/ivt_ar_yearly')
# nc4.save_nc4(avg_freq, '/home/linhaozhong/work/AR_NP85/freq_ar_yearly')


