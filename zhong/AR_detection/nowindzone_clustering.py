import os
import scipy.io as scio
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from function_shared import get_data_output_dataarray, select_rectangle_area
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
from Setting_mpv import *
def projection_transform_test(lon, lat, centroid, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    proj_Out = {'proj': 'lcc', 'lon_0': int(centroid[1]),
                'lat_1': int(centroid[0])}



    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)
def projection_transform_relative_coor(lon_2d, lat_2d, points):
    lon, lat = points
    lon_2d = lon_2d[0,:]-lon
    lat_2d = lat_2d[:,0]-lat
    arg_lon = np.argmin(np.abs(lon_2d))
    arg_lat = np.argmin(np.abs(lat_2d))
    return [arg_lon, arg_lat]




def contour_overlap_or_not(contour, mask):
    # way 1
    if np.logical_and(contour == 1, mask == 1).any():
        return True
    else:
        return False

    # way 2
    def contour_to_edge_relative_coords(should_roll_matirx, contour):
        """
        transform contour from lat ,lon coordination to relative coordination.
        if the contour pass though the edge, roll to the central part.
        :param contour: contour
        :param position:
        :param lon_2d:
        :param lat_2d:
        :return:
        """
        roll = False
        if np.isin(should_roll_matirx[:, 0], [1]).any():
            roll = True
            contour = np.roll(contour, int(180 / resolution), axis=1)
        import cv2
        im_bw = cv2.threshold(contour, 0, 255, cv2.THRESH_BINARY)[1]
        im_bw = np.array(im_bw, dtype=np.uint8)
        contours, _ = cv2.findContours(im_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        polygons = []
        for object in contours:
            if object.shape[0] > 200:
                from shapely.geometry import MultiPolygon, Polygon
                polygons.append(Polygon(np.squeeze(object)))
        return polygons

    contour_arg = contour_to_edge_relative_coords(contour, contour)

    from shapely.geometry import MultiPolygon
    p1 = MultiPolygon(contour_arg)

    # load EU continent
    mask_arg = contour_to_edge_relative_coords(contour, mask)
    p2 = MultiPolygon(mask_arg)

    try:
        return p1.intersects(p2)
    except:
        for poly in p2.geoms:
            x, y = poly.exterior.xy
            plt.plot(x, y)
        for poly in p1.geoms:
            x, y = poly.exterior.xy
            plt.plot(x, y)
        plt.savefig(path_saveData+'test.png')
        exit(0)
    return p1.intersects(p2)

var_with_shortname = {'Hgt': 'z', 'Tem': 't', 'Uwnd': 'u', 'Vwnd': 'v', 'RH': 'r'}
path_contourData = r'/home/linhaozhong/work/AR_NP85/AR_contour_42/'
composite_prssure_level = 1000
path_saveData = r'/home/linhaozhong/work/AR_NP85/nowindzone/'
path_EU_Data = r'/home/linhaozhong/work/AR_NP85/EU_mask.mat'
def main_3phase():

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_contourData + time_file_new)

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        in_land = 0
        count_after_in_land = 0
        for i_time in time_all:

            i_contour = info_AR_row['contour_all'].sel(time_all=i_time).values
            mask = scio.loadmat(path_EU_Data)['mask']


            if (in_land == 0)&(contour_overlap_or_not(i_contour, mask)):
                in_land = 1

            if in_land != 0:
                count_after_in_land += 1

            if count_after_in_land == count_after_in_land_standard:
                i_centroid = info_AR_row['centroid_all'].sel(time_all=i_time).values

                i_time = pd.to_datetime(i_time)
                cross = get_data_output_dataarray(pd.to_datetime(i_time), ['Hgt', 'Uwnd', 'Vwnd'], [1000])
                cross = cross.sel(level=1000)

                def plot_quiver():
                    plt.rcParams['hatch.color'] = 'darkred'
                    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree(central_longitude=i_centroid[1])})
                    delta = 4
                    delta_lat = 2
                    data, [lat, long] = select_rectangle_area([i_contour, cross['z'].values / 98, cross['u'].values, cross['v'].values],
                                                              [int(i_centroid[0]), int(i_centroid[1])],
                                                              cross.longitude.values, cross.latitude.values,
                                                              r_lat=80, r_lon=90)
                    "###############################"
                    for iIss,iss in enumerate(data):
                        if iss.shape[0] != 160:
                            a_un = np.ones([160,180])*-999
                            a_un[:iss.shape[0],:] = iss
                            a_un[a_un==-999] = np.nan
                            data[iIss] = a_un
                    return data,[i_centroid[1], i_centroid[0]]

                    "###############################"
                    i_contour_cut, z, u, v = data

                    def uv_in_AR(u, v, i_contour):
                        u_AR = np.mean(u[i_contour > 0.5])
                        v_AR = np.mean(v[i_contour > 0.5])

                        u_diff = u - u_AR
                        v_diff = v - v_AR
                        return np.sqrt(u_diff**2+ v_diff**2)
                        # return np.sqrt(u ** 2 + v ** 2)

                    ax.contourf(long, lat, np.ma.masked_less(i_contour_cut, 0.1),

                                colors='none', levels=[0.1, 1.1],
                                hatches=[2 * '.', 2 * '.'], alpha=0, facecolor='red', edgecolor='red',
                                transform=ccrs.PlateCarree(), zorder=20)

                    "split into rectangle"



                    "plot"

                    cb = ax.contour(long, lat,
                                     z,
                                     transform=ccrs.PlateCarree(), zorder=5)

                    # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                    #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
                    #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
                    import cmaps
                    cb = ax.contourf(long, lat,
                                    uv_in_AR(u,v,i_contour_cut), extend='both',levels=np.arange(0,20.1,0.5),
                                    transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=3)
                    plt.colorbar(cb)
                    # ax.quiver(long[::delta], lat[::delta_lat],
                    #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
                    #           transform=ccrs.PlateCarree(), scale=200, zorder=7)

                    plt.title(cross.time.values)
                    ax.coastlines(zorder=9)
                    # ax.set_extent()
                    # plt.show()
                    path_save = path_saveData + str(var) +  '/' + time_file_new +'/'
                    os.makedirs(path_save, exist_ok=True)
                    plt.savefig(path_save+i_time.strftime('%Y%m%d%H'), dpi=200)
                    plt.close()
                    

                data = plot_quiver()
                "###############################"
                return data
            "###############################"
        # plot_contour(uv_in_AR(), cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values, cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values,cross['pv'].values)

        return

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            if i=='0':
                times_file_all = df[i].values

                composite_result = []
                centroid = []
                for time_file in times_file_all:
                    if len(str(time_file)) < 5:
                        continue
                    a_all = contour_cal(time_file, i)
                    try:
                        a, a_centroid = a_all
                    except:
                        continue
                    centroid.append(a_centroid)
                    for issss in a:
                        print(issss.shape)
                    # exit(0)
                    composite_result.append(a)
                centroid = np.array(centroid, dtype='double')
                from function_shared import roll_longitude_from_359to_negative_180
                centroid[:,0] = roll_longitude_from_359to_negative_180(centroid[:,0])
                centroid = np.mean(centroid, axis=0)
                composite_result = np.array(composite_result,dtype='double')
                composite_result = np.nanmean(composite_result, axis=0)
                # print(composite_result.shape)
                scio.savemat(path_saveData+'composite_%s_%i.mat'%(i, count_after_in_land_standard), {'centroid':centroid, 'data':composite_result})
                # nc4.save_nc4(composite_result,path_saveData+'composite_%s'%i)

for count_after_in_land_standard in [1,2,3,4,5,6,7]:
    main_3phase()

#
exit(0)
# ##############################
#
# time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# plt.rcParams['hatch.color'] = 'darkred'
# path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# path_MainData = r'/home/linhaozhong/work/AR_NP85/'
# file_contour_Data = ''
# bu_addition_Name = ''
# path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
# path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
# resolution = 0.5
#
# path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# # path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
# path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
# path_savepath = r'/home/linhaozhong/work/AR_NP85/'
# path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# # path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'
#
# times_all = pd.date_range('1980-12-17', '1980-12-19', freq='1D').strftime('%Y%m%d')
#
# sta_AR = xr.open_dataset(r'/zhong/AR_detection/data/1980121717_2.nc')
# iI_AR_loc = 24
# for i_time in times_all:
#     vari_SaveName_all = ['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
#     # p0 =  xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\surface_pressure.%s.nc'%i_time)
#
#     data_row = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\Hgt.%s.nc' % i_time)
#     for i in vari_SaveName_all:
#         data_row = xr.merge([data_row, xr.open_dataset(
#             r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\%s.%s.nc' % (i, i_time))])
#
#     for iI_time_daily in [5, 11, 17, 23]:
#         data = data_row.isel(time=iI_time_daily)
#         print(data)
#         # p0 = p0.isel(time=iI_time_daily)
#         # data = data.sel(level=1000)
#
#         ##############################
#         # Get the cross section, and convert lat/lon to supplementary coordinates:
#
#         cross = data.squeeze().set_coords(('latitude', 'longitude'))
#
#         i_contour = sta_AR['contour_all'].sel(time_all=cross.time).values
#
#         i_centroid = sta_AR['centroid_all'].sel(time_all=cross.time).values
#
#
#         def select_rectangle_area(data, centroid, long, lat, r_lat=20, r_lon=30):
#             """
#             :param data: [hgt, u, v, ...], var is 2d.
#             :param centroid: select a rectangle based on centroid and r_lat, r_lon
#             :param long:
#             :param lat:
#             :param r_lat:
#             :param r_lon:
#             :return:
#             """
#
#             def roll_longitude_from_359to_negative_180(data, long):
#                 roll_length = np.argwhere(long < 180).shape[0] - 1
#                 long = np.roll(long, roll_length)
#                 data = np.roll(data, roll_length)
#                 long = xr.where(
#                     long > 180,
#                     long - 360,
#                     long)
#                 return data, long
#
#             lon_index = np.argwhere(long == centroid[1])[0]
#             if (lon_index - r_lon < 0) | (lon_index + r_lon > long.shape[0]):
#                 data, long = roll_longitude_from_359to_negative_180(data, long)
#                 print(long)
#                 centroid[1] = (centroid[1] + 180) % 360 - 180
#                 print(centroid[1])
#             long_2d, lat_2d = np.meshgrid(long, lat)
#             lat_index = np.argwhere(lat == centroid[0])[0]
#             lon_index = np.argwhere(long == centroid[1])[0]
#             result = []
#             for i_data in data:
#                 s_data = i_data[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])), :]
#                 lat_2d_flatten = lat_2d[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])),
#                                  0]
#
#                 s_data = s_data[:, int(lon_index - r_lon):int(lon_index + r_lon)]
#                 long_2d_flatten = long_2d[0, int(lon_index - r_lon):int(lon_index + r_lon)]
#
#                 result.append(s_data)
#             return result, [lat_2d_flatten, long_2d_flatten]
#
#
#         def plot_quiver():
#             plt.rcParams['hatch.color'] = 'darkred'
#             fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
#             delta = 4
#             delta_lat = 2
#
#             ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(i_contour, 0.1),
#
#                         colors='none', levels=[0.1, 1.1],
#                         hatches=[2 * '.', 2 * '.'], alpha=0, facecolor='red', edgecolor='red',
#                         transform=ccrs.PlateCarree(), zorder=20)
#
#             "split into rectangle"
#
#             data, [lat, long] = select_rectangle_area([cross['z'] / 98, cross['u'], cross['v']],
#                                                       [int(i_centroid[0]), int(i_centroid[1])],
#                                                       cross.longitude.values, cross.latitude.values,
#                                                       r_lat=80, r_lon=90)
#
#             "plot"
#
#             z, u, v = data
#             cb = ax.contourf(long, lat,
#                              z,
#                              transform=ccrs.PlateCarree(), zorder=5)
#             plt.colorbar(cb)
#             # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
#             #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
#             #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
#             ax.quiver(long[::delta], lat[::delta_lat],
#                       u[::delta_lat, ::delta], v[::delta_lat, ::delta],
#                       transform=ccrs.PlateCarree(), scale=200, zorder=7)
#
#             plt.title(cross.time.values)
#             ax.coastlines(zorder=9)
#             # ax.set_global()
#             plt.savefig()
#             plt.close()
#
#
#         def plot_contour(u, v):
#             # fig,axs = plt.subplots(1,1,subplot_kw={'projection':ccrs.Orthographic(0, 70)})
#             fig, axs = plt.subplots(1, 1, subplot_kw={'projection': ccrs.PlateCarree()})
#             delta = 4
#             delta_lat = 2
#             # data = [cross['t'],cross['r']]
#             data = u
#             for i in range(1):
#                 import cmaps
#                 ax = axs
#                 # data = xr.open_dataset(r'D:/vor.nc')['vo'].sel(level=1000, time=cross.time)
#                 # print(i_centroid)
#
#                 data, [lat, long] = select_rectangle_area([data], [int(i_centroid[0]), int(i_centroid[1])],
#                                                           cross.longitude.values, cross.latitude.values,
#                                                           r_lat=80, r_lon=90)
#
#                 # z = xr.open_dataset(r'D:/z.nc')['z'].sel(level=1000, time=cross.time)
#                 # plt.imshow(z)
#                 # plt.show()
#                 # slice = []
#
#                 # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
#                 #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
#                 #           transform=ccrs.PlateCarree(),scale=0.01, zorder=7)
#                 # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
#                 #           cross['u'].values[::delta_lat,::delta], cross['u'].values[::delta_lat,::delta],
#                 #           transform=ccrs.PlateCarree(),scale=500, zorder=7)
#
#                 ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(i_contour, 0.1),
#
#                             colors='none', levels=[0.1, 1.1],
#                             hatches=[2 * '.', 2 * '.'], alpha=0, edgecolor='black', transform=ccrs.PlateCarree(),
#                             zorder=20)
#
#                 cb = ax.contourf(long, lat,
#                                  data[0], extend='both', levels=np.arange(0, 20.1, 0.5),
#                                  transform=ccrs.PlateCarree(), cmap=cmaps.MPL_RdBu_r, zorder=5)
#
#                 """"# for relative vorticity"""
#                 # cb = ax.contourf(cross.longitude.values, cross.latitude.values,
#                 #                 data, extend='both',levels=np.arange(0,30.1,0.5),
#                 #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)
#
#                 # cb = ax.contourf(long, lat,
#                 #                 np.array(data[0])*1e4, extend='both',levels=np.arange(-4,4.01,0.1),
#                 #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)
#                 # cb = ax.contourf(cross.longitude.values, cross.latitude.values,
#                 #                 data.values*1e4, extend='both',level=np.arange(-4,4,0.1),
#                 #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu, zorder=5)
#                 delta = 4;
#                 delta_lat = 2
#                 # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
#                 #           cross.sel(level=1000)['u'].values[::delta_lat, ::delta], cross.sel(level=1000)['v'].values[::delta_lat, ::delta],
#                 #           transform=ccrs.PlateCarree(), scale=500, zorder=7)
#                 plt.colorbar(cb)
#                 plt.title(cross.time.values)
#                 ax.coastlines(zorder=9)
#                 # ax.set_global()
#             plt.show()
#
#
#         # plot_contour(uv_in_AR(), cross['pv'].values)
#         # plot_contour(cross['pv'].sel(level=1000).values, cross['pv'].values)
#         # plot_contour(cross['pv'].sel(level=1000).values,cross['pv'].values)
#         continue


"""=================================================================="""
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import numpy as np
import scipy.io as scio
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import scipy.interpolate as interp
from matplotlib import patches


def to_line(points):
    import shapely.geometry as sgeo
    query_geom = sgeo.LineString(points)
    return query_geom

contour_row = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\1980121717_2.nc')

contour = contour_row['contour_AR'].values[0]
lat = contour_row.lat.values
lon = contour_row.lon.values
lon_2d, lat_2d = np.meshgrid(lon, lat)
centroid = contour_row['centroid_AR'].values[0]
mask = scio.loadmat(r'D:/EU_mask.mat')['mask']


def points_to_relative_coor(should_roll_matirx, position, lon_2d, lat_2d):
    """
    transform contour from lat ,lon coordination to relative coordination.
    if the contour pass though the edge, roll to the central part.
    :param contour: contour
    :param position:
    :param lon_2d:
    :param lat_2d:
    :return:
    """
    roll = False
    if np.isin(should_roll_matirx[:,0], [1]).any():
        roll = True

    if roll:
        lon_2d = np.roll(lon_2d, int(180 / resolution), axis=1)
        lat_2d = np.roll(lat_2d, int(180 / resolution), axis=1)

    position_arg = []
    for i_points in position:
         position_arg.append(projection_transform_relative_coor(lon_2d, lat_2d, i_points))

    return contour_arg, position_arg





contour_overlap_or_not(contour, mask)
# x,y = p1.exterior.xy
# plt.plot(x,y )
#
# for poly in p2.geoms:
#     x, y = poly.exterior.xy
#     plt.plot(x, y)
# plt.show()
exit(0)



def interpolation_nan_for_longpath(longpath):
    longpath_shape = longpath.shape
    arr_ref_size = longpath.shape[2]
    longpath = longpath.reshape(longpath.shape[0] * 2, longpath.shape[-1])
    for i in range(len(longpath)):
        arr1 = longpath[i][~np.isnan(longpath[i])]  # skip nan
        arr1_interp = interp.interp1d(np.arange(arr1.size), arr1)
        longpath[i] = arr1_interp(np.linspace(0, arr1.size - 1, arr_ref_size))
    return longpath.reshape(longpath_shape)


def SOM_param(data, xylength, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = []
    for lead in ['X', 'Y']:
        for i in range(1, xylength):
            varname.append(str(i) + lead)
    varname.extend(addtional_varname)
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})


def SOM_spcific_param(data, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = addtional_varname
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})


def plot(save_labels, longpath, centroid, savename=''):
    def reject_outliers(data, m = 0.8):
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev if mdev else 0.
        return np.nanmean(data[s<m])
    # if longpath:
    #     longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s'%season)
    size = int(np.ceil(len(np.unique(save_labels)) / 3))
    fig = plt.figure()

    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            ax = fig.add_subplot(size, 3, iI + 1, projection=ccrs.NorthPolarStereo())
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())


            ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.05,
                       transform=ccrs.PlateCarree())
            s_centroid = np.apply_along_axis(reject_outliers, 0, centroid[save_labels == i])
            e1 = patches.Ellipse((s_centroid[1], s_centroid[0]), s_centroid[4], s_centroid[5],
                                 angle=180-np.nanmean(centroid[save_labels == i, 3]), color='red', linewidth=1, fill=False, zorder=2, transform=ccrs.PlateCarree())
            ax.add_patch(e1)
            ax.set_title('total number is %i, %0.0f%%'%(centroid[save_labels == i].shape[0], 100*centroid[save_labels == i].shape[0]/totol_events))
            ax.coastlines()
    plt.suptitle(season)
    plt.tight_layout()
    plt.show()
    # plt.savefig(path_pic+'%s_%s.png'%(season, savename), dpi=500)
    plt.close()
    # # # plot classification result
    # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    # for i in np.unique(save_labels[save_labels == 0]):
    #     ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    # ax.coastlines()
    # plt.title(season)
    # plt.show()
    # plt.close()

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)'];
""" divided into 4 regions"""

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
for season in ['DJF', 'MAM', 'JJA', 'SON']:
    clustering_fileName_Final = pd.DataFrame()
    # path_SaveData = r'/home/linhaozhong/work/AR_detection/'

    centroid_row = nc4.read_nc4(path_SaveData + r'FDI/centroid_42%s' % season)
    centroid_index_row = scio.loadmat(path_SaveData + r'FDI/centroid_index_42%s.mat' % season)['centroid_index']
    longpath_row = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    longpath_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat' % season)['centroid_index']



    # skip down_lat is positive
    longpath = longpath_row[centroid_row[:, -1] != 1]
    centroid = centroid_row[centroid_row[:,-1]!=1]
    centroid_index = centroid_index_row[centroid_row[:,-1]!=1]
    totol_events = centroid.shape[0]

    longpath_lon70 = []
    for i_time in range(longpath.shape[0]):

        s_longpath_lon70 = [longpath[i_time, 1, :][np.nanargmin(abs(longpath[i_time, 0, :] - 70))]]
        if len(s_longpath_lon70) != 0:
            longpath_lon70.append(s_longpath_lon70[-1])
        else:
            longpath_lon70.append(np.nan)

    classification = pd.cut(longpath_lon70, [0, 15, 150, 210, 270, 320, 360],
                            labels=[1, 2, 3, 4, 5, 6])

    save_labels = np.array(classification)
    save_labels[save_labels == 6] = 1

    for i_sl in [1,4,5]:
        clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(centroid_index[save_labels==i_sl])],
                                             ignore_index=True,axis=1)
    # # step01 all
    # plot(save_labels, longpath, centroid, 'all')
    # continue

    # # step02 pacific
    longpath_3 = longpath[save_labels==3]
    centroid_3 = centroid[save_labels==3]
    centroid_index_3 = centroid_index[save_labels==3]

    save_labels_3= np.zeros(longpath_3.shape[0])
    save_labels_3[centroid_3[:, 3] >= 0] = 1
    save_labels_3[centroid_3[:, 3] < 0] = 2
    for i_sl in [1,2]:
        clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(centroid_index_3[save_labels_3 == i_sl])],
                                             ignore_index=True,axis=1)
    ######### for siberia



    centroid_index_siberia = centroid_index[save_labels == 2]

    type_siberia = []
    filename = []

    for s_filename in centroid_index_siberia:
        time_file_new = ''
        for i in s_filename:
            time_file_new += i
            if i == 'c':
                break
        s_filename = time_file_new
        try:
            # print(s_filename)
            # print(path_moisture_path+s_filename[:-3]+'.mat')
            a = scio.loadmat(path_moisture_path + s_filename[:-3] + '.mat')
        except:
            continue
        index = []
        for i in sorted(a.keys()):
            if i[0] != '_':
                index.append(i)

        traj = a[index[0]][:, :use_day_for_classification * 4, :]
        if traj.shape[2] == 0:
            # print(traj.shape)
            # print(s_filename)
            continue
        classifi, geom = query_inserction(traj, [[(0, 50), (90, 50)], [(5, 50), (5, 90)]])
        ratio_classifi = [len(i) / traj.shape[2] for i in classifi]
        ratio_classifi.extend([max_ratio])
        type_siberia.append(np.argmax(ratio_classifi))
        filename.append(s_filename)

    type_siberia = np.array(type_siberia)
    filename = np.array(filename)
    for i_type in [0,1,2]:
        print(filename[type_siberia == int(i_type)].shape)
        clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(filename[type_siberia == i_type])],
                                             ignore_index=True,axis=1)

    clustering_fileName_Final.to_csv(path_savepath + 'type_filename_%s.csv' % season, index=False)


