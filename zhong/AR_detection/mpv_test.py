import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr

import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
var_with_shortname = {'Hgt':'z', 'Tem':'t', 'Uwnd':'u', 'Vwnd':'v', 'RH':'r'}
central_lon = [359,200,180,180,0,75]
import nc4
import scipy.io as scio
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE','BAF','BSE' , 'BSW', 'MED', 'ARC']
type_name_num = ['0','2','3','4','5','6']
phase = 0
for i in [4]:
    a= scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\composite_%s_%i_global_500.mat"%(type_name_num[i], phase))
    data1 = a['data']
    # centroid = a['centroid']
    # long_c, lat_c = centroid[0][:2]
    # print(centroid)
    # long = np.arange(long_c-45, long_c+45,0.5)
    # lat = np.arange(lat_c-40, lat_c+40, 0.5)
    long = np.arange(0, 360,0.5)
    lat = np.arange(90, -90.01, -0.5)
    def plot_quiver():
        plt.rcParams['hatch.color'] = 'darkred'
        fig, ax = plt.subplots(subplot_kw={'projection': ccrs.Orthographic(central_lon[i], 60)}
                               ,figsize=[4.5,4])
        plt.subplots_adjust(top=0.84,
bottom=0.08,
left=0.07,
right=0.99,
hspace=0.2,
wspace=0.2)
        # i_contour_cut, z, z_ano= data
        z, z_ano = data1


        import cmaps
        import matplotlib.colors as colors
        cb = ax.contourf(long, lat,
                         np.array((z-z_ano),dtype='double')/98,levels=15,cmap=cmaps.cmp_b2r,
                        transform=ccrs.PlateCarree(), norm=colors.CenteredNorm())
        # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
        #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
        #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
        import cmaps
        # cb = ax.contourf(long, lat,
        #                  uv_in_AR(u, v, i_contour_cut), extend='both', levels=np.arange(0, 15.1, 0.5),
        #                  transform=ccrs.PlateCarree(), cmap=cmaps.cmocean_matter, zorder=3)
        plt.colorbar(cb)
        # ax.quiver(long[::delta], lat[::delta_lat],
        #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
        #           transform=ccrs.PlateCarree(), scale=200, zorder=7)
        plt.title(plot_name[i]+' phase %i'%phase)
        ax.coastlines(zorder=9)
        ax.gridlines(draw_labels=True)
        # ax.set_extent()
        # plt.show()
        plt.savefig(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\pic_combine_result/'+
                    plot_name[i]+' phase %i.png'%phase, dpi=400)
        plt.close()
    plot_quiver()

exit(0)



##############################
def get_mpv(date):
    """date containing Hour information"""
    import scipy
    time = pd.to_datetime(date)
    # load hourly anomaly data
    vari_SaveName_all = ['RH', 'Tem', 'Uwnd', 'Vwnd']

    data_row = xr.open_dataset(
        path_PressureData + '%s/' % 'Hgt' + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (
        'Hgt', time.strftime('%Y%m%d')))

    for i in vari_SaveName_all:
        iia = xr.open_dataset(
            path_PressureData + '%s/' % i + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (i, time.strftime('%Y%m%d')))
        data_row = xr.merge([data_row, iia])

    # load present data
    data = data_row.sel(time=time.strftime('%Y%m%d %H:00:00'))

    data = data.sel(level=slice(500, 1000))
    data = data.metpy.parse_cf().squeeze()

    ##############################
    # Get the cross section, and convert lat/lon to supplementary coordinates:

    cross = data.set_coords(('latitude', 'longitude'))

    ##############################
    # For this example, we will be plotting potential temperature, relative humidity, and
    # tangential/normal winds. And so, we need to calculate those, and add them to the dataset:
    cross['dewpoint'] = mpcalc.dewpoint_from_relative_humidity(cross['t'], cross['r'])

    cross['e_Potential_temperature'] = mpcalc.equivalent_potential_temperature(
        cross['level'],
        cross['t'], cross['dewpoint']
    )

    cross['mpv'] = mpcalc.potential_vorticity_baroclinic(cross['e_Potential_temperature'], cross['level'],
                                                         cross['u'], cross['v'])
    return cross


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
plt.rcParams['hatch.color'] = 'darkred'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

times_all = pd.date_range('1980-12-17', '1980-12-19', freq='1D').strftime('%Y%m%d')

sta_AR = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\1980121717_2.nc')
iI_AR_loc = 24
for i_time in times_all:
    vari_SaveName_all = ['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
    # p0 =  xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\surface_pressure.%s.nc'%i_time)

    data_row = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\Hgt.%s.nc' % i_time)
    for i in vari_SaveName_all:
        data_row = xr.merge([data_row, xr.open_dataset(
            r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\%s.%s.nc' % (i, i_time))])

    for iI_time_daily in [5,11,17,23]:
        data = data_row.isel(time=iI_time_daily)
        print(data)
        # p0 = p0.isel(time=iI_time_daily)
        # data = data.sel(level=1000)

        ##############################
        # Get the cross section, and convert lat/lon to supplementary coordinates:

        cross = data.squeeze().set_coords(('latitude', 'longitude'))

        ##############################
        # dx, dy = mpcalc.lat_lon_grid_deltas(cross['longitude'], cross['latitude'])
        # dthetadx, dthetady = mpcalc.geospatial_gradient(cross['z'], dx=dx, dy=dy)
        # dthetadx = np.array(dthetadx)
        # dthetady = np.array(dthetady)
        # dthetadx[:,:20] = -999
        # dthetady[:, :20] = -999
        # fig, axs = plt.subplots(2,1)
        # axs[0].imshow(dthetady)
        # axs[1].imshow(dthetadx)
        # plt.show()
        # contour = sta_AR.sel(time=cross.time)


        i_contour = sta_AR['contour_all'].sel(time_all = cross.time).values


        i_centroid = sta_AR['centroid_all'].sel(time_all = cross.time).values

        def select_rectangle_area(data, centroid, long, lat, r_lat=20, r_lon=30):
            """
            :param data: [hgt, u, v, ...], var is 2d.
            :param centroid: select a rectangle based on centroid and r_lat, r_lon
            :param long:
            :param lat:
            :param r_lat:
            :param r_lon:
            :return:
            """
            def roll_longitude_from_359to_negative_180(data, long):
                roll_length = np.argwhere(long < 180).shape[0] - 1
                long = np.roll(long, roll_length)
                data = np.roll(data, roll_length)
                long = xr.where(
                    long > 180,
                    long - 360,
                    long)
                return data, long

            lon_index = np.argwhere(long == centroid[1])[0]
            if (lon_index - r_lon < 0) | (lon_index + r_lon > long.shape[0]):
                data, long = roll_longitude_from_359to_negative_180(data, long)
                print(long)
                centroid[1] = (centroid[1] + 180) % 360 - 180
                print(centroid[1])
            long_2d, lat_2d = np.meshgrid(long, lat)
            lat_index = np.argwhere(lat == centroid[0])[0]
            lon_index = np.argwhere(long == centroid[1])[0]
            result = []
            for i_data in data:
                s_data = i_data[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])), :]
                lat_2d_flatten = lat_2d[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])),
                                 0]

                s_data = s_data[:, int(lon_index - r_lon):int(lon_index + r_lon)]
                long_2d_flatten = long_2d[0, int(lon_index - r_lon):int(lon_index + r_lon)]

                result.append(s_data)
            return result, [lat_2d_flatten, long_2d_flatten]


        def plot_quiver(u, v):
            plt.rcParams['hatch.color'] = 'darkred'
            fig,ax = plt.subplots(subplot_kw={'projection':ccrs.Orthographic(0, 70)})
            delta = 4
            delta_lat = 2
            cb = ax.contourf(cross.longitude.values, cross.latitude.values,
                            cross['z']/98,
                            transform=ccrs.PlateCarree(), zorder=5)
            plt.colorbar(cb)
            # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
            #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
            #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
            ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                      cross['u'].values[::delta_lat,::delta], cross['v'].values[::delta_lat,::delta],
                      transform=ccrs.PlateCarree(),scale=200, zorder=7)

            ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(i_contour, 0.1),

                        colors='none', levels=[0.1, 1.1],
                        hatches=[2 * '.', 2 * '.'], alpha=0, facecolor='red', edgecolor='red', transform=ccrs.PlateCarree(), zorder=20)

            plt.title(cross.time.values)
            ax.coastlines(zorder=9)
            ax.set_global()
            plt.show()

        def plot_contour(u, v):
            # fig,axs = plt.subplots(1,1,subplot_kw={'projection':ccrs.Orthographic(0, 70)})
            fig, axs = plt.subplots(1, 1, subplot_kw={'projection': ccrs.PlateCarree()})
            delta = 4
            delta_lat = 2
            # data = [cross['t'],cross['r']]
            data = u
            for i in range(1):
                import cmaps
                ax = axs
                # data = xr.open_dataset(r'D:/vor.nc')['vo'].sel(level=1000, time=cross.time)
                # print(i_centroid)

                data, [lat,long] = select_rectangle_area([data], [int(i_centroid[0]),int(i_centroid[1])],
                                                         cross.longitude.values, cross.latitude.values,
                                                         r_lat=80, r_lon=90)

                # z = xr.open_dataset(r'D:/z.nc')['z'].sel(level=1000, time=cross.time)
                # plt.imshow(z)
                # plt.show()
                # slice = []

                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
                #           transform=ccrs.PlateCarree(),scale=0.01, zorder=7)
                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           cross['u'].values[::delta_lat,::delta], cross['u'].values[::delta_lat,::delta],
                #           transform=ccrs.PlateCarree(),scale=500, zorder=7)

                ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(i_contour, 0.1),

                            colors='none', levels=[0.1, 1.1],
                            hatches=[2 * '.', 2 * '.'], alpha=0,edgecolor='black', transform=ccrs.PlateCarree(), zorder=20)

                cb = ax.contourf(long, lat,
                                data[0], extend='both',levels=np.arange(0,20.1,0.5),
                                transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)

                """"# for relative vorticity"""
                # cb = ax.contourf(cross.longitude.values, cross.latitude.values,
                #                 data, extend='both',levels=np.arange(0,30.1,0.5),
                #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)

                # cb = ax.contourf(long, lat,
                #                 np.array(data[0])*1e4, extend='both',levels=np.arange(-4,4.01,0.1),
                #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)
                # cb = ax.contourf(cross.longitude.values, cross.latitude.values,
                #                 data.values*1e4, extend='both',level=np.arange(-4,4,0.1),
                #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu, zorder=5)
                delta=4;delta_lat=2
                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           cross.sel(level=1000)['u'].values[::delta_lat, ::delta], cross.sel(level=1000)['v'].values[::delta_lat, ::delta],
                #           transform=ccrs.PlateCarree(), scale=500, zorder=7)
                plt.colorbar(cb)
                plt.title(cross.time.values)
                ax.coastlines(zorder=9)
                # ax.set_global()
            plt.show()

        cross['dewpoint'] = mpcalc.dewpoint_from_relative_humidity(cross['t'], cross['r'])

        cross['e_Potential_temperature'] = mpcalc.equivalent_potential_temperature(
            cross['level'],
            cross['t'], cross['dewpoint']
        )
        #
        cross['Potential_temperature'] = mpcalc.potential_temperature(
            cross['level'],
            cross['t']
        )

        # fig, ax = plt.subplots(nrows=2, ncols=1)
        # ax[0].imshow(cross['e_Potential_temperature'].sel(level=500).values)
        # cb = ax[1].imshow(cross['e_Potential_temperature'].sel(level=850).values)
        # plt.colorbar(cb)
        # plt.show()
        #
        # exit(0)

        # fig, ax = plt.subplots(nrows=2, ncols=1)
        # cb = ax[0].imshow(cross['e_Potential_temperature'].sel(level=200).values, vmin=280, vmax=350)
        # ax[0].set_title('850hPa')
        # plt.colorbar(cb, ax=ax[0])
        # cb = ax[1].imshow(cross['Potential_temperature'].sel(level=200).values, vmin=280, vmax=350)
        # ax[1].set_title('1000hPa')
        # plt.colorbar(cb, ax=ax[1])
        # plt.show()

        dx, dy = mpcalc.lat_lon_grid_deltas(cross['longitude'], cross['latitude'])
        cross['pv'] = mpcalc.vorticity(cross['u'], cross['v'])
        # cross['pv'] = mpcalc.potential_vorticity_baroclinic(cross['Potential_temperature'], cross['level'],
        #                                                      cross['u'], cross['v'])

        def uv_in_AR():
            u = cross['u'].sel(level=1000).values
            v = cross['v'].sel(level=1000).values
            u_AR = np.mean(u[i_contour>0.5])
            v_AR = np.mean(v[i_contour>0.5])

            u_diff = u-u_AR
            v_diff = v-v_AR
            # return np.sqrt(u_diff**2+ v_diff**2)
            return np.sqrt(u**2+ v**2)

        plot_contour(uv_in_AR(), cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values, cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values,cross['pv'].values)
        continue



        # #         # start figure and set axis
        #         fig, axs = plt.subplots(2,1,figsize=(5, 5))
        #         plt.subplots_adjust(top=0.94,
        # bottom=0.06,
        # left=0.13,
        # right=0.9,
        # hspace=0.315,
        # wspace=0.2)
        #         level = 700
        #         cross = cross.sel(level=level)
        #         # plot and scale absolute vorticity by 1e5
        #         # cf = axs[0].contourf(cross.longitude, cross.latitude, cross['mpv'] * 1e6, np.arange(-4, 4.01, 0.2), cmap=plt.cm.PuOr_r,
        #         #                  transform=ccrs.PlateCarree())
        #
        #         # axs[0].imshow(cross['mpv'] * 1e6)
        #         a = xr.open_dataset(r'D:/download.nc')
        #         a = a.sel(level=level)
        #         a = a.isel(time=5)
        #
        #         print(a)
        #         # cf = axs[0].contourf(cross.longitude, cross.latitude, cross['mpv'] * 1e6, np.arange(-4, 4.01, 0.2), cmap=plt.cm.PuOr_r,
        #         #                  transform=ccrs.PlateCarree())
        #         cb = axs[0].imshow((a['pv'].values) * 1e6, vmin=-10, vmax=10, cmap=plt.cm.PuOr_r)
        #         axs[0].set_title('ERA5 %ihPa'%level)
        #         # plt.colorbar(cb, pad=0, aspect=50)
        #         # cb = axs[0].imshow((a['pv'].values - cross['mpv'].values) * 1e6, vmin=-10, vmax=10, cmap=plt.cm.PuOr_r)
        #         cb = axs[1].imshow((cross['mpv'].values) * 1e6, vmin=-10, vmax=10, cmap=plt.cm.PuOr_r)
        #         axs[1].set_title('%ihPa'%level)
        #         # plt.colorbar(cb, pad=0, aspect=50)
        #         plt.show()
        # cf = axs[1].contourf(cross.longitude, cross.latitude, a['pv'].values[5] * 1e6 - cross['mpv'].values * 1e6, extend='both',
        #                      cmap=plt.cm.PuOr_r,
        #                  transform=ccrs.PlateCarree())
        # plt.colorbar(cf, pad=0, aspect=50)
        # ax.barbs(cross.longitude.values[::2], cross.latitude.values[::2],
        #          cross.u.values[::2,::2], cross.v.values[::2,::2],
        #          color='black', length=4, alpha=0.5,
        #          transform=ccrs.PlateCarree())
        # ax.set(xlim=(260, 270), ylim=(60, 90))
        # ax.set_title('Absolute Vorticity Calculation')

        import metpy
        # a = metpy.interpolate.
        from metpy.units import units

        isentlevs = [315.] * units.kelvin

        isent_data = mpcalc.isentropic_interpolation_as_dataset(
            isentlevs,
            cross['e_Potential_temperature'],
            cross['mpv'] * 1e6,

        )

        a = metpy.interpolate.interpolate_to_isosurface(cross['e_Potential_temperature'].values,
                                                        cross['mpv'].values,
                                                        265)


        # fig, axs = plt.subplots(2,1)
        # axs[0].imshow(isent_data['mpv'].values[0], vmin=-10, vmax=10)
        # axs[1].imshow(a, vmin=-10, vmax=10)
        # plt.show()

        def plot_one_axes_mpv(data, centroid, contour, plot_name, savename=''):
            SMALL_SIZE = 8
            plt.rc('axes', titlesize=SMALL_SIZE)
            plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
            plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
            plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
            plt.rc('axes', titlepad=1, labelpad=1)
            plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
            plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
            plt.rc('xtick.major', size=2, width=0.5)
            plt.rc('xtick.minor', size=1.5, width=0.2)
            plt.rc('ytick.major', size=2, width=0.5)
            plt.rc('ytick.minor', size=1.5, width=0.2)
            plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
            plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
            import matplotlib as mpl

            mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
            # fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
            #                         figsize=[4, 5])
            fig = plt.figure()

            import cmaps
            ax = fig.add_subplot(111,
                                 projection=ccrs.Orthographic(centroid[1], 70))

            import matplotlib
            min_val, max_val = 0.1, 1.0
            n = 10
            orig_cmap = cmaps.prcp_1
            colors = orig_cmap(np.linspace(min_val, max_val, n))
            cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

            # choose 500hPa

            cir_hgt_now = data[0] / 98
            cir_mpv = data[1] * 1e6
            lon = data[2][1]
            lat = data[2][0]

            levels = [np.arange(-50, 50, 5),
                      np.arange(110, 155, 5),
                      np.arange(510, 600, 5)]
            cb = ax.contour(lon, lat, cir_hgt_now,
                            transform=ccrs.PlateCarree(), levels=levels[1], colors='black', zorder=5)
            import cmaps
            cb = ax.contourf(lon, lat, cir_mpv,
                             transform=ccrs.PlateCarree(), levels=np.arange(-3, 3.01, 0.2), cmap=cmaps.MPL_coolwarm,
                             extend='both', zorder=2)

            ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(contour, 0.1),

                        colors='none', levels=[0.1, 1.1],
                        hatches=[2 * '.', 2 * '.'], alpha=0, edgecolor='black', transform=ccrs.PlateCarree(), zorder=20)

            plt.colorbar(cb, orientation="horizontal")

            "==================================="

            ax.set_title('%s' % (plot_name))
            ax.set_extent([0, 359, 30, 90], crs=ccrs.PlateCarree())
            # ax.set_global()
            ax.coastlines(linewidth=0.3, zorder=10)
            # ax.stock_img()
            ax.gridlines(ylocs=[66], linewidth=0.3, color='black', draw_labels=True)
            # plt.show()

            plt.savefig(savename, dpi=400)
            plt.close()


        i_contour = sta_AR['contour_all'].values[iI_AR_loc]
        i_centroid = sta_AR['centroid_all'].values[iI_AR_loc]


        # if study_area[1][0] > study_area[1][1]:



        cross = cross.sel(level=850)

        # cross_rec, [lat_rec, lon_rec] = select_rectangle_area([cross['z'].values,
        #                                                        cross['mpv'].values],
        #                                                       [int(i_centroid[0]), int(i_centroid[1])],
        #                                                       cross['longitude'].values,
        #                                                       cross['latitude'].values,
        #                                                       r_lat=50, r_lon=50*2)

        plot_one_axes_mpv([cross['z'].values, cross['mpv'].values,
                           [cross['latitude'].values, cross['longitude'].values, ]],
                          i_centroid, i_contour, '850hPa', savename='850hPa%i.png' % iI_AR_loc)
        iI_AR_loc += 1

exit(0)



