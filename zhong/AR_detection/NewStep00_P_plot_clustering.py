
import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

"""
===================================
density is created by 1D_FDI.py
===================================
"""

def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1,colors_shape[0]])
    return  ListedColormap(np.concatenate((colors, ones.T), axis=1))

def add_longitude_latitude_labels(ax, xloc):
    gl = ax.gridlines(ylocs=[66], xlocs=[0, 60, 120, 179.9999, -60, -120],
                      linewidth=0.3, color='black',crs=ccrs.PlateCarree(),
                      draw_labels=True,
                      x_inline=True, y_inline=True)


    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}


    fig.canvas.draw()
    for ea in gl.ylabel_artists:
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        ea.set_position([165, 66])

    for ea in gl.xlabel_artists:
        ea.set_visible(True)
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        pos = ea.get_position()
        ea.set_position([pos[0], xloc])

path_SecondData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\\'

path_MainData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
# time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
type_name_num = ['0','2','3','4','5','6']

type_name = ['greenland_east','pacific_east','MS',
             'greenland_west', 'pacific_west', 'GE']

plot_name = ['(a) GRE (28,4.7%)','(c) BSE (19,4.4%)', '(e) MED (26,3.8%)',
             '(b) BAF (20,7.1%)', '(d) BSW (30,13.1%)', '(f) ARC (10,1.6%)']

central_lon = [359,180,75,300,180,75]

var_fmt = ['%0.1f', '%i', '%i', '%0.1f', '%0.1f']
var_title = []
var_ylabel = ['impacted region/the Arctic (units:%)', 'duration (units:6h)']

resolution=0.5

import cmaps
orig_cmap = cmaps.prcp_1


for iI_var, var in enumerate(['density']):

    def get_data():
        data_mean = []
        data_max = []
        for type_SOM in type_name:
            data_mean.append(np.nansum(nc4.read_nc4(path_SecondData + 'mean_1D_%s_%s_%s' % (var, 'DJF', type_SOM)),
                                       axis=0))
            data_max.append(np.nansum(nc4.read_nc4(path_SecondData + 'max_1D_%s_%s_%s' % (var, 'DJF', type_SOM)),
                                       axis=0))
        return np.array(data_mean), np.array(data_max)

    labels = type_name

    frequency_mean, frequency_max = get_data()


    def cal_quiver_var_plot(ax, data):
        from cartopy.util import add_cyclic_point

        data, lon = add_cyclic_point(data, coord=np.arange(0, 360, 0.5))


        base_ax = circulation(ax, lon, np.arange(90, -90.001, -0.5),
                              [[0, 360, 30, 90], np.arange(0, 360, 90), [50, 60, 70, 80]])

        base_ax.colorbar_orientation = 'h'
        import cmaps
        base_ax.contourf_arg = {'cmap': orig_cmap,
                                }
        base_ax.colorbar_fmt = '%i'


        base_ax.ranges_reconstrcution_button_contourf = np.arange(0, 150, 1)
        base_ax.colorbar = False

        base_ax.p_contourf(data)
        # base_ax.p_contour(data)
        base_ax.ax.coastlines(linewidth=0.8)
        # plt.show()
        return base_ax.cb


    data_circulation = frequency_max
    # a = np.ma.masked_less(data_circulation[0],0.01)
    # a[0,:] = 0
    # a[1,:] = 0
    # print(np.min(a), np.max(a))
    # # a = np.ma.masked_greater(a, 20)
    # # plt.imshow(a)
    # # plt.show()
    # import cmaps
    # fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})
    # cb = ax.contourf(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), a,
    #                  transform=ccrs.PlateCarree(),cmap=cmaps.prcp_1, extend='max')
    # ax.set_extent([0, 359, 30, 90], crs=ccrs.PlateCarree())
    # plt.colorbar(cb)
    # # ax.set_global()
    # ax.coastlines()
    # plt.show()
    max_data = int(np.max(data_circulation[:, :, :].flatten()))
    min_data = int(np.min(data_circulation[:, :, :].flatten()))
    mm_data = np.max(np.abs([min_data, max_data]))
    # data_circulation[0][data_circulation[0]<20] = 0
    SMALL_SIZE = 8
    plt.rc('axes', titlesize=SMALL_SIZE)
    plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
    plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('axes', titlepad=1, labelpad=1)
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
    plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('xtick.major', size=2, width=0.5)
    plt.rc('xtick.minor', size=1.5, width=0.2)
    plt.rc('ytick.major', size=2, width=0.5)
    plt.rc('ytick.minor', size=1.5, width=0.2)
    plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
    import matplotlib as mpl

    mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
    # fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection': ccrs.NorthPolarStereo()},
    #                         figsize=[4, 5])
    fig = plt.figure(figsize=[5,3.9])

    plt.subplots_adjust(top=0.97,
bottom=0.1,
left=0.005,
right=0.995,
hspace=0.14,
wspace=0.055)


    for level in range(2):
        for i_ax in range(3):
            import cmaps
            ax = fig.add_subplot(2,3,level*3+i_ax+1, projection=ccrs.Orthographic(central_lon[level*3+i_ax], 70))
            # ax = fig.add_subplot(2,3,level*3+i_ax+1, projection=ccrs.PlateCarree())

            # cb = cal_quiver_var_plot(ax,data_circulation[level*2+i_ax])

            import matplotlib
            min_val, max_val = 0.1, 1.0
            n = 10

            colors = orig_cmap(np.linspace(min_val, max_val, n))
            cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

            "==================================="
    #         # choose 500hPa
    #         cir_hgt_row = nc4.read_nc4(path_MainData + 'circulation/AR_Hgt_DJF_%s'%type_name[level*3+i_ax])[:,0,:,:]
    #
    #         cir_hgt_now = cir_hgt_row[0]/98
    #         print(np.max(cir_hgt_now), np.min(cir_hgt_now))
    #         cir_hgt_avg = cir_hgt_row[1]/98
    #         cir_hgt_p = cir_hgt_row[2]
    #         cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(-30,31,2),
    #                          transform=ccrs.PlateCarree(), colors='black',zorder=5)
    #         # cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(510,600,5),
    #         #                  transform=ccrs.PlateCarree(), colors='black',zorder=5)
    #         # cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(110,155,5),
    #         #                  transform=ccrs.PlateCarree(), colors='black',zorder=5)
    #         # cb = ax.contour(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), cir_hgt_now, levels=np.arange(1050,1250,10),
    #         #                  transform=ccrs.PlateCarree(), colors='black',zorder=5)
    #
    #         # apply t-test
    #
    #         cb = ax.contourf(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), np.ma.masked_greater(cir_hgt_p, 0.1),
    #
    #                          colors='none',levels=[0,0.1],
    # hatches=[3*'.',3*'.'], alpha=0, transform=ccrs.PlateCarree(), zorder=20)
    #
    #         cb = ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), cir_hgt_now-cir_hgt_avg,
    #                          levels=np.arange(-15, 15, 0.5),
    #                          transform=ccrs.PlateCarree(),cmap=cmap, extend='both',zorder=2)
    #         ax.scatter(central_lon[level*3+i_ax], 70, s=8, c='black',transform=ccrs.PlateCarree(),zorder=8)
            "==================================="


            cb = ax.contourf(np.arange(0,360,0.5), np.arange(90,-90.1,-0.5), data_circulation[level*3+i_ax],
                             levels=np.arange(1,151,4),
                             transform=ccrs.PlateCarree(),cmap=cmap, extend='max', alpha=1)
            ax.set_title('%s' % (plot_name[level*3+i_ax]))
            ax.set_extent([0, 359, 10, 90], crs=ccrs.PlateCarree())
            # ax.set_global()
            ax.coastlines(linewidth=0.3,zorder=10)
            # ax.stock_img()
            # ax.gridlines(ylocs=[66],linewidth=0.3,color='black')
            # plt.show()

    cb_ax = fig.add_axes([0.15, 0.07, 0.7, 0.01])
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)

    cb_ax.set_xlabel('counts')
    plt.show()
    save_pic_path = path_MainData + 'pic_combine_result/'
    os.makedirs(save_pic_path, exist_ok=True)
    plt.savefig('ex_figure1_500hPa.png', dpi=400)
    plt.close()


