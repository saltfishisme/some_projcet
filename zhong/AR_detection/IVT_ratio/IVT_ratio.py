import nc4
import numpy as np
import pandas as pd

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\\'
type_name = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
resolution = 0.5
classification_range = [[[0,15],[320,359]], [270,320], [150, 210],[150, 210],[15, 150],[15, 150]]
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
time_Seaon_divide_Name = ['DJF', 'MAM', 'JJA', 'SON']
path_IVTratio = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\IVT_ratio\\'
var = 'IVT_ratio'
def get_IVT_ratio():
    total = nc4.read_nc4(path_IVTratio + 'IVT_totalYear')
    classification_range = [[[0,15],[320,359]], [270,320], [150, 210],[150, 210],[15, 150],[15, 150]]
    var = 'IVT_ratio'
    ratio = []
    for iI_type, type_SOM in enumerate(type_name):
        s_ratio = []
        for iI_season, season in enumerate(time_Seaon_divide_Name):
            if iI_type != 0:
                lon_range = (np.array(classification_range[iI_type])/resolution).astype('int')
                climately = np.sum(total[:,iI_season, lon_range[0]:lon_range[1]])
            else:
                lon_range = (np.array(classification_range[iI_type][0])/resolution).astype('int')
                climately = np.sum(total[:,iI_season, lon_range[0]:lon_range[1]])
                lon_range = (np.array(classification_range[iI_type][1])/resolution).astype('int')
                climately += np.sum(total[:,iI_season, lon_range[0]:lon_range[1]])

            a = np.sum(nc4.read_nc4(path_IVTratio + r'mean_1D_%s_%s_%s' % (var, season, type_SOM)))
            b = np.sum(nc4.read_nc4(path_IVTratio + r'max_1D_%s_%s_%s' % (var, season, type_SOM)))
            # s_ratio.append((np.round(b*100/climately,1), np.round(a*100/climately,1)))

            s_ratio.append((np.round(b*100/np.sum(total[:, iI_season]),1), np.round(a*100/np.sum(total[:, iI_season]),1)))

        ratio.append(s_ratio)
    return ratio

a = pd.DataFrame(get_IVT_ratio()).T
print(a)
# a.to_csv('IVT_ratio_allArctic.csv')
# exit(0)
total = nc4.read_nc4(path_IVTratio + 'IVT_totalYear')
print(total.shape)
for iI_season, season in enumerate(time_Seaon_divide_Name):
    a = 0
    b = 0
    for iI_type, type_SOM in enumerate(type_name):

        a += np.sum(nc4.read_nc4(path_IVTratio + r'mean_1D_%s_%s_%s' % (var, season, type_SOM)))
        b += np.sum(nc4.read_nc4(path_IVTratio + r'max_1D_%s_%s_%s' % (var, season, type_SOM)))
    print(a/np.sum(total[:,iI_season]))
    print(b/np.sum(total[:,iI_season]))
    exit(0)

#
# def fit_line2(x, y):
#     from scipy.stats import linregress
#     from scipy.stats.mstats import zscore
#     (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(zscore(x), zscore(y))
#     return beta_coeff, rvalue, pvalue
# var = 'duration'
# df_AR = pd.DataFrame(index=np.arange(1979,2020+1))
# df_all = pd.DataFrame(index=np.arange(1979,2020+1))
# for iI_type, type_SOM in enumerate(type_name):
#     s_df_AR = []; s_df_year = []
#     s_df_all = []
#     for iI_season, season in enumerate(['DJF']):
#
#         filename = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season,skiprows=1, names=type_name_row)[type_SOM]
#         year = [int(i[:4]) for i in np.array(filename) if isinstance(i, str)]
#         s_df_year.extend(year)
#         a = nc4.read_nc4(path_SecondData + r'mean_1D_%s_%s_%s' % (var, season, type_SOM))
#         b = nc4.read_nc4(path_SecondData + r'max_1D_%s_%s_%s' % (var, season, type_SOM))
#
#         s_df_AR.extend(a)
#         s_df_all.extend(b)
#     df_AR = pd.DataFrame(np.array([s_df_AR, s_df_all, s_df_year]).T, columns=['AR', 'all', 'year'])
#     df_AR = df_AR[['AR','all']].groupby([df_AR['year']]).sum()
#     sdf = pd.DataFrame(np.zeros(2021-1979), index=np.arange(1979, 2020+1))
#     df_AR = pd.concat([df_AR, sdf], axis=1)
#     df_AR = df_AR.fillna(0)
#     # print(df_AR.mean()/360)
#     print(fit_line2(np.array(df_AR.index),np.array(df_AR['AR'])))

