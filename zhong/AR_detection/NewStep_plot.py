import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import numpy as np
import scipy.io as scio
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import scipy.interpolate as interp
from matplotlib import patches

def create_elipse(save_labels, centroid):
    def reject_outliers(data, m = 0.8):
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev if mdev else 0.
        return np.nanmean(data[s<m])

    add_centroid = centroid[:, 3]
    add_centroid[add_centroid<0] = add_centroid[add_centroid<0]+180
    centroid[:,3] = add_centroid

    for iI, i in enumerate(np.unique(save_labels)):
        print(i)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection=ccrs.NorthPolarStereo())
        ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
        ax.coastlines()

        s_centroid = np.apply_along_axis(reject_outliers, 0, centroid[save_labels == i])
        e1 = patches.Ellipse((s_centroid[1], s_centroid[0]), s_centroid[4], s_centroid[5],
                             angle=180-np.nanmean(centroid[save_labels == i, 3]), linewidth=2, fill=False, zorder=2, transform=ccrs.PlateCarree())
        ax.add_patch(e1)
        e1 = patches.Ellipse((s_centroid[1], s_centroid[0]), s_centroid[-2], s_centroid[-1],
                             angle=180-s_centroid[-3], linewidth=2, fill=False, color='red', transform=ccrs.PlateCarree())
        ax.add_patch(e1)
        plt.show()
        plt.close()
    return


def projection_transform(lon, lat, central_latitude, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    central_latitude_abs = abs(central_latitude)
    if (central_latitude_abs <= 60) & (central_latitude_abs >= 30):
        if central_latitude > 0:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': 30, 'lat_2': 60}
        else:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': -30, 'lat_2': -60}

    elif central_latitude_abs > 60:
        proj_Out = {'proj': 'ups'}

    else:
        proj_Out = {'proj': 'merc', 'lon_0': 120}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)


def interpolation_nan_for_longpath(longpath):
    longpath_shape = longpath.shape
    arr_ref_size = longpath.shape[2]
    longpath = longpath.reshape(longpath.shape[0] * 2, longpath.shape[-1])
    for i in range(len(longpath)):
        arr1 = longpath[i][~np.isnan(longpath[i])]  # skip nan
        arr1_interp = interp.interp1d(np.arange(arr1.size), arr1)
        longpath[i] = arr1_interp(np.linspace(0, arr1.size - 1, arr_ref_size))
    return longpath.reshape(longpath_shape)


def SOM_param(data, xylength, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = []
    for lead in ['X', 'Y']:
        for i in range(1, xylength):
            varname.append(str(i) + lead)
    varname.extend(addtional_varname)
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})


def SOM_spcific_param(data, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = addtional_varname
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})


def plot(save_labels, longpath):
    # if longpath:
    #     longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s'%season)
    size = int(np.ceil(len(np.unique(save_labels)) / 3))
    fig = plt.figure()
    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            ax = fig.add_subplot(size, 3, iI + 1, projection=ccrs.NorthPolarStereo())
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())
            ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.05,
                       transform=ccrs.PlateCarree())
            ax.scatter(np.apply_along_axis(reject_outliers, 0, longpath[save_labels == i, 1]),
                       np.apply_along_axis(reject_outliers, 0, longpath[save_labels == i, 0]),
                       s=0.05,
                       transform=ccrs.PlateCarree())
            ax.coastlines()
    plt.suptitle(season)
    plt.show()
    plt.close()
    # # plot classification result
    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    for i in np.unique(save_labels[save_labels == 0]):
        ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    ax.coastlines()
    plt.title(season)
    plt.show()
    plt.close()


""" divided into 4 regions"""

for season in ['DJF', 'MAM', 'JJA', 'SON']:
    # path_SaveData = r'/home/linhaozhong/work/AR_detection/'
    path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'

    centroid_row = nc4.read_nc4(path_SaveData + r'FDI/centroid_42%s' % season)
    centroid_index = scio.loadmat(path_SaveData + r'FDI/centroid_index_42%s.mat' % season)['centroid_index']
    longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    longpath_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat' % season)['centroid_index']

    longpath_lon70 = []
    for i_time in range(longpath.shape[0]):

        s_longpath_lon70 = [longpath[i_time, 1, :][np.nanargmin(abs(longpath[i_time, 0, :] - 70))]]
        if len(s_longpath_lon70) != 0:
            longpath_lon70.append(s_longpath_lon70[-1])
        else:
            longpath_lon70.append(np.nan)

    classification = pd.cut(longpath_lon70, [0, 15, 150, 210, 270, 320, 360],
                            labels=[1, 2, 3, 4, 5, 6])

    save_labels = np.array(classification)
    save_labels[save_labels == 6] = 1

    longpath = longpath[save_labels==3]
    centroid_row = centroid_row[save_labels==3]
    save_labels= np.zeros(longpath.shape[0])
    save_labels[centroid_row[:,3]>=0] = 1
    save_labels[centroid_row[:,3]<0] = 2
    print(len(save_labels[save_labels==1]))
    plot(save_labels, longpath)

    create_elipse(save_labels, centroid_row)

    ########################################################
    '''for all SOM data: longpath, centroid, direction, orientation'''
    # # path_SaveData = r'/home/linhaozhong/work/AR_detection/'
    path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
    #
    centroid_row = nc4.read_nc4(path_SaveData + r'FDI/centroid_42%s' % season)
    centroid_index = scio.loadmat(path_SaveData + r'FDI/centroid_index_42%s.mat' % season)['centroid_index']
    centroid_row = centroid_row[:, :4]
    longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    longpath_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat' % season)['centroid_index']
    longpath = interpolation_nan_for_longpath(longpath)
    longpath = longpath.reshape([longpath_index.shape[0], longpath.shape[1] * longpath.shape[2]])
    longpath = np.append(longpath, centroid_row, axis=1)

    longpath = longpath[save_labels == 2]

