import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================

=============================
"""



def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='anomaly'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    if types == 'anomaly':
        return data_present - data_anomaly
    else:
        return [data_present, data_anomaly]


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


def main_3phase(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'AR_contour_42/%s' % (time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))
        print(AR_loc)

        if var == 't2m':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                        '2m_temperature',
                                                        't2m')
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())


        elif var == 'IVT':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = load_ivt_total(pd.to_datetime(i_time))
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'tcwv':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                        'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour')
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'sic':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i_present, s_i_climatly = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                         'Sea_Ice_Cover',
                                                         'siconc', types='now')
                s_i = s_i_present-s_i_climatly

                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                # s_i[s_i_present<0.0001]=np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        return avg

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            phase1 = []
            phase2 = []
            phase3 = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean = contour_cal(time_file, var)
                phase1.extend(s_mean[0])
                phase2.extend(s_mean[1])
                phase3.extend(s_mean[2])

            phase = [phase1, phase2, phase3]
            for iI_phase, i_phase in enumerate(phase):
                nc4.save_nc4(np.array(i_phase),
                             path_SaveData + 'PDF/all_%s' % (var + '%i_'%iI_phase + season + '_' + type_name[iI]))


# # main('area')
# # main('duration')
# main_3phase('IVT')
# main_3phase('t2m')
# main_3phase('tcwv')
#
# # main('lat')
# # main('lon')
# # main_3phase('str')
# main_3phase('sic')
# # # main_3phase('advection')
# # # main_3phase('adibatic')
# # # main_3phase('ssr')
# # # main_3phase('slhf')
# # # main_3phase('sshf')
# #
# # # main_3phase('t2m')
# exit(0)


path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF_for_ARbegin_3phase/'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE', 'BAF','BSE', 'BSW',
             'MED', 'ARC']

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

infig_fontsize = 4

resolution=0.5
var_all = ['IVT', 'tcwv']
var_all = ['t2m', 'sic']



SMALL_SIZE = 5
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize


for season in time_Seaon_divide_Name:

    def get_data():
        data_mean = []
        for s_type_name in type_name:
            s_data_mean = []

            for var in var_all:

                s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%i_%s_%s' % (var, phase, season, s_type_name))*plot_param[var][5])

            data_mean.append(s_data_mean)

        return np.array(data_mean)





    def plots(ax, data, bin_width):
        percentile_label = ['Q1', 'Q2', 'Q3']
        percentile = np.percentile(data, [25,50,75])
        mean = np.nanmean(data)
        HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
        # ax.boxplot(data)

        _, _, bar_container = ax.hist(data,bins=bin_width,density=True, lw=1,
                                  ec="black", fc="black",zorder=2)

        # yticks = ax.get_yticks()
        # ax.set_yticks(yticks)
        # ax.set_yticklabels((yticks*len(data)).astype('int'))

        ylim_max = ax.get_ylim()[1]
        trans = ax.get_xaxis_transform()
        for i_percentile in range(3):
            ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
            ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
                    transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
                    bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
            )
            if plot_param[var][4] == 0:
                values =percentile[i_percentile].astype('int')
            else:
                values = np.round(percentile[i_percentile], plot_param[var][4])

            ax.text(percentile[i_percentile], 1.01, values, transform=trans,
                    horizontalalignment='center', fontsize=infig_fontsize)

        return mean


    fig, axs = plt.subplots(6, 4, figsize=[7, 5])
    plt.subplots_adjust(top=0.95,
bottom=0.04,
left=0.055,
right=0.98,
hspace=0.4,
wspace=0.2)




    for phase in [0,1]:
        if phase == 0:
            plot_param = {
                'IVT': [['IVTmean=\n', ' kg/(m*s)'], 'IVT', [-10, 250], [0, 0.03], 0, 1, 600],
                'tcwv': [['TCWVmean=\n', ' kg/(m**2)'], 'TCWVano', [-1.5, 8], [0, 0.6], 1, 1, 800],

                't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-3, 16], [0, 0.25], 2, 1, 800],
                'sic': [['SICmean=\n', ''], 'SICano', [-7, 5], [0, 0.5], 2, 100, 100]
            }
        else:
            plot_param = {
                'IVT': [['IVTmean=\n', ' kg/(m*s)'], 'IVT', [-10, 200], [0, 0.035], 0, 1, 200],
                'tcwv': [['TCWVmean=\n', ' kg/(m**2)'], 'TCWVano', [-1.5, 8], [0, 0.6], 1, 1, 150],

                't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-5, 20], [0, 0.25], 2, 1, 80],
                'sic': [['SICmean=\n', ''], 'SICano', [-10, 5], [0, 0.5], 2, 100, 100]
            }

        frequency = get_data()

        for i in range(6):
            for j in range(2):

                # def flatten(l):
                #     l = np.array([item for sublist in l for item in sublist])
                #     return l[~np.isnan(l)]
                # bin_col = np.histogram(frequency[i][j], bins=plot_param[var_all[j]][6])[1]

                data = frequency[i][j]
                ax = axs[i][phase*2+j]
                var = var_all[j]
                mean = plots(ax, data, plot_param[var_all[j]][6])


                if (var != 'IVT') & (var != 'tcwv') :
                    ax.axvline(0, color='black',zorder=0)
                ax.set_xlim(plot_param[var][2])
                ax.set_ylim(plot_param[var][3])


                mean_label = plot_param[var][0]
                if plot_param[var][4] == 0:
                    mean = int(mean)
                else:
                    mean = np.round(mean, plot_param[var][4])

                if var != 'sic':
                    x = 0.85
                else:
                    x=0.15
                ax.text(x, 0.6, mean_label[0]+str(mean)+mean_label[1],
                        transform=ax.transAxes,horizontalalignment='center',fontsize=4)

                if (var == 'IVT') & (phase == 0) :
                    axs[0][0].set_xlim([-10,500])
                    axs[-1][0].set_xlim([-10,400])
                if (var == 'tcwv') & (phase == 0) :
                    axs[0][1].set_xlim([-1.5,15])
                if (var == 'IVT') & (phase == 1) :
                    axs[2][0].set_xlim([-10,300])
            axs[i][0].set_ylabel(plot_name[i] + '\nProbability')




for i_col in range(2):
    var = var_all[i_col]
    axs[0][i_col].set_title(plot_param[var][1]+' phase 1', pad=10)
    axs[0][i_col+2].set_title(plot_param[var][1]+' phase 2', pad=10)

for i_rows in range(6):
    for j_col in range(4):
        axs[i_rows][j_col].annotate(figlabelstr[4 * i_rows + j_col], [0.01, 1.01], xycoords='axes fraction')

# plt.show()
plt.savefig('figure5_%s.png'%(var_all[0]), dpi=300)
plt.close()







