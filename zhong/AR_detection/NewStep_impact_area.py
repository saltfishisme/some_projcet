import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import numpy as np
import scipy.io as scio
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import scipy.interpolate as interp

"""
may be uesless!
"""


def projection_transform(lon, lat, central_latitude, xy2ll=False):
    lon = np.array(lon); lat = np.array(lat)
    shape_lon = lon.shape;  shape_lat = lat.shape
    lon = lon.flatten(); lat = lat.flatten()

    central_latitude_abs = abs(central_latitude)
    if (central_latitude_abs <= 60) & (central_latitude_abs >= 30):
        if central_latitude > 0:
            proj_Out = {'proj':'lcc', 'lon_0':120, 'lat_1':30, 'lat_2':60}
        else:
            proj_Out = {'proj':'lcc', 'lon_0':120, 'lat_1':-30, 'lat_2':-60}

    elif central_latitude_abs > 60:
        proj_Out = {'proj':'ups'}

    else:
        proj_Out = {'proj':'merc', 'lon_0':120}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)
def interpolation_nan_for_longpath(longpath):
    longpath_shape = longpath.shape
    arr_ref_size = longpath.shape[2]
    longpath = longpath.reshape(longpath.shape[0]*2, longpath.shape[-1])
    for i in range(len(longpath)):
        arr1 = longpath[i][~np.isnan(longpath[i])] # skip nan
        arr1_interp = interp.interp1d(np.arange(arr1.size),arr1)
        longpath[i] = arr1_interp(np.linspace(0,arr1.size-1,arr_ref_size))
    return longpath.reshape(longpath_shape)

for season in ['SON']:
    # path_SaveData = r'/home/linhaozhong/work/AR_detection/'
    path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'

    centroid_row = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s'%season)
    centroid_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat'%season)['centroid_index']


    '''for centroid clustering'''
    # #skip latitude characteristic
    # b = centroid.copy()
    # b[:, 0] = 70
    # long_ups, lat_ups = projection_transform(centroid[:,1], centroid[:, 0], 70)
    # b[:, 1] = long_ups
    # b[:, 0] = lat_ups
    fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})
    for i in range(len(centroid_row)):
        ax.coastlines()
        ax.set_extent([0,359,20,90], crs=ccrs.PlateCarree())

        ax.scatter(centroid_row[i, 1], centroid_row[i, 0], s=0.1,transform=ccrs.PlateCarree())
    plt.show()
    plt.close()
    '''for longpath clustering'''
    centroid = interpolation_nan_for_longpath(centroid_row)
    for i in range(len(centroid)):
        centroid[i,1], centroid[i, 0] = projection_transform(centroid[i,1], centroid[i, 0], 70)
    b = centroid.reshape([centroid.shape[0], centroid.shape[1]*centroid.shape[2]])



    # BGM classification
    from sklearn import mixture
    s_dpgmm = mixture.BayesianGaussianMixture(n_components=5,
                                              covariance_type='full').fit(b)
    save_labels = s_dpgmm.predict(b)

    # # plot classification result
    fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})

    for i in np.unique(save_labels):

        ax.scatter(centroid_row[save_labels == i, 1], centroid_row[save_labels == i, 0], s=0.1,transform=ccrs.PlateCarree())
    ax.coastlines()
    plt.show()
    # plt.savefig(path_SaveData+'FDI/IA_42%s.png'%(season))


    def cal_3phase(contour):
        division = np.array_split(contour, 3)
        division_mean = []
        for i in division:
            division_mean.append(np.nanmean(i, axis=0))
        return np.array(division_mean)
    def contour_cal(time_file):
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break
        info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))
        time_all = info_AR_row.time_all.values
        time_AR = info_AR_row.time_AR.values

        index = np.argwhere(time_all==time_AR[0]).flatten()[0]
        info_AR_3phase = cal_3phase(info_AR_row['contour_AR'].values)
        info_all_3phase = cal_3phase(info_AR_row['contour_all'].values[index:])
        return [info_AR_3phase, info_all_3phase]

    for i in np.unique(save_labels):
        times_file_all = centroid_index[save_labels==i]
        info_AR_3phase = []
        info_all_3phase = []

        for time_file in times_file_all:

            s_info_AR_3phase, s_info_all_3phase = contour_cal(time_file)
            info_all_3phase.append(s_info_all_3phase)
            info_AR_3phase.append(s_info_AR_3phase)

        info_all_3phase = np.array(info_all_3phase)
        info_AR_3phase = np.array(info_AR_3phase)
        print(info_all_3phase.shape)
        print(np.nansum(info_AR_3phase, axis=0).shape)
        nc4.save_nc4(np.nansum(info_AR_3phase, axis=0), path_SaveData+'FDI/IA_AR_42%s_som%i'%(season, i))
        nc4.save_nc4(np.nansum(info_all_3phase, axis=0), path_SaveData+'FDI/IA_all_42%s_som%i'%(season, i))

        def plot(a_path, season, som, all_AR):
            a = nc4.read_nc4(a_path)
            fig, axs = plt.subplots(ncols=3, nrows=1, subplot_kw={'projection':ccrs.NorthPolarStereo()}, figsize=[6,2.5])
            plt.subplots_adjust(top=0.94,
                                bottom=0.12,
                                left=0.02,
                                right=0.98,)
            for i, s_a in enumerate(a):
                ax = axs[i]
                cb = ax.pcolormesh(np.arange(0,360,0.5), np.arange(90,-90.25,-0.5),  s_a, vmax=20,  transform=ccrs.PlateCarree())
                ax.set_title('%s SOM%i %s'%(season, som, all_AR), loc='left')
                ax.coastlines()
                ax.gridlines(ylocs=[70], xlocs=[])
                ax.set_extent([0, 359, 60, 90], ccrs.PlateCarree())
            ax1 = fig.add_axes([0.1, 0.1, 0.8, 0.05])
            plt.colorbar(cb, cax=ax1, orientation = 'horizontal')
            plt.savefig(a_path+'.png', dpi=400)
            plt.close()
        if os.path.exists(path_SaveData+'FDI/IA_AR_42%s_som%i.png'%(season, i)) != 1:
            plot(path_SaveData+'FDI/IA_AR_42%s_som%i'%(season, i), season, i, 'AR')
        if os.path.exists(path_SaveData+'FDI/IA_all_42%s_som%i.png'%(season, i)) != 1:
            plot(path_SaveData+'FDI/IA_all_42%s_som%i'%(season, i), season, i, 'all')

