import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================
select a standard event based on some critieria.
1, temperature
2, intensity

=============================
"""





"""
=============================
get circulation and other characteristics.
=============================
"""

import datetime

def load_Q(s_time, var, types='climate'):
    """
    :param s_time: datetime64
    :param var:
    :param type: climate or now
    :return:
    """

    def create_save_adv_adi(time):
        import metpy.calc as mpcalc
        from metpy.units import units
        import numpy as np
        import xarray as xr
        import os

        def get_data(time, var, level_isobaric):
            '''
            :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
            :param level_isobaric:
            :return:
            '''

            def get_svar_data(var):
                data = xr.open_dataset(
                    path_PressureData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
                return data

            result = []
            if np.isin(var, 'Hgt').any():
                hght_var = get_svar_data('Hgt').sel(time=time, level=level_isobaric)['z']
                result.append(hght_var)
            if np.isin(var, 'Tem').any():
                temp_var = get_svar_data('Tem').sel(time=time, level=level_isobaric)['t']
                result.append(temp_var)
            if np.isin(var, 'Uwnd').any():
                u_wind_var = get_svar_data('Uwnd').sel(time=time, level=level_isobaric)['u']
                result.append(u_wind_var)
            if np.isin(var, 'Vwnd').any():
                v_wind_var = get_svar_data('Vwnd').sel(time=time, level=level_isobaric)['v']
                result.append(v_wind_var)
            if np.isin(var, 'Wwnd').any():
                w_wind_var = get_svar_data('Wwnd').sel(time=time, level=level_isobaric)['w']
                result.append(w_wind_var)

            lat_var = result[0]['latitude']
            lon_var = result[0]['longitude']
            result.append(lat_var)
            result.append(lon_var)
            return result

        def tem_advection_1000(time):
            temp_var, u_wind_var, v_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Uwnd', 'Vwnd'],
                                                                          level_isobaric=1000)

            # between lat/lon grid points
            dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)

            # Calculate temperature advection using metpy function
            adv = mpcalc.advection(temp_var * units.kelvin, u=u_wind_var * units('m/s'), v=v_wind_var * units('m/s'),
                                   dx=dx, dy=dy)
            return adv

        def adiabatic(time, level_isobaric=[1000, 850, 500, 200]):
            temp_var, w_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Wwnd'], level_isobaric=level_isobaric)

            pressure_var = np.ones([len(level_isobaric), len(lat_var), len(lon_var)])
            for i in range(len(level_isobaric)):
                # transform hPa into Pa
                pressure_var[i] = pressure_var[i] * level_isobaric[i] * 100
            pressure_var = pressure_var * units.Pa

            # Calculate temperature advection using metpy function
            adv = mpcalc.static_stability(pressure_var, temp_var * units.kelvin)

            Rd = units.Quantity(8.314462618, 'J / mol / K') / units.Quantity(28.96546e-3, 'kg / mol')
            adiabatic_warming = adv * (w_wind_var * units('Pa/s')) * pressure_var / Rd
            return adiabatic_warming

        s_advection = tem_advection_1000(time)
        s_adiabatic = adiabatic(time)

        s_year = int(time.strftime('%Y'))
        path_SaveData = r'/home/linhaozhong/work/AR_analysis/'
        os.makedirs(path_SaveData + 'adibatic_cli/%i' % s_year, exist_ok=True)
        os.makedirs(path_SaveData + 'advection_cli/%i' % s_year, exist_ok=True)
        s_adiabatic.to_netcdf(path_SaveData + 'adibatic_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))
        s_advection.to_netcdf(path_SaveData + 'advection_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))

    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q_withpath(s_time_path):
        now = xr.open_dataset(path_Qdata + '%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    if types == 'climate':
        cli = []
        for i_year in range(1979, 2020 + 1):
            # judge whether this date is valid
            try:
                datetime.datetime(i_year, int(s_time.strftime('%m')), int(s_time.strftime('%d')))
            except:
                print('%s is invalid' % (str(i_year) + s_time.strftime('%m%d%H')))
                continue

            s_time_str = var + '_cli/' + str(i_year) + '/' + str(i_year) + s_time.strftime('%m%d%H')
            if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
                a = True
            else:
                create_save_adv_adi(pd.to_datetime(str(i_year) + '-' + s_time.strftime('%m-%d %H:00:00')))
            cli.append(load_Q_withpath(s_time_str))
        cli = np.nanmean(np.array(cli), axis=0)
        now = load_Q_withpath(var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H'))
        return now - cli
    elif types == 'now':
        s_time_str = var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H')
        if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
            a = True
        else:
            create_save_adv_adi(s_time)
        now = load_Q_withpath(s_time_str)
        return now


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north


def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_MainData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m





def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='climate'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    if types == 'climate':
        data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                           time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values
    if types == 'climate':
        return data_present - data_anomaly
    elif types == 'now':
        return data_present


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_detection/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_detection/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']

var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
               'hcc': 'high_cloud_cover',
               'lcc': 'low_cloud_cover',
            'mcc': 'medium_cloud_cover',
               'tcc': 'total_cloud_cover',
               'tciw': 'total_column_cloud_ice_water',
            'tclw': 'total_column_cloud_liquid_water',

}

def main_linePhase(var, types='now'):
    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        if bu_all_or_AR == 'AR':
            all_or_AR = [time_AR, info_AR_row['contour_AR'].values]
        else:
            all_or_AR = [time_all, info_AR_row['contour_all'].values]

        def cc_call_date_now(s_time, types):
            """
            to avoid too many Duplication
            :param s_time: pd.datetime
            :param types: now, climate
            :return:
            """
            if (var == 'advection') | (var == 'adibatic'):
                data = load_Q(
                    s_time,
                    var, types=types)
            elif var == 'ivt':
                data = load_ivt_total(
                    s_time)
            else:
                data = get_circulation_data_based_on_date(
                    s_time,
                    var_LongName[var],
                    var, types=types)
            return data



        arg = []

        '''
        ####################
        loop time
        ####################
        '''
        time_loop = all_or_AR[0]

        # provided climately
        if types == 'now':
            data_climately = cc_call_date_now(pd.to_datetime(time_loop[0])-np.timedelta64(delay_time, 'D'), types=types)

        for datetime_now in time_loop:
            datetime_now = pd.to_datetime(datetime_now)

            # provided climately
            if types == 'acceleration':
                data_climately = cc_call_date_now(datetime_now-np.timedelta64(6, 'h'), types='now')
                data_now = cc_call_date_now(datetime_now, types='now')
            else:
                data_now = cc_call_date_now(datetime_now, types=types)

            # mask latitude lesser than 70
            contour = all_or_AR[1][0]
            contour[int(30 / resolution), :] = 0

            if types != 'climately':
                if var == 'siconc':
                    # contour[s_i==0] = 0
                    contour[data_climately==0] = 0
                    if data_now[contour == 1].flatten().size == 0:
                        return 'no ice'

                data_now = data_now-data_climately

            # get percentile and mean of every time step
            characteristics = [np.nanmean(data_now[contour == 1].flatten())]

            characteristics.extend(np.percentile(data_now[contour == 1].flatten(), [5, 25, 50, 75, 95]))
            return characteristics ### for AR[0]
            # arg.append(characteristics)
        return arg

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            characteristics = []
            characteristics_filename = []
            times_file_all = df[i].values

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue
                s_ii = contour_cal(time_file, var)
                # print(s_ii)
                characteristics.append(s_ii)
                characteristics_filename.append(time_file)

            characteristics = np.array(characteristics)
            # print(characteristics)
            # max of averaged variable
            max_index = np.nanargmax(characteristics[:,0]) # param

            print('avg_vari = ', characteristics_filename[max_index])
            max_index = np.nanargmax(characteristics[:,-1]) # param

            print('max_vari = ', characteristics_filename[max_index])

            # if types != 'climately':
            #     types_save = types + str(delay_time)+'_%i'%AR_series
            # else:
            #     types_save = types
            #
            # nc4.save_nc4(characteristics,
            #              path_SaveData + 'PDF_line/%s_%s_%s' % (types_save, bu_all_or_AR,
            #                                                     var + 'DAY%s_' % time_step + season + '_' +
            #                                                     type_name[iI]))



for delay_time in [3]:
    types = 'now'  # 'acceleration'/ 'now'/ 'climately'
    bu_all_or_AR = 'AR'
    # main_linePhase('ssr', types)
    # main_linePhase('slhf', types)
    # main_linePhase('sshf', types)
    main_linePhase('t2m', types)
    main_linePhase('ivt', types)
    # main_linePhase('advection', types)
    # main_linePhase('adibatic', types)
    # main_linePhase('str', types)
    # main_linePhase('siconc', types)
    # main_linePhase('cbh', types)
    # main_linePhase('hcc', types)
    # main_linePhase('lcc', types)
    # main_linePhase('mcc', types)
    # main_linePhase('tcc', types)
    # main_linePhase('tciw', types)
    # main_linePhase('tclw', types)
exit(0)

# # bu_all_or_AR = 'AR'
# # main_linePhase('str', types)
# # main_linePhase('advection', types)
# # main_linePhase('adibatic', types)
# # main_linePhase('ssr', types)
# # main_linePhase('slhf', types)
# # main_linePhase('sshf', types)
# # main_linePhase('t2m', types)
# # main_linePhase('sic', types)
# exit(0)
