import os
from geopy.distance import distance
import nc4
import pandas as pd
from scipy.ndimage import gaussian_filter
from fil_finder import FilFinder2D
import astropy.units as u
import numpy as np
from skimage import measure
import matplotlib.pyplot as plt
import cv2
import xarray as xr
import logging.handlers
import traceback


def daily_AR_detection(row_data, IVT_lat, IVT_long, given_threshold):
    '''
    :param data: np.array, 2D data.
    :param lat, lon, resolution: lat(list or array-like), lon(list or array-like) and resolution(float) of the data.
    :param given_threshold: float, the minimum value which can be marked as 'high_value'.
    :param numll_min: float, the points in one high_value region should be more than numll_min, otherwise, this region
                             will be filtered.

    :return: array, shape is [the number of high_value area, 7],
            the first five indexes are  x_centre, y_centre, width, height, angle,
            The sixth and seventh indices are area of fitted ellipse and high_value region.
            The eighth and ninth indices are averaged wind speed of fitted ellipse and high_value region
    '''

    data = row_data - given_threshold
    data = gaussian_filter(data, sigma=1)

    def cal_contour(data, roll=False):

        ####### prepare data
        s_IVT_long = IVT_long.copy()
        if roll != False:
            data = np.roll(data, roll, axis=1)
            s_IVT_long = np.roll(IVT_long, roll)
        thresh = cv2.threshold(data, 1, 255, cv2.THRESH_BINARY)[1]
        labels = measure.label(thresh, connectivity=1, background=0)

        #######
        Mask_onePic = np.zeros(data.shape, dtype='uint8')
        lineMask_onePic = np.zeros(data.shape, dtype='uint8')
        contourMask = []
        lineMask = []
        s_df_result = pd.DataFrame()

        if roll != False:
            num_AR = 1000
        else:
            num_AR = 0

        ''' #########################   loop for every contour in this image   ###########################'''
        for label in np.unique(labels):
            if label == 0:
                continue

            #######  prepare labelMask, labelMask is a narray contains one contour
            labelMask = np.zeros(thresh.shape, dtype="uint8")
            labelMask[labels == label] = 255
            numPixels = cv2.countNonZero(labelMask)

            ####### get index of this contour
            ar_pathway_index = np.argwhere(labelMask != 0)

            '''' ####### numPixels , longitude and latitude that a AR_detection event must spread across '''
            if roll != False:
                if (np.in1d(np.unique(ar_pathway_index[:, 1]), [roll])).any() == False:
                    continue
            else:
                if (np.in1d(np.unique(ar_pathway_index[:, 1]), [0, thresh.shape[1] - 1])).any():
                    continue

            if not ((np.in1d(s_IVT_long[np.unique(ar_pathway_index[:, 1])], bu_study_box[0]).any()) & (
                    np.in1d(IVT_lat[np.unique(ar_pathway_index[:, 0])], bu_study_box[1]).any())):
                continue


            if (numPixels > de_the_number_of_grids):
                simple = labelMask
                simple[simple == 255] = 1

                ######## sketetons
                fil = FilFinder2D(simple, distance=1 * u.pix, mask=simple)
                fil.preprocess_image(skip_flatten=True)
                fil.create_mask(border_masking=True, verbose=False,
                                use_existing_mask=True)
                fil.medskel(verbose=False)
                result = fil.analyze_skeletons(branch_thresh=8 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')

                # print(fil.lengths())
                # print(fil.lengths()/np.sum(fil.branch_properties['length']))
                # # print(fil.branch_properties)
                # exit(0)
                # print(fil.curvature_branches())
                # plt.imshow(simple, cmap='gray')
                # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                # plt.axis('off')
                # plt.show()

                '''###################  criteria ################################### '''

                #######
                if (fil.branch_properties['number'] > de_branch_number):
                    # print('kill by shape')
                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()
                    continue

                ######
                if ((fil.lengths() / np.sum(fil.branch_properties['length'])) < de_branch_ratio * u.pix):
                    # print('kill by ratio')
                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()
                    continue

                # plt.imshow(labelMask, cmap='gray')
                # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                # plt.axis('off')
                # plt.show()

                #######
                sslabels = measure.label(labelMask, connectivity=1, background=0)
                regions = measure.regionprops(sslabels, intensity_image=row_data)
                length = regions[0].axis_major_length
                width = regions[0].axis_minor_length
                if length / width < 2:
                    # plt.imshow(labelMask)
                    # plt.title('kill by length/width')
                    # plt.show()
                    continue

                #######
                try:
                    fil.analyze_skeletons(branch_thresh=40 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')
                except:
                    continue
                ll_loc = np.array(fil.end_pts)[0]
                lat_loc = IVT_lat[ll_loc[:, 0]]
                lon_loc = s_IVT_long[ll_loc[:, 1]]
                try:
                    length = distance((lat_loc[0], lon_loc[0]), (lat_loc[1], lon_loc[1])).km
                    if length < 2000:
                        continue
                except:
                    print('error')

                contourMask.append(np.array(labelMask))
                lineMask.append(np.array(fil.skeleton_longpath, dtype="uint8"))

                Mask_onePic = cv2.add(Mask_onePic, labelMask)
                lineMask_onePic = cv2.add(lineMask_onePic, np.array(fil.skeleton_longpath, dtype='uint8'))

                num_AR += 1
                contours = measure.find_contours(labelMask)[0]
                s_df = pd.DataFrame()

                def array_to_str(array):
                    saveStr = ''
                    for i in array:
                        saveStr += ',' + str(i)
                    return saveStr

                s_df['y'] = array_to_str([contours[:, 0]])
                s_df['x'] = array_to_str([contours[:, 1]])
                s_df['time'] = df_time
                s_df['num'] = num_AR
                s_df_result = s_df_result.append(s_df, ignore_index=True)

        if roll != False:
            thresh = np.roll(thresh, -roll, axis=1)
            lineMask_onePic = np.roll(lineMask_onePic, -roll, axis=1)
            Mask_onePic = np.roll(Mask_onePic, -roll, axis=1)

            if num_AR > 1000:
                contourMask = np.array(contourMask)
                lineMask = np.array(lineMask)
                for s_lineMask in range(len(lineMask)):
                    contourMask[s_lineMask] = np.roll(contourMask[s_lineMask], -roll, axis=1)
                    lineMask[s_lineMask] = np.roll(lineMask[s_lineMask], -roll, axis=1)
        return num_AR, s_df_result, contourMask, lineMask, thresh, lineMask_onePic

    # cal contour for row map
    num_AR, s_df_result, mask_ss, lineMask, thresh, lineMask_M = cal_contour(data)

    # cal contour for rolled map
    s_roll_number = int(data.shape[1] / 2)

    num_AR_cross, s_df_result_cross, mask_ss_cross, lineMask_cross, thresh_cross, lineMask_M_cross = cal_contour(data,
                                                                                                                 roll=s_roll_number)

    if (num_AR > 0) & (num_AR_cross > 1000):
        mask_ss.extend(mask_ss_cross)
        nc4.save_nc4(np.array(mask_ss), path_daily_AR_save + s_time + '_%02i' % timestep)
    elif (num_AR > 0) & (num_AR_cross <= 1000):
        nc4.save_nc4(np.array(mask_ss), path_daily_AR_save + s_time + '_%02i' % timestep)
    elif (num_AR <= 0) & (num_AR_cross > 1000):
        nc4.save_nc4(np.array(mask_ss_cross), path_daily_AR_save + s_time + '_%02i' % timestep)

    if num_AR_cross > 1000:
        s_df_result = s_df_result.append(s_df_result_cross, ignore_index=True)
        num_AR = num_AR + num_AR_cross - 1000

    if num_AR != 0:
        # fig, ax = plt.subplots(ncols=2, nrows=1, figsize=(5,1.8))
        # plt.subplots_adjust(top=0.96,
        #                     bottom=0.11,
        #                     left=0.01,
        #                     right=0.99,
        #                     hspace=0.2,
        #                     wspace=0.17)
        # ax[0].imshow(thresh, vmin=0)
        # ax[1].imshow(lineMask_M)
        # plt.title(s_time+' '+str(timestep))
        # ax[0].axhline(40, linewidth=0.4)
        # ax[0].axhline(60, linewidth=0.4)
        # # ax[2].imshow(lineMask)
        # #plt.show()
        # plt.savefig(path_SaveData+'pic/'+s_time+'_'+str(timestep)+'.png', dpi=500)
        # plt.close()
        return s_df_result


path_IVT_Data = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_Proj_Data = r'/home/linhaozhong/work/AR_China_500_extend/'

de_quantile_field_Name = 'quantile_field_500'

# bu_study_box = [np.arange(0, 360, 0.5), np.arange(70, 90.4, 0.5)]
bu_study_box = [np.arange(60, 240.1, 0.5), np.arange(70, -15.01, -0.5)]
# [lon_across (0,360), lat_across (90,-90)], Consistent with IVT data,
# longitude and latitude that a AR_detection event must spread across

de_Temporal_resolution = 6
time_loop = pd.date_range('1979-01-01', '2020-12-31', freq='1D')
de_branch_ratio = 0.7
de_branch_number = 5
de_the_number_of_grids = 400  # to remove the AR_detection pathway with the number of grids is smaller 400

os.makedirs(path_Proj_Data, exist_ok=True)
path_daily_AR_save = path_Proj_Data + 'daily_AR/'
os.makedirs(path_daily_AR_save, exist_ok=True)

# time_Seaon_divide = [[12, 1, 2]]
# time_Seaon_divide_Name = ['winter']
time_Seaon_divide = [[6, 7, 8]]
time_Seaon_divide_Name = ['summer']

''' ####### logger '''
import logging.handlers
import traceback
logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)
f_handler = logging.FileHandler('%sOF_log_step02.log' % path_Proj_Data, mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))
logger.addHandler(f_handler)

for sI_Season, s_Season in enumerate(time_Seaon_divide):
    df_result = pd.DataFrame(columns=['time', 'num', 'y', 'x'])
    s_time_loop = time_loop[np.in1d(time_loop.month, s_Season)]

    dual_threshold = nc4.read_nc4(path_Proj_Data + de_quantile_field_Name)[sI_Season]

    for s_time in s_time_loop.strftime('%Y%m%d'):
        print(s_time)
        index_vivt = xr.open_dataset(
            path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time[:4], s_time))
        index_uivt = xr.open_dataset(
            path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time[:4], s_time))

        for s_time_daily in range(int(24 / de_Temporal_resolution)):
            timestep = (s_time_daily + 1) * de_Temporal_resolution - 1
            df_time = index_vivt['p72.162'].isel(time=timestep).time.values
            ivt_north = index_vivt['p72.162'].values[timestep]
            ivt_east = index_uivt['p71.162'].values[timestep]
            ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
            IVT_long = index_uivt.longitude.values
            IVT_lat = index_uivt.latitude.values

            try:
                s_df_result = daily_AR_detection(ivt_total, IVT_lat, IVT_long, dual_threshold)
            except Exception as e:
                logger.error(s_time)
                logger.error(traceback.format_exc())
                continue
            df_result = df_result.append(s_df_result)

    df_result.to_csv(path_Proj_Data + 'daily_AR_%s.csv' % (time_Seaon_divide_Name[sI_Season]))
