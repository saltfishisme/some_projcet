import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
1D characteristics such as intensity, duration, frequency and so on.
get all AR characteristics from type_filename_SON.csv
while this file will provided the clustering result of AR and the corresponding AR fileAddress
"""

class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps
        self.ax = ax
        self.crs = ccrs.PlateCarree()
        self.cmaps = ''
        self.colorbar = True
        self.orientation = 'h'
        self.ranges = ranges
        self.fmt_contourf = '%.1f'
        self.fmt_contour = '%.1f'
        self.ranges_reconstrcution_button_contourf = False
        self.ranges_reconstrcution_button_contour = False
        self.linewidths = 1
        self.clabel_size = 5
        self.cmaps = cmaps.WhiteGreen
        self.colorbar = True
        self.orientation = 'h'
        self.spacewind = [1, 1]
        self.colors = False
        self.print_min_max = False
        self.contour_arg = {'colors': 'black'}
        self.contourf_arg = {}
        self.quiverkey_arg = {'X': 0.85, 'Y': 1.05, 'U': 10, 'label': r'$10 \frac{m}{s}$'}
        self.contour_clabel_color = 'white'
        self.MidPointNorm = False

        def del_ll(range, lat, lon):
            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]

            del_area = ll_del(lat, lon, range[0])
            # 人为重新排序
            if del_area[0] > del_area[1]:
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1] + 1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area

        if self.ranges[0] is not False:
            self.lat, self.lon, self.del_area = del_ll(ranges, lat, lon)
        else:
            self.lat, self.lon, self.del_area = [lat, lon, False]

    def del_data(self, data):
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1] + 1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        def ranges_create(begin, end, inter):
            class MidPointNorm(Normalize):
                def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                    Normalize.__init__(self, vmin, vmax, clip)
                    self.midpoint = midpoint

                def __call__(self, value, clip=None):
                    if clip is None:
                        clip = self.clip

                    result, is_scalar = self.process_value(value)

                    self.autoscale_None(result)
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if not (vmin < midpoint < vmax):
                        raise ValueError("midpoint must be between maxvalue and minvalue.")
                    elif vmin == vmax:
                        result.fill(0)  # Or should it be all masked? Or 0.5?
                    elif vmin > vmax:
                        raise ValueError("maxvalue must be bigger than minvalue")
                    else:
                        vmin = float(vmin)
                        vmax = float(vmax)
                        if clip:
                            mask = ma.getmask(result)
                            result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                              mask=mask)

                        # ma division is very slow; we can take a shortcut
                        resdat = result.data

                        # First scale to -1 to 1 range, than to from 0 to 1.
                        resdat -= midpoint
                        resdat[resdat > 0] /= abs(vmax - midpoint)
                        resdat[resdat < 0] /= abs(vmin - midpoint)

                        resdat /= 2.
                        resdat += 0.5
                        result = ma.array(resdat, mask=result.mask, copy=False)

                    if is_scalar:
                        result = result[0]
                    return result

                def inverse(self, value):
                    if not self.scaled():
                        raise ValueError("Not invertible until scaled")
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if cbook.iterable(value):
                        val = ma.asarray(value)
                        val = 2 * (val - 0.5)
                        val[val > 0] *= abs(vmax - midpoint)
                        val[val < 0] *= abs(vmin - midpoint)
                        val += midpoint
                        return val
                    else:
                        val = 2 * (value - 0.5)
                        if val < 0:
                            return val * abs(vmin - midpoint) + midpoint
                        else:
                            return val * abs(vmax - midpoint) + midpoint

            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0 + inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if isinstance(ranges_reconstrcution_button, list) or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button
        else:

            ranges_min = np.nanmin(data);
            ranges_max = np.nanmax(data)
            if ranges_reconstrcution_button is False:
                if ranges_min > 0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                ticks = (ranges_max - ranges_min) / 30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

        if self.MidPointNorm is not False:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       norm=self.MidPointNorm, **self.contourf_arg)

        else:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both',
                                       **self.contourf_arg)

        if self.colorbar:
            if self.orientation == 'h':
                orientation = 'horizontal'
            else:
                orientation = 'vertical'
            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.fmt_contourf)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=5, transform=self.crs, **self.contour_arg)
        cbs = self.ax.clabel(
            cb, fontsize=self.clabel_size,  # Typically best results when labelling line contours.
            colors=['black'],
            inline=True,  # Cut the line where the label will be placed.
            fmt=self.fmt_contour,  # Labes as integers, with some extra space.
        )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]],
                             u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m
def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    """ date containing Hour information """
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5
bu_length_lack = '_42'

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path%s/'%bu_length_lack
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'
path_row_moisture_path = r'/home/linhaozhong/work/Northpolar_era5/1950_2019/output_int6hr_NorthPolar-1deg/'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


def main(var):

    def contour_cal(time_file, var='IVT'):
        def area_weighted_mean(data, contour):
            data = data[:-1]
            contour = contour[:-1]
            area_weight = scio.loadmat(path_GlobalArea + 'GridAL_ERA0_5degGridC.mat')['S']
            area_weight[contour == 0] = 0
            area_weight = area_weight / np.sum(area_weight)

            data[contour == 0] = np.nan

            avg_AR = np.nansum(data * area_weight)

            max_AR = data.flatten()[np.nanargmax((data * area_weight).flatten())]

            return [avg_AR, max_AR]

        def area_of_AR(contour):
            """
            :param contour:
            :return: the ratio of the maximum area compared with the Arctic
            """
            contour = contour[:-1]
            area_weight = scio.loadmat(path_GlobalArea + 'GridAL_ERA0_5degGridC.mat')['S']

            # mask exceed the Arctic
            lat = int(30 / resolution)
            contour[lat:, :] = 0
            area_weight[lat:, ] = 0
            area_weight_total = np.sum(area_weight)

            # mask exceed contour
            area_weight[contour == 0] = 0
            area_weight = np.sum(area_weight)
            return area_weight / area_weight_total

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_MainData + '%sAR_contour%s/%s' % (file_contour_Data, bu_length_lack,time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        if var == 't2m':
            avg = []
            for iI_time in range(len(time_AR)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
                                                         '2m_temperature',
                                                         't2m')
                contour = info_AR_row['contour_AR'].values[iI_time]
                contour[int(30/resolution),:] = 0
                if np.sum(contour) == 0:
                    continue
                avg.append(area_weighted_mean(s_i, contour))
            avg = np.mean(np.array(avg), axis=0)

        elif var == 'IVT':
            avg = []
            for iI_time in range(len(time_AR)):
                s_i = load_ivt_total(pd.to_datetime(time_AR[iI_time]))
                contour = info_AR_row['contour_AR'].values[iI_time]
                avg.append(area_weighted_mean(s_i, contour))

            avg = np.mean(np.array(avg), axis=0)
            print(avg.shape)
            #
            # s_i = load_ivt_total(pd.to_datetime(time_AR[0]))
            # avg = area_weighted_mean(s_i, info_AR_row['contour_AR'].values[0])

        elif (var == 'advection') | (var == 'adibatic'):
            avg = []
            for iI_time in range(len(time_AR)):
                s_i = load_Q(pd.to_datetime(time_AR[iI_time]), var)
                contour = info_AR_row['contour_AR'].values[iI_time]
                avg.append(area_weighted_mean(s_i, contour))

            avg = np.mean(np.array(avg), axis=0)
            print(avg.shape)

        elif var == 'area':
            avg = []
            for i_contour in info_AR_row['contour_all'].values:
                s_avg = area_of_AR(i_contour)
                avg.append(s_avg)
            avg = np.max(avg)
            avg = [avg, avg]

        elif var == 'duration':
            index = np.argwhere(time_all == time_AR[0]).flatten()[0]
            avg = [len(time_AR), len(time_all[index:])]

        elif var == 'IVT_ratio':
            avg_all = 0
            for i_time in info_AR_row['time_all'].values:
                s_i = load_ivt_north_in70(pd.to_datetime(i_time))
                s_contour = info_AR_row['contour_all'].sel(time_all=i_time, lat=70).values
                avg_all += np.nansum(s_i[s_contour == 1])
            avg_AR = 0
            for i_time in info_AR_row['time_AR'].values:
                s_i = load_ivt_north_in70(pd.to_datetime(i_time))
                s_contour = info_AR_row['contour_AR'].sel(time_AR=i_time, lat=70).values
                avg_AR += np.nansum(s_i[s_contour == 1])
            avg = [avg_all, avg_AR]

        elif var == 'density':
            avg = []
            avg.append(np.nansum(info_AR_row['contour_AR'].values, axis=0))
            avg.append(np.nansum(info_AR_row['contour_all'].values, axis=0))
        return avg

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values
            mean_AR = []
            max_AR = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean, s_max = contour_cal(time_file, var)
                max_AR.append(s_max)
                mean_AR.append(s_mean)

            max_AR = np.array(max_AR)
            mean_AR = np.array(mean_AR)


            nc4.save_nc4(mean_AR,
                         path_MainData + 'combine_result/mean_1D_%s' % (var + '_' + season + '_' + type_name[iI]))
            nc4.save_nc4(max_AR,
                         path_MainData + 'combine_result/max_1D_%s' % (var + '_' + season + '_' + type_name[iI]))


def Pwm():

    def contour_cal_DRM(time_file):
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        time_file = time_file_new

        info_AR_row = xr.open_dataset(path_MainData + '%sAR_contour%s/%s' % (file_contour_Data,
                                                                              bu_length_lack, time_file))

        time_AR = info_AR_row['time_AR'].values
        contour_AR = info_AR_row['contour_AR']
        time_daily = pd.date_range(time_AR[0], time_AR[-1], freq='1D').strftime('%Y%m%d')
        moisture_path_all = {}
        for i_time_daily in time_daily:
            contour_daily = np.nansum(
                contour_AR.sel(time_AR=i_time_daily, lat=slice(89.5, 60.5), lon=slice(0.5, 359.5)).values, axis=0)[
                            ::2, ::2].flatten()
            try:

                a = scio.loadmat(path_row_moisture_path + '%sres_6hr.mat' % i_time_daily)

                moisture_path = a['pos_int']
                moisture_PWm = a['PWm_loc']
                lat = np.squeeze(a['lat_H'][:, 0])
                lon = np.squeeze(a['lon_H'][0, :])
            except:
                print('nonono')
                return
            moisture_path = moisture_path[:, :, contour_daily > 0]
            moisture_PWm = moisture_PWm[:, contour_daily > 0]
            sums_PWm = np.zeros([lat.shape[0], lon.shape[0]])
            for i_grid in range(moisture_path.shape[2]):
                for i_time in range(moisture_path.shape[1]):
                    ll = moisture_path[:, i_time, i_grid]
                    # sums_PWm[np.where(lat == ll[1]), np.where(lon == ll[0])] += moisture_PWm[i_time, i_grid]
                    sums_PWm[np.where(lat == ll[1]), np.where(lon == ll[0])] += 1

            moisture_path_all['a' + i_time_daily] = sums_PWm

        scio.savemat(path_MainData + 'Traj/%s.mat' % (time_file[:-3]),
                     moisture_path_all)
        return

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            pwm = []
            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue
                time_file_new = ''
                for i in time_file:
                    time_file_new += i
                    if i == 'c':
                        break
                time_file = time_file_new
                contour_cal_DRM(time_file)
                try:
                    a = scio.loadmat(path_MainData + 'Traj/%s.mat' % (time_file[:-3]))
                except:
                    continue
                for i_key in a.keys():
                    if i_key[0] != '_':
                       pwm.append(a[i_key])

            nc4.save_nc4(np.nansum(np.array(pwm),axis=0),
                         path_MainData + 'combine_result/Traj_%s' % (season + '_' + type_name[iI]))
            # nc4.save_nc4(max_AR,
            #              path_SaveData + 'combine_result/max_1D_%s' % (var + '_' + season + '_' + type_name[iI]))
def t2m():
    """get t2m anomaly based on the density of AR grid."""
    def contour_cal_t2m(time_file):
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break
        time_file = time_file_new
        info_AR_row = xr.open_dataset(path_MainData + '%sAR_contour%s/%s' % (file_contour_Data, bu_length_lack, time_file))
        info_AR = np.nansum(info_AR_row['contour_all'].values, axis=0)
        info_AR[info_AR_row.lat.values < 70, :] = 0  # only across 70
        # three phases is introduced into divided AR contour
        count_3phase_index = np.nanmax(info_AR) / 3
        info_AR_3range = [np.where(info_AR < count_3phase_index * 2, 0, info_AR),
                          np.where((info_AR >= count_3phase_index * 2) | (info_AR < count_3phase_index), 0, info_AR),
                          np.where(info_AR >= count_3phase_index, 0, info_AR), ]
        info_AR_3num = [np.where(info_AR < 11, 0, info_AR),
                        np.where((info_AR >= 11) | (info_AR < 7), 0, info_AR),
                        np.where(info_AR >= 7, 0, info_AR), ]

        def t2m_anomaly_in_info_AR(info_AR_3phase):
            anomaly_phase = []
            times_begin = pd.to_datetime(info_AR_row['time_AR'].values[0])
            times = pd.date_range(times_begin, times_begin + np.timedelta64(30, 'D'), freq='6H')

            break_all = 0
            for s_time in times:
                if break_all == 1:
                    anomaly_phase.append([np.nan, np.nan, np.nan])
                    continue
                t2m_anomaly_field = get_circulation_data_based_on_date(s_time, '2m_temperature', 't2m')
                s_anomaly_phase = []
                for ar_contour in info_AR_3phase:
                    if np.max(ar_contour) == 0:
                        s_anomaly_phase.append(np.nan)
                        continue
                    s_contour = t2m_anomaly_field[ar_contour != 0].flatten()
                    t2m_ano = np.percentile(s_contour, 50)
                    if t2m_ano < 0:
                        t2m_ano = np.nan
                    s_anomaly_phase.append(t2m_ano)
                s_anomaly_phase = np.array(s_anomaly_phase)

                if len(s_anomaly_phase[np.isnan(s_anomaly_phase)]) == 3:
                    break_all = 1
                anomaly_phase.append(s_anomaly_phase)

            return np.array(anomaly_phase)

        t2m_anomaly_3range = t2m_anomaly_in_info_AR(info_AR_3range)
        t2m_anomaly_3num = t2m_anomaly_in_info_AR(info_AR_3num)

        return t2m_anomaly_3range, t2m_anomaly_3num
    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            t2m_anomaly_3range = []
            t2m_anomaly_3num = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_t2m_anomaly_3range, s_t2m_anomaly_3num = contour_cal_t2m(time_file)
                t2m_anomaly_3range.append(s_t2m_anomaly_3range)
                t2m_anomaly_3num.append(s_t2m_anomaly_3num)


            nc4.save_nc4(np.array(t2m_anomaly_3range),
                         path_MainData + 'combine_result/t2m_range_%s' % (season + '_' + type_name[iI]))
            nc4.save_nc4(np.array(t2m_anomaly_3num),
                         path_MainData + 'combine_result/t2m_num_%s' % (season + '_' + type_name[iI]))

def get_IVT_all():
    """
    get all vIVT to the Arctic, climately. to get the ratio of IVT.
    divided by season.
    :return: [clustering, season, longitude]
    """
    years = np.arange(1979,2020+1)
    yearly_sum_IVT = []
    for i_year in years:
        sums = np.zeros([4,int(360/resolution)])
        times = pd.date_range('%i-01-01 05:00'%i_year, '%i-12-31 23:00'%i_year, freq='6H')
        for iI_season, i_season in enumerate(time_Seaon_divide):
            i_time = times[np.in1d(times.month, i_season)]
            print(i_time)
            for si_time in i_time:

                sums[iI_season] += load_ivt_north_in70(si_time)
        yearly_sum_IVT.append(sums)
    nc4.save_nc4(np.array(yearly_sum_IVT), path_MainData + 'IVT_totalYear')
# get_IVT_all()
# main('IVT_ratio')
Pwm()
main('area')
main('duration')
main('IVT')
# main('t2m')
main('density')
# t2m()


