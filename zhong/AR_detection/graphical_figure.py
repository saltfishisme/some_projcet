from matplotlib.font_manager import FontProperties
import matplotlib.patches
import matplotlib.pyplot as plt
import matplotlib.textpath

import cartopy.crs as ccrs


def main():
    fig = plt.figure(figsize=[8, 8])
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_longitude=180,central_latitude=50))

    ax.coastlines()
    ax.gridlines()
    ax.stock_img()
    # ax.set_extent([0,359,0,89], crs=ccrs.PlateCarree())

    plt.show()


if __name__ == '__main__':
    main()