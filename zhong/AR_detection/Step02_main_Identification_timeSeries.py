from shapely.geometry import Polygon
import scipy.io as scio
import nc4
import pandas as pd
from scipy.ndimage import gaussian_filter
import numpy as np
from skimage import measure
import cv2
import os
import xarray as xr

path_IVT_Data = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_Proj_Data = r'/home/linhaozhong/work/AR_NP85/'
de_quantile_field_Name = 'daily_85'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['winter']

sI_Season = 0 ###### needed to re-check

bu_Data_begin_year = '1978'  # if ERA5 is from 1979, then set it as '1978'
bu_main_period_length_all = [4] # a AR_detection event should last at least 3 times (Temporal_resolution is 6h
bu_lack_period_length_all = [2]
#  In an ARS, their temporal discontinuity is allowed up to 2 times (12 hr), as long as two AR_detection pathways partially overlap.

bu_Area_Protion_for_connective_contour = 0.4 # two AR_detection pathways partially overlap
bu_the_number_of_grids = 400 # to remove the AR_detection pathway with the number of grids is smaller 400




resolution = 0.5
slong = np.arange(0,180, 0.5)
IVT_long = np.append(slong, np.arange(-180,0, 0.5))
IVT_lat = np.arange(90,-90.25,-0.5)
bu_roll_num = int(len(IVT_long)/2)

for bu_main_period_length in bu_main_period_length_all:
    for bu_lack_period_length in bu_lack_period_length_all:

        for season in time_Seaon_divide_Name:
            df_row = pd.read_csv(path_Proj_Data + r'daily_AR_%s.csv' % (season), parse_dates=[0], usecols=[1, 2, 3, 4], infer_datetime_format=True, keep_date_col=True)

            def prepare_data(df):

                df['diff'] = df['time'].diff() / np.timedelta64(6, 'h')
                df['diff'] = df['diff'].fillna(0)
                df['index'] = np.zeros(df.shape[0])

                ####### select
                df['index'][df['diff']<=bu_lack_period_length] = 1
                return df



            def detection(df_prepared):

                main_period_length = (df_prepared['time'].iloc[[0, -1]].diff() / np.timedelta64(6, 'h') + 1).values[1]
                if (main_period_length < bu_main_period_length) | (df_prepared.shape[0] <= 3):
                    return np.zeros(df_prepared.shape[0])

                result_1000 = detection_by_timeSeries(df_prepared.copy(), roll_1000=True)
                df_prepared_Out1000 = df_prepared[(result_1000==0) & (df_prepared['num'] <1000)].copy()

                # whether df_prepared_Out1000 need to be re-grouped
                df_prepared_Out1000 = prepare_data(df_prepared_Out1000)


                # regroupby from 0
                num_0 = [0]
                # print(df_prepared_Out1000)
                if np.isin(df_prepared_Out1000['index'].values, [0]).any():
                    # print(np.argwhere(df_prepared_Out1000['index'].values == 0))
                    num_0 = list(np.argwhere(df_prepared_Out1000['index'].values == 0)[0])

                num_0 = np.unique([0]+ num_0+[len(df_prepared_Out1000)])

                num_start = np.max(result_1000)

                result_less1000 = []
                for i_num_0 in range(len(num_0))[:-1]:

                    s_result = detection_by_timeSeries(df_prepared_Out1000.iloc[num_0[i_num_0]:num_0[i_num_0+1]].copy())
                    s_result[s_result>0] = s_result[s_result>0]+num_start
                    num_start = np.max(s_result)
                    result_less1000.extend(s_result.copy())

                result = np.array(result_1000)
                result_less1000 = np.array(result_less1000)
                result[(result_1000==0) & (df_prepared['num'] <1000)] = result_less1000
                return result


            def detection_by_timeSeries(df, roll_1000=False):
                def create_polygon(p1_x, p1_y, p2_x, p2_y, num_12=[0, 0], given_p2xy=5):
                    '''
                    :param p1_x: String,
                    :param p1_y:
                    :param p2_x:
                    :param p2_y:
                    :param given_p2xy: if p2_x and p2_y dont need to pass function to_float()
                    :return: Spatial overlap, 0-1
                    '''

                    def to_float(String):
                        sdata = String.replace('\n', ' ')
                        sdata = (sdata[1:-1].split(' '))
                        try:
                            sdata = [float(item) for item in filter(lambda x: x != '', sdata)]
                        except:
                            print('1')
                        return np.array(sdata)

                    def cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y):
                        if (len(p1_x)<3) |(len(p2_x)<3):
                            return 0
                        p1 = Polygon(zip(p1_x, p1_y));
                        p2 = Polygon(zip(p2_x, p2_y))
                        # if NorthPolar !=False:
                        # import matplotlib.pyplot as plt
                        # plt.plot(*p1.exterior.xy)
                        # plt.plot(*p2.exterior.xy)
                        # plt.show()
                        try:
                            pp = p1.intersection(p2)
                        except:
                            return 0
                        try:
                            return np.nanmax([pp.area / p1.area, pp.area / p2.area])
                        except:
                            return 0

                    p1_x = to_float(p1_x)
                    p1_y = to_float(p1_y)
                    if given_p2xy == 5:
                        p2_x = to_float(p2_x)
                        p2_y = to_float(p2_y)

                    if roll_1000:
                        # if num is bigger than 1000, then need to roll. for a num_1000 AR, split it into two x,y,
                        # one is in left of figure, and the other is in the right.
                        if (num_12[0] > 1000) & (num_12[1] > 1000):
                            return cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y)

                        elif num_12[0] > 1000:
                            p1_x = p1_x - bu_roll_num
                            p1_x[p1_x < 0] = p1_x[p1_x < 0] + (bu_roll_num * 2)

                            p1_x_1 = p1_x[p1_x < bu_roll_num]
                            p1_y_1 = p1_y[p1_x < bu_roll_num]

                            p1_x_2 = p1_x[p1_x > bu_roll_num]
                            p1_y_2 = p1_y[p1_x > bu_roll_num]

                            ratio_1 = cal_overlap_ratio(p1_x_1, p1_y_1, p2_x, p2_y)
                            ratio_2 = cal_overlap_ratio(p1_x_2, p1_y_2, p2_x, p2_y)
                            return ratio_1 + ratio_2
                        elif num_12[1] > 1000:

                            p2_x = p2_x - bu_roll_num
                            p2_x[p2_x < 0] = p2_x[p2_x < 0] + (bu_roll_num * 2)

                            p2_x_1 = p2_x[p2_x < bu_roll_num]
                            p2_y_1 = p2_y[p2_x < bu_roll_num]

                            p2_x_2 = p2_x[p2_x > bu_roll_num]
                            p2_y_2 = p2_y[p2_x > bu_roll_num]
                            ratio_1 = cal_overlap_ratio(p2_x_1, p2_y_1, p1_x, p1_y)
                            ratio_2 = cal_overlap_ratio(p2_x_2, p2_y_2, p1_x, p1_y)
                            return ratio_1 + ratio_2
                        else:
                            return cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y)
                    else:
                        return cal_overlap_ratio(p1_x, p1_y, p2_x, p2_y)

                df['AR_detection'] = np.zeros(df.shape[0])

                time_unique = df['time'].unique()

                diff_unique = df['diff'][df['num'] == 1]

                ############### **************************************
                result = np.empty([int(len(df['num'].unique())), len(time_unique)], dtype='int') * np.nan

                for s_i in range(len(time_unique))[:-1]:
                    # print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
                    # print('s_i', time_unique[s_i])
                    polygon_list_Now = df[['x', 'y']][df['time'] == time_unique[s_i]].values
                    polygon_list_Next = df[['x', 'y']][df['time'] == time_unique[s_i + 1]].values
                    num_list_now = df['num'][df['time'] == time_unique[s_i]].values.astype('int')
                    num_list_Next = df['num'][df['time'] == time_unique[s_i + 1]].values.astype('int')

                    s_List = []
                    for sI_xy_now, s_xy_now in enumerate(polygon_list_Now):
                        for sI_xy_next, s_xy_next in enumerate(polygon_list_Next):

                            ######## pass error with xy
                            if '...' in s_xy_now[0] + s_xy_now[1] + s_xy_next[0] + s_xy_next[1]:
                                # print(time_unique[0], 'wrong')
                                return df['AR_detection'].values

                            s_index = create_polygon(s_xy_now[0], s_xy_now[1], s_xy_next[0], s_xy_next[1],
                                                     [num_list_now[sI_xy_now], num_list_Next[sI_xy_next]])

                            ''' ##########################   calculation #############################'''
                            if s_index > 0.1:
                                loc = 0
                                for s_result_nan in range(result.shape[0]):
                                    if result[s_result_nan, s_i] == (num_list_now[sI_xy_now]):
                                        # print('1')
                                        result[s_result_nan, s_i + 1] = num_list_Next[sI_xy_next]
                                        loc = 1
                                        break

                                if loc == 0:
                                    for s_result_nan in range(result.shape[0]):
                                        # # print('???', result[s_result_nan, s_i])
                                        if np.nansum(result[s_result_nan]) != 0:
                                            # # print('are you here?')
                                            # if too much nan contains ,skip.

                                            new_iloc_xy = np.argwhere([~np.isnan(result[s_result_nan])])[-1][-1]
                                            # # print(int(result[s_result_nan, new_iloc_xy]), 'new')
                                            new_iloc_xy_values = df[['x', 'y']][
                                                (df['time'] == time_unique[new_iloc_xy]) & (df['num'] == int(
                                                    result[s_result_nan, new_iloc_xy]))].values[0]
                                            new_iloc_num = df['num'][(df['time'] == time_unique[new_iloc_xy]) & (
                                                    df['num'] == int(result[s_result_nan, new_iloc_xy]))]

                                            s_diff = diff_unique.iloc[new_iloc_xy + 1:s_i + 1].values
                                            # # print('diff', diff_unique.iloc[new_iloc_xy+1:s_i])
                                            # # print('diff', diff_unique_test.iloc[new_iloc_xy+1:s_i])
                                            # # print('sum_diff', np.sum(s_diff))
                                            if np.sum(s_diff) > bu_lack_period_length:
                                                continue
                                            if '...' in s_xy_now[0] + s_xy_now[1] + new_iloc_xy_values[0] + \
                                                    new_iloc_xy_values[1]:
                                                # print(time_unique[0], 'wrong')
                                                return df['AR_detection'].values
                                            s_index = create_polygon(new_iloc_xy_values[0], new_iloc_xy_values[1],
                                                                     s_xy_now[0], s_xy_now[1], [new_iloc_num.values[0],
                                                                                                num_list_now[
                                                                                                    sI_xy_now]])

                                            if s_index > 0.1:
                                                # print('2',s_index)
                                                result[s_result_nan, s_i] = num_list_now[sI_xy_now]
                                                result[s_result_nan, s_i + 1] = num_list_Next[sI_xy_next]
                                                loc = 1
                                                break

                                if loc == 0:
                                    for s_result_nan in range(result.shape[0]):
                                        if np.nansum(result[s_result_nan]) == 0:
                                            # # print(result[s_result_nan])
                                            # print('3')
                                            result[s_result_nan, s_i] = num_list_now[sI_xy_now]
                                            result[s_result_nan, s_i + 1] = num_list_Next[sI_xy_next]
                                            loc = 1
                                            break

                                if loc == 0:
                                    result = np.append(result, [np.ones(result.shape[1]) * np.nan], axis=0)
                                    # print('4')

                                    result[-1, s_i] = num_list_now[sI_xy_now]
                                    result[-1, s_i + 1] = num_list_Next[sI_xy_next]
                                    loc = 1
                                    break

                ####### useless
                if bu_NorthPolar_protion != False:
                    for s_result_nan in range(result.shape[0]):
                        area_protion = []
                        if np.nansum(result[s_result_nan]) != 0:
                            s_result = result[s_result_nan]
                            for new_iloc_xy in range(len(s_result)):
                                if np.isnan(s_result[new_iloc_xy]) != 1:
                                    new_iloc_xy_values = df[['x', 'y']][(df['time'] == time_unique[new_iloc_xy]) & (
                                            df['num'] == int(result[s_result_nan, new_iloc_xy]))].values[0]
                                    s_index = create_polygon(new_iloc_xy_values[0], new_iloc_xy_values[1],
                                                             [146, 270, 270, 146], [174, 174, 74, 74], given_p2xy=True)
                                    area_protion.append(s_index)

                            area_protion = np.array(area_protion)
                            if len(area_protion[area_protion > 0.3]) < 2:
                                result[s_result_nan] = np.zeros(result[s_result_nan].shape)

                for s_result_nan in range(result.shape[0]):
                    s_result = result[s_result_nan]
                    new_iloc_xy = np.argwhere([~np.isnan(s_result.flatten())])

                    if len(new_iloc_xy) >= 2:

                        begin = new_iloc_xy[0][-1];
                        end = new_iloc_xy[-1][-1]
                        main_period_length = (time_unique[end] - time_unique[begin]) / np.timedelta64(6, 'h') + 1
                        if roll_1000:
                            if (len(s_result[s_result > 1000]) == 0):
                                continue

                        if (main_period_length >= bu_main_period_length):

                            min_lat = []
                            for iI_loc, iloc in enumerate(s_result):
                                if np.isnan(iloc):
                                    a = 0
                                else:
                                    time_loc = time_unique[iI_loc]
                                    df.loc[(df['time'] == time_loc) & (
                                            df['num'] == iloc), 'AR_detection'] = s_result_nan + 1

                        else:
                            result[s_result_nan] = np.zeros(result[s_result_nan].shape)

                return df['AR_detection'].values


            bu_NorthPolar_protion = False

            df_prepared = prepare_data(df_row.copy())
            a = df_prepared.groupby(df_prepared['index'].eq(0).cumsum()).apply(lambda x: detection(x)).reindex()
            a = a.values.flatten()

            ##########
            b = []
            for i in a:
                b.extend(i)
            b = np.array(b)

            df_prepared['AR'] = b
            df_prepared = df_prepared[['time', 'num', 'index', 'AR']]


            df_prepared.to_csv(path_Proj_Data + 'AR_timeSeries_%s%i%i.csv' % (season, bu_main_period_length, bu_lack_period_length), index=False)


        '''#################################  find all AR_detection ############################################'''

        def centroid(charater_AR, s_time):
            def direction(u, v):
                a = np.arctan2(v, u) * 180 / np.pi
                if a< 0:
                    a = 360+a
                a0 = 0
                if u > 0:
                    a0 = 180
                return 90-a+a0

            def load_ivt_total(s_time):
                '''
                :param s_time: datetime64
                :return: ivt_total
                '''
                s_time = pd.to_datetime(s_time)
                s_time_str = s_time.strftime('%Y%m%d')
                index_vivt = xr.open_dataset(path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time_str[:4], s_time_str))
                index_uivt = xr.open_dataset(path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time_str[:4], s_time_str))
                # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
                # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

                ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
                ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

                ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
                return ivt_total, ivt_east, ivt_north
            ivt_total, u, v = load_ivt_total(s_time)
            ivt_total[charater_AR==0] = np.nan
            s_area = area.copy()
            s_area[charater_AR==0] = np.nan
            long, lat = np.meshgrid(np.arange(0,360, resolution), IVT_lat)
            long = np.array(long,dtype='double')
            lat = np.array(lat,dtype='double')

            long_AR_index = np.argwhere(charater_AR != 0)[:,1]
            long_AR_index = sorted(long_AR_index)
            b = sorted(set(range(long_AR_index[0],long_AR_index[-1]+1))-set(long_AR_index))

            # if AR across longitude 0, and long_AR_index is not ascend
            if (np.isin(long_AR_index, [0]).any()) & (len(b)!=0):
                long_min = b[0]; long_max = b[-1]

                long_row = np.zeros(charater_AR.shape[1]-long_max+long_min+1)
                long_row[:charater_AR.shape[1]-long_max] = long[0, long_max:].copy()
                long_row[charater_AR.shape[1]-long_max:charater_AR.shape[1]-long_max+long_min+1] = long[0, :long_min+1].copy()

                long[:, long_max:] = np.arange(charater_AR.shape[1]-long_max)
                long[:, :long_min+1] = np.arange(charater_AR.shape[1]-long_max, charater_AR.shape[1]-long_max+long_min+1)
            else:
                long_min = np.min(long_AR_index); long_max = np.max(long_AR_index)
                long_row = long[0, long_min:long_max+1].copy()
                long[:, long_min:long_max+1] = np.arange(long_max-long_min+1)

            long[charater_AR==0] = np.nan
            lat[charater_AR==0] = np.nan

            protion = ivt_total*(s_area/np.nansum(s_area))
            centroid_long_int = np.nansum(long*(protion/np.nansum(protion)))

            centroid_long = long_row[int(centroid_long_int)]

            centroid_index = [np.argwhere(IVT_lat==int(np.nansum(lat*protion)/np.nansum(protion))).flatten(),
                              np.argwhere(np.arange(0,360, resolution)==int(centroid_long)).flatten()]
            centroid_direction = direction(u[centroid_index[0], centroid_index[1]], v[centroid_index[0], centroid_index[1]])
            return [np.nansum(lat*protion)/np.nansum(protion), centroid_long, centroid_direction]
        def character_mainaxis_orientation_width(simple):
            from fil_finder import FilFinder2D
            import astropy.units as u
            import numpy as np
            from skimage import measure
            long = np.arange(0,360, resolution)
            lat = IVT_lat
            # cross 0 index or not?
            roll = 0
            if (np.in1d(np.unique(np.argwhere(simple!=0)[:,1]), [0])).any():
                simple = np.roll(simple, int(180/resolution), axis=1)
                roll = 1
            ######## sketetons
            fil = FilFinder2D(simple, distance=1* u.pix, mask=simple)
            fil.preprocess_image(skip_flatten=True)
            fil.create_mask(border_masking=True, verbose=False,
                            use_existing_mask=True)
            fil.medskel(verbose=False)

            sslabels = measure.label(simple, connectivity=1, background=0)
            regions = measure.regionprops(sslabels)
            longth = regions[0].axis_major_length
            width = regions[0].axis_minor_length

            fil.analyze_skeletons(branch_thresh=8*u.pix, skel_thresh=1 * u.pix, prune_criteria='length')
            fil.exec_rht()
            orientation = np.degrees(fil.orientation)
            longpath = [fil.filaments[0].longpath_pixel_coords[0], fil.filaments[0].longpath_pixel_coords[1]]
            if roll:
                longpath = np.array([longpath[0], longpath[1]-(180/resolution)])
                longpath[1][longpath[1]<0] = longpath[1][longpath[1]<0]+(360/resolution)

            longpath[0] = np.array([lat[int(i)] for i in longpath[0]])
            longpath[1] = np.array([long[int(i)] for i in longpath[1]])
            return longpath, orientation[0].value, width*resolution, longth*resolution

        def save_standard_nc(data1, data2, times_1, times_2, shape,
                             centroid1, centroid2,
                             file_vari_name, lon=None, lat=None, path_save=""):
            import xarray as xr
            import numpy as np
            shape_longpath, shape_orientation, shape_width, shape_longth = shape
            if lon is None:
                lon = np.arange(0, 360, resolution)
            if lat is None:
                lat = np.arange(90, -90.5, -resolution)

            def get_dataarray(data, times, time_name):
                foo = xr.DataArray(data, coords=[times, lat, lon], dims=[time_name, "lat", 'lon'])
                return foo
            def get_centroidarray(data, times, time_name):
                foo = xr.DataArray(data, coords=[times, ['y', 'x', 'a']], dims=[time_name, 'yxa'])
                return foo

            longpath_shape = np.arange(len(shape_longpath[0][0]))
            ds = xr.Dataset(
                {
                    'contour_all': get_dataarray(data1, times_1, 'time_all'),
                    'contour_AR': get_dataarray(data2, times_2, 'time_AR'),
                    'centroid_all': get_centroidarray(centroid1, times_1, 'time_all'),
                    'centroid_AR': get_centroidarray(centroid2, times_2, 'time_AR'),
                    'longpath': xr.DataArray(np.array(shape_longpath), coords=[times_2, ['y', 'x'], longpath_shape], dims=['time_AR', 'yx', 'lp']),
                    'orientation': xr.DataArray(shape_orientation, coords=[times_2], dims=['time_AR']),
                    'width': xr.DataArray(shape_width, coords=[times_2], dims=['time_AR']),
                    'longth': xr.DataArray(shape_longth, coords=[times_2], dims=['time_AR']),

                }
            )
            print(ds)
            ds.to_netcdf(path_save + '%s.nc' % file_vari_name)


        def Overlap(row_data, given_threshold, exist_contour):
            contourMask = []
            num = 0

            data = row_data-given_threshold
            data = gaussian_filter(data, sigma=1)
            s_IVT_long = IVT_long
            area_Protion = []

            exist_contour = np.array(exist_contour, dtype='uint8')
            p_area_exist_contour = len(np.argwhere(exist_contour!=0).flatten())

            thresh = cv2.threshold(data, 1, 255, cv2.THRESH_BINARY)[1]
            labels = measure.label(thresh, connectivity=1, background=0)

            ################### get pair-contour ###############################
            labels_unique = np.unique(labels)[np.unique(labels)!=0]
            labels_Mask_num = []
            for label in labels_unique:
                s_label_index = np.argwhere(labels==label)
                s_lon = s_label_index[:,1]
                if np.isin(s_lon, [0]).any():
                    labels_Mask_num.append(2)
                elif np.isin(s_lon, [thresh.shape[1]-1]).any():
                    labels_Mask_num.append(3)
                else:
                    labels_Mask_num.append(1)
            labels_Mask_num = np.array(labels_Mask_num)
            for label_begin in np.argwhere(labels_Mask_num==2):
                s_lon_begin = np.argwhere(labels[:,0]==labels_unique[label_begin]).flatten()
                # print('%%')
                # print(s_lon_begin)
                for label_end in np.argwhere(labels_Mask_num==3):
                    s_lon_end = np.argwhere(labels[:,-1]==labels_unique[label_end]).flatten()
                    # print()
                    # print(s_lon_end)
                    if len(s_lon_end) == 0:
                        continue
                    protion1 = len(s_lon_begin[np.isin(s_lon_begin, s_lon_end)])/len(s_lon_begin)
                    protion2 = len(s_lon_end[np.isin(s_lon_end, s_lon_begin)])/len(s_lon_end)
                    protion = np.max([protion1, protion2])
                    # print(protion, 'protion')
                    if protion>0.3:
                        labels[labels==labels_unique[label_end]] = labels_unique[label_begin]

            ################### loop in every contour ###############################
            Area_Protion = []
            for label in np.unique(labels)[np.unique(labels)!=0]:

                #######  prepare labelMask, labelMask is a narray contains one contour
                labelMask = np.zeros(thresh.shape, dtype="uint8")
                labelMask[labels == label] = 1.0
                numPixels = cv2.countNonZero(labelMask)

                # plt.imshow(labelMask)
                # plt.show()

                if (numPixels > bu_the_number_of_grids):
                    ar = cv2.bitwise_and(labelMask, exist_contour)
                    if cv2.countNonZero(ar)!=0:
                        # p_area_labelMask = len(np.argwhere(labelMask!=0).flatten())
                        p_area = len(np.argwhere(ar!=0).flatten())
                        s_Area_Protion = p_area/p_area_exist_contour

                        if s_Area_Protion > bu_Area_Protion_for_connective_contour:
                            num += 1
                            Area_Protion.append(s_Area_Protion)
                            contourMask.append(labelMask)

            if num >0:
                s_selected = np.argmax(np.array(Area_Protion))
                # plt.imshow(np.array(contourMask[s_selected]))
                # plt.show()
                return np.array(contourMask[s_selected])
            else:
                return -999


        for season in time_Seaon_divide_Name:
            df = pd.read_csv(path_Proj_Data + 'AR_timeSeries_%s%i%i.csv' % (season, bu_main_period_length, bu_lack_period_length), index_col=0, parse_dates=[0])
            # df = pd.read_csv(path_ProjData + 'result_%s%i%i%s.csv' % (season, bu_main_period_length, bu_lack_period_length), index_col=0, parse_dates=[0])
            df['num_loc'] = np.ones(df.shape[0])
            df['num_loc'] = df['num_loc'].groupby(df.index).cumsum()

            time_be_all = []
            num_ARloc_all = []
            time_AR_all = []
            num_AR_name_all = []

            def cals_AR_num(df):
                num = len(np.argwhere(df['AR'].unique() != 0))
                for i in df['AR'].unique():
                    if i == 0:
                        continue
                    s_df = df[df['AR']==i]
                    time_be_all.append([s_df.index[0], s_df.index[-1]])
                    num_ARloc_all.append(s_df['num_loc'].values)
                    time_AR_all.append(s_df.index.values)
                    num_AR_name_all.append(i)
                return

            def load_ivt_total(s_time):
                '''
                :param s_time: datetime64
                :return: ivt_total
                '''
                s_time = pd.to_datetime(s_time)


                s_dual_threshold = dual_threshold[sI_Season]
                s_time_str = s_time.strftime('%Y%m%d')
                index_vivt = xr.open_dataset(path_IVT_Data + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (s_time_str[:4], s_time_str))
                index_uivt = xr.open_dataset(path_IVT_Data + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (s_time_str[:4], s_time_str))
                # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
                # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

                ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
                ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

                ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
                return ivt_total, s_dual_threshold
            def load_ivt_now(timeAR, num):
                '''
                load all AR_detection shape
                :param timeAR: list ,datetime64s, ARs exist time
                :param num: list, int, ARs num in accurate time
                :return: exist_contour_all
                '''
                exist_contour_all = []
                for sI_loc, s_time in enumerate(timeAR):
                    s_time = pd.to_datetime(s_time)
                    path_AR = path_Proj_Data + 'daily_AR/%s' % (s_time.strftime('%Y%m%d_%H'))
                    exist_contour = nc4.read_nc4(path_AR)[int(num[sI_loc] - 1)]
                    exist_contour_all.append(exist_contour)
                return exist_contour_all

            a = df.groupby(df['index'].eq(0).cumsum()).apply(lambda x: cals_AR_num(x))

            dual_threshold = nc4.read_nc4(path_Proj_Data + de_quantile_field_Name)
            area_S = scio.loadmat(path_Proj_Data + 'GridAL_ERA0_5degGridC.mat')['S']
            area= np.zeros([area_S.shape[0]+1, area_S.shape[1]])
            area[:-1, :] = area_S
            area[-1,:] = area_S[-1,:]
            for sI_time_all, s_time_be_all in enumerate(time_be_all):

                warnings = 0
                s_num_ARloc = num_ARloc_all[sI_time_all]
                s_time_AR_all = time_AR_all[sI_time_all]

                exist_contour_all = load_ivt_now(s_time_AR_all, s_num_ARloc)

                ######## fill
                s_time_loop = pd.date_range(s_time_be_all[0], s_time_be_all[1], freq='6h')
                s_timeAR_str = [pd.to_datetime(i).strftime('%Y%m%d%H') for i in s_time_AR_all]

                new_exist_contour_all = []
                dirs_path = path_Proj_Data + 'AR_contour_%i%i/' % (bu_main_period_length, bu_lack_period_length)

                if os.path.exists(dirs_path + '%s_%i.nc' % (s_timeAR_str[0], num_AR_name_all[sI_time_all])):
                    continue

                num_sI_time = 0
                for sI_time, s_time in enumerate(s_time_loop):
                    if np.isin(s_timeAR_str, pd.to_datetime(s_time).strftime('%Y%m%d%H')).any() == False:
                        s_exist_contour = new_exist_contour_all[sI_time-1]
                        ivt_total, thresh = load_ivt_total(s_time)
                        contour_new = Overlap(ivt_total, thresh, s_exist_contour)
                        if isinstance(contour_new, np.ndarray):
                            new_exist_contour_all.append(contour_new)
                        else:
                            print('warning: no AR_detection find between all AR_detection events', s_time)
                            # plt.imshow(s_exist_contour)
                            # plt.show()
                            # plt.imshow(np.ma.masked_less(ivt_total-thresh, 0))
                            # plt.show()
                            warnings = 1
                            break
                    else:
                        new_exist_contour_all.append(exist_contour_all[num_sI_time])
                        num_sI_time += 1


                if warnings == 1:
                    continue

                ####### backward

                back_bu = 0
                s_time_loop = pd.date_range(s_time_be_all[0] - np.timedelta64(10, 'D'), s_time_be_all[0], freq='6h')[:-1]
                if s_time_loop[0].strftime('%Y') == bu_Data_begin_year:
                    s_time_loop = pd.date_range('19790101 05:00', s_time_be_all[0], freq='6h')[:-1]

                back_contour_all = []

                for sI_time, s_time in enumerate(s_time_loop[::-1]):
                    if sI_time == 0:
                        s_exist_contour = exist_contour_all[0]
                    ivt_total, thresh = load_ivt_total(s_time)
                    # plt.imshow(ivt_total-thresh)
                    # plt.show()
                    contour_new = Overlap(ivt_total, thresh, s_exist_contour)

                    if isinstance(contour_new, np.ndarray):

                        back_contour_all.append(contour_new)
                        s_exist_contour = contour_new
                    else:
                        back_bu=1
                        back_time=s_time
                        break
                if back_bu != 1:
                    back_time=s_time- np.timedelta64(6, 'h')

                ####### upward
                up_bu = 0
                s_time_loop = pd.date_range(s_time_be_all[1], s_time_be_all[1] + np.timedelta64(10, 'D'), freq='6h')[1:]
                up_contour_all = []

                for sI_time, s_time in enumerate(s_time_loop):
                    if sI_time == 0:
                        s_exist_contour = exist_contour_all[-1]
                    ivt_total, thresh = load_ivt_total(s_time)
                    contour_new = Overlap(ivt_total, thresh, s_exist_contour)
                    if isinstance(contour_new, np.ndarray):
                        print(s_time)
                        up_contour_all.append(contour_new)
                        s_exist_contour = contour_new
                    else:
                        up_bu = 1
                        up_time=s_time
                        break

                if up_bu != 1:
                    up_time=s_time+ np.timedelta64(6, 'h')

                final_contour = back_contour_all[::-1]+new_exist_contour_all+up_contour_all
                final_time = pd.date_range(back_time, up_time, freq='6H')[1:-1]

                AR_time = pd.date_range(s_time_be_all[0], s_time_be_all[1], freq='6h')

                ######## """get centroid""" ############
                centroid_all = []
                for i_centroid in range(len(final_time)):
                    centroid_all.append(centroid(final_contour[i_centroid], final_time[i_centroid]))

                centroid_AR = []
                for i_centroid in range(len(AR_time)):
                    centroid_AR.append(centroid(new_exist_contour_all[i_centroid], AR_time[i_centroid]))

                shape_longpath_all= pd.DataFrame()
                shape_orientation = []
                shape_width = []
                shape_longth = []
                for i_centroid in range(len(AR_time)):
                    longpath, orientation, width, longth = character_mainaxis_orientation_width(new_exist_contour_all[i_centroid])
                    shape_longpath = pd.DataFrame()
                    shape_longpath[str(i_centroid)] = longpath[0]
                    shape_longpath[str(i_centroid)+str(i_centroid)] = longpath[1]
                    shape_longpath_all = pd.concat([shape_longpath_all, shape_longpath], ignore_index=True, axis=1)

                    shape_orientation.append(orientation)
                    shape_width.append(width)
                    shape_longth.append(longth)


                shape_longpath_all = shape_longpath_all.T.values
                shape_longpath = []
                for i_lp in range(0, shape_longpath_all.shape[0],2):
                    shape_longpath.append([shape_longpath_all[i_lp], shape_longpath_all[i_lp+1]])

                dirs_path = path_Proj_Data + 'AR_contour_%i%i/' % (bu_main_period_length, bu_lack_period_length)
                os.makedirs(dirs_path, exist_ok=True)
                save_standard_nc(final_contour, new_exist_contour_all,
                                 final_time, AR_time,
                                 [shape_longpath, shape_orientation, shape_width, shape_longth],
                                 np.array(centroid_all), np.array(centroid_AR),
                                         file_vari_name='%s_%i'%(s_timeAR_str[0], num_AR_name_all[sI_time_all]),
                                         path_save=dirs_path,)

