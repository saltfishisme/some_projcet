import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
from Setting_mpv import *


def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, level=[1000, 850, 500, 200], type='daily'):
    """date containing Hour information
    type: daily/monthly/monthly_now
    """
    import scipy
    time = pd.to_datetime(date)

    # load hourly anomaly data
    def load_anomaly(type):
        if type == 'daily':
            data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                               time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly':
            data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m'))[
                               time.strftime('%m')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly_now':

            def monthly_mean(var_dir, var_save, var_ShortName, time):
                path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir

                if os.path.exists(path_SaveData + '%s.mat' % time.strftime('%Y%m')):
                    return scio.loadmat(path_SaveData + '%s.mat' % time.strftime('%Y%m'))[time.strftime('%Y%m')]

                else:
                    date_montly_now = pd.date_range(time.strftime('%Y-%m-') + '01', freq='1D', periods=31)
                    dates = date_montly_now[date_montly_now.month == int(time.strftime('%m'))].strftime('%Y%m%d')
                    year = dates[0][:4]

                    def get_data(var_dir, i_Year, var_save, i_MonDay):
                        s_data = xr.open_dataset(
                            path_PressureData + '%s/%s/%s.%s%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
                        s_data = s_data.sel(level=level)
                        s_data = s_data[var_ShortName].values
                        return np.nanmean(s_data, axis=0)

                    data_in_MonDay = []

                    for iI_MonDay, i_MonDay in enumerate(dates):
                        # work for 0229
                        s_data = get_data(var_dir, year, var_save, i_MonDay[4:])
                        data_in_MonDay.append(s_data)

                    data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
                    data_in_MonDay = np.array(data_in_MonDay, dtype='double')

                    path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir
                    os.makedirs(path_SaveData, exist_ok=True)
                    scipy.io.savemat(path_SaveData + '%s.mat' % time.strftime('%Y%m'),
                                     {time.strftime('%Y%m'): data_in_MonDay})

                    return data_in_MonDay

            data_anomaly = monthly_mean('Hgt', 'Hgt', 'z', time)

        return data_anomaly

    data_anomaly = load_anomaly(type)

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

    return [data_present, data_anomaly]


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly


def get_data_output_array(time, var, level_isobaric):
    '''
    :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
    :param level_isobaric:
    :return:
    '''

    def get_svar_data(var):
        data = xr.open_dataset(
            path_PressureData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
        return data

    result = []
    if np.isin(var, 'Hgt').any():
        hght_var = get_svar_data('Hgt').sel(time=time, level=level_isobaric)['z']
        result.append(hght_var)
    if np.isin(var, 'Tem').any():
        temp_var = get_svar_data('Tem').sel(time=time, level=level_isobaric)['t']
        result.append(temp_var)
    if np.isin(var, 'Uwnd').any():
        u_wind_var = get_svar_data('Uwnd').sel(time=time, level=level_isobaric)['u']
        result.append(u_wind_var)
    if np.isin(var, 'Vwnd').any():
        v_wind_var = get_svar_data('Vwnd').sel(time=time, level=level_isobaric)['v']
        result.append(v_wind_var)
    if np.isin(var, 'Wwnd').any():
        w_wind_var = get_svar_data('Wwnd').sel(time=time, level=level_isobaric)['w']
        result.append(w_wind_var)

    lat_var = result[0]['latitude']
    lon_var = result[0]['longitude']
    result.append(lat_var)
    result.append(lon_var)
    return result


def get_data_output_dataarray(time, var, level_isobaric, keep_all_time=False, keep_all_level=False):
    '''
    :param var:  ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
    :param level_isobaric:
    :return:
    '''

    def get_svar_data(var):
        data = xr.open_dataset(
            path_PressureData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
        return data

    data_row = get_svar_data(var[0])
    for i in var[1:]:
        data_row = xr.merge([data_row, get_svar_data(i)])

    if not keep_all_time:
        data_row = data_row.sel(time=time)
    if not keep_all_level:
        data_row = data_row.sel(level=level_isobaric)
    return data_row


def cal_mpv(time, keep_all_time=False, keep_all_level=False):
    import metpy.calc as mpcalc

    data = get_data_output_dataarray(time, ['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd'], level_isobaric=[1000, 850, 500, 200],
                                     keep_all_time=keep_all_time, keep_all_level=keep_all_level)

    ##############################
    # Get the cross section, and convert lat/lon to supplementary coordinates:

    cross = data.squeeze().set_coords(('latitude', 'longitude'))

    ##############################
    # For this example, we will be plotting potential temperature, relative humidity, and
    # tangential/normal winds. And so, we need to calculate those, and add them to the dataset:
    cross['dewpoint'] = mpcalc.dewpoint_from_relative_humidity(cross['t'], cross['r'])

    cross['e_Potential_temperature'] = mpcalc.equivalent_potential_temperature(
        cross['level'],
        cross['t'], cross['dewpoint']
    )
    cross['mpv'] = mpcalc.potential_vorticity_baroclinic(cross['e_Potential_temperature'], cross['level'],
                                                         cross['u'], cross['v'])
    return cross


def cal_gradient(time, var_with_shortname, keep_all_time=False, keep_all_level=False):
    """
    :param time: pd.datetime
    :param var_with_shortname: dict, {'Hgt':'z'}
    :param keep_all_time:
    :param keep_all_level:
    :return:
    """
    import metpy.calc as mpcalc

    data = get_data_output_dataarray(time, list(var_with_shortname.keys()), level_isobaric=[1000, 850, 500, 200],
                                     keep_all_time=keep_all_time, keep_all_level=keep_all_level)

    ##############################
    # Get the cross section, and convert lat/lon to supplementary coordinates:

    cross = data.squeeze().set_coords(('latitude', 'longitude'))

    ##############################
    # For this example, we will be plotting potential temperature, relative humidity, and
    # tangential/normal winds. And so, we need to calculate those, and add them to the dataset:

    gradient_dx = []
    gradient_dy = []

    for shortname in var_with_shortname.values:
        dthetadx, dthetady = mpcalc.geospatial_gradient(cross[shortname])
        gradient_dx.append(dthetadx)
        gradient_dy.append(dthetady)
    return gradient_dx, gradient_dy


def load_mpv(s_time, var, path_Qdata, types='climate'):
    """
    :param s_time: datetime64
    :param var:
    :param type: climate or now
    :param path_Qdata: /home/work/AR_detection/  will be /AR_detection/mpv_cli/1979/1979051005.nc
    :return:
    """
    import datetime
    s_time = pd.to_datetime(s_time)
    s_time_str = s_time.strftime('%Y%m%d%H')

    def create_save_adv_adi(time):
        import metpy.calc as mpcalc
        from metpy.units import units
        import numpy as np
        import xarray as xr
        import os

        mpv = cal_mpv(time, keep_all_level=True)

        s_year = int(time.strftime('%Y'))
        path_SaveData = path_Qdata
        os.makedirs(path_SaveData + '%s_cli/%i' % (var, s_year), exist_ok=True)
        try:
            mpv.reset_coords('metpy_crs', drop=True)
        except:
            aaa = 0
        mpv.to_netcdf(path_SaveData + '%s_cli/%i/%s.nc' % (var, s_year, time.strftime('%Y%m%d%H')))

    def load_Q_withpath(s_time_path):
        now = xr.open_dataset(path_Qdata + '%s.nc' % (s_time_path))[var].values
        if var == 'adibatic':
            now = now[0]
        return now

    if types == 'climate':
        cli = []
        for i_year in range(1979, 2020 + 1):
            # judge whether this date is valid
            try:
                datetime.datetime(i_year, int(s_time.strftime('%m')), int(s_time.strftime('%d')))
            except:
                print('%s is invalid' % (str(i_year) + s_time.strftime('%m%d%H')))
                continue

            # s_time_str: mpv_cli/1979/1979052017.nc
            s_time_str = var + '_cli/' + str(i_year) + '/' + str(i_year) + s_time.strftime('%m%d%H')

            if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
                a = True
            else:
                create_save_adv_adi(pd.to_datetime(str(i_year) + '-' + s_time.strftime('%m-%d %H:00:00')))

            cli.append(load_Q_withpath(s_time_str))
        cli = np.nanmean(np.array(cli), axis=0)
        now = load_Q_withpath(var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H'))
        return now - cli

    elif types == 'now':
        s_time_str = var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H')
        if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
            a = True
        else:
            create_save_adv_adi(s_time)
        now = load_Q_withpath(s_time_str)
        return now


def select_rectangle_area(data, centroid, long, lat, r_lat=20, r_lon=30):
    """
    :param data: [hgt, u, v, ...], var is 2d.
    :param centroid: select a rectangle based on centroid and r_lat, r_lon
    :param long:
    :param lat:
    :param r_lat:
    :param r_lon:
    :return:
    """

    def roll_longitude_from_359to_negative_180(data, long):
        roll_length = np.argwhere(long < 180).shape[0] - 1
        long = np.roll(long, roll_length)
        data = np.roll(data, roll_length)
        long = xr.where(
            long > 180,
            long - 360,
            long)
        return data, long

    lon_index = np.argwhere(long == centroid[1])[0]
    if (lon_index - r_lon < 0) | (lon_index + r_lon > long.shape[0]):
        data, long = roll_longitude_from_359to_negative_180(data, long)

        centroid[1] = (centroid[1] + 180) % 360 - 180

    long_2d, lat_2d = np.meshgrid(long, lat)
    lat_index = np.argwhere(lat == centroid[0])[0]
    lon_index = np.argwhere(long == centroid[1])[0]
    result = []
    for i_data in data:
        s_data = i_data[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])), :]
        lat_2d_flatten = lat_2d[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])),
                         0]

        s_data = s_data[:, int(lon_index - r_lon):int(lon_index + r_lon)]
        long_2d_flatten = long_2d[0, int(lon_index - r_lon):int(lon_index + r_lon)]

        result.append(s_data)
    return result, [lat_2d_flatten, long_2d_flatten]


def roll_longitude_from_359to_negative_180(long):
    return xr.where(long > 180,
                    long - 360,
                    long)
