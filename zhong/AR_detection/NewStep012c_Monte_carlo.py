import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import random

"""
=============================

=============================
"""
def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_MainData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_detection/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_detection/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\\'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name_num = ['0','2','3','4','5','6']
type_name = [['greenland_east', 'greenland_west'],['pacific_east', 'pacific_west'],
             ['MS', 'GE']]
plot_name = ['Greenland E', 'Greenland W','Pacific E', 'Pacific W',
             'Mediterranean', 'GE']
infig_fontsize = 5

resolution=0.5

plot_param = {'t2m':[['T2Mmean=',' K'],'T2Mano',[-5,25], 1],
              'sshf':[['SSHFmean=',' J/(m**2)'],'sensible heat anomaly',[-2,6], 1/100000],
              'slhf':[['SLHFmea=',' J/(m**2)'], 'latent heat anomaly',[-2,6],1/100000],
              'advection':[['ADVmean=',' K/6h'], 'advection',[-10,10], 6*3600],
              'adibatic':[['ABmean=',' K/6h'],'adiabatic',[-5,5],6*3600],
              'IVT':[['IVTmean=', ' kg/(m*s)'],'IVT',[0,700],1],
              'tcwv':[['TCWVmean=', ' kg/(m**2)'],'TCWVano',[0,20],1,],
                'ssr':[['TCWVmean=', ' kg/(m**2)'],'TCWVano',[0,20],1/100000]
              }

def pdf(data, bins=200, size=500):
    dist = ['norm', 'gamma',
            'pearson3', 'uniform']
    # Create models from data
    def best_fit_distribution(data, ax=None):
        import scipy.stats as st
        import statsmodels.api as sm
        from scipy.stats._continuous_distns import _distn_names
        import warnings
        """Model data by finding best fit distribution to data"""
        # Get histogram of original data
        y, x = np.histogram(data, bins=bins, density=True)
        x = (x + np.roll(x, -1))[:-1] / 2.0

        # Best holders
        best_distributions = []

        # Estimate distribution parameters from data
        for ii, distribution in enumerate([d for d in dist if not d in ['levy_stable', 'studentized_range']]):

            print("{:>3} / {:<3}: {}".format(ii + 1, len(_distn_names), distribution))

            distribution = getattr(st, distribution)

            # Try to fit the distribution
            try:
                # Ignore warnings from data that can't be fit
                with warnings.catch_warnings():
                    warnings.filterwarnings('ignore')

                    # fit dist to data
                    params = distribution.fit(data)

                    # Separate parts of parameters
                    arg = params[:-2]
                    loc = params[-2]
                    scale = params[-1]

                    # Calculate fitted PDF and error with fit in distribution
                    pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                    sse = np.sum(np.power(y - pdf, 2.0))

                    # if axis pass in add to plot
                    try:
                        if ax:
                            pd.Series(pdf, x).plot(ax=ax)

                    except Exception:
                        pass

                    # identify if this distribution is better
                    best_distributions.append((distribution, params, sse))

            except Exception:
                pass

        return sorted(best_distributions, key=lambda x: x[2])

    def make_pdf(dist, params):
        """Generate distributions's Probability Distribution Function """

        # Separate parts of parameters
        arg = params[:-2]
        loc = params[-2]
        scale = params[-1]

        # Get sane start and end points of distribution
        start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
        end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

        # Build PDF and turn into pandas Series
        x = np.linspace(start, end, size)
        y = dist.pdf(x, loc=loc, scale=scale, *arg)
        return np.array([x, y])

    # Load data from statsmodels datasets
    data = pd.Series(data)

    # Find best fit distribution
    best_distibutions = best_fit_distribution(data)
    best_dist = best_distibutions[0]
    # Make PDF with best params
    pdf = make_pdf(best_dist[0], best_dist[1])
    return pdf


vars = ['t2m','t2m']
# vars = ['sshf','slhf']
# vars = ['advection','ssr']
for i_phase in ['0', '1', '2']:
    for type_name_compare in type_name:

        def get_data():
            data_mean = []
            for type_SOM in type_name_compare:
                s_data_mean = []
                # for var in ['IVT', 'tcwv']:
                #     s_data_mean.append(nc4.read_nc4(path_SecondData + '%s0_%s_%s' % (var, season, type_SOM)))

                # for var in ['advection','adibatic']:
                #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))*6*3600)
                # for var in ['sshf','slhf']:
                #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))/100000)
                for var in vars:
                    s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM)))

                data_mean.append(s_data_mean)

            return np.array(data_mean)


        frequency = get_data()
        # [type_Compare, var]



        def monte_carlo_in_PDF(data1, data2, test_range_num=20):
            # divided into space range and test
            test_range = np.linspace(np.nanmin(np.append(data1, data2)), np.nanmax(np.append(data1, data2)), test_range_num)
            def monte_carlo(data1, data2):
                data = np.append(data1, data2)
                div = []
                for i in range(1000):
                    s_data1 = random.sample(set(data), len(data1))
                    s_data2 = np.setdiff1d(data, s_data1)
                    div.append(np.nanmean(s_data1)-np.nanmean(s_data2))

                return div, np.nanmean(data1)-np.nanmean(data2)

            for i in range(len(test_range))[:-1]:
                print(test_range[i], test_range[i+1])
                div_all, div = monte_carlo(data1[(data1 >= test_range[i]) & (data1 < test_range[i+1])],
                                           data2[(data2 >= test_range[i]) & (data2 < test_range[i+1])])
                p = np.percentile(div_all, [99,95,90])

                print(p, div)


        print(monte_carlo_in_PDF(frequency[0,0], frequency[1,0]))
        exit(0)


















