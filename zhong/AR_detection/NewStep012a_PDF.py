import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================

=============================
"""



def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_MainData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_detection/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_detection/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


# def main(var):
#
#     def contour_cal(time_file, var='IVT'):
#
#         # mat file always has some bugs with str type. skip whitespace
#         time_file_new = ''
#         for i in time_file:
#             time_file_new += i
#             if i == 'c':
#                 break
#
#         info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))
#
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#
#         if var == 't2m':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
#                                                          '2m_temperature',
#                                                          't2m')
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_i[contour==1].flatten())
#
#
#         elif var == 'IVT':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_i = load_ivt_total(pd.to_datetime(time_AR[iI_time]))
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_i[contour==1].flatten())
#
#         elif var == 'tcwv':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
#                                                          'WaterVapor',
#                                                          'tcwv', var_dir='Total_column_water_vapour')
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_i[contour==1].flatten())
#
#         elif var == 'lon':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_lon, s_lat = np.meshgrid(info_AR_row.lon.values, info_AR_row.lat.values)
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_lon[contour==1].flatten())
#
#         elif var == 'lat':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_lon, s_lat = np.meshgrid(info_AR_row.lon.values, info_AR_row.lat.values)
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_lat[contour==1].flatten())
#
#         elif (var == 'advection') | (var == 'adibatic'):
#             def cal_3phase(contour, phase_num=3):
#                 time_len = np.arange(len(contour))
#                 division = [contour[0]]
#                 for i in np.arange(1, phase_num - 1):
#                     percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
#                     p_index = int(percentile)
#                     p_weighted = percentile % 1
#                     division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
#                 division.append(contour[-1])
#                 # division = np.array_split(contour, 3)
#                 # division_mean = []
#                 # for i in division:
#                 #     division_mean.append(np.nanmean(i, axis=0))
#                 return np.array(division)
#
#
#             var_all = []
#             for iI_time in range(len(time_AR)):
#                 s_i = load_Q(pd.to_datetime(time_AR[iI_time]), var)
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 s_i[contour != 1] = np.nan
#                 var_all.append(s_i)
#             var_3phase = cal_3phase(var_all)
#
#         return avg
#
#     for season in ['DJF']:
#         df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)
#
#         for iI, i in enumerate(df.columns):
#             times_file_all = df[i].values
#             mean_AR = []
#
#             for time_file in times_file_all:
#                 if len(str(time_file)) < 5:
#                     continue
#                 s_mean = contour_cal(time_file, var)
#                 mean_AR.extend(s_mean)
#             mean_AR = np.array(mean_AR)
#             print(mean_AR)
#
#             nc4.save_nc4(mean_AR,
#                          path_SaveData + 'PDF/%s' % (var + '_' + season + '_' + type_name[iI]))

def main_3phase(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        if var == 'ssr':
            var_all = []
            for iI_time in range(len(time_all)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_all[iI_time]),
                                                        'surface_net_solar_radiation',
                                                        'ssr')
                contour = info_AR_row['contour_all'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'slhf':
            var_all = []
            for iI_time in range(len(time_all)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_all[iI_time]),
                                                        'surface_latent_heat_flux',
                                                        'slhf')
                contour = info_AR_row['contour_all'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'sshf':
            var_all = []
            for iI_time in range(len(time_all)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_all[iI_time]),
                                                        'surface_sensible_heat_flux',
                                                        'sshf')
                contour = info_AR_row['contour_all'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'str':
            var_all = []
            for iI_time in range(len(time_all)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_all[iI_time]),
                                                        'surface_net_thermal_radiation',
                                                            'str')
                contour = info_AR_row['contour_all'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())


        elif var == 't2m':
            var_all = []
            for iI_time in range(len(time_all)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_all[iI_time]),
                                                        '2m_temperature',
                                                        't2m')
                contour = info_AR_row['contour_all'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'sic':
            var_all = []
            for iI_time in range(len(time_all)):
                s_i = get_circulation_data_based_on_date(pd.to_datetime(time_all[iI_time]),
                                                        'Sea_Ice_Cover',
                                                        'siconc')
                contour = info_AR_row['contour_all'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif (var == 'advection') | (var == 'adibatic'):

            var_all = []
            for iI_time in range(len(time_AR)):
                s_i = load_Q(pd.to_datetime(time_AR[iI_time]), var)
                contour = info_AR_row['contour_AR'].values[iI_time]
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())
        return avg

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            phase1 = []
            phase2 = []
            phase3 = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean = contour_cal(time_file, var)
                phase1.extend(s_mean[0])
                phase2.extend(s_mean[1])
                phase3.extend(s_mean[2])

            phase = [phase1, phase2, phase3]
            for iI_phase, i_phase in enumerate(phase):
                nc4.save_nc4(np.array(i_phase),
                             path_SaveData + 'PDF/all_%s' % (var + '%i_'%iI_phase + season + '_' + type_name[iI]))


# # main('area')
# # main('duration')
# # main('IVT')
# # main('t2m')
# # main('tcwv')
#
# # main('lat')
# # main('lon')
# # main_3phase('str')
# main_3phase('sic')
# # main_3phase('advection')
# # main_3phase('adibatic')
# # main_3phase('ssr')
# # main_3phase('slhf')
# # main_3phase('sshf')
#
# # main_3phase('t2m')
# exit(0)


path_SecondData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\\'

path_MainData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']
type_name_num = ['0','2','3','4','5','6']
type_name = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE', 'BAF','BSE', 'BSW',
             'MED', 'ARC']

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

infig_fontsize = 5

resolution=0.5

plot_param = {'t2m':[['T2Mmean=',' K'],'T2Mano',[-5,25], 1],
              'sshf':[['SSHFmean=',' J/(m**2)'],'sensible heat anomaly',[-2,6], 1/100000],
              'slhf':[['SLHFmea=',' J/(m**2)'], 'latent heat anomaly',[-2,6],1/100000],
              'advection':[['ADVmean=',' K/6h'], 'advection',[-10,10], 6*3600],
              'adibatic':[['ABmean=',' K/6h'],'adiabatic',[-5,5],6*3600],
              'IVT':[['IVTmean=', ' kg/(m*s)'],'IVT',[0,700],1],
              'tcwv':[['TCWVmean=', ' kg/(m**2)'],'TCWVano',[0,20],1,],
                'ssr':[['TCWVmean=', ' kg/(m**2)'],'TCWVano',[0,20],1/100000]
              }

SMALL_SIZE = 7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize


def pdf(data, bins=200, size=500):
    dist = ['norm', 'gamma',
            'pearson3', 'uniform']
    # Create models from data
    def best_fit_distribution(data, ax=None):
        import scipy.stats as st
        import statsmodels.api as sm
        from scipy.stats._continuous_distns import _distn_names
        import warnings
        """Model data by finding best fit distribution to data"""
        # Get histogram of original data
        y, x = np.histogram(data, bins=bins, density=True)
        x = (x + np.roll(x, -1))[:-1] / 2.0

        # Best holders
        best_distributions = []

        # Estimate distribution parameters from data
        for ii, distribution in enumerate([d for d in dist if not d in ['levy_stable', 'studentized_range']]):

            print("{:>3} / {:<3}: {}".format(ii + 1, len(_distn_names), distribution))

            distribution = getattr(st, distribution)

            # Try to fit the distribution
            try:
                # Ignore warnings from data that can't be fit
                with warnings.catch_warnings():
                    warnings.filterwarnings('ignore')

                    # fit dist to data
                    params = distribution.fit(data)

                    # Separate parts of parameters
                    arg = params[:-2]
                    loc = params[-2]
                    scale = params[-1]

                    # Calculate fitted PDF and error with fit in distribution
                    pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                    sse = np.sum(np.power(y - pdf, 2.0))

                    # if axis pass in add to plot
                    try:
                        if ax:
                            pd.Series(pdf, x).plot(ax=ax)

                    except Exception:
                        pass

                    # identify if this distribution is better
                    best_distributions.append((distribution, params, sse))

            except Exception:
                pass

        return sorted(best_distributions, key=lambda x: x[2])

    def make_pdf(dist, params):
        """Generate distributions's Probability Distribution Function """

        # Separate parts of parameters
        arg = params[:-2]
        loc = params[-2]
        scale = params[-1]

        # Get sane start and end points of distribution
        start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
        end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

        # Build PDF and turn into pandas Series
        x = np.linspace(start, end, size)
        y = dist.pdf(x, loc=loc, scale=scale, *arg)
        return np.array([x, y])

    # Load data from statsmodels datasets
    data = pd.Series(data)

    # Find best fit distribution
    best_distibutions = best_fit_distribution(data)
    best_dist = best_distibutions[0]
    # Make PDF with best params
    pdf = make_pdf(best_dist[0], best_dist[1])
    return pdf

for season in time_Seaon_divide_Name:

    def get_data():
        data_mean = []
        for s_type_name in type_name:
            s_data_mean = []
            for var in ['IVT', 'tcwv']:
                s_data_mean.append(nc4.read_nc4(path_SecondData + '%s_%s_%s' % (var, season, s_type_name)))

            # for var in ['advection','adibatic']:
            #     s_data_mean.append(nc4.read_nc4(path_SecondData + '%s0_%s_%s' % (var, season, type_SOM))*6*3600)

            data_mean.append(s_data_mean)

        return np.array(data_mean)


    frequency = get_data()

    ##########################################
    # import warnings
    # import numpy as np
    # import pandas as pd
    # import scipy.stats as st
    # import statsmodels.api as sm
    # from scipy.stats._continuous_distns import _distn_names
    # import matplotlib
    # import matplotlib.pyplot as plt
    #
    # matplotlib.rcParams['figure.figsize'] = (16.0, 12.0)
    # matplotlib.style.use('ggplot')
    #
    #
    # # Create models from data
    # def best_fit_distribution(data, bins=200, ax=None):
    #     dist = ['norm', 'gamma',
    #             'pearson3', 'uniform','beta', 'rayleigh', 'pareto']
    #     """Model data by finding best fit distribution to data"""
    #     # Get histogram of original data
    #     y, x = np.histogram(data, bins=bins, density=True)
    #     x = (x + np.roll(x, -1))[:-1] / 2.0
    #
    #     # Best holders
    #     best_distributions = []
    #
    #     # Estimate distribution parameters from data
    #     # for ii, distribution in enumerate(
    #     #         [d for d in _distn_names if not d in ['levy_stable', 'studentized_range']]):
    #     for ii, distribution in enumerate([d for d in dist if not d in ['levy_stable', 'studentized_range']]):
    #
    #         print("{:>3} / {:<3}: {}".format(ii + 1, len(_distn_names), distribution))
    #
    #         distribution = getattr(st, distribution)
    #
    #         # Try to fit the distribution
    #         try:
    #             # Ignore warnings from data that can't be fit
    #             with warnings.catch_warnings():
    #                 warnings.filterwarnings('ignore')
    #
    #                 # fit dist to data
    #                 params = distribution.fit(data)
    #
    #                 # Separate parts of parameters
    #                 arg = params[:-2]
    #                 loc = params[-2]
    #                 scale = params[-1]
    #
    #                 # Calculate fitted PDF and error with fit in distribution
    #                 pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
    #                 sse = np.sum(np.power(y - pdf, 2.0))
    #
    #                 # if axis pass in add to plot
    #                 try:
    #                     if ax:
    #                         pd.Series(pdf, x).plot(ax=ax)
    #                 except Exception:
    #                     pass
    #
    #                 # identify if this distribution is better
    #                 best_distributions.append((distribution, params, sse))
    #
    #         except Exception:
    #             pass
    #
    #     return sorted(best_distributions, key=lambda x: x[2])
    #
    #
    # def make_pdf(dist, params, size=10000):
    #     """Generate distributions's Probability Distribution Function """
    #
    #     # Separate parts of parameters
    #     arg = params[:-2]
    #     loc = params[-2]
    #     scale = params[-1]
    #
    #     # Get sane start and end points of distribution
    #     start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    #     end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)
    #
    #     # Build PDF and turn into pandas Series
    #     x = np.linspace(start, end, size)
    #     y = dist.pdf(x, loc=loc, scale=scale, *arg)
    #     pdf = pd.Series(y, x)
    #
    #     return pdf
    #
    #
    # # Load data from statsmodels datasets
    # data = pd.Series(frequency[1][0])
    #
    # # Plot for comparison
    # plt.figure(figsize=(12, 8))
    # ax = data.plot(kind='hist', bins=50, density=True, alpha=0.5,
    #                color=list(matplotlib.rcParams['axes.prop_cycle'])[1]['color'])
    #
    # # Save plot limits
    # dataYLim = ax.get_ylim()
    #
    # # Find best fit distribution
    # best_distibutions = best_fit_distribution(data, 200, ax)
    # best_dist = best_distibutions[0]
    #
    # # Update plots
    # ax.set_ylim(dataYLim)
    # ax.set_title(u'El Niño sea temp.\n All Fitted Distributions')
    # ax.set_xlabel(u'Temp (°C)')
    # ax.set_ylabel('Frequency')
    #
    # # Make PDF with best params
    # pdf = make_pdf(best_dist[0], best_dist[1])
    # print(type(pdf))
    # print(pdf)
    # # Display
    # plt.figure(figsize=(12, 8))
    # ax = pdf.plot(lw=2, label='PDF', legend=True)
    # data.plot(kind='hist', bins=50, density=True, alpha=0.5, label='Data', legend=True, ax=ax)
    #
    # param_names = (best_dist[0].shapes + ', loc, scale').split(', ') if best_dist[0].shapes else ['loc', 'scale']
    # param_str = ', '.join(['{}={:0.2f}'.format(k, v) for k, v in zip(param_names, best_dist[1])])
    # dist_str = '{}({})'.format(best_dist[0].name, param_str)
    #
    # ax.set_title(u'El Niño sea temp. with best fit distribution \n' + dist_str)
    # ax.set_xlabel(u'Temp. (°C)')
    # ax.set_ylabel('Frequency')
    # plt.show()
    # exit(0)


    # pdf_other_axes_1 = pdf(frequency[0][0], bins=100, size=10000)
    # print(pdf_other_axes_1)
    # # pdf_other_axes_2 = pdf(frequency[1][0], bins=100)
    # plt.plot(pdf_other_axes_1[0], pdf_other_axes_1[1])
    # plt.show()

    def plots(ax, data, bin_width, mean_label=['','']):
        percentile_label = ['Q1', 'Q2', 'Q3']
        percentile = np.percentile(data, [25,50,75])
        mean = np.nanmean(data)
        HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
        # ax.boxplot(data)

        _, _, bar_container = ax.hist(data,bins=bin_width,density=True, lw=1,
                                  ec="black", fc="gray",zorder=2)

        # yticks = ax.get_yticks()
        # ax.set_yticks(yticks)
        # ax.set_yticklabels((yticks*len(data)).astype('int'))

        ylim_max = ax.get_ylim()[1]
        trans = ax.get_xaxis_transform()
        for i_percentile in range(3):
            ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
            ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
                    transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
                    bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
            )
            ax.text(percentile[i_percentile], 1.01, percentile[i_percentile].astype('int'), transform=trans,
                    horizontalalignment='center', fontsize=infig_fontsize)

        ax.text(0.85, 0.6, mean_label[0]+str(mean)+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)

        return [ax.get_ylim()[1], ax.get_xlim()[1]]

    # mean_labels = [['IVTmean=', 'kg/(m*s)'], ['Tmean=','℃'], ['TCWVmean=','kg/(m**2)']]
    # x_labels = ['IVTano', 'Tano', 'TCWVano']
    mean_labels = [['IVTmean=\n', 'kg/(m*s)'], ['IWVmean=\n','kg/$m^{2}$']]

    fig, axs = plt.subplots(6, 2, figsize=[5.5, 5])
    plt.subplots_adjust(top=0.95,
bottom=0.04,
left=0.1,
right=0.98,
hspace=0.4,
wspace=0.2)



    def flatten(l):
        return [item for sublist in l for item in sublist]

    bin_col = [np.histogram(flatten(frequency[:,0]), bins=100)[1], np.histogram(flatten(frequency[:,1].flatten()), bins=100)[1]]

    for i in range(6):
        # lim = np.zeros([6,2])
        # for j in range(2):
        #     data = frequency[i][j]
        #     ax = axs[i][j]
        #
        #     lim[i] = plots(ax, data, bin_col[j],mean_label=mean_labels[j])
        #     ax.annotate(figlabelstr[2 * i + j], [0.01, 0.85], xycoords='axes fraction')
        #
        # axs[i,0].set_ylim(top=0.01)
        # axs[i,1].set_ylim(top=0.3)
        #
        # axs[i][0].set_xlim([0,600])
        # axs[i][1].set_xlim([0,15])
        #
        # axs[i][0].set_ylabel(plot_name[i] + '\nProbability')

        pdf_other_axes_1 = pdf(frequency[0][i], bins=100, size=1000)
        pdf_other_axes_2 = pdf(frequency[1][i], bins=100, size=1000)
        axs[i][1].plot(pdf_other_axes_2[0], pdf_other_axes_2[1], color='black',linewidth=1)
        axs[i][1].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='black',linestyle='--',linewidth=1.5)
        axs[i][0].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='black',linewidth=1)
        plt.show()

    # add title

x_labels = ['IVT', 'IWVano']
for i_col in range(2):
    axs[0][i_col].set_title(x_labels[i_col], pad=10)

# ax.set_xlabel(x_labels[j])
# plt.show()
plt.savefig('figure4.png', dpi=400)
plt.close()


exit(0)

vars = ['t2m','advection']
vars = ['IVT','tcwv']
# vars = ['sshf','slhf']
# vars = ['advection','ssr']
for i_phase in ['0', '1', '2']:
    for s_type_name in type_name:

        def get_data():
            data_mean = []
            for type_SOM in s_type_name:
                s_data_mean = []
                # for var in ['IVT', 'tcwv']:
                #     s_data_mean.append(nc4.read_nc4(path_SecondData + '%s0_%s_%s' % (var, season, type_SOM)))

                # for var in ['advection','adibatic']:
                #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))*6*3600)
                # for var in ['sshf','slhf']:
                #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))/100000)
                for var in vars:
                    s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM)))

                data_mean.append(s_data_mean)

            return np.array(data_mean)


        frequency = get_data()


        ##########################################
        # import warnings
        # import numpy as np
        # import pandas as pd
        # import scipy.stats as st
        # import statsmodels.api as sm
        # from scipy.stats._continuous_distns import _distn_names
        # import matplotlib
        # import matplotlib.pyplot as plt
        #
        # matplotlib.rcParams['figure.figsize'] = (16.0, 12.0)
        # matplotlib.style.use('ggplot')
        #
        #
        # # Create models from data
        # def best_fit_distribution(data, bins=200, ax=None):
        #     dist = ['norm', 'gamma',
        #             'pearson3', 'uniform','beta', 'rayleigh', 'pareto']
        #     """Model data by finding best fit distribution to data"""
        #     # Get histogram of original data
        #     y, x = np.histogram(data, bins=bins, density=True)
        #     x = (x + np.roll(x, -1))[:-1] / 2.0
        #
        #     # Best holders
        #     best_distributions = []
        #
        #     # Estimate distribution parameters from data
        #     # for ii, distribution in enumerate(
        #     #         [d for d in _distn_names if not d in ['levy_stable', 'studentized_range']]):
        #     for ii, distribution in enumerate([d for d in dist if not d in ['levy_stable', 'studentized_range']]):
        #
        #         print("{:>3} / {:<3}: {}".format(ii + 1, len(_distn_names), distribution))
        #
        #         distribution = getattr(st, distribution)
        #
        #         # Try to fit the distribution
        #         try:
        #             # Ignore warnings from data that can't be fit
        #             with warnings.catch_warnings():
        #                 warnings.filterwarnings('ignore')
        #
        #                 # fit dist to data
        #                 params = distribution.fit(data)
        #
        #                 # Separate parts of parameters
        #                 arg = params[:-2]
        #                 loc = params[-2]
        #                 scale = params[-1]
        #
        #                 # Calculate fitted PDF and error with fit in distribution
        #                 pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
        #                 sse = np.sum(np.power(y - pdf, 2.0))
        #
        #                 # if axis pass in add to plot
        #                 try:
        #                     if ax:
        #                         pd.Series(pdf, x).plot(ax=ax)
        #                 except Exception:
        #                     pass
        #
        #                 # identify if this distribution is better
        #                 best_distributions.append((distribution, params, sse))
        #
        #         except Exception:
        #             pass
        #
        #     return sorted(best_distributions, key=lambda x: x[2])
        #
        #
        # def make_pdf(dist, params, size=10000):
        #     """Generate distributions's Probability Distribution Function """
        #
        #     # Separate parts of parameters
        #     arg = params[:-2]
        #     loc = params[-2]
        #     scale = params[-1]
        #
        #     # Get sane start and end points of distribution
        #     start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
        #     end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)
        #
        #     # Build PDF and turn into pandas Series
        #     x = np.linspace(start, end, size)
        #     y = dist.pdf(x, loc=loc, scale=scale, *arg)
        #     pdf = pd.Series(y, x)
        #
        #     return pdf
        #
        #
        # # Load data from statsmodels datasets
        # data = pd.Series(frequency[1][0])
        #
        # # Plot for comparison
        # plt.figure(figsize=(12, 8))
        # ax = data.plot(kind='hist', bins=50, density=True, alpha=0.5,
        #                color=list(matplotlib.rcParams['axes.prop_cycle'])[1]['color'])
        #
        # # Save plot limits
        # dataYLim = ax.get_ylim()
        #
        # # Find best fit distribution
        # best_distibutions = best_fit_distribution(data, 200, ax)
        # best_dist = best_distibutions[0]
        #
        # # Update plots
        # ax.set_ylim(dataYLim)
        # ax.set_title(u'El Niño sea temp.\n All Fitted Distributions')
        # ax.set_xlabel(u'Temp (°C)')
        # ax.set_ylabel('Frequency')
        #
        # # Make PDF with best params
        # pdf = make_pdf(best_dist[0], best_dist[1])
        # print(type(pdf))
        # print(pdf)
        # # Display
        # plt.figure(figsize=(12, 8))
        # ax = pdf.plot(lw=2, label='PDF', legend=True)
        # data.plot(kind='hist', bins=50, density=True, alpha=0.5, label='Data', legend=True, ax=ax)
        #
        # param_names = (best_dist[0].shapes + ', loc, scale').split(', ') if best_dist[0].shapes else ['loc', 'scale']
        # param_str = ', '.join(['{}={:0.2f}'.format(k, v) for k, v in zip(param_names, best_dist[1])])
        # dist_str = '{}({})'.format(best_dist[0].name, param_str)
        #
        # ax.set_title(u'El Niño sea temp. with best fit distribution \n' + dist_str)
        # ax.set_xlabel(u'Temp. (°C)')
        # ax.set_ylabel('Frequency')
        # plt.show()
        # exit(0)


        # pdf_other_axes_1 = pdf(frequency[0][0], bins=100, size=10000)
        # print(pdf_other_axes_1)
        # # pdf_other_axes_2 = pdf(frequency[1][0], bins=100)
        # plt.plot(pdf_other_axes_1[0], pdf_other_axes_1[1])
        # plt.show()


        SMALL_SIZE = 7

        plt.rc('axes', titlesize=SMALL_SIZE)
        plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
        plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
        plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
        plt.rc('axes', titlepad=1, labelpad=1)
        plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
        plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
        plt.rc('xtick.major', size=2, width=0.5)
        plt.rc('xtick.minor', size=1.5, width=0.2)
        plt.rc('ytick.major', size=2, width=0.5)
        plt.rc('ytick.minor', size=1.5, width=0.2)
        plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
        plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize


        fig, axs = plt.subplots(frequency.shape[1],2, figsize=[5,4])
        plt.subplots_adjust(top=0.9,
bottom=0.07,
left=0.09,
right=0.98,
hspace=0.23,
wspace=0.2)

        def plots(ax, data, mean_label=['','']):
            percentile_label = ['Q1', 'Q2', 'Q3']
            percentile = np.percentile(data, [25,50,75])
            mean = np.nanmean(data)
            HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
            # ax.boxplot(data)

            _, _, bar_container = ax.hist(data,bins=HIST_BINS,density=True, lw=1,
                                      ec="black", fc="gray",zorder=2)

            # yticks = ax.get_yticks()
            # ax.set_yticks(yticks)
            # ax.set_yticklabels((yticks*len(data)).astype('int'))

            ylim_max = ax.get_ylim()[1]
            trans = ax.get_xaxis_transform()
            for i_percentile in range(3):
                ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
                ax.text(percentile[i_percentile], 0.89, percentile_label[i_percentile],
                        transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
                        bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
                )
                ax.text(percentile[i_percentile], 1.01, percentile[i_percentile].astype('int'), transform=trans,
                        horizontalalignment='center', fontsize=infig_fontsize)

            ax.text(0.85, 0.89, mean_label[0]+'\n%0.2f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)

            return [ax.get_ylim()[1], ax.get_xlim()[1]]


        for i in range(frequency.shape[1]):
            lim = np.zeros([frequency.shape[1],2])
            for j in range(2):
                data = frequency[j][i]
                ax = axs[i][j]
                lim[j] = plots(ax, data*plot_param[vars[i]][-1], mean_label=plot_param[vars[i]][0])
                ax.set_xlabel(plot_param[vars[i]][1])

            maxy = np.max(lim[:,0])
            maxx = np.max(lim[:, 1])
            axs[i][0].set_ylim(top=maxy)
            axs[i][1].set_ylim(top=maxy)
            # axs[i][0].set_xlim(plot_param[vars[i]][2])
            # axs[i][1].set_xlim(plot_param[vars[i]][2])

            # axs[i][0].set_xlim([-20, 20])
            # axs[i][1].set_xlim([-20,20])

            # if i != 0:
            #     axs[i][0].set_xlim([0,20])
            #     axs[i][1].set_xlim([0,20])
            # else:
            #     axs[0][0].set_xlim([0,700])
            #     axs[0][1].set_xlim([0,700])

            # pdf_other_axes_1 = pdf(frequency[0][i], bins=100, size=1000)
            # pdf_other_axes_2 = pdf(frequency[1][i], bins=100, size=1000)
            # axs[i][1].plot(pdf_other_axes_2[0], pdf_other_axes_2[1], color='black',linewidth=1)
            # axs[i][1].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='black',linestyle='--',linewidth=1.5)
            # axs[i][0].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='black',linewidth=1)
            # plt.show()

        # add title

        for i_col in range(2):
            axs[0][i_col].set_title(s_type_name[i_col], pad=10)

        for i_row in range(frequency.shape[1]):
            axs[i_row][0].set_ylabel('Probability')
        plt.suptitle('phase %s'%i_phase)
        plt.show()
        save_path = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\pic\\'
        plt.savefig(save_path +'all_%s_pdf_%s_%s.png' % (vars[0], s_type_name[0], i_phase), dpi=400)
        plt.close()









