import numpy as np
import pandas as pd
import xarray as xr

import xarray as xr
import numpy as np
import pandas as pd

#
# da_row = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\t2m_climately.nc').sel(time=slice('1981-01', '2010-12'))
# # da_row = da_row.reduce(reduce_expver, 'expver')
#
# result = []
#
# time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]
#
# result = []
# for sI_Season, s_Season in enumerate(time_Seaon_divide):
#     da = da_row.sel(time=np.in1d(da_row['time.month'], s_Season))
#
#     da = da.mean(dim='time')['t2m'].values
#     result.append(da)
#
# import nc4
# nc4.save_nc4(np.array(result), r'D:\OneDrive\basis\some_projects\zhong\AR_detection\quantile_field_t2m')
# print(result)
# exit(0)
#
#
#
#
# def save_standard_nc(data, file_vari_name, times, lon=None, lat=None, path_save="", unified_vari_name=False):
#     import xarray as xr
#     import numpy as np
#
#     if lon is None:
#         lon = np.arange(0, 360)
#
#     def get_dataarray(data):
#         foo = xr.DataArray(data, coords=[times, lon], dims=['time', 'lon'])
#         return foo
#
#     name = file_vari_name
#     if unified_vari_name is not False:
#         name = 'vari'
#     ds = xr.Dataset(
#         {
#             name: get_dataarray(data),
#
#         }
#     )
#     print(ds)
#     ds.to_netcdf(path_save + '%s.nc' % file_vari_name)
#
# time_loop = pd.date_range('1981-01-01', '2010-12-31', freq='1D')
#
# path_MainData = r'/home/linhaozhong/work/Data/MoistureTrackData/ERA5/Vertical_integral_of_northward_water_vapour_flux'
#
# index_data = []
# for s_itime in time_loop.strftime('%Y%m%d'):
#     s_data = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%s_itime)
#     s_data = s_data.sel(latitude=70).mean(dim='time')['p72.162'].values
#     index_data.append(s_data)
#
# save_standard_nc(np.array(index_data), 'vivt', time_loop, path_save='/home/linhaozhong/work/Data/MoistureTrackData/ERA5/')

###################################  daily percentile #############################

import xarray as xr
import numpy as np
import pandas as pd
import nc4

# time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]
# path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
# for year_b, year_e in zip([1979,1989,1999,2009,2019],[1988,1998,2008,2018,2020]):
#     time_loop = pd.date_range('%i-01-01'%year_b, '%i-12-31'%year_e, freq='1D')
#
#     u_result = []
#     v_result = []
#
#     for s_time in time_loop:
#         s_time_str = s_time.strftime('%Y%m%d')
#         index_vivt = xr.open_dataset(
#             path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
#                 s_time_str[:4], s_time_str)).mean(dim='time')
#         index_uivt = xr.open_dataset(
#             path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
#                 s_time_str[:4], s_time_str)).mean(dim='time')
#
#         ivt_v = index_vivt['p72.162'].values
#         ivt_u = index_uivt['p71.162'].values
#
#         u_result.append(ivt_u)
#         v_result.append(ivt_v)
#     import nc4
#
#     nc4.save_nc4(np.array(v_result), path_MainData +
#                  'Vertical_integral_of_northward_water_vapour_flux/daily_%i-%i'%(year_b, year_e))
#     nc4.save_nc4(np.array(u_result), path_MainData +
#                  'Vertical_integral_of_eastward_water_vapour_flux/daily_%i-%i'%(year_b, year_e))

'''###################################  daily percentile #############################'''

import xarray as xr
import numpy as np
import pandas as pd
import nc4

percentile = 90
types = 'daily'
path_SingleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_MainData = '/home/linhaozhong/work/percentile/'
da_row_u = nc4.read_nc4(path_SingleData +
                 'Vertical_integral_of_eastward_water_vapour_flux/daily_%i-%i' % (1979, 2020))
da_row_v = nc4.read_nc4(path_SingleData +
                 'Vertical_integral_of_northward_water_vapour_flux/daily_%i-%i' % (1979, 2020))

result = []
time_loop = pd.date_range('%i-01-01'%1979, '%i-12-31'%2020, freq='1D')


time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]
time_Seaon_divide = [[5],[6],[7],[8],[9]]
for sI_Season, s_Season in enumerate(time_Seaon_divide):
    index = np.argwhere(np.isin(time_loop.month, s_Season)).flatten()

    u = da_row_u[index]
    v = da_row_v[index]

    s_data = np.sqrt(u**2+v**2)

    s_result = np.percentile(s_data.reshape(s_data.shape[0], s_data.shape[1]*s_data.shape[2]), percentile, axis=0)
    s_result = s_result.reshape(s_data.shape[1], s_data.shape[2])

    result.append(s_result)
import nc4
nc4.save_nc4(np.array(result), path_MainData+'%s_%i'%(types, percentile))
exit(0)

'''###################################  monthly percentile #############################'''
import xarray as xr
import numpy as np
import pandas as pd
import nc4
def reduce_expver(data, axis='dim'):
    result = np.nansum(data,axis=axis)
    result[np.all(np.isnan(data), axis=axis)] = np.nan
    return result
da_row = xr.open_dataset(r'/zhong/AR_detection\data\uv_monthly.nc').sel(time=slice('1979-01', '2020-12'))
da_row = da_row.reduce(reduce_expver, 'expver')

result = []

time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

for sI_Season, s_Season in enumerate(time_Seaon_divide):
    da = da_row.sel(time=np.in1d(da_row['time.month'], s_Season))

    u = da['p71.162'].values
    v = da['p72.162'].values

    s_data = np.sqrt(u**2+v**2)

    s_result = np.percentile(s_data.reshape(s_data.shape[0], s_data.shape[1]*s_data.shape[2]), 85, axis=0)
    s_result = s_result.reshape(s_data.shape[1], s_data.shape[2])

    result.append(s_result)
import nc4
nc4.save_nc4(np.array(result), r'D:\OneDrive\basis\some_projects\zhong\AR\quantile_field_85')
print(result)

