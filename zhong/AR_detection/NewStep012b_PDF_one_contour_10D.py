import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================

=============================
"""

import datetime


def load_Q(s_time, var, types='climate'):
    """
    :param s_time: datetime64
    :param var:
    :param type: climate or now
    :return:
    """

    def create_save_adv_adi(time):
        import metpy.calc as mpcalc
        from metpy.units import units
        import numpy as np
        import xarray as xr
        import os

        def get_data(time, var, level_isobaric):
            '''
            :param var: must follow ['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
            :param level_isobaric:
            :return:
            '''

            def get_svar_data(var):
                data = xr.open_dataset(
                    path_PressureData + '%s/%s/%s.%s.nc' % (var, time.strftime('%Y'), var, time.strftime('%Y%m%d')))
                return data

            result = []
            if np.isin(var, 'Hgt').any():
                hght_var = get_svar_data('Hgt').sel(time=time, level=level_isobaric)['z']
                result.append(hght_var)
            if np.isin(var, 'Tem').any():
                temp_var = get_svar_data('Tem').sel(time=time, level=level_isobaric)['t']
                result.append(temp_var)
            if np.isin(var, 'Uwnd').any():
                u_wind_var = get_svar_data('Uwnd').sel(time=time, level=level_isobaric)['u']
                result.append(u_wind_var)
            if np.isin(var, 'Vwnd').any():
                v_wind_var = get_svar_data('Vwnd').sel(time=time, level=level_isobaric)['v']
                result.append(v_wind_var)
            if np.isin(var, 'Wwnd').any():
                w_wind_var = get_svar_data('Wwnd').sel(time=time, level=level_isobaric)['w']
                result.append(w_wind_var)

            lat_var = result[0]['latitude']
            lon_var = result[0]['longitude']
            result.append(lat_var)
            result.append(lon_var)
            return result

        def tem_advection_1000(time):
            temp_var, u_wind_var, v_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Uwnd', 'Vwnd'],
                                                                          level_isobaric=1000)

            # between lat/lon grid points
            dx, dy = mpcalc.lat_lon_grid_deltas(lon_var, lat_var)

            # Calculate temperature advection using metpy function
            adv = mpcalc.advection(temp_var * units.kelvin, u=u_wind_var * units('m/s'), v=v_wind_var * units('m/s'),
                                   dx=dx, dy=dy)
            return adv

        def adiabatic(time, level_isobaric=[1000, 850, 500, 200]):
            temp_var, w_wind_var, lat_var, lon_var = get_data(time, ['Tem', 'Wwnd'], level_isobaric=level_isobaric)

            pressure_var = np.ones([len(level_isobaric), len(lat_var), len(lon_var)])
            for i in range(len(level_isobaric)):
                # transform hPa into Pa
                pressure_var[i] = pressure_var[i] * level_isobaric[i] * 100
            pressure_var = pressure_var * units.Pa

            # Calculate temperature advection using metpy function
            adv = mpcalc.static_stability(pressure_var, temp_var * units.kelvin)

            Rd = units.Quantity(8.314462618, 'J / mol / K') / units.Quantity(28.96546e-3, 'kg / mol')
            adiabatic_warming = adv * (w_wind_var * units('Pa/s')) * pressure_var / Rd
            return adiabatic_warming

        s_advection = tem_advection_1000(time)
        s_adiabatic = adiabatic(time)

        s_year = int(time.strftime('%Y'))
        path_SaveData = r'/home/linhaozhong/work/AR_analysis/'
        os.makedirs(path_SaveData + 'adibatic_cli/%i' % s_year, exist_ok=True)
        os.makedirs(path_SaveData + 'advection_cli/%i' % s_year, exist_ok=True)
        s_adiabatic.to_netcdf(path_SaveData + 'adibatic_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))
        s_advection.to_netcdf(path_SaveData + 'advection_cli/%i/%s.nc' % (s_year, time.strftime('%Y%m%d%H')))

    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q_withpath(s_time_path):
        now = xr.open_dataset(path_Qdata + '%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    if types == 'climate':
        cli = []
        for i_year in range(1979, 2020 + 1):
            # judge whether this date is valid
            try:
                datetime.datetime(i_year, int(s_time.strftime('%m')), int(s_time.strftime('%d')))
            except:
                print('%s is invalid' % (str(i_year) + s_time.strftime('%m%d%H')))
                continue

            s_time_str = var + '_cli/' + str(i_year) + '/' + str(i_year) + s_time.strftime('%m%d%H')
            if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
                a = True
            else:
                create_save_adv_adi(pd.to_datetime(str(i_year) + '-' + s_time.strftime('%m-%d %H:00:00')))
            cli.append(load_Q_withpath(s_time_str))
        cli = np.nanmean(np.array(cli), axis=0)
        now = load_Q_withpath(var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H'))
        return now - cli
    elif types == 'now':
        s_time_str = var + '_cli/' + s_time.strftime('%Y') + '/' + s_time.strftime('%Y%m%d%H')
        if os.path.exists(path_Qdata + '%s.nc' % (s_time_str)):
            a = True
        else:
            create_save_adv_adi(s_time)
        now = load_Q_withpath(s_time_str)
        return now


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north


def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir='', types='climate'):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    if types == 'climate':
        data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                           time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values
    if types == 'climate':
        return data_present - data_anomaly
    elif types == 'now':
        return data_present


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']

var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
               'hcc': 'high_cloud_cover',
               'lcc': 'low_cloud_cover',
            'mcc': 'medium_cloud_cover',
               'tcc': 'total_cloud_cover',
               'tciw': 'total_column_cloud_ice_water',
            'tclw': 'total_column_cloud_liquid_water',

}


# def main(var):
#
#     def contour_cal(time_file, var='IVT'):
#
#         # mat file always has some bugs with str type. skip whitespace
#         time_file_new = ''
#         for i in time_file:
#             time_file_new += i
#             if i == 'c':
#                 break
#
#         info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))
#
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#
#         if var == 't2m':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
#                                                          '2m_temperature',
#                                                          't2m')
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_i[contour==1].flatten())
#
#
#         elif var == 'IVT':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_i = load_ivt_total(pd.to_datetime(time_AR[iI_time]))
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_i[contour==1].flatten())
#
#         elif var == 'tcwv':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
#                                                          'WaterVapor',
#                                                          'tcwv', var_dir='Total_column_water_vapour')
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_i[contour==1].flatten())
#
#         elif var == 'lon':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_lon, s_lat = np.meshgrid(info_AR_row.lon.values, info_AR_row.lat.values)
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_lon[contour==1].flatten())
#
#         elif var == 'lat':
#             avg = []
#             for iI_time in range(len(time_AR)):
#                 s_lon, s_lat = np.meshgrid(info_AR_row.lon.values, info_AR_row.lat.values)
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 avg.extend(s_lat[contour==1].flatten())
#
#         elif (var == 'advection') | (var == 'adibatic'):
#             def cal_3phase(contour, phase_num=3):
#                 time_len = np.arange(len(contour))
#                 division = [contour[0]]
#                 for i in np.arange(1, phase_num - 1):
#                     percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
#                     p_index = int(percentile)
#                     p_weighted = percentile % 1
#                     division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
#                 division.append(contour[-1])
#                 # division = np.array_split(contour, 3)
#                 # division_mean = []
#                 # for i in division:
#                 #     division_mean.append(np.nanmean(i, axis=0))
#                 return np.array(division)
#
#
#             var_all = []
#             for iI_time in range(len(time_AR)):
#                 s_i = load_Q(pd.to_datetime(time_AR[iI_time]), var)
#                 contour = info_AR_row['contour_AR'].values[iI_time]
#                 s_i[contour != 1] = np.nan
#                 var_all.append(s_i)
#             var_3phase = cal_3phase(var_all)
#
#         return avg
#
#     for season in ['DJF']:
#         df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)
#
#         for iI, i in enumerate(df.columns):
#             times_file_all = df[i].values
#             mean_AR = []
#
#             for time_file in times_file_all:
#                 if len(str(time_file)) < 5:
#                     continue
#                 s_mean = contour_cal(time_file, var)
#                 mean_AR.extend(s_mean)
#             mean_AR = np.array(mean_AR)
#             print(mean_AR)
#
#             nc4.save_nc4(mean_AR,
#                          path_SaveData + 'PDF/%s' % (var + '_' + season + '_' + type_name[iI]))

def main_3phase(var):
    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_MainData + '%sAR_contour_42/%s' % (file_contour_Data, time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        if bu_all_or_AR == 'AR':
            all_or_AR = [time_AR, info_AR_row['contour_AR'].values]
        else:
            all_or_AR = [time_all, info_AR_row['contour_all'].values]

        if (var == 'advection') | (var == 'adibatic'):

            arg = []
            for add_Day in time_step:
                s_var_all = []
                for iI_time in range(len(all_or_AR[0])):
                    datetime_now = pd.to_datetime(all_or_AR[0][iI_time]) + np.timedelta64(add_Day, 'D')
                    if datetime_now.strftime('%m%d') == '0229':
                        datetime_now = pd.to_datetime(all_or_AR[0][iI_time]) + np.timedelta64(add_Day + 1, 'D')

                    print(datetime_now.strftime('%Y'))
                    if datetime_now.strftime('%Y') == '2021':
                        continue

                    print(datetime_now)
                    s_i = load_Q(
                        datetime_now,
                        var)
                    contour = all_or_AR[1][iI_time]
                    contour[int(30 / resolution), :] = 0
                    s_var_all.extend(s_i[contour == 1].flatten())
                arg.append(s_var_all)
        else:
            arg = []
            for add_Day in time_step:
                s_var_all = []
                for iI_time in range(len(all_or_AR[0])):
                    s_i = get_circulation_data_based_on_date(
                        pd.to_datetime(all_or_AR[0][iI_time]) + np.timedelta64(add_Day, 'D'),
                        var_LongName[var],
                        var)
                    contour = all_or_AR[1][iI_time]
                    contour[int(30 / resolution), :] = 0
                    s_var_all.extend(s_i[contour == 1].flatten())
                arg.append(s_var_all)
        return arg

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            phase1 = []
            phase2 = []
            phase3 = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean = contour_cal(time_file, var)
                phase1.extend(s_mean[0])
                phase2.extend(s_mean[1])
                phase3.extend(s_mean[2])

            phase = [phase1, phase2, phase3]
            for iI_phase, i_phase in enumerate(phase):
                nc4.save_nc4(np.array(i_phase),
                             path_MainData + 'PDF_5D/%s_%s' % (bu_all_or_AR,
                                                               var + 'DAY%s_' % time_step[iI_phase] + season + '_' +
                                                               type_name[iI]))


def main_linePhase(var, types='now'):
    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_MainData + '%sAR_contour_42/%s' % (file_contour_Data, time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        if bu_all_or_AR == 'AR':
            all_or_AR = [time_AR, info_AR_row['contour_AR'].values]
        else:
            all_or_AR = [time_all, info_AR_row['contour_all'].values]

        def cc_call_date_now(s_time, types):
            """
            to avoid too many Duplication
            :param s_time: pd.datetime
            :param types: now, climate
            :return:
            """
            if (var == 'advection') | (var == 'adibatic'):
                data = load_Q(
                    s_time,
                    var, types=types)
            else:
                data = get_circulation_data_based_on_date(
                    s_time,
                    var_LongName[var],
                    var, types=types)
            return data



        arg = []

        if AR_series >= len(all_or_AR[0]) - 1:
            return 'no returen'
        '''
        ####################
        loop time
        ####################
        '''
        time_loop = pd.date_range(pd.to_datetime(all_or_AR[0][AR_series]) - np.timedelta64(delay_time, 'D'),
                                  pd.to_datetime(all_or_AR[0][AR_series]) + np.timedelta64(time_step * 24, 'h'),
                                  freq='6H')

        date_era5_begin = pd.to_datetime('1979-01-02 00:00')
        if time_loop[0] < date_era5_begin:
            return  'no'

        # provided climately
        if types == 'now':
            data_climately = cc_call_date_now(time_loop[0], types=types)

        for s_I_AR, datetime_now in enumerate(time_loop):

            # provided climately
            if types == 'acceleration':
                data_climately = cc_call_date_now(datetime_now-np.timedelta64(6, 'h'), types='now')
                data_now = cc_call_date_now(datetime_now, types='now')
            else:
                data_now = cc_call_date_now(datetime_now, types=types)

            # mask latitude lesser than 70
            contour = all_or_AR[1][AR_series]
            # contour[int(30 / resolution), :] = 0

            if types != 'climately':
                if var == 'siconc':
                    # contour[s_i==0] = 0
                    contour[data_climately==0] = 0
                    if data_now[contour == 1].flatten().size == 0:
                        return 'no ice'

                data_now = data_now-data_climately

            # get percentile and mean of every time step
            characteristics = [np.nanmean(data_now[contour == 1].flatten())]

            characteristics.extend(np.percentile(data_now[contour == 1].flatten(), [5, 25, 50, 75, 95]))
            arg.append(characteristics)
        return arg

    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            characteristics = []
            times_file_all = df[i].values

            if types != 'climately':
                types_save = types + str(delay_time)+'_%i'%AR_series
            else:
                types_save = types
            print(path_MainData + 'PDF_line/%s_%s_%s.nc4' % (types_save, bu_all_or_AR,
                                                                var + 'DAY%s_' % time_step + season + '_' +
                                                                type_name[iI]))
            if os.path.exists(path_MainData + 'PDF_line/%s_%s_%s.nc4' % (types_save, bu_all_or_AR,
                                                                var + 'DAY%s_' % time_step + season + '_' +
                                                                type_name[iI])):
                print('exist'+'%s_%s_%s' % (types_save, bu_all_or_AR,
                                                                var + 'DAY%s_' % time_step + season + '_' +
                                                                type_name[iI]))
                continue

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue
                s_ii = contour_cal(time_file, var)
                if isinstance(s_ii, str):
                    continue
                characteristics.append(s_ii)
            if len(characteristics) == 0:
                continue
            characteristics = np.nanmean(characteristics, axis=0)




            nc4.save_nc4(characteristics,
                         path_MainData + 'PDF_line/%s_%s_%s' % (types_save, bu_all_or_AR,
                                                                var + 'DAY%s_' % time_step + season + '_' +
                                                                type_name[iI]))


# time_step = 5
# for AR_series in np.arange(0,8):
#     for delay_time in [3,5]:
#         types = 'acceleration'  # 'now'
#         bu_all_or_AR = 'all'
#         main_linePhase('ssr', types)
#         main_linePhase('slhf', types)
#         main_linePhase('sshf', types)
#         main_linePhase('t2m', types)
#         main_linePhase('advection', types)
#         main_linePhase('adibatic', types)
#         main_linePhase('str', types)
#         main_linePhase('siconc', types)
#         # main_linePhase('cbh', types)
#         # main_linePhase('hcc', types)
#         # main_linePhase('lcc', types)
#         # main_linePhase('mcc', types)
#         # main_linePhase('tcc', types)
#         # main_linePhase('tciw', types)
#         # main_linePhase('tclw', types)
#
# time_step = 5
# for AR_series in np.arange(0,8):
#     for delay_time in [3,5]:
#         types = 'now'  # 'now''acceleration'
#         bu_all_or_AR = 'all'
#         main_linePhase('ssr', types)
#         main_linePhase('slhf', types)
#         main_linePhase('sshf', types)
#         main_linePhase('t2m', types)
#         main_linePhase('advection', types)
#         main_linePhase('adibatic', types)
#         main_linePhase('str', types)
#         main_linePhase('siconc', types)
#         # main_linePhase('cbh', types)
#         # main_linePhase('hcc', types)
#         # main_linePhase('lcc', types)
#         # main_linePhase('mcc', types)
#         # main_linePhase('tcc', types)
#         # main_linePhase('tciw', types)
#         # main_linePhase('tclw', types)
# exit(0)

# # bu_all_or_AR = 'AR'
# # main_linePhase('str', types)
# # main_linePhase('advection', types)
# # main_linePhase('adibatic', types)
# # main_linePhase('ssr', types)
# # main_linePhase('slhf', types)
# # main_linePhase('sshf', types)
# # main_linePhase('t2m', types)
# # main_linePhase('sic', types)
# exit(0)

"""
=====================================
for anomaly compared with two days before, and line plot
=====================================
"""
path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF_line_NP85\\'

path_singleData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
                 'MS', 'GE', 'useless2']
type_name_num = ['0', '2', '3', '4', '5', '6']

type_name = ['greenland_east', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE']
colors = ['#000000', '#7FADB2', '#C2AE72', '#e7837a', 'tab:brown', 'tab:gray']
plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
             'MED', 'ARC']
infig_fontsize = 5

resolution = 0.5

delay_time = 5
time_step = 5

select_percentile =0
AR_series = 5
savename = '%i'%AR_series
types = 'acceleration'
types = 'now'


var_LongName = {'ssr': 'surface_net_solar_radiation',
                'strd': 'surface_thermal_radiation_downwards',
                'str': 'surface_net_thermal_radiation',
                'siconc': 'Sea_Ice_Cover',
                'slhf': 'surface_latent_heat_flux',
                'sshf': 'surface_sensible_heat_flux',
                't2m': '2m_temperature',
                'cbh': 'cloud_base_height',
               'hcc': 'high_cloud_cover',
               'lcc': 'low_cloud_cover',
            'mcc': 'medium_cloud_cover',
               'tcc': 'total_cloud_cover',
               'tciw': 'total_column_cloud_ice_water',
            'tclw': 'total_column_cloud_liquid_water',

}
plot_param = {'cbh': [['T2Mmean=', ' K'], 'CBH', [0, 120], 1],
              'hcc': [['SSHFmean=', ' W/$m^{2}$'], 'HCC', [0, 120], 1 ],
              'lcc': [['SLHFmea=', ' W/$m^{2}$'], 'LCC', [0, 120], 1],
              'mcc': [['ADVmean=', ' K/6h'], 'MCC', [0, 120], 1],
              'tcc': [['ABmean=', ' K/6h'], 'TCC', [0, 120], 1],
              'tciw': [['IVTmean=', ' kg/(m*s)'], 'TCIW', [0, 120], 1],
              'tclw': [['TCWVmean=', ' kg/$m^{2}$'], 'TCLW', [0, 120], 1, ],

                't2m':[['T2Mmean=',' K'],'T2M', [0, 120], 1],
              'sshf': [['SSHFmean=', ' W/$m^{2}$'], 'SSHF', [0, 120], 1 / (6 * 3600)],
              'slhf': [['SLHFmea=', ' W/$m^{2}$'], 'SLHF', [0, 120], 1 / (6 * 3600)],
              'advection': [['ADVmean=', ' K/6h'], 'advection', [0, 120], 6 * 3600],
              'adibatic': [['ABmean=', ' K/6h'], 'adiabatic', [0, 120], 6 * 3600],
              'IVT': [['IVTmean=', ' kg/(m*s)'], 'IVT', [0, 120], 1],
              'tcwv': [['TCWVmean=', ' kg/$m^{2}$'], 'TCWV', [0, 120], 1, ],
              'ssr': [['TCWVmean=', ' W/$m^{2}$'], 'TCWVano', [0, 120], 1 / (6 * 3600)],
              'str': [['STRmean=', ' W/$m^{2}$'], 'STR', [0, 120], 1 / (6 * 3600)],
              'siconc': [['SICmean=', ' %'], 'SIC', [0, 120], 100],
              }
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
plt.style.use('bmh')
SMALL_SIZE = 7

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

phase = ['0', '1', '2']
phase_name = ['1', '2', '3']
# vars = ['sshf','slhf']
vars = ['t2m', 'advection', 'sshf', 'slhf', 'str', 'siconc']
vars = ['t2m', 't2m', 'sshf', 'slhf', 'str', 'siconc']
# vars = ['hcc','mcc',  'lcc',
#             'tcc', 'tciw',
#             'tclw',]
frequency = []
for var in vars:

    def get_data():
        data_mean = []

        for s_type_SOM in type_name:
            data_mean.append(
                nc4.read_nc4(path_SecondData + '%s%i_%i_all_%s' % (types, delay_time,AR_series,
                                                                var + 'DAY%s_' % time_step + 'DJF' + '_' +
                                                                s_type_SOM)) *
                plot_param[var][-1])

        return np.array(data_mean)
    frequency.append(get_data())

def monte_carlo_in_PDF(data1, data2, test_range=20):
    import random
    # divided into space range and test
    if isinstance(test_range, int) | isinstance(test_range, float):
        test_range = np.linspace(np.nanmin(np.append(data1, data2)), np.nanmax(np.append(data1, data2)), test_range)
    print(test_range)

    def monte_carlo(data1, data2):
        data = np.append(data1, data2)
        div = []
        for i in range(1000):
            s_data1 = random.sample(set(data), len(data1))
            s_data2 = np.setdiff1d(data, s_data1)
            div.append(np.nanmean(s_data1) - np.nanmean(s_data2))

        return div, np.nanmean(data1) - np.nanmean(data2)

    for i in range(len(test_range))[:-1]:
        print(test_range[i], test_range[i + 1])
        div_all, div = monte_carlo(data1[(data1 >= test_range[i]) & (data1 < test_range[i + 1])],
                                   data2[(data2 >= test_range[i]) & (data2 < test_range[i + 1])])
        p = np.percentile(div_all, [99, 95, 90])

        print(p, div)


def plots(ax, data, color='black', mean_label=['', '']):
    # def pdf_kernel(X, x_range=''):
    #     from sklearn.neighbors import KernelDensity
    #     if x_range == '':
    #         x_range = [np.nanmin(X), np.nanmax(X)]
    #     X_plot = np.linspace(x_range[0], x_range[1], 100)[:, np.newaxis]
    #     kde = KernelDensity(kernel="gaussian", bandwidth=0.8).fit(X.reshape(-1, 1))
    #     log_dens = np.exp(kde.score_samples(X_plot))
    #     return [X_plot, log_dens]
    #
    # percentile_label = ['Q1', 'Q2', 'Q3']
    # percentile = np.percentile(data, [25, 50, 75])
    # mean = np.nanmean(data)
    # HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
    # # ax.boxplot(data)
    #
    # # _, _, bar_container = ax.hist(data,bins=HIST_BINS,density=True, lw=1,
    # #                           ec="black", fc="gray",zorder=2)
    #
    # data = data[~np.isnan(data)]
    # pdf_other_axes_1 = pdf_kernel(data)
    ax.plot(np.arange(data.shape[0]), data, color=color, linewidth=1, label=mean_label)

    # yticks = ax.get_yticks()
    # ax.set_yticks(yticks)
    # ax.set_yticklabels((yticks*len(data)).astype('int'))

    # ylim_max = ax.get_ylim()[1]
    # trans = ax.get_xaxis_transform()
    # for i_percentile in range(3):
    #     ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color=color,zorder=1)
    #     ax.text(percentile[i_percentile], 0.89, percentile_label[i_percentile],
    #             transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
    #             bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
    #     )
    #     ax.text(percentile[i_percentile], 1.01, '%0.1f'%percentile[i_percentile], transform=trans,
    #             horizontalalignment='center', fontsize=infig_fontsize)
    #
    # ax.axvline(mean, linestyle='-', linewidth=0.5, color=color, zorder=1)
    # ax.text(0.85, 0.89, mean_label[0]+'\n%0.2f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)

    return [ax.get_ylim()[1], ax.get_xlim()[1]]


# print(monte_carlo_in_PDF(frequency[0,1], frequency[3,1], np.array([0,10,20])))
# exit(0)


fig, axs = plt.subplots(6, 1)
plt.subplots_adjust(top=0.975,
                    bottom=0.07,
                    left=0.095,
                    right=0.98,
                    hspace=0.4,
                    wspace=0.2)

for i in range(6):


    ax = axs[i]

    for iI_region, i_region in enumerate(type_name):
        '''选取数值'''
        data = frequency[i][iI_region][:,select_percentile]
        plots(ax, data, color=colors[iI_region], mean_label=plot_name[iI_region])

    # ax.annotate(figlabelstr[3 * i + j], [0.01, 0.85], xycoords='axes fraction')

    axs[i].set_ylabel(plot_param[vars[i]][1] + '\n' + plot_param[vars[i]][0][1])
    ax.axvline(delay_time*4)
    ax.set_xticks(np.arange(0, data.shape[0]+1, 4))

axs[-1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
                  fancybox=True, ncol=6)
# axs[:][0].set_ylabel(plot_param[var][1]+plot_param[var][0][1])

plt.show()
# save_path = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\pic\\'
plt.title(savename)
plt.savefig('%s.png'%savename, dpi=400)
plt.close()



"""
=====================================
for anomaly compared with climately, and separated into 3 phase
=====================================
"""
# path_SecondData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\\'
#
# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
# time_Seaon_divide = [[12, 1, 2]]
# time_Seaon_divide_Name = ['DJF']
# type_name_row = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
#                  'MS', 'GE', 'useless2']
# type_name_num = ['0', '2', '3', '4', '5', '6']
#
# type_name = ['greenland_east', 'greenland_west', 'pacific_east', 'pacific_west',
#              'MS', 'GE']
# colors = ['#000000', '#7FADB2', '#C2AE72', '#e7837a', '#6893C2', '#E3ECC4']
# plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
#              'MED', 'ARC']
# infig_fontsize = 5
#
# resolution = 0.5
#
# plot_param = {'t2m': [['T2Mmean=', ' K'], 'T2M', [-5, 30], 1],
#               'sshf': [['SSHFmean=', ' W/$m^{2}$'], 'SSHF', [-10, 25], 1 / (6 * 3600)],
#               'slhf': [['SLHFmea=', ' W/$m^{2}$'], 'SLHF', [-10, 25], 1 / (6 * 3600)],
#               'advection': [['ADVmean=', ' K/6h'], 'advection', [-5, 10], 6 * 3600],
#               'adibatic': [['ABmean=', ' K/6h'], 'adiabatic', [-5, 5], 6 * 3600],
#               'IVT': [['IVTmean=', ' kg/(m*s)'], 'IVT', [0, 700], 1],
#               'tcwv': [['TCWVmean=', ' kg/$m^{2}$'], 'TCWV', [0, 20], 1, ],
#               'ssr': [['TCWVmean=', ' W/$m^{2}$'], 'TCWVano', [0, 20], 1 / (6 * 3600)],
#               'str': [['STRmean=', ' W/$m^{2}$'], 'STR', [-5, 15], 1 / (6 * 3600)],
#               'sic': [['SICmean=', ' %'], 'SIC', [-10, 10], 100]
#               }
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
# plt.style.use('bmh')
# SMALL_SIZE = 7
#
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
# plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
# plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
#
# phase = ['0', '1', '2']
# phase_name = ['1', '2', '3']
# # vars = ['sshf','slhf']
# vars = ['t2m', 'advection', 'sshf', 'slhf', 'str', 'sic']
#
# frequency = []
# for var in vars:
#
#     def get_data():
#         data_mean = []
#         for i_phase in phase:
#
#             s_data_mean = []
#             # for var in ['IVT', 'tcwv']:
#             #     s_data_mean.append(nc4.read_nc4(path_SecondData + '%s0_%s_%s' % (var, season, type_SOM)))
#
#             # for var in ['advection','adibatic']:
#             #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))*6*3600)
#             # for var in ['sshf','slhf']:
#             #     s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', type_SOM))/100000)
#             for s_type_SOM in type_name:
#                 s_data_mean.append(
#                     nc4.read_nc4(path_SecondData + 'all_%s%s_%s_%s' % (var, i_phase, 'DJF', s_type_SOM)) *
#                     plot_param[var][-1])
#
#             data_mean.append(s_data_mean)
#
#         return np.array(data_mean)
#
#
#     frequency.append(get_data())
#
#
# def monte_carlo_in_PDF(data1, data2, test_range=20):
#     import random
#     # divided into space range and test
#     if isinstance(test_range, int) | isinstance(test_range, float):
#         test_range = np.linspace(np.nanmin(np.append(data1, data2)), np.nanmax(np.append(data1, data2)), test_range)
#     print(test_range)
#
#     def monte_carlo(data1, data2):
#         data = np.append(data1, data2)
#         div = []
#         for i in range(1000):
#             s_data1 = random.sample(set(data), len(data1))
#             s_data2 = np.setdiff1d(data, s_data1)
#             div.append(np.nanmean(s_data1) - np.nanmean(s_data2))
#
#         return div, np.nanmean(data1) - np.nanmean(data2)
#
#     for i in range(len(test_range))[:-1]:
#         print(test_range[i], test_range[i + 1])
#         div_all, div = monte_carlo(data1[(data1 >= test_range[i]) & (data1 < test_range[i + 1])],
#                                    data2[(data2 >= test_range[i]) & (data2 < test_range[i + 1])])
#         p = np.percentile(div_all, [99, 95, 90])
#
#         print(p, div)
#
#
# def plots(ax, data, color='black', mean_label=['', '']):
#     def pdf_kernel(X, x_range=''):
#         from sklearn.neighbors import KernelDensity
#         if x_range == '':
#             x_range = [np.nanmin(X), np.nanmax(X)]
#         X_plot = np.linspace(x_range[0], x_range[1], 100)[:, np.newaxis]
#         kde = KernelDensity(kernel="gaussian", bandwidth=0.8).fit(X.reshape(-1, 1))
#         log_dens = np.exp(kde.score_samples(X_plot))
#         return [X_plot, log_dens]
#
#     percentile_label = ['Q1', 'Q2', 'Q3']
#     percentile = np.percentile(data, [25, 50, 75])
#     mean = np.nanmean(data)
#     HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
#     # ax.boxplot(data)
#
#     # _, _, bar_container = ax.hist(data,bins=HIST_BINS,density=True, lw=1,
#     #                           ec="black", fc="gray",zorder=2)
#
#     data = data[~np.isnan(data)]
#     pdf_other_axes_1 = pdf_kernel(data)
#     ax.plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color=color, linewidth=1, label=mean_label)
#
#     # yticks = ax.get_yticks()
#     # ax.set_yticks(yticks)
#     # ax.set_yticklabels((yticks*len(data)).astype('int'))
#
#     # ylim_max = ax.get_ylim()[1]
#     # trans = ax.get_xaxis_transform()
#     # for i_percentile in range(3):
#     #     ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color=color,zorder=1)
#     #     ax.text(percentile[i_percentile], 0.89, percentile_label[i_percentile],
#     #             transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
#     #             bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
#     #     )
#     #     ax.text(percentile[i_percentile], 1.01, '%0.1f'%percentile[i_percentile], transform=trans,
#     #             horizontalalignment='center', fontsize=infig_fontsize)
#     #
#     # ax.axvline(mean, linestyle='-', linewidth=0.5, color=color, zorder=1)
#     # ax.text(0.85, 0.89, mean_label[0]+'\n%0.2f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)
#
#     return [ax.get_ylim()[1], ax.get_xlim()[1]]
#
#
# # print(monte_carlo_in_PDF(frequency[0,1], frequency[3,1], np.array([0,10,20])))
# # exit(0)
#
#
# fig, axs = plt.subplots(6, 3, figsize=[5.5, 6])
# plt.subplots_adjust(top=0.975,
#                     bottom=0.07,
#                     left=0.095,
#                     right=0.98,
#                     hspace=0.4,
#                     wspace=0.2)
#
# for i in range(6):
#     for j in range(3):
#
#         ax = axs[i][j]
#
#         for iI_region, i_region in enumerate(type_name):
#             data = frequency[i][j][iI_region]
#             plots(ax, data, color=colors[iI_region], mean_label=plot_name[iI_region])
#         ax.set_xlim(plot_param[vars[i]][-2])
#         ax.annotate(figlabelstr[3 * i + j], [0.01, 0.85], xycoords='axes fraction')
#     axs[i][0].set_ylabel(plot_param[vars[i]][1] + '\n' + plot_param[vars[i]][0][1])
#
#     # axs[i].axvline(0)
#
# # for i in range(3):
# #     # lim = np.zeros([len(vars), 1])
# #
# #     for i_region in range(len(type_name_compare)):
# #         data = frequency[i_region][i]
# #         ax = axs[i]
# #         plots(ax, data, color=colors[i_region], mean_label=type_name_compare[i_region])
# #         # plt.show()
# #     axs[i].set_xlim(plot_param[var][-2])
# #     axs[i].set_title('phase %s'%phase[i])
# #     axs[i].axvline(0)
#
#
# # ax.set_xlabel(plot_param[vars[i]][1])
#
# # maxy = np.max(lim[:,0])
# # maxx = np.max(lim[:, 1])
# # axs[i][0].set_ylim(top=maxy)
# # axs[i][1].set_ylim(top=maxy)
# # axs[i][0].set_xlim(plot_param[vars[i]][2])
# # axs[i][1].set_xlim(plot_param[vars[i]][2])
#
# # axs[i][0].set_xlim([-20, 20])
# # axs[i][1].set_xlim([-20,20])
#
# # if i != 0:
# #     axs[i][0].set_xlim([0,20])
# #     axs[i][1].set_xlim([0,20])
# # else:
# #     axs[0][0].set_xlim([0,700])
# #     axs[0][1].set_xlim([0,700])
#
# # pdf_other_axes_1 = pdf(frequency[0][i])
# # pdf_other_axes_2 = pdf(frequency[1][i])
# # axs[i][1].plot(pdf_other_axes_2[0], pdf_other_axes_2[1], color='red',linewidth=1)
# # axs[i][1].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='red',linestyle='--',linewidth=1.5)
# # axs[i][0].plot(pdf_other_axes_1[0], pdf_other_axes_1[1], color='red',linewidth=1)
# axs[-1][1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
#                   fancybox=True, ncol=6)
# # axs[:][0].set_ylabel(plot_param[var][1]+plot_param[var][0][1])
# for j in range(3):
#     axs[0][j].set_title('phase %s' % (phase_name[j]))
# # plt.show()
# save_path = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\PDF\pic\\'
# plt.savefig('figure5.png', dpi=400)
# plt.close()
