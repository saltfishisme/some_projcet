import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import nc4
import numpy as np
import scipy.io as scio
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import scipy.interpolate as interp
from matplotlib import patches
def query_inserction(traj, cyclone):
    import  shapely.geometry as sgeo
    from shapely.strtree import STRtree
    # print('get cyclone one west and two north')
    def querys(traj, cyclone):
        def get_range_between_two_cyclone(cyclone1, cyclone2):
            query_geom1 = sgeo.Point(cyclone1).buffer(2.5)
            query_geom2 = sgeo.Point(cyclone2).buffer(2.5)
            lines = sgeo.LineString([cyclone1, cyclone2]).buffer(2)
            lines = lines.difference(query_geom1)
            lines = lines.difference(query_geom2)
            return lines
        polys = []
        for i in range(traj.shape[2]):
            polys.append(sgeo.LineString(traj[:,:,i]))
        s = STRtree(polys)
        index_by_id = dict((id(pt), i) for i, pt in enumerate(polys))
        if len(np.array(cyclone).shape) == 1:
            query_geom = sgeo.LineString(cyclone)
        else:
            query_geom = get_range_between_two_cyclone(cyclone[0], cyclone[1])

        bc = []
        for i, o in enumerate(s.query(query_geom)):
            if o.intersects(query_geom):
                bc.append(index_by_id[id(o)])
                # bc.append(i)
        # print(np.max(bc), len(bc))
        return bc, query_geom

    main_idx = np.arange(traj.shape[2])
    dell_idx = []
    classification = []
    geom_all = []
    geom = []
    # get two cyclone traj
    a, query_geom = querys(np.transpose(traj, [1,0,2]), cyclone[0])
    geom.append(query_geom)
    geom_all.append(query_geom)
    classification.append(a)

    dell_idx.extend(a)
    diff_idx = np.setdiff1d(main_idx, dell_idx)
    traj_diff = traj[:,:,diff_idx]

    b, query_geom = querys(np.transpose(traj_diff, [1,0,2]), cyclone[1])
    geom.append(query_geom)
    geom_all.append(query_geom)
    classification.append(diff_idx[b])
    dell_idx.extend(diff_idx[b])
    diff_idx = np.setdiff1d(main_idx, dell_idx)
    traj_diff = traj[:,:,diff_idx]

    final_classifi = classification
    return final_classifi, geom
def projection_transform(lon, lat, central_latitude, xy2ll=False):
    lon = np.array(lon);
    lat = np.array(lat)
    shape_lon = lon.shape;
    shape_lat = lat.shape
    lon = lon.flatten();
    lat = lat.flatten()

    central_latitude_abs = abs(central_latitude)
    if (central_latitude_abs <= 60) & (central_latitude_abs >= 30):
        if central_latitude > 0:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': 30, 'lat_2': 60}
        else:
            proj_Out = {'proj': 'lcc', 'lon_0': 120, 'lat_1': -30, 'lat_2': -60}

    elif central_latitude_abs > 60:
        proj_Out = {'proj': 'ups'}

    else:
        proj_Out = {'proj': 'merc', 'lon_0': 120}

    from pyproj import Transformer
    if xy2ll:
        transproj = Transformer.from_crs(
            proj_Out,
            "EPSG:4326",
            always_xy=True,
        )
    else:
        from pyproj import Transformer
        transproj = Transformer.from_crs(
            "EPSG:4326",
            proj_Out,
            always_xy=True,
        )

    lon, lat = transproj.transform(lon, lat)
    return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)


def interpolation_nan_for_longpath(longpath):
    longpath_shape = longpath.shape
    arr_ref_size = longpath.shape[2]
    longpath = longpath.reshape(longpath.shape[0] * 2, longpath.shape[-1])
    for i in range(len(longpath)):
        arr1 = longpath[i][~np.isnan(longpath[i])]  # skip nan
        arr1_interp = interp.interp1d(np.arange(arr1.size), arr1)
        longpath[i] = arr1_interp(np.linspace(0, arr1.size - 1, arr_ref_size))
    return longpath.reshape(longpath_shape)


def SOM_param(data, xylength, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = []
    for lead in ['X', 'Y']:
        for i in range(1, xylength):
            varname.append(str(i) + lead)
    varname.extend(addtional_varname)
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})


def SOM_spcific_param(data, addtional_varname, save_name):
    import pandas as pd
    import scipy.io as scio
    import numpy as np

    path_Save = r'D:\OneDrive\a_matlab\a_test\\'
    varname = addtional_varname
    labelcell = np.arange(data.shape[0])
    scio.savemat(path_Save + '%s_param.mat' % save_name,
                 {'D': np.array(data, dtype='double'), 'varname': varname, 'labelcell': labelcell})


def plot(save_labels, longpath, centroid, savename=''):
    def reject_outliers(data, m = 0.8):
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev if mdev else 0.
        return np.nanmean(data[s<m])
    # if longpath:
    #     longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s'%season)
    size = int(np.ceil(len(np.unique(save_labels)) / 3))
    fig = plt.figure()

    for iI, i in enumerate(np.unique(save_labels)):
        if i != 0:
            ax = fig.add_subplot(size, 3, iI + 1, projection=ccrs.NorthPolarStereo())
            ax.set_extent([0, 359, 20, 90], crs=ccrs.PlateCarree())


            ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.05,
                       transform=ccrs.PlateCarree())
            s_centroid = np.apply_along_axis(reject_outliers, 0, centroid[save_labels == i])
            e1 = patches.Ellipse((s_centroid[1], s_centroid[0]), s_centroid[4], s_centroid[5],
                                 angle=180-np.nanmean(centroid[save_labels == i, 3]), color='red', linewidth=1, fill=False, zorder=2, transform=ccrs.PlateCarree())
            ax.add_patch(e1)
            ax.set_title('total number is %i, %0.0f%%'%(centroid[save_labels == i].shape[0], 100*centroid[save_labels == i].shape[0]/totol_events))
            ax.coastlines()
    plt.suptitle(season)
    plt.tight_layout()
    plt.show()
    # plt.savefig(path_pic+'%s_%s.png'%(season, savename), dpi=500)
    plt.close()
    # # # plot classification result
    # fig, ax = plt.subplots(subplot_kw={'projection': ccrs.NorthPolarStereo()})
    # for i in np.unique(save_labels[save_labels == 0]):
    #     ax.scatter(longpath[save_labels == i, 1], longpath[save_labels == i, 0], s=0.1, transform=ccrs.PlateCarree())
    # ax.coastlines()
    # plt.title(season)
    # plt.show()
    # plt.close()

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)'];
""" divided into 4 regions"""

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
for season in ['DJF', 'MAM', 'JJA', 'SON']:
    clustering_fileName_Final = pd.DataFrame()
    # path_SaveData = r'/home/linhaozhong/work/AR_detection/'

    centroid_row = nc4.read_nc4(path_SaveData + r'FDI/centroid_42%s' % season)
    centroid_index_row = scio.loadmat(path_SaveData + r'FDI/centroid_index_42%s.mat' % season)['centroid_index']
    longpath_row = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    longpath_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat' % season)['centroid_index']



    # skip down_lat is positive
    longpath = longpath_row[centroid_row[:, -1] != 1]
    centroid = centroid_row[centroid_row[:,-1]!=1]
    centroid_index = centroid_index_row[centroid_row[:,-1]!=1]
    totol_events = centroid.shape[0]

    longpath_lon70 = []
    for i_time in range(longpath.shape[0]):

        s_longpath_lon70 = [longpath[i_time, 1, :][np.nanargmin(abs(longpath[i_time, 0, :] - 70))]]
        if len(s_longpath_lon70) != 0:
            longpath_lon70.append(s_longpath_lon70[-1])
        else:
            longpath_lon70.append(np.nan)

    classification = pd.cut(longpath_lon70, [0, 15, 150, 210, 270, 320, 360],
                            labels=[1, 2, 3, 4, 5, 6])

    save_labels = np.array(classification)
    save_labels[save_labels == 6] = 1

    for i_sl in [1,4,5]:
        clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(centroid_index[save_labels==i_sl])],
                                             ignore_index=True,axis=1)
    # # step01 all
    # plot(save_labels, longpath, centroid, 'all')
    # continue

    # # step02 pacific
    longpath_3 = longpath[save_labels==3]
    centroid_3 = centroid[save_labels==3]
    centroid_index_3 = centroid_index[save_labels==3]

    save_labels_3= np.zeros(longpath_3.shape[0])
    save_labels_3[centroid_3[:, 3] >= 0] = 1
    save_labels_3[centroid_3[:, 3] < 0] = 2
    for i_sl in [1,2]:
        clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(centroid_index_3[save_labels_3 == i_sl])],
                                             ignore_index=True,axis=1)
    ######### for siberia



    centroid_index_siberia = centroid_index[save_labels == 2]

    type_siberia = []
    filename = []

    for s_filename in centroid_index_siberia:
        time_file_new = ''
        for i in s_filename:
            time_file_new += i
            if i == 'c':
                break
        s_filename = time_file_new
        try:
            # print(s_filename)
            # print(path_moisture_path+s_filename[:-3]+'.mat')
            a = scio.loadmat(path_moisture_path + s_filename[:-3] + '.mat')
        except:
            continue
        index = []
        for i in sorted(a.keys()):
            if i[0] != '_':
                index.append(i)

        traj = a[index[0]][:, :use_day_for_classification * 4, :]
        if traj.shape[2] == 0:
            # print(traj.shape)
            # print(s_filename)
            continue
        classifi, geom = query_inserction(traj, [[(0, 50), (90, 50)], [(5, 50), (5, 90)]])
        ratio_classifi = [len(i) / traj.shape[2] for i in classifi]
        ratio_classifi.extend([max_ratio])
        type_siberia.append(np.argmax(ratio_classifi))
        filename.append(s_filename)

        # if np.argmax(ratio_classifi) == 2:
        # print('######################')
        # print(s_filename)
        # print(ratio_classifi)
        # lineplot = 5
        # try:
        #     fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
        #     plot_trajectory(a[index[0]])
        #     ax.add_geometries(geom, ccrs.PlateCarree(),
        #                       facecolor='yellow', edgecolor='k')
        #     ax.set_global()
        #     ax.coastlines()
        #     ax.set_title(str(np.argmax(ratio_classifi)))
        #     # plt.show()
        #     plt.savefig(r'/home/linhaozhong/work/AR_detection/clustering_pic/%s.png' % s_filename[:-3], dpi=300)
        #     plt.close()
        # except:
        #     continue
    type_siberia = np.array(type_siberia)
    filename = np.array(filename)
    for i_type in [0,1,2]:
        print(filename[type_siberia == int(i_type)].shape)
        clustering_fileName_Final= pd.concat([clustering_fileName_Final, pd.Series(filename[type_siberia == i_type])],
                                             ignore_index=True,axis=1)

    clustering_fileName_Final.to_csv(path_savepath + 'type_filename_%s.csv' % season, index=False)


    # # type_filename = pd.DataFrame()
    # # type_filename['type'] = type
    # # type_filename['filename'] = filename
    # # print(type_filename['type'].value_counts())
    # # type_filename.to_csv(path_savepath + 'type_filename_%s.csv' % season)
    #
    #
    #
    #
    #
    #
    #
    #
    #
    # # print(len(save_labels[save_labels==1]))
    # # plot(save_labels, longpath, centroid_3, 'pacific')
    # # continue
    #
    #
    # # # step03 siberia
    # # longpath = longpath[save_labels==2]
    # # centroid_3 = centroid[save_labels==2]
    # # centroid_index3 = centroid_index[save_labels==2]
    # #
    # # save_labels= np.ones(longpath.shape[0])
    # #
    # # save_labels[(centroid_3[:,3]<=50) & (centroid_3[:,3]>0)] = 2
    # # save_labels[(centroid_3[:,3]>=-50) & (centroid_3[:,3]<0)] = 3
    # # print(centroid_index3[save_labels==3])
    # # plot(save_labels, longpath, centroid_3, 'siberia')
    #
    #
    # longpath_endpoint = np.array([np.array(i[~np.isnan(i)]).reshape([2, -1])[0,-1] for i in longpath])
    # som_data = longpath_endpoint[save_labels == 2]
    #
    # longpath = longpath[save_labels==2]
    # centroid_3 = centroid[save_labels==2]
    # centroid_index3 = centroid_index[save_labels==2]
    #
    # save_labels= np.ones(longpath.shape[0])
    #
    # save_labels[som_data<60] = 2
    # # save_labels[(centroid_3[:,3]>=-50) & (centroid_3[:,3]<0)] = 3
    # plot(save_labels, longpath, centroid_3, 'siberia')
    #
    # continue
    # ''' skip given direction and orientation'''
    # # for i in np.unique(save_labels):
    # #     arg_i_3 = np.argwhere(save_labels==i).flatten()[centroid_row[:,3][save_labels==i]>0]
    # #     arg_i_2 = np.argwhere(save_labels==i).flatten()[(centroid_row[:,2][save_labels==i]>90)&(centroid_row[:,2][save_labels==i]<270)]
    # #
    # #     save_labels[np.unique(np.append(arg_i_2, arg_i_3))] = 0
    #
    # ########################################################
    # """ for SOM """
    #
    #
    # # # for endpoint
    # # longpath_endpoint = np.array([np.array(i[~np.isnan(i)]).reshape([2, -1])[:,-1] for i in longpath])
    # # longpath = longpath_endpoint[save_labels == 2]
    #
    # # # for orientation
    # longpath_endpoint = np.array([np.array(i[~np.isnan(i)]).reshape([2, -1])[:,-1] for i in longpath])
    #
    # longpath_endpoint = np.append(longpath_endpoint, centroid[:, 3].reshape([longpath_endpoint.shape[0],1]), axis=1)
    # longpath_endpoint = np.append(longpath_endpoint, centroid[:, 2].reshape([longpath_endpoint.shape[0],1]), axis=1)
    #
    # som_data = longpath_endpoint[save_labels == 2]
    #
    # # b = longpath
    # # from sklern import mixture
    # # s_dpgmm = mixture.BayesianGaussianMixture(n_components=10,
    # #                                           covariance_type='full').fit(b)
    # # save_labels_bmus = s_dpgmm.predict(b)
    # # longpath a= nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    # # plot(save_labels_bmus, longpath[save_labels == 2])
    #
    # ## SOM
    # SOM_spcific_param(som_data[:,:2], ['oritation', 'oritentaon2'], '%s'%season)
    # save_labels_bmus = scio.loadmat(r"D:\OneDrive\a_matlab\a_test\%s_BMUS.mat"%season)['BMUS'][:, -1].flatten()
    # plot(save_labels_bmus, longpath[save_labels == 2], centroid[save_labels==2], 'Siberia')
    # # exit(0)
    #
    # continue
    # ########################################################
    # '''for all SOM data: longpath, centroid, direction, orientation'''
    # # # path_SaveData = r'/home/linhaozhong/work/AR_detection/'
    # path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
    # #
    # centroid_row = nc4.read_nc4(path_SaveData + r'FDI/centroid_42%s' % season)
    # centroid_index = scio.loadmat(path_SaveData + r'FDI/centroid_index_42%s.mat' % season)['centroid_index']
    # centroid_row = centroid_row[:, :4]
    # longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    # longpath_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat' % season)['centroid_index']
    # longpath = interpolation_nan_for_longpath(longpath)
    # longpath = longpath.reshape([longpath_index.shape[0], longpath.shape[1] * longpath.shape[2]])
    # longpath = np.append(longpath, centroid_row, axis=1)
    #
    # longpath = longpath[save_labels == 2]
    # SOM_param(longpath, int((longpath.shape[1] - 4) / 2), ['centroidx', 'centroidy', 'cd', 'orientation'], 'test')
    #
    # longpath = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    # save_labels_bmus = scio.loadmat(r"D:\OneDrive\a_matlab\a_test\BMUS.mat")['BMUS'][:, -1].flatten()
    # plot(save_labels_bmus, longpath[save_labels == 2])
    # exit(0)
    #
    # # '''for centroid clustering'''
    # # # #skip latitude characteristic
    # # b = centroid_row.copy()
    # # b[:, 0] = 70
    # # long_ups, lat_ups = projection_transform(centroid_row[:,1], centroid_row[:, 0], 70)
    # # b[:, 1] = long_ups
    # # b[:, 0] = lat_ups
    #
    # '''for longpath clustering'''
    # centroid_row = nc4.read_nc4(path_SaveData + r'FDI/IP_42%s' % season)
    # centroid_index = scio.loadmat(path_SaveData + r'FDI/IP_index_42%s.mat' % season)['centroid_index']
    # # fig, ax = plt.subplots(subplot_kw={'projection':ccrs.NorthPolarStereo()})
    # # for i in range(len(centroid_row)):
    # #     ax.coastlines()
    # #     ax.set_extent([0,359,20,90], crs=ccrs.PlateCarree())
    # #
    # #     ax.scatter(centroid_row[i, 1], centroid_row[i, 0], s=0.1,transform=ccrs.PlateCarree())
    # # plt.show()
    # # plt.close()
    # centroid = interpolation_nan_for_longpath(centroid_row)
    # # for i in range(len(centroid)):
    # #     centroid[i,1], centroid[i, 0] = projection_transform(centroid[i,1], centroid[i, 0], 70)
    # b = centroid.reshape([centroid.shape[0], centroid.shape[1] * centroid.shape[2]])
    #
    # # BGM classification
    # from sklearn import mixture
    #
    # s_dpgmm = mixture.BayesianGaussianMixture(n_components=10,
    #                                           covariance_type='full').fit(b)
    # save_labels = s_dpgmm.predict(b)
    #
    # plot(save_labels)
