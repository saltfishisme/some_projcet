import os

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from function_shared import get_data_output_dataarray, select_rectangle_area
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.interpolate import cross_section
from Setting_mpv import *

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

var_with_shortname = {'Hgt': 'z', 'Tem': 't', 'Uwnd': 'u', 'Vwnd': 'v', 'RH': 'r'}
path_contourData = r'/home/linhaozhong/work/AR_NP85/AR_contour_42/'
composite_prssure_level = 1000
path_saveData = r'/home/linhaozhong/work/AR_NP85/nowindzone/'

def main_3phase():

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_contourData + time_file_new)

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        for i_time in time_all:

            i_contour = info_AR_row['contour_all'].sel(time_all=i_time).values
            i_centroid = info_AR_row['centroid_all'].sel(time_all=i_time).values

            i_time = pd.to_datetime(i_time)
            # cross = get_data_output_dataarray(pd.to_datetime(i_time), ['Hgt', 'Uwnd', 'Vwnd'], [1000])
            # cross = cross.sel(level=1000)
            cross = load_ivt_total(i_time)
            def plot_quiver():
                plt.rcParams['hatch.color'] = 'darkred'
                fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree(central_longitude=i_centroid[1])})
                delta = 4
                delta_lat = 2
                data, [lat, long] = select_rectangle_area([i_contour, cross],
                                                          [int(i_centroid[0]), int(i_centroid[1])],
                                                          info_AR_row.lon.values, info_AR_row.lat.values,
                                                          r_lat=80, r_lon=90)
                i_contour_cut, ivt = data

                def uv_in_AR(u, v, i_contour):
                    u_AR = np.mean(u[i_contour > 0.5])
                    v_AR = np.mean(v[i_contour > 0.5])

                    u_diff = u - u_AR
                    v_diff = v - v_AR
                    return np.sqrt(u_diff**2+ v_diff**2)
                    # return np.sqrt(u ** 2 + v ** 2)

                ax.contourf(long, lat, np.ma.masked_less(i_contour_cut, 0.1),

                            colors='none', levels=[0.1, 1.1],
                            hatches=[2 * '.', 2 * '.'], alpha=0, facecolor='red', edgecolor='red',
                            transform=ccrs.PlateCarree(), zorder=20)

                "split into rectangle"



                "plot"

                # cb = ax.contour(long, lat,
                #                  z,
                #                  transform=ccrs.PlateCarree(), zorder=5)

                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
                #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
                import cmaps
                cb = ax.contourf(long, lat,
                                ivt, extend='both',levels=np.arange(0,300,20),
                                transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=3)
                plt.colorbar(cb)
                # ax.quiver(long[::delta], lat[::delta_lat],
                #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
                #           transform=ccrs.PlateCarree(), scale=200, zorder=7)

                plt.title(i_time)
                ax.coastlines(zorder=9)
                # ax.set_extent()
                # plt.show()
                path_save = path_saveData + str(var) +  '_ivt/' + time_file_new +'/'
                os.makedirs(path_save, exist_ok=True)
                plt.savefig(path_save+i_time.strftime('%Y%m%d%H'), dpi=200)
                plt.close()

            plot_quiver()
        # plot_contour(uv_in_AR(), cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values, cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values,cross['pv'].values)

        return

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                contour_cal(time_file, i)

main_3phase()

exit(0)
##############################

time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
plt.rcParams['hatch.color'] = 'darkred'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_MainData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_NP85/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_NP85/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

times_all = pd.date_range('1980-12-17', '1980-12-19', freq='1D').strftime('%Y%m%d')

sta_AR = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\1980121717_2.nc')
iI_AR_loc = 24
for i_time in times_all:
    vari_SaveName_all = ['Hgt', 'RH', 'Tem', 'Uwnd', 'Vwnd']
    # p0 =  xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\surface_pressure.%s.nc'%i_time)

    data_row = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\Hgt.%s.nc' % i_time)
    for i in vari_SaveName_all:
        data_row = xr.merge([data_row, xr.open_dataset(
            r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\%s.%s.nc' % (i, i_time))])

    for iI_time_daily in [5, 11, 17, 23]:
        data = data_row.isel(time=iI_time_daily)
        print(data)
        # p0 = p0.isel(time=iI_time_daily)
        # data = data.sel(level=1000)

        ##############################
        # Get the cross section, and convert lat/lon to supplementary coordinates:

        cross = data.squeeze().set_coords(('latitude', 'longitude'))

        i_contour = sta_AR['contour_all'].sel(time_all=cross.time).values

        i_centroid = sta_AR['centroid_all'].sel(time_all=cross.time).values


        def select_rectangle_area(data, centroid, long, lat, r_lat=20, r_lon=30):
            """
            :param data: [hgt, u, v, ...], var is 2d.
            :param centroid: select a rectangle based on centroid and r_lat, r_lon
            :param long:
            :param lat:
            :param r_lat:
            :param r_lon:
            :return:
            """

            def roll_longitude_from_359to_negative_180(data, long):
                roll_length = np.argwhere(long < 180).shape[0] - 1
                long = np.roll(long, roll_length)
                data = np.roll(data, roll_length)
                long = xr.where(
                    long > 180,
                    long - 360,
                    long)
                return data, long

            lon_index = np.argwhere(long == centroid[1])[0]
            if (lon_index - r_lon < 0) | (lon_index + r_lon > long.shape[0]):
                data, long = roll_longitude_from_359to_negative_180(data, long)
                print(long)
                centroid[1] = (centroid[1] + 180) % 360 - 180
                print(centroid[1])
            long_2d, lat_2d = np.meshgrid(long, lat)
            lat_index = np.argwhere(lat == centroid[0])[0]
            lon_index = np.argwhere(long == centroid[1])[0]
            result = []
            for i_data in data:
                s_data = i_data[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])), :]
                lat_2d_flatten = lat_2d[int(np.max([0, lat_index - r_lat])):int(np.min([len(lat), lat_index + r_lat])),
                                 0]

                s_data = s_data[:, int(lon_index - r_lon):int(lon_index + r_lon)]
                long_2d_flatten = long_2d[0, int(lon_index - r_lon):int(lon_index + r_lon)]

                result.append(s_data)
            return result, [lat_2d_flatten, long_2d_flatten]


        def plot_quiver():
            plt.rcParams['hatch.color'] = 'darkred'
            fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
            delta = 4
            delta_lat = 2

            ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(i_contour, 0.1),

                        colors='none', levels=[0.1, 1.1],
                        hatches=[2 * '.', 2 * '.'], alpha=0, facecolor='red', edgecolor='red',
                        transform=ccrs.PlateCarree(), zorder=20)

            "split into rectangle"

            data, [lat, long] = select_rectangle_area([cross['z'] / 98, cross['u'], cross['v']],
                                                      [int(i_centroid[0]), int(i_centroid[1])],
                                                      cross.longitude.values, cross.latitude.values,
                                                      r_lat=80, r_lon=90)

            "plot"

            z, u, v = data
            cb = ax.contourf(long, lat,
                             z,
                             transform=ccrs.PlateCarree(), zorder=5)
            plt.colorbar(cb)
            # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
            #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
            #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
            ax.quiver(long[::delta], lat[::delta_lat],
                      u[::delta_lat, ::delta], v[::delta_lat, ::delta],
                      transform=ccrs.PlateCarree(), scale=200, zorder=7)

            plt.title(cross.time.values)
            ax.coastlines(zorder=9)
            # ax.set_global()
            plt.savefig()
            plt.close()


        def plot_contour(u, v):
            # fig,axs = plt.subplots(1,1,subplot_kw={'projection':ccrs.Orthographic(0, 70)})
            fig, axs = plt.subplots(1, 1, subplot_kw={'projection': ccrs.PlateCarree()})
            delta = 4
            delta_lat = 2
            # data = [cross['t'],cross['r']]
            data = u
            for i in range(1):
                import cmaps
                ax = axs
                # data = xr.open_dataset(r'D:/vor.nc')['vo'].sel(level=1000, time=cross.time)
                # print(i_centroid)

                data, [lat, long] = select_rectangle_area([data], [int(i_centroid[0]), int(i_centroid[1])],
                                                          cross.longitude.values, cross.latitude.values,
                                                          r_lat=80, r_lon=90)

                # z = xr.open_dataset(r'D:/z.nc')['z'].sel(level=1000, time=cross.time)
                # plt.imshow(z)
                # plt.show()
                # slice = []

                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
                #           transform=ccrs.PlateCarree(),scale=0.01, zorder=7)
                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           cross['u'].values[::delta_lat,::delta], cross['u'].values[::delta_lat,::delta],
                #           transform=ccrs.PlateCarree(),scale=500, zorder=7)

                ax.contourf(np.arange(0, 360, 0.5), np.arange(90, -90.1, -0.5), np.ma.masked_less(i_contour, 0.1),

                            colors='none', levels=[0.1, 1.1],
                            hatches=[2 * '.', 2 * '.'], alpha=0, edgecolor='black', transform=ccrs.PlateCarree(),
                            zorder=20)

                cb = ax.contourf(long, lat,
                                 data[0], extend='both', levels=np.arange(0, 20.1, 0.5),
                                 transform=ccrs.PlateCarree(), cmap=cmaps.MPL_RdBu_r, zorder=5)

                """"# for relative vorticity"""
                # cb = ax.contourf(cross.longitude.values, cross.latitude.values,
                #                 data, extend='both',levels=np.arange(0,30.1,0.5),
                #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)

                # cb = ax.contourf(long, lat,
                #                 np.array(data[0])*1e4, extend='both',levels=np.arange(-4,4.01,0.1),
                #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu_r, zorder=5)
                # cb = ax.contourf(cross.longitude.values, cross.latitude.values,
                #                 data.values*1e4, extend='both',level=np.arange(-4,4,0.1),
                #                 transform=ccrs.PlateCarree(),cmap=cmaps.MPL_RdBu, zorder=5)
                delta = 4;
                delta_lat = 2
                # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
                #           cross.sel(level=1000)['u'].values[::delta_lat, ::delta], cross.sel(level=1000)['v'].values[::delta_lat, ::delta],
                #           transform=ccrs.PlateCarree(), scale=500, zorder=7)
                plt.colorbar(cb)
                plt.title(cross.time.values)
                ax.coastlines(zorder=9)
                # ax.set_global()
            plt.show()


        # plot_contour(uv_in_AR(), cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values, cross['pv'].values)
        # plot_contour(cross['pv'].sel(level=1000).values,cross['pv'].values)
        continue
