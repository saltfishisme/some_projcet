import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
path_moisture_path = r'/home/linhaozhong/work/Northpolar_era5/1950_2019/output_int6hr_NorthPolar-1deg/'
bu_addition_Name = ''
file_contour_Data = ''


def basis_analysis(bu_length_lack, times_file_all, saveName=''):
    def centroid(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal_DRM(time_file):
            info_AR_row = xr.open_dataset(
                path_SaveData + '%sAR_contour_%s/%s' % (file_contour_Data, bu_length_lack, time_file))

            time_AR = info_AR_row['time_AR'].values
            contour_AR = info_AR_row['contour_AR']
            time_daily = pd.date_range(time_AR[0], time_AR[-1], freq='1D').strftime('%Y%m%d')
            moisture_path_all = {}
            for i_time_daily in time_daily:
                contour_daily = np.nansum(
                    contour_AR.sel(time_AR=i_time_daily, lat=slice(89.5, 60.5), lon=slice(0.5, 359.5)).values, axis=0)[
                                ::2, ::2].flatten()
                try:
                    moisture_path = scio.loadmat(path_moisture_path + '%sres_6hr.mat' % i_time_daily)
                except:
                    return
                # moisture_path = scio.loadmat(r'D:\OneDrive\a_matlab\a_test\output_int6hr_NorthPolar-1deg\19500121res_6hr.mat')['pos_int']
                moisture_path_all['a' + i_time_daily] = moisture_path['pos_int'][:, :, contour_daily > 0]
                moisture_path_all['rr' + i_time_daily] = moisture_path['rr_int'][:, contour_daily > 0]

            os.makedirs(path_SaveData + 'AR_moisture_path_%s/' % bu_length_lack, exist_ok=True)
            scio.savemat(path_SaveData + 'AR_moisture_path_%s/%s.mat' % (bu_length_lack, time_file[:-3]),
                         moisture_path_all)
            return

        contour_cal_DRM('1980122717_1.nc')
        exit(0)
        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            s_data = []
            s_index = []
            files = df[df['season'] == i]['file']
            for time_file in files:
                contour_cal_DRM(time_file)

            # s_data = np.array(s_data)
            # nc4.save_nc4(s_data, path_SaveData+'FDI/centroid_%s%s%s'%(bu_length_lack, saveName, i))
            # s_index = np.array(s_index)
            # scio.savemat(path_SaveData+'FDI/centroid_index_%s%s%s.mat'%(bu_length_lack, saveName, i),
            #              {'centroid_index': s_index})

    centroid(times_file_all)
    # IVT(times_file_all)
    # longpath(times_file_all)


for bu_main_period_length in [4]:
    for bu_lack_period_length in [2]:
        times_file_all = np.array(os.listdir(path_SaveData + '%sAR_contour_%s/' % (
        file_contour_Data, '%i%i' % (bu_main_period_length, bu_lack_period_length))))
        basis_analysis('%i%i' % (bu_main_period_length, bu_lack_period_length), times_file_all)
