import os
# import mat73
import matplotlib.pyplot as plt

import nc4
import scipy.io as scio
import pandas as pd
import numpy as np
import xarray as xr


wrong_file = []
def loadmat(path):
    import mat73
    import scipy.io as scio
    try:
        data = mat73.loadmat(path)
    except:
        data = scio.loadmat(path)
    return data

def create_params_for_pw_eul(time):
    """
    :param time: pd.datetime
    :return:
    """

    dt_int=6 * 3600 # time interval of the time interpolation
    t_total= 15 * 24 * 3600 # the total time length of each trajectory. Note: not exceeding the maximal tracking time

    params = {
        't_int': np.arange(0, t_total+0.01, dt_int) # time vector
    }

    path_info = {
        'Inputinfo': r"/home/linhaozhong/work/Global_era5/3/Global-1deg_Inputinfo.mat",
        'data': r"/home/linhaozhong/work/Global_era5/3/output_Global-1deg/%s_1-level-int_trajN.mat"%time,
        'area_S': r"/home/linhaozhong/work/Global_era5/3/GridAL_ERA1degGridC.mat"
    }
    return [params, path_info]

def get_pw_eul(params, path_info, mask_sta=None):
    t_int = params['t_int']
    area_S = loadmat(path_info['area_S'])['S']
    inputinfo = loadmat(path_info['Inputinfo'])
    lon_H_sta = inputinfo['lon_H'][0,:]
    lat_H_sta = inputinfo['lat_H'][:,0]
    ### get subregion idx in row region
    seedidx_row = inputinfo['seedidx']
    seedidx = inputinfo['seedidx']-1

    try:
        try:
            data = loadmat(path_info['data'])
        except:
            data = loadmat(path_info['data'])
    except:
        wrong_file.append(path_info['data'])
        return '0'
    if mask_sta is not None:
        seedidx = np.argwhere(mask_sta>0)
        seedidx_index = (seedidx_row[:, None] == seedidx+1).all(-1).any(-1)
        for ikey in data.keys():
            if ikey[:1] == '_':
                continue

            ivalue = data[ikey]
            try:
                if ivalue.shape[-1] == seedidx_row.shape[0]:
                    data[ikey] = np.array(ivalue[..., seedidx_index], dtype='double')
            except:
                continue

    traj_pw = data['traj_PW']
    traj_rr = data['traj_rr']
    p_lonlat = data['plonlat']
    tlev_iter = np.squeeze(data['tlev_iter'])
    tlev_iter = np.array([int(i) for i in tlev_iter])
    traj_time = data['traj_time']

    # get PWm_all
    pw_all_area = 0
    pw_all = 0
    for i_traj in range(traj_pw.shape[-1]):

        pw_all_area += traj_pw[0, i_traj] * area_S[seedidx[i_traj,0],seedidx[i_traj,1]]
        pw_all += traj_pw[0, i_traj]


    new_row = traj_rr[-1,:]
    traj_rr = np.vstack((traj_rr, new_row))

    traj_rho = np.zeros(traj_rr.shape)
    for i_traj in range(traj_rho.shape[-1]):
        traj_rr[tlev_iter-1, i_traj] = traj_rr[tlev_iter-2, i_traj]
        traj_rho[:, i_traj] = traj_rr[:, i_traj] * area_S[seedidx[i_traj,0],seedidx[i_traj,1]] * traj_pw[0, i_traj]/pw_all_area

    pw_eul = np.zeros(area_S.shape)
    rho_loc = np.zeros([len(t_int), traj_pw.shape[-1]])
    pwm_loc = np.zeros(rho_loc.shape)
    pos_int = np.zeros([2,rho_loc.shape[0], rho_loc.shape[1]])
    from scipy import interpolate
    rho_int = np.ones(rho_loc.shape)
    for i_traj in range(traj_pw.shape[-1]):
        t_lev = int(tlev_iter[i_traj])

        pos_org = np.squeeze(p_lonlat[:,0:t_lev, i_traj])
        func = interpolate.interp1d(traj_time[:t_lev, i_traj], np.squeeze(pos_org[0]),kind='nearest')
        pos_int[0, :, i_traj] = func(t_int)
        func = interpolate.interp1d(traj_time[:t_lev, i_traj], np.squeeze(pos_org[1]),kind='nearest')
        pos_int[1, :, i_traj] = func(t_int)

        traj_rho_squeeze = np.squeeze(traj_rho[0:t_lev, i_traj])
        func = interpolate.interp1d(traj_time[:t_lev, i_traj], traj_rho_squeeze,kind='nearest')
        rho_int[:, i_traj] = func(t_int)

        rho_loc[0, i_traj] = rho_int[0,i_traj]
        rho_loc[1:, i_traj] = np.diff(rho_int[:, i_traj])

        pwm_loc[:, i_traj] = pw_all*rho_loc[:, i_traj]

        # print('################################')
        for i_date in range(rho_loc.shape[0]):
            lon_now = pos_int[0,i_date, i_traj]
            lat_now = pos_int[1, i_date, i_traj]

            index = [0, 0]
            index[1] = np.argmin(abs(lon_H_sta - lon_now))
            index[0] = np.argmin(abs(lat_H_sta - lat_now))
            pw_eul[index[0], index[1]] += pwm_loc[i_date, i_traj]
    return pw_eul



time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'
path_pw_eulSaveData = r'/home/linhaozhong/work/AR_NP85/pw_eul/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''

resolution = 0.5


def main(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_MainData + 'AR_contour_42/%s' % (time_file_new))

        time_all = info_AR_row.time_all.values

        for i_time_row in time_all:
            i_time = pd.to_datetime(i_time_row)
            ar_contour = info_AR_row['contour_all'].sel(time_all=i_time_row).values

            # 0.5*0.5 to 1*1
            ar_contour = ar_contour[::2, ::2][:-1,:]
            for i_time_allyear in range(1979,2019+1):
                """
                ############################
                ############################
                #############################
                # check create_params_for_pw_eul
                #########
                """
                i_time_year = str(i_time_allyear)+i_time.strftime('%m%d')
                save_path = path_pw_eulSaveData+time_file+'/cli/%s/'%i_time.strftime('%m%d')
                if os.path.exists(save_path+'/%s.nc4'%i_time_year):
                    print(i_time_year, 'exist')
                    continue
                params, path_info = create_params_for_pw_eul(i_time_year)
                pw_eul = get_pw_eul(params,path_info,ar_contour)
                if not isinstance(pw_eul, str):
                    os.makedirs(save_path, exist_ok=True)
                    nc4.save_nc4(pw_eul, path_pw_eulSaveData+time_file+'/cli/%s/%s'%(i_time.strftime('%m%d'),i_time_year))
                    if str(i_time_allyear) == i_time.strftime('%Y'):

                        nc4.save_nc4(pw_eul, path_pw_eulSaveData + time_file + '/%s' % i_time_year)


    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            if (i != '1')| (i != '7'):
                times_file_all = df[i].values
                for time_file in times_file_all:
                    if len(str(time_file)) < 5:
                        continue
                    contour_cal(time_file, var)

def main_composite(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_MainData + 'AR_contour_42/%s' % (time_file_new))
        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        time_use = int(len(time_all[AR_loc:]) / 2)
        time_use = [time_all[AR_loc], time_all[AR_loc + time_use], time_all[-1]]

        for i_time_row in [time_use[phase]]:
            i_time = pd.to_datetime(i_time_row)

            path_now = path_pw_eulSaveData + time_file + '/%s' % i_time.strftime('%Y%m%d')
            if not os.path.exists(path_now+'.nc4'):
                return 'no'
            now = nc4.read_nc4(path_now)

            cli = []
            lack_num = 0
            for i_time_allyear in range(1979,2019+1):
                i_time_year = str(i_time_allyear) + i_time.strftime('%m%d')
                path_cli = path_pw_eulSaveData + time_file + '/cli/%s/%s' % (i_time.strftime('%m%d'), i_time_year)
                if not os.path.exists(path_cli + '.nc4'):
                    lack_num += 1

                    if lack_num > 3:
                        return 'no'
                    continue
                cli.append(nc4.read_nc4(path_cli))

            return now - np.nanmean(np.array(cli))


    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            if (i != '1') | (i != '7'):
                print(i)
                composite_result_z = []

                times_file_all = df[i].values
                for time_file in times_file_all:
                    if len(str(time_file)) < 5:
                        continue

                    a_all = contour_cal(time_file, i)
                    if isinstance(a_all, str):
                        print(time_file)
                        continue
                    composite_result_z.append(a_all)

                # centroid = np.array(centroid, dtype='double')
                # from function_shared import roll_longitude_from_359to_negative_180
                # if (i=='0') | (i=='2'):
                #     centroid[:, 0] = roll_longitude_from_359to_negative_180(centroid[:, 0])
                # centroid = np.mean(centroid, axis=0)
                composite_result_z = np.array(composite_result_z, dtype='double')
                composite_result_z = np.nanmean(composite_result_z, axis=0)

                # print(composite_result.shape)
                scio.savemat(path_saveData + 'mr_%s_%i.mat' % (i, phase),
                             {'data': composite_result_z})
                # nc4.save_nc4(composite_result,path_saveData+'composite_%s'%i)


path_saveData = r'/home/linhaozhong/work/AR_NP85/moisture_resources/'
phase = 2
main('time')
main_composite('time')

print(wrong_file)
exit()
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr



def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1, colors_shape[0]])
    return ListedColormap(np.concatenate((colors, ones.T), axis=1))
cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')
var_with_shortname = {'Hgt':'z', 'Tem':'t', 'Uwnd':'u', 'Vwnd':'v', 'RH':'r'}
central_lon = [320,300,180,180,0,320]
import nc4
import scipy.io as scio

long = np.arange(0.5, 360, 1)
lat = np.arange(89.5, -90, -1)

import matplotlib

SMALL_SIZE = 6

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

fig = plt.figure(figsize=[6.5,5])
plt.subplots_adjust(top=0.95,
bottom=0.08,
left=0.035,
right=0.99,
hspace=0.2,
wspace=0.2)
figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

type_name_row = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE','BAF','BSE' , 'BSW', 'MED', 'ARC']
type_name_num = ['0','2','3','4','5','6']

subplot_number = 1
for i in [0,1,2,3,4,5]:

    # centroid = a['centroid']
    # long_c, lat_c = centroid[0][:2]
    # print(centroid)
    # long = np.arange(long_c-45, long_c+45,0.5)
    # lat = np.arange(lat_c-40, lat_c+40, 0.5)

    def plot_quiver():
        plt.rcParams['hatch.color'] = 'darkred'
        ax = fig.add_subplot(3,4, subplot_number,  projection=ccrs.Orthographic(central_lon[i], 45))

        # i_contour_cut, z, z_ano= data
        z = data1

        import cmaps
        import matplotlib.colors as colors
        cb = ax.contourf(long, lat,
                         np.array((z),dtype='double'),levels=np.arange(0,3.5,0.2),extend='max',cmap=cmpas_zhong,
                        transform=ccrs.PlateCarree())
        # plt.colorbar(cb, orientation="horizontal")
        # ax.quiver(cross.longitude.values[::delta], cross.latitude.values[::delta_lat],
        #           np.array(u)[::delta_lat,::delta], np.array(v)[::delta_lat,::delta],
        #           transform=ccrs.PlateCarree(),scale=0.1, zorder=7)
        import cmaps
        # cb = ax.contourf(long, lat,
        #                  uv_in_AR(u, v, i_contour_cut), extend='both', levels=np.arange(0, 15.1, 0.5),
        #                  transform=ccrs.PlateCarree(), cmap=cmaps.cmocean_matter, zorder=3)
        # plt.colorbar(cb)
        # ax.quiver(long[::delta], lat[::delta_lat],
        #           u[::delta_lat, ::delta], v[::delta_lat, ::delta],
        #           transform=ccrs.PlateCarree(), scale=200, zorder=7)
        plt.title(figlabelstr[subplot_number-1]+plot_name[i]+' phase %i'%(phase+1), fontdict={'size':SMALL_SIZE+1})
        ax.coastlines(zorder=9)
        ax.gridlines()
        # ax.set_extent()
        return cb

    phase = 0
    a= scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
    data1 =a['data']
    plot_quiver()
    subplot_number += 1
    phase = 1
    a= scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\combine_result\mr_%s_%i.mat"%(type_name_num[i], phase))
    data1 =a['data']
    cb = plot_quiver()
    subplot_number += 1
    # plt.show()
cb_ax = fig.add_axes([0.25, 0.05, 0.5, 0.01])
cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
cb_ax.set_xlabel('mm/day')
plt.savefig('figure_7.png',dpi=500)











