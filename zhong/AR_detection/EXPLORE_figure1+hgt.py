import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
2D characteristics combine analysis of IVT, T2m anomaly, circulation.
get all AR characteristics from type_filename_SON.csv
while this file will provided the clustering result of AR and the corresponding AR fileAddress
"""
from scipy import stats


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total


def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata + '%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var + '/' + s_time_str)
    cli = []
    for i_year in range(1979, 2020 + 1):
        s_time_str = var + '_cli/' + str(i_year) + '/' + str(i_year) + s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now - cli


def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_MainData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir == '':
        var_dir = var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly


#                 s_i = get_circulation_data_based_on_date(pd.to_datetime(time_AR[iI_time]),
#                                                          'WaterVapor',
#                                                          'tcwv', var_dir='Total_column_water_vapour')


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'


def get_circulation_data_based_on_pressure_date(date, var, var_ShortName, level=[1000, 850, 500, 200]):
    """date containing Hour information"""
    import scipy
    time = pd.to_datetime(date)
    # load hourly anomaly data
    def load_anomaly(type):
        if type == 'daily':
            data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m%d'))[
                               time.strftime('%m%d')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly':
            data_anomaly = scipy.io.loadmat(path_PressureData + '%s/anomaly/' % var + '%s.mat' % time.strftime('%m'))[
                               time.strftime('%m')][:, int(time.strftime('%H')), :, :]
        elif type == 'monthly_now':

            def monthly_mean(var_dir, var_save, var_ShortName, time):
                path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir

                if os.path.exists(path_SaveData + '%s.mat' % time.strftime('%Y%m')):
                    return scio.loadmat(path_SaveData + '%s.mat' % time.strftime('%Y%m'))[time.strftime('%Y%m')]

                else:
                    date_montly_now = pd.date_range(time.strftime('%Y-%m-') + '01', freq='1D', periods=31)
                    dates = date_montly_now[date_montly_now.month == int(time.strftime('%m'))].strftime('%Y%m%d')
                    year = dates[0][:4]

                    def get_data(var_dir, i_Year, var_save, i_MonDay):
                        s_data = xr.open_dataset(
                            path_PressureData + '%s/%s/%s.%s%s.nc' % (var_dir, i_Year, var_save, i_Year, i_MonDay))
                        s_data = s_data.sel(level=level)
                        s_data = s_data[var_ShortName].values
                        return np.nanmean(s_data, axis=0)
                    data_in_MonDay = []

                    for iI_MonDay, i_MonDay in enumerate(dates):
                        # work for 0229
                        print(path_MainData + '%s/%s/%s.%s%s.nc' % (var_dir, year, var_save, year, i_MonDay[4:]))
                        s_data = get_data(var_dir, year, var_save, i_MonDay[4:])
                        data_in_MonDay.append(s_data)

                    data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
                    data_in_MonDay = np.array(data_in_MonDay, dtype='double')

                    path_SaveData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/' + '%s/monthly_mean/' % var_dir
                    os.makedirs(path_SaveData, exist_ok=True)
                    scipy.io.savemat(path_SaveData + '%s.mat' % time.strftime('%Y%m'), {time.strftime('%Y%m'): data_in_MonDay})

                    return data_in_MonDay

            data_anomaly = monthly_mean('Hgt', 'Hgt', 'z', time)


        return data_anomaly

    data_anomaly = load_anomaly('daily')

    # load present data
    data_present = xr.open_dataset(
        path_PressureData + '%s/' % var + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'), level=level)[var_ShortName].values

    return [data_present, data_anomaly]


resolution = 0.5


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)


def contour_cal(time_file, var='IVT', trans_lon=False):

    def roll_contour(contour, centroid):
        """
        roll all AR variables to [0,0] position based on their centroids.
        :param contour: [N, single contour]
        :param centroid: [N, single centroid]
        :return:
        """
        for i in range(len(contour)):
            roll_lat = [int(centroid[i][0] / resolution)]
            roll_lon = [int((180 - centroid[i][1]) / resolution)]
            contour[i] = np.roll(contour[i], roll_lat, 0)
            contour[i] = np.roll(contour[i], roll_lon, 1)
        return contour



    # mat file always has some bugs with str type. skip whitespace
    time_file_new = ''
    for i in time_file:
        time_file_new += i
        if i == 'c':
            break

    info_AR_row = xr.open_dataset(path_SaveData + 'new1_AR_contour_42/%s' % (time_file_new))

    # get the shape characteristic
    if var == 'shape':
        shape_charac_name = ['width', 'longth', 'orientation']
        centroid_AR_row = info_AR_row.centroid_AR.values
        if trans_lon:
            centroid_AR = centroid_AR_row[:, 1]
            centroid_AR[(centroid_AR < 180) & (centroid_AR > 0)] = centroid_AR[
                                                                       (centroid_AR < 180) & (centroid_AR > 0)] + 360
            centroid_AR_row[:, 1] = centroid_AR
        shape_charac_AR = np.array([np.mean(i, axis=0) for i in np.array_split(centroid_AR_row, 3)])

        centroid_all_row = info_AR_row.centroid_all.values
        if trans_lon:
            centroid_all = centroid_all_row[:, 1]
            centroid_all[(centroid_all < 180) & (centroid_all > 0)] = centroid_all[(centroid_all < 180) & (
                        centroid_all > 0)] + 360
            centroid_all_row[:, 1] = centroid_all
        shape_charac_all = np.array([np.mean(i, axis=0) for i in np.array_split(centroid_all_row, 3)])

        # exit(0)
        # for i_shape_charac in shape_charac_name:
        #     isc = info_AR_row[i_shape_charac].values
        #     if i_shape_charac == 'orientation':
        #         isc[isc<0] = isc[isc<0]+180
        #     isc = np.array([np.mean(i) for i in np.array_split(isc, 3)])
        #     shape_charac = np.append(shape_charac, np.expand_dims(isc, axis=1), axis=1)

        return [shape_charac_AR, shape_charac_all]

    ####################################################################

    time_AR = info_AR_row.time_AR.values


    contour_IVT_AR = get_circulation_data_based_on_pressure_date(pd.to_datetime(time_AR[0]), var[0],
                                                                        var[1])

    # # roll circulation into centroid position for every levels
    # for i_level in range(4):
    #
    #     s_contour_IVT_AR = roll_contour([contour_IVT_AR[i_level]], [info_AR_row.centroid_AR.values[0]])
    #     info_AR_3phase.append(s_contour_IVT_AR[0])

    return contour_IVT_AR



path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_SaveData = r'/home/linhaozhong/work/AR_detection/'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
path_moisture_path = r'/home/linhaozhong/work/AR_detection/AR_moisture_path_42/'
path_savepath = r'/home/linhaozhong/work/AR_detection/'
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'


use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']


def main(var):
    for season in ['DJF']:
        df = pd.read_csv(path_savepath + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):

            times_file_all = df[i].values
            cir_now = []
            cir_avg = []
            trans_lon = False
            if (iI == 0) & (var == 'shape'):
                trans_lon = True


            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue
                s_cir_now, s_cir_avg = contour_cal(time_file, var, trans_lon)
                print(s_cir_avg.shape, s_cir_now.shape)
                cir_avg.append(s_cir_avg)
                cir_now.append(s_cir_now)
            cir_avg = np.array(cir_avg)
            cir_now = np.array(cir_now)

            ''' ==============================================================='''
            cir_Pvalue = np.empty(cir_now.shape)[0]
            for i_level in np.arange(cir_now.shape[1]):
                for i_lat in np.arange(cir_now.shape[2]):
                    for i_lon in np.arange(cir_now.shape[3]):
                        cir_Pvalue[i_level, i_lat, i_lon] = stats.ttest_ind(cir_avg[:,i_level, i_lat, i_lon], cir_now[:,i_level, i_lat, i_lon],equal_var=False)[1]


            cir_avg = np.nanmean(cir_avg, axis=0)
            cir_now = np.nanmean(cir_now, axis=0)



            data_save = np.array([cir_now, cir_avg, cir_Pvalue])


            if isinstance(var, list):
                nc4.save_nc4(data_save,
                             path_SaveData + 'circulation/AR_%s' % (var[0] + '_' + season + '_' + type_name[iI]))

            else:
                nc4.save_nc4(data_save,
                             path_SaveData + 'circulation/AR_%s' % (var + '_' + season + '_' + type_name[iI]))


# main('shape')
# main('IVT')
# main('t2m')
# main('tcwv')
# main('advection')
# main('adibatic')
vari_SaveName_all = ['Hgt']
vari_ShortName = ['z']
for i in range(len(vari_ShortName)):
    main([vari_SaveName_all[i], vari_ShortName[i]])