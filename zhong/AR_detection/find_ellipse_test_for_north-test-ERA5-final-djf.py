# threshold the image to reveal light regions in the
# blurred image

from geopy.distance import distance
import nc4
import pandas as pd
from scipy.ndimage import gaussian_filter
from fil_finder import FilFinder2D
import astropy.units as u
import numpy as np
from skimage import measure
import matplotlib.pyplot as plt
import cv2
import xarray as xr
import logging.handlers




def find_highlight(row_data, lat, lon, resolution, given_threshold, numll_min=10):
    '''
    :param data: np.array, 2D data.
    :param lat, lon, resolution: lat(list or array-like), lon(list or array-like) and resolution(float) of the data.
    :param given_threshold: float, the minimum value which can be marked as 'high_value'.
    :param numll_min: float, the points in one high_value region should be more than numll_min, otherwise, this region
                             will be filtered.

    :return: array, shape is [the number of high_value area, 7],
            the first five indexes are  x_centre, y_centre, width, height, angle,
            The sixth and seventh indices are area of fitted ellipse and high_value region.
            The eighth and ninth indices are averaged wind speed of fitted ellipse and high_value region
    '''

    data = row_data-given_threshold
    data = gaussian_filter(data, sigma=1)

    def cal_contour(data, roll=False):
        if roll != False:
            # print('df will not roll back !!!!')
            data = np.roll(data, roll, axis=1)
        thresh = cv2.threshold(data, 1, 255, cv2.THRESH_BINARY)[1]
        labels = measure.label(thresh, connectivity=1, background=0)

        Mask = np.zeros(data.shape, dtype='uint8')
        lineMask_M = np.zeros(data.shape, dtype='uint8')
        mask_ss = []
        lineMask = []
        if roll != False:
            num_AR = 1000
        else:
            num_AR = 0

        s_df_result = pd.DataFrame()

        for label in np.unique(labels):
            if label == 0:
                continue

            #######  prepare labelMask, labelMask is a narray contains one contour
            labelMask = np.zeros(thresh.shape, dtype="uint8")
            labelMask[labels == label] = 255
            numPixels = cv2.countNonZero(labelMask)

            ####### get index of this contour
            ar_pathway_index=np.argwhere(labelMask!=0)

            ####### numPixels , must in given latitude
            if roll != False:
                if (np.in1d(np.unique(ar_pathway_index[:,1]), [roll])).any() == False:
                    continue
            if (numPixels > bu_the_number_of_grids) & (np.in1d(IVT_lat[np.unique(ar_pathway_index[:,0])], bu_study_box).any()):
                simple = labelMask
                simple[simple==255] = 1

                ######## sketetons
                fil = FilFinder2D(simple, distance=1* u.pix, mask=simple)
                fil.preprocess_image(skip_flatten=True)
                fil.create_mask(border_masking=True, verbose=False,
                                use_existing_mask=True)
                fil.medskel(verbose=False)
                result = fil.analyze_skeletons(branch_thresh=8*u.pix, skel_thresh=1 * u.pix, prune_criteria='length')

                # print(fil.lengths())
                # print(fil.lengths()/np.sum(fil.branch_properties['length']))
                # # print(fil.branch_properties)
                # exit(0)
                # print(fil.curvature_branches())
                # plt.imshow(simple, cmap='gray')
                # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                # plt.axis('off')
                # plt.show()
                ###################  condition
                if (fil.branch_properties['number']>bu_branch_number) :
                    # print('kill by shape')
                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()
                    continue

                if ((fil.lengths()/np.sum(fil.branch_properties['length']))<bu_branch_ratio*u.pix):
                    # print('kill by ratio')
                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()
                    continue
                # plt.imshow(labelMask, cmap='gray')
                # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                # plt.axis('off')
                # plt.show()
                sslabels = measure.label(labelMask, connectivity=1, background=0)
                regions = measure.regionprops(sslabels,  intensity_image=row_data)
                length = regions[0].axis_major_length
                width = regions[0].axis_minor_length
                if length/width < 2:
                    # plt.imshow(labelMask)
                    # plt.title('kill by length/width')
                    # plt.show()
                    continue

                try:
                    fil.analyze_skeletons(branch_thresh=40*u.pix, skel_thresh=1 * u.pix, prune_criteria='length')

                except:
                    continue
                ll_loc = np.array(fil.end_pts)[0]
                lat_loc =IVT_lat[ll_loc[:,0]]
                lon_loc = IVT_long[ll_loc[:,1]]
                if (np.min(lat_loc)<bu_min_latitude):
                    yess = True
                else:
                    # plt.imshow(fil.skeleton_longpath)
                    # plt.title('kill by across 60°')
                    # plt.show()
                    continue

                length=distance((lat_loc[0], lon_loc[0]), (lat_loc[1], lon_loc[1])).km
                if length < 2000:
                    continue


                Mask = cv2.add(Mask, labelMask)
                lineMask_M = cv2.add(lineMask_M, np.array(fil.skeleton_longpath, dtype='uint8'))

                mask_ss.append(np.array(labelMask))
                lineMask.append(np.array(fil.skeleton_longpath, dtype="uint8"))

                num_AR +=1
                centroid = regions[0].centroid
                arr = np.ma.masked_where(labelMask==0, row_data)
                max_values = np.unravel_index(arr.argmax(), arr.shape)
                contours = measure.find_contours(labelMask)[0]
                s_df = pd.DataFrame()
                s_df['y'] = [contours[:,0]]
                s_df['x'] = [contours[:,1]]
                s_df['time'] = df_time
                s_df['num'] =num_AR
                s_df_result = s_df_result.append(s_df, ignore_index=True)

        if roll != False:
            thresh = np.roll(thresh, -roll, axis=1)
            lineMask_M = np.roll(lineMask_M, -roll, axis=1)
            Mask = np.roll(Mask, -roll, axis=1)

        return  num_AR, s_df_result,     mask_ss, lineMask,   thresh, lineMask_M


    num_AR, s_df_result,     mask_ss, lineMask,   thresh, lineMask_M = cal_contour(data)
    s_roll_number = int(data.shape[1]/2)

    num_AR_cross, s_df_result_cross, mask_ss_cross, lineMask_cross,thresh_cross, lineMask_M_cross = cal_contour(data, roll=s_roll_number)

    if (num_AR > 0) & (num_AR_cross > 1000):
        mask_ss.extend(mask_ss_cross)
        lineMask.extend(lineMask_cross)
        nc4.save_nc4(np.array(mask_ss), path_SaveData+'daily_AR/'+s_time+'_'+str(timestep))
        nc4.save_nc4(np.array(mask_ss), path_SaveData+'daily_AR/'+s_time+'_'+str(timestep))
    elif (num_AR > 0) & (num_AR_cross <= 1000):
        nc4.save_nc4(np.array(mask_ss), path_SaveData+'daily_AR/'+s_time+'_'+str(timestep))
        nc4.save_nc4(np.array(mask_ss), path_SaveData+'daily_AR/'+s_time+'_'+str(timestep))
    elif (num_AR <= 0) & (num_AR_cross > 1000):
        nc4.save_nc4(np.array(mask_ss_cross), path_SaveData+'daily_AR/'+s_time+'_'+str(timestep))
        nc4.save_nc4(np.array(mask_ss_cross), path_SaveData+'daily_AR/'+s_time+'_'+str(timestep))

    if num_AR_cross>1000:
        thresh = cv2.add(thresh, thresh_cross)
        lineMask_M = cv2.add(lineMask_M, lineMask_M_cross)

        s_df_result = s_df_result.append(s_df_result_cross, ignore_index=True)

        num_AR = num_AR+num_AR_cross-1000

    if num_AR != 0:
        fig, ax = plt.subplots(ncols=2, nrows=1, figsize=(5,1.8))
        plt.subplots_adjust(top=0.96,
                            bottom=0.11,
                            left=0.01,
                            right=0.99,
                            hspace=0.2,
                            wspace=0.17)
        ax[0].imshow(thresh, vmin=0)
        ax[1].imshow(lineMask_M)
        plt.title(s_time+' '+str(timestep))
        ax[0].axhline(40, linewidth=0.4)
        ax[0].axhline(60, linewidth=0.4)
        # ax[2].imshow(lineMask)
        # plt.show()
        plt.savefig(path_SaveData+'pic/'+s_time+'_'+str(timestep)+'.png', dpi=500)
        plt.close()
        return s_df_result


bu_branch_ratio = 0.7
bu_min_latitude = 60
bu_study_box = [70,60]
bu_branch_number = 5
bu_the_number_of_grids = 400 # 800





time_loop = pd.date_range('1983-05-18', '2020-12-31', freq='1D')

time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]
time_Seaon_divide_Name = ['winter', 'spring', 'summer', 'fall']
# path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5x0.5/'
path_MainData = r'/zhong/AR_detection\data\\'
path_SaveData = r'/zhong/AR_detection\\'
# path_SaveData = r'/home/linhaozhong/work/AR_detection/'




for sI_Season, s_Season in enumerate(time_Seaon_divide):
    if sI_Season == 0:
        continue
    s_time_loop = time_loop[np.in1d(time_loop.month, s_Season)]

    # dual_threshold = nc4.read_nc4(r'/home/linhaozhong/work/AR_detection/quantile_field')[sI_Season]
    dual_threshold = nc4.read_nc4(r'quantile_field')[sI_Season]
    for s_time in s_time_loop.strftime('%Y%m%d'):
        print(s_time)
        index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time))
        index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time))
        # index_vivt = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data/NorthWater.%s.nc'%( s_time))
        # index_uivt = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data/EastWaterVapor.%s.nc'%(s_time))

        for s_time_daily in range(4):
            timestep = (s_time_daily+1)*6-1
            df_time = index_vivt['p72.162'].isel(time=timestep).time.values

            ivt_north = index_vivt['p72.162'].values[timestep]
            ivt_east = index_uivt['p71.162'].values[timestep]

            ivt_total = np.sqrt(ivt_east**2+ivt_north**2)

            slong = np.arange(0,180, 0.5)
            IVT_long = np.append(slong, np.arange(-180,0, 0.5))

            IVT_lat = index_uivt.latitude.values

            resolution_long=0.5

            resolution_lat = 0.5
            # print(s_time)
            # nc4.save_nc4(ivt_total-dual_threshold, 'test_data')
            # # plt.imshow(ivt_total)
            # plt.imshow(ivt_total-dual_threshold, vmin=0)
            # plt.imshow(np.ma.masked_less(ivt_total-dual_threshold,0))
            # plt.show()
            # continue

            s_df_result = find_highlight(ivt_total, IVT_lat, IVT_long, 0.5, dual_threshold, numll_min=400)
            df_result = df_result.append(s_df_result)
            # print('next')
    df_result.to_csv(path_SaveData+'df_%i.csv'%time_Seaon_divide_Name[sI_Season])
