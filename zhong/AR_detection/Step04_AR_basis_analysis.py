import scipy.io

import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps


def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
    return ivt_total


time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

path_MainData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5x0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'

# dual_threshold = nc4.read_nc4(path_SaveData+'quantile_field_t2m')[:, ::4, ::4]

# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\\'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
def basis_analysis(bu_length_lack, times_file_all, saveName=''):

    def yearly_frequency(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)
        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['freq'] = np.ones(df.shape[0])
        a= df['freq'].groupby([df['season'], df['year']]).sum()
        return a

    def yearly_duration(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)
        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]

        num = []
        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s/%s'%(bu_length_lack, s_time_all))

            num.append(len(info_AR.time.values))
        df['duration'] = num
        a= df['duration'].groupby([df['season'], df['year']]).mean()/4
        return a

    def yearly_duration_across_70(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)
        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]

        num = []
        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s/%s'%(bu_length_lack, s_time_all))
            info_AR = info_AR.sel(lat=70)
            s_num = 0
            for s_contour in info_AR['vari'].values:
                if (s_contour>0).any():
                    s_num+=1
            num.append(s_num)
        df['duration_70'] = num
        a= df['duration_70'].groupby([df['season'], df['year']]).mean()/4
        return a

    def yearly_frequency_along_70_2D(times_file_all, years):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):
            for sI_time, s_time_all in enumerate(time_file):
                info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s/%s'%(bu_length_lack, s_time_all))
                info_AR = info_AR.sel(lat=70)

                for s_contour in info_AR['vari'].values:
                    try:
                        s_num += s_contour
                    except:
                        s_num = s_contour
            return s_num
        frequency_2D= df['file'].groupby([df['season'], df['year']]).apply(lambda x: contour_cal(x)).reset_index()
        def plot_frequency_2D(frequency_2D, season):
            s_frequency_2D = frequency_2D[frequency_2D['season']==season]
            data = np.empty([len(years), 720])
            for si, i  in enumerate(years):
                try:
                    data[si] = s_frequency_2D[s_frequency_2D['year']==i]['file'].values[0]
                except:
                    continue
            return data
        data = []
        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            data.append(plot_frequency_2D(frequency_2D, i))
        data = np.array(data)
        nc4.save_nc4(data, path_SaveData+'basis_analysis/frequency_2D_%s%s.csv'%(bu_length_lack, saveName))

    def IVT_intensity(times_file_all):
        bu_vari = 'IVT'
        tem_index_mean_all = []
        tem_index_max_all = []
        tem_index_mean_70 = []
        tem_index_max_70 = []

        time_all = []

        for sI_time, s_time_all in enumerate(times_file_all):

            info_AR = xr.open_dataset(path_SaveData+'AR_contour_%s/%s'%(bu_length_lack, s_time_all))

            s_time_loop = info_AR.time.values

            s_tem_index_mean_all = []
            s_tem_index_max_all = []
            s_tem_index_mean_70 = []
            s_tem_index_max_70 = []

            for s_time in s_time_loop:

                s_time = pd.to_datetime(s_time)

                ivt_total = load_ivt_total(s_time)
                charater_AR = info_AR.sel(time=s_time)['vari'].values

                tem_index_used_grid = np.ma.masked_where(charater_AR==0, ivt_total)

                s_tem_index_mean_all.append(np.nanmean(tem_index_used_grid))
                s_tem_index_max_all.append(np.nanmax(tem_index_used_grid))

                tem_index_used_grid[info_AR['lat'].values<70, :] = np.nan

                s_tem_index_mean_70.append(np.nanmean(tem_index_used_grid))
                s_tem_index_max_70.append(np.nanmax(tem_index_used_grid))

            tem_index_mean_all.append('%'.join(str(i) for i in s_tem_index_mean_all))
            tem_index_max_all.append('%'.join(str(i) for i in s_tem_index_max_all))
            tem_index_mean_70.append('%'.join(str(i) for i in s_tem_index_mean_70))
            tem_index_max_70.append('%'.join(str(i) for i in s_tem_index_max_70))
            time_all.append(s_time_all[:-3])

        df  = pd.DataFrame()
        df['%s_mean'%bu_vari] = tem_index_mean_all
        df['%s_max'%bu_vari] = tem_index_max_all
        df['%s_mean_70'%bu_vari] = tem_index_mean_70
        df['%s_max_70'%bu_vari] = tem_index_max_70
        df['time'] = time_all
        df.to_csv(path_SaveData+'basis_analysis/%s_index_%s%s.csv'%(bu_vari, bu_length_lack, saveName))

    df = yearly_frequency(times_file_all)
    df1 = yearly_duration(times_file_all)
    df2 = yearly_duration_across_70(times_file_all)
    df = pd.concat([df, df1, df2], axis=1)
    df.to_csv(path_SaveData+'basis_analysis/basis_analysis_%s%s.csv'%(bu_length_lack, saveName))
    yearly_frequency_along_70_2D(times_file_all, np.arange(1979,2021))
    IVT_intensity(times_file_all)

for bu_main_period_length in [4]:
    for bu_lack_period_length in [2]:
        for impact_area in np.arange(1,7):
            data = pd.read_csv(path_SaveData+'impact_area.csv', index_col=0)
            times_file_all = data[data['impact_area']==impact_area]['fileName'].values
        # times_file_all = np.array(os.listdir(path_SaveData+'AR_contour_%s/'%('%i%i'%(bu_main_period_length, bu_lack_period_length))))
            basis_analysis('%i%i'%(bu_main_period_length, bu_lack_period_length), times_file_all, impact_area)

