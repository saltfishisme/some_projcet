import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
"""
get every characteristics based on file name and divided into seasonal clustering.
"""
class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps
        self.ax = ax
        self.crs = ccrs.PlateCarree()
        self.cmaps = ''
        self.colorbar=True
        self.orientation='h'
        self.ranges = ranges
        self.fmt_contourf='%.1f'
        self.fmt_contour='%.1f'
        self.ranges_reconstrcution_button_contourf = False
        self.ranges_reconstrcution_button_contour = False
        self.linewidths=1
        self.clabel_size=5
        self.cmaps=cmaps.WhiteGreen
        self.colorbar=True
        self.orientation='h'
        self.spacewind=[1,1]
        self.colors=False
        self.print_min_max=False
        self.contour_arg={'colors':'black'}
        self.contourf_arg={}
        self.quiverkey_arg={'X':0.85, 'Y':1.05, 'U':10, 'label': r'$10 \frac{m}{s}$'}
        self.contour_clabel_color = 'white'
        self.MidPointNorm = False
        def del_ll(range, lat, lon):
            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]
            del_area = ll_del(lat, lon, range[0])
            # 人为重新排序
            if del_area[0] > del_area[1]:
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1]+1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area
        if self.ranges[0] is not False:
            self.lat,  self.lon, self.del_area=del_ll(ranges, lat, lon)
        else:
            self.lat,  self.lon, self.del_area = [lat, lon, False]

    def del_data(self, data):
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1]+1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        def ranges_create(begin, end, inter):
            class MidPointNorm(Normalize):
                def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                    Normalize.__init__(self, vmin, vmax, clip)
                    self.midpoint = midpoint

                def __call__(self, value, clip=None):
                    if clip is None:
                        clip = self.clip

                    result, is_scalar = self.process_value(value)

                    self.autoscale_None(result)
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if not (vmin < midpoint < vmax):
                        raise ValueError("midpoint must be between maxvalue and minvalue.")
                    elif vmin == vmax:
                        result.fill(0)  # Or should it be all masked? Or 0.5?
                    elif vmin > vmax:
                        raise ValueError("maxvalue must be bigger than minvalue")
                    else:
                        vmin = float(vmin)
                        vmax = float(vmax)
                        if clip:
                            mask = ma.getmask(result)
                            result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                              mask=mask)

                        # ma division is very slow; we can take a shortcut
                        resdat = result.data

                        # First scale to -1 to 1 range, than to from 0 to 1.
                        resdat -= midpoint
                        resdat[resdat > 0] /= abs(vmax - midpoint)
                        resdat[resdat < 0] /= abs(vmin - midpoint)

                        resdat /= 2.
                        resdat += 0.5
                        result = ma.array(resdat, mask=result.mask, copy=False)

                    if is_scalar:
                        result = result[0]
                    return result

                def inverse(self, value):
                    if not self.scaled():
                        raise ValueError("Not invertible until scaled")
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if cbook.iterable(value):
                        val = ma.asarray(value)
                        val = 2 * (val - 0.5)
                        val[val > 0] *= abs(vmax - midpoint)
                        val[val < 0] *= abs(vmin - midpoint)
                        val += midpoint
                        return val
                    else:
                        val = 2 * (value - 0.5)
                        if val < 0:
                            return val * abs(vmin - midpoint) + midpoint
                        else:
                            return val * abs(vmax - midpoint) + midpoint
            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0+inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if isinstance(ranges_reconstrcution_button, list)  or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button
        else:

            ranges_min = np.nanmin(data); ranges_max = np.nanmax(data)
            if ranges_reconstrcution_button is False:
                if ranges_min>0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                ticks = (ranges_max-ranges_min)/30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

        if self.MidPointNorm is not False:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', norm=self.MidPointNorm, **self.contourf_arg)

        else:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', **self.contourf_arg)



        if self.colorbar:
            if self.orientation == 'h':
                orientation='horizontal'
            else:
                orientation='vertical'
            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.fmt_contourf)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=5, transform=self.crs, **self.contour_arg)
        cbs = self.ax.clabel(
            cb,  fontsize=self.clabel_size,# Typically best results when labelling line contours.
            colors=['black'],
            inline=True,  # Cut the line where the label will be placed.
            fmt=self.fmt_contour,  # Labes as integers, with some extra space.
        )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(path_singleLevelData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(path_singleLevelData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
    return ivt_total

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(path_singleLevelData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(path_singleLevelData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
    return ivt_total


time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

path_singleLevelData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
file_contour_Data = ''
bu_addition_Name = ''
# dual_threshold = nc4.read_nc4(path_SaveData+'quantile_field_t2m')[:, ::4, ::4]

# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\\'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
def basis_analysis(bu_length_lack, times_file_all, saveName=''):

    def duration(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):
            print(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row.sel(lat=70)['contour_AR'].values
            info_AR[info_AR!=0] = len(info_AR_row['time_AR'].values)

            return info_AR

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            s_data = []
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                s_data.extend(contour_cal(time_file))

            s_data = np.array(s_data)
            nc4.save_nc4(s_data, path_SaveData+'FDI/duration_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            nc4.save_nc4(s_index, path_SaveData+'FDI/duration_index_%s%s%s'%(bu_length_lack, saveName, i))


    def centroid(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):
            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row['centroid_AR'].values
            near_70_centroid = np.argmin(info_AR[:, 0]-70)

            down_lat = 0
            if info_AR[0,0]-info_AR[-1,0]>0:
                down_lat=1
            final = list(info_AR[near_70_centroid, :])+\
                    [info_AR_row['orientation'].values[near_70_centroid]]+\
                    [info_AR_row['width'].values[near_70_centroid]]+ \
                    [info_AR_row['longth'].values[near_70_centroid]]+ \
                    [down_lat]

            return final

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            s_data = []
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                s_data.append(contour_cal(time_file))

            s_data = np.array(s_data)
            nc4.save_nc4(s_data, path_SaveData+'FDI/centroid_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            scio.savemat(path_SaveData+'FDI/centroid_index_%s%s%s.mat'%(bu_length_lack, saveName, i),
                         {'centroid_index': s_index})

        ### test and plot
        # import matplotlib.pyplot as plt
        # import numpy as np
        # import nc4
        # import xarray as xr
        #
        # data = xr.open_dataset(r'1979022123_1.nc')
        #
        # a = data['contour_AR'].values
        # b = data['centroid_AR'].values
        #
        # for i, iss in enumerate(a):
        #     import matplotlib.pyplot as plt
        #     import cartopy.crs as ccrs
        #     fig, ax = plt.subplots(nrows=1, ncols=1, subplot_kw={'projection':ccrs.PlateCarree()})
        #     ax.contourf(np.arange(0,360,0.5), data.lat.values, iss, transform=ccrs.PlateCarree())
        #
        #     ax.scatter(b[i][1],b[i][0], s=1, transform=ccrs.PlateCarree())
        #     plt.show()

    def IVT(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):

            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row.sel(lat=70)['contour_AR'].values

            for sI, s_time in enumerate(info_AR_row['time_AR'].values):
                s_time = pd.to_datetime(s_time)
                ivt_total = load_ivt_total(s_time)
                info_AR[sI, info_AR[sI]!=0] = ivt_total[info_AR_row['lat'].values==70, info_AR[sI]!=0]

            return info_AR

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            s_data = []
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                s_data.extend(contour_cal(time_file))

            s_data = np.array(s_data)
            nc4.save_nc4(s_data, path_SaveData+'FDI/IVT_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            nc4.save_nc4(s_index, path_SaveData+'FDI/IVT_index_%s%s%s'%(bu_length_lack, saveName, i))

    def T2M(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):

            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row.sel(lat=70)['contour_AR'].values

            for sI, s_time in enumerate(info_AR_row['time_AR'].values):
                s_time = pd.to_datetime(s_time)
                ivt_total = load_ivt_total(s_time)
                info_AR[sI, info_AR[sI]!=0] = ivt_total[info_AR_row['lat'].values==70, info_AR[sI]!=0]

            return info_AR

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            s_data = []
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                s_data.extend(contour_cal(time_file))

            s_data = np.array(s_data)
            nc4.save_nc4(s_data, path_SaveData+'FDI/IVT_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            nc4.save_nc4(s_index, path_SaveData+'FDI/IVT_index_%s%s%s'%(bu_length_lack, saveName, i))

    def longpath(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):

            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row['longpath'].values[0]
            return info_AR

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            shape_longpath_all = pd.DataFrame()
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                lp = contour_cal(time_file)
                shape_longpath = pd.DataFrame()
                shape_longpath[time_file] = lp[0]
                shape_longpath[time_file+time_file] = lp[1]
                shape_longpath_all = pd.concat([shape_longpath_all, shape_longpath], ignore_index=True, axis=1)



            shape_longpath_all = shape_longpath_all.T.values
            shape_longpath = []
            for i_lp in range(0, shape_longpath_all.shape[0],2):
                shape_longpath.append([shape_longpath_all[i_lp], shape_longpath_all[i_lp+1]])



            print(np.array(shape_longpath).shape)
            nc4.save_nc4(np.array(shape_longpath), path_SaveData+'FDI/IP_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            scio.savemat(path_SaveData+'FDI/IP_index_%s%s%s.mat'%(bu_length_lack, saveName, i),
                         {'centroid_index': s_index})

    def IVT_2d(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):

            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row.sel(lat=70)['contour_AR'].values

            for sI, s_time in enumerate(info_AR_row['time_AR'].values):
                s_time = pd.to_datetime(s_time)
                ivt_total = load_ivt_total(s_time)


            return info_AR

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            s_data = []
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                s_data.extend(contour_cal(time_file))

            s_data = np.array(s_data)
            nc4.save_nc4(s_data, path_SaveData+'FDI/IVT_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            nc4.save_nc4(s_index, path_SaveData+'FDI/IVT_index_%s%s%s'%(bu_length_lack, saveName, i))


    def TEM_2d(times_file_all):
        df = pd.DataFrame()
        df['year'] = [i[:4] for i in times_file_all]
        df['month'] = [i[4:6] for i in times_file_all]
        df['day'] = [i[6:8] for i in times_file_all]
        df = df.astype(int)

        month_to_season_dct = {
            1: 'DJF', 2: 'DJF',
            3: 'MAM', 4: 'MAM', 5: 'MAM',
            6: 'JJA', 7: 'JJA', 8: 'JJA',
            9: 'SON', 10: 'SON', 11: 'SON',
            12: 'DJF'
        }
        df['season'] = [month_to_season_dct.get(i) for i in df['month']]
        df['file'] = times_file_all

        def contour_cal(time_file):

            info_AR_row = xr.open_dataset(path_SaveData+'%sAR_contour_%s/%s'%(file_contour_Data,bu_length_lack, time_file))
            info_AR = info_AR_row['longpath'].values[0]
            return info_AR

        for i in ['DJF', 'MAM', 'JJA', 'SON']:
            shape_longpath_all = pd.DataFrame()
            s_index = []
            files = df[df['season']==i]['file']
            for time_file in files:
                s_index.append(time_file)
                lp = contour_cal(time_file)
                shape_longpath = pd.DataFrame()
                shape_longpath[time_file] = lp[0]
                shape_longpath[time_file+time_file] = lp[1]
                shape_longpath_all = pd.concat([shape_longpath_all, shape_longpath], ignore_index=True, axis=1)



            shape_longpath_all = shape_longpath_all.T.values
            shape_longpath = []
            for i_lp in range(0, shape_longpath_all.shape[0],2):
                shape_longpath.append([shape_longpath_all[i_lp], shape_longpath_all[i_lp+1]])



            print(np.array(shape_longpath).shape)
            nc4.save_nc4(np.array(shape_longpath), path_SaveData+'FDI/IP_%s%s%s'%(bu_length_lack, saveName, i))
            s_index = np.array(s_index)
            scio.savemat(path_SaveData+'FDI/IP_index_%s%s%s.mat'%(bu_length_lack, saveName, i),
                         {'centroid_index': s_index})

    centroid(times_file_all)
    # IVT(times_file_all)
    longpath(times_file_all)

for bu_main_period_length in [4]:
    for bu_lack_period_length in [2]:

        times_file_all = np.array(os.listdir(path_SaveData+'%sAR_contour_%s/'%(file_contour_Data, '%i%i'%(bu_main_period_length, bu_lack_period_length))))
        basis_analysis('%i%i'%(bu_main_period_length, bu_lack_period_length), times_file_all)

