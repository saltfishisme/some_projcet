### Exectutalbe gernralised budget script
import time

import xarray as xr
import matplotlib.pyplot as plt

import numpy as np
import pickle
import datetime as dt

from dateutil.relativedelta import relativedelta
import copy
import pandas as pd
import os

# for i_file in os.listdir(r'G:/sic/'):
#     if '_2.hdf' in i_file:
#         continue
#     i_file_s = i_file.split('_')
#     os.rename(r'G:/sic/'+i_file, r'G:/sic_noaa/'+i_file_s[4]+'.hdf')

# dates = pd.date_range('1979-01-01', '2020-12-31', freq='1D').strftime('%Y%m%d')
# for i in dates:
#     if not os.path.exists(r'G:/sic_noaa/'+i+'.hdf'):
#         print(i)
#
# exit()


# dates = pd.date_range('2000-10-18', '2000-12-31', freq='1D').strftime('%m%d')
#
#
# for i_file in dates:
#     years = []
#     year_hear = range(1979,2020+1)
#     if i_file == '0229':
#         year_hear = range(1980,2020+1,4)
#     for i_year in year_hear:
#         print(r"G:\sic_noaa\%i%s.hdf"%(i_year, i_file))
#         try:
#             a = xr.open_dataset(r"G:\sic_noaa\%i%s.hdf"%(i_year, i_file))
#         except:
#             a = xr.open_dataset(r"G:\sic_noaa\%i%s.nc" % (i_year, i_file))
#         a = a['cdr_seaice_conc'].values[0]
#         a[a>1] = np.nan
#         years.append(a)
#     years = np.nanmean(years, axis=0)
#     import nc4
#     nc4.save_nc4(np.array(years), r"G:\sic/"+i_file)
#
# exit()

import os
import numpy as np
import pandas as pd
import xarray as xr


path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
import nc4
import scipy.io as scio
from datetime import timedelta


'''
prepare for esiv pdf
'''
"""prepare for esiv, load pioms grid and trans into stree"""
# import cv2
# resolution = 0.5
# long = np.arange(0, 360, 0.5)
# lat = np.arange(90, -90.01, -0.5)
# bu_addition_Name = ''
# path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
# path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
# path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
# path_PIOMS = r'/home/linhaozhong/work/Data/NDISI_SIC/sic_noaa/'
#
#
# pioms_grid = xr.open_dataset(path_PIOMS+r"19881205.hdf")
# def projection_transform_test(lon, lat, xy2ll=False):
#     lon = np.array(lon)
#     lat = np.array(lat)
#     shape_lon = lon.shape
#     shape_lat = lat.shape
#     lon = lon.flatten()
#     lat = lat.flatten()
#
#     proj_Out = {'proj': 'stere',
#                 'lat_0': 90, 'lat_ts':70, 'lon_0':-45, 'k':1, 'x_0':0, 'y_0':0,
#                 'a':6378273, 'b':6356889.449,
#                 'units':'m'}
#
#     from pyproj import Transformer
#     if xy2ll:
#         transproj = Transformer.from_crs(
#             proj_Out,
#             "EPSG:4326",
#             always_xy=True,
#         )
#     else:
#         from pyproj import Transformer
#         transproj = Transformer.from_crs(
#             "EPSG:4326",
#             proj_Out,
#             always_xy=True,
#         )
#
#     lon, lat = transproj.transform(lon, lat)
#     return np.reshape(lon, shape_lon), np.reshape(lat, shape_lat)
# points_lon = pioms_grid.xgrid.values
# points_lat = pioms_grid.ygrid.values
# lon_2d, lat_2d = np.meshgrid(points_lon, points_lat)
# lon_2d, lat_2d = projection_transform_test(lon_2d, lat_2d, xy2ll=True)
# points_lon = lon_2d.flatten()
# points_lat = lat_2d.flatten()
#
# from  function_shared_Main import roll_longitude_from_359to_negative_180
# points_lon = roll_longitude_from_359to_negative_180(points_lon, reverse=True)
#
# from function_shared_Main import cal_area_differentLL_1d
# weighted_PIOMS = cal_area_differentLL_1d(points_lon, points_lat)
#
# # transform into 0,1,2,3 relative coordination, only works in north hemisphere
# # !!!!!!!!!!!!!!!!!!!!!!!#
#
#
# points_lat_relaCoord = (90 - points_lat) / resolution
# points_lon_relaCoord = points_lon / resolution
#
# # to Stree
# from shapely.strtree import STRtree
# from shapely.geometry import Point
#
# points = [Point(x, y) for x, y in zip(points_lon_relaCoord, points_lat_relaCoord)]
# tree = STRtree(points)  # create a 'database' of points
#
#
# def load_esiv(s_time, contour):
#     def query_in(contour, tree):
#         # ================================================================================================ #
#         contour = np.array(contour, dtype='uint8')
#         thresh = cv2.threshold(contour, 0.1, 255, cv2.THRESH_BINARY)[1]
#         contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
#         from shapely.geometry import Polygon
#         res = []
#         for i_contour in contours:
#             i_contour = np.squeeze(i_contour)
#             if len(i_contour) < 5:
#                 continue
#             # trans to row
#             polygon = Polygon(i_contour)
#
#             # print('hi')
#             # import matplotlib.pyplot as plt
#             # # plt.plot(*polygon.exterior.xy)
#             # plt.plot(*polygon.exterior.xy)
#             # plt.show()
#             res_now = tree.query(polygon)
#             res.extend(res_now.tolist())
#         return res
#
#         # cv2.imshow('now', bitwiseand)
#         # cv2.waitKey(0)
#         # plt.show()
#
#     s_time = pd.to_datetime(s_time)
#
#     try:
#         pioms_ah = xr.open_dataset(path_PIOMS+s_time.strftime('%Y%m%d.hdf'))['cdr_seaice_conc'].values[0]
#     except:
#         pioms_ah = xr.open_dataset(path_PIOMS+s_time.strftime('%Y%m%d.nc'))['cdr_seaice_conc'].values[0]
#     pioms_ah[pioms_ah > 1] = np.nan
#
#     ah_anomaly = nc4.read_nc4(r'/home/linhaozhong/work/Data/NDISI_SIC/sic_anomaly/'+s_time.strftime('%m%d'))
#     pioms_ah_ano = pioms_ah - ah_anomaly
#     pioms_ah_ano = pioms_ah_ano.flatten()
#
#     if plot_type == 'pdf':
#         index = query_in(contour, tree)
#
#         weighted_PIOMS_contour = weighted_PIOMS[index]/np.nansum(weighted_PIOMS[index])
#         pioms_ah_ano = pioms_ah_ano[index]
#         # print(np.nansum(pioms_ah_ano*weighted_PIOMS_contour/np.nansum(weighted_PIOMS_contour)))
#         return np.nansum(pioms_ah_ano * weighted_PIOMS_contour)
#     else:
#         return [pioms_ah, ah_anomaly]
#
#
#
#
# time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
#
# path_MainData = r'/home/linhaozhong/work/AR_NP85/'
# path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
#
# resolution = 0.5
#
# path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
# path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# # path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'
#
# use_day_for_classification = 9
# max_ratio = 0.5
# type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
#              'MS', 'GE', 'useless2']
#
# plot_type = 'pdf'

# def main_3phase(var):
#     def cal_3phase(contour, phase_num=3):
#         time_len = np.arange(len(contour))
#         division = [contour[0]]
#         for i in np.arange(1, phase_num - 1):
#             percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
#             p_index = int(percentile)
#             p_weighted = percentile % 1
#             division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
#         division.append(contour[-1])
#         # division = np.array_split(contour, 3)
#         # division_mean = []
#         # for i in division:
#         #     division_mean.append(np.nanmean(i, axis=0))
#         return np.array(division)
#
#     def contour_cal(time_file, var='IVT'):
#
#         # mat file always has some bugs with str type. skip whitespace
#         time_file_new = ''
#         for i in time_file:
#             time_file_new += i
#             if i == 'c':
#                 break
#
#         info_AR_row = xr.open_dataset(path_SaveData + 'AR_contour_42/%s' % (time_file_new))
#
#         time_AR = info_AR_row.time_AR.values
#         time_all = info_AR_row.time_all.values
#         AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))
#
#         contour_all = info_AR_row.contour_all
#
#         next_one_time = time_AR[0]
#         next_one_contour = np.zeros(contour_all.values[AR_loc].shape)
#
#         var_all = []
#         # to daily resolution and create mat param for moisture track
#         for i_time_AR in time_all[AR_loc:]:
#             if pd.to_datetime(i_time_AR).strftime('%Y%m%d') == pd.to_datetime(next_one_time).strftime('%Y%m%d'):
#                 next_one_contour += contour_all.sel(time_all=i_time_AR).values
#             else:
#                 s_i = load_esiv(pd.to_datetime(next_one_time), next_one_contour)
#                 var_all.append(s_i)
#
#                 next_one_time = i_time_AR
#                 next_one_contour = contour_all.sel(time_all=i_time_AR).values
#
#         if not len(var_all) >= 2:
#             return 'no'
#         avg = []
#         for i_p in var_all[:2]:
#             avg.append(i_p[~np.isnan(i_p)].flatten())
#         return avg
#
#     for season in ['DJF']:
#         df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)
#
#         for iI, i in enumerate(df.columns):
#             times_file_all = df[i].values
#
#             phase1 = []
#             phase2 = []
#
#             for time_file in times_file_all:
#                 if len(str(time_file)) < 5:
#                     continue
#
#                 s_mean = contour_cal(time_file, var)
#                 if isinstance(s_mean, str):
#                     continue
#                 phase1.extend(s_mean[0])
#                 phase2.extend(s_mean[1])
#
#             phase = [phase1, phase2]
#             for iI_phase, i_phase in enumerate(phase):
#                 nc4.save_nc4(np.array(i_phase),
#                              path_SaveData + 'PDF_nsidc/all_%s' % (var + '%i_'%iI_phase + season + '_' + type_name[iI]))
#
# main_3phase('nsidc')
# exit()
"""
plot
"""

# path_SecondData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\PDF_nsidc/'
#
# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
# time_Seaon_divide = [[12, 1, 2]]
# time_Seaon_divide_Name = ['DJF']
# type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
#              'MS', 'GE', 'useless2']
# type_name_num = ['0','2','3','4','5','6']
# type_name = ['greenland_east', 'greenland_west','pacific_east', 'pacific_west',
#              'MS', 'GE']
# plot_name = ['GRE', 'BAF','BSE', 'BSW',
#              'MED', 'ARC']
#
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# infig_fontsize = 4
#
# resolution=0.5
# var_all = ['IVT', 'tcwv']
# var_all = ['nsidc', 'nsidc']
#
#
#
# SMALL_SIZE = 5
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
# plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
# plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
#
#
# for season in time_Seaon_divide_Name:
#
#     def get_data():
#         data_mean = []
#         for s_type_name in type_name:
#             s_data_mean = []
#
#             for var in var_all:
#
#                 s_data_mean.append(nc4.read_nc4(path_SecondData + 'all_%s%i_%s_%s' % (var, phase, season, s_type_name))*plot_param[var][5])
#
#             data_mean.append(s_data_mean)
#
#         return np.array(data_mean)
#
#     def plots(ax, data, bin_width):
#         percentile_label = ['Q1', 'Q2', 'Q3']
#         percentile = np.percentile(data, [25,50,75])
#         mean = np.nanmean(data)
#         HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
#         # ax.boxplot(data)
#
#         _, _, bar_container = ax.hist(data,bins=bin_width,density=True, lw=1,
#                                   ec="black", fc="black",zorder=2)
#
#         # yticks = ax.get_yticks()
#         # ax.set_yticks(yticks)
#         # ax.set_yticklabels((yticks*len(data)).astype('int'))
#
#         ylim_max = ax.get_ylim()[1]
#         trans = ax.get_xaxis_transform()
#         for i_percentile in range(3):
#             ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
#             ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
#                     transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
#                     bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
#             )
#             if plot_param[var][4] == 0:
#                 values =percentile[i_percentile].astype('int')
#             else:
#                 values = np.round(percentile[i_percentile], plot_param[var][4])
#
#             ax.text(percentile[i_percentile], 1.01, values, transform=trans,
#                     horizontalalignment='center', fontsize=infig_fontsize)
#
#         return mean
#
#
#     fig, axs = plt.subplots(6, 4, figsize=[7, 5])
#     plt.subplots_adjust(top=0.95,
# bottom=0.04,
# left=0.055,
# right=0.98,
# hspace=0.4,
# wspace=0.2)
#
#
#     for phase in [0,1]:
#
#         if phase == 0:
#             plot_param = {
#                 'IVT': [['IVTmean=\n', ' kg/(m*s)'], 'IVT', [-10, 250], [0, 0.03], 0, 1, 600],
#                 'tcwv': [['TCWVmean=\n', ' kg/(m**2)'], 'TCWVano', [-1.5, 8], [0, 0.6], 1, 1, 800],
#
#                 't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-3, 16], [0, 0.25], 2, 1, 800],
#                 'sic': [['SICmean=\n', ''], 'SICano', [-7, 5], [0, 0.5], 2, 100, 100],
#                 'nsidc': [['SICmean=\n', ''], 'SICano', [-20, 20], [0, 1], 2, 100, 100]
#             }
#         else:
#             plot_param = {
#                 'IVT': [['IVTmean=\n', ' kg/(m*s)'], 'IVT', [-10, 200], [0, 0.035], 0, 1, 200],
#                 'tcwv': [['TCWVmean=\n', ' kg/(m**2)'], 'TCWVano', [-1.5, 8], [0, 0.6], 1, 1, 150],
#
#                 't2m': [['T2Mmean=\n', ' K'], 'T2Mano', [-5, 20], [0, 0.25], 2, 1, 80],
#                 'sic': [['SICmean=\n', ''], 'SICano', [-10, 5], [0, 0.5], 2, 100, 100],
#                 'nsidc': [['SICmean=\n', ''], 'SICano', [-20, 20], [0, 1], 2, 100, 100]
#             }
#
#         frequency = get_data()
#
#         for i in range(6):
#             for j in range(2):
#
#                 # def flatten(l):
#                 #     l = np.array([item for sublist in l for item in sublist])
#                 #     return l[~np.isnan(l)]
#                 # bin_col = np.histogram(frequency[i][j], bins=plot_param[var_all[j]][6])[1]
#
#                 data = frequency[i][j]
#                 ax = axs[i][phase*2+j]
#                 var = var_all[j]
#                 mean = plots(ax, data, plot_param[var_all[j]][6])
#
#
#                 if (var != 'IVT') & (var != 'tcwv') :
#                     ax.axvline(0, color='black',zorder=0)
#                 ax.set_xlim(plot_param[var][2])
#                 ax.set_ylim(plot_param[var][3])
#
#
#                 mean_label = plot_param[var][0]
#                 if plot_param[var][4] == 0:
#                     mean = int(mean)
#                 else:
#                     mean = np.round(mean, plot_param[var][4])
#
#                 if var != 'sic':
#                     x = 0.85
#                 else:
#                     x=0.15
#                 ax.text(x, 0.6, mean_label[0]+str(mean)+mean_label[1],
#                         transform=ax.transAxes,horizontalalignment='center',fontsize=4)
#
#                 if (var == 'IVT') & (phase == 0) :
#                     axs[0][0].set_xlim([-10,500])
#                     axs[-1][0].set_xlim([-10,400])
#                 if (var == 'tcwv') & (phase == 0) :
#                     axs[0][1].set_xlim([-1.5,15])
#                 if (var == 'IVT') & (phase == 1) :
#                     axs[2][0].set_xlim([-10,300])
#             axs[i][0].set_ylabel(plot_name[i] + '\nProbability')
#
#
#
#
# for i_col in range(2):
#     var = var_all[i_col]
#     axs[0][i_col].set_title(plot_param[var][1]+' phase 1', pad=10)
#     axs[0][i_col+2].set_title(plot_param[var][1]+' phase 2', pad=10)
#
# for i_rows in range(6):
#     for j_col in range(4):
#         axs[i_rows][j_col].annotate(figlabelstr[4 * i_rows + j_col], [0.01, 1.01], xycoords='axes fraction')
#
# plt.show()
# plt.savefig('figure5_%s.png'%(var_all[0]), dpi=300)
# plt.close()

path_SecondData = r'G:\OneDrive\basis\some_projects\zhong\AR_detection\PDF_nsidc/'

path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'
time_Seaon_divide = [[12, 1, 2]]
time_Seaon_divide_Name = ['DJF']
type_name_row = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
                 'MS', 'GE', 'useless2']
type_name_num = ['0', '2', '3', '4', '5', '6']
type_name = ['greenland_east', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE']
plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
             'MED', 'ARC']

figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
               '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']

infig_fontsize = 4

resolution = 0.5
var_all = ['IVT', 'tcwv']
var_all = ['nsidc', 'nsidc']

SMALL_SIZE = 7
plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize

for season in time_Seaon_divide_Name:

    def get_data(phase):
        data_mean = []
        for s_type_name in type_name:
            s_data_mean = []

            for var in var_all:
                s_data_mean.append(
                    nc4.read_nc4(path_SecondData + 'all_%s%i_%s_%s' % (var, phase, season, s_type_name)))

            data_mean.append(s_data_mean)

        return np.array(data_mean)


    frequency = [get_data(0)[:,0],get_data(1)[:,0]]

    fig, ax = plt.subplots(1, figsize=[5, 4])
    plt.subplots_adjust(top=0.88,
                        bottom=0.295,
                        left=0.11,
                        right=0.9,
                        hspace=0.2,
                        wspace=0.2)

    labels = ['GRE', 'BAF', 'BSE', 'BSW',
             'MED', 'ARC']
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:pink']

    xtick = []
    xticklabels = []
    loc_box = [[1, 1.1, 1.2, 1.3,1.4,1.5], [1.9, 2.0, 2.1,2.2,2.3,2.4,2.5]]
    for iI_boxplot in range(2):
        print('^^^^^^^^^^^')
        for i_box in range(6):
            xtick.append(loc_box[iI_boxplot][i_box])
            xticklabels.append(labels[i_box])


            def reject_outliers(data, m=1.2):
                d = np.abs(data - np.median(data))
                mdev = np.median(d)
                s = d / mdev if mdev else 0.
                return data[s < m]
            y_here = frequency[iI_boxplot][i_box].flatten()*100
            # y_here = reject_outliers(y_here)
            y_here = y_here[y_here<5]
            # ax.boxplot(frequency[iI_boxplot][i_box].flatten(), positions=[loc_box[iI_boxplot][i_box]])
            ax.scatter(np.array(loc_box[iI_boxplot][i_box]).repeat(y_here.shape),
                       y_here
                       , c='black', s=0.1)
            print(labels[i_box], np.nanmean(y_here))
    ax.axhline(0, )
    ax.set_xticks(xtick)
    ax.set_xticklabels(xticklabels, rotation=45)
    ax.set_ylim([-20, 10])
    ax.set_ylabel('SIC anomaly')
    # plt.suptitle('M' + merge_type)
    plt.title('phase1                                                               phase2')
    plt.show()
    exit()














