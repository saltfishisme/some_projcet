import scipy.io

import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps
class circulation():
    def __init__(self, ax, lon, lat, ranges):
        import cartopy.crs as ccrs
        import cmaps
        self.ax = ax
        self.crs = ccrs.PlateCarree()
        self.cmaps = ''
        self.colorbar=True
        self.orientation='h'
        self.ranges = ranges
        self.fmt_contourf='%.1f'
        self.fmt_contour='%.1f'
        self.ranges_reconstrcution_button_contourf = False
        self.ranges_reconstrcution_button_contour = False
        self.linewidths=1
        self.clabel_size=5
        self.cmaps=cmaps.WhiteGreen
        self.colorbar=True
        self.orientation='h'
        self.spacewind=[1,1]
        self.colors=False
        self.print_min_max=False
        self.contour_arg={'colors':'black'}
        self.contourf_arg={}
        self.quiverkey_arg={'X':0.85, 'Y':1.05, 'U':10, 'label': r'$10 \frac{m}{s}$'}
        self.contour_clabel_color = 'white'
        self.MidPointNorm = False
        def del_ll(range, lat, lon):
            def ll_del(lat, lon, area):
                import numpy as np
                lat0 = lat - area[3]
                slat0 = int(np.argmin(abs(lat0)))
                lat1 = lat - area[2]
                slat1 = int(np.argmin(abs(lat1)))
                lon0 = lon - area[0]
                slon0 = int(np.argmin(abs(lon0)))
                lon1 = lon - area[1]
                slon1 = int(np.argmin(abs(lon1)))
                return [slat0, slat1, slon0, slon1]
            del_area = ll_del(lat, lon, range[0])
            # 人为重新排序
            if del_area[0] > del_area[1]:
                del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
            if del_area[2] > del_area[3]:
                del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

            lat = lat[del_area[0]:del_area[1]+1]
            lon = lon[del_area[2]:del_area[3] + 1]
            return lat, lon, del_area
        if self.ranges[0] is not False:
            self.lat,  self.lon, self.del_area=del_ll(ranges, lat, lon)
        else:
            self.lat,  self.lon, self.del_area = [lat, lon, False]

    def del_data(self, data):
        if self.del_area is not False:
            return data[self.del_area[0]:self.del_area[1]+1, self.del_area[2]:self.del_area[3] + 1]
        else:
            return data

    def get_ranges(self, data, ranges_reconstrcution_button):
        if self.print_min_max:
            print(np.nanmin(data), np.nanmax(data))
        from numpy import ma
        from matplotlib import cbook
        from matplotlib.colors import Normalize
        def ranges_create(begin, end, inter):
            class MidPointNorm(Normalize):
                def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                    Normalize.__init__(self, vmin, vmax, clip)
                    self.midpoint = midpoint

                def __call__(self, value, clip=None):
                    if clip is None:
                        clip = self.clip

                    result, is_scalar = self.process_value(value)

                    self.autoscale_None(result)
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if not (vmin < midpoint < vmax):
                        raise ValueError("midpoint must be between maxvalue and minvalue.")
                    elif vmin == vmax:
                        result.fill(0)  # Or should it be all masked? Or 0.5?
                    elif vmin > vmax:
                        raise ValueError("maxvalue must be bigger than minvalue")
                    else:
                        vmin = float(vmin)
                        vmax = float(vmax)
                        if clip:
                            mask = ma.getmask(result)
                            result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                              mask=mask)

                        # ma division is very slow; we can take a shortcut
                        resdat = result.data

                        # First scale to -1 to 1 range, than to from 0 to 1.
                        resdat -= midpoint
                        resdat[resdat > 0] /= abs(vmax - midpoint)
                        resdat[resdat < 0] /= abs(vmin - midpoint)

                        resdat /= 2.
                        resdat += 0.5
                        result = ma.array(resdat, mask=result.mask, copy=False)

                    if is_scalar:
                        result = result[0]
                    return result

                def inverse(self, value):
                    if not self.scaled():
                        raise ValueError("Not invertible until scaled")
                    vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                    if cbook.iterable(value):
                        val = ma.asarray(value)
                        val = 2 * (val - 0.5)
                        val[val > 0] *= abs(vmax - midpoint)
                        val[val < 0] *= abs(vmin - midpoint)
                        val += midpoint
                        return val
                    else:
                        val = 2 * (value - 0.5)
                        if val < 0:
                            return val * abs(vmin - midpoint) + midpoint
                        else:
                            return val * abs(vmax - midpoint) + midpoint
            levels1 = np.arange(begin, 0, inter)
            # levels1 = np.insert(levels1, 0, begin_b)

            levels1 = np.append(levels1, 0)
            levels2 = np.arange(0+inter, end, inter)
            # levels2 = np.append(levels2, end_b)
            levels = np.append(levels1, levels2)
            return levels, MidPointNorm(0)

        if isinstance(ranges_reconstrcution_button, list)  or (type(ranges_reconstrcution_button) is np.ndarray):
            self.levels = ranges_reconstrcution_button
        else:

            ranges_min = np.nanmin(data); ranges_max = np.nanmax(data)
            if ranges_reconstrcution_button is False:
                if ranges_min>0:
                    self.levels = np.linspace(ranges_min, ranges_max, 30)
                    return
                ticks = (ranges_max-ranges_min)/30
            else:
                ticks = ranges_reconstrcution_button
            self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

    def p_contourf(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

        if self.MidPointNorm is not False:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', norm=self.MidPointNorm, **self.contourf_arg)

        else:
            self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', **self.contourf_arg)



        if self.colorbar:
            if self.orientation == 'h':
                orientation='horizontal'
            else:
                orientation='vertical'
            self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                     shrink=0.9, ax=self.ax, format=self.fmt_contourf)

            # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

    def p_contour(self, data):
        data = self.del_data(data)
        self.get_ranges(data, self.ranges_reconstrcution_button_contour)

        cb = self.ax.contour(self.lon, self.lat, data, levels=5, transform=self.crs, **self.contour_arg)
        cbs = self.ax.clabel(
            cb,  fontsize=self.clabel_size,# Typically best results when labelling line contours.
            colors=['black'],
            inline=True,  # Cut the line where the label will be placed.
            fmt=self.fmt_contour,  # Labes as integers, with some extra space.
        )
        # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

    def p_quiver(self, u, v):
        u = self.del_data(u)
        v = self.del_data(v)

        # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
        qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                             , transform=self.crs, **self.quiver_arg)
        # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
        #                      , transform=self.crs)
        qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                               coordinates='axes')

    def lat_lon_shape(self):

        # self.ax.gridlines(draw_labels=True)
        # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
        self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
        self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        self.ax.xaxis.set_major_formatter(lon_formatter)
        self.ax.yaxis.set_major_formatter(lat_formatter)
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader

        # if shp_China is not False:
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        #     shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.4)
        #     ax.add_feature(shape_feature)
        # if shp_US is not False:
        #     shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        #     shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
        #                                    ccrs.PlateCarree(),
        #                                    edgecolor='k', facecolor='none', linewidths=0.55)
        #     ax.add_feature(shape_feature, alpha=0.5)

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc'%(s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(path_MainData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc'%(s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east**2+ivt_north**2)
    return ivt_total

def load_ivt_now(timeAR, num):
    '''
    load all AR_detection shape
    :param timeAR: list ,datetime64s, ARs exist time
    :param num: list, int, ARs num in accurate time
    :return: exist_contour_all
    '''
    exist_contour_all = []
    for sI_loc, s_time in enumerate(timeAR):
        s_time = pd.to_datetime(s_time)
        path_AR = path_SaveData+'daily_AR/%s'%(s_time.strftime('%Y%m%d_%H'))
        exist_contour = nc4.read_nc4(path_AR)[int(num[sI_loc] - 1)]
        exist_contour_all.append(exist_contour)
    return exist_contour_all



time_Seaon_divide = [[12,1,2], [3,4,5], [6,7,8], [9,10,11]]

path_MainData = r'/home/linhaozhong/work/Northpolar_era5/1950_2019/output_int6hr_NorthPolar-1deg/'
path_SaveData = r'/home/linhaozhong/work/AR_detection/'

path_MainData = r'/zhong/AR_detection\data\\'
path_SaveData = r'/zhong/AR_detection\\'

dual_threshold = nc4.read_nc4(path_SaveData+'quantile_field_t2m')[:, ::4, ::4]

# path_MainData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\data\\'
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhong\AR_detection\\'


times_file_all = os.listdir(path_SaveData+'AR_contour/')

for sI_time, s_time_all in enumerate(times_file_all):

    info_AR = xr.open_dataset(path_SaveData+'AR_contour/%s'%s_time_all)

    s_time_loop = info_AR.time.values


    # dirs_path = path_SaveData+'pic_t2m/%s/'%(s_time_all[:-3])
    # os.makedirs(dirs_path, exist_ok=True)

    for s_time in s_time_loop:

        s_time = pd.to_datetime(s_time)
        sI_Season = np.argwhere(np.isin(time_Seaon_divide, s_time.month))[0][0]
        s_dual_threshold = dual_threshold[sI_Season]


        charater_AR = info_AR.sel(time=s_time)['vari'].values
        trajectory = scipy.io.loadmat(path_MainData + r'%s_6hr.mat')
        lat = trajectory['lat_H']
        lon = trajectory['lon_H']
        lat = np.ma.masked_where(charater_AR==0, lat)
        lon = np.ma.masked_where(charater_AR==0, lon)
        exit(0)







