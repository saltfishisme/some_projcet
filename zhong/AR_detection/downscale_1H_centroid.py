import os
from geopy.distance import distance
import nc4
import pandas as pd
from scipy.ndimage import gaussian_filter
from fil_finder import FilFinder2D
import astropy.units as u
import numpy as np
from skimage import measure
import matplotlib.pyplot as plt
import cv2
import xarray as xr
import logging.handlers
import traceback

"""
get AR centroid and retrace the contour before AR
"""


bu_Area_Protion_for_connective_contour = 0.3 # two AR_detection pathways partially overlap
bu_the_number_of_grids = 400 # to remove the AR_detection pathway with the number of grids is smaller 400
resolution = 0.5
slong = np.arange(0,180, 0.5)
IVT_long = np.append(slong, np.arange(-180,0, 0.5))
IVT_lat = np.arange(90,-90.25,-0.5)

def AR_orNot(contour, ivt_total):
    def daily_AR_detection(data, IVT_lat, IVT_long, row_data):
        '''
        :param data: np.array, 2D data.
        :param lat, lon, resolution: lat(list or array-like), lon(list or array-like) and resolution(float) of the data.
        :param given_threshold: float, the minimum value which can be marked as 'high_value'.
        :param numll_min: float, the points in one high_value region should be more than numll_min, otherwise, this region
                                 will be filtered.

        :return: array, shape is [the number of high_value area, 7],
                the first five indexes are  x_centre, y_centre, width, height, angle,
                The sixth and seventh indices are area of fitted ellipse and high_value region.
                The eighth and ninth indices are averaged wind speed of fitted ellipse and high_value region
        '''


        def cal_contour(data, roll=False):

            ####### prepare data
            s_IVT_long = IVT_long.copy()
            if roll != False:
                data = np.roll(data, roll, axis=1)
                s_IVT_long = np.roll(IVT_long, roll)
            thresh = cv2.threshold(data, 0.1, 255, cv2.THRESH_BINARY)[1]
            labels = measure.label(thresh, connectivity=1, background=0)

            #######
            Mask_onePic = np.zeros(data.shape, dtype='uint8')
            lineMask_onePic = np.zeros(data.shape, dtype='uint8')
            contourMask = []
            lineMask = []
            s_df_result = pd.DataFrame()

            if roll != False:
                num_AR = 1000
            else:
                num_AR = 0

            ''' #########################   loop for every contour in this image   ###########################'''
            for label in np.unique(labels):
                if label == 0:
                    continue

                #######  prepare labelMask, labelMask is a narray contains one contour
                labelMask = np.zeros(thresh.shape, dtype="uint8")
                labelMask[labels == label] = 255
                numPixels = cv2.countNonZero(labelMask)

                ####### get index of this contour
                ar_pathway_index = np.argwhere(labelMask != 0)

                '''' ####### numPixels , longitude and latitude that a AR_detection event must spread across '''
                if roll != False:
                    if (np.in1d(np.unique(ar_pathway_index[:, 1]), [roll])).any() == False:
                        continue
                else:
                    if (np.in1d(np.unique(ar_pathway_index[:, 1]), [0, thresh.shape[1] - 1])).any():
                        continue

                if not ((np.in1d(s_IVT_long[np.unique(ar_pathway_index[:, 1])], bu_study_box[0]).any()) & (
                        np.in1d(IVT_lat[np.unique(ar_pathway_index[:, 0])], bu_study_box[1]).any())):
                    continue


                if (numPixels > de_the_number_of_grids):
                    simple = labelMask
                    simple[simple == 255] = 1

                    ######## sketetons
                    fil = FilFinder2D(simple, distance=1 * u.pix, mask=simple)
                    fil.preprocess_image(skip_flatten=True)
                    fil.create_mask(border_masking=True, verbose=False,
                                    use_existing_mask=True)
                    fil.medskel(verbose=False)
                    result = fil.analyze_skeletons(branch_thresh=8 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')

                    # print(fil.lengths())
                    # print(fil.lengths()/np.sum(fil.branch_properties['length']))
                    # # print(fil.branch_properties)
                    # exit(0)
                    # print(fil.curvature_branches())
                    # plt.imshow(simple, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()

                    '''###################  criteria ################################### '''

                    #######
                    if (fil.branch_properties['number'] > de_branch_number):
                        # print('kill by shape')
                        # plt.imshow(labelMask, cmap='gray')
                        # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                        # plt.axis('off')
                        # plt.show()
                        continue

                    ######
                    if ((fil.lengths() / np.sum(fil.branch_properties['length'])) < de_branch_ratio * u.pix):
                        # print('kill by ratio')
                        # plt.imshow(labelMask, cmap='gray')
                        # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                        # plt.axis('off')
                        # plt.show()
                        continue

                    # plt.imshow(labelMask, cmap='gray')
                    # plt.contour(fil.skeleton, colors='r', linewidth=0.1)
                    # plt.axis('off')
                    # plt.show()

                    #######
                    sslabels = measure.label(labelMask, connectivity=1, background=0)
                    regions = measure.regionprops(sslabels, intensity_image=row_data)
                    length = regions[0].axis_major_length
                    width = regions[0].axis_minor_length
                    if length / width < 2:
                        # plt.imshow(labelMask)
                        # plt.title('kill by length/width')
                        # plt.show()
                        continue

                    #######
                    try:
                        fil.analyze_skeletons(branch_thresh=40 * u.pix, skel_thresh=1 * u.pix, prune_criteria='length')
                    except:
                        continue
                    ll_loc = np.array(fil.end_pts)[0]
                    lat_loc = IVT_lat[ll_loc[:, 0]]
                    lon_loc = s_IVT_long[ll_loc[:, 1]]
                    try:
                        length = distance((lat_loc[0], lon_loc[0]), (lat_loc[1], lon_loc[1])).km
                        if length < 2000:
                            continue
                    except:
                        print('error')

                    contourMask.append(np.array(labelMask))
                    lineMask.append(np.array(fil.skeleton_longpath, dtype="uint8"))

                    Mask_onePic = cv2.add(Mask_onePic, labelMask)
                    lineMask_onePic = cv2.add(lineMask_onePic, np.array(fil.skeleton_longpath, dtype='uint8'))

                    num_AR += 1
                    contours = measure.find_contours(labelMask)[0]
                    s_df = pd.DataFrame()

                    def array_to_str(array):
                        saveStr = ''
                        for i in array:
                            saveStr += ',' + str(i)
                        return saveStr

                    s_df['y'] = array_to_str([contours[:, 0]])
                    s_df['x'] = array_to_str([contours[:, 1]])
                    s_df['num'] = num_AR
                    s_df_result = s_df_result.append(s_df, ignore_index=True)

            if roll != False:
                thresh = np.roll(thresh, -roll, axis=1)
                lineMask_onePic = np.roll(lineMask_onePic, -roll, axis=1)
                Mask_onePic = np.roll(Mask_onePic, -roll, axis=1)

                if num_AR > 1000:
                    contourMask = np.array(contourMask)
                    lineMask = np.array(lineMask)
                    for s_lineMask in range(len(lineMask)):
                        contourMask[s_lineMask] = np.roll(contourMask[s_lineMask], -roll, axis=1)
                        lineMask[s_lineMask] = np.roll(lineMask[s_lineMask], -roll, axis=1)
            return num_AR, s_df_result, contourMask, lineMask, thresh, lineMask_onePic

        # cal contour for row map
        num_AR, s_df_result, mask_ss, lineMask, thresh, lineMask_M = cal_contour(data)

        # cal contour for rolled map
        s_roll_number = int(data.shape[1] / 2)

        num_AR_cross, s_df_result_cross, mask_ss_cross, lineMask_cross, thresh_cross, lineMask_M_cross = cal_contour(data,
                                                                                                                     roll=s_roll_number)
        if num_AR+num_AR_cross != 0:
            return True
        else:
            return False

    bu_study_box = [np.arange(0, 360, 0.5), np.arange(90, -90.01, -0.5)]
    IVT_long = np.arange(0, 360, 0.5)
    IVT_lat = np.arange(90, -90.01, -0.5)
    de_branch_ratio = 0.7
    de_branch_number = 5
    de_the_number_of_grids = 400  # to remove the AR_detection pathway with the number of grids is smaller 400

    return daily_AR_detection(contour, IVT_lat, IVT_long, ivt_total)

def Overlap(row_data, given_threshold, exist_contour, contour_next):
    contour_next = np.array(contour_next, dtype='uint8')
    contourMask = []
    num = 0

    data = row_data - given_threshold
    data = gaussian_filter(data, sigma=1)
    s_IVT_long = IVT_long
    area_Protion = []

    exist_contour = np.array(exist_contour, dtype='uint8')
    p_area_exist_contour = len(np.argwhere(exist_contour != 0).flatten())

    thresh = cv2.threshold(data, 1, 255, cv2.THRESH_BINARY)[1]
    labels = measure.label(thresh, connectivity=1, background=0)

    ################### get pair-contour ###############################
    labels_unique = np.unique(labels)[np.unique(labels) != 0]
    labels_Mask_num = []
    for label in labels_unique:
        s_label_index = np.argwhere(labels == label)
        s_lon = s_label_index[:, 1]
        if np.isin(s_lon, [0]).any():
            labels_Mask_num.append(2)
        elif np.isin(s_lon, [thresh.shape[1] - 1]).any():
            labels_Mask_num.append(3)
        else:
            labels_Mask_num.append(1)
    labels_Mask_num = np.array(labels_Mask_num)
    for label_begin in np.argwhere(labels_Mask_num == 2):
        s_lon_begin = np.argwhere(labels[:, 0] == labels_unique[label_begin]).flatten()
        # print('%%')
        # print(s_lon_begin)
        for label_end in np.argwhere(labels_Mask_num == 3):
            s_lon_end = np.argwhere(labels[:, -1] == labels_unique[label_end]).flatten()
            # print()
            # print(s_lon_end)
            if len(s_lon_end) == 0:
                continue
            protion1 = len(s_lon_begin[np.isin(s_lon_begin, s_lon_end)]) / len(s_lon_begin)
            protion2 = len(s_lon_end[np.isin(s_lon_end, s_lon_begin)]) / len(s_lon_end)
            protion = np.max([protion1, protion2])
            # print(protion, 'protion')
            if protion > 0.3:
                labels[labels == labels_unique[label_end]] = labels_unique[label_begin]

    ################### loop in every contour ###############################
    Area_Protion = []
    for label in np.unique(labels)[np.unique(labels) != 0]:

        #######  prepare labelMask, labelMask is a narray contains one contour
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 1.0
        numPixels = cv2.countNonZero(labelMask)

        # plt.imshow(labelMask)
        # plt.show()

        if (numPixels > bu_the_number_of_grids):
            ar = cv2.bitwise_and(labelMask, exist_contour)
            if cv2.countNonZero(ar) != 0:
                # p_area_labelMask = len(np.argwhere(labelMask!=0).flatten())
                p_area = len(np.argwhere(ar != 0).flatten())
                s_Area_Protion = p_area / p_area_exist_contour

                if s_Area_Protion > bu_Area_Protion_for_connective_contour:
                    ar = cv2.bitwise_and(labelMask, contour_next)
                    if cv2.countNonZero(ar) != 0:
                        num += 1
                        Area_Protion.append(s_Area_Protion)
                        contourMask.append(labelMask)

    if num > 0:
        s_selected = np.argmax(np.array(Area_Protion))

        # plt.imshow(np.array(contourMask[s_selected]))
        # plt.show()
        return np.array(contourMask[s_selected])
    else:
        return 'no'

def centroid(charater_AR, s_time):
    def direction(u, v):

        a = np.arctan2(v, u) * 180 / np.pi
        if a< 0:
            a = 360+a
        a0 = 0
        if u > 0:
            a0 = 180
        return 90-a+a0

    def load_ivt_total(s_time):
        '''
        :param s_time: datetime64
        :return: ivt_total
        '''
        s_time = pd.to_datetime(s_time)
        s_time_str = s_time.strftime('%Y%m%d')
        index_vivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
        index_uivt = xr.open_dataset(
            path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
        # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
        # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

        ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
        ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

        ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
        return ivt_total, ivt_east, ivt_north

    ivt_total, u, v = load_ivt_total(s_time)
    ivt_total[charater_AR==0] = np.nan
    from function_shared_Main import cal_area
    s_area = cal_area(np.arange(0,360, resolution), IVT_lat)
    s_area[charater_AR==0] = np.nan
    long, lat = np.meshgrid(np.arange(0,360, resolution), IVT_lat)
    long = np.array(long,dtype='double')
    lat = np.array(lat,dtype='double')

    long_AR_index = np.argwhere(charater_AR != 0)[:,1]
    long_AR_index = sorted(long_AR_index)
    b = sorted(set(range(long_AR_index[0],long_AR_index[-1]+1))-set(long_AR_index))

    # if AR across longitude 0, and long_AR_index is not ascend
    if (np.isin(long_AR_index, [0]).any()) & (len(b)!=0):
        long_min = b[0]; long_max = b[-1]

        long_row = np.zeros(charater_AR.shape[1]-long_max+long_min+1)
        long_row[:charater_AR.shape[1]-long_max] = long[0, long_max:].copy()
        long_row[charater_AR.shape[1]-long_max:charater_AR.shape[1]-long_max+long_min+1] = long[0, :long_min+1].copy()

        long[:, long_max:] = np.arange(charater_AR.shape[1]-long_max)
        long[:, :long_min+1] = np.arange(charater_AR.shape[1]-long_max, charater_AR.shape[1]-long_max+long_min+1)
    else:
        long_min = np.min(long_AR_index); long_max = np.max(long_AR_index)
        long_row = long[0, long_min:long_max+1].copy()
        long[:, long_min:long_max+1] = np.arange(long_max-long_min+1)

    long[charater_AR==0] = np.nan
    lat[charater_AR==0] = np.nan

    protion = ivt_total*(s_area/np.nansum(s_area))
    centroid_long_int = np.nansum(long*(protion/np.nansum(protion)))

    centroid_long = long_row[int(centroid_long_int)]

    centroid_index = [np.argwhere(IVT_lat==int(np.nansum(lat*protion)/np.nansum(protion))).flatten(),
                      np.argwhere(np.arange(0,360, resolution)==int(centroid_long)).flatten()]
    centroid_direction = direction(u[centroid_index[0], centroid_index[1]], v[centroid_index[0], centroid_index[1]])
    return [np.nansum(lat*protion)/np.nansum(protion), centroid_long, centroid_direction]

import os

path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

project_name = 'AR_NP85_1940'
path_SaveData = r'/home/linhaozhong/work/%s/AR_contour_reAR_centroid_all/'%project_name
path_contour = r'/home/linhaozhong/work/%s/AR_contour_42_reAR_all//'%project_name
file = os.listdir(path_contour)


for i_file in file:
    if not os.path.exists(path_SaveData+i_file[:-3]):
        info_AR_row = xr.open_dataset(path_contour + i_file)

        time_allbutAR = info_AR_row.time_all_butAR.values
        centroid_allbutAR = []

        for iI_time in range(len(time_allbutAR[:-1])):
            i_time_now = pd.to_datetime(time_allbutAR[iI_time])
            i_time_next = pd.to_datetime(time_allbutAR[iI_time+1])
            contour_now = info_AR_row['contour_all'].sel(time_all=time_allbutAR[iI_time]).values
            contour_next = info_AR_row['contour_all'].sel(time_all=time_allbutAR[iI_time+1]).values

            # loop between now and next to get 1h frequency centroid
            centroid_allbutAR.append(centroid(contour_now, i_time_now)[:2])

            loop_time_two_AR = pd.date_range(i_time_now, i_time_next, freq='1H')
            for s_loop_time_2AR in loop_time_two_AR[1:-1]:
                ivt = load_ivt_total(s_loop_time_2AR)

                dual = nc4.read_nc4(r'/home/linhaozhong/work/%s/daily_85'%project_name)[0]

                # overlap must in contour_now, and contour_next.
                # while contour_now must have enough overlapped grids.
                overlap_now = Overlap(ivt, dual, contour_now, contour_next)
                if not isinstance(overlap_now, str):

                    centroid_allbutAR.append(centroid(overlap_now, s_loop_time_2AR)[:2])

                else:
                    centroid_allbutAR.append([np.nan, np.nan])

        # add last centroid.
        centroid_allbutAR.append(centroid(contour_next, i_time_next)[:2])

        nc4.save_nc4(np.array(centroid_allbutAR), path_SaveData+i_file[:-3])


path_centroid = r'/home/linhaozhong/work/%s/AR_contour_reAR_centroid_all/'%project_name

file = os.listdir(path_centroid)

centroid_all = []
length = 0
filename = []
for i_file in file:
    if int(i_file[:4]) >= 1940:
        centroid_now = nc4.read_nc4(path_centroid+i_file[:-4])
        length = np.max([centroid_now.shape[0], length])
        centroid_all.append(centroid_now)
        filename.append(i_file)

print(length, len(centroid_all))
centroid_all_ll = np.empty([2,int(len(centroid_all)), length])*np.nan

for iI, i in enumerate(centroid_all):
    centroid_all_ll[0,iI, :int(len(i))] = i[:,0]
    centroid_all_ll[1,iI, :int(len(i))] = i[:,1]
import scipy.io as scio
scio.savemat('/home/linhaozhong/work/%s/centroid_all_1940.mat'%project_name, {'filename':filename, 'centroid':centroid_all_ll})






