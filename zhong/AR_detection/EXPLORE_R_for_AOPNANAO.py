import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys
import cartopy.crs as ccrs

import nc4

import scipy.io as scio
import nc4
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import os
import cartopy.crs as ccrs
import cmaps

"""
=============================
first, we get the period
=============================
"""

type_name = ['greenland_east', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE']

pattern_name = ['AO', 'NAO', 'PNA']


# def get_climate_index(index):
#     if index == 'NAO':
#         time_pattern_index = pd.read_csv(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\time\norm.daily.nao.cdas.z500.19500101_current.csv")
#     elif index == 'AO':
#         time_pattern_index = pd.read_csv(
#             r"D:\OneDrive\basis\some_projects\zhong\AR_detection\time\norm.daily.ao.cdas.z1000.19500101_current.csv")
#     elif index == 'PNA':
#         time_pattern_index = pd.read_csv(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\time\norm.daily.pna.cdas.z500.19500101_current.csv")
#     return time_pattern_index
#
# result_df = pd.DataFrame()
# for i_type_name in type_name:
#     s_result = []
#     for index in pattern_name:
#         time_inAR = scio.loadmat(r"D:\OneDrive\basis\some_projects\zhong\AR_detection\time\AR_before_after_timeDJF_%s"%i_type_name)
#         # print(time_inAR)
#         # exit()
#         time_AR = [pd.to_datetime(i) for i in time_inAR['time_AR']]
#         time_all = [pd.to_datetime(i) for i in time_inAR['time_all']]
#
#         time_inAR = pd.DataFrame(np.zeros(len(time_all)), index=time_all, columns=['AR_all']).resample('D').mean()
#         time_inAR_1 = pd.DataFrame(np.zeros(len(time_AR)), index=time_AR, columns=['AR_ar']).resample('D').mean()
#
#         # result = pd.concat([time_inAR_1,time_inAR, ], axis=1,ignore_index=True)
#         # time_inAR = time_inAR.resample('D').mean()
#
#         # time pattern
#         time_pattern_index = get_climate_index(index)
#         datatime = pd.to_datetime(time_pattern_index['year'] * 10000 + time_pattern_index['month'] * 100 + time_pattern_index['day'], format='%Y%m%d')
#         time_pattern_index = time_pattern_index.set_index(datatime).drop(columns=['year', 'month', 'day'])
#         time_pattern_index.columns = ['index']
#         # transform into date range
#         time_pattern_index = pd.concat([time_pattern_index, time_inAR,time_inAR_1], axis=1)
#
#         time_pattern_index = time_pattern_index.loc['1979-01-01':]
#         time_pattern_index = time_pattern_index[np.isin(time_pattern_index.index.month, [11,12,1])]
#         # time_pattern_index = time_pattern_index[time_pattern_index['AR_all']==0]
#         # time_pattern_index['index'].hist()
#         # plt.show()
#         # exit(0)
#         def index_in_different_mode(time_pattern_index, mode=None):
#             """
#             :param time_pattern_index:
#             :param mode: [1,'all'], [0,'ar']
#             :return:
#             """
#             if mode[0]:
#                 time_pattern_index['index'][time_pattern_index['index']>0] = 1
#                 time_pattern_index['index'][time_pattern_index['index']<0] = -1
#             if mode[1] == 'all':
#                 if mode[0]:
#                     values_count = time_pattern_index['index'][time_pattern_index['AR_all']==0].value_counts()
#                     values_count = values_count[1]/(values_count[1]+values_count[-1])
#                 else:
#                     values_posi = time_pattern_index['index'][(time_pattern_index['AR_all']==0)&(time_pattern_index['index']>0)].mean()
#                     values_nega = time_pattern_index['index'][(time_pattern_index['AR_all']==0)&(time_pattern_index['index']<0)].mean()
#                     return [np.round(values_posi,2), np.round(values_nega,2)]
#
#                 # time_pattern_index = time_pattern_index[time_pattern_index['AR_all']==0].mean()
#             else:
#                 if mode[0]:
#                     values_count = time_pattern_index['index'][time_pattern_index['AR_ar']==0].value_counts()
#                     values_count = values_count[1]/(values_count[1]+values_count[-1])
#                 else:
#                     values_posi = time_pattern_index['index'][(time_pattern_index['AR_ar']==0)&(time_pattern_index['index']>0)].mean()
#                     values_nega = time_pattern_index['index'][(time_pattern_index['AR_ar']==0)&(time_pattern_index['index']<0)].mean()
#                     return [np.round(values_posi,2), np.round(values_nega,2)]
#                 # time_pattern_index = time_pattern_index[time_pattern_index['AR_ar']==0].mean()
#             return np.round(values_count,2)
#
#
#         def extract_index(time_pattern_index, mode=None):
#             """
#             :param time_pattern_index:
#             :param mode: [1,'all'], [0,'ar']
#             :return:
#             """
#
#             if mode == 'all':
#
#                 values = time_pattern_index['index'][(time_pattern_index['AR_all']==0)].values
#
#             else:
#                 values = time_pattern_index['index'][(time_pattern_index['AR_ar']==0)].values
#
#             save_name = r'ba_%s_%s_%s'%(index,mode, i_type_name)
#             nc4.save_nc4(values, r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+save_name)
#
#                 # time_pattern_index = time_pattern_index[time_pattern_index['AR_ar']==0].mean()
#
#         # for mode in ['all', 'ar']:
#         #     extract_index(time_pattern_index.copy(), mode)
#
#         mode_result = []
#         for mode in [[1,'all'], [1,'ar'],[0,'all'], [0,'ar']]:
#             print(index_in_different_mode(time_pattern_index.copy(), mode))
#             mode_result.append(index_in_different_mode(time_pattern_index.copy(), mode))
#         s_result.append(mode_result)
#     #
#     result_df[i_type_name] = pd.Series(s_result, index=['ao', 'nao', 'pna'])
#
#
# print(result_df)
# #
# result_df.to_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+'%s.csv'%'ar_posi_ratio_before_after')
#
#
# exit(0)


"""
=============================
work for pdf plot
"""

# pattern_name = ['AO', 'NAO', 'PNA']
#
#
#
# type_name_row = ['greenland_east', 'useless', 'greenland_west','pacific_east', 'pacific_west',
#              'MS', 'GE', 'useless2']
# type_name_num = ['0','2','3','4','5','6']
#
# plot_name = ['GRE', 'BAF','BSE', 'BSW',
#              'MED', 'ARC']
#
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
#
# infig_fontsize = 5
#
# resolution=0.5
#
# plot_param = {'AO':[['mean=',''],'T2Mano',[-3,3], 1],
#               'NAO':[['mean=',''],'sensible heat anomaly',[-2,2], 1],
#               'PNA':[['mean=',''], 'latent heat anomaly',[-2,2],1],
#
#               }
#
# SMALL_SIZE = 7
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font', size=SMALL_SIZE)  # controlsdefaulttextsizes
# plt.rc('lines', linewidth=0.5)  # controlsdefaulttextsizes
# plt.rc('axes', titlesize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes', labelsize=SMALL_SIZE)  # fontsizeoftheaxestitle
# plt.rc('xtick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick', labelsize=SMALL_SIZE, direction='in')  # fontsizeoftheticklabels
# plt.rc('legend', fontsize=SMALL_SIZE)  # legendfontsize
#
#
#
# def get_data():
#     data_mean = []
#     for s_type_name in type_name:
#         s_data_mean = []
#         for var in pattern_name:
#             for aa in ['ar']:
#                 s_data_mean.append(nc4.read_nc4(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'
#                                                 + 'ba_%s_%s_%s' % (var, aa, s_type_name)))
#
#         data_mean.append(s_data_mean)
#
#     return np.array(data_mean)
#
#
# frequency = get_data()
#
# def plots(ax, data, bin_width, mean_label=['','']):
#     percentile_label = ['Q1', 'Q2', 'Q3']
#     percentile = np.percentile(data, [25,50,75])
#     mean = np.nanmean(data)
#     HIST_BINS = np.linspace(np.min(data), np.max(data), 100)
#     # ax.boxplot(data)
#
#     _, _, bar_container = ax.hist(data,bins=bin_width,density=True, lw=1,
#                               ec="black", fc="gray",zorder=2)
#
#     # yticks = ax.get_yticks()
#     # ax.set_yticks(yticks)
#     # ax.set_yticklabels((yticks*len(data)).astype('int'))
#
#     ylim_max = ax.get_ylim()[1]
#     trans = ax.get_xaxis_transform()
#     for i_percentile in range(3):
#         ax.axvline(percentile[i_percentile], linestyle='--',linewidth=0.5, color='black',zorder=1)
#         ax.text(percentile[i_percentile], 0.8, percentile_label[i_percentile],
#                 transform=trans,horizontalalignment='center', zorder=3,fontsize=infig_fontsize,
#                 bbox = {'facecolor': 'white','edgecolor':'white', 'pad': 0.1}
#         )
#         ax.text(percentile[i_percentile], 1.01, '%0.2f'%percentile[i_percentile], transform=trans,
#                 horizontalalignment='center', fontsize=infig_fontsize)
#
#     ax.text(0.9, 0.89, mean_label[0]+'%0.1f'%mean+mean_label[1], transform=ax.transAxes,horizontalalignment='center',fontsize=infig_fontsize)
#
#     return [ax.get_ylim()[1], ax.get_xlim()[1]]
#
# # mean_labels = [['IVTmean=', 'kg/(m*s)'], ['Tmean=','℃'], ['TCWVmean=','kg/(m**2)']]
# # x_labels = ['IVTano', 'Tano', 'TCWVano']
#
# # mean_labels = [['IVTmean=\n', 'kg/(m*s)'], ['Tano=\n','K']]
# fig, axs = plt.subplots(6, 3, figsize=[7.5, 5])
# plt.subplots_adjust(top=0.95,
# bottom=0.04,
# left=0.065,
# right=0.995,
# hspace=0.4,
# wspace=0.11)
#
#
#
# def flatten(l):
#     l = np.array([item for sublist in l for item in sublist])
#     return l[~np.isnan(l)]
# bin_col = []
# for i in range(frequency.shape[1]):
#     bin_col.append(np.histogram(flatten(frequency[:,i]), bins=20)[1])
#
# for i in range(6):
#     lim = np.zeros([6,3])
#     for j in range(3):
#         data = frequency[i,j]
#         ax = axs[i][j]
#         plots(ax, data, bin_col[j],mean_label=plot_param[pattern_name[j]][0])
#         ax.annotate(figlabelstr[3 * i + j], [0.01, 0.85], xycoords='axes fraction')
#         ax.axvline(0, color='red')
#     # axs[i,0].set_ylim(top=0.4)
#     # axs[i,1].set_ylim(top=0.4)
#
#     # axs[i][0].set_xlim([-4,4])
#     # axs[i][1].set_xlim([-2,2])
#     # axs[i][2].set_xlim([-2, 2])
#
#     axs[i][0].set_ylabel(plot_name[i] + '\nProbability')
#
#
#
# # x_labels = pattern_name
# x_labels = ['AO Lag-', 'NAO Lag-', 'PNA Lag-']
# for i_col in range(3):
#     axs[0][i_col].set_title(x_labels[i_col], pad=10)
#
# # ax.set_xlabel(x_labels[j])
# # plt.show()
# plt.savefig('figure7_cor1.png', dpi=300)
# plt.close()


"""
=============================
work for bar plot
"""
def bar_plot(frequency, ax, abs_value=False, one_two_label=['ALL, AR']):
    plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
              'MED', 'ARC']

    figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)',
                   '(o)',
                   '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
    x = np.arange(len(plot_name))  # the label locations
    x_width = [-0.3, -0.1, 0.1, 0.3]
    width = 0.2  # the width of the bars


    pos = []
    lab = []
    for j_som in range(6):
        for i in range(3):
            s_frequency = frequency[i, j_som]

            rects1 = ax.bar(x[j_som] +x_width[i], s_frequency[1], width, label=one_two_label[0],
                            facecolor='#2c7fb8', edgecolor='black', linewidth=0.7,zorder=5)
            # if (abs(s_frequency[1])-abs(s_frequency[0]))>0:
            #     # rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1]-s_frequency[0], width, label='AR',
            #     #                 bottom=s_frequency[0],
            #     #                 facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7)
            #     rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1], width, label=one_two_label[1],
            #                     facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7,zorder=2)
            # else:
            #     rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1], width, label=one_two_label[1],
            #                     facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7,zorder=7)


            if abs_value:
                s_abs_value = frequency[i, j_som,2]
                print(s_abs_value)
                if s_abs_value != 0:
                    arr = frequency[i, j_som,0:2]
                    abs_arr = np.abs(arr)
                    max_abs_value = abs_arr.max()
                    max_value = np.sign(arr) * max_abs_value
                    print(max_value)
                    rects3 = ax.bar(x[j_som] +x_width[i], max_value, width,
                                    facecolor=None, edgecolor=None, linewidth=0.01,zorder=20, alpha=0)
                    ax.bar_label(rects1, labels=[s_abs_value],color='black')

            pos.append(x[j_som] + x_width[i])
            lab.append(pattern_name[i])
    ax.axhline(0, color='black')

    ax.set_xticks(pos,minor=True)
    ax.set_xticklabels(lab,minor=True,rotation = 90)

    ax.tick_params(axis='x', which='major', pad=15, size=0)
    plt.setp(ax.get_xticklabels(), rotation=0)

    handles, labels = ax.get_legend_handles_labels()
    # ax.legend(handles[:2], labels[:2], loc='lower right')

    ax.set_xticklabels(['']+plot_name)
    ax.tick_params(axis='x', which='major', pad=35)

    ax.set_ylabel('AR occurrence number (units: times)')
    # ax.set_title('')
    return

def to_float(sdata):
    sdata = sdata.replace('[', '')
    sdata = sdata.replace(']', '')
    sdata = (sdata.split(','))
    sdata = [float(item) for item in filter(lambda x: x != ',', sdata)]
    return np.array(sdata)


fig, axs = plt.subplots(ncols=1,nrows=1, figsize=[4,4])
plt.subplots_adjust(top=0.97,
bottom=0.16,
left=0.11,
right=0.98,
hspace=0.2,
wspace=0.2)
result_df = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+'%s.csv'%'ar_posi_ratio', index_col=0)

abs_value = True
frequency = np.empty([3,6,3])
for iI_index, index in enumerate(['ao', 'nao', 'pna']):
    s_frequency = []
    for iI_type_name, i_type_name in enumerate(type_name):
        s_i = to_float(result_df[i_type_name].loc[index])
        frequency[iI_index, iI_type_name,0:2] = s_i[:2]-0.5

        if abs_value:
            # get abs_values
            compare = s_i[:2]-0.5
            if (abs(compare)>0.2).any():
                if compare[0] > 0:
                    abs_index = np.max(s_i[[2,4]])
                else:
                    abs_index = np.min(s_i[[3,5]])
            else:
                abs_index=0
            frequency[iI_index, iI_type_name,-1] = abs_index

result_df = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+
                        '%s.csv'%'ar_posi_ratio_before_after',
                        index_col=0)
abs_value = True
frequency2 = np.empty([3,6,3])
for iI_index, index in enumerate(['ao', 'nao', 'pna']):
    s_frequency = []
    for iI_type_name, i_type_name in enumerate(type_name):
        s_i = to_float(result_df[i_type_name].loc[index])
        frequency2[iI_index, iI_type_name,0:2] = s_i[:2]-0.5

        if abs_value:
            # get abs_values
            compare = s_i[:2]-0.5
            if (abs(compare)>0.2).any():
                if compare[0] > 0:
                    abs_index = np.max(s_i[[2,4]])
                else:
                    abs_index = np.min(s_i[[3,5]])
            else:
                abs_index=0
            frequency2[iI_index, iI_type_name,-1] = abs_index

frequency[:,0,:] = frequency2[:,0, :]
bar_plot(frequency, axs, abs_value, ['ALL', 'AR'])
plt.show()
result_df = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+
                        '%s.csv'%'ar_posi_ratio_before_after',
                        index_col=0)



bar_plot(frequency, axs[1], abs_value, ['Lag+', 'Lag-'])


plt.show()
plt.savefig(r'pattern corr.png', dpi=400)


"""
=============================
work for bar plot
"""
def bar_plot(frequency, ax, abs_value=False, one_two_label=['ALL, AR']):
    plot_name = ['GRE', 'BAF', 'BSE', 'BSW',
              'MED', 'ARC']

    figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)',
                   '(o)',
                   '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
    x = np.arange(len(plot_name))  # the label locations
    x_width = [-0.3, -0.1, 0.1, 0.3]
    width = 0.2  # the width of the bars


    pos = []
    lab = []
    for j_som in range(6):
        for i in range(3):
            s_frequency = frequency[i, j_som]

            rects1 = ax.bar(x[j_som] +x_width[i], s_frequency[0], width, label=one_two_label[0],
                            facecolor='#2c7fb8', edgecolor='black', linewidth=0.7,zorder=5)
            if (abs(s_frequency[1])-abs(s_frequency[0]))>0:
                # rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1]-s_frequency[0], width, label='AR',
                #                 bottom=s_frequency[0],
                #                 facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7)
                rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1], width, label=one_two_label[1],
                                facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7,zorder=2)
            else:
                rects2 = ax.bar(x[j_som] + x_width[i], s_frequency[1], width, label=one_two_label[1],
                                facecolor='#7fcdbb', edgecolor='black', hatch='///', linewidth=0.7,zorder=7)


            if abs_value:
                s_abs_value = frequency[i, j_som,2]
                print(s_abs_value)
                if s_abs_value != 0:
                    arr = frequency[i, j_som,0:2]
                    abs_arr = np.abs(arr)
                    max_abs_value = abs_arr.max()
                    max_value = np.sign(arr) * max_abs_value
                    print(max_value)
                    rects3 = ax.bar(x[j_som] +x_width[i], max_value, width,
                                    facecolor=None, edgecolor=None, linewidth=0.01,zorder=20, alpha=0)
                    ax.bar_label(rects2, labels=[s_abs_value],color='black')

            pos.append(x[j_som] + x_width[i])
            lab.append(pattern_name[i])
    ax.axhline(0, color='black')

    ax.set_xticks(pos,minor=True)
    ax.set_xticklabels(lab,minor=True,rotation = 90)

    ax.tick_params(axis='x', which='major', pad=15, size=0)
    plt.setp(ax.get_xticklabels(), rotation=0)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[:2], labels[:2], loc='lower right')

    ax.set_xticklabels(['']+plot_name)
    ax.tick_params(axis='x', which='major', pad=35)

    ax.set_ylabel('AR occurrence number (units: times)')
    # ax.set_title('')
    return

def to_float(sdata):
    sdata = sdata.replace('[', '')
    sdata = sdata.replace(']', '')
    sdata = (sdata.split(','))
    sdata = [float(item) for item in filter(lambda x: x != ',', sdata)]
    return np.array(sdata)


fig, axs = plt.subplots(ncols=2,nrows=1, figsize=[10,5])
plt.subplots_adjust(top=0.97,
bottom=0.15,
left=0.08,
right=0.98,
hspace=0.2,
wspace=0.2)
result_df = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+'%s.csv'%'ar_posi_ratio', index_col=0)

abs_value = True
frequency = np.empty([3,6,3])
for iI_index, index in enumerate(['ao', 'nao', 'pna']):
    s_frequency = []
    for iI_type_name, i_type_name in enumerate(type_name):
        s_i = to_float(result_df[i_type_name].loc[index])
        frequency[iI_index, iI_type_name,0:2] = s_i[:2]-0.5

        if abs_value:
            # get abs_values
            compare = s_i[:2]-0.5
            if (abs(compare)>0.2).any():
                if compare[0] > 0:
                    abs_index = np.max(s_i[[2,4]])
                else:
                    abs_index = np.min(s_i[[3,5]])
            else:
                abs_index=0
            frequency[iI_index, iI_type_name,-1] = abs_index

result_df = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+
                        '%s.csv'%'ar_posi_ratio_before_after',
                        index_col=0)
abs_value = True
frequency2 = np.empty([3,6,3])
for iI_index, index in enumerate(['ao', 'nao', 'pna']):
    s_frequency = []
    for iI_type_name, i_type_name in enumerate(type_name):
        s_i = to_float(result_df[i_type_name].loc[index])
        frequency2[iI_index, iI_type_name,0:2] = s_i[:2]-0.5

        if abs_value:
            # get abs_values
            compare = s_i[:2]-0.5
            if (abs(compare)>0.2).any():
                if compare[0] > 0:
                    abs_index = np.max(s_i[[2,4]])
                else:
                    abs_index = np.min(s_i[[3,5]])
            else:
                abs_index=0
            frequency2[iI_index, iI_type_name,-1] = abs_index

frequency[:,0,:] = frequency2[:,0, :]
bar_plot(frequency, axs[0], abs_value, ['ALL', 'AR'])
plt.show()
result_df = pd.read_csv(r'D:\OneDrive\basis\some_projects\zhong\AR_detection\time/'+
                        '%s.csv'%'ar_posi_ratio_before_after',
                        index_col=0)



bar_plot(frequency, axs[1], abs_value, ['Lag+', 'Lag-'])


# plt.show()
plt.savefig(r'pattern_corr.png', dpi=400)


exit(0)


time_pattern_index['roll'] = time_pattern_index['index'].rolling(30).mean()
print(time_pattern_index.shape)
# time_pattern_index.dropna(inplace=True)
time_pattern_index = time_pattern_index.set_index(np.arange(time_pattern_index.shape[0]))
time_pattern_index[['roll', 'AR_all', 'AR_ar']].plot()
plt.show()

def load_Q(s_time, var):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d%H')

    def load_Q(s_time_path):
        now = xr.open_dataset(path_Qdata +'%s.nc' % (s_time_path))['__xarray_dataarray_variable__'].values
        if var == 'adibatic':
            now = now[0]
        return now

    now = load_Q(var+'/'+s_time_str)
    cli = []
    for i_year in range(1979, 2020+1):
        s_time_str = var+'_cli/'+str(i_year)+'/'+str(i_year)+s_time.strftime('%m%d%H')
        cli.append(load_Q(s_time_str))
    cli = np.nanmean(np.array(cli), axis=0)
    return now-cli

def load_ivt_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))
    index_uivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_eastward_water_vapour_flux/%s/EastWaterVapor.%s.nc' % (
            s_time_str[:4], s_time_str))
    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    ivt_north = index_vivt.sel(time=s_time)['p72.162'].values
    ivt_east = index_uivt.sel(time=s_time)['p71.162'].values

    ivt_total = np.sqrt(ivt_east ** 2 + ivt_north ** 2)
    return ivt_total

def load_ivt_north_in70(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_vivt = xr.open_dataset(
        path_singleData + 'Vertical_integral_of_northward_water_vapour_flux/%s/NorthWater.%s.nc' % (
            s_time_str[:4], s_time_str))

    ivt_north = index_vivt.sel(time=s_time, latitude=70)['p72.162'].values
    return ivt_north

def load_t2m_total(s_time):
    '''
    :param s_time: datetime64
    :return: ivt_total
    '''
    s_time = pd.to_datetime(s_time)

    s_time_str = s_time.strftime('%Y%m%d')
    index_t2m = xr.open_dataset(
        path_singleData + '2m_temperature/%s/2m_temperature.%s.nc' % (
            s_time_str[:4], s_time_str))

    # index_vivt = xr.open_dataset(path_MainData + 'NorthWater.%s.nc'%(s_time_str))
    # index_uivt = xr.open_dataset(path_MainData + 'EastWaterVapor.%s.nc'%(s_time_str))

    t2m = index_t2m.sel(time=s_time)['t2m'].values

    return t2m


def cal_3phase(contour, phase_num=3):
    time_len = np.arange(len(contour))
    division = [contour[0]]
    for i in np.arange(1, phase_num - 1):
        percentile = np.percentile(time_len, 100 * i / (phase_num - 1))
        p_index = int(percentile)
        p_weighted = percentile % 1
        division.append(contour[p_index] * p_weighted + contour[p_index + 1] * (1 - p_weighted))
    division.append(contour[-1])
    # division = np.array_split(contour, 3)
    # division_mean = []
    # for i in division:
    #     division_mean.append(np.nanmean(i, axis=0))
    return np.array(division)
def get_circulation_data_based_on_date(s_time, var, var_ShortName, var_dir=''):
    '''date containing Hour information'''
    time = pd.to_datetime(s_time)

    if var_dir=='':
        var_dir=var
    # load hourly anomaly data
    data_anomaly = scio.loadmat(path_singleData + '%s/anomaly/' % var_dir + '%s.mat' % time.strftime('%m%d'))[
                       time.strftime('%m%d')][int(time.strftime('%H')), :, :]

    # load present data
    data_present = xr.open_dataset(
        path_singleData + '%s/' % var_dir + '%s/' % time.strftime('%Y') + r'%s.%s.nc' % (var, time.strftime('%Y%m%d')))
    data_present = data_present.sel(time=time.strftime('%Y%m%d %H:00:00'))[var_ShortName].values

    return data_present - data_anomaly


time_Seaon_divide = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

path_MainData = r'/home/linhaozhong/work/AR_NP85/'
path_SaveData = r'/home/linhaozhong/work/AR_NP85/'
path_singleData = r'/home/linhaozhong/work/Data/ERA5/single_level/0.5X0.5/'
bu_addition_Name = ''
path_PressureData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
path_GlobalArea = r'/home/linhaozhong/work/AR_detection/'
path_Qdata = r'/home/linhaozhong/work/AR_analysis/'
resolution = 0.5

path_pic = r"D:\OneDrive\basis\some_projects\zhong\AR_detection\FDI_pic\\"
path_color_zhong = '/home/linhaozhong/MATLAB/colormapdata/rainbow200.mat'
# path_color_zhong = 'D:\MATLAB\colormapdata/rainbow200.mat'

use_day_for_classification = 9
max_ratio = 0.5
type_name = ['greenland_east', 'useless', 'greenland_west', 'pacific_east', 'pacific_west',
             'MS', 'GE', 'useless2']

plot_param = {
              'sshf':[['SSHFmean=',' J/(m**2)'],'sensible heat anomaly',[-2,6], 1/100000],
              'slhf':[['SLHFmea=',' J/(m**2)'], 'latent heat anomaly',[-2,6],1/100000],
              'advection':[['ADVmean=',' K/6h'], 'advection',[-10,10], 6*3600],
              'adibatic':[['ABmean=',' K/6h'],'adiabatic',[-5,5],6*3600],
                'ssr':[['TCWVmean=', ' kg/(m**2)'],'TCWVano',[0,20],1/100000],

              'IVT':[],
              'tcwv':['WaterVapor'],
                't2m':['2m_temperature']
              }
def main(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'AR_contour_42/%s' % (time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values
        AR_loc = np.squeeze(np.argwhere(time_all == time_AR[0]))
        return [[pd.to_datetime(s).strftime('%Y%m%d %H:00:00') for s in time_all[:AR_loc]],
                [pd.to_datetime(s).strftime('%Y%m%d %H:00:00') for s in time_all[AR_loc:]]]

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values
            time_AR_s=[]
            time_all_s=[]

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue
                time_AR, time_all = contour_cal(time_file, var)
                time_AR_s.extend(time_AR)
                time_all_s.extend(time_all)

            scio.savemat(path_SaveData+'AR_before_after_%s' % (var +  season + '_' + type_name[iI]), {'time_AR':time_AR_s,
                                                                                      'time_all':time_all_s})

def main_3phase(var):

    def contour_cal(time_file, var='IVT'):

        # mat file always has some bugs with str type. skip whitespace
        time_file_new = ''
        for i in time_file:
            time_file_new += i
            if i == 'c':
                break

        info_AR_row = xr.open_dataset(path_SaveData + 'AR_contour_42/%s' % (time_file_new))

        time_AR = info_AR_row.time_AR.values
        time_all = info_AR_row.time_all.values

        AR_loc = np.squeeze(np.argwhere(time_all==time_AR[0]))
        print(AR_loc)

        if var == 't2m':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                        '2m_temperature',
                                                        't2m')
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())


        elif var == 'IVT':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = load_ivt_total(pd.to_datetime(i_time))
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())

        elif var == 'tcwv':
            var_all = []
            for i_time in time_all[AR_loc:]:
                s_i = get_circulation_data_based_on_date(pd.to_datetime(i_time),
                                                        'WaterVapor',
                                                         'tcwv', var_dir='Total_column_water_vapour')
                contour = info_AR_row['contour_all'].sel(time_all=i_time).values
                contour[int(30 / resolution), :] = 0
                s_i[contour != 1] = np.nan
                var_all.append(s_i)
            var_3phase = cal_3phase(var_all)
            avg = []
            for i_p in var_3phase:
                avg.append(i_p[~np.isnan(i_p)].flatten())


        return avg

    for season in ['DJF']:
        df = pd.read_csv(path_MainData + 'type_filename_%s.csv' % season)

        for iI, i in enumerate(df.columns):
            times_file_all = df[i].values

            phase1 = []
            phase2 = []
            phase3 = []

            for time_file in times_file_all:
                if len(str(time_file)) < 5:
                    continue

                s_mean = contour_cal(time_file, var)
                phase1.extend(s_mean[0])
                phase2.extend(s_mean[1])
                phase3.extend(s_mean[2])

            phase = [phase1, phase2, phase3]
            for iI_phase, i_phase in enumerate(phase):
                nc4.save_nc4(np.array(i_phase),
                             path_SaveData + 'PDF/all_%s' % (var + '%i_'%iI_phase + season + '_' + type_name[iI]))

main('time')
# main('area')
# # main('duration')
# main_3phase('IVT')
# main_3phase('t2m')
# main_3phase('tcwv')
#
# # main('lat')
# # main('lon')
# # main_3phase('str')
# main_3phase('sic')
# # main_3phase('advection')
# # main_3phase('adibatic')
# # main_3phase('ssr')
# # main_3phase('slhf')
# # main_3phase('sshf')
#
# # main_3phase('t2m')
# exit(0)





