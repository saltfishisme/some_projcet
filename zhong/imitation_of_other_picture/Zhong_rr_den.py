import cdsapi
import xarray as xr
import scipy.io as scio
import matplotlib.pyplot as plt

import pandas as pd

import scipy.io as scio
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import cartopy.feature as cfeature
import mat73
import sys
import cmaps
from cartopy.util import add_cyclic_point


def one():
    class circulation():
        def __init__(self, ax, lon, lat, ranges):
            self.ax = ax
            self.crs = ccrs.PlateCarree()
            self.cmaps = ''
            self.colorbar=True
            self.orientation='h'
            self.ranges = ranges
            self.fmt_contourf='%.1f'
            self.fmt_contour='%.1f'
            self.ranges_reconstrcution_button_contourf = False
            self.ranges_reconstrcution_button_contour = False
            self.linewidths=1
            self.clabel_size=5
            self.cmaps=cmaps.WhiteGreen
            self.colorbar=True
            self.orientation='h'
            self.spacewind=[1,1]
            self.colors=False
            self.print_min_max=False
            self.contour_arg={'colors':'black'}
            self.contourf_arg={}
            self.quiverkey_arg={'X':0.85, 'Y':1.05, 'U':10, 'label': r'$10 \frac{m}{s}$'}
            self.contour_clabel_color = 'white'
            self.MidPointNorm = False
            def del_ll(range, lat, lon):
                def ll_del(lat, lon, area):
                    import numpy as np
                    lat0 = lat - area[0]
                    slat0 = int(np.argmin(abs(lat0)))
                    lat1 = lat - area[1]
                    slat1 = int(np.argmin(abs(lat1)))
                    lon0 = lon - area[2]
                    slon0 = int(np.argmin(abs(lon0)))
                    lon1 = lon - area[3]
                    slon1 = int(np.argmin(abs(lon1)))
                    return [slat0, slat1, slon0, slon1]
                del_area = ll_del(lat, lon, range[0])
                # 人为重新排序
                if del_area[0] > del_area[1]:
                    del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
                if del_area[2] > del_area[3]:
                    del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

                lat = lat[del_area[0]:del_area[1]+1]
                lon = lon[del_area[2]:del_area[3] + 1]
                return lat, lon, del_area
            if self.ranges[0] is not False:
                self.lat,  self.lon, self.del_area=del_ll(ranges, lat, lon)
            else:
                self.lat,  self.lon, self.del_area = [lat, lon, False]

        def del_data(self, data):
            if self.del_area is not False:
                return data[self.del_area[0]:self.del_area[1]+1, self.del_area[2]:self.del_area[3] + 1]
            else:
                return data

        def get_ranges(self, data, ranges_reconstrcution_button):
            if self.print_min_max:
                print(np.nanmin(data), np.nanmax(data))
            from numpy import ma
            from matplotlib import cbook
            from matplotlib.colors import Normalize
            def ranges_create(begin, end, inter):
                class MidPointNorm(Normalize):
                    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                        Normalize.__init__(self, vmin, vmax, clip)
                        self.midpoint = midpoint

                    def __call__(self, value, clip=None):
                        if clip is None:
                            clip = self.clip

                        result, is_scalar = self.process_value(value)

                        self.autoscale_None(result)
                        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                        if not (vmin < midpoint < vmax):
                            raise ValueError("midpoint must be between maxvalue and minvalue.")
                        elif vmin == vmax:
                            result.fill(0)  # Or should it be all masked? Or 0.5?
                        elif vmin > vmax:
                            raise ValueError("maxvalue must be bigger than minvalue")
                        else:
                            vmin = float(vmin)
                            vmax = float(vmax)
                            if clip:
                                mask = ma.getmask(result)
                                result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                                  mask=mask)

                            # ma division is very slow; we can take a shortcut
                            resdat = result.data

                            # First scale to -1 to 1 range, than to from 0 to 1.
                            resdat -= midpoint
                            resdat[resdat > 0] /= abs(vmax - midpoint)
                            resdat[resdat < 0] /= abs(vmin - midpoint)

                            resdat /= 2.
                            resdat += 0.5
                            result = ma.array(resdat, mask=result.mask, copy=False)

                        if is_scalar:
                            result = result[0]
                        return result

                    def inverse(self, value):
                        if not self.scaled():
                            raise ValueError("Not invertible until scaled")
                        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                        if cbook.iterable(value):
                            val = ma.asarray(value)
                            val = 2 * (val - 0.5)
                            val[val > 0] *= abs(vmax - midpoint)
                            val[val < 0] *= abs(vmin - midpoint)
                            val += midpoint
                            return val
                        else:
                            val = 2 * (value - 0.5)
                            if val < 0:
                                return val * abs(vmin - midpoint) + midpoint
                            else:
                                return val * abs(vmax - midpoint) + midpoint
                levels1 = np.arange(begin, 0, inter)
                # levels1 = np.insert(levels1, 0, begin_b)

                levels1 = np.append(levels1, 0)
                levels2 = np.arange(0+inter, end, inter)
                # levels2 = np.append(levels2, end_b)
                levels = np.append(levels1, levels2)
                return levels, MidPointNorm(0)

            if isinstance(ranges_reconstrcution_button, list)  or (type(ranges_reconstrcution_button) is np.ndarray):
                self.levels = ranges_reconstrcution_button
            else:

                ranges_min = np.nanmin(data); ranges_max = np.nanmax(data)
                if ranges_reconstrcution_button is False:
                    if ranges_min>0:
                        self.levels = np.linspace(ranges_min, ranges_max, 30)
                        return
                    ticks = (ranges_max-ranges_min)/30
                else:
                    ticks = ranges_reconstrcution_button
                self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

        def p_contourf(self, data):
            data = self.del_data(data)
            self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

            if self.MidPointNorm is not False:
                self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', norm=self.MidPointNorm, **self.contourf_arg)

            else:
                self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', **self.contourf_arg)



            if self.colorbar:
                if self.orientation == 'h':
                    orientation='horizontal'
                else:
                    orientation='vertical'
                self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                         shrink=0.9, ax=self.ax, format=self.fmt_contourf)

                # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

        def p_contour(self, data):
            data = self.del_data(data)
            self.get_ranges(data, self.ranges_reconstrcution_button_contour)

            cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)
            cbs = self.ax.clabel(
                cb,  fontsize=self.clabel_size,# Typically best results when labelling line contours.
                colors=['black'],
                inline=True,  # Cut the line where the label will be placed.
                fmt=self.fmt_contour,  # Labes as integers, with some extra space.
            )
            # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

        def p_quiver(self, u, v):
            u = self.del_data(u)
            v = self.del_data(v)

            # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
            qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                                 , transform=self.crs, **self.quiver_arg)
            # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
            #                      , transform=self.crs)
            qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                                   coordinates='axes')

        def lat_lon_shape(self):

            # self.ax.gridlines(draw_labels=True)
            # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
            self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
            self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

            from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
            lon_formatter = LongitudeFormatter(zero_direction_label=True)
            lat_formatter = LatitudeFormatter()
            self.ax.xaxis.set_major_formatter(lon_formatter)
            self.ax.yaxis.set_major_formatter(lat_formatter)
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader

            # shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
            #                                ccrs.PlateCarree(),
            #                                edgecolor='k', facecolor='none', linewidths=0.55)
            # self.ax.add_feature(shape_feature, alpha=0.5)
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))
    from matplotlib import pyplot as plt
    import numpy as np
    import mat73
    a = mat73.loadmat(r'D:\OneDrive\a_matlab\texas_era5\output_int6hr_Texas-25deg/20210214res_6hr.mat')
    long_row = a['lon_H'][0,:]
    lat = a['lat_H'][:,0]

    rr14,long = add_cyclic_point(a['PWm_loc_eul'], coord=long_row)
    den14,_ = add_cyclic_point(a['traj_den_eul'], coord=long_row)
    a = mat73.loadmat(r'D:\OneDrive\a_matlab\texas_era5\output_int6hr_Texas-25deg/20210215res_6hr.mat')
    rr15,_ = add_cyclic_point(a['PWm_loc_eul'], coord=long_row)
    den15,_ = add_cyclic_point(a['traj_den_eul'], coord=long_row)

    # def mask0(data):
    #     data[np.where(data==0)] = np.nan
    #     return data
    # den14 = mask0(den14)
    # den15 = mask0(den15)
    # rr14 = mask0(rr14)
    # rr15 = mask0(rr15)
    # den[np.where(den==0)] = np.nan
    fig, ax = plt.subplots(2, 2, figsize=(6, 4), subplot_kw={'projection':ccrs.PlateCarree(central_longitude=250)}, sharex='col', sharey='row')
    plt.subplots_adjust(top=0.99,
                        bottom=0.06,
                        left=0.09,
                        right=0.96,
                        hspace=0.2,
                        wspace=0.2)

    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')
    def single_plot(ax, data, vmax, text, title, boundline, pad):
        cb = ax.contourf(long, lat, data, levels=np.linspace(0, vmax[-1], 20), extend='max', cmap=cmpas_zhong, transform=ccrs.PlateCarree())
        cbar = plt.colorbar(cb, ax=ax, extend='max', orientation='horizontal',aspect=30, shrink=0.8, pad=pad)
        sax = cbar.ax
        sax.annotate(text, xy=(0.38, -5.5), xycoords='axes fraction')
        cbar.set_ticks(vmax)
        ranges = [False, np.arange(70, -21, -20), np.arange(130, 310, 50)]
        a = circulation(ax, lon=np.arange(100,360, 0.5), lat=np.arange(90,-90.5,-0.5), ranges=ranges)

        a.lat_lon_shape()
        ax.coastlines(linewidth=0.5)
        # ax.add_feature(cfeature.LAND, alpha=0.3)
        ax.add_feature(cfeature.OCEAN, alpha=0.3)
        ax.set_title(title, fontsize=10)

        ax.yaxis.set_label_position("right")
        ax.plot( boundline[:,0], boundline[:,1], color='red', transform=ccrs.PlateCarree(), linewidth=0.5)
        ax.set_extent([120, 310, -10, 70], crs=ccrs.PlateCarree())
        from cartopy.feature import ShapelyFeature
        from cartopy.io.shapereader import Reader
        shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)


    boundline = mat73.loadmat(r'D:\OneDrive\a_matlab\texas_era5\Texas-25deg_Inputinfo14.mat')['boundline']
    pad = 0.06
    single_plot(ax[0][0], den14, np.arange(0,20.1,5), 'count/day', 'a) Trajectory count',boundline, pad)
    single_plot(ax[0][1], rr14, np.arange(0,2.1,0.5), 'mm/day', 'b) Moisture contribution',boundline, pad)
    boundline = mat73.loadmat(r'D:\OneDrive\a_matlab\texas_era5\Texas-25deg_Inputinfo15.mat')['boundline']
    pad = 0.175
    single_plot(ax[1][0], den15, np.arange(0,20.1,5), 'count/day', 'c) Trajectory count',boundline, pad)
    single_plot(ax[1][1], rr15, np.arange(0,2.1,0.5), 'mm/day', 'd) Moisture contribution',boundline, pad)

    ax[0][1].annotate('14th Feb.', xy=(1.01, 0.18), xycoords='axes fraction', rotation=-90)
    ax[1][1].annotate('15th Feb.', xy=(1.01, 0.18), xycoords='axes fraction', rotation=-90)
    # plt.show()


    plt.savefig('den.png', dpi=800)
    plt.close()

one()