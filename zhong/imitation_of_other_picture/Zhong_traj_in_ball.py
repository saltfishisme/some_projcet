import os

import cdsapi
import xarray as xr
import scipy.io as scio
import matplotlib.pyplot as plt
# import mat73
import pandas as pd

import scipy.io as scio
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import cartopy.feature as cfeature

import sys
import cmaps
from cartopy.util import add_cyclic_point


def plot_trajectory(ax, projection_name, time, info_Path=False, shp_China=False, shp_US=False):
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))



    ################################# prepare data ################################

    a = scio.loadmat(r'%soutput_int6hr_%s/%sres_6hr.mat'%(projection_Path, projection_name, time))
    long = a['lon_H'][0,:]
    lat = a['lat_H'][:,0]
    pos_int=a['pos_int']
    rr_int=a['rr_int']


    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/rainbow200.mat', 'rainbow200')
    def plot_colored(ax, x, y, data, cmap, steps=1):
        '''
        :param ax: axes
        :param x: (N,) np.array
        :param y: (N,) np.array
        :param data: (N,) np.array
        :param cmap:
        :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
        :return:
        '''
        ''' from matplotlib import cm
        cmpa should be    cm.get_cmap(cmpas_zhong, 100)

         $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
         before plot all data, please normalized all this data.'''
        data = np.asarray(data)
        it=0
        while it<(data.size-steps):
            x_segm = x[it:it+steps+1]
            y_segm = y[it:it+steps+1]

            c_segm = cmap(data[it + steps // 2])
            ax.plot(x_segm, y_segm, c=c_segm, transform=ccrs.Geodetic(), linewidth=0.1)
            it += steps

    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len+1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :]=np.arange(0,101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb

    ####### plot colorbar in given cmap  ##########
    cb_ax = fig.add_axes([0.1,0.02,0.8,0.01])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    cb_ax.set_axes_locator(ip)
    cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
    cbar.ax.annotate('%', xy=(0.45, -7.5), xycoords='axes fraction')
    cbar.set_ticks(np.arange(0,101,25))
    ####### plot trajectories by colored lines   ##########
    from matplotlib import cm
    viridis = cm.get_cmap(cmpas_zhong, 500)
    for i in range(pos_int.shape[2])[::lineplot]:
    # for i in range(200):
        plot_colored(ax, pos_int[0,:,i], pos_int[1,:,i], rr_int[:,i], cmap=cmpas_zhong)

    ####### plot end of trajectories with black dot  ##########
    ax.scatter(pos_int[0,-1,:], pos_int[1,-1,:], c='k', s=1, marker='.',linewidths=0.01,transform=ccrs.PlateCarree())


    ################################# add some feature #################################

    ax.coastlines(linewidth=0.5)
    ax.set_global()
    ax.gridlines(linewidth=0.5, colors='k')

    ######### plot boundlines seed region ##########
    if info_Path is False:
        info_Path = r'%s%s_Inputinfo.mat' %(projection_Path, projection_name)
    print(info_Path)
    boundline = scio.loadmat(info_Path)['boundline']
    ax.plot( boundline[:,0], boundline[:,1], color='red', transform=ccrs.PlateCarree(), linewidth=0.5)

    ############# add shp ###############
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader
    if shp_China is not False:
        shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)
    if shp_US is not False:
        shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.55)
        ax.add_feature(shape_feature, alpha=0.5)


    ################################# plot ERA5 TOPO ###################################

    cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    lat25 = topo['lat25']; lon25=topo['lon25']; topo25=topo['topo25']
    ax.pcolormesh(lon25, lat25, topo25, shading='nearest',vmin=-6000, vmax=6000,cmap=cmap_topo, transform=ccrs.PlateCarree(), zorder=0)


def plot_trajectory_simple(ax, pos_int):
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return ListedColormap(np.concatenate((colors, ones.T), axis=1))

    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    path_color_zhong = 'D:/MATLAB/colormapdata/rainbow200.mat'
    cmpas_zhong = call_colors_in_mat(path_color_zhong, 'rainbow200')
    def plot_colored(ax, x, y, cmap, steps=1):
        '''
        :param ax: axes
        :param x: (N,) np.array
        :param y: (N,) np.array
        :param data: (N,) np.array
        :param cmap:
        :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
        :return:
        '''
        ''' from matplotlib import cm
        cmpa should be    cm.get_cmap(cmpas_zhong, 100)

         $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
         before plot all data, please normalized all this data.'''
        data = np.arange(x.shape[0])
        it=0
        while it<(data.size-steps):
            x_segm = x[it:it+steps+1]
            y_segm = y[it:it+steps+1]

            c_segm = cmap(data[it + steps // 2])
            ax.plot(x_segm, y_segm, c=c_segm, transform=ccrs.Geodetic(), linewidth=0.1)
            it += steps
    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len+1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :]=np.arange(0,101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb

    ####### plot colorbar in given cmap  ##########
    cb_ax = fig.add_axes([0.1,0.02,0.8,0.01])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    cb_ax.set_axes_locator(ip)
    cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
    cbar.ax.annotate('%', xy=(0.45, -9.5), xycoords='axes fraction')
    cbar.set_ticks(np.arange(0,101,25))
    ####### plot trajectories by colored lines   ##########
    from matplotlib import cm
    viridis = cm.get_cmap(cmpas_zhong, 500)
    for i in range(pos_int.shape[2])[::lineplot]:
        # for i in range(200):
        # plot_colored(ax, pos_int[0,:,i], pos_int[1,:,i], cmap=cmpas_zhong)
        ax.plot(pos_int[0,:,i], pos_int[1,:,i], color='black', transform=ccrs.Geodetic(), linewidth=0.1)
    ####### plot end of trajectories with black dot  ##########
    ax.scatter(pos_int[0,0,:], pos_int[1,0,:], c='k', s=1, marker='.',linewidths=0.5,transform=ccrs.PlateCarree())


    ################################# add some feature #################################

    ax.coastlines(linewidth=0.5)
    ax.set_global()
    ax.gridlines(linewidth=0.5, colors='k')



    ################################# plot ERA5 TOPO ###################################

    # cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    # topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    # lat25 = topo['lat25']; lon25=topo['lon25']; topo25=topo['topo25']
    # ax.pcolormesh(lon25, lat25, topo25, shading='nearest',vmin=-6000, vmax=6000,cmap=cmap_topo, transform=ccrs.PlateCarree(), zorder=0)

def plot_trajectory_with_rr(ax, pos_int, rr_int):
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))


    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    path_color_zhong = 'D:/MATLAB/colormapdata/rainbow200.mat'
    cmpas_zhong = call_colors_in_mat(path_color_zhong, 'rainbow200')

    def plot_colored(ax, x, y, data, cmap, steps=1):
        '''
        :param ax: axes
        :param x: (N,) np.array
        :param y: (N,) np.array
        :param data: (N,) np.array
        :param cmap:
        :param steps: 1 <= steps <= data.size, if steps is 1, every segment of data will be plotted with different colors.
        :return:
        '''
        ''' from matplotlib import cm
        cmpa should be    cm.get_cmap(cmpas_zhong, 100)

         $$$$$  $$$$$$  $$$$$$$$$$$$$$$$
         before plot all data, please normalized all this data.'''
        data = np.asarray(data)
        it = 0
        while it < (data.size - steps):
            x_segm = x[it:it + steps + 1]
            y_segm = y[it:it + steps + 1]

            c_segm = cmap(data[it + steps // 2])
            ax.plot(x_segm, y_segm, c=c_segm, transform=ccrs.Geodetic(), linewidth=0.1)
            it += steps
    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len+1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :]=np.arange(0,101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb

    ####### plot colorbar in given cmap  ##########
    cb_ax = fig.add_axes([0.1,0.02,0.8,0.01])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    cb_ax.set_axes_locator(ip)
    cbar = plt.colorbar(plot_colorbar(cmpas_zhong), orientation="horizontal", cax=cb_ax)
    cbar.ax.annotate('%', xy=(0.45, -9.5), xycoords='axes fraction')
    cbar.set_ticks(np.arange(0,101,25))
    ####### plot trajectories by colored lines   ##########
    from matplotlib import cm
    viridis = cm.get_cmap(cmpas_zhong, 500)
    for i in range(pos_int.shape[2])[::lineplot]:
        # for i in range(200):
        plot_colored(ax, pos_int[0,:,i], pos_int[1,:,i], rr_int[:,i], cmap=cmpas_zhong)
    ####### plot end of trajectories with black dot  ##########
    ax.scatter(pos_int[0,0,:], pos_int[1,0,:], c='k', s=1, marker='.',linewidths=0.5,transform=ccrs.PlateCarree())


    ################################# add some feature #################################

    ax.coastlines(linewidth=0.5)
    ax.set_global()
    ax.gridlines(linewidth=0.5, colors='k')



    ################################# plot ERA5 TOPO ###################################

    cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    lat25 = topo['lat25']; lon25=topo['lon25']; topo25=topo['topo25']
    ax.pcolormesh(lon25, lat25, topo25, shading='nearest',vmin=-6000, vmax=6000,cmap=cmap_topo, transform=ccrs.PlateCarree(), zorder=0)


def plot_rr_or_den(ax, projection_name, time, use_var_name, vmax=20, info_Path=False, shp_China=False, shp_US=False, colorbar=True, ranges=[False, np.arange(70, -21, -20), np.arange(130, 310, 50)]):
    class circulation():
        def __init__(self, ax, lon, lat, ranges):
            self.ax = ax
            self.crs = ccrs.PlateCarree()
            self.cmaps = ''
            self.colorbar=True
            self.orientation='h'
            self.ranges = ranges
            self.fmt_contourf='%.1f'
            self.fmt_contour='%.1f'
            self.ranges_reconstrcution_button_contourf = False
            self.ranges_reconstrcution_button_contour = False
            self.linewidths=1
            self.clabel_size=5
            self.cmaps=cmaps.WhiteGreen
            self.colorbar=True
            self.orientation='h'
            self.spacewind=[1,1]
            self.colors=False
            self.print_min_max=False
            self.contour_arg={'colors':'black'}
            self.contourf_arg={}
            self.quiverkey_arg={'X':0.85, 'Y':1.05, 'U':10, 'label': r'$10 \frac{m}{s}$'}
            self.contour_clabel_color = 'white'
            self.MidPointNorm = False
            def del_ll(range, lat, lon):
                def ll_del(lat, lon, area):
                    import numpy as np
                    lat0 = lat - area[3]
                    slat0 = int(np.argmin(abs(lat0)))
                    lat1 = lat - area[2]
                    slat1 = int(np.argmin(abs(lat1)))
                    lon0 = lon - area[0]
                    slon0 = int(np.argmin(abs(lon0)))
                    lon1 = lon - area[1]
                    slon1 = int(np.argmin(abs(lon1)))
                    return [slat0, slat1, slon0, slon1]
                del_area = ll_del(lat, lon, range[0])
                # 人为重新排序
                if del_area[0] > del_area[1]:
                    del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
                if del_area[2] > del_area[3]:
                    del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

                lat = lat[del_area[0]:del_area[1]+1]
                lon = lon[del_area[2]:del_area[3] + 1]
                return lat, lon, del_area
            if self.ranges[0] is not False:
                self.lat,  self.lon, self.del_area=del_ll(ranges, lat, lon)
            else:
                self.lat,  self.lon, self.del_area = [lat, lon, False]

        def del_data(self, data):
            if self.del_area is not False:
                return data[self.del_area[0]:self.del_area[1]+1, self.del_area[2]:self.del_area[3] + 1]
            else:
                return data

        def get_ranges(self, data, ranges_reconstrcution_button):
            if self.print_min_max:
                print(np.nanmin(data), np.nanmax(data))
            from numpy import ma
            from matplotlib import cbook
            from matplotlib.colors import Normalize
            def ranges_create(begin, end, inter):
                class MidPointNorm(Normalize):
                    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                        Normalize.__init__(self, vmin, vmax, clip)
                        self.midpoint = midpoint

                    def __call__(self, value, clip=None):
                        if clip is None:
                            clip = self.clip

                        result, is_scalar = self.process_value(value)

                        self.autoscale_None(result)
                        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                        if not (vmin < midpoint < vmax):
                            raise ValueError("midpoint must be between maxvalue and minvalue.")
                        elif vmin == vmax:
                            result.fill(0)  # Or should it be all masked? Or 0.5?
                        elif vmin > vmax:
                            raise ValueError("maxvalue must be bigger than minvalue")
                        else:
                            vmin = float(vmin)
                            vmax = float(vmax)
                            if clip:
                                mask = ma.getmask(result)
                                result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                                  mask=mask)

                            # ma division is very slow; we can take a shortcut
                            resdat = result.data

                            # First scale to -1 to 1 range, than to from 0 to 1.
                            resdat -= midpoint
                            resdat[resdat > 0] /= abs(vmax - midpoint)
                            resdat[resdat < 0] /= abs(vmin - midpoint)

                            resdat /= 2.
                            resdat += 0.5
                            result = ma.array(resdat, mask=result.mask, copy=False)

                        if is_scalar:
                            result = result[0]
                        return result

                    def inverse(self, value):
                        if not self.scaled():
                            raise ValueError("Not invertible until scaled")
                        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                        if cbook.iterable(value):
                            val = ma.asarray(value)
                            val = 2 * (val - 0.5)
                            val[val > 0] *= abs(vmax - midpoint)
                            val[val < 0] *= abs(vmin - midpoint)
                            val += midpoint
                            return val
                        else:
                            val = 2 * (value - 0.5)
                            if val < 0:
                                return val * abs(vmin - midpoint) + midpoint
                            else:
                                return val * abs(vmax - midpoint) + midpoint
                levels1 = np.arange(begin, 0, inter)
                # levels1 = np.insert(levels1, 0, begin_b)

                levels1 = np.append(levels1, 0)
                levels2 = np.arange(0+inter, end, inter)
                # levels2 = np.append(levels2, end_b)
                levels = np.append(levels1, levels2)
                return levels, MidPointNorm(0)

            if isinstance(ranges_reconstrcution_button, list)  or (type(ranges_reconstrcution_button) is np.ndarray):
                self.levels = ranges_reconstrcution_button
            else:

                ranges_min = np.nanmin(data); ranges_max = np.nanmax(data)
                if ranges_reconstrcution_button is False:
                    if ranges_min>0:
                        self.levels = np.linspace(ranges_min, ranges_max, 30)
                        return
                    ticks = (ranges_max-ranges_min)/30
                else:
                    ticks = ranges_reconstrcution_button
                self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

        def p_contourf(self, data):
            data = self.del_data(data)
            self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

            if self.MidPointNorm is not False:
                self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', norm=self.MidPointNorm, **self.contourf_arg)

            else:
                self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', **self.contourf_arg)



            if self.colorbar:
                if self.orientation == 'h':
                    orientation='horizontal'
                else:
                    orientation='vertical'
                self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                         shrink=0.9, ax=self.ax, format=self.fmt_contourf)

                # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

        def p_contour(self, data):
            data = self.del_data(data)
            self.get_ranges(data, self.ranges_reconstrcution_button_contour)

            cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)
            cbs = self.ax.clabel(
                cb,  fontsize=self.clabel_size,# Typically best results when labelling line contours.
                colors=['black'],
                inline=True,  # Cut the line where the label will be placed.
                fmt=self.fmt_contour,  # Labes as integers, with some extra space.
            )
            # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

        def p_quiver(self, u, v):
            u = self.del_data(u)
            v = self.del_data(v)

            # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
            qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                                 , transform=self.crs, **self.quiver_arg)
            # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
            #                      , transform=self.crs)
            qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                                   coordinates='axes')

        def lat_lon_shape(self):

            # self.ax.gridlines(draw_labels=True)
            # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
            self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
            self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

            from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
            lon_formatter = LongitudeFormatter(zero_direction_label=True)
            lat_formatter = LatitudeFormatter()
            self.ax.xaxis.set_major_formatter(lon_formatter)
            self.ax.yaxis.set_major_formatter(lat_formatter)
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader

            # shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
            #                                ccrs.PlateCarree(),
            #                                edgecolor='k', facecolor='none', linewidths=0.55)
            # self.ax.add_feature(shape_feature, alpha=0.5)
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))
    from matplotlib import pyplot as plt
    import numpy as np



    ################################# prepare data ################################

    a = scio.loadmat(r'%soutput_int6hr_%s/%sres_6hr.mat'%(projection_Path, projection_name, time))
    long_row = a['lon_H'][0,:]
    lat = a['lat_H'][:,0]
    data=a[use_var_name]


    # if use_var_name == 'PWm_loc_eul':
    # data = np.array(data, dtype='float64')
    # data[data<0]=np.nan
    # data[data<0.1] = 0.09
    # print(np.nanmin(data))
    # # long = long_row
    data,long = add_cyclic_point(data, coord=long_row)
    # #
    # data = np.roll(data, 720, axis=1)
    #
    # long_row = np.arange(0,180.01,0.25)
    # long_row = np.insert(long_row, 0, np.arange(-179.75,0,0.25))
    # long = np.append(long_row,  -179.75)
    # print(np.array([data[:,-1]]).shape)
    # data = np.append(data, np.array([data[:,-1]]), axis=1)
    # print(long_row)
    # data, long = add_cyclic_point(data, coord=long_row)
    # print(data.shape)
    # print(long)
    # exit(0)
    # data_new = np.zeros([data.shape[0], data.shape[1]+1])
    # data_new[:, -721:-1] = data[:,:720]
    # data_new[:, :720] = data[:, -720:]
    # long_row = np.arange(0,180.01,0.25)
    # long_row = np.insert(long_row, 0, np.arange(-179.75,0,0.25))
    # long_row = np.append(long_row,  -179.75)
    # print(long_row)
    # data_new[:,-1] = data_new[:,0]
    #
    # long = long_row
    # data = data_new

    # data, long = add_cyclic_point(data, coord=long_row)
    # long = long_row

    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')

    if use_var_name == 'PWm_loc_eul':
        cbar_unit = 'mm/day'
    elif use_var_name == 'traj_den_eul':
        cbar_unit = 'count/day'


    #################################### plot var #####################################

    # cb = ax.pcolormesh(long, lat, data, cmap=cmpas_zhong, transform=ccrs.PlateCarree(), vmax=vmax[-1], shading='nearest')
    cb = ax.contourf(long, lat, data, levels=np.linspace(0, vmax[-1], 100), extend='max', cmap=cmpas_zhong, transform=ccrs.PlateCarree())
    if colorbar:
        cbar = plt.colorbar(cb, ax=ax, extend='max', orientation='horizontal',aspect=30, shrink=0.8, pad=0.175)
        sax = cbar.ax
        sax.annotate(cbar_unit, xy=(0.38, -5.5), xycoords='axes fraction')
        cbar.set_ticks(vmax)

    #################################### add some feature #####################################

    a = circulation(ax, lon=np.arange(100,360, 0.5), lat=np.arange(90,-90.5,-0.5), ranges=ranges)
    # a.lat_lon_shape()
    ax.coastlines(linewidth=0.5)
    ax.add_feature(cfeature.OCEAN, alpha=0.3)

    ######### plot boundlines seed region ##########
    if info_Path is False:
        info_Path = r'%s%s_Inputinfo.mat' %(projection_Path, projection_name)
    boundline = scio.loadmat(info_Path)['boundline']
    ax.plot( boundline[:,0], boundline[:,1], color='red', transform=ccrs.PlateCarree(), linewidth=0.5)

    ############# add shp ###############
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader
    if shp_China is not False:
        shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)
    if shp_US is not False:
        shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)

    # ############### set ranges ##############
    if ranges[0] is not False:
        ax.set_extent(ranges[0], crs=ccrs.PlateCarree())

    return cb

def plot_rr_or_den_in_Oth(ax, projection_name, time, use_var_name, vmax=20, info_Path=False, shp_China=False, shp_US=False, colorbar=True, ranges=[False, np.arange(70, -21, -20), np.arange(130, 310, 50)]):
    class circulation():
        def __init__(self, ax, lon, lat, ranges):
            self.ax = ax
            self.crs = ccrs.PlateCarree()
            self.cmaps = ''
            self.colorbar=True
            self.orientation='h'
            self.ranges = ranges
            self.fmt_contourf='%.1f'
            self.fmt_contour='%.1f'
            self.ranges_reconstrcution_button_contourf = False
            self.ranges_reconstrcution_button_contour = False
            self.linewidths=1
            self.clabel_size=5
            self.cmaps=cmaps.WhiteGreen
            self.colorbar=True
            self.orientation='h'
            self.spacewind=[1,1]
            self.colors=False
            self.print_min_max=False
            self.contour_arg={'colors':'black'}
            self.contourf_arg={}
            self.quiverkey_arg={'X':0.85, 'Y':1.05, 'U':10, 'label': r'$10 \frac{m}{s}$'}
            self.contour_clabel_color = 'white'
            self.MidPointNorm = False
            def del_ll(range, lat, lon):
                def ll_del(lat, lon, area):
                    import numpy as np
                    lat0 = lat - area[3]
                    slat0 = int(np.argmin(abs(lat0)))
                    lat1 = lat - area[2]
                    slat1 = int(np.argmin(abs(lat1)))
                    lon0 = lon - area[0]
                    slon0 = int(np.argmin(abs(lon0)))
                    lon1 = lon - area[1]
                    slon1 = int(np.argmin(abs(lon1)))
                    return [slat0, slat1, slon0, slon1]
                del_area = ll_del(lat, lon, range[0])
                # 人为重新排序
                if del_area[0] > del_area[1]:
                    del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
                if del_area[2] > del_area[3]:
                    del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]

                lat = lat[del_area[0]:del_area[1]+1]
                lon = lon[del_area[2]:del_area[3] + 1]
                return lat, lon, del_area
            if self.ranges[0] is not False:
                self.lat,  self.lon, self.del_area=del_ll(ranges, lat, lon)
            else:
                self.lat,  self.lon, self.del_area = [lat, lon, False]

        def del_data(self, data):
            if self.del_area is not False:
                return data[self.del_area[0]:self.del_area[1]+1, self.del_area[2]:self.del_area[3] + 1]
            else:
                return data

        def get_ranges(self, data, ranges_reconstrcution_button):
            if self.print_min_max:
                print(np.nanmin(data), np.nanmax(data))
            from numpy import ma
            from matplotlib import cbook
            from matplotlib.colors import Normalize
            def ranges_create(begin, end, inter):
                class MidPointNorm(Normalize):
                    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
                        Normalize.__init__(self, vmin, vmax, clip)
                        self.midpoint = midpoint

                    def __call__(self, value, clip=None):
                        if clip is None:
                            clip = self.clip

                        result, is_scalar = self.process_value(value)

                        self.autoscale_None(result)
                        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                        if not (vmin < midpoint < vmax):
                            raise ValueError("midpoint must be between maxvalue and minvalue.")
                        elif vmin == vmax:
                            result.fill(0)  # Or should it be all masked? Or 0.5?
                        elif vmin > vmax:
                            raise ValueError("maxvalue must be bigger than minvalue")
                        else:
                            vmin = float(vmin)
                            vmax = float(vmax)
                            if clip:
                                mask = ma.getmask(result)
                                result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                                  mask=mask)

                            # ma division is very slow; we can take a shortcut
                            resdat = result.data

                            # First scale to -1 to 1 range, than to from 0 to 1.
                            resdat -= midpoint
                            resdat[resdat > 0] /= abs(vmax - midpoint)
                            resdat[resdat < 0] /= abs(vmin - midpoint)

                            resdat /= 2.
                            resdat += 0.5
                            result = ma.array(resdat, mask=result.mask, copy=False)

                        if is_scalar:
                            result = result[0]
                        return result

                    def inverse(self, value):
                        if not self.scaled():
                            raise ValueError("Not invertible until scaled")
                        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

                        if cbook.iterable(value):
                            val = ma.asarray(value)
                            val = 2 * (val - 0.5)
                            val[val > 0] *= abs(vmax - midpoint)
                            val[val < 0] *= abs(vmin - midpoint)
                            val += midpoint
                            return val
                        else:
                            val = 2 * (value - 0.5)
                            if val < 0:
                                return val * abs(vmin - midpoint) + midpoint
                            else:
                                return val * abs(vmax - midpoint) + midpoint
                levels1 = np.arange(begin, 0, inter)
                # levels1 = np.insert(levels1, 0, begin_b)

                levels1 = np.append(levels1, 0)
                levels2 = np.arange(0+inter, end, inter)
                # levels2 = np.append(levels2, end_b)
                levels = np.append(levels1, levels2)
                return levels, MidPointNorm(0)

            if isinstance(ranges_reconstrcution_button, list)  or (type(ranges_reconstrcution_button) is np.ndarray):
                self.levels = ranges_reconstrcution_button
            else:

                ranges_min = np.nanmin(data); ranges_max = np.nanmax(data)
                if ranges_reconstrcution_button is False:
                    if ranges_min>0:
                        self.levels = np.linspace(ranges_min, ranges_max, 30)
                        return
                    ticks = (ranges_max-ranges_min)/30
                else:
                    ticks = ranges_reconstrcution_button
                self.levels, self.MidPointNorm = ranges_create(ranges_min, ranges_max, ticks)

        def p_contourf(self, data):
            data = self.del_data(data)
            self.get_ranges(data, self.ranges_reconstrcution_button_contourf)

            if self.MidPointNorm is not False:
                self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', norm=self.MidPointNorm, **self.contourf_arg)

            else:
                self.cb = self.ax.contourf(self.lon, self.lat, data, levels=self.levels, transform=self.crs, extend='both', **self.contourf_arg)



            if self.colorbar:
                if self.orientation == 'h':
                    orientation='horizontal'
                else:
                    orientation='vertical'
                self.cbar = plt.colorbar(self.cb, extend='both', orientation=orientation,
                                         shrink=0.9, ax=self.ax, format=self.fmt_contourf)

                # cbar.ax.set_xticklabels(['Low', 'Medium', 'High'])  # horizontal colorbar

        def p_contour(self, data):
            data = self.del_data(data)
            self.get_ranges(data, self.ranges_reconstrcution_button_contour)

            cb = self.ax.contour(self.lon, self.lat, data, levels=self.levels, transform=self.crs, **self.contour_arg)
            cbs = self.ax.clabel(
                cb,  fontsize=self.clabel_size,# Typically best results when labelling line contours.
                colors=['black'],
                inline=True,  # Cut the line where the label will be placed.
                fmt=self.fmt_contour,  # Labes as integers, with some extra space.
            )
            # [txt.set_bbox(dict(facecolor=self.contour_clabel_color, edgecolor='none', pad=0)) for txt in cbs]

        def p_quiver(self, u, v):
            u = self.del_data(u)
            v = self.del_data(v)

            # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
            qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
                                 , transform=self.crs, **self.quiver_arg)
            # qui = self.ax.quiver(self.lon[::self.spacewind[0]], self.lat[::self.spacewind[1]], u[::self.spacewind[1], ::self.spacewind[0]], v[::self.spacewind[1], ::self.spacewind[0]]
            #                      , transform=self.crs)
            qk = self.ax.quiverkey(qui, **self.quiverkey_arg, labelpos='E',
                                   coordinates='axes')

        def lat_lon_shape(self):

            # self.ax.gridlines(draw_labels=True)
            # ax.set_extent((self.ranges[0][2], self.ranges[0][3], self.ranges[0][0], self.ranges[0][1]), crs=ccrs.PlateCarree())
            self.ax.set_xticks(self.ranges[2], crs=ccrs.PlateCarree())
            self.ax.set_yticks(self.ranges[1], crs=ccrs.PlateCarree())

            from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
            lon_formatter = LongitudeFormatter(zero_direction_label=True)
            lat_formatter = LatitudeFormatter()
            self.ax.xaxis.set_major_formatter(lon_formatter)
            self.ax.yaxis.set_major_formatter(lat_formatter)
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader

            # shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
            #                                ccrs.PlateCarree(),
            #                                edgecolor='k', facecolor='none', linewidths=0.55)
            # self.ax.add_feature(shape_feature, alpha=0.5)
    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))
    from matplotlib import pyplot as plt
    import numpy as np



    ################################# prepare data ################################

    a = scio.loadmat(r'%soutput_int6hr_%s/%sres_6hr.mat'%(projection_Path, projection_name, time))
    long_row = a['lon_H'][0,:]
    lat = a['lat_H'][:,0]
    data=a[use_var_name]


    # if use_var_name == 'PWm_loc_eul':
    # data = np.array(data, dtype='float64')
    # data[data<0]=np.nan
    # data[data<0.1] = 0.09
    # print(np.nanmin(data))
    # # long = long_row
    data,long = add_cyclic_point(data, coord=long_row)



    if use_var_name == 'PWm_loc_eul':
        cbar_unit = 'mm/day'
    elif use_var_name == 'traj_den_eul':
        cbar_unit = 'count/day'


    ################################## plot trajectory ###############################

    import matplotlib.pyplot as plt
    import numpy as np
    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/ColorSodemannB.mat', 'ColorSD')
    def plot_colorbar(cmap):
        date_len = 10
        date_len = date_len+1
        import numpy as np
        import matplotlib.pyplot as plt
        Z = np.zeros([6, date_len])
        Z[0, :]=np.arange(0,101, 10)
        x = np.arange(-0.5, date_len, 1)  # len = 21
        y = np.arange(4.5, 11, 1)  # len = 7

        ax0 = fig.add_axes([0.1, 0.2, 0.01, 0.6])
        cb = ax0.pcolormesh(x, y, Z, cmap=cmap)

        ax0.set_visible(False)
        return cb



    ################################# add some feature #################################

    ax.coastlines(linewidth=0.5)
    ax.set_global()
    ax.gridlines(linewidth=0.5, colors='k')

    ################################# plot ERA5 TOPO ###################################

    cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    lat25 = topo['lat25']; lon25=topo['lon25']; topo25=topo['topo25']
    ax.pcolormesh(lon25, lat25, topo25, shading='nearest',vmin=-6000, vmax=6000,cmap=cmap_topo, transform=ccrs.PlateCarree(), zorder=0)

    #################################### plot var #####################################

    # cb = ax.pcolormesh(long, lat, data, cmap=cmpas_zhong, transform=ccrs.PlateCarree(), vmax=vmax[-1], shading='nearest')
    cb = ax.contourf(long, lat, data, levels=np.linspace(0, vmax[-1], 20), extend='max', cmap=cmpas_zhong, transform=ccrs.PlateCarree())

    ####### plot colorbar in given cmap  ##########
    cb_ax = fig.add_axes([0.1,0.02,0.8,0.01])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.1, -0.05, 0.8, 0.02])
    cb_ax.set_axes_locator(ip)
    cbar = plt.colorbar(cb, orientation="horizontal", cax=cb_ax)
    cbar.ax.annotate(cbar_unit, xy=(0.35, -7.5), xycoords='axes fraction')
    cbar.set_ticks(vmax)

    ######### plot boundlines seed region ##########
    if info_Path is False:
        info_Path = r'%s%s_Inputinfo.mat' %(projection_Path, projection_name)
    boundline = scio.loadmat(info_Path)['boundline']
    ax.plot( boundline[:,0], boundline[:,1], color='red', transform=ccrs.PlateCarree(), linewidth=0.5)

    ax.coastlines(zorder=100, linewidth=0.3)
    ############# add shp ###############
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader
    if shp_China is not False:
        shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)
        shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)
    if shp_US is not False:
        shp_Path = r'C:\Users\zhaoh\.local\share\cartopy\shapefiles\natural_earth\cultural\ne_110m_admin_1_states_provinces_lakes_shp.shx'
        shape_feature = ShapelyFeature(Reader(shp_Path).geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.4)
        ax.add_feature(shape_feature)

    return


# time = '19891016'
# lineplot=1
# orth_central_ll = [
#     [79.5, 66.5],
# [145.5, 80.5],
# [43.5, 73.5],
# [293.5, 70.5]]
# days = [30, 20,20,20]
# path_dir = r'D:\AR_moisture_path_42/'
# for iI_path, i_path in enumerate(['1981010717_6.mat', '1981021911_3.mat', '1985011317_1.mat', '1986121923_1.mat']):
#     projection_Path = path_dir+i_path
#     projection_name = scio.loadmat(projection_Path)
#     keys = []
#     for i in projection_name.keys():
#         if i[0] == 'a':
#             keys.append(i)
#
#     rr = projection_name['rr'+keys[0][1:]]
#     projection_name = projection_name[keys[0]]
#
#     ranges = [[75,200,-10,50], np.arange(70, -21, -20), np.arange(90, 310, 30)]
#
#     import matplotlib.gridspec as gridspec
#     fig = plt.figure()
#     gs = gridspec.GridSpec(nrows=1, ncols=1)
#     plt.subplots_adjust(top=0.95,
# bottom=0.105,
# left=0.08,
# right=0.99,
# hspace=0.4,
# wspace=0.2)
#     # ax1 = fig.add_subplot(gs[0, :], projection=ccrs.PlateCarree())
#     print(np.median(projection_name[0,0,:]),np.median(projection_name[1,0,:]))
#
#     ax1 = fig.add_subplot(gs[0, :], projection=ccrs.Orthographic(orth_central_ll[iI_path][0], orth_central_ll[iI_path][1]))
#     # ax2 = fig.add_subplot(gs[1, 0], projection=ccrs.PlateCarree(central_longitude=120))
#     # ax3 = fig.add_subplot(gs[1, 1], projection=ccrs.PlateCarree(central_longitude=120))
#     ax1.set_aspect('equal')
#     # plot_trajectory(ax1, projection_name[:, :40, :])
#     plot_trajectory_with_rr(ax1, projection_name[:,:days[iI_path],:], rr)
#     # plot_rr_or_den(ax2, projection_name, time, use_var_name='PWm_loc_eul', vmax=np.arange(0,5.1,1), shp_China=True, ranges=ranges)
#     # plot_rr_or_den(ax3, projection_name, time, use_var_name='traj_den_eul', vmax=np.arange(0,10.1,2.5), shp_China=True, ranges=ranges)
#     # ax1.add_feature(cfeature.LAND)
#     # ax1.add_feature(cfeature.OCEAN)
#     # ax1.stock_img()
#     # ax1.set_title('a) Trajectory', fontsize=10)
#     # ax2.set_title('b) Moisture contribution', fontsize=10)
#     # ax3.set_title('c) Trajectory count', fontsize=10)
#     # plt.show()
#     # ax1.set_extent([0, 359, 40, 90], crs=ccrs.PlateCarree())
#     plt.savefig(r'D:/'+i_path[:-3]+'png', dpi=500)
#     plt.close()
# exit(0)


lineplot = 20

ranges = [[10,120,-10,50], np.arange(70, -21, -20), np.arange(90, 310, 30)]

import matplotlib.gridspec as gridspec

fig, axs = plt.subplots(nrows=1, ncols=2, subplot_kw={'projection':ccrs.Orthographic(50, 50)}, figsize=[5.5,3.5])
plt.subplots_adjust(top=0.935,
                    bottom=0.1,
                    left=0.06,
                    right=0.94,
                    hspace=0.2,
                    wspace=0.2)

projection_Path = r'D:\OneDrive\a_matlab\US_0.25_25/'
projection_name = scio.loadmat(projection_Path+'projectname.mat')['projname'][0]
plot_trajectory(axs[0], projection_name, '20221125', info_Path=False, shp_China=True)

projection_Path = r'D:\OneDrive\a_matlab\US_0.25_26/'
projection_name = scio.loadmat(projection_Path+'projectname.mat')['projname'][0]
plot_trajectory(axs[1], projection_name, '20221126', info_Path=False, shp_China=True)

figlabel = ['(d) 25 November', '(e)26 November']
for iI_ax, iax in enumerate(axs):
    iax.set_title(figlabel[iI_ax], loc='left')

    iax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False)
# ax1.set_title('a) Trajectory', fontsize=10)
# ax2.set_title('b) Moisture contribution', fontsize=10)
# ax3.set_title('c) Trajectory count', fontsize=10)

# def ll_ticks(ax):
#     ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
#     ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
#
#     from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
#
#     lon_formatter = LongitudeFormatter(zero_direction_label=True)
#     lat_formatter = LatitudeFormatter()
#     ax.xaxis.set_major_formatter(lon_formatter)
#     ax.yaxis.set_major_formatter(lat_formatter)
#
# for i_ax in [axs[0], axs[1]]:
#     ll_ticks(i_ax)
plt.show()
plt.savefig('China_trajectory.png', dpi=800)

exit(0)


time = '20221119'
projection_Path = r'D:\OneDrive\a_matlab\US_0.25/'
lineplot = 500
projection_name = scio.loadmat(projection_Path+'projectname.mat')['projname'][0]


ranges = [[10,120,-10,50], np.arange(70, -21, -20), np.arange(90, 310, 30)]

import matplotlib.gridspec as gridspec

fig, axs = plt.subplots(nrows=2, ncols=2, subplot_kw={'projection':ccrs.Orthographic(50, 50)},
                        figsize=[6,7], sharey='col')
plt.subplots_adjust(top=0.95,
bottom=0.15,
left=0.08,
right=0.94,
hspace=0.2,
wspace=0.2)

for iI_time, i_time in enumerate(['20221125', '20221126']):
    projection_Path = r'D:\OneDrive\a_matlab\US_0.25_%s/'%i_time[-2:]
    projection_name = scio.loadmat(projection_Path + 'projectname.mat',mat_dtype=True)['projname'][0]

    cb = plot_rr_or_den_in_Oth(axs[iI_time, 0], projection_name, i_time, use_var_name='PWm_loc_eul', colorbar=False,
                        vmax=np.arange(0,1.01,0.25), shp_China=True, ranges=ranges)


    cb = plot_rr_or_den_in_Oth(axs[iI_time, 1], projection_name, i_time, use_var_name='traj_den_eul', colorbar=False,
                        vmax=np.arange(0,8.1,1), shp_China=True, ranges=ranges)


axs[0, 0].set_title('Moisture contribution', fontsize=10)
axs[0, 1].set_title('Trajectory count', fontsize=10)

axs[0, 0].annotate('(a)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[0, 1].annotate('(b)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[1, 0].annotate('(c)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[1, 1].annotate('(d)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)

axs[0, 1].annotate('25 Nov.', xy=(1.02,0.38),rotation='vertical', xycoords='axes fraction', fontsize=10)
axs[1, 1].annotate('26 Nov.', xy=(1.02,0.38),rotation='vertical', xycoords='axes fraction', fontsize=10)


def ll_ticks(ax):
    ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
    ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())

    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter

    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)

for i_ax in [axs[0, 0], axs[1, 0],axs[2, 0]]:
    ll_ticks(i_ax)
plt.show()
plt.savefig('China_moisture.png', dpi=800)
exit(0)

'''#####################################################################'''
time = '20221119'
projection_Path = r'D:\OneDrive\a_matlab\US_0.25/'
lineplot = 1
projection_name = scio.loadmat(projection_Path+'projectname.mat',mat_dtype=True)['projname'][0]

ranges = [[160,300,30,80], np.arange(80, 29, -20), np.arange(180, 350, 30)]

import matplotlib.gridspec as gridspec

fig, axs = plt.subplots(nrows=1, ncols=3, subplot_kw={'projection':ccrs.Orthographic(220, 50)}, figsize=[8,3.5])
plt.subplots_adjust(top=0.95,
                    bottom=0.1,
                    left=0.01,
                    right=0.99,
                    hspace=0.2,
                    wspace=0.1)


plot_trajectory(axs[0], projection_name, '20221117', info_Path=False, shp_China=True)
plot_trajectory(axs[1], projection_name, '20221118', info_Path=False, shp_China=True)
plot_trajectory(axs[2], projection_name, '20221119', info_Path=False, shp_China=True)
# plot_rr_or_den(ax2, projection_name, time, use_var_name='PWm_loc_eul', vmax=np.arange(0,5.1,1), shp_China=True, ranges=ranges)
# plot_rr_or_den(ax3, projection_name, time, use_var_name='traj_den_eul', vmax=np.arange(0,10.1,2.5), shp_China=True, ranges=ranges)
figlabel = ['(a) 17 November', '(b)18 November', '(c) 19 November']
for iI_ax, iax in enumerate(axs):
    iax.set_title(figlabel[iI_ax], loc='left')
# ax1.set_title('a) Trajectory', fontsize=10)
# ax2.set_title('b) Moisture contribution', fontsize=10)
# ax3.set_title('c) Trajectory count', fontsize=10)
# plt.show()
plt.savefig('US_trajectory.png', dpi=800)

exit(0)


time = '20221119'
projection_Path = r'D:\OneDrive\a_matlab\US_0.25/'
lineplot = 1
projection_name = scio.loadmat(projection_Path+'projectname.mat')['projname'][0]

# a = scio.loadmat(r'%soutput_int6hr_%s/%sres_6hr.mat' % (projection_Path, projection_name, time))
# long_row = a['lon_H'][0, :]
# lat = a['lat_H'][:, 0]
# data1 = a['PWm_loc_eul']
# data2 = a['traj_den_eul']
# print(type(data1[0,0]))
# print(type(data2[0,0]))
# exit(0)


ranges = [[120,300,20,80], [70,55,40,25], np.arange(130, 340, 50)]

import matplotlib.gridspec as gridspec

fig, axs = plt.subplots(nrows=3, ncols=2, subplot_kw={'projection':ccrs.PlateCarree(central_longitude=220)},
                        figsize=[7,4.5], sharey='col')
plt.subplots_adjust(top=0.95,
bottom=0.15,
left=0.08,
right=0.94,
hspace=0.2,
wspace=0.2)

for iI_time, i_time in enumerate(['20221117', '20221118', '20221119']):
    cb = plot_rr_or_den(axs[iI_time, 0], projection_name, i_time, use_var_name='PWm_loc_eul', colorbar=False, vmax=np.arange(0,1.6,0.1), shp_China=True, ranges=ranges)

    if iI_time == 2:
        cb_ax = fig.add_axes([0.089, 0.09, 0.4, 0.01])
        cbar = plt.colorbar(cb, cax=cb_ax, extend='max', orientation='horizontal',aspect=30, shrink=0.8, pad=0.175)
        cb_ax.annotate('mm/day', xy=(0.38, -6), xycoords='axes fraction')
        cb_ax.set_xticks(np.arange(0,1.6,0.5))

    cb = plot_rr_or_den(axs[iI_time, 1], projection_name, i_time, use_var_name='traj_den_eul', colorbar=False, vmax=np.arange(0,5.1,0.5), shp_China=True, ranges=ranges)
    if iI_time == 2:
        cb_ax = fig.add_axes([0.54, 0.09, 0.4, 0.01])
        cbar = plt.colorbar(cb, cax=cb_ax, extend='max', orientation='horizontal',aspect=30, shrink=0.8, pad=0.175)
        cb_ax.annotate('count/day', xy=(0.38, -6.8), xycoords='axes fraction')
        cb_ax.set_xticks(np.arange(0,5.1,1))


axs[0, 0].set_title('Moisture contribution', fontsize=10)
axs[0, 1].set_title('Trajectory count', fontsize=10)

axs[0, 0].annotate('(a)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[0, 1].annotate('(b)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[1, 0].annotate('(c)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[1, 1].annotate('(d)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[2, 0].annotate('(e)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)
axs[2, 1].annotate('(f)', xy=(0.01, 0.89), xycoords='axes fraction', fontsize=8)

axs[0, 1].annotate('17 Nov.', xy=(1.02,0.3),rotation='vertical', xycoords='axes fraction', fontsize=10)
axs[1, 1].annotate('18 Nov.', xy=(1.02,0.3),rotation='vertical', xycoords='axes fraction', fontsize=10)
axs[2, 1].annotate('19 Nov.', xy=(1.02,0.3),rotation='vertical', xycoords='axes fraction', fontsize=10)

# def ll_ticks(ax):
#     ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
#     ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
#
#     from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
#
#     lon_formatter = LongitudeFormatter(zero_direction_label=True)
#     lat_formatter = LatitudeFormatter()
#     ax.xaxis.set_major_formatter(lon_formatter)
#     ax.yaxis.set_major_formatter(lat_formatter)
#
# for i_ax in [axs[0, 0], axs[1, 0],axs[2, 0]]:
#     ll_ticks(i_ax)
# plt.show()
plt.savefig('hengzhou.png', dpi=800)