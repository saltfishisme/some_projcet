import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import scipy.io
from scipy.interpolate import Rbf
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import cartopy.feature as cfeature
import matplotlib.path as mpath
import matplotlib.patches as mpatches
from matplotlib.path import Path
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import matplotlib.patches as mpatches
from shapely.geometry import Point, LineString,Polygon
import numpy as np

import nc4


def main():
    central_lon = 88
    central_lat = 55
    lat_boundary = 30

    # data = scipy.io.loadmat(r'G:\OneDrive\basis\some_projects\zhong\Forest_fire\figure3_1750-1899.mat')
    # # data = scipy.io.loadmat(r'D:\figure3_1900-2003.mat')
    # #
    # lat_small = np.squeeze(data['lat'])
    # lon_small = np.squeeze(data['lon'])
    #
    # lat = np.arange(90,-86.01, -1)
    # lon = np.arange(0,360.01,1)
    #
    # def regrid(sparse_grid, x, y, new_x, new_y):
    #     lon, lat = np.meshgrid(x, y)
    #     func = Rbf(lon, lat, sparse_grid, function='linear')
    #     new_x, new_y = np.meshgrid(new_x, new_y)
    #     data_new = func(new_x, new_y)
    #     return data_new
    #
    # import cartopy.util as cutil
    # hgt,_,_ = cutil.add_cyclic(data['f3a_M234_Hgt500'], lon_small, lat_small)
    # hgt_p,_,_  = cutil.add_cyclic(data['f3a_M234_Hgt500_p'], lon_small, lat_small)
    # t2m,_,_  = cutil.add_cyclic(data['f3a_M234_t2m'], lon_small, lat_small)
    # t2m_p, lon_small, lat_small = cutil.add_cyclic(data['f3a_M234_t2m_p'], lon_small, lat_small)
    #
    # hgt = regrid(hgt, lon_small, lat_small, lon, lat)
    # hgt_p  =regrid(hgt_p, lon_small, lat_small, lon, lat)
    # t2m  = regrid(t2m, lon_small, lat_small, lon, lat)
    # t2m_p = regrid(t2m_p, lon_small, lat_small, lon, lat)
    # print(lon.shape)
    #
    # scipy.io.savemat('test.mat', {'t2m':t2m, 't2m_p':t2m_p,
    #                               'hgt': hgt, 'hgt_p': hgt_p,
    #                               'lon':lon, 'lat':lat})

    data = scipy.io.loadmat('test.mat')
    t2m = data['t2m']
    t2m_p = data['t2m_p']
    hgt = data['hgt']
    hgt_p = data['hgt_p']
    lon = np.squeeze(data['lon'])
    print(lon)
    lat = np.squeeze(data['lat'])
    lon2d, lat2d = np.meshgrid(lon,lat)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Orthographic(central_lon, central_lat))

    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np

        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return  ListedColormap(np.concatenate((colors, ones.T), axis=1))
    cmpas_zhong = call_colors_in_mat(r'D:\MATLAB\colormapdata/BlueWhiteOrangeRed.mat', 'BlueWhiteOrangeRed')

    # plot contour Hgt
    ax.contour(lon, lat, hgt, levels=np.arange(-16,16+1,4),
               colors='black', linewidths=0.3,
               transform=ccrs.PlateCarree())
    ax.contour(lon, lat, hgt, levels=[0],
               colors='black', linewidths=0.7,
               transform=ccrs.PlateCarree())
    ############# plot contourf ##########
    levels = np.arange(-1, 1.001,0.05)
    import cmaps
    contour =ax.contourf(lon, lat, t2m, levels=levels, cmap=cmaps.MPL_RdYlGn_r,
                         norm=colors.CenteredNorm(),extend='both',
                               transform=ccrs.PlateCarree(), alpha=1)

    # set alpha based on value
    def custom_alpha(value, min_val, max_val):
        normalized_value = abs(value) / max(abs(min_val), abs(max_val))
        return 0.01 + 0.75 * np.sqrt(normalized_value)
    for i, collection in enumerate(contour.collections):
        try: # extend == 'both', actual levels will larger than given levels
            alpha = custom_alpha(levels[i] ,levels.min(), levels.max())
            collection.set_alpha(alpha)
        except:
            print('ha')

    # colorbar
    # cb_ax = fig.add_axes([0.1, 0.25, 0.8, 0.01])
    # cbar = plt.colorbar(contour, orientation="horizontal", cax=cb_ax)
    # cb_ax.set_xlabel('mm/day')

    # significance
    lon2d[t2m_p>0.1] = np.nan
    lat2d[t2m_p>0.1] = np.nan
    ax.scatter(lon2d, lat2d, s=1, color='gray', edgecolor='gray', linewidth=0.1
               , transform=ccrs.PlateCarree(), zorder=20)

    ax.set_global()
    # ax.gridlines()
    ax.stock_img()
    # ax.set_extent([-5378070, 5378070, -5378070, 5378070], crs=ccrs.Orthographic(central_lon, central_lat))

    theta = np.linspace(0, 2*np.pi, 100)
    center, radius = [0.5, 0.5], 0.5
    verts = np.vstack([np.sin(theta), np.cos(theta)]).T
    circle = mpath.Path(verts * radius + center)
    ax.set_boundary(circle, transform=ax.transAxes)
    # plt.show()
    ##############################  re boundary ##############################
    # get the transAxes coordination's for a longitude to split earth
    line, = ax.plot(np.linspace(-180, 180, 100), [lat_boundary] * 100, color='black', linewidth=1, transform=ccrs.PlateCarree())
    x_data, y_data = line.get_data()
    line_path = np.column_stack((x_data, y_data))
    new_line_path = []
    for iI_xyLatLine, i_xyLatLine in enumerate(x_data):
        x_mollw, y_mollw = ax.projection.transform_point(line_path[iI_xyLatLine,0], line_path[iI_xyLatLine,1], ccrs.PlateCarree())
        # convert to display coordinates
        x_disp, y_disp = ax.transData.transform((x_mollw, y_mollw))
        # convert to axes coordinates
        x_disp, y_disp= ax.transAxes.inverted().transform((x_disp, y_disp))
        if not (np.isnan(x_disp) | np.isnan(y_disp)):
            new_line_path.append([x_disp, y_disp])
    line_path = np.array(new_line_path)
    # nc4.save_nc4(line_path, 'test')
    # exit()
    def get_boundary(line_path):
        """
        :param line_path: [[x1,y1], [x2, y2], ...]
        :return:
        """

        def reorder_points_to_polygon(points, closed=True):
            """
            根据距离重新排列一组无序的点，生成一个闭合的多边形。

            参数:
            points: List of tuples [(x1, y1), (x2, y2), ...] 需要重新排序的无序点

            返回:
            ordered_points: 按顺序排列的点，形成一个闭合的多边形
            polygon: 生成的Shapely多边形对象
            """

            # 计算两点间的欧几里得距离
            def euclidean_distance(p1, p2):
                return np.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)

            # 将点转换为列表
            points = [list(p) for p in points]

            # 选择起始点（可以选择列表中的任意一点）
            start_point = points[0]
            ordered_points = [start_point]

            # 排列剩余的点，找到最近的点
            remaining_points = points[1:]
            current_point = start_point

            while len(remaining_points) > 0:
                # 计算当前点和剩余所有点的距离
                distances = [euclidean_distance(current_point, p) for p in remaining_points]

                # 找到距离当前点最近的点
                closest_point = remaining_points[np.argmin(distances)]

                # 将最近的点添加到有序点列表
                ordered_points.append(closest_point)

                # 更新当前点并移除已选的点
                current_point = closest_point
                # print(closest_point)
                # print(remaining_points)
                remaining_points.remove(closest_point)

            # 最后，将起始点加回以闭合多边形
            if closed:
                ordered_points.append(ordered_points[0])

            # 返回按顺序排列的点和生成的多边形对象
            return ordered_points

        def remove_points_outside_polygon(line, polygon):
            """
            删除 LineString 中不在 Polygon 内的点，但保留与 Polygon 相交的点

            参数:
            line: Shapely LineString 对象
            polygon: Shapely Polygon 对象

            返回:
            filtered_line: 删除不在 Polygon 内且不相交的点后的 LineString 对象
            """
            new_coords = []

            # 遍历 LineString 中的每个点，检查它是否在 Polygon 内，或者与 Polygon 相交
            for i in range(len(line.coords) - 1):
                start_point = Point(line.coords[i])
                end_point = Point(line.coords[i + 1])

                # 获取 LineString 的每个线段
                segment = LineString([start_point, end_point])

                # 如果点在 Polygon 内，或者该线段与 Polygon 相交，则保留
                if polygon.contains(start_point) or polygon.contains(end_point) or segment.intersects(polygon):
                    if start_point not in new_coords:
                        new_coords.append(start_point.coords[0])  # 加入起点
                    if end_point not in new_coords:
                        new_coords.append(end_point.coords[0])  # 加入终点

            # 使用筛选后的坐标创建新的 LineString
            filtered_line = LineString(new_coords)

            return filtered_line

        line_path = reorder_points_to_polygon(line_path, closed=False)
        line_path = np.array(line_path)

        center, radius = [0.5, 0.5], 0.5
        num_points = 100
        angle = np.linspace(0, 2 * np.pi, num_points)
        x = center[0] + radius * np.cos(angle)
        y = center[1] + radius * np.sin(angle)
        point_in_data = ax.transData.inverted().transform(ax.transAxes.transform((0.5, 0.5)))
        x_mollw, y_mollw =  ccrs.PlateCarree().transform_point(point_in_data[0], point_in_data[1], ax.projection)

        print(x_mollw, y_mollw)
        exit()
        circle_polygon = LineString(zip(x, y))  # boundary in shapely, transAxes
        lat_line = LineString(line_path)  # longitude line in shapely, transAxes

        lat_line = remove_points_outside_polygon(lat_line, polygon=Polygon(zip(x, y)))

        def add_intersection(lat_line):
            def find_shortest_points(points, line2_points):
                # 定义线段和目标点
                line = LineString(line2_points)  # 线段
                point = Point(points[0], points[1])  # 目标点

                # 计算线段到点的最近位置
                projected_distance = line.project(point)  # 计算目标点在线段上的投影距离
                closest_point = line.interpolate(projected_distance)  # 获取线段上最近点的坐标
                if closest_point.distance(point) == 0:
                    return points
                return closest_point

            lat_line = list(lat_line.coords)
            intersection_point = find_shortest_points(lat_line[0],
                                                      list(circle_polygon.coords))
            intersection_point2 = find_shortest_points(lat_line[-1],
                                                       list(circle_polygon.coords))

            return intersection_point, intersection_point2

        p1, p2 = add_intersection(lat_line)

        from shapely.ops import split

        def cut_line_with_split(line, point1, point2):
            """
            使用两个点定义的线段来切割 LineString，容忍浮动误差，确保点在 LineString 上。

            参数：
            line: Shapely LineString 对象，表示原始线段
            point1: Shapely Point 对象，第一个切割点
            point2: Shapely Point 对象，第二个切割点
            tolerance: 容忍误差，默认是 1e-6

            返回：
            split_line: MultiLineString 对象，表示切割后的线段部分
            """

            def expand_point_to_line(point, delta):
                """
                扩展一个点在 x 轴上前后各扩展 delta，变成一条线段。

                参数:
                point: Shapely Point 对象
                delta: 扩展量

                返回:
                LineString 对象，表示扩展后的线段
                """
                # 获取点的坐标
                x, y = point.x, point.y

                # 创建线段，向左和向右各扩展 delta
                line = LineString([(x - delta, y), (x + delta, y)])

                return line

            # 调整切割点，确保它们在 LineString 上
            adjusted_point1_line = expand_point_to_line(point1, 0.01)
            adjusted_point2_line = expand_point_to_line(point2, 0.01)

            # 使用调整后的点创建切割线
            from shapely.ops import unary_union
            cutting_line = unary_union([adjusted_point1_line, adjusted_point2_line])

            # intersection = circle_polygon.intersection(cutting_line)
            # for point in intersection.geoms:
            #     x, y = point.xy
            #     # plt.scatter(x, y)
            # plt.plot(circle_polygon.xy[0], circle_polygon.xy[1])
            # # plt.scatter(circle_polygon.xy[0], circle_polygon.xy[1], s=np.arange(len(circle_polygon.xy[0])))
            # plt.plot(adjusted_point1_line.xy[0], adjusted_point1_line.xy[1])
            # plt.show()
            split_line = split(line, cutting_line)

            return split_line

        split_circle = cut_line_with_split(circle_polygon, p1, p2)
        boundary_x = []
        boundary_y = []
        for part in split_circle.geoms:
            # print(part.exterior.coords)
            # x, y = part.exterior.coords.xy
            x, y = part.xy
            # plt.plot(x, y, linewidth=3, alpha=0.3)
            if np.max(y) > 0.95:
                # plt.plot(x, y, color='red', transform=ax.transAxes)
                boundary_x.extend(x)
                boundary_y.extend(y)
        # ax.plot(line_path[:, 0], line_path[:, 1], transform=ax.transAxes)
        boundary_x.extend(np.squeeze(lat_line.xy[0]))
        boundary_y.extend(np.squeeze(lat_line.xy[1]))
        # ax.plot(boundary_x, boundary_y, transform=ax.transAxes)
        # plt.show()
        boundary = reorder_points_to_polygon(np.column_stack([boundary_x, boundary_y]), closed=True)
        boundary = np.array(boundary)
        # ax.scatter(boundary[:,0], boundary[:,1], s=np.arange(boundary.shape[0]), alpha=0.5, transform=ax.transAxes)
        # plt.show()
        circle = mpath.Path(boundary)
        return circle

    circle = get_boundary(line_path)
    ax.set_boundary(circle, transform=ax.transAxes)

    plt.show()


if __name__ == '__main__':
    main()