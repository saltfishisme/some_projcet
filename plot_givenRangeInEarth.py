import matplotlib.pyplot as plt
import cartopy.crs as ccrs
fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)}
                       , figsize=[4.5, 4])
plt.subplots_adjust(top=0.84,
                    bottom=0.08,
                    left=0.07,
                    right=0.99,
                    hspace=0.2,
                    wspace=0.2)


ranges = [200,300,40,60]
# ranges = [200,223,72,78]
ax.plot([ranges[0], ranges[1], ranges[1], ranges[0], ranges[0]], [ranges[2], ranges[2], ranges[3], ranges[3], ranges[2]], transform=ccrs.PlateCarree())

import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
from matplotlib.patches import PathPatch
shp_dir = r'G:\OneDrive\basis\data\shp\shengjie/'
shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, 11)).geometries(),
                               ccrs.PlateCarree(),
                               edgecolor='k', facecolor='none', linewidths=0.5, zorder=899)
ax.add_feature(shape_feature)

for i_shp in range(1, 10 + 1):
    print(i_shp)
    shape_feature = ShapelyFeature(Reader(r'%s/Export_Output_%i.shp' % (shp_dir, i_shp)).geometries(),
                                   ccrs.PlateCarree(),
                                   edgecolor='k', facecolor='none', linewidths=2, zorder=999)
    ax.add_feature(shape_feature)

ax.coastlines(zorder=9)
ax.gridlines(draw_labels=True)
ax.set_global()
# ax.set_extent([0, 359, 50, 90], crs=ccrs.PlateCarree())
plt.show()


