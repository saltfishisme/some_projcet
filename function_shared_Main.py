def add_longitude_latitude_labels(fig, ax, xloc='auto', yloc=0):
    import cartopy.crs as ccrs
    import numpy as np

    xlocs = [0, 60, 120, 179.9999, -60, -120]
    gl = ax.gridlines(ylocs=[66], xlocs=xlocs,
                      linewidth=0.3, color='black', crs=ccrs.PlateCarree(),
                      draw_labels=True,
                      x_inline=True, y_inline=True,zorder=10000)


    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}

    fig.canvas.draw()
    for ea in gl.ylabel_artists:
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        ea.set_position([yloc, 66])
        ea.set_zorder(1000)

    if isinstance(xloc, str):
        map_boundary_path = gl.axes.spines["geo"].get_path().vertices
        map_boundary_path = ccrs.PlateCarree().transform_points(x=map_boundary_path[:, 1], y=map_boundary_path[:, 0],
                                                                src_crs=ax.projection)

        def get_nearst_loc(arr, value):
            return np.argmin(abs(arr - value))

        yloc_forX = [get_nearst_loc(map_boundary_path[:, 0], i) for i in xlocs]
        yloc_forX = dict(zip(xlocs, map_boundary_path[yloc_forX][:, 0:2]))

        for ea in gl.xlabel_artists:
            print(help(ea))
            ea.set_visible(True)
            ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
            pos = ea.get_position()
            new_pos = yloc_forX[pos[0]]
            ea.set_position([new_pos[0], new_pos[1] + 30])
            ea.set_zorder(1000)
    else:
        for ea in gl.xlabel_artists:
            ea.set_visible(True)
            ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
            pos = ea.get_position()
            ea.set_position([pos[0], xloc])
            ea.set_zorder(1000)

def add_longitude_latitude_labels_givenloc(fig, ax, xloc=[], yloc=0):
    import cartopy.crs as ccrs
    import numpy as np

    xlocs = [0, 60, 120, 179.9999, -60, -120]
    gl = ax.gridlines(ylocs=[66], xlocs=xlocs,
                      linewidth=0.3, color='black', crs=ccrs.PlateCarree(),
                      draw_labels=True,
                      x_inline=True, y_inline=True, zorder=1000)

    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'fontsize': 5}

    fig.canvas.draw()
    for ea in gl.ylabel_artists:
        ea.set_visible(True)
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        ea.set_position([yloc, 66])
        ea.set_zorder(1000)

    x_locnum = 0
    for ea in gl.xlabel_artists:
        ea.set_zorder(1000)
        ea.set_visible(True)
        ea.set_bbox(dict(facecolor='white', alpha=1, edgecolor='none', pad=0))
        ea.set_text(xloc[x_locnum][0])
        ea.set_position([xloc[x_locnum][1], xloc[x_locnum][2]])
        x_locnum += 1
        if x_locnum >= len(xloc):
            break
    # plt.show()

def cal_area(lon, lat):
    import math
    import numpy as np
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S


def cal_area_differentLL(lon, lat):
    """
    work for
    :param lon:
    :param lat:
    :return:
    """
    import math
    import numpy as np
    R = 6378.137

    dlon = np.zeros(lon.shape)
    dlon[:,:-1] = list(np.diff(lon))
    dlon[:,-1] = dlon[:,-2]

    delta = dlon * math.pi / 180.

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = delta * R

    S = Dx * Dy
    return S

def cal_area_differentLL_1d(lon, lat):
    """
    work for
    :param lon:
    :param lat:
    :return:
    """
    import math
    import numpy as np
    R = 6378.137

    dlon = np.zeros(lon.shape)
    dlon[:-1] = list(np.diff(lon))
    dlon[-1] = dlon[-2]

    delta = dlon * math.pi / 180.

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = delta * R

    S = Dx * Dy
    return S

def roll_longitude_from_359to_negative_180(long, reverse=False):
    import xarray as xr
    if reverse:
        return xr.where(long <0,
                        long + 360,
                        long)
    return xr.where(long > 180,
                    long - 360,
                    long)


