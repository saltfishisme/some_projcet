import pandas as pd


def save_nc4(data, savtname):
    # 传入lon, lat, time, data以及储存时候使用的名字即可。
    import netCDF4 as nc4
    f = nc4.Dataset('%s.nc4' % savtname, 'w', format='NETCDF4')  # 'w' stands for write

    tempgrp = f.createGroup('data')
    shape = data.shape
    shapename = []

    for i in range(len(shape)):
        tempgrp.createDimension('%i' % i, shape[i])
        shapename.append('%i' % i)

    mask = tempgrp.createVariable('data', 'f4', shapename)
    try:
        mask[:] = data
    except:
        print(data)
    f.close()
    return


def read_nc4(savtname):
    # 传入lon, lat, time, data以及储存时候使用的名字即可。
    from netCDF4 import Dataset
    f = Dataset('%s.nc4' % savtname, 'r')
    tempgrp = f.groups['data']
    data = tempgrp.variables['data'][:]
    return data


def save_standard_nc(data, var_name, times, lon=None, lat=None, path_save="", file_name='Vari'):
    """
    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
    :param var_name: ['t2m', 'ivt', 'uivt']
    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
    :param lon: list, 720
    :param lat: list, 360
    :param path_save: r'D://'
    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
    :return:
    """
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    # def get_dataarray(data):
    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
    #     return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times
    for iI_vari, ivari in enumerate(var_name):

        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + '%s.nc' % file_name)

