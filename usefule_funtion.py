def call_colors_in_mat(Path, var_name):
    '''
    mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
    which represents alpha of those colors.

    :param Path: mat file Path
    :param var_name: var in mat file
    :return: colormap
    '''
    from matplotlib.colors import ListedColormap
    import scipy.io as scio
    import numpy as np

    colors = scio.loadmat(Path)[var_name]
    colors_shape = colors.shape
    ones = np.ones([1,colors_shape[0]])
    return  ListedColormap(np.concatenate((colors, ones.T), axis=1))


import os

import pandas as pd
import scipy.io
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import sys

def save_standard_nc(data, file_vari_name, times, lon=None, lat=None, path_save="", Save_name='Vari'):
    '''
    :param data: list, containing [varis, time, lat, lon]
    :param file_vari_name: list, [varis]
    :param times:
    :param lon:
    :param lat:
    :param path_save:
    :param Save_name:
    :return:
    '''
    import xarray as xr
    import numpy as np

    if lon is None:
        lon = np.arange(0, 360)
    if lat is None:
        lat = np.arange(90, -91, -1)

    def get_dataarray(data):
        foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
        return foo

    ds = xr.Dataset()
    ds.coords["lat"] = lat
    ds.coords["lon"] = lon
    ds.coords["time"] = times

    for iI_vari, ivari in enumerate(file_vari_name):

        ds[ivari] = (('time', "lat", "lon"), np.array(data[iI_vari]))

    print(ds)
    ds.to_netcdf(path_save + '%s.nc' % Save_name)
def nc_anomaly():
    da = xr.open_dataset(r'D:\OneDrive\basis\some_projects\hgt500_20c.nc').sel(lat=60)
    da = da.assign_coords(year_month=da.time.dt.strftime("%m-%d"))
    result = da.groupby("year_month") - da.groupby("year_month").mean("time")
    return result


def anomaly_for_separate_file(var, var_ShortName):
    dates = pd.date_range('2000-01-01', '2000-12-31',freq='1D').strftime('%m%d')
    years = np.arange(1979, 2020+1)
    # containing 2/29

    anomaly = np.empty([len(level_isobaric), 366 * 24, len(lat), len(lon)])
    for iI_MonDay, i_MonDay in enumerate(dates):
        data_in_MonDay = []
        for i_Year in years:
            try:
                print(path_MainData+'%s/%i/%s.%i%s.nc'%(var, i_Year, var, i_Year, i_MonDay))
                if level_isobaric == -999:
                    s_data = xr.open_dataset(path_MainData+'%s/%i/%s.%i%s.nc'%(var, i_Year, var, i_Year, i_MonDay))[var_ShortName].values
                else:
                    s_data = xr.open_dataset(path_MainData+'%s/%i/%s.%i%s.nc'%(var, i_Year, var, i_Year, i_MonDay)).sel(level=level_isobaric)[var_ShortName].values
                    s_data = np.transpose(s_data, [1,0,2,3])
                print(s_data.shape)
                data_in_MonDay.append(s_data)
            except:
                print(i_Year, i_MonDay)
        data_in_MonDay = np.nanmean(np.array(data_in_MonDay), axis=0)
        path_SaveData = path_MainData+'%s/anomaly/'%var
        os.makedirs(path_SaveData, exist_ok=True)
        scipy.io.savemat(path_SaveData+'%s.mat'%i_MonDay, {'%s'%i_MonDay:np.array(data_in_MonDay,dtype='double')})
        print(data_in_MonDay.shape)
        anomaly[:, iI_MonDay*24:(iI_MonDay+1)*24, :, :] = data_in_MonDay

    for iI_level, i_level in level_isobaric:
        save_standard_nc(anomaly[iI_level], ['%s_%i'%(var, i_level)], pd.date_range('1000-01-01 00:00', '1000-12-31 23:00', freq='1H'),
                         lon=lon, lat=lat, path_save=path_SaveData, Save_name='%s_%i_dailyMean'%(var, i_level))
    # scipy.io.savemat(path_SaveData + 'anomaly_%s.mat'%var, {var:np.array(anomaly, dtype='double')})


path_MainData = '/home/linhaozhong/work/Data/ERA5/pressure_level/0.5X0.5/'
level_isobaric = [1000, 850, 500, 200]
vari_SaveName_all=['Hgt', 'Tem', 'Uwnd', 'Vwnd', 'Wwnd']
var_ShortName = ['z', 't', 'u', 'v', 'w']
lon = np.arange(0,360,0.5)
lat = np.arange(90,-90.2,-0.5)


for iI_var in range(len(var_ShortName)):
    anomaly_for_separate_file(vari_SaveName_all[iI_var], var_ShortName[iI_var])

