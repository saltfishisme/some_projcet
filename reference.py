import re
from docx import Document


def read_word_document(file_path):
    # 读取Word文档内容
    doc = Document(file_path)
    full_text = []
    for para in doc.paragraphs:
        full_text.append(para.text)
    return full_text


def check_references(paragraphs):
    # 标志是否进入参考文献部分
    in_references_section = False
    references_text = []
    body_text = []

    # 分离正文和参考文献部分
    for para in paragraphs:
        if in_references_section:
            references_text.append(para)
        else:
            body_text.append(para)
        # 当遇到 "参考文献：" 这一行时，标记进入参考文献部分
        if "参考文献：" in para:
            in_references_section = True

    # 将正文合并为单一字符串
    body_text_str = '\n'.join(body_text)

    # 识别正文中的 [*] 形式
    pattern = r'\[(\d+)\]'  # 匹配方括号内的数字
    found_references = re.findall(pattern, body_text_str)  # 找到正文中的所有引用

    # 识别下方的引用，假设引用是数字开头加内容
    ref_pattern = r'^\d+'  # 匹配引用的格式：数字+空格+内容
    found_citations = []
    for i in references_text:
        found_citations.extend(re.findall(ref_pattern, i))

    # 打印正文中的引用
    print("正文中的引用：", found_references)

    # 打印下方的引用
    print("下方的引用：", found_citations)

    # 检查引用是否一一对应
    ref_set = set(found_references)
    cit_set = set(found_citations)

    if ref_set == cit_set:
        print("所有引用与正文中的数字一一对应！")
    else:
        missing_in_text = cit_set - ref_set
        missing_in_ref = ref_set - cit_set
        if missing_in_text:
            print(f"这些引用出现在参考文献中，但正文中没有：{missing_in_text}")
        if missing_in_ref:
            print(f"这些引用出现在正文中，但参考文献中没有：{missing_in_ref}")

    # 给引用中的数字加上 []
    updated_references = [re.sub(r'^(\d+)', r'[\1] ', ref) for ref in references_text]
    return body_text_str, updated_references


def save_word_document(body_text, references_text, file_path):

    def add_paragraph_with_superscript(doc, text):
        """添加段落并将引用部分设置为上标"""
        para = doc.add_paragraph()

        # 分割文本，查找引用
        parts = []
        while "[" in text and "]" in text:
            before, citation, after = text.partition("]")
            citation = "[" + before.split("[")[-1] + "]"
            parts.append(before.split("[")[0])
            parts.append(citation)
            text = after

        parts.append(text)  # 添加剩余文本

        # 添加文本和上标
        for part in parts:
            if part:  # 只处理非空部分
                run = para.add_run(part)
                if "[" in part and "]" in part:  # 检查是否为引用
                    run.font.superscript = True

    doc = Document()

    # 添加正文
    for line in body_text.split('\n'):
        add_paragraph_with_superscript(doc, line)

    # 添加更新后的参考文献
    for line in references_text:
        para = doc.add_paragraph(line)

    from docx.oxml.ns import qn
    for para in doc.paragraphs:
        for run in para.runs:
            run.font.name = '宋体'
            run._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

    doc.save(file_path)


# 示例：读取Word文档，检查并更新引用，保存到新文档
input_file = r"G:\OneDrive\A_A学习工作\project\词条\台风苏拉_ref1.docx"
output_file = r'G:\OneDrive\A_A学习工作\project\词条\台风泰利_refnew.docx'

# 读取 Word 文档
document_paragraphs = read_word_document(input_file)

# 检查并更新引用
body_text, updated_references = check_references(document_paragraphs)
def extract_citations_from_body(body):
    """提取正文中的引用数字"""
    body_text = body

    # 匹配正文中的 [数字] 引用
    citations = re.findall(r'\[(\d+)\]', body_text)
    def unique_list(lst):
        seen = set()
        return [x for x in lst if not (x in seen or seen.add(x))]
    return unique_list([int(x) for x in citations])  # 去重并转成整数

def map_references_to_body(citations, references):
    """将参考文献与正文中的引用对应，并移除未引用的参考文献"""
    # 创建一个映射，将引用重新编号

    new_references = []
    citation_map = {}
    new_idx = 1
    for idx, cite in enumerate(citations):
        for ref in references:
            if ref.startswith('[%i]'%cite):
                citation_map[cite] = new_idx
                new_references.append(ref.replace('[%i]'%cite, '[%i]'%new_idx))
                new_idx += 1
    print(citation_map)

    # for idx, ref in enumerate(references, 1):
    #     if idx in citations:
    #         citation_map[idx] = new_citation_index
    #         new_references.append(f"[{new_citation_index}] {ref}")
    #         new_citation_index += 1

    # # 遍历正文中的引用，按照出现顺序更新参考文献
    # for citation in sorted(citations):
    #     if citation <= len(references):
    #         new_references.append(f"[{new_citation_index}] {references[citation - 1]}")
    #         citation_map[citation] = new_citation_index
    #         new_citation_index += 1

    return citation_map, new_references

def update_body_with_new_citations(body, citation_map):
    """根据新的引用编号更新正文中的引用"""
    body_text = body

    # 使用引用映射更新正文中的引用编号
    for old, new in citation_map.items():
        body_text = re.sub(rf'\[{old}\]', f'[@@{new}]', body_text)
    return body_text


citation_map, new_references = map_references_to_body(extract_citations_from_body(body_text),
                       updated_references)

updated_body = update_body_with_new_citations(body_text, citation_map)

updated_body = updated_body.replace('@@', '')

save_word_document(updated_body, new_references, output_file)
#
# save_word_document(body_text, updated_references, output_file)
# print(f"处理完成，结果已保存到 {output_file}")
