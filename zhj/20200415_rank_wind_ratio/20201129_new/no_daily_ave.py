import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from docx import Document
from docx.shared import Inches

'''计算日平均风速，画序列图。例如1.1日-3.15日，时间序列图，
	这样一张图上有8根线，分别是观测、预见期为1天的预报、预见期为2天的预报。。。。。。

	备注：比如，1月31日，有观测值，还有7个预报值，这7个预报值，分别是1月31日，1月30日，1月29日，1月28日。。。以此类推得来的。

'''


def main(area, compare_data_name):
    # 绘制标签
    if area == 0:
        title = '瑞安'
        legendlabel = compare_data_name
    else:
        title = '苍南'
        legendlabel = compare_data_name
    legendlabels = ['实测 '+legendlabel, '预测 '+legendlabel]

    # 找到要用的数据
    if area == 0:
        sheetname = '瑞安1#'
    else:
        sheetname = '苍南4#'

    areaname = ['ruian', 'cangnan']
    areanumber = ['3303811A', '3303273A']

    def cal_time(begin, end, skip=None):
        '''计算出需要计算的天数
        '''
        date = pd.date_range(begin, end, freq='1D')
        if skip is not None:
            for i in skip:
                date = date[date != i]
        return [str(i)[5:7] + str(i)[8:10] for i in np.array(date)]
    begin_times = [cal_time('2020-01-01','2020-7-31'),

                   cal_time('2020-01-11','2020-8-30')]

    monthss = [list(range(1,8)),
               list(range(1,9))]
    begin_time = begin_times[area]
    months = monthss[area]

    # create sheet3 averaged wind
    # create sheet3 averaged wind
    # 观测数据
    data1 = pd.read_csv(r'D:\basis\some_projects\zhj\20200415_rank_wind_ratio\20201129_new\%s.csv'%areaname[area],
                    index_col=0,skiprows=1,parse_dates=True,
                    names=['0', '100 风速','20 风速','100 风向'])

    # data1=pd.read_excel(r'D:\basis\some_projects\zhj\20200415_rank_wind_ratio\20201129_new\温州海域数据.xlsx',
    #                 index_col=0,dtype={'0':str}, sheet_name=sheetname,skiprows=0,
    #                 names=['0', '100 风速','20 风速','100 风向'])

    def choose_data(time_loc):
        need_time = begin_time[time_loc]
        sdata = []

        for i in range(time_loc-7, time_loc):
            select_time = begin_time[i]
            try:
                try:
                    data2 = pd.read_csv(
                        r'D:\basis\some_projects\zhj\20200415_rank_wind_ratio\20201129_new\evaluation\%s.2020%s12.csv' % (
                        areanumber[area], select_time),
                        parse_dates=True)
                    data2 = data2[['fcst_dt', '100米风速', '10米风速']]
                    data2.columns = ['0', '100 风速', '20 风速']
                    data2['0'] = pd.to_datetime(data2['0'], format='%Y-%m-%d %H:%M')
                    data2 = data2.set_index('0')
                except:
                    data2 = pd.read_csv(
                        r'D:\basis\some_projects\zhj\20200415_rank_wind_ratio\20201129_new\evaluation\%s.2020%s00.csv' % (
                        areanumber[area], select_time),
                        parse_dates=True)
                    data2 = data2[['fcst_dt', '100米风速', '10米风速']]
                    data2.columns = ['0', '100 风速', '20 风速']
                    data2['0'] = pd.to_datetime(data2['0'], format='%Y-%m-%d %H:%M')
                    data2 = data2.set_index('0')
                data_none = 0
            except:
                data_none = 1
            if data_none == 0:
                select_data = data2.set_index(data2.index + pd.Timedelta(8, unit='H'))['2020-%s-%s'% (need_time[0:2], need_time[2:4])][compare_data_name]
                select_data = select_data.resample('1H').asfreq()
                if select_data.shape[0] < 24:
                    indexs = pd.date_range('2020-%s-%s 00'% (need_time[0:2], need_time[2:4]),'2020-%s-%s 23'% (need_time[0:2], need_time[2:4]), freq='1H')
                    select_data = pd.Series(select_data, index=indexs)
            elif data_none == 1:

                select_data = np.ones([24])*np.nan
                indexs = pd.date_range('2020-%s-%s 00' % (need_time[0:2], need_time[2:4]),
                                       '2020-%s-%s 23' % (need_time[0:2], need_time[2:4]), freq='1H')
                select_data = pd.Series(select_data, index=indexs)
            sdata.append(select_data)

        rowdata = data1['2020-%s-%s' % (need_time[0:2], need_time[2:4])][compare_data_name]
        rowdata.index = rowdata.index.strftime('%Y-%m-%d %H:%M')

        return sdata, rowdata

    # print(choose_data(60))
    # 按照7天预见期分类
    pre_mean = pd.DataFrame()
    obs_mean = pd.DataFrame()
    pre_mean = []
    obs_mean = []
    for i in range(7,len(begin_time)):
        # 找到给定日期的七天预报期数据
        spre_mean, sobs_mean = choose_data(i)
        pre_mean.append(spre_mean)
        obs_mean.append(sobs_mean)

    df = pd.DataFrame()
    for i in range(7):
        newlist = []
        for j in range(len(pre_mean)):
            newlist.append(pre_mean[j][i])
        df['pre%iD'%(7-i)] = pd.concat(newlist, join='outer')
    df['obs'] = pd.concat(obs_mean)
    plot_mean(df,legendlabels, title, months)
    return


def plot_mean(df,legendlabels, title, months):
    def plot_monthly_data(df, month):
        '''再另出一套一个月一幅图，这样能看的清楚一点。'''
        fig, ax = plt.subplots(1,1, figsize=(7,5))
        plt.subplots_adjust(0.08,0.16,0.99,0.94)
        for i in range(3):
            ax.plot(df.index, df['pre%iD'%(i+1)], label='预见期：%i天'%(i+1), linewidth=1)
        ax.plot(df.index, df['obs'], label='实测值', linestyle='--', color='black', linewidth=1)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.07,
                         box.width, box.height * 0.9])

        ax.set_ylabel('风速 m/s')
        ax.set_xlabel('2020年')

        legendlabel_single = legendlabels[0][3:].split(' ')
        ax.set_title('%s%s每小时%s米%s'%(title, month, legendlabel_single[0], legendlabel_single[1]))
        # Put a legend below current axis
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12),
                  fancybox=True, shadow=True, ncol=4)
        # years = mdates.HourLocator(interval=24)
        yearsFmt = mdates.DateFormatter('%m-%d')
        # ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)
        # plt.show()
        plt.savefig('fig.png')
        plt.close()

        document.add_picture('fig.png', width=Inches(5))
        p3 = document.add_paragraph()

        run3 = p3.add_run('图4. %s%s米%s%s不同预见期预报风速与实测风速对比图'%(title, legendlabel_single[0], legendlabel_single[1], month))


    for i in months:
        plot_monthly_data(df[df.index.month==i], '%i月'%i)


def main2():
    variables = ['20 风速','100 风速']
    names = ['瑞安', '苍南']

    for sj in variables:

        main(0, sj)

    for sj in variables:
            main(1, sj)

    document.save('预见期比较.docx')


document = Document()
main2()
