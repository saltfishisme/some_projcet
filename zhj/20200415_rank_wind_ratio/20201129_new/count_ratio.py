import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from docx import Document
from docx.shared import Inches

'''
总样本数，各等级的样本个数、占比
'''


def creat_word_table(document, title, df):
    from docx import Document
    from docx.enum.table import WD_TABLE_ALIGNMENT
    from docx.shared import Cm
    from docx.oxml.ns import qn
    '''
    document = Document()
document.styles['Normal'].font.name = '宋体'
document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')'''
    from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
    p = document.add_paragraph('')
    p.add_run(title).bold = True
    p.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    tb = document.add_table(rows=len(df.index), cols=len(df.columns))
    # tb.add_row()

    for row in range(len(df.index)):
        for col in range(len(df.columns)):
            tb.cell(row, col).text = str(df.iloc[row, col])

    tb.style = 'Table Grid'
    tb.autofit = True


def main(area, sheetnumber, compare_data_name, document, time_step, wind_level):
    # 绘制标签
    if area == 0:
        title = '瑞安'
        legendlabel = compare_data_name
    else:
        title = '苍南'
        legendlabel = compare_data_name
    legendlabels = ['实测 ' + legendlabel, '预测 ' + legendlabel]

    # 找到要用的数据
    if area == 0:
        sheetname = '瑞安1#'
    else:
        sheetname = '苍南4#'

    areaname = ['ruian', 'cangnan']
    areanumber = ['3303811A', '3303273A']

    def cal_time(begin, end, skip=None):
        '''计算出需要计算的天数
        '''
        date = pd.date_range(begin, end, freq='1D')
        if skip is not None:
            for i in skip:
                date = date[date != i]
        return [str(i)[5:7] + str(i)[8:10] for i in np.array(date)]

    begin_times = [cal_time('2020-01-01', '2020-7-31'),

                   cal_time('2020-01-11', '2020-8-30')]

    begin_time = begin_times[area]

    # create sheet3 averaged wind
    # create sheet3 averaged wind
    # 观测数据
    data1 = pd.read_csv(r'D:\basis\some_projects\zhj\20200415_rank_wind_ratio\20201129_new\%s.csv'%areaname[area],
                    index_col=0,skiprows=1,parse_dates=True,
                    names=['0', '100 风速','20 风速','100 风向'])

    # sdata = pd.DataFrame()
    # for i in begin_time:
    #     data2 = pd.read_csv(
    #         r'D:\basis\some_project\20200415_rank_wind_ratio\forecast\forecast\%s.2020%s12.csv' % (
    #         areanumber[area], i),
    #         index_col=0, parse_dates=True, skiprows=1,
    #         names=['0', '100 风速', '20 风向', '20 风速'])
    #     sdata = pd.concat([sdata, data2])



    total_number = data1.shape[0]

    regg_predata = np.array(data1[compare_data_name])

    wind_rank_index = np.argwhere((regg_predata >= wind_level[0]) &
                                  (regg_predata <  wind_level[1])).flatten()
    regg_predata = regg_predata[wind_rank_index]


    # print(regg_predata, regg_obsdata)
    if (regg_predata.shape[0] <= 1):
        number = 0
    else:
        number = regg_predata.shape[0]

    # document.save('%s_%s.docx'%(title, legendlabels[0][3:]))
    return '%s %s'%(title, legendlabels[0][3:]), [number, total_number, str(round(number*100/total_number,2))+'%']


def main2(time_step, wind_all_level):
    variables = ['100 风速','20 风速']
    names = ['瑞安', '苍南']
    for i in range(0,1):
        name = names[i]
        all_mse = pd.DataFrame()
        for sj in variables:
            # 不同风级地点
            word_document = Document()

            from docx.enum.table import WD_TABLE_ALIGNMENT
            from docx.shared import Cm
            from docx.oxml.ns import qn
            document = Document()
            document.styles['Normal'].font.name = '宋体'
            document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

            mean_obs_data = pd.DataFrame()



            for wind_level in wind_all_level:
                locname, mse = main(i, 2, sj, word_document, time_step, wind_level)
                mean_obs_data[str(wind_level)] = mse

            list = [mean_obs_data]

            for name_forvariable in range(1):
                creat_word_table(document, str(name_forvariable), list[name_forvariable].T)

            document.save('ratio%s 逐%i小时平均 %i风级.docx' % (locname, time_step, len(wind_all_level)))


    for sj in variables:
        word_document = Document()
        from docx.enum.table import WD_TABLE_ALIGNMENT
        from docx.shared import Cm
        from docx.oxml.ns import qn
        document = Document()
        document.styles['Normal'].font.name = '宋体'
        document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

        mean_obs_data = pd.DataFrame()
        mean_pre_data = pd.DataFrame()
        MAE_data = pd.DataFrame()
        MRPE_data = pd.DataFrame()
        RMSE_data = pd.DataFrame()
        R_data = pd.DataFrame()

        for wind_level in wind_all_level:
            locname, mse = main(1, 0, sj, word_document, time_step, wind_level)
            mean_obs_data[str(wind_level)] = mse

        list = [mean_obs_data]

        for name_forvariable in range(1):
            creat_word_table(document, str(name_forvariable), list[name_forvariable].T)

        document.save('ratio%s 逐%i小时平均 %i风级.docx' % (locname, time_step, len(wind_all_level)))


wind_all_level2 = [[0,3.4],[3.4,5.5],[5.5,8.0],[8.0,10.8], [10.8,13.9],[13.9,100]]
wind_all_level1 =[[0.3,1.6],[1.6,3.4],[3.4,5.5],[5.5,8.0],[8.0,10.8], [10.8,13.9],[13.9,17.1]]


# main2(1, wind_all_level1)
main2(1, wind_all_level1)
# main2(6)
# main2(12)
# main2(24)



# def plot_all_statistic_variable_in_hline(all_mse):
#     '''variables = ['100 风速','20 风速']
# names = ['瑞安', '苍南']
# for i in range(0,1):
# name = names[i]
# all_mse = pd.DataFrame()
# for j in range(3):
#     for sj in variables:
#         locname, mse = main(i, j, sj)
#         all_mse[locname] = mse'''
#     all_mse = all_mse.set_index(np.arange(24,169,24))
#     order = ['%s_20 风速'%name, '%s_20 NW风速'%name, '%s_20 SE风速'%name,'%s_100 风速'%name, '%s_100 NW风速'%name, '%s_100 SE风速'%name]
#     all_mse = all_mse[order]
#     all_mse.columns = ['%s 20 风速'%name, '%s 20 NW风速'%name, '%s 20 SE风速'%name,'%s 100 风速'%name, '%s 100 NW风速'%name, '%s 100 SE风速'%name]
#
#     all_mse.ZhengZhou.bar(legend=False)
#     ax = plt.gca()
#     box = ax.get_position()
#     ax.set_position([box.x0, box.y0 + box.height * 0.1,
#                      box.width, box.height * 0.9])
#
#     # Put a legend below current axis
#     plt.subplots_adjust(top=0.86,bottom=0.11,left=0.11,right=0.9,hspace=0.2,wspace=0.2)
#     ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.18),
#               fancybox=True, shadow=True, ncol=3)
#     ax.set_xlabel('预报时效 （小时）')
#     ax.set_ylabel('平均绝对误差 （m/s）')
#     ax.axhline(0, color='black', alpha=0.8)
#     plt.savefig('%i.png'%i)
#     plt.close()
