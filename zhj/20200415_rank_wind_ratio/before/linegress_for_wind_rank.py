import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from docx import Document
from docx.shared import Inches

'''1，	预见期1-7天分类；基于逐小时预报和观测数据，根据观测数据按标准风级划分等级。
在不同预见期、不同风级的样本中，计算绝对误差、误差百分率、均方根误差、相关系数；计算预报和观测的平均值；计算散点图、回归方程。
备注：总体上就是之前的全部样本进行了回归以及计算柱状图，现在是把全部样本，先分级，再进行计算。
'''


def creat_word_table(document, title, df):
    from docx import Document
    from docx.enum.table import WD_TABLE_ALIGNMENT
    from docx.shared import Cm
    from docx.oxml.ns import qn
    '''
    document = Document()
document.styles['Normal'].font.name = '宋体'
document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')'''
    from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
    p = document.add_paragraph('')
    p.add_run(title).bold = True
    p.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    tb = document.add_table(rows=len(df.index), cols=len(df.columns))
    # tb.add_row()

    for row in range(len(df.index)):
        for col in range(len(df.columns)):
            tb.cell(row, col).text = str(df.iloc[row, col])

    tb.style = 'Table Grid'
    tb.autofit = True


def main(area, sheetnumber, compare_data_name, document, time_step, wind_level):
    def choose_data(step):
        sdata = pd.DataFrame()
        rowdata = pd.DataFrame()
        for i in begin_time:
            data2 = pd.read_csv(
                r'D:\basis\some_project\20200415_rank_wind_ratio\forecast\forecast\%s.2020%s12.csv' % (
                areanumber[area], i),
                index_col=0, parse_dates=True, skiprows=1,
                names=['0', '100 风速', '20 风向', '20 风速'])
            data2 = data2.set_index(data2.index + pd.Timedelta(8, unit='H'))[
                str(np.datetime64('2020-%s-%s' % (i[0:2], i[2:4])) + pd.Timedelta(step, unit='D'))[:10]]
            rowdata2 = data1[
                str(np.datetime64('2020-%s-%s' % (i[0:2], i[2:4])) + pd.Timedelta(step, unit='D'))[:10]]
            sdata = pd.concat([sdata, data2])
            rowdata = pd.concat([rowdata, rowdata2])
        return sdata, rowdata

    def regg(ax, time, y, monthname, ylabel_name, labels=None, timeticksname=None):
        def fit_line(x, y):
            # from scipy.stats import linregress
            # (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(np.array(x), y)
            # return beta_coeff, intercept, rvalue, pvalue, stderr
            import statsmodels.api as sm
            import statsmodels
            from statsmodels.formula.api import ols
            import pandas as pd
            sx = sm.add_constant(x)
            models = sm.OLS(y, sx)
            model = models.fit()
            fit_y = model.fittedvalues
            from sklearn.metrics import r2_score
            me = statsmodels.tools.eval_measures.meanabs(x,y)
            mae = np.mean(abs(x-y)/x)*100
            # mae = statsmodels.tools.eval_measures.meanabs(x,y)
            rmse = statsmodels.tools.eval_measures.rmse(x,y)
            return model.params[1], model.params[0], (model.rsquared)**(1/2), me, mae, rmse

        def plotall(ax, time, line1, month, beta_coeff, intercept, r, mse, mae, rmse,
                    varible_name, timeticksname=None, labels=None):
            line20 = np.min(time) * beta_coeff + intercept
            line21 = np.max(time) * beta_coeff + intercept
            if compare_data_name[0] == '1':
                ax.set_ylim(0,19)
                ax.set_xlim(0,19)
            else:
                ax.set_ylim(0,16)
                ax.set_xlim(0,16)
            # ax.set_ylim(wind_level[0], wind_level[1])
            # ax.set_xlim(wind_level[0], wind_level[1])
            plt.scatter(time, line1, label='观测', color='blue', s=1.5)
            plt.plot([np.min(time), np.max(time)], [line20, line21], label='趋势', color='red')
            plt.plot([0,ax.get_xlim()[1]], [0, ax.get_ylim()[1]], linestyle='--', color='grey')


            ax.text(0.02, 0.95,
                    '预报时效：T+%ih'%predict_time, fontsize=12, transform=ax.transAxes)
            ax.text(0.02, 0.9,
                    '%sy=%.4fx+%.2f' % (month, beta_coeff, intercept), fontsize=12, transform=ax.transAxes)

            ax.text(0.25, 0.1,
                    '预报平均值=%0.2f' % np.mean(time), fontsize=12, transform=ax.transAxes)
            ax.text(0.55, 0.1,
                    '观测平均值=%0.2f' % np.mean(line1), fontsize=12, transform=ax.transAxes)

            ax.text(0.15,0.02,
                    'MAE=%0.2f' % mse, fontsize=12, transform=ax.transAxes)
            ax.text(0.35, 0.02,
                    'MRPE=%i%%' % int(mae), fontsize=12, transform=ax.transAxes)
            ax.text(0.55, 0.02,
                    'RMSE=%0.2f' % rmse, fontsize=12, transform=ax.transAxes)

            ax.text(0.75, 0.02,
                    'r=%.2f' % (r), fontsize=12, transform=ax.transAxes)


            # plt.legend(loc=1)
            ax.set_ylabel('%s (m/s)' % varible_name)
            ax.set_xlabel('%s (m/s)' %legendlabels[0])

            ax.set_title(title+'风速范围：%s-%sm/s'%(str(wind_level[0]), str(wind_level[1])))

            if labels is not None:
                plt.text(ax.get_xlim()[0], ax.get_ylim()[1], labels)
            if timeticksname is not None:
                from pandas import Series
                ax.set_xticks(ax.get_xticks()[1:-2])
                x = list(Series(ax.get_xticks()).map(int))
                ax.set_xticklabels(timeticksname[x])
            # plt.show()
            return


        beta_coeff, intercept, rvalue, mse, mae, rmse = fit_line(time, y)
        plotall(ax, time, y, monthname, beta_coeff, intercept, rvalue, mse, mae, rmse, ylabel_name, labels=labels)
        return [mse,mae,rmse,rvalue], [np.mean(time), np.mean(y)]

    # 绘制标签
    if area == 0:
        title = '瑞安'
        sheetlabel = ['SE', 'NW', '']
        legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
    else:
        title = '苍南'
        sheetlabel = ['NW', 'SE', '']
        legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
    legendlabels = ['实测 '+legendlabel, '预测 '+legendlabel]

    # 找到要用的数据
    sheetcangnan = ['605019#', '605683#', 'ave']
    sheetruian = ['605684#', '605537#', 'ave']
    if area == 0:
        sheetname = sheetruian[sheetnumber]
    else:
        sheetname = sheetcangnan[sheetnumber]
    areaname = ['ruian', 'cangnan']
    areanumber = ['3303811A', '3303273A']

    begin_times = [['0101', '0102', '0103', '0104', '0105', '0106', '0107', '0108', '0109', '0110', '0111', '0112',
                    '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                    '0125', '0126', '0127', '0128', '0129', '0130','0201','0202','0203','0204', '0205', '0206', '0207', '0208', '0209', '0210', '0211', '0212',
                    '0213', '0214', '0215', '0216', '0217', '0218', '0219', '0220', '0221', '0222', '0223', '0224',
                    '0225', '0226', '0227', '0228', '0301','0302','0303','0304', '0305', '0306', '0307', '0308'],

                   ['0108', '0109', '0110', '0111', '0112',
                    '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                    '0125', '0126', '0127', '0128', '0129', '0130','0201','0202','0203','0204', '0205', '0206', '0207', '0208', '0209', '0210', '0211', '0212',
                    '0213', '0214', '0215', '0216', '0217', '0218', '0219', '0220', '0221', '0222', '0223', '0224',
                    '0225', '0226', '0227', '0228', '0301','0302','0303','0304', '0305', '0306', '0307', '0308']]
    #                ]
    # begin_times = [['0101','0108', '0116','0124','0130','0201','0208',
    #               '0222','0301','0308'],
    #                ['0116', '0124', '0130', '0201', '0208',
    #                 '0222', '0301', '0308']
    #                ]
    begin_time = begin_times[area]

    # create sheet3 averaged wind
    # create sheet3 averaged wind
    # 观测数据
    data1=pd.read_excel(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\%s.xlsx'%areaname[area],
                    index_col=0, parse_dates=True,sheet_name=sheetname,skiprows=0,
                    names=['0', '100 风速','20 风向','20 风速', 'nans'])


    all_statistic_variable_series = []
    means = []

    # 按照7天预见期分类
    for i in range(1,8):

        predict_time = i*24
        pre_data, obs_data = choose_data(i)
        pre_data = pre_data.dropna(axis=0, how='any')
        obs_data = obs_data.dropna(axis=0, how='any')
        index = pre_data.index.intersection(obs_data.index)
        pre_data = pre_data.loc[index]
        obs_data = obs_data.loc[index]


        # 六小时平均或者十二小时平均
        if time_step == 24:
            pre_data = pre_data.groupby([pre_data.index.month, pre_data.index.day]).mean().reindex()
            obs_data = obs_data.groupby([obs_data.index.month, obs_data.index.day]).mean().reindex()
        if time_step == 12:
            pre_data = pre_data.groupby([obs_data.index.month, obs_data.index.day]).resample('12H').mean()
            obs_data = obs_data.groupby([obs_data.index.month, obs_data.index.day]).resample('12H').mean()
        if time_step == 6:
            pre_data = pre_data.groupby([obs_data.index.month, obs_data.index.day]).resample('6H').mean()
            obs_data = obs_data.groupby([obs_data.index.month, obs_data.index.day]).resample('6H').mean()

        regg_obsdata = np.array(obs_data[compare_data_name])
        regg_predata = np.array(pre_data[compare_data_name])
        wind_rank_index = np.argwhere((regg_obsdata >= wind_level[0]) &
                                      (regg_obsdata <  wind_level[1])).flatten()
        regg_obsdata = regg_obsdata[wind_rank_index]
        regg_predata = regg_predata[wind_rank_index]
        # print(regg_predata, regg_obsdata)
        if (regg_predata.shape[0] <= 1) | (regg_obsdata.shape[0] <= 1):
            print()
            all_statistic_variable_series.append([0,0,0,0])
            means.append([0,0])
        else:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            ax = fig.add_subplot(111)
            mse,smean = regg(ax, regg_obsdata, regg_predata,
                 '', legendlabels[1])
            all_statistic_variable_series.append(mse)
            means.append(smean)
            # plt.show()
            plt.savefig('第%i天_%s_%s.png'%(i,title, legendlabels[0][3:]))
            plt.close()
            document.add_picture('第%i天_%s_%s.png'%(i,title, legendlabels[0][3:]), width=Inches(5))

    # document.save('%s_%s.docx'%(title, legendlabels[0][3:]))
    return '%s %s'%(title, legendlabels[0][3:]), all_statistic_variable_series, means



def plot_statistic_variable_in_hline(ax, all_mse, ylabel, number):
    if number == 3:
        width = 5
    else:
        width = 10
    ax.bar(np.arange(24, 169, 24)[:number], all_mse[:number].flatten(), width=width)
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0 + box.height * 0.1,
    #                  box.width, box.height * 0.9])

    # Put a legend below current axis
    # plt.subplots_adjust(top=0.86, bottom=0.11, left=0.11, right=0.9, hspace=0.2, wspace=0.2)
    # ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.18),
    #           fancybox=True, shadow=True, ncol=3)
    ax.set_xlabel('预报时效 （小时）')
    ax.set_ylabel(ylabel)
    ax.axhline(0, color='black', alpha=0.8)
    ax.set_xticks(np.arange(24,169,24)[:number])
    # plt.savefig('%i.png' % i)
    # plt.close()


def main2(time_step, wind_all_level):
    variables = ['100 风速','20 风速']
    names = ['瑞安', '苍南']
    for i in range(0,1):
        name = names[i]
        all_mse = pd.DataFrame()
        for sj in variables:
            # 不同风级地点
            word_document = Document()

            from docx.enum.table import WD_TABLE_ALIGNMENT
            from docx.shared import Cm
            from docx.oxml.ns import qn
            document = Document()
            document.styles['Normal'].font.name = '宋体'
            document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

            mean_obs_data = pd.DataFrame()
            mean_pre_data = pd.DataFrame()
            MAE_data = pd.DataFrame()
            MRPE_data = pd.DataFrame()
            RMSE_data = pd.DataFrame()
            R_data = pd.DataFrame()


            for wind_level in wind_all_level:
                locname, mse, means = main(i, 2, sj, word_document, time_step, wind_level)
                mse = np.array(mse)
                mean_obs_data[str(wind_level)] = np.array(means)[:,0]
                mean_pre_data[str(wind_level)] = np.array(means)[:, 1]
                MAE_data[str(wind_level)] = mse[:, 0]
                MRPE_data[str(wind_level)] = mse[:, 1]
                RMSE_data[str(wind_level)] = mse[:, 2]
                R_data[str(wind_level)] = mse[:, 3]

                def select_plot_number(number):
                    fig, ax = plt.subplots(2,2)
                    plt.subplots_adjust(top=0.92,bottom=0.09,left=0.11,right=0.975,hspace=0.34,wspace=0.225)
                    plot_statistic_variable_in_hline(ax[0][0], mse[:,0], 'MAE (m/s)', number)
                    plot_statistic_variable_in_hline(ax[0][1], mse[:, 1], 'MRE (%)', number)
                    plot_statistic_variable_in_hline(ax[1][0], mse[:, 2], 'RMSE (m/s)', number)
                    plot_statistic_variable_in_hline(ax[1][1], mse[:, 3], 'r', number)
                    fig.suptitle('%s 逐%i小时平均 风速范围：%s-%sm/s'%(locname, time_step,str(wind_level[0]), str(wind_level[1])))
                    plt.savefig('%s 逐%i小时平均 %i.png'%(locname, time_step, number))
                    plt.close()

                select_plot_number(7)
                select_plot_number(3)

                word_document.add_picture('%s 逐%i小时平均 %i.png'%(locname, time_step, 7), width=Inches(5))
                word_document.add_picture('%s 逐%i小时平均 %i.png' % (locname, time_step, 3), width=Inches(5))

            list = [mean_pre_data - mean_obs_data]
            name_forvariable = 0
            formater = "{0:.02f}".format
            creat_word_table(document, str(name_forvariable), list[name_forvariable].T.applymap(formater))

            # list = [mean_obs_data, mean_pre_data, MAE_data, MRPE_data, RMSE_data, R_data]
            # for name_forvariable in range(6):
            #     formater = "{0:.02f}".format
            #     creat_word_table(document, str(name_forvariable), list[name_forvariable].T.applymap(formater))
            document.save('%s 逐%i小时平均 %i风级.docx' % (locname, time_step, len(wind_all_level)))


    for sj in variables:
        word_document = Document()
        from docx.enum.table import WD_TABLE_ALIGNMENT
        from docx.shared import Cm
        from docx.oxml.ns import qn
        document = Document()
        document.styles['Normal'].font.name = '宋体'
        document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

        mean_obs_data = pd.DataFrame()
        mean_pre_data = pd.DataFrame()
        MAE_data = pd.DataFrame()
        MRPE_data = pd.DataFrame()
        RMSE_data = pd.DataFrame()
        R_data = pd.DataFrame()

        for wind_level in wind_all_level:
            locname, mse, means = main(1, 0, sj, word_document, time_step, wind_level)
            mse = np.array(mse)
            mean_obs_data[str(wind_level)] = np.array(means)[:, 0]
            mean_pre_data[str(wind_level)] = np.array(means)[:, 1]
            MAE_data[str(wind_level)] = mse[:, 0]
            MRPE_data[str(wind_level)] = mse[:, 1]
            RMSE_data[str(wind_level)] = mse[:, 2]
            R_data[str(wind_level)] = mse[:, 3]
            def select_plot_number(number):
                fig, ax = plt.subplots(2, 2)
                plt.subplots_adjust(top=0.92, bottom=0.09, left=0.11, right=0.975, hspace=0.34, wspace=0.225)
                plot_statistic_variable_in_hline(ax[0][0], mse[:, 0], 'MAE (m/s)', number)
                plot_statistic_variable_in_hline(ax[0][1], mse[:, 1], 'MRE (%)', number)
                plot_statistic_variable_in_hline(ax[1][0], mse[:, 2], 'RMSE (m/s)', number)
                plot_statistic_variable_in_hline(ax[1][1], mse[:, 3], 'r', number)
                fig.suptitle('%s 逐%i小时平均 风速范围：%s-%sm/s' % (locname, time_step, str(wind_level[0]), str(wind_level[1])))
                plt.savefig('%s 逐%i小时平均 %i.png' % (locname, time_step, number))
                plt.close()

            # select_plot_number(7)
            # select_plot_number(3)
            # word_document.add_picture('%s 逐%i小时平均 %i.png' % (locname, time_step, 7), width=Inches(5))
            # word_document.add_picture('%s 逐%i小时平均 %i.png' % (locname, time_step, 3), width=Inches(5))


        list = [mean_pre_data-mean_obs_data]
        name_forvariable = 0
        formater = "{0:.02f}".format
        creat_word_table(document, str(name_forvariable), list[name_forvariable].T.applymap(formater))

        # list = [mean_obs_data, mean_pre_data, MAE_data, MRPE_data, RMSE_data, R_data]
        # for name_forvariable in range(6):
        #     formater = "{0:.02f}".format
        #     # 或者
        #     # format = lambda x:'%.2f' % x
        #     creat_word_table(document, str(name_forvariable), list[name_forvariable].T.applymap(formater))
        # document.save('%s 逐%i小时平均 %i风级.docx' % (locname, time_step, len(wind_all_level)))

        document.save('%s 逐%i小时平均 %i风级.docx' % (locname, time_step, len(wind_all_level)))

wind_all_level2 = [[0,3.4],[3.4,5.5],[5.5,8.0],[8.0,10.8], [10.8,13.9],[13.9,100]]
wind_all_level1 =[[0.3,1.6],[1.6,3.4],[3.4,5.5],[5.5,8.0],[8.0,10.8], [10.8,13.9],[13.9,17.1]]


main2(1, wind_all_level1)
main2(1, wind_all_level2)
# main2(6)
# main2(12)
# main2(24)



# def plot_all_statistic_variable_in_hline(all_mse):
#     '''variables = ['100 风速','20 风速']
# names = ['瑞安', '苍南']
# for i in range(0,1):
# name = names[i]
# all_mse = pd.DataFrame()
# for j in range(3):
#     for sj in variables:
#         locname, mse = main(i, j, sj)
#         all_mse[locname] = mse'''
#     all_mse = all_mse.set_index(np.arange(24,169,24))
#     order = ['%s_20 风速'%name, '%s_20 NW风速'%name, '%s_20 SE风速'%name,'%s_100 风速'%name, '%s_100 NW风速'%name, '%s_100 SE风速'%name]
#     all_mse = all_mse[order]
#     all_mse.columns = ['%s 20 风速'%name, '%s 20 NW风速'%name, '%s 20 SE风速'%name,'%s 100 风速'%name, '%s 100 NW风速'%name, '%s 100 SE风速'%name]
#
#     all_mse.ZhengZhou.bar(legend=False)
#     ax = plt.gca()
#     box = ax.get_position()
#     ax.set_position([box.x0, box.y0 + box.height * 0.1,
#                      box.width, box.height * 0.9])
#
#     # Put a legend below current axis
#     plt.subplots_adjust(top=0.86,bottom=0.11,left=0.11,right=0.9,hspace=0.2,wspace=0.2)
#     ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.18),
#               fancybox=True, shadow=True, ncol=3)
#     ax.set_xlabel('预报时效 （小时）')
#     ax.set_ylabel('平均绝对误差 （m/s）')
#     ax.axhline(0, color='black', alpha=0.8)
#     plt.savefig('%i.png'%i)
#     plt.close()
