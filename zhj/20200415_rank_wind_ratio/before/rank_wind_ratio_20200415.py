import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from docx import Document
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.shared import Cm
from docx.oxml.ns import qn
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
'''2020-04-15 老师让我处理的风速数据
截图中是日期，2020010112代表2020年1月1日12时（国际时，需要+8小时，才和excel对应）。1，你按照这个时次，从excel的605684#和标签中挑选数据，时段为（对应截图时次+174小时），作序列图，每个起始日期作一个图。只到截止时段是3月18日。2，把所有起始日期的对应的时段数据，按照风级归类，按不同类进行分析（如下图）。
举例：（1）2020010112，对应的应该是2020010120的北京时，这时你需要从excel中从2020010120时开始取数据，一共取174个时次。excel两个标签，你把取到的两个标签的数据画序列图放在一个图上，即一张图两个曲线。估计最后一个起始时间，差不多是2020030812的国际时时刻。那么一共应该是12幅图。
（2）分等级后，表格最后一个样本占比，是该等级样本占总样本的比例。误差范围中的数据，就是excel两个标签数据相减，取绝对值，看看占等级样本总数的比例。注意：这里就不分12个起始时次了，是12个起始时次的数据全放在一起，一共是12*174个样本数。
风速对比改进：1，时次调整为72个时次。2，排除掉系统误差，即：我们有12个起报时次，用12*72小时，这么多样本，将预报的平均值减去观测的平均值，比如得到A，这个A要记录一下，哪怕写在一张纸上。然后把所有的预报值都减去A，当成新的预报值。3，再算一遍，先算一个瑞安100的给我看下；如果ok，就继续算所有的。
'''

def creat_word_table(document, title, df):

    p = document.add_paragraph('')
    p.add_run(title).bold =True
    p.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    tb = document.add_table(rows=len(df.index)+2, cols=len(df.columns)+1)
    # tb.add_row()

    table_title = ['风速范围', '±1m/s（%）', '±3m/s（%）', '±3m/s（%）', '样本占比（%）']
    tb.cell(0, 2).text = '误差范围'
    for i in range(5):
        tb.cell(1, i).text = table_title[i]  # 添加表头

    print(len(df.index))
    for row in range(2, len(df.index) + 2):
        for col in range(1,len(df.columns)+1):
            tb.cell(row, col).width = 1
            tb.cell(row, 0).text = '%s' %df.index[row-2]
            tb.cell(row, col).text = '%.2f' %df.iloc[row-2, col-1]
            # tb.cell(row, col).width = Cm(6)
            # tb.cell(row, col).ipynb
            
    # tb.style = 'Medium Grid 1 Accent 1'
    tb.autofit = True


def create_sdata(obs_data, pre_data):
    begin = pre_data.index[0]
    if time_step == 24:
        index = pd.date_range(begin, '%s 23:00'%str(begin+pd.Timedelta(3, unit='D'))[:10], freq='H')
    else:
        index = pd.date_range(begin, freq='H', periods=72)
    sdata = pd.DataFrame()

    sdata['obs'] = obs_data[compare_data_name].loc[index]
    sdata['pre'] = pre_data[compare_data_name].loc[index]
    # sdata['pre'] = np.array(sdata['pre'])-A
    # print(A)
    return sdata

def create_sdata_in_A(obs_data, pre_data, A):
    begin = pre_data.index[0]
    if time_step == 24:
        index = pd.date_range(begin, '%s 23:00'%str(begin+pd.Timedelta(3, unit='D'))[:10], freq='H')
    else:
        index = pd.date_range(begin, freq='H', periods=72)
    sdata = pd.DataFrame()
    sdata['obs'] = obs_data[compare_data_name].loc[index]
    sdata['pre'] = (pre_data[compare_data_name].loc[index]-A)
    return sdata

def plot_data(sdata, legends, savename, title):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(sdata.index, sdata['obs'], label=legends[0])
    ax.plot(sdata.index, sdata['pre'], label=legends[1])
    years = mdates.HourLocator(interval = 24)  # every year
    # months = mdates.HourLocator(12)  # every month
    yearsFmt = mdates.DateFormatter('%m-%d %H:00')
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(yearsFmt)
    ax.set_xlabel('2020年')
    ax.set_ylabel('风速 m/s')
    ax.set_title(title)
    ax.legend(loc='upper right')
    # ax.xaxis.set_minor_locator(months)
    # ax.xaxis.set_minor_formatter(mdates.DateFormatter('%d %H:00'))
    plt.show()
    plt.savefig('%s.png'%savename)
    plt.close()

def compare_data(all_data, level):
    all_data['sub'] = (all_data['obs'] - all_data['pre']).abs()
    all_data['pre'] = all_data['pre'].round(0)

    if level == 'more':
        rank_levels = [0,0.3,1.6,3.4,5.5,8,10.8,13.9,17.2,20.8,24.5,28.5,32.7,37.0,41.5,46.2,51.0,56.1,61.3]
        index = []
        for i in np.arange(len(rank_levels)-1):
            index.append('[%.1f-%.1fm/s]'%(rank_levels[i], rank_levels[i+1]-0.1))
    else:
        rank_levels = [0,2.1,4,7,9,14,24,100]
        index = ['[0-2m/s]', '[2.1-3.9m/s]']
        for i in np.arange(2, len(rank_levels)-1):
            index.append('[%i-%im/s]'%(rank_levels[i], rank_levels[i+1]-1))
    # rang 4-6m/s
    ranks = []
    sta_result = pd.DataFrame()
    for i in np.arange(len(rank_levels)-1):
        begin = rank_levels[i]
        end = rank_levels[i+1]
        ranks = all_data[(all_data['pre'] >= begin) &
                         (all_data['pre'] < end)]
        sample_lens = ranks.count()[0]
        ms1 = ranks[(ranks['sub'] <= 1)].count()[0]
        ms2 = ranks[(ranks['sub'] <= 2)].count()[0]
        ms3 = ranks[(ranks['sub'] <= 3)].count()[0]
        ms = [ms1/sample_lens, ms2/sample_lens, ms3/sample_lens, sample_lens/all_data.count()[0]]
        a = [round(i, 4)*100 for i in ms]
        sta_result['%i'%i] = a
    sta_result = sta_result.T
    sta_result.index = index
    sta_result = sta_result.dropna(axis=0, how='any')
    return sta_result

# variables = ['100 风速','20 风向','20 风速']


def main(choose, level, time_step, systematic_error = False):

    area = choose[0]
    sheetnumber= choose[1]
    compare_data_name= choose[2]
    def find_things_you_need(area, sheetnumber, compare_data_name):
        # 绘制标签
        if area == 0:
            title = '瑞安'
            sheetlabel = ['SE', 'NW', '']
            legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
        else:
            title = '苍南'
            sheetlabel = ['NW', 'SE', '']
            legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
        legendlabels = ['实测 '+legendlabel, '预测 '+legendlabel]

        # 找到要用的数据
        sheetcangnan = ['605019#', '605683#', 'ave']
        sheetruian = ['605684#', '605537#', 'ave']
        if area == 0:
            sheetname = sheetruian[sheetnumber]
        else:
            sheetname = sheetcangnan[sheetnumber]
        areaname = ['ruian', 'cangnan']
        areanumber = ['33038101', '33032701']
        begin_times = [['0101', '0102', '0103', '0104', '0105', '0106', '0107', '0108', '0109', '0110', '0111', '0112',
                        '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                        '0125', '0126', '0127', '0128', '0129', '0130', '0201', '0202', '0203', '0204', '0208', '0216',
                        '0222', '0228', '0301', '0308'],

                       ['0108', '0109', '0110', '0111', '0112',
                        '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                        '0125', '0126', '0127', '0128', '0129', '0130', '0201', '0202', '0203', '0204', '0208', '0216',
                        '0222', '0228', '0301', '0308']]
        # begin_times = [['0101','0108', '0116','0124','0130','0201','0208','0216',
        #               '0222','0228','0301','0308'],
        #                ['0116', '0124', '0130', '0201', '0208', '0216',
        #                 '0222', '0228', '0301', '0308']
        #                ]
        # begin_times = [['0101','0108', '0116','0124','0130','0201','0208',
        #               '0222','0301','0308'],
        #                ['0116', '0124', '0130', '0201', '0208',
        #                 '0222', '0301', '0308']
        #                ]
        begin_time = begin_times[area]
        return legendlabels, title, begin_time, areaname, areanumber, sheetname
    legendlabels, title, begin_time, areaname, areanumber, sheetname = find_things_you_need(area, sheetnumber, compare_data_name)


    # create sheet3 averaged wind
    data1=pd.read_excel(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\%s.xlsx'%areaname[area],
                    index_col=0, parse_dates=True,sheet_name=sheetname,skiprows=1,
                    names=['0', '100 风速','20 风向','20 风速', 'nans'])


    all_data = pd.DataFrame()
    for i in begin_time:
        data2 = pd.read_csv(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\2020%s12\%s.2020%s12.csv'%(i, areanumber[area], i),
                            index_col=0, parse_dates=True, skiprows=1,
                            names=['0', '100 风速','20 风向','20 风速'])
        data2 = data2.set_index(data2.index+pd.Timedelta(8, unit='H'))
        sdata = create_sdata(data1, data2)
        all_data = pd.concat([all_data, sdata])

    if systematic_error is True:
        A = all_data['pre'].mean() - all_data['obs'].mean()
        print('系统误差A为：', A)
        all_data = pd.DataFrame()
        for i in begin_time:
            data2 = pd.read_csv(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\2020%s12\%s.2020%s12.csv'%(i, areanumber[area], i),
                                index_col=0, parse_dates=True, skiprows=1,
                                names=['0', '100 风速','20 风向','20 风速'])
            data2 = data2.set_index(data2.index+pd.Timedelta(8, unit='H'))
            sdata = create_sdata_in_A(data1, data2, A)
            plot_data(sdata, legends=legendlabels, savename=i, title=title)
            all_data = pd.concat([all_data, sdata])

    def select_time_step(all_data, time_step):
        if time_step == 24:
            all_data = all_data.groupby([all_data.index.month, all_data.index.day]).mean().reindex()
        else:
            sls = 0
        return all_data

    all_data = select_time_step(all_data, time_step)
    df = compare_data(all_data, level)
    print(df)
    if title == '瑞安':
        creat_word_table(document, '表1 2020年1月1日 - 3月12日 %s%s预报与实测结果检验'%(title, legendlabels[0][2:]),
                         df)
    else:
        creat_word_table(document, '表1 2020年1月8日 - 3月12日 %s%s预报与实测结果检验'%(title, legendlabels[0][2:]),
                         df)


    return




# time_step = 24
# sys_err = False
# level = 'more'
# compare_data_name = '100 风速'
# main([0,0,'100 风速'], 'more', 24, False)
for time_step in [1,24]:
    for sys_err in [False, True]:
        for level in ['less', 'more']:
            document = Document()
            document.styles['Normal'].font.name = '宋体'
            document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')
            variables = ['100 风速', '20 风速']
            names = ['瑞安', '苍南']
            for i in range(1):
                for j in range(3):
                    for sj in variables:
                        compare_data_name = sj
                        choose = [i, j, sj]
                        main(choose, level, time_step, sys_err)

            for sj in variables:
                compare_data_name = sj
                choose = [1, 0, sj]
                main(choose, level, time_step, sys_err)

            if time_step == 1:
                time = '逐时'
            else:
                time = '每日'
            if sys_err is False:
                sysss = ''
            else:
                sysss = '去系统误差'
            if level == 'less':
                levessss = '新分级'
            else:
                levessss = '旧分级'
            document.save('%s%s%s.docx'%(time, sysss, levessss))


