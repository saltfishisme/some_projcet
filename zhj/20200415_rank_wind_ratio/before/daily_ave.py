import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from docx import Document
from docx.shared import Inches

'''计算日平均风速，画序列图。例如1.1日-3.15日，时间序列图，
	这样一张图上有8根线，分别是观测、预见期为1天的预报、预见期为2天的预报。。。。。。

	备注：比如，1月31日，有观测值，还有7个预报值，这7个预报值，分别是1月31日，1月30日，1月29日，1月28日。。。以此类推得来的。

'''


def main(area, sheetnumber, compare_data_name):
    def choose_data(time_loc):
        need_time = begin_time[time_loc]
        sdata = []

        for i in range(time_loc-7, time_loc):
            select_time = begin_time[i]
            data2 = pd.read_csv(
                r'D:\basis\some_project\20200415_rank_wind_ratio\forecast\forecast\%s.2020%s12.csv' % (
                areanumber[area], select_time),
                index_col=0, parse_dates=True, skiprows=1,
                names=['0', '100 风速', '20 风向', '20 风速'])
            sdata.append(data2.set_index(data2.index + pd.Timedelta(8, unit='H'))['2020-%s-%s'% (need_time[0:2], need_time[2:4])][compare_data_name].mean())
        rowdata = data1['2020-%s-%s' % (need_time[0:2], need_time[2:4])][compare_data_name].mean()
        return sdata, rowdata

    # 绘制标签
    if area == 0:
        title = '瑞安'
        sheetlabel = ['SE', 'NW', '']
        legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
    else:
        title = '苍南'
        sheetlabel = ['NW', 'SE', '']
        legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
    legendlabels = ['实测 '+legendlabel, '预测 '+legendlabel]

    # 找到要用的数据
    sheetcangnan = ['605019#', '605683#', 'ave']
    sheetruian = ['605684#', '605537#', 'ave']
    if area == 0:
        sheetname = sheetruian[sheetnumber]
    else:
        sheetname = sheetcangnan[sheetnumber]
    areaname = ['ruian', 'cangnan']
    areanumber = ['3303811A', '3303273A']

    begin_times = [['0101', '0102', '0103', '0104', '0105', '0106', '0107', '0108', '0109', '0110', '0111', '0112',
                    '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                    '0125', '0126', '0127', '0128', '0129', '0130','0131', '0201','0202','0203','0204', '0205', '0206', '0207', '0208', '0209', '0210', '0211', '0212',
                    '0213', '0214', '0215', '0216', '0217', '0218', '0219', '0220', '0221', '0222', '0223', '0224',
                    '0225', '0226', '0227', '0228', '0301','0302','0303','0304', '0305', '0306', '0307', '0308','0309',
                    '0310','0311','0312','0313','0314','0315','0316','0317','0318'],

                   ['0101', '0102', '0103', '0104', '0105', '0106', '0107','0108', '0109', '0110', '0111', '0112',
                    '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                    '0125', '0126', '0127', '0128', '0129', '0130','0131','0201','0202','0203','0204', '0205', '0206', '0207', '0208', '0209', '0210', '0211', '0212',
                    '0213', '0214', '0215', '0216', '0217', '0218', '0219', '0220', '0221', '0222', '0223', '0224',
                    '0225', '0226', '0227', '0228', '0301','0302','0303','0304', '0305', '0306', '0307', '0308','0309',
                    '0310','0311','0312','0313','0314','0315','0316']]
    #                ]
    # begin_times = [['0101','0108', '0116','0124','0130','0201','0208',
    #               '0222','0301','0308'],
    #                ['0116', '0124', '0130', '0201', '0208',
    #                 '0222', '0301', '0308']
    #                ]
    begin_time = begin_times[area]

    # create sheet3 averaged wind
    # create sheet3 averaged wind
    # 观测数据
    data1=pd.read_excel(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\%s.xlsx'%areaname[area],
                    index_col=0, parse_dates=True,sheet_name=sheetname,skiprows=0,
                    names=['0', '100 风速','20 风向','20 风速', 'nans'])


    all_statistic_variable_series = []

    # 按照7天预见期分类
    pre_mean = []
    obs_mean = []
    for i in range(7,len(begin_time)):
        spre_mean, sobs_mean = choose_data(i)
        need_time = begin_time[i]
        pre_mean.append(spre_mean)
        obs_mean.append(sobs_mean)
    if area == 0:
        time = pd.date_range('2020-01-08', '2020-03-18', freq='D')
    elif area ==1:
        time = pd.date_range('2020-01-08', '2020-03-16', freq='D')
    time = time[time!='2020-02-29']
    df = pd.DataFrame(index=time)
    for i in range(7):
        df['pre%iD'%(7-i)] = np.array(pre_mean)[:,i]
    df['obs'] = np.array(obs_mean)
    print(df[df.index.month == 3])
    plot_mean(df,legendlabels, title)
    return


def plot_mean(df,legendlabels, title):
    def plot_monthly_data(df, month):
        '''再另出一套一个月一幅图，这样能看的清楚一点。'''
        fig, ax = plt.subplots(1,1, figsize=(7,5))
        plt.subplots_adjust(0.07,0.16,0.99,0.94)
        for i in range(3):
            ax.plot(df.index, df['pre%iD'%(i+1)], label='预见期：%i天'%(i+1))
        ax.plot(df.index, df['obs'], label='实测值', linestyle='--', color='black', linewidth=2)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.07,
                         box.width, box.height * 0.9])

        ax.set_ylabel('风速 m/s')
        ax.set_xlabel('2020年')

        legendlabel_single = legendlabels[0][3:].split(' ')
        print(legendlabel_single)
        ax.set_title('%s%s日均%s%s'%(title, month, legendlabel_single[0], legendlabel_single[1]))
        # Put a legend below current axis
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12),
                  fancybox=True, shadow=True, ncol=4)
        # years = mdates.HourLocator(interval=24)
        yearsFmt = mdates.DateFormatter('%m-%d')
        # ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)
        # plt.show()
        plt.savefig('fig%s.png' %month)
        plt.close()

    plot_monthly_data(df[df.index.month==1], '1月')
    plot_monthly_data(df[df.index.month == 2], '2月')
    plot_monthly_data(df[df.index.month == 3], '3月')

def main2():
    variables = ['100 风速','20 风速']
    names = ['瑞安', '苍南']
    for i in range(0,1):
        for j in range(3):
            for sj in variables:

                main(i, j, sj)
                document.add_picture('fig1月.png', width=Inches(5))
                document.add_picture('fig2月.png', width=Inches(5))
                document.add_picture('fig3月.png', width=Inches(5))

    for sj in variables:
            main(1, 0, sj)
            document.add_picture('fig1月.png', width=Inches(5))
            document.add_picture('fig2月.png', width=Inches(5))
            document.add_picture('fig3月.png', width=Inches(5))

    document.save('预见期比较.docx')

document = Document()
main2()
