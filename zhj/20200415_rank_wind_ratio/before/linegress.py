import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from docx import Document
from docx.shared import Inches
'''2020-04-15 老师让我处理的风速数据
截图中是日期，2020010112代表2020年1月1日12时（国际时，需要+8小时，才和excel对应）。1，你按照这个时次，从excel的605684#和标签中挑选数据，时段为（对应截图时次+174小时），作序列图，每个起始日期作一个图。只到截止时段是3月18日。2，把所有起始日期的对应的时段数据，按照风级归类，按不同类进行分析（如下图）。
举例：（1）2020010112，对应的应该是2020010120的北京时，这时你需要从excel中从2020010120时开始取数据，一共取174个时次。excel两个标签，你把取到的两个标签的数据画序列图放在一个图上，即一张图两个曲线。估计最后一个起始时间，差不多是2020030812的国际时时刻。那么一共应该是12幅图。
（2）分等级后，表格最后一个样本占比，是该等级样本占总样本的比例。误差范围中的数据，就是excel两个标签数据相减，取绝对值，看看占等级样本总数的比例。注意：这里就不分12个起始时次了，是12个起始时次的数据全放在一起，一共是12*174个样本数。
风速对比改进：1，时次调整为72个时次。2，排除掉系统误差，即：我们有12个起报时次，用12*72小时，这么多样本，将预报的平均值减去观测的平均值，比如得到A，这个A要记录一下，哪怕写在一张纸上。然后把所有的预报值都减去A，当成新的预报值。3，再算一遍，先算一个瑞安100的给我看下；如果ok，就继续算所有的。
'''


def main(area, sheetnumber, compare_data_name, document, time_step):
    def choose_data(step):
        sdata = pd.DataFrame()
        rowdata = pd.DataFrame()
        for i in begin_time:
            data2 = pd.read_csv(
                r'D:\basis\some_project\20200415_rank_wind_ratio\rui\2020%s12\%s.2020%s12.csv' % (
                i, areanumber[area], i),
                index_col=0, parse_dates=True, skiprows=1,
                names=['0', '100 风速', '20 风向', '20 风速'])
            data2 = data2.set_index(data2.index + pd.Timedelta(8, unit='H'))[
                str(np.datetime64('2020-%s-%s' % (i[0:2], i[2:4])) + pd.Timedelta(step, unit='D'))[:10]]
            rowdata2 = data1[
                str(np.datetime64('2020-%s-%s' % (i[0:2], i[2:4])) + pd.Timedelta(step, unit='D'))[:10]]
            sdata = pd.concat([sdata, data2])
            rowdata = pd.concat([rowdata, rowdata2])
        return sdata, rowdata

    def regg(ax, time, y, monthname, ylabel_name, labels=None, timeticksname=None):
        def fit_line(x, y):
            # from scipy.stats import linregress
            # (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(np.array(x), y)
            # return beta_coeff, intercept, rvalue, pvalue, stderr
            import statsmodels.api as sm
            import statsmodels
            from statsmodels.formula.api import ols
            import pandas as pd
            sx = sm.add_constant(x)
            models = sm.OLS(y, sx)
            model = models.fit()
            fit_y = model.fittedvalues
            from sklearn.metrics import r2_score
            me = np.sum(x-y)/len(y)
            r2 = r2_score(x,y)
            mae = statsmodels.tools.eval_measures.meanabs(x,y)
            rmse = statsmodels.tools.eval_measures.rmse(x,y)
            print(x,y)
            return model.params[1], model.params[0], (model.rsquared)**(1/2), me, mae, rmse

        def plotall(ax, time, line1, month, beta_coeff, intercept, r, mse, mae, rmse,
                    varible_name, timeticksname=None, labels=None):
            line2 = time * beta_coeff + intercept

            if compare_data_name[0] == '1':
                ax.set_ylim(0,19)
                ax.set_xlim(0,19)
            else:
                ax.set_ylim(0,16)
                ax.set_xlim(0,16)
            plt.scatter(time, line1, label='观测', color='blue', s=1.5)
            plt.plot(time, line2, label='趋势', color='red')
            plt.plot([0,ax.get_xlim()[1]], [0, ax.get_ylim()[1]], linestyle='--', color='grey')

            maxy = ax.get_ylim()[1]
            miny = ax.get_ylim()[0]
            ranges = maxy - miny
            ax.text(0.5, maxy - ranges * 0.05,
                    '预报时效：T+%ih'%predict_time, fontsize=12)
            ax.text(0.5, maxy - ranges * 0.11,
                    '%sy=%.4fx+%.2f' % (month, beta_coeff, intercept), fontsize=12)

            maxx = ax.get_xlim()[1]
            minx = ax.get_xlim()[0]
            ranges = maxx - minx
            ax.text(1, 0.5,
                    'ME=%0.2f' % mse, fontsize=12)
            ax.text(ranges/4+1, 0.5,
                    'MAE=%0.2f' % mae, fontsize=12)
            ax.text(ranges*2/4+1, 0.5,
                    'RMSE=%0.2f' % rmse, fontsize=12)

            ax.text(ranges*3/4+1, 0.5,
                    'r=%.2f' % (r), fontsize=12)


            # plt.legend(loc=1)
            ax.set_ylabel('%s (m/s)' % varible_name)
            ax.set_xlabel('%s (m/s)' %legendlabels[0])
            ax.set_title(title)
            if labels is not None:
                plt.text(ax.get_xlim()[0], ax.get_ylim()[1], labels)
            if timeticksname is not None:
                from pandas import Series
                ax.set_xticks(ax.get_xticks()[1:-2])
                x = list(Series(ax.get_xticks()).map(int))
                ax.set_xticklabels(timeticksname[x])
            return


        beta_coeff, intercept, rvalue, mse, mae, rmse = fit_line(time, y)
        plotall(ax, time, y, monthname, beta_coeff, intercept, rvalue, mse, mae, rmse, ylabel_name, labels=labels)
        return [mse,mae,rmse,rvalue]

    # 绘制标签
    if area == 0:
        title = '瑞安'
        sheetlabel = ['SE', 'NW', '']
        legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
    else:
        title = '苍南'
        sheetlabel = ['NW', 'SE', '']
        legendlabel = compare_data_name.split()[0]+' '+sheetlabel[sheetnumber]+''+compare_data_name.split()[1]
    legendlabels = ['实测 '+legendlabel, '预测 '+legendlabel]

    # 找到要用的数据
    sheetcangnan = ['605019#', '605683#', 'ave']
    sheetruian = ['605684#', '605537#', 'ave']
    if area == 0:
        sheetname = sheetruian[sheetnumber]
    else:
        sheetname = sheetcangnan[sheetnumber]
    areaname = ['ruian', 'cangnan']
    areanumber = ['33038101', '33032701']

    begin_times = [['0101', '0102', '0103', '0104', '0105', '0106', '0107', '0108', '0109', '0110', '0111', '0112',
                    '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                    '0125', '0126', '0127', '0128', '0129', '0130','0201','0202','0203','0204', '0208', '0216',
                    '0222', '0228', '0301', '0308'],

                   ['0108','0109', '0110', '0111', '0112',
                    '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                    '0125', '0126', '0127', '0128', '0129', '0130','0201','0202','0203','0204', '0208', '0216',
                    '0222', '0228', '0301', '0308']]
    #                ]
    # begin_times = [['0101','0108', '0116','0124','0130','0201','0208',
    #               '0222','0301','0308'],
    #                ['0116', '0124', '0130', '0201', '0208',
    #                 '0222', '0301', '0308']
    #                ]
    begin_time = begin_times[area]

    # create sheet3 averaged wind
    # create sheet3 averaged wind
    data1=pd.read_excel(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\%s.xlsx'%areaname[area],
                    index_col=0, parse_dates=True,sheet_name=sheetname,skiprows=0,
                    names=['0', '100 风速','20 风向','20 风速', 'nans'])


    all_statistic_variable_series = []

    for i in range(1,8):
        predict_time = i*24
        pre_data, obs_data = choose_data(i)
        pre_data = pre_data.dropna(axis=0, how='any')
        obs_data = obs_data.dropna(axis=0, how='any')
        index = pre_data.index.intersection(obs_data.index)
        pre_data = pre_data.loc[index]
        obs_data = obs_data.loc[index]
        if time_step == 24:
            pre_data = pre_data.groupby([pre_data.index.month, pre_data.index.day]).mean().reindex()
            obs_data = obs_data.groupby([obs_data.index.month, obs_data.index.day]).mean().reindex()

        if time_step == 12:
            pre_data = pre_data.groupby([obs_data.index.month, obs_data.index.day]).resample('12H').mean()
            obs_data = obs_data.groupby([obs_data.index.month, obs_data.index.day]).resample('12H').mean()
        if time_step == 6:
            pre_data = pre_data.groupby([obs_data.index.month, obs_data.index.day]).resample('6H').mean()
            obs_data = obs_data.groupby([obs_data.index.month, obs_data.index.day]).resample('6H').mean()

        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(111)
        mse = regg(ax, np.array(obs_data[compare_data_name]), np.array(pre_data[compare_data_name]),
             '', legendlabels[1])
        print(mse)
        all_statistic_variable_series.append(mse)
        # plt.show()
        plt.savefig('第%i天_%s_%s.png'%(i,title, legendlabels[0][3:]))
        plt.close()
        document.add_picture('第%i天_%s_%s.png'%(i,title, legendlabels[0][3:]), width=Inches(5))
    # document.save('%s_%s.docx'%(title, legendlabels[0][3:]))
    return '%s %s'%(title, legendlabels[0][3:]), all_statistic_variable_series




def plot_statistic_variable_in_hline(ax, all_mse, ylabel, number):
    if number == 3:
        width = 5
    else:
        width = 10
    print(all_mse)
    ax.bar(np.arange(24, 169, 24)[:number], all_mse[:number], width=width)
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0 + box.height * 0.1,
    #                  box.width, box.height * 0.9])

    # Put a legend below current axis
    # plt.subplots_adjust(top=0.86, bottom=0.11, left=0.11, right=0.9, hspace=0.2, wspace=0.2)
    # ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.18),
    #           fancybox=True, shadow=True, ncol=3)
    ax.set_xlabel('预报时效 （小时）')
    ax.set_ylabel(ylabel)
    ax.axhline(0, color='black', alpha=0.8)
    ax.set_xticks(np.arange(24,169,24)[:number])
    # plt.savefig('%i.png' % i)
    # plt.close()


def main2(time_step):
    variables = ['100 风速','20 风速']
    names = ['瑞安', '苍南']
    for i in range(0,1):
        name = names[i]
        all_mse = pd.DataFrame()
        for j in range(3):
            for sj in variables:

                document = Document()
                locname, mse = main(i, j, sj, document, time_step)
                mse = np.array(mse)

                def select_plot_number(number):
                    fig, ax = plt.subplots(2,2)
                    plt.subplots_adjust(top=0.92,bottom=0.09,left=0.11,right=0.975,hspace=0.34,wspace=0.225)
                    plot_statistic_variable_in_hline(ax[0][0], mse[:,0], 'ME (m/s)', number)
                    plot_statistic_variable_in_hline(ax[0][1], mse[:, 1], 'MAE (m/s)', number)
                    plot_statistic_variable_in_hline(ax[1][0], mse[:, 2], 'RMSE (m/s)', number)
                    plot_statistic_variable_in_hline(ax[1][1], mse[:, 3], 'r', number)
                    fig.suptitle('%s 逐%i小时平均'%(locname, time_step))
                    plt.savefig('%s 逐%i小时平均 %i.png'%(locname, time_step, number))

                select_plot_number(7)
                select_plot_number(3)
                document.add_picture('%s 逐%i小时平均 %i.png'%(locname, time_step, 7), width=Inches(5))
                document.add_picture('%s 逐%i小时平均 %i.png' % (locname, time_step, 3), width=Inches(5))
                document.save('%s 逐%i小时平均.docx' % (locname, time_step))

    for sj in variables:
        document = Document()
        locname, mse = main(1, 0, sj, document, time_step)
        mse = np.array(mse)

        def select_plot_number(number):
            fig, ax = plt.subplots(2, 2)
            plt.subplots_adjust(top=0.92, bottom=0.09, left=0.11, right=0.975, hspace=0.34, wspace=0.225)
            plot_statistic_variable_in_hline(ax[0][0], mse[:, 0], 'ME (m/s)', number)
            plot_statistic_variable_in_hline(ax[0][1], mse[:, 1], 'MAE (m/s)', number)
            plot_statistic_variable_in_hline(ax[1][0], mse[:, 2], 'RMSE (m/s)', number)
            plot_statistic_variable_in_hline(ax[1][1], mse[:, 3], 'r', number)
            fig.suptitle('%s 逐%i小时平均' % (locname, time_step))
            plt.savefig('%s 逐%i小时平均 %i.png' % (locname, time_step, number))

        select_plot_number(7)
        select_plot_number(3)
        document.add_picture('%s 逐%i小时平均 %i.png' % (locname, time_step, 7), width=Inches(5))
        document.add_picture('%s 逐%i小时平均 %i.png' % (locname, time_step, 3), width=Inches(5))
        document.save('%s 逐%i小时平均.docx' % (locname, time_step))

main2(1)
main2(6)
main2(12)
main2(24)


# def plot_all_statistic_variable_in_hline(all_mse):
#     '''variables = ['100 风速','20 风速']
# names = ['瑞安', '苍南']
# for i in range(0,1):
# name = names[i]
# all_mse = pd.DataFrame()
# for j in range(3):
#     for sj in variables:
#         locname, mse = main(i, j, sj)
#         all_mse[locname] = mse'''
#     all_mse = all_mse.set_index(np.arange(24,169,24))
#     order = ['%s_20 风速'%name, '%s_20 NW风速'%name, '%s_20 SE风速'%name,'%s_100 风速'%name, '%s_100 NW风速'%name, '%s_100 SE风速'%name]
#     all_mse = all_mse[order]
#     all_mse.columns = ['%s 20 风速'%name, '%s 20 NW风速'%name, '%s 20 SE风速'%name,'%s 100 风速'%name, '%s 100 NW风速'%name, '%s 100 SE风速'%name]
#
#     all_mse.ZhengZhou.bar(legend=False)
#     ax = plt.gca()
#     box = ax.get_position()
#     ax.set_position([box.x0, box.y0 + box.height * 0.1,
#                      box.width, box.height * 0.9])
#
#     # Put a legend below current axis
#     plt.subplots_adjust(top=0.86,bottom=0.11,left=0.11,right=0.9,hspace=0.2,wspace=0.2)
#     ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.18),
#               fancybox=True, shadow=True, ncol=3)
#     ax.set_xlabel('预报时效 （小时）')
#     ax.set_ylabel('平均绝对误差 （m/s）')
#     ax.axhline(0, color='black', alpha=0.8)
#     plt.savefig('%i.png'%i)
#     plt.close()

