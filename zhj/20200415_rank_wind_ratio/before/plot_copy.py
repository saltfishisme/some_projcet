import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()
from docx import Document
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.shared import Cm
from docx.oxml.ns import qn
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx import Document
from docx.shared import Inches
'''2020-04-15 老师让我处理的风速数据
截图中是日期，2020010112代表2020年1月1日12时（国际时，需要+8小时，才和excel对应）。1，你按照这个时次，从excel的605684#和标签中挑选数据，时段为（对应截图时次+174小时），作序列图，每个起始日期作一个图。只到截止时段是3月18日。2，把所有起始日期的对应的时段数据，按照风级归类，按不同类进行分析（如下图）。
举例：（1）2020010112，对应的应该是2020010120的北京时，这时你需要从excel中从2020010120时开始取数据，一共取174个时次。excel两个标签，你把取到的两个标签的数据画序列图放在一个图上，即一张图两个曲线。估计最后一个起始时间，差不多是2020030812的国际时时刻。那么一共应该是12幅图。
（2）分等级后，表格最后一个样本占比，是该等级样本占总样本的比例。误差范围中的数据，就是excel两个标签数据相减，取绝对值，看看占等级样本总数的比例。注意：这里就不分12个起始时次了，是12个起始时次的数据全放在一起，一共是12*174个样本数。
风速对比改进：1，时次调整为72个时次。2，排除掉系统误差，即：我们有12个起报时次，用12*72小时，这么多样本，将预报的平均值减去观测的平均值，比如得到A，这个A要记录一下，哪怕写在一张纸上。然后把所有的预报值都减去A，当成新的预报值。3，再算一遍，先算一个瑞安100的给我看下；如果ok，就继续算所有的。
'''


def create_sdata(obs_data, pre_data):
    begin = pre_data.index[0]
    if time_step == 24:
        index = pd.date_range(begin, '%s 23:00' % str(begin + pd.Timedelta(3, unit='D'))[:10], freq='H')
    else:
        index = pd.date_range(begin, freq='H', periods=72)
    sdata = pd.DataFrame()
    interindex = obs_data.index.intersection(index)
    interindex = pre_data.index.intersection(interindex)
    sdata['obs'] = obs_data[compare_data_name].loc[interindex]
    sdata['pre'] = pre_data[compare_data_name].loc[interindex]

    # sdata['pre'] = np.array(sdata['pre'])-A
    # print(A)
    return sdata


def create_sdata_in_A(obs_data, pre_data, A):
    begin = pre_data.index[0]
    if time_step == 24:
        index = pd.date_range(begin, '%s 23:00' % str(begin + pd.Timedelta(3, unit='D'))[:10], freq='H')
    else:
        index = pd.date_range(begin, freq='H', periods=72)
    sdata = pd.DataFrame()
    sdata['obs'] = obs_data[compare_data_name].loc[index]
    sdata['pre'] = (pre_data[compare_data_name].loc[index] - A)
    return sdata


def plot_data(sdata, legends, savename, title):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(sdata.index, sdata['obs'], label=legends[0])
    ax.plot(sdata.index, sdata['pre'], label=legends[1])
    years = mdates.HourLocator(interval=24)  # every year
    # months = mdates.HourLocator(12)  # every month
    yearsFmt = mdates.DateFormatter('%m-%d %H:00')
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(yearsFmt)
    ax.set_xlabel('2020年')
    ax.set_ylabel('风速 m/s')
    ax.set_title(title)
    ax.legend(loc='upper right')
    # ax.xaxis.set_minor_locator(months)
    # ax.xaxis.set_minor_formatter(mdates.DateFormatter('%d %H:00'))
    # plt.show()
    plt.savefig('%s.png' % savename)
    plt.close()




# variables = ['100 风速','20 风向','20 风速']


def main(choose, document, systematic_error=False):
    area = choose[0]
    sheetnumber = choose[1]
    compare_data_name = choose[2]

    def find_things_you_need(area, sheetnumber, compare_data_name):
        # 绘制标签
        if area == 0:
            title = '瑞安'
            sheetlabel = ['SE', 'NW', '']
            legendlabel = compare_data_name.split()[0] + ' ' + sheetlabel[sheetnumber] + '' + compare_data_name.split()[
                1]
        else:
            title = '苍南'
            sheetlabel = ['NW', 'SE', '']
            legendlabel = compare_data_name.split()[0] + ' ' + sheetlabel[sheetnumber] + '' + compare_data_name.split()[
                1]
        legendlabels = ['实测 ' + legendlabel, '预测 ' + legendlabel]

        # 找到要用的数据
        sheetcangnan = ['605019#', '605683#', 'ave']
        sheetruian = ['605684#', '605537#', 'ave']
        if area == 0:
            sheetname = sheetruian[sheetnumber]
        else:
            sheetname = sheetcangnan[sheetnumber]
        areaname = ['ruian', 'cangnan']
        areanumber = ['3303811A', '3303273A']
        begin_times = [['0101', '0102', '0103', '0104', '0105', '0106', '0107', '0108', '0109', '0110', '0111', '0112',
                        '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                        '0125', '0126', '0127', '0128', '0129', '0130', '0201', '0202', '0203', '0204', '0205', '0206',
                        '0207', '0208', '0209', '0210', '0211', '0212',
                        '0213', '0214', '0215', '0216', '0217', '0218', '0219', '0220', '0221', '0222', '0223', '0224',
                        '0225', '0226', '0227', '0228', '0301', '0302', '0303', '0304', '0305', '0306', '0307', '0308',
                        '0309', '0310', '0311'],

                       ['0108', '0109', '0110', '0111', '0112',
                        '0113', '0114', '0115', '0116', '0117', '0118', '0119', '0120', '0121', '0122', '0123', '0124',
                        '0125', '0126', '0127', '0128', '0129', '0130', '0201', '0202', '0203', '0204', '0205', '0206',
                        '0207', '0208', '0209', '0210', '0211', '0212',
                        '0213', '0214', '0215', '0216', '0217', '0218', '0219', '0220', '0221', '0222', '0223', '0224',
                        '0225', '0226', '0227', '0228', '0301', '0302', '0303', '0304', '0305', '0306', '0307', '0308',
                        '0309']]

        #                ]
        begin_time = begin_times[area]
        return legendlabels, title, begin_time, areaname, areanumber, sheetname

    legendlabels, title, begin_time, areaname, areanumber, sheetname = find_things_you_need(area, sheetnumber,
                                                                                            compare_data_name)

    # create sheet3 averaged wind
    data1 = pd.read_excel(r'D:\basis\some_project\20200415_rank_wind_ratio\rui\%s.xlsx' % areaname[area],
                          index_col=0, parse_dates=True, sheet_name=sheetname, skiprows=1,
                          names=['0', '100 风速', '20 风向', '20 风速', 'nans'])

    all_data = pd.DataFrame()

    for i in begin_time:
        data2 = pd.read_csv(
            r'D:\basis\some_project\20200415_rank_wind_ratio\forecast\forecast\%s.2020%s12.csv' % (
                areanumber[area], i),
            index_col=0, parse_dates=True, skiprows=1,
            names=['0', '100 风速', '20 风向', '20 风速'])
        data2 = data2.set_index(data2.index + pd.Timedelta(8, unit='H'))
        sdata = create_sdata(data1, data2)
        if systematic_error is not True:
            plot_data(sdata, legends=legendlabels, savename=i, title=title)
            document.add_picture('%s.png' % i, width=Inches(5))
        all_data = pd.concat([all_data, sdata])

    if systematic_error is True:
        A = all_data['pre'].mean() - all_data['obs'].mean()
        print('系统误差A为：', A)
        all_data = pd.DataFrame()
        for i in begin_time:
            data2 = pd.read_csv(
                r'D:\basis\some_project\20200415_rank_wind_ratio\forecast\forecast\%s.2020%s12.csv' % (
                    areanumber[area], i),
                index_col=0, parse_dates=True, skiprows=1,
                names=['0', '100 风速', '20 风向', '20 风速'])
            data2 = data2.set_index(data2.index + pd.Timedelta(8, unit='H'))
            sdata = create_sdata_in_A(data1, data2, A)
            plot_data(sdata, legends=legendlabels, savename=i, title=title)
            document.add_picture('%s.png' % i, width=Inches(5))

    return '%s%s'%(title, legendlabels[0][3:])


# time_step = 24
# sys_err = False
# level = 'more'
# compare_data_name = '100 风速'
# main([0,0,'100 风速'], 'more', 24, False)
time_step=1
for sys_err in [False, True]:
    if sys_err is False:
        sysss = '不去系统误差'
    else:
        sysss = '去系统误差'
    variables = ['100 风速', '20 风速']
    names = ['瑞安', '苍南']
    for i in range(1):
        for j in range(3):
            for sj in variables:
                document = Document()
                document.styles['Normal'].font.name = '宋体'
                document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

                compare_data_name = sj
                choose = [i, j, sj]
                savename = main(choose, document, sys_err)

                document.save('%s%s.docx' % (savename, sysss))
    for sj in variables:
        document = Document()
        document.styles['Normal'].font.name = '宋体'
        document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')
        compare_data_name = sj
        choose = [1, 0, sj]
        savename = main(choose,document, sys_err)
        document.save('%s%s.docx' % (savename, sysss))



