import pandas as pd
import numpy as np

'''从4个txt中抽取数据（2019年12月-2020年11月），写成csv的文件。样例发给你了。
csv文件中每个月有418个要素值：分别是M_Atm_Nc、M_Oce_Er、M_Ext、-999、-999、HadISST'''
vari_name = ['M_Atm_Nc', 'M_Oce_Er', 'M_Ext',    '999',         'HadISST']
begin_num = [0,                  88,             88+26,  88+26+16,   88+26+16+2] # 114, 130, 132



def call_999():
    a = pd.read_csv(r'M_Atm_Nc.txt', delim_whitespace=True, index_col=0)
    data = pd.DataFrame(index=a.index)
    data['1'] = np.ones(data.shape[0])*-999
    data['2'] = np.ones(data.shape[0])*-999
    return data

result = pd.DataFrame()
for i, vari in enumerate(vari_name):
    if vari != '999':
        a = pd.read_csv('%s.txt'%vari, delim_whitespace=True, index_col=0)
    else:
        a = call_999()
    a = a.set_index(pd.to_datetime(a.index, format='%Y%m'))
    a = a[a.index[0]:a.index[-1]].T
    a = a.set_index(np.array(a.index).astype(int)+begin_num[i])
    a = a.stack().reset_index()
    result = pd.concat((result, a))

result = result.sort_values(by=['level_1', 'level_0'])
result = result.set_index(pd.to_datetime(result['level_1']))
result = result.sort_index()
result['YR'] = result.index.year
result['MNTH'] = result.index.month
result = result.drop('level_1', axis=1)
print(result)
result.columns = ['CCFI', 'CCFIVAL','YR', 'MNTH']
result = result[['YR', 'MNTH', 'CCFI', 'CCFIVAL']]
result['CCFIVAL'] = np.round(np.array(result['CCFIVAL']), 3)
print(result)
result.to_csv('combine_result.csv', index=None)