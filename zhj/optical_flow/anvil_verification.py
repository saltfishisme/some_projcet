import warnings
import cv2
import matplotlib.pyplot as plt
import numpy as np
from pysteps import motion, utils
from pysteps.utils import conversion, dimension, transformation
from pysteps.nowcasts import anvil
from pysteps.nowcasts import anvil, extrapolation, sprog
import bz2
from configparser import ConfigParser
import logging.handlers
import struct
import sys
import os
import traceback
from scipy.ndimage import gaussian_filter

def optical_flow(data_addr,  result_name, header, accutime,
                 zr_a=200, zr_b=1.4, aggre_level=4):

    def get_radar_data(header, offset, scale):
        # skip to data position
        def get_data(data_addr, header):
            f = bz2.BZ2File(data_addr, 'rb')
            f.seek(256)
            data_header = [('data', '<i2', (header['rows'][0], header['cols'][0]))]
            data = np.frombuffer(f.read(), data_header)
            f.close()
            return data['data'][0]

        data = []
        for iaddr in data_addr:
            data.append(get_data(iaddr, header))
        data = np.array(data)
        # reconstruction
        data[np.where(data==1)] = 0
        data[np.where(data>1)] = (data[np.where(data>1)]-offset)/scale

        return data
    def get_metadata(header):
        metadata = {'accutime': accutime,
                    'projection': '+proj=longlat  +datum=WGS84 no_defs +ellps=WGS84 +towgs84=0,0,0',
                    'threshold': 15,
                    # 'timestamps': date_intime,
                    'transform': None,
                    'unit': 'dBZ',
                    'yorigin': 'upper',
                    'zerovalue': 0.0,
                    'x1': header['wlon'][0]/10000,
                    'x2': header['elon'][0]/10000,
                    'y1': header['slat'][0]/10000,
                    'y2': header['nlat'][0]/10000,
                    'ypixelsize': 1,
                    'xpixelsize': 1,
                    'zr_a': zr_a,
                    'zr_b': zr_b}
        return metadata


    ###################################prepare data ####################################

    offset  = header['offset']
    scale = header['scale']
    rainrate_field = get_radar_data(header, offset, scale)
    metadata = get_metadata(header)


    rainrate_field_shape = rainrate_field.shape[1:3]
    metadata_row = metadata.copy()
    # mask


    ##################################    upscale ########################################

    def aggregate_fields_space(rainrate_field, metadata):
        def selected_radar_area(rainrate_field, metadata):
            rainrate_field_row = rainrate_field.copy()

            lat_U = 0
            lat_L = rainrate_field.shape[1]
            lon_L = 0
            lon_R = rainrate_field.shape[2]

            lat = np.arange(metadata['y1'], metadata['y2']+metadata['ypixelsize'], metadata['ypixelsize'])
            lon = np.arange(metadata['x1'], metadata['x2']+metadata['xpixelsize'], metadata['xpixelsize'])

            # create for upscale
            lat_range = (lat_L-lat_U)%aggre_level; lon_range = (lon_R-lon_L)%aggre_level

            lat_L = lat_L-lat_range; lon_R = lon_R - lon_range
            return rainrate_field[:, lat_U:lat_L, lon_L:lon_R], metadata, [lat_L, lat_U, lon_R, lon_L]


        rainrate_field, metadata, selected_area = selected_radar_area(rainrate_field, metadata)

        rainrate_field_no_level = rainrate_field.copy()
        metadata_no_level = metadata.copy()


        rainrate_field, metadata = dimension.aggregate_fields_space(rainrate_field, metadata, aggre_level)
        def mapping_to_gray(z, Zc=33, param=4):
            '''
          :param z: the reflectivity factor in 0-60 dBZ
          :param Zc: the parameter Zc specifies the point of inflection and is
                            chosen in ROVER to be 33 dBZ, which is currently adopted as
                            the threshold for identification of significant convection in
                            aviation forecasting in Hong Kong.
          :param param: controls the sharpness of the inflection and the gray levels on
                                    both sides of the inflection point. set to 1, 4 and 16 dBZ
          :return: converted non-linearly to a gray scale of 0-255
          '''
            import math
            import numpy as np
            def mapping(z):
                return np.arctan((z-Zc)/param)
            z[z>60] = 60
            arr = mapping(np.arange(0,61))
            z = mapping(z)
            img = ((z - arr.min()) * (1/(arr.max() - arr.min()) * 255)).astype('uint8')
            return img
        rainrate_field = mapping_to_gray(rainrate_field)

        rainrate_field_no_level, metadata_no_level = dimension.aggregate_fields_space(rainrate_field_no_level, metadata_no_level, aggre_level)
        return   rainrate_field, metadata, selected_area, rainrate_field_no_level, metadata_no_level

    aggregate_fields = False
    if (rainrate_field_shape[0]>2000) | (rainrate_field_shape[1]>2000):
        rainrate_field, metadata, selected_area, rainrate_field_no_level, metadata_no_level  = aggregate_fields_space(rainrate_field, metadata)
        aggregate_fields = True



    ################################### optical flow ##########################################

    oflow = motion.get_method("lucaskanade")

    # transform the input data to logarithmic scale
    # rainrate_field_log, _ = utils.transformation.dB_transform(
    #     rainrate_field, metadata=metadata
    # )
    velocity = oflow(rainrate_field[-3:,:,:])
    velocity[0] = gaussian_filter(velocity[0], sigma=1)
    velocity[1] = gaussian_filter(velocity[1], sigma=1)

    ##################################### start forecast ###################################

    if aggregate_fields:
        rainrate_field = rainrate_field_no_level

    # vil = anvil.forecast(
    #     rainrate_field[-3:], velocity, int(int(forecast_time)/int(accutime)), ar_order=1
    # )

    vil = extrapolation.forecast(
        rainrate_field[-1], velocity, int(int(forecast_time)/int(accutime)), extrap_kwargs={"allow_nonfinite_values": True}
    )
    vil[vil < 0.1] = 0.0

    # rainrate_field_db, _ = transformation.dB_transform(
    #     rainrate_field, metadata, threshold=0.1, zerovalue=-15.0
    # )
    # rainrate_thr, _ = transformation.dB_transform(
    #     np.array([0.1]), metadata, threshold=0.1, zerovalue=-15.0
    # )
    # forecast_sprog = sprog.forecast(
    #     rainrate_field_db[-3:], velocity, int(int(forecast_time)/int(accutime)), n_cascade_levels=8, R_thr=rainrate_thr[0]
    # )
    # vil, _ = transformation.dB_transform(
    #     forecast_sprog, threshold=-10.0, inverse=True
    # )
    # vil[vil < 0.1] = 0.0
    ###################################    return to row shape ################################

    if aggregate_fields:
        def trans_into_row_shape(data, selected_area):
            from scipy.interpolate import RectBivariateSpline
            lat_L, lat_U, lon_R, lon_L = selected_area
            olon = np.arange(lon_L, lon_R)
            olat = np.arange(lat_U, lat_L)
            import matplotlib.pyplot as plt
            # # 插值处理

            data[np.isnan(data)] = 0
            lon = np.arange(lon_L, lon_R)[::aggre_level]
            lat = np.arange(lat_U, lat_L)[::aggre_level]

            from scipy import interpolate
            func = interpolate.RectBivariateSpline(lat, lon, data)
            data = func(olat, olon)

            lat_L, lat_U, lon_R, lon_L = selected_area
            data_row = np.zeros([rainrate_field_shape[0], rainrate_field_shape[1]])
            data_row[lat_U:lat_L, lon_L:lon_R] = data
            data_row[np.isnan(data_row)] = 0
            data_row[np.where(data_row<0)] = 0
            return data_row

        rainrate_field_row = np.zeros([vil.shape[0], rainrate_field_shape[0], rainrate_field_shape[1]])
        for ssi in range(vil.shape[0]):
            rainrate_field_row[ssi] = trans_into_row_shape(vil[ssi], selected_area)

    else:
        rainrate_field_row = vil

    rainrate_field = rainrate_field_row
    #################################    return to dbz ########################################
    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver
    for i in range(rainrate_field.shape[0]):
        # plt.subplots_adjust()
        data = rainrate_field[i]

        # data = rainrate_field[i][800:2800,2800:4800]
        plot_precip_field(data, units='dBZ', colorbar=False)
        # plot_precip_field(rainrate_field[-1], units='dBZ')
        ax = plt.gca()
        ax.set_xticks(np.arange(data.shape[1])[::100])
        ax.set_yticks(np.arange(data.shape[0])[::100])
        ax.set_xticklabels(np.arange(data.shape[1])[::100])
        ax.set_yticklabels(np.arange(data.shape[0])[::100][::-1])
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
        ax.grid()
        plt.show()


    ####################################   save file #########################################

    def save_to_bin(mydata, i):
        f = bz2.BZ2File(data_addr[0], 'rb')
        ##################################################################
        header = f.read(256)
        f.close()
        # mask nan and inf

        # return to
        mydata = (mydata*scale+offset).flatten()
        mydata[np.isnan(mydata)] = 0
        mydata[np.where(mydata==float('-inf'))] = 0
        mydata[np.where(mydata==float('inf'))] = 0
        mydata = mydata.astype(int)

        myfmt='h'*len(mydata)
        bin=struct.pack(myfmt,*mydata)
        all = header+bin

        name_file = data_addr[-1].split('\\')[-1]
        name_file = name_file.split('.LatLon.bz2')
        name_file = data_addr[-1].split('\\')[-1]
        name_file = name_file.split('.LatLon.bz2')[0]

        stepss = str((i+1)*int(accutime))
        if len(stepss) < 3:
            stepss='0'+stepss
        sname_file = '%s_%s.LatLon.bz2'%(name_file, stepss)
        with bz2.open(result_name+sname_file, "wb") as f:
            # Write compressed data to file
            unused = f.write(all)
    for f_i in range(rainrate_field.shape[0]):
        save_to_bin(rainrate_field[f_i], f_i)
    return



conf = ConfigParser()

# data1= sys.argv[1]
# data2=sys.argv[2]
# data3=sys.argv[3]
# accutime=sys.argv[4]
# forecast_time = sys.argv[5]
# result_name=sys.argv[6]
# logfile_addr=sys.argv[7]

data1= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\data_now\SL_SSQJ_20211003_2000_HYBR.LatLon.bz2'
data2= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\data_now\SL_SSQJ_20211003_2010_HYBR.LatLon.bz2'
data3= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\data_now\SL_SSQJ_20211003_2020_HYBR.LatLon.bz2'
accutime=r'10'
forecast_time = '120'
result_name = r'D:\\test1_line_gauss\\'
logfile_addr= r'D:\\test1_line_gauss\\'

os.makedirs(result_name, exist_ok=True)
logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)

f_handler = logging.FileHandler('%s/OF_log.log'%logfile_addr, mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))

logger.addHandler(f_handler)

try:
    def get_header():
        f = bz2.BZ2File(data1, 'rb')
        section1 = [('VolumeLabel', 'S4'),('VersionNo', 'S4'),('FileLength', '<u4')]
        # header = np.frombuffer(f.read(12), section1)

        header_param_r = np.array([['i4', 'slat'],
                                   ['i4', 'wlon'],
                                   ['i4', 'nlat'],
                                   ['i4', 'elon'],
                                   ['i4', 'rows'],
                                   ['i4', 'cols'],
                                   ['i4', 'dlat'],
                                   ['i4', 'dlon'],
                                   ['i4', 'calt'],
                                   ['S8', 'varCode'],
                                   ['S8', 'varUnit'],
                                   ['S32', 'varName'],
                                   ['<u2', 'varID'],
                                   ['<i2', 'mode'],
                                   ['<i2', 'range'],
                                   ['<i2', 'scale'],
                                   ['<i2', 'offset'],
                                   ['<i2', 'clear'],
                                   ['<i2', 'missing'],
                                   ['<i2', 'minCode'],
                                   ['i4', 'span'],
                                   ['<i2', 'sYear'],
                                   ['<i2', 'sMonth'],
                                   ['<i2', 'sDay'],
                                   ['<i2', 'sHour'],
                                   ['<i2', 'sMinute'],
                                   ['<i2', 'sSecond'],
                                   ['<i2', 'eYear'],
                                   ['<i2', 'eMonth'],
                                   ['<i2', 'eDay'],
                                   ['<i2', 'eHour'],
                                   ['<i2', 'eMinute'],
                                   ['<i2', 'eSecond'],
                                   ['S8', 'rgnID'],
                                   ['S52', 'rgnName'],
                                   ['S20', 'country'],
                                   ['a36', 'reserve']], dtype='<U7')
        header_param = []
        for i in range(header_param_r.shape[0]):
            header_param.append((header_param_r[i][1], header_param_r[i][0]))

        # skip section1 file information
        f.seek(12)
        header = np.frombuffer(f.read(244), header_param)
        f.close()
        return header


    header = get_header()

    data_addr = [data1, data2, data3]

    optical_flow(data_addr,  result_name, header, accutime)
except Exception as e:
    logger.error(traceback.format_exc())






