import warnings

import matplotlib.pyplot as plt
import numpy as np
from pysteps import motion, utils
from pysteps.utils import conversion, dimension, transformation
from pysteps.nowcasts import anvil
import bz2
from configparser import ConfigParser
import logging.handlers
import struct
import sys
import os
import traceback
import xarray as xr
def optical_flow(data_addr, accutime,
                 zr_a=200, zr_b=1.4, aggre_level=4):

    def get_radar_data():

        data = []
        for iaddr in data_addr:
            print(xr.open_dataset(iaddr).alt)
            data.append(xr.open_dataset(iaddr).dbz.values.astype('float64'))
        data = np.array(data)
        data[np.isnan(data)] = 0
        print(data)
        return data

    def get_metadata():
        metadata = {'accutime': accutime,
                    'projection': '+proj=longlat  +datum=WGS84 no_defs +ellps=WGS84 +towgs84=0,0,0',
                    'threshold': 10,
                    # 'timestamps': date_intime,
                    'transform': None,
                    'unit': 'dBZ',
                    'yorigin': 'upper',
                    'zerovalue': 0.0,
                    'x1': 10,
                    'x2': 15,
                    'y1': 10,
                    'y2': 15,
                    'ypixelsize': 1,
                    'xpixelsize': 1,
                    'zr_a': zr_a,
                    'zr_b': zr_b}
        return metadata


    ###################################prepare data ####################################


    rainrate_field = get_radar_data()
    metadata = get_metadata()

    rainrate_field_shape = rainrate_field.shape[1:3]
    metadata_row = metadata.copy()

    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver
    plot_precip_field(rainrate_field[1], units='dBZ')
    ax = plt.gca()
    ax.set_xticks(np.arange(rainrate_field.shape[1])[::200])
    ax.set_yticks(np.arange(rainrate_field.shape[1])[::200])
    ax.set_xticklabels(np.arange(rainrate_field.shape[1])[::200])
    ax.set_yticklabels(np.arange(rainrate_field.shape[1])[::200])
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.grid()
    plt.show()


    ##################################    upscale ########################################

    rainrate_field, metadata = utils.to_rainrate(rainrate_field, metadata)


    ################################### optical flow ##########################################

    fd_kwargs = {}
    fd_kwargs["max_corners"] = 1000
    fd_kwargs["quality_level"] = 0.01
    fd_kwargs["min_distance"] = 2
    fd_kwargs["block_size"] = 8

    lk_kwargs = {}
    lk_kwargs["winsize"] = (15, 15)

    oflow_kwargs = {}
    oflow_kwargs["fd_kwargs"] = fd_kwargs
    oflow_kwargs["lk_kwargs"] = lk_kwargs
    oflow_kwargs["decl_scale"] = 10

    oflow = motion.get_method("lucaskanade")

    # transform the input data to logarithmic scale
    rainrate_field_log, _ = utils.transformation.dB_transform(
        rainrate_field, metadata=metadata
    )
    velocity = oflow(rainrate_field_log, **oflow_kwargs)

    print(velocity)
    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver
    plot_precip_field(rainrate_field[-1], title="LK")
    quiver(velocity, step=20)
    plt.show()


    ##################################### start forecast ###################################


    from pysteps.nowcasts import anvil, extrapolation, sprog
    # vil = extrapolation.forecast(
    #     rainrate_field[-1], velocity, int(int(forecast_time)/int(accutime)), extrap_kwargs={"allow_nonfinite_values": True}
    # )
    vil = anvil.forecast(
        rainrate_field[-3:], velocity, int(int(forecast_time)/int(accutime)), ar_order=1
    )


    ###################################    return to row shape ################################

    rainrate_field_row = vil



    #################################    return to dbz ########################################

    rainrate_field = zr_a * rainrate_field_row ** zr_b

    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver
    plot_precip_field(rainrate_field[-1], units='dBZ')
    ax = plt.gca()
    print(ax.get_yticks())
    ax.set_xticks(np.arange(rainrate_field.shape[1])[::200])
    ax.set_yticks(np.arange(rainrate_field.shape[1])[::200])
    ax.set_xticklabels(np.arange(rainrate_field.shape[1])[::200])
    ax.set_yticklabels(np.arange(rainrate_field.shape[1])[::200])
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.grid()
    plt.show()

    return



forecast_time = 120
accutime = 6

import os

use_path = r'D:\OneDrive\basis\some_projects\zhj\optical_flow\Z9835\20180709\\'
data_all = os.listdir(use_path)



# for i in data_all:
#     print(i)
#     data = xr.open_dataset(use_path+i).dbz.values.astype('float64')
#     cb  = plt.imshow(data)
#     plt.colorbar(cb)
#     plt.show()
data_addr = [use_path+data_all[0],
             use_path+data_all[1],
             use_path+data_all[2]]
optical_flow(data_addr, accutime)





