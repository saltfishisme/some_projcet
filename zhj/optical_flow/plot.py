import os
import  xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as scio

import nc4
import cv2
a = xr.open_dataset(r'D:\OneDrive\a_matlab\China_0.5\a.nc')
print(a)
# scio.savemat('China_mask.mat', {'China_mask':np.array(a['p72.162'].values, dtype='double')})
# plt.imshow(a['p72.162'].values)

ssa = a['tp'].values
a['tp'].values = cv2.dilate(ssa, np.ones((3, 3), dtype=np.uint8),1)
a.to_netcdf(r'D:\OneDrive\a_matlab\China_0.5\b.nc')
exit(0)
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from datetime import timedelta
# date_all = pd.date_range('20210711 02:40', freq='10T', periods=12).strftime('%Y%m%d_%H%M')
#
#
# def plot(path_img, titles):
#     for s_i, ipath_img in enumerate(path_img):
#         fig, ax = plt.subplots(1,1)
#         img = mpimg.imread(ipath_img)[1000:3000,2800:4800]
#         imgplot = plt.imshow(img)
#         ax = plt.gca()
#         ax.set_xticks(np.arange(img.shape[1])[::100])
#         ax.set_yticks(np.arange(img.shape[0])[::100])
#         ax.set_xticklabels(np.arange(img.shape[1])[::100])
#         ax.set_yticklabels(np.arange(img.shape[0])[::100])
#         plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
#                  rotation_mode="anchor")
#         plt.grid()
#         ax.set_title(titles[s_i])
#         # plt.show()
#         plt.savefig(titles[s_i]+'_row.png', dpi=600)
#         plt.close()
#
# date_all = pd.date_range('20210711 02:40', freq='10T', periods=12).strftime('%Y%m%d_%H%M')
#
# data_addr = []
# titles = []
# for i in range(10,130,10):
#     titles.append(f'+{i:03d}minutes')
#     data_addr.append(rf'D:\OneDrive\basis\some_projects\zhj\optical_flow\old_data\china\QPF\SL_CHINA_20210711_0230_CALR_{i:03d}.png')
# plot(data_addr, titles)
#
# data_addr = []
# titles = []
# for i in range(12):
#     titles.append(f'OBS +{(i+1)*10:03d}minutes')
#     data_addr.append(r'D:\OneDrive\basis\some_projects\zhj\optical_flow\old_data\china\HYBR_PNG\SL_CHINA_%s_HYBR.png'%date_all[i])
# plot(data_addr, titles)
#
# exit(0)


import numpy as np
import bz2
import pandas as pd
import cartopy.crs as ccrs

def plot_dBZ(datas):
    # colors = ['#63A0F5', '#90EAEC', '#99FB27', '#77C51C', '#548E11', '#FEFD24', '#DABF17', '#E39918',
    #           '#D52300', '#B31C00', '#A01800', '#D730EE', '#641383', '#A693EF' ]
    colors = ['#63A0F5', '#90EAEC', '#77C51C', '#548E11', '#FEFD24', '#DABF17', '#E39918',
              '#D52300', '#B31C00', '#A01800', '#D730EE', '#641383', '#A693EF' ]
    levels = np.arange(5,71,5)
    import  matplotlib.pyplot as plt
    # fig, axs = plt.subplots(2,2, subplot_kw={'projection':ccrs.PlateCarree()}, figsize=[4.2,4])

    fig, axs = plt.subplots(2,2, subplot_kw={'projection':ccrs.AlbersEqualArea(central_longitude=105)}, figsize=[4.2,4])
    plt.subplots_adjust(top=0.945,
                        bottom=0.025,
                        left=0.02,
                        right=0.99,
                        hspace=0.2,
                        wspace=0.2)
    labels = ['a) +30 minutes', 'b) +60 minutes','c) +90 minutes','d) +120 minutes',]
    for i in range(2):
        for j in range(2):
            ax = axs[i][j]
            data = datas[i*2+j]
            print(data.shape)
            im = ax.contourf(np.arange(72.98, 135, 0.01), np.arange(54.01, 16.99,-0.01),
                             data, levels,
                             colors=colors,transform=ccrs.PlateCarree()
                             )
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader

            shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.4)
            ax.add_feature(shape_feature)
            shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.4)
            ax.add_feature(shape_feature)

            ax.set_title(labels[i*2+j], fontsize=9, loc='left')
            # ax.coastlines()
    # plt.colorbar(im)
    # plt.axis('off')
    plt.show()
    plt.savefig('1_obs.png', dpi=800)
    exit(0)



def optical_flow(data_addr,  result_name, header, accutime,
                 zr_a=200, zr_b=1.4, aggre_level=4):

    def get_radar_data(header, offset, scale):
        # skip to data position
        def get_data(data_addr, header):
            f = bz2.BZ2File(data_addr, 'rb')
            f.seek(256)
            data_header = [('data', '<i2', (header['rows'][0], header['cols'][0]))]
            data = np.frombuffer(f.read(), data_header)
            f.close()
            return data['data'][0]

        data = []
        print()
        for iaddr in data_addr:
            data.append(get_data(iaddr, header))
        data = np.array(data)
        # reconstruction
        data[np.where(data==1)] = 0
        data[np.where(data>1)] = (data[np.where(data>1)]-offset)/scale

        return data

    def get_metadata(header):
        metadata = {'accutime': accutime,
                    'projection': '+proj=longlat  +datum=WGS84 no_defs +ellps=WGS84 +towgs84=0,0,0',
                    'threshold': 10,
                    # 'timestamps': date_intime,
                    'transform': None,
                    'unit': 'dBZ',
                    'yorigin': 'upper',
                    'zerovalue': 0.0,
                    'x1': header['wlon'][0]/10000,
                    'x2': header['elon'][0]/10000,
                    'y1': header['slat'][0]/10000,
                    'y2': header['nlat'][0]/10000,
                    'ypixelsize': 1,
                    'xpixelsize': 1,
                    'zr_a': zr_a,
                    'zr_b': zr_b}
        return metadata


    ###################################prepare data ####################################

    offset  = header['offset']
    scale = header['scale']
    rainrate_field = get_radar_data(header, offset, scale)
    metadata = get_metadata(header)

    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver
    for i in range(rainrate_field.shape[0]):
        # plt.subplots_adjust()
        data = rainrate_field[i]

        # data = rainrate_field[i][800:2800,2800:4800]
        plot_precip_field(data, units='dBZ', colorbar=False)
        # plot_precip_field(rainrate_field[-1], units='dBZ')
        ax = plt.gca()
        ax.set_xticks(np.arange(data.shape[1])[::100])
        ax.set_yticks(np.arange(data.shape[0])[::100])
        ax.set_xticklabels(np.arange(data.shape[1])[::100])
        ax.set_yticklabels(np.arange(data.shape[0])[::100][::-1])
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
        ax.grid()
        plt.title(titles[i])
        # plt.show()
        # plt.show()
        plt.savefig(titles[i]+'.png', dpi=300)
        plt.close()
    # mask
    exit(0)


    return



accutime=r'10'
forecast_time = '40'
result_name = r'D:\\'
logfile_addr= r'D:\\'

from datetime import timedelta


# date_all = pd.date_range('20211004 01:30', freq='10T', periods=12).strftime('%Y%m%d_%H%M')
#
# data_addr = []
# titles = []
# for i in range(10,130,10):
#     titles.append(f'+{i:03d}minutes')
#     data_addr.append(rf'D:\new\SL_SSQJ_20211004_0120_HYBR_{i:03d}.LatLon.bz2')

# data_addr = []
# titles = []
# for i in range(12):
#     titles.append(f'OBS +{(i+1)*10:03d}minutes')
#     data_addr.append(r'D:\OneDrive\basis\some_projects\zhj\optical_flow\old_data\radar_high_r\4\\SL_SSQJ_%s_HYBR.LatLon.bz2'%date_all[i])


short_path = r'D:\\new111\\'
def list_full_paths(directory):
    result = []
    for file in os.listdir(directory):
        if file[-3:] == 'bz2':
            result.append(os.path.join(directory, file))
    return result
data_addr = list_full_paths(short_path)
titles = data_addr
print(data_addr)

def get_header():
    f = bz2.BZ2File(data_addr[0], 'rb')
    section1 = [('VolumeLabel', 'S4'),('VersionNo', 'S4'),('FileLength', '<u4')]
    # header = np.frombuffer(f.read(12), section1)

    header_param_r = np.array([['i4', 'slat'],
                               ['i4', 'wlon'],
                               ['i4', 'nlat'],
                               ['i4', 'elon'],
                               ['i4', 'rows'],
                               ['i4', 'cols'],
                               ['i4', 'dlat'],
                               ['i4', 'dlon'],
                               ['i4', 'calt'],
                               ['S8', 'varCode'],
                               ['S8', 'varUnit'],
                               ['S32', 'varName'],
                               ['<u2', 'varID'],
                               ['<i2', 'mode'],
                               ['<i2', 'range'],
                               ['<i2', 'scale'],
                               ['<i2', 'offset'],
                               ['<i2', 'clear'],
                               ['<i2', 'missing'],
                               ['<i2', 'minCode'],
                               ['i4', 'span'],
                               ['<i2', 'sYear'],
                               ['<i2', 'sMonth'],
                               ['<i2', 'sDay'],
                               ['<i2', 'sHour'],
                               ['<i2', 'sMinute'],
                               ['<i2', 'sSecond'],
                               ['<i2', 'eYear'],
                               ['<i2', 'eMonth'],
                               ['<i2', 'eDay'],
                               ['<i2', 'eHour'],
                               ['<i2', 'eMinute'],
                               ['<i2', 'eSecond'],
                               ['S8', 'rgnID'],
                               ['S52', 'rgnName'],
                               ['S20', 'country'],
                               ['a36', 'reserve']], dtype='<U7')
    header_param = []
    for i in range(header_param_r.shape[0]):
        header_param.append((header_param_r[i][1], header_param_r[i][0]))

    # skip section1 file information
    f.seek(12)
    header = np.frombuffer(f.read(244), header_param)
    f.close()
    return header

header = get_header()
optical_flow(data_addr,  result_name, header, accutime)




