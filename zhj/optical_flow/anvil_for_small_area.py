import warnings
import numpy as np
from pysteps import motion, utils
from pysteps.utils import conversion, dimension, transformation
from pysteps.nowcasts import anvil
import bz2
from configparser import ConfigParser
import logging.handlers
import struct
import sys
import os
import traceback
def optical_flow(data_addr,  result_name, header, accutime,
                 zr_a=200, zr_b=1.4, aggre_level=1):

    def get_radar_data(header, offset, scale):
        # skip to data position
        def get_data(data_addr, header):
            f = bz2.BZ2File(data_addr, 'rb')
            f.seek(256)
            data_header = [('data', '<i2', (header['rows'][0], header['cols'][0]))]
            data = np.frombuffer(f.read(), data_header)
            f.close()
            return data['data'][0]

        data = []
        for iaddr in data_addr:
            data.append(get_data(iaddr, header))
        data = np.array(data)
        # reconstruction
        data[np.where(data==1)] = 0
        data[np.where(data>1)] = (data[np.where(data>1)]-offset)/scale

        return data
    def get_metadata(header):
        metadata = {'accutime': accutime,
                    'projection': '+proj=longlat  +datum=WGS84 no_defs +ellps=WGS84 +towgs84=0,0,0',
                    'threshold': 10,
                    # 'timestamps': date_intime,
                    'transform': None,
                    'unit': 'dBZ',
                    'yorigin': 'upper',
                    'zerovalue': 0.0,
                    'x1': header['wlon'][0]/10000,
                    'x2': header['elon'][0]/10000,
                    'y1': header['slat'][0]/10000,
                    'y2': header['nlat'][0]/10000,
                    'ypixelsize': 1,
                    'xpixelsize': 1,
                    'zr_a': zr_a,
                    'zr_b': zr_b}
        return metadata


    ###################################prepare data ####################################

    offset  = header['offset']
    scale = header['scale']
    rainrate_field = get_radar_data(header, offset, scale)[:, 740:2501, 1500:3701]
    metadata = get_metadata(header)

    rainrate_field_shape = rainrate_field.shape[1:3]
    metadata_row = metadata.copy()
    # mask

    # rainrate_field[np.where(rainrate_field<25)] = 0

    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver


    # plot_precip_field(rainrate_field[-1], units='dBZ')
    # ax = plt.gca()
    # ax.set_xticks(np.arange(rainrate_field.shape[2])[::200])
    # ax.set_yticks(np.arange(rainrate_field.shape[1])[::200])
    # ax.set_xticklabels(np.arange(rainrate_field.shape[2])[::200])
    # ax.set_yticklabels(np.arange(rainrate_field.shape[1])[::200][::-1])
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    # plt.grid()
    # plt.show()


    ##################################    upscale ########################################

    def aggregate_fields_space(rainrate_field, metadata):
        def selected_radar_area(rainrate_field, metadata):
            rainrate_field_row = rainrate_field.copy()
            import cv2
            from skimage import measure
            for ir, irain in enumerate(rainrate_field):
                rainrate_field[ir]  = cv2.GaussianBlur(irain, (15,15), 0)

                # thresh = cv2.threshold(blurred, 5, 255, cv2.THRESH_BINARY)[1]
                #
                # # first to contact the connective area
                # labels = measure.label(thresh, connectivity=2, background=0)
                # mask = np.zeros(thresh.shape, dtype="uint8")
                # for label in np.unique(labels):
                #     if label == 0:
                #         continue
                #     labelMask = np.zeros(thresh.shape, dtype="uint8")
                #     labelMask[labels == label] = 255
                #     numPixels = cv2.countNonZero(labelMask)
                #
                #     if numPixels > 500:
                #         mask = cv2.add(mask, labelMask)
                #     rainrate_field[ir] = mask

            lat_U = 0
            lat_L = rainrate_field.shape[1]
            lon_L = 0
            lon_R = rainrate_field.shape[2]

            lat = np.arange(metadata['y1'], metadata['y2']+metadata['ypixelsize'], metadata['ypixelsize'])
            lon = np.arange(metadata['x1'], metadata['x2']+metadata['xpixelsize'], metadata['xpixelsize'])

            # create for upscale
            lat_range = (lat_L-lat_U)%aggre_level; lon_range = (lon_R-lon_L)%aggre_level

            lat_L = lat_L-lat_range; lon_R = lon_R - lon_range
            return rainrate_field[:, lat_U:lat_L, lon_L:lon_R], metadata, [lat_L, lat_U, lon_R, lon_L]


        rainrate_field, metadata, selected_area = selected_radar_area(rainrate_field, metadata)

        rainrate_field_no_level = rainrate_field.copy()
        metadata_no_level = metadata.copy()

        # rainrate_field[(rainrate_field<10) ]=0
        # rainrate_field[(rainrate_field>0) & (rainrate_field<=15)]=7.5
        # rainrate_field[(rainrate_field>=15) & (rainrate_field<=30)]=22.5
        # rainrate_field[(rainrate_field>=30) & (rainrate_field<=45)]=37.5
        # rainrate_field[(rainrate_field>=45) & (rainrate_field<=60)]=52.5
        # rainrate_field[(rainrate_field>=60) ]=67.5

        rainrate_field, metadata = utils.to_rainrate(rainrate_field, metadata)
        rainrate_field, metadata = dimension.aggregate_fields_space(rainrate_field, metadata, aggre_level)

        rainrate_field_no_level, metadata_no_level = utils.to_rainrate(rainrate_field_no_level, metadata_no_level)
        rainrate_field_no_level, metadata_no_level = dimension.aggregate_fields_space(rainrate_field_no_level, metadata_no_level, aggre_level)
        return   rainrate_field, metadata, selected_area, rainrate_field_no_level, metadata_no_level

    aggregate_fields = False
    if (rainrate_field_shape[0]>2000) | (rainrate_field_shape[1]>2000):
        rainrate_field, metadata, selected_area, rainrate_field_no_level, metadata_no_level  = aggregate_fields_space(rainrate_field, metadata)
        aggregate_fields = True
    if aggregate_fields is False:
        rainrate_field, metadata = utils.to_rainrate(rainrate_field, metadata)


    ################################### optical flow ##########################################

    # oflow = motion.get_method("proesmans")
    #
    # # transform the input data to logarithmic scale
    # rainrate_field_log, _ = utils.transformation.dB_transform(
    #     rainrate_field, metadata=metadata, threshold=0.1
    # )
    # velocity = oflow(rainrate_field_log[-2:,:,:])

    fd_kwargs = {}
    fd_kwargs["max_corners"] = 1000
    fd_kwargs["quality_level"] = 0.01
    fd_kwargs["min_distance"] = 2
    fd_kwargs["block_size"] = 8

    lk_kwargs = {}
    lk_kwargs["winsize"] = (15, 15)

    oflow_kwargs = {}
    oflow_kwargs["fd_kwargs"] = fd_kwargs
    oflow_kwargs["lk_kwargs"] = lk_kwargs
    oflow_kwargs["decl_scale"] = 10

    oflow = motion.get_method("lucaskanade")

    # transform the input data to logarithmic scale
    rainrate_field_log, _ = utils.transformation.dB_transform(
        rainrate_field, metadata=metadata
    )
    velocity = oflow(rainrate_field_log, **oflow_kwargs)

    # Plot the motion field on top of the reference frame
    plot_precip_field(rainrate_field[-1])
    quiver(velocity, step=25)
    plt.show()

    ##################################### start forecast ###################################

    if aggregate_fields:
        rainrate_field = rainrate_field_no_level

    # import matplotlib.pyplot as plt
    # from pysteps.visualization import plot_precip_field, quiver
    # plot_precip_field(rainrate_field[-1], title="LK")
    # quiver(velocity, step=50)
    # plt.show()

    from pysteps.nowcasts import anvil, extrapolation, sprog
    # vil = extrapolation.forecast(
    #     rainrate_field[-1], velocity, int(int(forecast_time)/int(accutime)), extrap_kwargs={"allow_nonfinite_values": True}
    # )
    vil = anvil.forecast(
        rainrate_field[-3:], velocity, int(int(forecast_time)/int(accutime)), ar_order=1
    )


    ###################################    return to row shape ################################

    if aggregate_fields:
        def trans_into_row_shape(data, selected_area):
            from scipy.interpolate import RectBivariateSpline
            lat_L, lat_U, lon_R, lon_L = selected_area
            olon = np.arange(lon_L, lon_R)
            olat = np.arange(lat_U, lat_L)
            import matplotlib.pyplot as plt
            # # 插值处理

            data[np.isnan(data)] = 0
            lon = np.arange(lon_L, lon_R)[::aggre_level]
            lat = np.arange(lat_U, lat_L)[::aggre_level]

            from scipy import interpolate
            func = interpolate.RectBivariateSpline(lat, lon, data)
            data = func(olat, olon)

            lat_L, lat_U, lon_R, lon_L = selected_area
            data_row = np.zeros([rainrate_field_shape[0], rainrate_field_shape[1]])
            data_row[lat_U:lat_L, lon_L:lon_R] = data
            data_row[np.isnan(data_row)] = 0
            data_row[np.where(data_row<0)] = 0
            return data_row

        rainrate_field_row = np.zeros([vil.shape[0], rainrate_field_shape[0], rainrate_field_shape[1]])
        for ssi in range(vil.shape[0]):
            rainrate_field_row[ssi] = trans_into_row_shape(vil[ssi], selected_area)

    else:
        rainrate_field_row = vil



    #################################    return to dbz ########################################

    rainrate_field = zr_a * rainrate_field_row ** zr_b


    import matplotlib.pyplot as plt
    from pysteps.visualization import plot_precip_field, quiver
    plt.rcParams['font.sans-serif']=['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    fig, axs = plt.subplots(1,4, figsize=[8,2])
    plt.subplots_adjust(top=0.88,
                        bottom=0.11,
                        left=0.02,
                        right=0.99,
                        hspace=0.2,
                        wspace=0.2)
    colorbar = False
    labels = ['(a) 05:30', '(b) 05:40', '(c) 05:50', '(d) 06:00']

    rain_idx = [0,4,7,9]
    for i in range(4):
        plot_precip_field(rainrate_field[rain_idx[i]], units='dBZ', ax=axs[i], colorbar=colorbar)

        axs[i].set_title(labels[i], loc='left', fontsize=9)
    plt.show()


    # import matplotlib.pyplot as plt
    # from pysteps.visualization import plot_precip_field, quiver
    # plot_precip_field(rainrate_field[-1], units='dBZ')
    # ax = plt.gca()
    # ax.set_xticks(np.arange(rainrate_field.shape[2])[::200])
    # ax.set_yticks(np.arange(rainrate_field.shape[1])[::200])
    # ax.set_xticklabels(np.arange(rainrate_field.shape[2])[::200])
    # ax.set_yticklabels(np.arange(rainrate_field.shape[1])[::200][::-1])
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    # plt.grid()
    # plt.title('%s anvil +2h'%data_addr[-1][-20:-15])
    # plt.show()
    # plt.savefig(data_addr[-1][:-15]+'_re.png', dpi=600)
    # plt.close()


    ####################################   save file #########################################

    def save_to_bin(mydata, i):
        f = bz2.BZ2File(data_addr[0], 'rb')
        ##################################################################
        header = f.read(256)
        f.close()
        # mask nan and inf

        # return to
        mydata = (mydata*scale+offset).flatten()
        mydata[np.isnan(mydata)] = 0
        mydata[np.where(mydata==float('-inf'))] = 0
        mydata[np.where(mydata==float('inf'))] = 0
        mydata = mydata.astype(int)

        myfmt='h'*len(mydata)
        bin=struct.pack(myfmt,*mydata)
        all = header+bin

        name_file = data_addr[-1].split('\\')[-1]
        name_file = name_file.split('.LatLon.bz2')
        name_file = data_addr[-1].split('\\')[-1]
        name_file = name_file.split('.LatLon.bz2')[0]

        stepss = str((i+1)*int(accutime))
        if len(stepss) < 3:
            stepss='0'+stepss
        sname_file = '%s_%s.LatLon.bz2'%(name_file, stepss)
        with bz2.open(result_name+sname_file, "wb") as f:
            # Write compressed data to file
            unused = f.write(all)
    for f_i in range(rainrate_field.shape[0]):
        # for f_i in range(28,29):
        save_to_bin(rainrate_field[f_i], f_i)
    return



conf = ConfigParser()

# data1= sys.argv[1]
# data2=sys.argv[2]
# data3=sys.argv[3]
# accutime=sys.argv[4]
# forecast_time = sys.argv[5]
# result_name=sys.argv[6]
# logfile_addr=sys.argv[7]

data1= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\HYBR\SL_SSQJ_20211004_0516_HYBR.LatLon.bz2'
data2= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\HYBR\SL_SSQJ_20211004_0518_HYBR.LatLon.bz2'
data3= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\HYBR\SL_SSQJ_20211004_0520_HYBR.LatLon.bz2'
accutime=r'2'
forecast_time = '20'
result_name = r'D:\OneDrive\basis\some_projects\zhj\optical_flow\result\\'
logfile_addr= r'D:\OneDrive\basis\some_projects\zhj\optical_flow\result\\'

logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)

f_handler = logging.FileHandler('%s/OF_log.log'%logfile_addr, mode='w')
f_handler.setLevel(logging.DEBUG)
f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))

logger.addHandler(f_handler)


try:
    def get_header():
        f = bz2.BZ2File(data1, 'rb')
        section1 = [('VolumeLabel', 'S4'),('VersionNo', 'S4'),('FileLength', '<u4')]
        # header = np.frombuffer(f.read(12), section1)

        header_param_r = np.array([['i4', 'slat'],
                                   ['i4', 'wlon'],
                                   ['i4', 'nlat'],
                                   ['i4', 'elon'],
                                   ['i4', 'rows'],
                                   ['i4', 'cols'],
                                   ['i4', 'dlat'],
                                   ['i4', 'dlon'],
                                   ['i4', 'calt'],
                                   ['S8', 'varCode'],
                                   ['S8', 'varUnit'],
                                   ['S32', 'varName'],
                                   ['<u2', 'varID'],
                                   ['<i2', 'mode'],
                                   ['<i2', 'range'],
                                   ['<i2', 'scale'],
                                   ['<i2', 'offset'],
                                   ['<i2', 'clear'],
                                   ['<i2', 'missing'],
                                   ['<i2', 'minCode'],
                                   ['i4', 'span'],
                                   ['<i2', 'sYear'],
                                   ['<i2', 'sMonth'],
                                   ['<i2', 'sDay'],
                                   ['<i2', 'sHour'],
                                   ['<i2', 'sMinute'],
                                   ['<i2', 'sSecond'],
                                   ['<i2', 'eYear'],
                                   ['<i2', 'eMonth'],
                                   ['<i2', 'eDay'],
                                   ['<i2', 'eHour'],
                                   ['<i2', 'eMinute'],
                                   ['<i2', 'eSecond'],
                                   ['S8', 'rgnID'],
                                   ['S52', 'rgnName'],
                                   ['S20', 'country'],
                                   ['a36', 'reserve']], dtype='<U7')
        header_param = []
        for i in range(header_param_r.shape[0]):
            header_param.append((header_param_r[i][1], header_param_r[i][0]))

        # skip section1 file information
        f.seek(12)
        header = np.frombuffer(f.read(244), header_param)
        f.close()
        return header
    def create_all_data_addr():
        data_Path_r = sys.argv[0].split('/')[:-1]
        data_Path=''
        for i in data_Path_r:
            data_Path = data_Path+'/'+i
        makedir = data_Path[1:]+'/data'
        os.makedirs(makedir, exist_ok=True)

        from shutil import copy
        for i in [data1, data2, data3]:
            copy(i, makedir+'/')

        data_addr = [makedir]

        # def trans_file_Path_into_datetime(file_Path):
        #     name_file = file_Path.split("\\")[-1]
        #     time_loc= []
        #     time_end = ''
        #     time_yes = 0
        #     for si, i in enumerate(name_file):
        #         if i.isdigit():
        #             time_yes +=1
        #             if time_yes == 1:
        #                 time_loc.append(si)
        #                 time_end += i
        #             if (len(time_loc)>=1) & (time_yes !=1):
        #                 if si - time_loc[-1]<=2:
        #                     time_loc.append(si)
        #                     time_end += i
        #
        #     data_addr.append(name_file[0:time_loc[0]])
        #     data_addr.append(name_file[time_loc[-1]+1:])
        #
        #     end = '%s-%s-%s %s:%s:00' %(time_end[:4], time_end[4:6], time_end[6:8], time_end[8:10], time_end[10:12])
        #     return np.datetime64(end)
        #
        # end = trans_file_Path_into_datetime(data_end)
        # end_before = trans_file_Path_into_datetime(date_end_before)
        #
        # accutime = (end-end_before).astype(int)/60
        # if (accutime % 1) != 0:
        #     logger.error('The sampling interval of radar data must be minute')
        #     sys.exit(0)
        # date_intime = pd.date_range(end=end, freq='%iT'%int(accutime), periods=4)
        # date = date_intime.strftime('%Y%m%d_%H%M')
        # data_addr_all  = []
        # for idate in date:
        #     data_addr_all.append(data_addr[0]+'/'+data_addr[1]+idate+data_addr[2])
        # print(data_addr_all)
        # for i in data_addr_all:
        #     if os.path.exists(i) is False:
        #         logger.error('Missing file: %s' %i)
        #         sys.exit(0)
        return [data1, data2, data3]

    header = get_header()
    print(header['nlat'], header['slat'],header['wlon'],header['elon'])
    from datetime import timedelta
    import pandas as pd
    data_addr = [data1, data2, data3]
    optical_flow(data_addr,  result_name, header, accutime)
except Exception as e:
    logger.error(traceback.format_exc())




