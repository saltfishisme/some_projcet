import os
import numpy as np
import pandas as pd
# print(os.listdir(r'D:\onedrive\cma_data\SURF_CLI_CHN_MERGE_CMP_PRE_HOUR_GRID_0.10'))
'''这个是下载数据时候，改下载命令的。
之前数据老师用linux里面的wget下的，但是文件名太长了存不进去，所以老师让我批量改命令，把默认的特别长的文件名改成短的文件名'''
def shorten(file_name):
    # print(np.loadtxt(file_name, dtype=str)[0].find('SURF_CLI_CHN_MERGE_CMP_PRE_HOUR_GRID_0.10'))
    # try:
        # print(np.loadtxt(file_name, dtype=str)[0].find('grd?'))
    # except:
        # print(np.loadtxt(file_name, dtype=str)[0].find('gif?'))
    # print(np.loadtxt(file_name, dtype=str)[0][153:209])
    data = pd.read_csv(r'F:\tdown\%s'%file_name, dtype=str, header=None)
    data['before'] = 'wget '
    data['middle'] = ' -o '
    data['later'] = [i[153:209] for i in np.array(data[0])]
    result = data['before']+data[0]+data['middle']+data['later']
    np.savetxt(r'D:\onedrive\cma_data\SURF_CLI_CHN_MERGE_CMP_PRE_HOUR_GRID_0.10_shorten\%s'%file_name, np.array(result), fmt='%s')

for i in os.listdir(r'F:\tdown'):
    shorten(str(i))

