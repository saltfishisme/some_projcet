'''续接中国地面气候资料日值数据集'''

import numpy as np
import pandas as pd
import os

"""
合并多个txt
"""

def combine_folder(path, var):
    # 获取目标文件夹的路径
    path = path+'/'+var
    # 获取当前文件夹中的文件名称列表
    filenames = os.listdir(path)
    result = "result/%s.txt" % var
    # 打开当前目录下的result.txt文件，如果没有则创建
    file = open(result, 'w+', encoding="utf-8")
    # 向文件中写入字符

    # 先遍历文件名
    for filename in filenames:
        filepath = path + '/'
        filepath = filepath + filename
        # 遍历单个文件，读取行数
        for line in open(filepath, encoding="utf-8"):
            file.writelines(line)
        file.write('\n')
    # 关闭文件
    file.close()
def combine_file(path, var):
    result = "result/%s.txt" % var
    # 打开当前目录下的result.txt文件，如果没有则创建
    file = open(result, 'w+', encoding="utf-8")
    # 向文件中写入字符
    # 先遍历文件名
    for filename in path:
        for line in open(filename+'/'+var+'.txt', encoding="utf-8"):
            file.writelines(line)
        file.write('\n')
    # 关闭文件
    file.close()
var = ['EVP', 'GST','PRE','PRS','RHU','SSD','TEM','WIN']
combine_file([r'D:\basis\data\row',
              r'D:\basis\some_projects\combine_or_shorten_file_name_length\2019.1-2020.9v3.0'],
             'WIN')
# for i in var:
#     combine_file([r'D:\basis\data\row',
#                   r'D:\basis\some_projects\combine_or_shorten_file_name_length\2019.1-2020.9v3.0'],
#                  i)
# for i in var:
#     combine_folder(r'D:\basis\some_projects\combine_or_shorten_file_name_length\2019.1-2020.9v3.0', i)
# for i in var:
#     a = np.loadtxt('%s2018.txt'%i)
#     np.savetxt('i%s2018.txt'%i,a, fmt='%i')

