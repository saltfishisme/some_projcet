import xarray as xr
import numpy as np
import cartopy.crs as ccrs
import scipy.io as scio
import matplotlib.pyplot as plt

def roll_longitude_from_359to_negative_180(long):
    return xr.where(long > 180,
        long - 360,
        long)


uv1 = xr.open_dataset(r'uv_01_1992.nc')
uv2 = xr.open_dataset(r'uv_02_1992.nc')

uv_extro = xr.concat([uv1, uv2], dim="time").mean(dim='time').sel(level=300)


def call_scatter(uv):
    u = uv['u'].values
    v = uv['v'].values
    wind_speed = np.sqrt(u ** 2 + v ** 2)
    lat = uv.latitude.values
    lon = uv.longitude.values
    wind_lon_2d, wind_lat_2d = np.meshgrid(lon, lat)

    evlon = roll_longitude_from_359to_negative_180(wind_lon_2d)

    fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs.Orthographic(90, 50)},
                           figsize=[4,4])
    plt.subplots_adjust(top=0.99,
                            bottom=0.01,
                            left=0.01,
                            right=0.99)
    ax.contourf(lon, lat, np.ma.masked_greater(wind_speed, 14),levels=20,cmap='Blues', transform=ccrs.PlateCarree(),alpha=0.5)
    import cmaps
    ax.contourf(lon, lat, np.ma.masked_less(wind_speed,14),levels=30, cmap=cmaps.MPL_YlOrBr, transform=ccrs.PlateCarree())


    def to01(x):
        result = (x - np.min(x)) / (np.max(x) - np.min(x))
        result[result<0] = 0
        result[result>1] = 1
        return result

    from matplotlib.colors import ListedColormap
    import matplotlib
    viridis_big = matplotlib.colormaps['binary_r']
    cmap = ListedColormap(viridis_big(np.linspace(0.8, 1, 128)))
    aa = ax.streamplot(wind_lon_2d, wind_lat_2d, u, v, transform=ccrs.PlateCarree(),
                  linewidth=0.3, density=5,color=wind_speed,cmap=cmap, arrowsize=0,)

    ax.coastlines()

    def call_colors_in_mat(Path, var_name):
        '''
        mat file must contain RGB colors as color_len * 3, and this function will add 1 to the end,
        which represents alpha of those colors.

        :param Path: mat file Path
        :param var_name: var in mat file
        :return: colormap
        '''
        from matplotlib.colors import ListedColormap
        import scipy.io as scio
        import numpy as np
        colors = scio.loadmat(Path)[var_name]
        colors_shape = colors.shape
        ones = np.ones([1,colors_shape[0]])
        return ListedColormap(np.concatenate((colors, ones.T), axis=1))

    # add topo
    cmap_topo = call_colors_in_mat(r'D:\MATLAB\colormapdata/OceanLandgray64_light.mat', 'OceanLandgray64_light')
    topo = scio.loadmat('D:/MATLAB/Topo_ERA25.mat')
    lat25 = topo['lat25']; lon25=topo['lon25']; topo25=topo['topo25']
    ax.pcolormesh(lon25, lat25, topo25, shading='nearest',vmin=-6000, vmax=6000,cmap=cmap_topo, transform=ccrs.PlateCarree(), zorder=0)

    # add china shp
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader

    shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\china_basic_map/bou2_4p.shp').geometries(),
                                   ccrs.PlateCarree(),
                                   edgecolor='k', facecolor='none', linewidths=0.4)
    ax.add_feature(shape_feature)
    shape_feature = ShapelyFeature(Reader(r'D:\OneDrive\basis\data\shp\jiuduanxian/jiuduanxian.shp').geometries(),
                                   ccrs.PlateCarree(),
                                   edgecolor='k', facecolor='none', linewidths=0.4)
    ax.add_feature(shape_feature)

    ax.stock_img()

    # extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    # plt.show()
    fig.savefig('fig_2.png',dpi=400)
call_scatter(uv_extro)