import pandas as pd
import numpy as np
df = pd.read_csv('station.csv')

result = pd.DataFrame()
name_list = ['windNumber', 'windfarmName',"coord",'quadrangle', "windfamStage",
      "capacity", "provinceName","cityName","areaName","boosterStationNo", "boosterStationCoord"]
# print(df[df['Column1'].str.contains("boosterStationNo")])
for name in name_list[:-2]:
    a = df[df['Column1'].str.contains(name)]
    f = a.Column1.str.split('"',expand=True)

    result[name] = np.array(f[3])


f = result.coord.str.split(',',expand=True)
f[0] = f[0].str.strip('[')
f[1] = f[1].str.strip(']')
result['lon'] = np.array(f[0])
result['lat'] = np.array(f[1])
# print(result)
# result[name] = np.array(f[3])


result = result[["provinceName","cityName","areaName",'windNumber', 'windfarmName',"windfamStage",'lon','lat']]
result.columns = ["省","市","县",'风电场编码', '项目名称',"阶段",'经度','纬度']
result['阶段'] = result['阶段'].str.replace('01', '规划', regex=False)
result['阶段'] = result['阶段'].str.replace('02', '核准', regex=False)
result['阶段'] = result['阶段'].str.replace('03', '在建', regex=False)
result['阶段'] = result['阶段'].str.replace('04', '已建', regex=False)

result = result.sort_values(by=["省"])
result['序号'] = np.arange(1,300)
result = result[['序号', "省","市","县",'风电场编码', '项目名称',"阶段",'经度','纬度']]
result.to_excel('result.xlsx', index=False)



