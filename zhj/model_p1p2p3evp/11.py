import os
import global_land_mask
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io
import xarray as xr
import nc4
import math
import difflib
def get_overlap(s1, s2):
    s = difflib.SequenceMatcher(None, s1, s2)
    pos_a, pos_b, size = s.find_longest_match(0, len(s1), 0, len(s2))
    return s1[pos_a:pos_a+size]

# prepare land mask
def cal_area(lon, lat):
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S



def area_weighted_mean(data, mask, area_weight):
    area_weight = area_weight.copy()
    data = data.copy()
    mask = mask.copy()
    area_weight[~mask] = 0
    area_weight = area_weight / np.sum(area_weight)
    data[~mask] = np.nan
    return np.nansum(data * area_weight)

def dryzone(data_moment, long, lat):

    area_weight = cal_area(long, lat)

    long = xr.where(
        long > 180,
        long - 360,
        long)
    long, lat = np.meshgrid(long, lat)
    # year loop
    land_mask = global_land_mask.is_land(lat, long)
    sea_mask = global_land_mask.is_ocean(lat, long)

    prect_mask = data_moment<2.5
    # import random
    # random_now = random.randint(1,10)
    # prect_mask[:random_now,:] = 0
    # prect_mask[-random_now:,:] = 0

    def characteristics(mask):
        lat_now = lat.copy()
        lat_now[~mask] = np.nan

        return [np.nanmean(np.nanmax(lat_now, axis=0)),
                np.nanmean(np.nanmin(lat_now, axis=0)),
                area_weighted_mean(data_moment, mask, area_weight),
                np.sum(area_weight[mask])
                ]
    # for all

    try:
        index_all = characteristics(prect_mask)
    except:
        plt.imshow(data_moment)
        plt.show()
    index_land = characteristics((land_mask & prect_mask))
    index_sea = characteristics((sea_mask & prect_mask))

    return [index_all, index_land, index_sea], [prect_mask, (land_mask & prect_mask), (sea_mask & prect_mask)]



def p1p2p3evp(mask, path_main, filename_keyword, year_slice):
    mask = np.array(mask)
    file_path_all = os.listdir(path_main)

    result_all = []

    rank = ['p1', 'p2', 'p3', 'ev']
    rank_nc_var = ['p', 'p', 'p', 'evp']

    for iI_rank, i_rank in enumerate(rank):
        for file_path in file_path_all:
            if i_rank == file_path[:2]:

                if get_overlap(filename_keyword, file_path) == filename_keyword:
                    data = xr.open_dataset(path_main+file_path,decode_times=False).sel(lat=slice(lat_slice[0], lat_slice[1]))
                    print(file_path, data)


                    area_weight = cal_area(lon=data.lon.values, lat=data.lat.values)

                    if not isinstance(year_slice, str):
                        try:
                            prect_time = data.time.values
                        except:
                            prect_time = data.year.values

                        prect_year_mean = []

                        for i in range(year_slice[0], year_slice[1] + 1):
                            i_prect = np.array(data[rank_nc_var[iI_rank]].values[prect_time == i], dtype='double')
                            prect_year_mean.append(i_prect)
                    else:
                        prect_year_mean = data[rank_nc_var[iI_rank]].values

                    result = []
                    for iI, i_prect in enumerate(prect_year_mean):

                        i_data = [area_weighted_mean(np.squeeze(i_prect), mask[iI,0],area_weight),
                                  area_weighted_mean(np.squeeze(i_prect), mask[iI,1],area_weight),
                                  area_weighted_mean(np.squeeze(i_prect), mask[iI,2],area_weight)]

                        result.append(i_data)
                    result_all.append(result)
    return np.array(result_all)

# lat_slice = [-40,-10]
# lat_name = 'S'
lat_slice = [10,40]
lat_name = 'N'
# 需要平均一下
# =================================================================================

# 1900-1999
# =================================================================================


def main_fun_ssp(year_slice, path_main, resultname):
    def combine_nc(path_main, i_keyword):
        file_all = os.listdir(path_main)

        # keyword = []
        # for file_name in file_all:
        #     file_name_split = file_name.split('_')
        #     keyword.append(file_name_split[2])
        # keyword = np.unique(keyword)
        # print(keyword)
        data_in_keyword = []

        i_data = []
        for file_name in file_all:
            if get_overlap(i_keyword, file_name) == i_keyword:
                i_data.append(file_name)

        a = xr.open_dataset(path_main + i_data[0])
        # a = a.assign_coords(time=[i.year for i in a.time.values])
        # a.to_netcdf('tyr2.nc')
        for i_file in i_data[1:]:
            a = xr.concat([a, xr.open_dataset(path_main + i_file)], dim="time")
        return a
        # times = pd.date_range('2001-01-01', periods=a.time.values.shape[0],freq='1D')
        # a = a.assign_coords(time = times)
        # a.to_netcdf('pr_Amon_'+i_keyword+'.nc')

        # xr.concat([da[:, :1], da[:, 1:]], dim="y")
    for file_name_keyword in keyword:
        data_row = combine_nc(path_main, file_name_keyword).sel(lat=slice(lat_slice[0], lat_slice[1]))
        # data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
        print('pr file', file_name_keyword)
        def monthly_to_yearly(data):
            prect_time = data.time.values
            try:
                prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
            except:
                prect_time= np.array([i.year for i in prect_time])
            prect_year_mean = []
            for i in range(year_slice[0], year_slice[1] + 1):
                i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))

            return np.array(prect_year_mean)

        data = monthly_to_yearly(data_row)

        index = []
        mask = []
        for i_data in data:
            i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
            index.append(i_index)
            mask.append(i_mask)
        index = np.array(index)



        index_p1p2p3evp = p1p2p3evp(mask, r'D:\test_zhu\Data\CMIP6\Data-P1-P2-P3/', file_name_keyword, year_slice)

        result = np.append(np.transpose(index, [2,0,1]), index_p1p2p3evp, axis=0)

        times = np.arange(year_slice[0], year_slice[1]+1)

        def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
            """
            :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
            :param var_name: ['t2m', 'ivt', 'uivt']
            :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
            :param lon: list, 720
            :param lat: list, 360
            :param path_save: r'D://'
            :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
            :return:
            """
            import xarray as xr
            import numpy as np

            # def get_dataarray(data):
            #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
            #     return foo

            ds = xr.Dataset()
            ds.coords["land_sea"] = ['all', 'land', 'sea']
            ds.coords["time"] = times

            for iI_vari, ivari in enumerate(var_name):
                ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

            ds.to_netcdf(path_save + '%s.nc' % file_name)

        save_standard_nc(result, ['lat1', 'lat2', 'pre', 'area', 'p1', 'p2', 'p3', 'evp'],
                             times, file_name='index_' + resultname +file_name_keyword+'_'+ lat_name)


def main_fun(year_slice, path_main, resultname, p1p2p3=True, p1p2p3_year_slice=''):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        print(file_name_keyword)
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
                print(file_name, data_row)
                def monthly_to_yearly(data):
                    prect_time = data.time.values
                    try:
                        prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
                    except:
                        prect_time= np.array([i.year for i in prect_time])
                    prect_year_mean = []
                    for i in range(year_slice[0], year_slice[1] + 1):
                        try:
                            i_prect = np.array(data['PRECT'].values[prect_time == i], dtype='double')
                        except:
                            i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                        prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))


                    return np.array(prect_year_mean)

                data = monthly_to_yearly(data_row)

                # def plot_contour(s_data, lon, lat):
                #     import cartopy.crs as ccrs
                #     s_data = np.nanmean(s_data, axis=0)
                #     fig, axs = plt.subplots(1,1,subplot_kw={'projection':ccrs.PlateCarree(central_longitude=180)})
                #     levels = [1.5,2,2.5]
                #     for i in range(3):
                #         axs[i].contour(lon, lat, s_data, levels=levels[i])
                #     plt.show()
                #
                # plot_contour(data, data_row.lon.values, data_row.lat.values)
                #
                #
                # exit(0)

                index = []
                mask = []
                for i_data in data:
                    i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
                    index.append(i_index)
                    mask.append(i_mask)
                result = np.transpose(index, [2,0,1])

                var_name = ['lat1', 'lat2', 'pre', 'area']
                if p1p2p3:
                    index_p1p2p3evp = p1p2p3evp(mask, r'D:\test_zhu\Data\CESM\Data-P1-P2-P3/', file_name_keyword, p1p2p3_year_slice)
                    result = np.append(result, index_p1p2p3evp, axis=0)
                    var_name = ['lat1', 'lat2', 'pre', 'area', 'p1', 'p2', 'p3', 'evp']

                times = np.arange(year_slice[0], year_slice[1]+1)

                def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']
                    ds.coords["time"] = times

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                save_standard_nc(result, var_name,
                                     times, file_name='index_' + resultname +file_name_keyword+'_'+ lat_name)

def main_fun_all_file(path_main, resultname):
    file_name_all =os.listdir(path_main)

    for file_name in file_name_all:
        data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
        if np.isin(data_row['pr'].values.shape, 0).any():
            data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[1], lat_slice[0]))


        print('pr file', file_name)


        def monthly_to_yearly(data):
            prect_time = data.time.values
            try:
                prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
            except:
                prect_time= np.array([i.year for i in prect_time])
            prect_year_mean = []

            for i in range(year_slice[0], year_slice[1] + 1):
                i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')
                prect_year_mean.append(np.nanmean(i_prect * plus000, axis=0))
            return np.array(prect_year_mean)

        need_plus = False
        if np.nanmax(data_row['pr']) < 0.01:
            need_plus = True


        data = data_row['pr'].values* plus

        index = []
        mask = []
        for i_data in data:

            i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
            index.append(i_index)
            mask.append(i_mask)
        result = np.transpose(index, [2,0,1])

        var_name = ['lat1', 'lat2', 'pre', 'area']

        times = data_row.time.values

        def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
            """
            :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
            :param var_name: ['t2m', 'ivt', 'uivt']
            :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
            :param lon: list, 720
            :param lat: list, 360
            :param path_save: r'D://'
            :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
            :return:
            """
            import xarray as xr
            import numpy as np

            # def get_dataarray(data):
            #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
            #     return foo

            ds = xr.Dataset()
            ds.coords["land_sea"] = ['all', 'land', 'sea']
            ds.coords["time"] = times

            for iI_vari, ivari in enumerate(var_name):
                ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

            ds.to_netcdf(path_save + '%s.nc' % file_name)

        keywords = file_name.split('_')[2]
        save_standard_nc(result, var_name,
                             times, file_name='index_' + resultname +keywords+'_'+ lat_name)

def main_fun_avg(path_main, resultname):
    file_name_all =os.listdir(path_main)

    for file_name in file_name_all:

        data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
        if np.isin(data_row['pr'].values.shape, 0).any():
            data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[1], lat_slice[0]))


        print('pr file', file_name)

        need_plus = False
        if np.nanmax(data_row['pr']) < 0.01:
            need_plus = True


        data = data_row['pr'].values * plus

        index, mask = dryzone(data, long=data_row.lon.values, lat=data_row.lat.values)

        result = np.transpose(index, [1,0])

        var_name = ['lat1', 'lat2', 'pre', 'area']


        def save_standard_nc(data, var_name, path_save="", file_name='Vari'):
            """
            :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
            :param var_name: ['t2m', 'ivt', 'uivt']
            :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
            :param lon: list, 720
            :param lat: list, 360
            :param path_save: r'D://'
            :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
            :return:
            """
            import xarray as xr
            import numpy as np

            # def get_dataarray(data):
            #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
            #     return foo

            ds = xr.Dataset()
            ds.coords["land_sea"] = ['all', 'land', 'sea']

            for iI_vari, ivari in enumerate(var_name):
                ds[ivari] = (("land_sea"), np.array(data[iI_vari]))

            ds.to_netcdf(path_save + '%s.nc' % file_name)

        keywords = file_name.split('_')[2]
        save_standard_nc(result, var_name, file_name='index_' + resultname +keywords+'_'+ lat_name)

def main_fun_avg_in_fun(year_slice, path_main, resultname):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        print(file_name_keyword)
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
                print(file_name, data_row)
                def monthly_to_yearly(data):
                    prect_time = data.time.values
                    try:
                        prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
                    except:
                        prect_time= np.array([i.year for i in prect_time])
                    prect_year_mean = []
                    for i in range(year_slice[0], year_slice[1] + 1):
                        try:
                            i_prect = np.array(data['PRECT'].values[prect_time == i], dtype='double')
                        except:
                            i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                        prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))


                    return np.array(prect_year_mean)

                data = monthly_to_yearly(data_row)
                data = np.nanmean(data, axis=0)

                index, mask = dryzone(data, long=data_row.lon.values, lat=data_row.lat.values)


                result = np.transpose(index, [1, 0])

                var_name = ['lat1', 'lat2', 'pre', 'area']

                def save_standard_nc(data, var_name, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (("land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                save_standard_nc(result, var_name, file_name='index_' + resultname + file_name_keyword + '_' + lat_name)


def to_one_time(path_main, yearly_slice):
    path_all = os.listdir(path_main)
    for i_path in path_all:
        a = xr.open_dataset(path_main+ i_path)
        a.sel()
        try:
            a = a['pr'].mean(dim='time')
        except:
            a = a['PRECT'].mean(dim='time')
        os.makedirs(path_main+'avg/',exist_ok=True)
        a.to_netcdf(path_main+'avg/'+i_path[:-3]+'_avg.nc')

# plus = 86400
# # #
# keyword=['ACCESS-CM2', 'ACCESS-ESM1-5', 'CanESM5', 'IPSL',
#  'MRI','CESM2']
# main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist_')
# #
# #
# keyword=['ACCESS-CM2', 'ACCESS-ESM1-5', 'CanESM5', 'IPSL',
#  'MRI']
# # ssp for combine nc
# # mian_Fun for year_slice, keyword
# # all file and avg dont have year_slice and keyword
# main_fun_ssp([2015, 2300], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
# #
# keyword=['CESM2']
# main_fun_ssp([2015, 2299], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
# #
# #
# #
# main_fun_all_file(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr/', 'long-term_')
# main_fun_avg(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr_avg/', 'long-term-control_')
#
plus = 86400000
keyword=['1pctCO']
main_fun([1, 233], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)

keyword=['2XCO']
main_fun([1, 500], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
keyword=['4XCO']
main_fun([1, 1000], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)

keyword=['pictrl']
main_fun_avg_in_fun([1, 151], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_')
#
exit(0)

# vertification






# a = xr.open_dataset(r'index_CMIP6_ssp585_ACCESS-CM2_N.nc')
# print(a)
# var = 'lat1'
# print(a[var].values[:,0])


# plt.plot(a[var].values[:,0], label='all')
# plt.plot(a[var].values[:,1], label='land')
# plt.plot(a[var].values[:,2], label='sea')
# plt.legend()
# plt.show()

import matplotlib
SMALL_SIZE=5

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize




path = os.listdir(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/')
for i_path in path:
    if i_path[-4] == 'N':
        var_NS = 'north'
    if i_path[-4] == 'S':
        var_NS = 'south'

    a = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/'+
                        i_path)

    land_sea_name = ['all', 'land', 'sea']

    for i_var in list(a.data_vars):
        for land_sea in range(3):

            file = r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp'+'/'+\
                   land_sea_name[land_sea]+'/'+\
                   i_path.split('_')[1]+'/'+\
                   i_path.split('_')[2]+'/'
            os.makedirs(file, exist_ok=True)
            try:
                np.savetxt(file + r'%s.txt'%(var_NS+i_var), a[i_var].values[:, land_sea])
            except:
                np.savetxt(file + r'%s.txt'%(var_NS+i_var), [a[i_var].values[land_sea]])







exit(0)
for i_path in path:

    a = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/'+
                        i_path)
    print(a)
    try:
        sdfsdf = a.time
    except:
        continue

    if len(list(a.data_vars)) >5:
        var = [ 'p1', 'p2', 'p3', 'evp', 'pre', 'lat1', 'lat2', 'area',]
        axs_num = [3,3]
    # else:
    #     var = ['lat1', 'lat2', 'pre', 'area']
    #     axs_num = [2,2]
        fig, axs = plt.subplots(axs_num[0], axs_num[1])

        for i in range(axs_num[0]):
            for j in range(axs_num[0]):
                ax = axs[i][j]
                now = i * axs_num[0] + j
                if now == 7:
                    break

                try:
                    ax.plot(a.time, a[var[now]].values[:, 0], label='all', color='red')
                except:
                    print(i_path)
                ax.plot(a.time, a[var[now]].values[:, 1], label='land', color='blue')
                ax.plot(a.time, a[var[now]].values[:, 2], label='sea', color='black')
                ax.set_title(var[now]+' '+i_path[6:-3])
                ax.legend()

            # plt.ylim([-0.5,0.5])
        plt.savefig(i_path[6:-3]+'.png', dpi=400)
        plt.close()
exit(0)


plt.plot(a[var].values[:,0], label='all')
plt.plot(a[var].values[:,1], label='land')
plt.plot(a[var].values[:,2], label='sea')
plt.legend()
plt.show()

