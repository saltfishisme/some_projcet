import os
import global_land_mask
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io
import xarray as xr
import nc4
import math
import difflib
def get_overlap(s1, s2):
    s = difflib.SequenceMatcher(None, s1, s2)
    pos_a, pos_b, size = s.find_longest_match(0, len(s1), 0, len(s2))
    return s1[pos_a:pos_a+size]

# prepare land mask
def cal_area(lon, lat):
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S



def area_weighted_mean(data, mask, area_weight):
    area_weight = area_weight.copy()
    data = data.copy()
    mask = mask.copy()
    area_weight[~mask] = 0
    area_weight = area_weight / np.sum(area_weight)
    data[~mask] = np.nan
    return np.nansum(data * area_weight)

def dryzone(data_moment, long, lat):

    area_weight = cal_area(long, lat)

    long = xr.where(
        long > 180,
        long - 360,
        long)
    long, lat = np.meshgrid(long, lat)
    # year loop
    land_mask = global_land_mask.is_land(lat, long)
    sea_mask = global_land_mask.is_ocean(lat, long)

    prect_mask = data_moment<2.5
    # import random
    # random_now = random.randint(1,10)
    # prect_mask[:random_now,:] = 0
    # prect_mask[-random_now:,:] = 0

    def characteristics(mask):
        lat_now = lat.copy()
        lat_now[~mask] = np.nan

        return [np.nanmean(np.nanmax(lat_now, axis=0)),
                np.nanmean(np.nanmin(lat_now, axis=0)),
                area_weighted_mean(data_moment, mask, area_weight),
                np.sum(area_weight[mask])
                ]
    # for all

    try:
        index_all = characteristics(prect_mask)
    except:
        plt.imshow(data_moment)
        plt.show()
    index_land = characteristics((land_mask & prect_mask))
    index_sea = characteristics((sea_mask & prect_mask))

    return [index_all, index_land, index_sea], [prect_mask, (land_mask & prect_mask), (sea_mask & prect_mask)]



def p1p2p3evp(mask, path_main, filename_keyword, year_slice):
    mask = np.array(mask)
    file_path_all = os.listdir(path_main)

    result_all = []

    rank = ['p1', 'p2', 'p3', 'ev']
    rank_nc_var = ['p', 'p', 'p', 'evp']

    for iI_rank, i_rank in enumerate(rank):
        for file_path in file_path_all:
            if i_rank == file_path[:2]:

                if get_overlap(filename_keyword, file_path) == filename_keyword:
                    data = xr.open_dataset(path_main+file_path,decode_times=False).sel(lat=slice(lat_slice[0], lat_slice[1]))
                    print(file_path, data)


                    area_weight = cal_area(lon=data.lon.values, lat=data.lat.values)

                    if not isinstance(year_slice, str):
                        try:
                            prect_time = data.time.values
                        except:
                            prect_time = data.year.values

                        prect_year_mean = []

                        for i in range(year_slice[0], year_slice[1] + 1):
                            i_prect = np.array(data[rank_nc_var[iI_rank]].values[prect_time == i], dtype='double')
                            prect_year_mean.append(i_prect)
                    else:
                        prect_year_mean = data[rank_nc_var[iI_rank]].values

                    result = []
                    for iI, i_prect in enumerate(prect_year_mean):

                        i_data = [area_weighted_mean(np.squeeze(i_prect), mask[iI,0],area_weight),
                                  area_weighted_mean(np.squeeze(i_prect), mask[iI,1],area_weight),
                                  area_weighted_mean(np.squeeze(i_prect), mask[iI,2],area_weight)]

                        result.append(i_data)
                    result_all.append(result)
    return np.array(result_all)


# 需要平均一下
# =================================================================================

# 1900-1999
# =================================================================================



def main_fun_avg_in_fun(year_slice, path_main, resultname):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
                print(file_name)
                def monthly_to_yearly(data):
                    prect_time = data.time.values
                    try:
                        prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
                    except:
                        prect_time= np.array([i.year for i in prect_time])
                    prect_year_mean = []
                    for i in range(year_slice[0], year_slice[1] + 1):
                        try:
                            i_prect = np.array(data['PRECT'].values[prect_time == i], dtype='double')
                        except:
                            i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                        prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))


                    return np.array(prect_year_mean)

                data = monthly_to_yearly(data_row)
                data = np.nanmean(data, axis=0)
                return [data, data_row.lon.values, data_row.lat.values]
                print('dryzone')
                index, mask = dryzone(data, long=data_row.lon.values, lat=data_row.lat.values)
                print('dryzone2')

                result = np.transpose(index, [1, 0])

                var_name = ['lat1', 'lat2', 'pre', 'area']

                def save_standard_nc(data, var_name, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (("land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                save_standard_nc(result, var_name, file_name='index_' + resultname + file_name_keyword + '_' + lat_name)

def main_fun(year_slice, path_main, resultname, p1p2p3=True, p1p2p3_year_slice=''):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        print(file_name_keyword)
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
                print(file_name, data_row)
                def monthly_to_yearly(data):
                    prect_time = data.time.values
                    try:
                        prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
                    except:
                        prect_time= np.array([i.year for i in prect_time])
                    prect_year_mean = []
                    for i in range(year_slice[0], year_slice[1] + 1):
                        try:
                            i_prect = np.array(data['PRECT'].values[prect_time == i], dtype='double')
                        except:
                            i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                        prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))


                    return np.array(prect_year_mean)

                data = monthly_to_yearly(data_row)
                return [np.nanmean(data, axis=0),data_row.lon.values, data_row.lat.values]

                index = []
                mask = []
                for i_data in data:
                    i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
                    index.append(i_index)
                    mask.append(i_mask)
                result = np.transpose(index, [2,0,1])

                var_name = ['lat1', 'lat2', 'pre', 'area']
                if p1p2p3:
                    index_p1p2p3evp = p1p2p3evp(mask, r'D:\test_zhu\Data\CESM\Data-P1-P2-P3/', file_name_keyword, p1p2p3_year_slice)
                    result = np.append(result, index_p1p2p3evp, axis=0)
                    var_name = ['lat1', 'lat2', 'pre', 'area', 'p1', 'p2', 'p3', 'evp']

                times = np.arange(year_slice[0], year_slice[1]+1)

                def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']
                    ds.coords["time"] = times

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                save_standard_nc(result, var_name,
                                     times, file_name='index_' + resultname +file_name_keyword+'_'+ lat_name)


#

lat_slice = [-40,-10]
lat_name = 'S'

datas = []
plus = 86400000
keyword=['pictrl']
datas.append(main_fun_avg_in_fun([1, 151], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM-control_'))
keyword=['2XCO']
datas.append(main_fun([451, 500], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False))
keyword=['4XCO']
datas.append(main_fun([951, 1000], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False))
keyword=['1pctCO']
datas.append(main_fun([151, 200], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False))





lat_slice = [10,40]
lat_name = 'N'
plus = 86400000
keyword=['pictrl']
datas.append(main_fun_avg_in_fun([1, 151], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM-control_'))
keyword=['2XCO']
datas.append(main_fun([451, 500], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False))
keyword=['4XCO']
datas.append(main_fun([951, 1000], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False))
keyword=['1pctCO']
datas.append(main_fun([151, 200], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False))


import matplotlib
SMALL_SIZE=9

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize


def plot_contour(data):
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    import cartopy.crs as ccrs
    fig, axs = plt.subplots(4, 1, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)},
                            figsize=[5,6])
    plt.subplots_adjust(top=0.95,
bottom=0.05,
left=0.13,
right=0.9,
hspace=0.425,
wspace=0.2)
    levels = [1.5, 2, 2.5]
    names = ['(a) PiControl','(b) 2XCO2', '(c) 4XCO2', '(d) 1%CO2']
    for i in range(4):
        s_data, lon, lat = data[i]
        cl = axs[i].contour(lon, lat, s_data, levels=levels, transform=ccrs.PlateCarree(),
                            colors=['red', 'blue', 'black'],
                            linewidths=1)
        s_data, lon, lat = data[4+i]
        cl = axs[i].contour(lon, lat, s_data, levels=levels, transform=ccrs.PlateCarree(),
                            colors=['red', 'blue', 'black'],
                            linewidths=1)

        # axs[i].clabel(cl)
        axs[i].set_title(names[i],loc='left')
        axs[i].coastlines(linewidth=0.3)
        axs[i].gridlines(crs=ccrs.PlateCarree(),ylocs=[40, 10,-10, -40], xlocs=[], linestyle='--')
        # a = circulation(axs[i], lon=np.arange(100, 360, 0.5), lat=np.arange(90, -90.5, -0.5), ranges=[[0,359,45,-45],
        # [0,360,90],[45,-46,-15]])
        # a.lat_lon_shape()
        axs[i].set_xticks([0,90,180,270,0], crs=ccrs.PlateCarree())
        axs[i].set_yticks(np.arange(45,-46,-15), crs=ccrs.PlateCarree())
        axs[i].set_extent([0,359,45,-45], ccrs.PlateCarree())
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        axs[i].xaxis.set_major_formatter(lon_formatter)
        axs[i].yaxis.set_major_formatter(lat_formatter)
    # plt.show()
    plt.savefig('cesm_contour.png', dpi=400)


plot_contour(datas)



