import os
import global_land_mask
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io
import xarray as xr
import nc4
import math
import difflib
def get_overlap(s1, s2):
    s = difflib.SequenceMatcher(None, s1, s2)
    pos_a, pos_b, size = s.find_longest_match(0, len(s1), 0, len(s2))
    return s1[pos_a:pos_a+size]

# prepare land mask
def cal_area(lon, lat):
    lon, lat = np.meshgrid(lon, lat)

    R = 6378.137
    dlon = lon[0, 1] - lon[0, 0]
    delta = dlon * math.pi / 180.
    S=np.zeros(lon.shape)
    Dx=np.zeros(lon.shape)
    Dy=np.ones(lon.shape)

    Dx = R * np.cos(lat * math.pi / 180.) * delta
    Dy = Dy * delta * R

    S = Dx * Dy
    return S



def area_weighted_mean(data, mask, area_weight):
    area_weight = area_weight.copy()
    data = data.copy()
    mask = mask.copy()
    area_weight[~mask] = 0
    area_weight = area_weight / np.sum(area_weight)
    data[~mask] = np.nan
    return np.nansum(data * area_weight)

def dryzone(data_moment, long, lat):
    import metpy.calc as mpcalc
    # plt.imshow(data_moment)
    # plt.show()
    # plt.close()
    data_moment = mpcalc.smooth_n_point(data_moment, 9)
    area_weight = cal_area(long, lat)
    # plt.imshow(data_moment)
    # plt.show()
    # plt.close()
    long = xr.where(
        long > 180,
        long - 360,
        long)
    long, lat = np.meshgrid(long, lat)
    # year loop
    land_mask = global_land_mask.is_land(lat, long)
    sea_mask = global_land_mask.is_ocean(lat, long)

    prect_mask = data_moment<2.5


    def characteristics(mask, create_mask=0):
        lat_now = lat.copy()
        lat_now[~mask] = np.nan
        if mask_NSedge:
            a_add = []
            lat1 = np.nanmax(lat_now, axis=0)
            a_add.append(lat1 == np.max([lat[0, 0], lat[-1, 0]]))
            lat1[a_add[0]]=np.nan

            lat1 = np.nanmean(lat1)

            lat2 = np.nanmin(lat_now, axis=0)

            a_add.append(lat2 == np.min([lat[0, 0], lat[-1, 0]]))
            lat2[a_add[1]] = np.nan
            lat2 = np.nanmean(lat2)

            if create_mask:
                global mask_NSedge_array
                mask_NSedge_array = a_add
            # plt.imshow(mask)
            # plt.show()
            # plt.close()
        else:
            # plt.imshow(np.array(mask_NSedge_array))
            # plt.show()
            # plt.close()
            lat1 = np.nanmax(lat_now, axis=0)
            lat1[mask_NSedge_array[0]]=np.nan
            lat1 = np.nanmean(lat1)

            lat2 = np.nanmin(lat_now, axis=0)
            lat2[mask_NSedge_array[1]] = np.nan
            lat2 = np.nanmean(lat2)

        return [lat1,
                lat2,
                area_weighted_mean(data_moment, mask, area_weight),
                np.sum(area_weight[mask])
                ]
    # for all


    index_all = characteristics(prect_mask, 1)
    index_land = characteristics((land_mask & prect_mask))
    index_sea = characteristics((sea_mask & prect_mask))

    return [index_all, index_land, index_sea], [prect_mask, (land_mask & prect_mask), (sea_mask & prect_mask)]



def p1p2p3evp(mask, path_main, filename_keyword, year_slice):
    mask = np.array(mask)
    file_path_all = os.listdir(path_main)

    result_all = []

    rank = ['p1', 'p2', 'p3', 'ev']
    rank_nc_var = ['p', 'p', 'p', 'evp']

    for iI_rank, i_rank in enumerate(rank):
        for file_path in file_path_all:
            if i_rank == file_path[:2]:

                if get_overlap(filename_keyword, file_path) == filename_keyword:
                    data = xr.open_dataset(path_main+file_path,decode_times=False).sel(lat=slice(lat_slice[0], lat_slice[1]),
                                                                                       lon=slice(lon_slice[0], lon_slice[1]))
                    print(file_path)


                    area_weight = cal_area(lon=data.lon.values, lat=data.lat.values)

                    if not isinstance(year_slice, str):
                        try:
                            prect_time = data.time.values
                        except:
                            prect_time = data.year.values

                        prect_year_mean = []

                        for i in range(year_slice[0], year_slice[1] + 1):
                            i_prect = np.array(data[rank_nc_var[iI_rank]].values[prect_time == i], dtype='double')
                            prect_year_mean.append(i_prect)
                    else:
                        prect_year_mean = data[rank_nc_var[iI_rank]].values

                    result = []
                    for iI, i_prect in enumerate(prect_year_mean):

                        i_data = [area_weighted_mean(np.squeeze(i_prect), mask[iI,0],area_weight),
                                  area_weighted_mean(np.squeeze(i_prect), mask[iI,1],area_weight),
                                  area_weighted_mean(np.squeeze(i_prect), mask[iI,2],area_weight)]

                        result.append(i_data)
                    result_all.append(result)
    return np.array(result_all)


# 需要平均一下
# =================================================================================

# 1900-1999
# =================================================================================


def main_fun_ssp(year_slice, path_main, resultname):
    def combine_nc(path_main, i_keyword):
        file_all = os.listdir(path_main)

        # keyword = []
        # for file_name in file_all:
        #     file_name_split = file_name.split('_')
        #     keyword.append(file_name_split[2])
        # keyword = np.unique(keyword)
        # print(keyword)
        data_in_keyword = []

        i_data = []
        for file_name in file_all:
            if get_overlap(i_keyword, file_name) == i_keyword:
                i_data.append(file_name)

        a = xr.open_dataset(path_main + i_data[0])
        # a = a.assign_coords(time=[i.year for i in a.time.values])
        # a.to_netcdf('tyr2.nc')
        for i_file in i_data[1:]:
            a = xr.concat([a, xr.open_dataset(path_main + i_file)], dim="time")
        return a
        # times = pd.date_range('2001-01-01', periods=a.time.values.shape[0],freq='1D')
        # a = a.assign_coords(time = times)
        # a.to_netcdf('pr_Amon_'+i_keyword+'.nc')

        # xr.concat([da[:, :1], da[:, 1:]], dim="y")
    for file_name_keyword in keyword:
        data_row = combine_nc(path_main, file_name_keyword).sel(lat=slice(lat_slice[0], lat_slice[1]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))
        # data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]))
        print('pr file', file_name_keyword)
        def monthly_to_yearly(data):
            prect_time = data.time.values
            try:
                prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
            except:
                prect_time= np.array([i.year for i in prect_time])
            prect_year_mean = []
            for i in range(year_slice[0], year_slice[1] + 1):
                i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))

            return np.array(prect_year_mean)

        data = monthly_to_yearly(data_row)
        if return_now:
            return data, data_row.lon.values, data_row.lat.values
        index = []
        mask = []
        for i_data in data:
            i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
            index.append(i_index)
            mask.append(i_mask)
        index = np.array(index)



        index_p1p2p3evp = p1p2p3evp(mask, r'D:\test_zhu\Data\CMIP6\Data-P1-P2-P3/', file_name_keyword, year_slice)

        result = np.append(np.transpose(index, [2,0,1]), index_p1p2p3evp, axis=0)

        times = np.arange(year_slice[0], year_slice[1]+1)

        def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
            """
            :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
            :param var_name: ['t2m', 'ivt', 'uivt']
            :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
            :param lon: list, 720
            :param lat: list, 360
            :param path_save: r'D://'
            :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
            :return:
            """
            import xarray as xr
            import numpy as np

            # def get_dataarray(data):
            #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
            #     return foo

            ds = xr.Dataset()
            ds.coords["land_sea"] = ['all', 'land', 'sea']
            ds.coords["time"] = times

            for iI_vari, ivari in enumerate(var_name):
                ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

            ds.to_netcdf(path_save + '%s.nc' % file_name)

        save_standard_nc(result, ['lat1', 'lat2', 'pre', 'area', 'p1', 'p2', 'p3', 'evp'],
                             times, file_name=path_SaveData+'index_' + resultname +file_name_keyword+'_'+ lat_name)
        return data, data_row.lon.values, data_row.lat.values

def main_fun(year_slice, path_main, resultname, p1p2p3=True, p1p2p3_year_slice=''):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        print(file_name_keyword)
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))
                print(data_row)

                def monthly_to_yearly(data):
                    prect_time = data.time.values
                    try:
                        prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
                    except:
                        prect_time= np.array([i.year for i in prect_time])
                    prect_year_mean = []
                    for i in range(year_slice[0], year_slice[1] + 1):
                        try:
                            i_prect = np.array(data['PRECT'].values[prect_time == i], dtype='double')
                        except:
                            i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                        prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))


                    return np.array(prect_year_mean)

                data = monthly_to_yearly(data_row)
                if return_now:
                    return data, data_row.lon.values, data_row.lat.values
                # def plot_contour(s_data, lon, lat):
                #     import cartopy.crs as ccrs
                #     s_data = np.nanmean(s_data, axis=0)
                #     fig, axs = plt.subplots(1,1,subplot_kw={'projection':ccrs.PlateCarree(central_longitude=180)})
                #     levels = [1.5,2,2.5]
                #     for i in range(3):
                #         axs[i].contour(lon, lat, s_data, levels=levels[i])
                #     plt.show()
                #
                # plot_contour(data, data_row.lon.values, data_row.lat.values)
                #
                #
                # exit(0)

                index = []
                mask = []
                for i_data in data:
                    i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
                    index.append(i_index)
                    mask.append(i_mask)
                result = np.transpose(index, [2,0,1])

                var_name = ['lat1', 'lat2', 'pre', 'area']
                if p1p2p3:
                    index_p1p2p3evp = p1p2p3evp(mask, r'D:\test_zhu\Data\CESM\Data-P1-P2-P3/', file_name_keyword, p1p2p3_year_slice)
                    result = np.append(result, index_p1p2p3evp, axis=0)
                    var_name = ['lat1', 'lat2', 'pre', 'area', 'p1', 'p2', 'p3', 'evp']

                times = np.arange(year_slice[0], year_slice[1]+1)

                def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']
                    ds.coords["time"] = times

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                save_standard_nc(result, var_name,
                                     times, file_name=path_SaveData+'index_' + resultname +file_name_keyword+'_'+ lat_name)
                return data, data_row.lon.values, data_row.lat.values

def main_fun_all_file(path_main, resultname):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        print(file_name_keyword)
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))
                if np.isin(data_row['pr'].values.shape, 0).any():
                    data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[1], lat_slice[0]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))

                print('pr file', file_name)

                data = data_row['pr'].values* plus
                if return_now:
                    return data, data_row.lon.values, data_row.lat.values
                index = []
                mask = []
                for i_data in data:

                    i_index, i_mask = dryzone(i_data, long=data_row.lon.values, lat=data_row.lat.values)
                    index.append(i_index)
                    mask.append(i_mask)
                result = np.transpose(index, [2,0,1])

                var_name = ['lat1', 'lat2', 'pre', 'area']

                times = data_row.time.values

                def save_standard_nc(data, var_name, times, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']
                    ds.coords["time"] = times

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (('time', "land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                keywords = file_name.split('_')[2]
                save_standard_nc(result, var_name,
                                     times, file_name=path_SaveData+'index_' + resultname +keywords+'_'+ lat_name)
                return data, data_row.lon.values, data_row.lat.values

def main_fun_avg(path_main, resultname):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))
                if np.isin(data_row['pr'].values.shape, 0).any():
                    data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[1], lat_slice[0]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))

                data = data_row['pr'].values * plus
                if return_now:
                    return data, data_row.lon.values, data_row.lat.values

                index, mask = dryzone(data, long=data_row.lon.values, lat=data_row.lat.values)

                result = np.transpose(index, [1,0])

                var_name = ['lat1', 'lat2', 'pre', 'area']


                def save_standard_nc(data, var_name, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (("land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                keywords = file_name.split('_')[2]
                save_standard_nc(result, var_name, file_name=path_SaveData+'index_' + resultname +keywords+'_'+ lat_name)
                return data, data_row.lon.values, data_row.lat.values

def main_fun_avg_in_fun(year_slice, path_main, resultname):
    file_name_all =os.listdir(path_main)
    for file_name_keyword in keyword:
        for file_name in file_name_all:
            if get_overlap(file_name_keyword, file_name) == file_name_keyword:
                data_row = xr.open_dataset(path_main + file_name).sel(lat=slice(lat_slice[0], lat_slice[1]),
                                                                lon=slice(lon_slice[0], lon_slice[1]))
                print(file_name)
                def monthly_to_yearly(data):
                    prect_time = data.time.values
                    try:
                        prect_time = np.array([pd.to_datetime(i).year for i in prect_time])
                    except:
                        prect_time= np.array([i.year for i in prect_time])
                    prect_year_mean = []
                    for i in range(year_slice[0], year_slice[1] + 1):
                        try:
                            i_prect = np.array(data['PRECT'].values[prect_time == i], dtype='double')
                        except:
                            i_prect = np.array(data['pr'].values[prect_time == i], dtype='double')

                        prect_year_mean.append(np.nanmean(i_prect * plus, axis=0))


                    return np.array(prect_year_mean)

                data = monthly_to_yearly(data_row)
                data = np.nanmean(data, axis=0)

                if return_now:
                    return data, data_row.lon.values, data_row.lat.values

                print('dryzone')
                index, mask = dryzone(data, long=data_row.lon.values, lat=data_row.lat.values)
                print('dryzone2')

                result = np.transpose(index, [1, 0])

                var_name = ['lat1', 'lat2', 'pre', 'area']

                def save_standard_nc(data, var_name, path_save="", file_name='Vari'):
                    """
                    :param data: list. [4*360*720], [t2m]|[4*360*720, 4*360*720, 4*360*720], [t2m, ivt, uivt]
                    :param var_name: ['t2m', 'ivt', 'uivt']
                    :param times: list, ['20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00','20001-01-01 05:00']
                    :param lon: list, 720
                    :param lat: list, 360
                    :param path_save: r'D://'
                    :param file_name: str, 'DJF', and will saved as D://DJF.nc, save_name
                    :return:
                    """
                    import xarray as xr
                    import numpy as np

                    # def get_dataarray(data):
                    #     foo = xr.DataArray(data, coords=[times, lat, lon], dims=['time', "lat", 'lon'])
                    #     return foo

                    ds = xr.Dataset()
                    ds.coords["land_sea"] = ['all', 'land', 'sea']

                    for iI_vari, ivari in enumerate(var_name):
                        ds[ivari] = (("land_sea"), np.array(data[iI_vari]))

                    ds.to_netcdf(path_save + '%s.nc' % file_name)

                save_standard_nc(result, var_name, file_name=path_SaveData+'index_' + resultname + file_name_keyword + '_' + lat_name)

def to_one_time(path_main, yearly_slice):
    path_all = os.listdir(path_main)
    for i_path in path_all:
        a = xr.open_dataset(path_main+ i_path)
        a.sel()
        try:
            a = a['pr'].mean(dim='time')
        except:
            a = a['PRECT'].mean(dim='time')
        os.makedirs(path_main+'avg/',exist_ok=True)
        a.to_netcdf(path_main+'avg/'+i_path[:-3]+'_avg.nc')


def mask_NSedge_by_control(control):
    prect_mask = control < 2.5
    prect_mask[1:-1,:] = 0

# lon_slice = [130,180+70]
lon_slice = [0,360]
# lon_slice_index =
# # lat_slice = [-40,-10]
# # lat_name = 'S'
lat_slice = [10,40]
lat_name = 'N'
#
return_now = 0
# ######################### no data return ####################################
'''====================================================================='''

plus = 86400

# ==============================================================================

path_SaveData = r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/'

for keyword in [['ACCESS-CM2'], ['ACCESS-ESM1-5'], ['CanESM5'], ['IPSL'],
 ['MRI']]:
    mask_NSedge = 1
    main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
    mask_NSedge = 0

    main_fun_ssp([2015, 2300], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')



for keyword in [['CESM2']]:
    mask_NSedge = 1
    main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
    mask_NSedge = 0
    main_fun_ssp([2015, 2299], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')


# ==============================================================================

for keyword in [['MPIESM11'],['CESM104'],['GISSE2R']]:
    mask_NSedge = 1
    main_fun_avg(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr_avg/', 'long-term-control_')
    mask_NSedge = 0
    main_fun_all_file(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr/', 'long-term_')


# ==============================================================================
# 共用一个mask
plus = 86400000
mask_NSedge = 1
keyword=['pictrl']
main_fun_avg_in_fun([1, 151], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM-control_')

mask_NSedge = 0
keyword=['1pctCO']
main_fun([1, 233], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)

keyword=['2XCO']
main_fun([1, 500], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
keyword=['4XCO']
main_fun([1, 1000], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
#


""" data return """


# ######################### data return ####################################
# # ==============================================================================
#
# path_SaveData = r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/'
# obs_rank = ['2XCO', '4XCO','1pctCO',
#             'ACCESS-CM2', 'ACCESS-ESM1-5', 'CanESM5', 'CESM2', 'IPSL', 'MRI',
#             'MPIESM11', 'GISSE2R', 'CESM104']
# hist_rank = ['pictrl', 'pictrl','pictrl',
#             'ACCESS-CM2', 'ACCESS-ESM1-5', 'CanESM5', 'CESM2', 'IPSL', 'MRI',
#             'MPIESM11', 'GISSE2R', 'CESM104']
# figlabelstr = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)',
#                '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)']
# name_rank = ['CESM1 2xCO2', 'CESM1 4xCO2','CESM1 1%CO2',
#             'ACCESS-CM2 SSP5-8.5', 'ACCESS-ESM1-5 SSP5-8.5', 'CanESM5 SSP5-8.5', 'CESM2-WACCM SSP5-8.5',
#              'IPSL-CM6A-LR SSP5-8.5', 'MRI-ESM2-0 SSP5-8.5',
#             'MPI-ESM-1.1 4xCO2', 'GISS-E2-R 4xCO2', 'CESM1.0.4 4xCO2']
#
# '''====================================================================='''
# # lat_slice = [-40,-10]
# # lat_name = 'S'
# lon_slice=[0,360]
# lat_slice = [10,40]
# lat_name = 'N'
#
# return_now = 1
#
# plot_result = []
# plot_model_name = []
#
# # ==============================================================================
# # 共用一个mask
# plus = 86400000
# mask_NSedge = 0
#
# keyword=['pictrl']
# s_data_pictrl = main_fun_avg_in_fun([1, 151], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM-control_')
#
#
# mask_NSedge = 0
# keyword=['2XCO']
# plot_model_name.append(keyword)
# s_data = main_fun([1, 500], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
# plot_result.append([s_data_pictrl , [np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]]])
#
# keyword=['4XCO']
# plot_model_name.append(keyword)
# s_data = main_fun([1, 1000], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
# plot_result.append([s_data_pictrl , [np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]]])
#
# keyword=['1pctCO']
# plot_model_name.append(keyword)
# s_data = main_fun([151, 200], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
# plot_result.append([s_data_pictrl , [np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]]])
#
# plus = 86400
# for keyword in [['ACCESS-CM2'], ['ACCESS-ESM1-5'], ['CanESM5']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
#     s_result.append(s_data)
#
#     mask_NSedge = 0
#     s_data = main_fun_ssp([2015, 2300], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#     plot_result.append(s_result)
#
# for keyword in [['CESM2']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
#     s_result.append(s_data)
#
#     mask_NSedge = 0
#     s_data = main_fun_ssp([2015, 2299], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#     plot_result.append(s_result)
#
# for keyword in [ ['IPSL'],
#  ['MRI']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
#     s_result.append(s_data)
#
#     mask_NSedge = 0
#     s_data = main_fun_ssp([2015, 2300], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#     plot_result.append(s_result)
# # ==============================================================================
#
# for keyword in [['MPIESM11'],['GISSE2R'],['CESM104']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr_avg/', 'long-term-control_')
#     s_result.append(s_data)
#     mask_NSedge = 0
#     s_data = main_fun_all_file(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr/', 'long-term_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#
#     plot_result.append(s_result)
#
#
#
# ######################### no data return ####################################
# '''====================================================================='''
# lat_slice = [-40,-10]
# lat_name = 'S'
# # lat_slice = [10,60]
# # lat_name = 'N'
#
# plot_result2 = []
# plot_model_name = []
#
#
# # ==============================================================================
# # 共用一个mask
# plus = 86400000
# mask_NSedge = 0
#
# keyword=['pictrl']
# s_data_pictrl = main_fun_avg_in_fun([1, 151], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM-control_')
#
#
# mask_NSedge = 0
#
#
#
# keyword=['2XCO']
# plot_model_name.append(keyword)
# s_data = main_fun([1, 500], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
# plot_result2.append([s_data_pictrl , [np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]]])
#
# keyword=['4XCO']
# plot_model_name.append(keyword)
# s_data = main_fun([1, 1000], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
# plot_result2.append([s_data_pictrl , [np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]]])
#
# keyword=['1pctCO']
# plot_model_name.append(keyword)
# s_data = main_fun([151, 200], r'D:\test_zhu\Data\CESM\Data-Pr/', 'CESM_', p1p2p3=False)
# plot_result2.append([s_data_pictrl , [np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]]])
#
#
# plus = 86400
#
# for keyword in [['ACCESS-CM2'], ['ACCESS-ESM1-5'], ['CanESM5']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
#     s_result.append(s_data)
#
#     mask_NSedge = 0
#     s_data = main_fun_ssp([2015, 2300], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#     plot_result2.append(s_result)
#
# for keyword in [['CESM2']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
#     s_result.append(s_data)
#
#     mask_NSedge = 0
#     s_data = main_fun_ssp([2015, 2299], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#     plot_result2.append(s_result)
#
# for keyword in [ ['IPSL'],
#  ['MRI']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg_in_fun([1900, 1999], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585\hist/', 'CMIP6-hist-control_')
#     s_result.append(s_data)
#
#     mask_NSedge = 0
#     s_data = main_fun_ssp([2015, 2300], r'D:\test_zhu\Data\CMIP6\Data-Pr-SSP585/ssp585/', 'CMIP6-ssp585_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#     plot_result2.append(s_result)
# # ==============================================================================
#
# for keyword in [['MPIESM11'],['GISSE2R'],['CESM104']]:
#     plot_model_name.append(keyword)
#     s_result = []
#
#     mask_NSedge = 0
#     s_data = main_fun_avg(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr_avg/', 'long-term-control_')
#     s_result.append(s_data)
#     mask_NSedge = 0
#     s_data = main_fun_all_file(r'D:\test_zhu\Data\CMIP6-long-term\Data-Pr/', 'long-term_')
#     s_result.append([np.nanmean(s_data[0][-50:], axis=0), s_data[1], s_data[2]])
#
#     plot_result2.append(s_result)
#
#
# import matplotlib
# SMALL_SIZE=9
#
# plt.rc('axes', titlesize=SMALL_SIZE)
# plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
# plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
# plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('axes', titlepad=1, labelpad=1)
# plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
# plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('xtick.major', size=2, width=0.5)
# plt.rc('xtick.minor', size=1.5, width=0.2)
# plt.rc('ytick.major', size=2, width=0.5)
# plt.rc('ytick.minor', size=1.5, width=0.2)
# plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
# plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize
#
#
#
#
# def northlat(data_moment, long, lat,mask_NSedge=0):
#
#     area_weight = cal_area(long, lat)
#
#     long = xr.where(
#         long > 180,
#         long - 360,
#         long)
#     long, lat = np.meshgrid(long, lat)
#     # year loop
#     land_mask = global_land_mask.is_land(lat, long)
#     sea_mask = global_land_mask.is_ocean(lat, long)
#
#     prect_mask = data_moment<2.5
#
#
#     def characteristics(mask, create_mask=0):
#         lat_now = lat.copy()
#         lat_now[~mask] = -999
#
#         if mask_NSedge:
#             a_add = []
#             lat1 = np.nanmax(lat_now, axis=0)
#             a_add.append(lat1 == np.max([lat[0, 0], lat[-1, 0]]))
#             lat1[a_add[0]]=np.nan
#
#             lat1 = np.nanmean(lat1)
#
#             lat2 = np.nanmin(lat_now, axis=0)
#
#             a_add.append(lat2 == np.min([lat[0, 0], lat[-1, 0]]))
#             lat2[a_add[1]] = np.nan
#             lat2 = np.nanmean(lat2)
#
#             if create_mask:
#                 global mask_NSedge_array
#                 mask_NSedge_array = a_add
#             plt.imshow(mask)
#             plt.show()
#             plt.close()
#         else:
#             # plt.imshow(np.array(mask_NSedge_array))
#             # plt.show()
#             # plt.close()
#             lat1 = np.nanmax(lat_now, axis=0)
#
#
#         return lat1
#     # for all
#
#
#     index_all = characteristics(prect_mask, 1)
#
#     return index_all
#
# def plot_contour(data):
#     from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
#     import cartopy.crs as ccrs
#     fig, axs = plt.subplots(4, 3, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)},
#                             figsize=[12,6])
#     plt.subplots_adjust(top=0.98,
# bottom=0.03,
# left=0.05,
# right=0.99,
# hspace=0.3,
# wspace=0.2)
#     levels = [2.5]
#
#     northlat_obs = []
#     northlat_hist = []
#
#     for i_ax in range(4):
#         for j_ax in range(3):
#             i = i_ax*3+j_ax
#             print(len(plot_result[i]))
#             data_hist, data_obs = plot_result[i]
#             # data_hist = plot_result[i][0]
#             # data_obs = plot_result[i][1]
#             cl = axs[i_ax][j_ax].contour(data_hist[1], data_hist[2], data_hist[0], levels=levels, transform=ccrs.PlateCarree(),
#                                 colors=['black'],
#                                 linewidths=1)
#
#             northlat1 = northlat(data_hist[0], data_hist[1], data_hist[2])
#             np.savetxt('%s_hist.txt'%name_rank[i], northlat1)
#             cl = axs[i_ax][j_ax].contour(data_obs[1], data_obs[2], data_obs[0],  levels=levels, transform=ccrs.PlateCarree(),
#                                 colors=['red'],
#                                 linewidths=1)
#             northlat1 = northlat(data_obs[0], data_obs[1], data_obs[2])
#             print(northlat1)
#             np.savetxt('%s_obs.txt'%name_rank[i], northlat1)
#
#             data_hist, data_obs = plot_result2[i]
#             # data_hist = plot_result[i][0]
#             # data_obs = plot_result[i][1]
#             cl = axs[i_ax][j_ax].contour(data_hist[1], data_hist[2], data_hist[0], levels=levels, transform=ccrs.PlateCarree(),
#                                 colors=['black'],
#                                 linewidths=0.8)
#
#             cl = axs[i_ax][j_ax].contour(data_obs[1], data_obs[2], data_obs[0],  levels=levels, transform=ccrs.PlateCarree(),
#                                 colors=['red'],
#                                 linewidths=1)
#
#             # axs[i].clabel(cl)
#             axs[i_ax][j_ax].set_title(figlabelstr[i]+' '+name_rank[i],loc='left')
#             axs[i_ax][j_ax].coastlines(linewidth=0.3)
#             axs[i_ax][j_ax].gridlines(crs=ccrs.PlateCarree(),ylocs=[50,40, 10,-10, -40,-50], xlocs=[], linestyle='--')
#             # a = circulation(axs[i], lon=np.arange(100, 360, 0.5), lat=np.arange(90, -90.5, -0.5), ranges=[[0,359,45,-45],
#             # [0,360,90],[45,-46,-15]])
#             # a.lat_lon_shape()
#             axs[i_ax][j_ax].set_xticks([0,90,180,270,0], crs=ccrs.PlateCarree())
#             axs[i_ax][j_ax].set_yticks(np.arange(60,-61,-20), crs=ccrs.PlateCarree())
#             axs[i_ax][j_ax].set_extent([0,359,60,-60], ccrs.PlateCarree())
#             lon_formatter = LongitudeFormatter(zero_direction_label=True)
#             lat_formatter = LatitudeFormatter()
#             axs[i_ax][j_ax].xaxis.set_major_formatter(lon_formatter)
#             axs[i_ax][j_ax].yaxis.set_major_formatter(lat_formatter)
#     # plt.show()
#     # plt.savefig('spatial_distribution.png', dpi=400)
#     northlat_hist = np.array(northlat_hist)
#     northlat_obs = np.array(northlat_obs)
#     print(northlat_obs)
#     print(northlat_hist)
#     np.savetxt('northlat_hist.txt', northlat_hist)
#     np.savetxt('northlat_obs.txt', northlat_obs)
#
# plot_contour(1)
# exit(0)






# a = xr.open_dataset(r'index_CMIP6_ssp585_ACCESS-CM2_N.nc')
# print(a)
# var = 'lat1'
# print(a[var].values[:,0])


# plt.plot(a[var].values[:,0], label='all')
# plt.plot(a[var].values[:,1], label='land')
# plt.plot(a[var].values[:,2], label='sea')
# plt.legend()
# plt.show()

import matplotlib
SMALL_SIZE=5

plt.rc('axes', titlesize=SMALL_SIZE)
plt.rc('font',size=SMALL_SIZE)#controlsdefaulttextsizes
plt.rc('lines',linewidth=0.5)#controlsdefaulttextsizes
plt.rc('axes',titlesize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('axes', titlepad=1, labelpad=1)
plt.rc('axes',labelsize=SMALL_SIZE)#fontsizeoftheaxestitle
plt.rc('xtick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('xtick.major', size=2, width=0.5)
plt.rc('xtick.minor', size=1.5, width=0.2)
plt.rc('ytick.major', size=2, width=0.5)
plt.rc('ytick.minor', size=1.5, width=0.2)
plt.rc('ytick',labelsize=SMALL_SIZE, direction='in')#fontsizeoftheticklabels
plt.rc('legend',fontsize=SMALL_SIZE)#legendfontsize




path = os.listdir(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/')
for i_path in path:
    if i_path[-4] == 'N':
        var_NS = 'north'
    if i_path[-4] == 'S':
        var_NS = 'south'
        continue

    a = xr.open_dataset(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\result/'+
                        i_path)

    land_sea_name = ['ALL', 'Land', 'Sea']

    for i_var in list(a.data_vars):
        for land_sea in range(3):

            control = 'obs'
            if get_overlap(i_path, 'control')=='control':
                control = 'ctl'
            file = r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp'+'/'+\
                   land_sea_name[land_sea]+'/'+\
                   control+'/'+\
                   i_path.split('_')[2]+'/'
            os.makedirs(file, exist_ok=True)
            try:
                np.savetxt(file + r'%s.txt'%(var_NS+i_var), a[i_var].values[:, land_sea])
            except:
                np.savetxt(file + r'%s.txt'%(var_NS+i_var), [a[i_var].values[land_sea]])



