import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


obs_rank = ['2XCO', '4XCO','1pctCO',
            'ACCESS-CM2', 'ACCESS-ESM1-5', 'CanESM5', 'CESM2', 'IPSL', 'MRI',
            'MPIESM11', 'GISSE2R', 'CESM104']
hist_rank = ['pictrl', 'pictrl','pictrl',
            'ACCESS-CM2', 'ACCESS-ESM1-5', 'CanESM5', 'CESM2', 'IPSL', 'MRI',
            'MPIESM11', 'GISSE2R', 'CESM104']

name_rank = ['CESM1 2xCO2', 'CESM1 4xCO2','CESM1 1%CO2',
            'ACCESS-CM2 SSP5-8.5', 'ACCESS-ESM1-5 SSP5-8.5', 'CanESM5 SSP5-8.5', 'CESM2-WACCM SSP5-8.5',
             'IPSL-CM6A-LR SSP5-8.5', 'MRI-ESM2-0 SSP5-8.5',
            'MPI-ESM-1.1 4xCO2', 'GISS-E2-R 4xCO2', 'CESM1.0.4 4xCO2']

# prepare data
result = []
labels1 = ''
labels2 = ''
for i in range(len(name_rank)):
    if i%2==1:

        labels1+='%i = %s    \n'%(i+1, name_rank[i])
    else:
        labels2+='%i = %s    \n'%(i+1, name_rank[i])


    hist = np.loadtxt(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\ALL\ctl/'+hist_rank[i]+'/northlat1.txt')

    obs = np.loadtxt(r'D:\OneDrive\basis\some_projects\zhj\model_p1p2p3evp\ALL\obs/' + obs_rank[i] + '/northlat1.txt')

    dt = np.loadtxt(r'D:\test_zhu\dT/' + obs_rank[i] + '/dT.txt')

    param_a = (obs-hist)/dt
    param = np.mean(np.array(param_a[-50:]))
    result.append(param)
fig, ax = plt.subplots(figsize=[5,4.8])
plt.subplots_adjust(top=0.93,
bottom=0.275,
left=0.15,
right=0.97,
hspace=0.2,
wspace=0.2)
plt.bar(np.arange(1,12+1), result, color='black', label='N.B.', width=0.5)
plt.title('Northern Boundary Change of Dry Zone in N.H.')
plt.axhline(0, color='grey')
plt.ylabel('Latitude Change(°/K)')
plt.xticks(np.arange(1,12+1))
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
ax = plt.gca()
ax.yaxis.set_minor_locator(MultipleLocator(0.02))

plt.text(0.1, 0.005, labels2, color="black", transform=plt.gcf().transFigure)
plt.text(0.6, 0.005, labels1, color="black", transform=plt.gcf().transFigure)
plt.show()
plt.savefig(r'Northern Boundary Change.png', dpi=400)




