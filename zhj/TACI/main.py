import pandas as pd
from pandas import DataFrame
import numpy as np


def de_region(varible_name):
    sdata = pd.read_csv(r'D:\basis\TACI\rowdata\data %s.csv' % varible_name, index_col=1)
    # sdata = pd.read_csv(r'data2016 lowtem.csv', index_col=1)
    # sdata = sdata.set_index(pd.to_datetime(sdata.index))
    del sdata.index.name
    station = pd.read_csv(r'D:\basis\data\region\name_all_station.csv', index_col=0)
    station['number'].groupby(station.index).apply(lambda x: sdata.loc[x].to_csv(r'D:\basis\TACI\province_data'
                                                                                 r'\regiondata_%s.csv'%(varible_name + x.name)))

    # data.loc[region_station.index.map(int)].to_csv('region_data %s.csv' % (varible_name+i))

def findarea(varible_name, usenumber):
    # 一些特殊值的处理，可以不用管，自动按照变量名处理
    def else_analysis(data, varible_name):
        if varible_name == 'TEM':
            data['tem'][data['tem'] == 32766] = np.nan
            data['tem'] = data['tem'] / 10
        if varible_name == 'WIND':
            data['wind'][data['wind'] == 32766] = np.nan
            data['wind'][data['wind'] > 1000] = np.array(data['wind'][data['wind'] > 1000]) - 1000
            data['wind'] = data['wind'] / 10
        if varible_name == 'PRE':
            data['pre'][data['pre'] == 32766] = np.nan
            data['pre'][data['pre'] == 32700] = 0.9
            data['pre'][data['pre'] > 32000] = np.array(data['pre'][data['pre'] > 32000]) - 32000
            data['pre'][data['pre'] > 31000] = np.array(data['pre'][data['pre'] > 31000]) - 31000
            data['pre'][data['pre'] > 30000] = np.array(data['pre'][data['pre'] > 30000]) - 30000
            data['pre'] = data['pre'] / 10
        return data

    data = pd.read_csv(r'D:\basis\data\%s.txt' % varible_name.upper(), delim_whitespace=True, header=None,
                       index_col=0, usecols=[0, 4, 5, 6, usenumber], names=['station', 'year', 'month', 'day', '%s' %varible_name])
    datatime = pd.to_datetime(data['year'] * 10000 + data['month'] * 100 + data['day'], format='%Y%m%d')
    sdata = DataFrame({'station': np.array(data.index),'%s' %varible_name:np.array(data['%s' %varible_name])}, index=datatime)
    sdata = else_analysis(sdata, varible_name.upper())
    nonandata = sdata['%s' %varible_name].groupby(sdata['station']).resample('D').asfreq().interpolate(method='linear')
    nonandata = nonandata.reset_index().set_index('level_1').to_csv('data %s.csv' % varible_name)


def find_province_station():
    province = pd.read_csv(r'D:\basis\data\province_name_and_number.csv', index_col=1)
    ff = pd.read_csv(r'D:\basis\data\all_station.csv', index_col=0)
    a = province.loc[ff.index].dropna()
    a.to_csv('name_all_station.csv')
    # a = list(set(province).intersection(set(np.array(ff.index))))
    # ff.loc[a].to_csv(r'D:\basis\data\region\%s.csv' % province_name)

if __name__ == '__main__':
    # findarea('wind', 7)
    # findarea('tem', 8)

    de_region('hightem')
    # de_region('tem', ['fujian'])
    # findarea('pre', 9)
    # de_region('pre',['fujian'])

    #find 2016 data to ZhengZhou a temporal map
    # varible_name=['wind', 'pre', 'lowtem', 'hightem']
    # for i in varible_name:
    #     sdata = pd.read_csv(r'D:\basis\TACI\rowdata\data %s.csv' % i, index_col=0)
    #     sdata = sdata.set_index(pd.to_datetime(sdata.index))
    #     del sdata.index.name
    #     print(sdata)
    #     sdata['2016'].to_csv('data2016 %s.csv' % i)
