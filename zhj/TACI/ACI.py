import pandas as pd
from pandas import DataFrame, Series
import numpy as np
import plot as pl

'''
负责计算所有差值的标准化 
For the purpose of combining the six components, the monthly differences versus the reference period
are divided by the reference period standard deviation.
'''


def ACI(province_name, seasons=False):

    def basis(varible_name):
        sdata = pd.read_csv(r'D:\basis\TACI\province_data\regiondata_%s.csv' % (varible_name+province_name)
                            , index_col=1, header=None, skiprows=1, names=['station', varible_name])
        sdata = sdata.set_index(pd.to_datetime(sdata.index))
        del sdata.index.name
        return sdata
    def tembasis(varible_name):
        sdata = pd.read_csv(r'D:\basis\TACI\province_data\regiondata_%s.csv' % (varible_name+province_name)
                            , index_col=1, header=None, skiprows=1, names=['station', 'tem'])
        sdata = sdata.set_index(pd.to_datetime(sdata.index))
        del sdata.index.name
        return sdata
    if seasons is not False:
        D = Drought(basis('pre')['1960-03':'2017-02'], seasons=True)
        P = precipitation(basis('pre')['1961-03':'2016-02'], seasons=True)
        T10 = lowtem(tembasis('lowtem')['1961-03':'2016-02'], seasons=True)
        T90 = hightem(tembasis('hightem')['1961-03':'2016-02'], seasons=True)
        W = wind(basis('wind')['1961-03':'2016-02'], seasons=True)
        aci = np.array(T90['tem']) - np.array(T10['tem']) + np.array(D['pre']) + np.array(P['pre']) + np.array(
            W['wind'])
        aci = DataFrame({'aci': aci}, index=T90.index)
    else:
        D = Drought(basis('pre')['1960':'2017'])
        P = precipitation(basis('pre')['1961':'2016'])
        T10 = lowtem(tembasis('lowtem')['1961':'2016'])
        T90 = hightem(tembasis('hightem')['1961':'2016'])
        W = wind(basis('wind')['1961':'2016'])
        aci = np.array(T90['tem']) - np.array(T10['tem']) + np.array(D['pre']) + np.array(P['pre']) + np.array(W['wind'])
        aci = DataFrame({'aci': aci}, index=T90.index)
    result = {'T90':T90, 'T10':T10, 'P':P, 'D':D, 'W':W}
    return aci, result


def precipitation(data, seasons=False):
    # if seasons is not False:
    #     r5y = (data['pre'].groupby(pd.Grouper(freq='3M')) \
    #         .apply(lambda val: val.rolling(5).sum().max())).dropna()
    r5y = (data['pre'].groupby(pd.Grouper(freq='M')) \
           .apply(lambda val: val.rolling(5).sum().max())).dropna()
    r5yref = r5y['1961':'1990'].groupby([r5y['1961':'1990'].index.strftime('%m')]).mean()
    stdr5yref = r5y['1961':'1990'].groupby([r5y['1961':'1990'].index.strftime('%m')]).std()
    reindex = list(r5yref.index.map(int))
    r5yref_l = Series(np.array(r5yref), index=reindex)
    stdr5yref_l = Series(np.array(stdr5yref), index=reindex)
    or5y = r5y.groupby([r5y.index.year, r5y.index.month]) \
        .apply(lambda x: (x - r5yref_l[x.name[1]]) / stdr5yref_l[x.name[1]]).reset_index().set_index('index')
    return or5y


def Drought(data, seasons=False):
    D = data.groupby(data.index.year).apply(lambda x: find5con(x)).drop(['station'], axis=1)
    D = D.set_index(pd.period_range(D.index.min(), D.index.max(), freq='A-DEC'))
    D = D.shift(1)
    D = D.resample('M', kind='period').asfreq().interpolate(method='linear')
    D = D.shift(-1)['1961':'2016']
    r5yref = D['1961':'1990'].groupby([D['1961':'1990'].index.strftime('%m')]).mean()
    stdr5yref = D['1961':'1990'].groupby([D['1961':'1990'].index.strftime('%m')]).std()
    reindex = list(r5yref.index.map(int))
    r5yref_l = Series(np.array(r5yref).flatten(), index=reindex)
    stdr5yref_l = Series(np.array(stdr5yref).flatten(), index=reindex)
    oT10 = D.groupby([D.index.year, D.index.month]) \
        .apply(lambda x: (x - r5yref_l[x.name[1]]) / stdr5yref_l[x.name[1]]).reset_index().set_index('index')
    return oT10


def find5con(data):
    data = data.reset_index()
    data['pre'][data['pre'] < 1] = 0
    r = data['pre'].eq(0)
    r = data['station'] * r
    a = r.ne(r.shift()).cumsum()
    mask = data['pre'].eq(0)
    Dstd = (data[mask].groupby(['station', a[mask]]).size().max(level=0)
            .reindex(data['station'].unique(), fill_value=0)
            .reset_index(name='pre')).max()
    return Dstd


def lowtem(data, seasons=False):
    a = data['1961':'1990'].groupby([data['1961':'1990'].index.strftime('%m')])['tem'].quantile(0.1)
    reindex = list(a.index.map(int))
    a_1 = Series(np.array(a), index=reindex)

    T10 = data['tem'].groupby([data.index.year, data.index.month]) \
        .apply(lambda x: ((x < a_1[x.name[1]]).sum()) / len(x)).reset_index()
    T10 = T10.set_index(pd.to_datetime(T10['level_0'] * 10000 + T10['level_1'] * 100 + 1, format='%Y%m%d')) \
        .drop(['level_0', 'level_1'], axis=1)

    r5yref = T10['1961':'1990'].groupby([T10['1961':'1990'].index.strftime('%m')]).mean()
    stdr5yref = T10['1961':'1990'].groupby([T10['1961':'1990'].index.strftime('%m')]).std()
    reindex = list(r5yref.index.map(int))
    r5yref_l = Series(np.array(r5yref).flatten(), index=reindex)
    stdr5yref_l = Series(np.array(stdr5yref).flatten(), index=reindex)
    oT10 = T10.groupby([T10.index.year, T10.index.month]) \
        .apply(lambda x: (x - r5yref_l[x.name[1]]) / stdr5yref_l[x.name[1]]).reset_index().set_index('index')
    return oT10


def hightem(data, seasons=False):
    # r5y = (data['tem'].groupby(pd.Grouper(freq='3M')) \
    #        .apply(lambda val: print(val['1961'])))
    a = data['1961':'1990'].groupby([data['1961':'1990'].index.strftime('%m')])['tem'].quantile(0.9)
    reindex = list(a.index.map(int))
    a_1 = Series(np.array(a), index=reindex)

    T10 = data['tem'].groupby([data.index.year, data.index.month]) \
        .apply(lambda x: ((x > a_1[x.name[1]]).sum()) / len(x)).reset_index()
    T10 = T10.set_index(pd.to_datetime(T10['level_0'] * 10000 + T10['level_1'] * 100 + 1, format='%Y%m%d')) \
        .drop(['level_0', 'level_1'], axis=1)

    r5yref = T10['1961':'1990'].groupby([T10['1961':'1990'].index.strftime('%m')]).mean()
    stdr5yref = T10['1961':'1990'].groupby([T10['1961':'1990'].index.strftime('%m')]).std()
    reindex = list(r5yref.index.map(int))
    r5yref_l = Series(np.array(r5yref).flatten(), index=reindex)
    stdr5yref_l = Series(np.array(stdr5yref).flatten(), index=reindex)
    oT10 = T10.groupby([T10.index.year, T10.index.month]) \
        .apply(lambda x: (x - r5yref_l[x.name[1]]) / stdr5yref_l[x.name[1]]).reset_index().set_index('index')
    return oT10


# 风速为m3， 且在‘wind’行。 index为datatime格式。
def wind(data, seasons=False):
    data['wind'] = (np.array(data['wind']) ** 3) * 0.5 * 1.23
    a = data['1961':'1990'].groupby([data['1961':'1990'].index.strftime('%m')])['wind'] \
        .apply(lambda x: x.mean() + (1.28 * x.std()))
    reindex = list(a.index.map(int))
    a_1 = Series(np.array(a), index=reindex)

    T10 = data['wind'].groupby([data.index.year, data.index.month]) \
        .apply(lambda x: ((x > a_1[x.name[1]]).sum()) / len(x)).reset_index()
    T10 = T10.set_index(pd.to_datetime(T10['level_0'] * 10000 + T10['level_1'] * 100 + 1, format='%Y%m%d')) \
        .drop(['level_0', 'level_1'], axis=1)

    r5yref = T10['1961':'1990'].groupby([T10['1961':'1990'].index.strftime('%m')]).mean()
    stdr5yref = T10['1961':'1990'].groupby([T10['1961':'1990'].index.strftime('%m')]).std()
    reindex = list(r5yref.index.map(int))
    r5yref_l = Series(np.array(r5yref).flatten(), index=reindex)
    stdr5yref_l = Series(np.array(stdr5yref).flatten(), index=reindex)
    oT90 = T10.groupby([T10.index.year, T10.index.month]) \
        .apply(lambda x: (x - r5yref_l[x.name[1]]) / stdr5yref_l[x.name[1]]).reset_index().set_index('index')
    return oT90


if __name__ == '__main__':
    def findcolors(value):
        colors = np.ones(len(value))
        colors[np.where(value >= 2)] = 0
        colors[np.where((value >= 1.5) & (value[:] <= 1.99))] = 1
        colors[np.where((value >= 1) & (value[:] <= 1.49))] = 2
        colors[np.where((value >= 0.5) & (value[:] <= 0.99))] = 3
        colors[np.where((value >= 0.25) & (value[:] <= 0.49))] = 4
        colors[np.where((value >= -0.24) & (value[:] <= 0.24))] = 5
        colors[np.where((value <= -0.25) & (value[:] >= -0.49))] = 6
        colors[np.where((value <= -0.5) & (value[:] >= -0.99))] = 7
        colors[np.where((value <= -1) & (value[:] >= -1.49))] = 8
        colors[np.where((value <= -1.5) & (value[:] >= -1.99))] = 9
        colors[np.where(value[:] <= -2)] = 10
        return colors


    # aci = ACI()
    # aci.to_csv('aci.csv')
    # 实验季节
    # sdata = pd.read_csv('region_data hightem.csv', index_col=1)
    # sdata = sdata.set_index(pd.to_datetime(sdata.index))
    # data = DataFrame({'year':np.array(sdata.index.year),
    #                   'month':np.array(sdata.index.month),
    #                   'day':np.array(sdata.index.day),
    #                   'pre':sdata['pre'],
    #                   'station': sdata['station']})
    # data['month'][(data['month'] == 3) | (data['month'] == 4) | (data['month'] == 5)] = 1
    # data['month'][(data['month'] == 5) | (data['month'] == 6) | (data['month'] == 7)] = 2
    # data['month'][(data['month'] == 8) | (data['month'] == 9) | (data['month'] == 10)] = 3
    # data['month'][(data['month'] == 11) | (data['month'] == 12) | (data['month'] == 1)] = 4
    # del sdata.index.name
    # T10 = hightem(sdata['1961-03':'2017-02'])
    # import matplotlib.pyplot as plt
    # pl.plot_bar(plt.subplot(111), T10, 'pre', 'drought')
    # r5yref = T10.groupby([T10.index.strftime('%m')])\
    #     .apply(lambda x: pl.plot_bar(plt.subplot(111), x, 'pre', '%s' % x.name))



    # aci = pd.read_csv('aci.csv', index_col=0)
    # aci = aci.set_index(pd.to_datetime(aci.index))
    # import matplotlib.pyplot as plt
    # #画全年ACI
    # pl.plot_bar(plt.subplot(111), aci, 'aci', 'ACI')
    # #单月的ACI
    # r5yref = aci.groupby([aci.index.strftime('%m')])\
    #     .apply(lambda x: pl.plot_bar(plt.subplot(111), x, 'aci', '%s' % x.name))

    # a = pd.read_csv('sese.csv', index_col=0)
    # pl.plot_region(a)

    # station = pd.read_csv(r'D:\basis\data\region\name_all_station.csv', index_col=0)
    #     #     # data2016 = DataFrame()
    #     #     # for i in station.index.unique():
    #     #     #     aci, all = ACI(i)
    #     #     #     data2016['%s' %i] = np.array(aci['aci'])
    #     #     # data2016.to_csv('data.csv')


    sdata = pd.read_csv('aci_data.csv', index_col=0)
    sdata = sdata.set_index(pd.to_datetime(sdata.index))
    data = sdata['2016']

    aa = []
    for i in data.columns:
        aa.append(float(data[i].mean()))

    aci = DataFrame(findcolors(np.array(aa)), index=data.columns, columns=['color'])
    print(aci)
    pl.plot_region(aci, 'ACI')
    # aci = aci.T
    # for i in range(12):
    #     colorn = DataFrame(aci[i])
    #     colorn.columns=['color']
    #     pl.plot_region(colorn, str(i+1)+'月')
