import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from pandas import Series
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False

def plot_bar(ax, xseries, xname, yseries, savename , moving_ave=False, set_blank=False, ylim=None):
    def fast_moving_average(x, N):
        return np.convolve(x, np.ones((N,)) / N)[(N - 1):]

    if ylim is not None:
        ax.set_ylim([np.floor(yseries.min()), ylim])
        ax_c = ax.twinx()
        ax_c.set_ylim([np.floor(yseries.min()), ylim])
    else:
        ax.set_ylim([np.floor(yseries.min()), np.round(yseries.max())])
        ax_c = ax.twinx()
        ax_c.set_ylim([np.floor(yseries.min()), np.round(yseries.max())])

    ax.bar(xseries, yseries, color=(Series(yseries) > 0).map({True: '#ff5c5c', False: '#6cbfff'}))
    if moving_ave is not False:
        ax.plot(xseries, fast_moving_average(yseries, moving_ave),
                color='black', alpha=0.5)

    # 设置颜色块
    def set_colorblank(xylist, facecolors):
        # xylist:left low,  left high,  right high , right low
        codes = [Path.MOVETO] + [Path.LINETO] * 3 + [Path.CLOSEPOLY]
        vertices = [xylist[0], xylist[1], xylist[2], xylist[3], xylist[4]]
        vertices = np.array(vertices, float)
        path = Path(vertices, codes)
        pathpatch = PathPatch(path, facecolor=facecolors, edgecolor=facecolors, alpha=0.2)
        ax.add_patch(pathpatch)
    if isinstance(set_blank, dict) == True:
        for i in set_blank:
            set_colorblank(set_blank[i], i)


    ax.spines['top'].set_color('none')
    ax_c.spines['top'].set_color('none')
    ax.grid(axis='y', alpha=0.4)
    ax.set_xticks(ax.get_xticks()[1:-1])
    x = list(Series(ax.get_xticks()).map(int))
    ax.set_xticklabels(xname[x])
    # ax.set_xticklabels(['1961', '1969', '1977', '1986', '1994', '2002', '2011'])
    ax.set_title('%s'%savename)
    plt.show()
    # plt.savefig('%s.png' %save )
    plt.close()
    return


def plotcors(ax, data, oslon, oslat, labels=None, ranges=[[89.5, -89.5, 0.5, 359.5], [89.5, -89.5], [0.5, 359.5]]
             , Ldensity=8):
    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    from matplotlib import colors
    from copy import copy
    from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER
    import shapely.geometry as sgeom
    from scipy.interpolate import Rbf, RegularGridInterpolator
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader
    # 适用于data画曲线图、r2画显著性的图
    colormap = ['#ab0f45', '#de4c4b', '#f06744', '#f7814c', '#fca55d', '#fec877', '#fee18d','#fdfebb', '#ffffff', '#fdfebb', '#ddf19a', '#bce4a0',
                '#a2d9a4', '#74c7a5', '#5cb7aa', '#3b92b9', '#555aa7']
    tmap = colors.LinearSegmentedColormap.from_list('cmap', colormap)

    fill = ax.contourf(oslon, oslat, data, Ldensity, transform=ccrs.PlateCarree(), colormaps=tmap)
    cbar = plt.colorbar(fill)
    # cbar.set_ticks(levels)
    # cbar.set_ticklabels(levels)
    # clevels = []
    # filllevels = fill.levels
    # for i in range(0, len(filllevels), 1):
    #     clevels.append(filllevels[i])
    # ax.clabel(fill, clevels, fmt='%6.1f')
    # ax.coastlines()


    # 设置坐标轴经纬度标志
    ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
    ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
    ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    # 添加网格线
    ax.gridlines(color='black', linestyle='--', alpha=0.4)

    if labels is not None:
        ax.text(ranges[0][2],56, labels,
                 transform=ccrs.PlateCarree())

    # # #添加shp文件进入图片中
    #
    pathss = shp2clip(r'D:\basis\data\shp\shengjie\shengjie.shp')
    plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
    upath = PathPatch(pathss, transform=plate_carre_data_transform)
    for collection in fill.collections:
        collection.set_clip_path(upath)

    shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries(), ccrs.PlateCarree(),
                                   edgecolor='k',facecolor='none', linewidths=0.55)
    ax.add_feature(shape_feature)
    plt.show()
    return


def shp2clip(shpfile):
    import shapefile
    from matplotlib.path import Path
    from matplotlib.patches import PathPatch
    sf = shapefile.Reader(shpfile)
    vertices = []
    codes = []
    for shape_rec in sf.shapeRecords():
        pts = shape_rec.shape.points
        prt = list(shape_rec.shape.parts) + [len(pts)]
        for i in range(len(prt) - 1):
            for j in range(prt[i], prt[i+1]):
                vertices.append((pts[j][0], pts[j][1]))
            codes += [Path.MOVETO]
            codes += [Path.LINETO] * (prt[i+1] - prt[i] - 2)
            codes += [Path.CLOSEPOLY]
        clip = Path(vertices, codes)
    return clip
