import numpy as np
import matplotlib.pyplot  as plt
import proplot as plot
a = np.loadtxt('factors.txt').T
a = np.ma.masked_equal(a, -999)
# a[np.where(a>200)] = 600
type1 = np.ma.masked_where((a<100)|(a>=200), a)
print(type1.shape)
type2 = np.ma.masked_where((a<200)|(a>=300), a)
type3 = np.ma.masked_where((a<300)|(a>=400), a)
type4 = np.ma.masked_where((a<400)|(a>=500), a)

lons = np.arange(69.75,140.5,0.25)
lats = np.arange(14.75,55.5,0.25)
import cartopy.crs as ccrs


# 改变颜色
def change_color():
    from matplotlib.colors import LinearSegmentedColormap
    interval = np.hstack([np.linspace(0.3, 1)])
    colors = plt.cm.Purples(interval)
    Purple = LinearSegmentedColormap.from_list('name', colors)

    colors = plt.cm.Greens(interval)
    Greens = LinearSegmentedColormap.from_list('name', colors)

    interval = np.hstack([np.linspace(0, 0.6)])
    colors = plt.cm.Wistia(interval)
    Wistia = LinearSegmentedColormap.from_list('name', colors)

    interval = np.hstack([np.linspace(0.6, 1)])
    colors = plt.cm.Reds(interval)
    Oranges = LinearSegmentedColormap.from_list('name', colors)
    return Purple, Greens, Wistia, Oranges

Purple, Greens, Wistia, Oranges = change_color()


fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105),figsize=[6,6],tight=False
                        , left='1em', top='2em', bottom='6em')
ax = ax[0]

def plots(ax):
    cb = ax.pcolormesh(lons,lats, type1, transform=ccrs.PlateCarree(),
                       cmap=Purple, vmin=type1.min(), vmax=type1.max())
    cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
    cb = plt.colorbar(cb, cax = cbaxes, orientation="horizontal", ticks=[100,105,110])

    cb = ax.pcolormesh(lons,lats, type2, transform=ccrs.PlateCarree(),
                       cmap=Greens, vmin=type2.min(), vmax=type2.max())
    cbaxes = fig.add_axes([0.29, 0.09, 0.2, 0.01])
    cb = plt.colorbar(cb, cax = cbaxes, orientation="horizontal", ticks=[200,205,210])

    cb = ax.pcolormesh(lons,lats, type3, transform=ccrs.PlateCarree(),
                       cmap=Wistia)
    cbaxes = fig.add_axes([0.53, 0.09, 0.2, 0.01])
    cb = plt.colorbar(cb, cax = cbaxes, orientation="horizontal", ticks=[300,305,310])

    cb = ax.pcolormesh(lons,lats, type4, transform=ccrs.PlateCarree(),
                       cmap=Oranges)
    cbaxes = fig.add_axes([0.775, 0.09, 0.2, 0.01])
    cb = plt.colorbar(cb, cax = cbaxes, orientation="horizontal", ticks=[400,405,410])
    from plot_in_proplot import shp_clip_for_proplot
    shp_clip_for_proplot(ax, clipshape=False)

def plotss(ax):
    cb = ax.pcolormesh(lons,lats, type1, transform=ccrs.PlateCarree(),
                       cmap=Purple, vmin=type1.min(), vmax=type1.max())

    cb = ax.pcolormesh(lons,lats, type2, transform=ccrs.PlateCarree(),
                       cmap=Greens, vmin=type2.min(), vmax=type2.max())

    cb = ax.pcolormesh(lons,lats, type3, transform=ccrs.PlateCarree(),
                       cmap=Wistia)

    cb = ax.pcolormesh(lons,lats, type4, transform=ccrs.PlateCarree(),
                       cmap=Oranges)
    ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
    from plot_in_proplot import shp_clip_for_proplot
    shp_clip_for_proplot(ax, clipshape=False)

plots(ax)
ax.format(latlim=[17, 54], lonlim=[80, 127])

ax2 = fig.add_axes([0.2, 0.5, 0.2, 0.2], projection=ccrs.AlbersEqualArea(central_longitude=105))
plotss(ax2)
# ax2.format(latlim=[17, 54], lonlim=[80, 127])


from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

ip = InsetPosition(ax, [0, -0.045, 0.15, 0.3])
ax2.set_axes_locator(ip)


plt.savefig('result.jpg', dpi=300)
# f = a[np.where((a<200)& (a>=100))]
# print(f.min(), f.max())