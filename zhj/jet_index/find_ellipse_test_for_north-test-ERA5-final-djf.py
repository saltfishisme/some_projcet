# threshold the image to reveal light regions in the
# blurred image
def find_highlight(data, lat, lon, resolution, given_threshold, numll_min=10):
    '''
    :param data: np.array, 2D data.
    :param lat, lon, resolution: lat(list or array-like), lon(list or array-like) and resolution(float) of the data.
    :param given_threshold: float, the minimum value which can be marked as 'high_value'.
    :param numll_min: float, the points in one high_value region should be more than numll_min, otherwise, this region
                             will be filtered.

    :return: array, shape is [the number of high_value area, 7],
            the first five indexes are  x_centre, y_centre, width, height, angle,
            The sixth and seventh indices are area of fitted ellipse and high_value region.
            The eighth and ninth indices are averaged wind speed of fitted ellipse and high_value region
    '''

    # import the necessary packages
    from imutils import contours
    from skimage import measure
    import numpy as np
    import argparse
    import imutils
    import cv2

    ######当小于max_thresold数值，则跳出
    if np.nanmax(data) < given_threshold:
        print('given_threshold is too high to find ellipse')
        return None

    thresh = cv2.threshold(data, given_threshold, 255, cv2.THRESH_BINARY)[1]
    # perform a connected component analysis on the thresholded
    # image, then initialize a mask to store only the "large"
    # components

    labels = measure.label(thresh, connectivity=2, background=0)
    mask = np.zeros(thresh.shape, dtype="uint8")
    # loop over the unique components

    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue
        # otherwise, construct the label mask and count the
        # number of pixels
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
        # if the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"

        if numPixels > numll_min:
            # cv2.imshow("Image", labelMask)
            # cv2.waitKey(0)

            # calculate wind
            mask = cv2.add(mask, labelMask)

    # find the contours in the mask, then sort them from left to
    # right
    # cv2.imshow("Image", mask)
    # cv2.waitKey(0)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
#    cnts = contours.sort_contours(cnts)[0]

    try:
        cnts = contours.sort_contours(cnts)[0]
    except Exception as e:
        if e.__class__.__name__ == 'ValueError':
            print('given_threshold is too high to find ellipse')
            return None

    # loop over the contours

    contains = np.ones([len(cnts), 9])*-999

    my_img = np.zeros(mask.shape, dtype="uint8")

    wind_label = measure.label(mask, connectivity=2, background=0)

    delete_i = []
    for (i, c) in enumerate(cnts):
        # draw the bright spot on the image
        if c.shape[0] < 5:
            delete_i.append(i)
            continue
        a = cv2.fitEllipse(c)
        print(a)
        nlon, nlat, cwidth, cheigh, cangle = [int(a[0][0]), int(a[0][1]),a[1][0]*resolution,a[1][1]*resolution,a[2]]

        if (nlon > len(lon)-1) | (nlon < 0)| (nlat > len(lat)-1)|(nlat < 0):
            delete_i.append(i)
            print('Warning: the Ellipse whose centroid is out of range are deleted')
            continue
        print(nlon)
        clon = lon[nlon]
        clat = lat[nlat]

        from area import area
        lat_for_ver = lat[c[:,0,1]]
        lon_for_ver = lon[c[:,0,0]]
        ver = [list(np.array([lon_for_ver, lat_for_ver]).T)]
        obj = {'type': 'Polygon', 'coordinates':ver}
        area_path = area(obj)
        # creating for ellipse
        cv2.ellipse(my_img, a, (255, 0, 255), -1)

        contains[i, :5] = [clon, clat, cwidth, cheigh, cangle]
        contains[i, 6] = area_path

        # calculate averaged wind speed in area
        contains[i, 8] = np.nanmean(data[np.where(wind_label==i+1)])


    elip_cnts = cv2.findContours(my_img.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_NONE)
    elip_cnts = imutils.grab_contours(elip_cnts)
    elip_cnts = contours.sort_contours(elip_cnts)[0]

    wind_label_ellipse = measure.label(my_img, connectivity=2, background=0)

    for (i, c) in enumerate(elip_cnts):
        # draw the bright spot on the image
        from area import area
        lat_for_ver = lat[c[:,0,1]]
        lon_for_ver = lon[c[:,0,0]]
        ver = [list(np.array([lon_for_ver, lat_for_ver]).T)]
        print(ver)
        obj = {'type': 'Polygon', 'coordinates':ver}
        carea = area(obj)
        print(carea)
        contains[i, 5] = carea
        # calculate averaged wind speed in area
        contains[i, 7] = np.nanmean(data[np.where(wind_label_ellipse==i+1)])

    # remove error ellipse
    contains = np.delete(contains, delete_i, axis=0)
    return contains

def plot_ellipse(contains):
    if contains is None:
        return

    # 绘制对应椭圆到基本图形上
    def add_ellipse(rec):
        clon, clat, width, heigh, angle = rec
        import matplotlib.patches as mpatches
        ax.add_patch(mpatches.Ellipse([clon, clat], width, heigh, 360-angle,
                                      transform=ccrs.Geodetic(), facecolor=None, edgecolor='black', fill=False))


    for i in contains:
        add_ellipse(i[:5])

import proplot as plot
import matplotlib.pyplot as plt
import xarray as xr
import cartopy.crs as ccrs
import numpy as np

###做循环逐月提取结果

# lon = data['lon']
# lat = data['lat']
# rain_data = data['降水']
# olon = np.linspace(78,100,88)
# olat = np.linspace(26,38,88)
# olon,olat = np.meshgrid(olon,olat)
# # 插值处理
# func = Rbf(lon, lat, rain_data,function='linear')
# rain_data_new = func(olon, olat)

# fout=open('out_put/plot_N_ERA5_djf_52/file_out.txt','w')

# 读取数据
import netCDF4 as nc
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from matplotlib.ticker import AutoMinorLocator

uwnd_all = xr.open_dataset(r'W300-ERA5-DJF.nc')

# 选取为北半球
uwnd_all = uwnd_all.sel(latitude=slice(90, 0))
maxvalue = np.ceil(np.nanmax(uwnd_all['w300'].values))

lat = uwnd_all['latitude']
lon = uwnd_all['longitude']

for iyear in range(1979, 2020):

    itime = (iyear-1979)

    wind = uwnd_all['w300'].isel(time=int(itime)).values

    # 绘制基本图形

    # 开始高值区判断
    thre = 52

    contains = find_highlight(wind, lat.values, lon.values, 0.25, thre)

    # if contains.all() != None:
    #     for inum in range(contains.shape[0]):
    #         if (contains[inum, 1] > 0) and (contains[inum, 5] > 0):
    #             if (contains[inum, 0] < (140+contains[inum,3]*0.5)  and contains[inum, 0] > (70-contains[inum,3]*0.5) ):
    #                 fout.write('%4d %6.2f %6.2f %6.2f %6.2f %6.2f %10.0f %10.0f %6.2f %6.2f \n'%(iyear, contains[inum,0], contains[inum,1],contains[inum,2],contains[inum,3],contains[inum,4],contains[inum,5]/1000000, contains[inum,6]/1000000, contains[inum,7], contains[inum,8] ))

    # ZhengZhou
    fig = plt.figure(figsize=[8.8,3.2])
    # Label axes of a Plate Carree projection with a central longitude of 180:
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))
    plt.subplots_adjust(0.05,0,0.99,0.98)

    levelss = list(range(0, int(maxvalue), 4))
    if maxvalue % 13 != 0:
        levelss.extend([maxvalue])
    m = ax.contourf(lon.values, lat.values, wind, levels=levelss, transform=ccrs.PlateCarree())
    ax.coastlines()


    plot_ellipse(contains)

    # 增加色标，标题等细节
    ticks = list(range(0, int(maxvalue), 13))
    if maxvalue % 13 != 0:
        ticks.extend([maxvalue])

    fig.colorbar(m, ticks=ticks, orientation='horizontal', shrink=0.95,aspect=40, pad=0.08)

    xticks = list(range(0,359,40))
    xticks.extend([359.9])
    yticks = list(range(0,89, 20))

    ax.set_xticks(xticks, crs=ccrs.PlateCarree())
    ax.set_yticks(yticks, crs=ccrs.PlateCarree())
    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    minor_locator = AutoMinorLocator(2)
    ax.xaxis.set_minor_locator(minor_locator)

    # ax.coastlines()

    labels_x=['0°']
    labels_x.extend([str(i)+'°E' for i in np.arange(40,180,40)])
    labels_x.extend([str(i)+'°W' for i in np.arange(160,0,-40)])
    labels_x.extend(['0°'])
    ax.set_xticklabels(labels_x)

    ax.tick_params(axis = "x", which = "both", bottom = True, top = False)
    ax.tick_params(which='both',direction='in')

    ax.set_title('threshold is %i'%thre)
    plt.show()
    fig.savefig('out_put/plot_N_ERA5_djf_52/u_%s.png'%(iyear), dpi=150)
    plt.close()
        
   # plt.show()

# plt.savefig('m.png', dpi=60)
