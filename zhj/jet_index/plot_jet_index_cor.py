import pandas as pd
import numpy as np
import xarray as xr
from nc4 import read_nc4, save_nc4
pre = xr.open_dataset(r'pre_month.nc4')['pre']

# read index, and trans into datatime index
indexs = pd.read_csv('ERA5_jet_index.txt', delim_whitespace=True, header=None
                    , names=['YEAR', 'MONTH', '1', '2', '3', '4', '5', '6', '7','8','9'])
indexs['DAY'] = np.ones(len(indexs.index))
indexs.index = pd.to_datetime(indexs[['YEAR', 'MONTH', 'DAY']])
indexs = indexs.drop(['DAY'], axis=1)['1979':'2014']


def verdata(msst, pc, a):
    from scipy.stats import pearsonr
    def fit_line2(x):
        # print(x.shape, y.shape)
        x = x.flatten()
        y = pc.flatten()
        index = np.isnan(x)
        x = x[~index]
        y = y[~index]
        if len(x) < 10:
            return np.nan, np.nan
        rvalue, pvalue = pearsonr(x.flatten(), y.flatten())
        return rvalue, pvalue
    r2, p = np.apply_along_axis(fit_line2, 0, msst)
    r2s = r2
    r2s[np.where(p>a)] = np.nan
    return r2s, r2

pre_result = []

for month in range(1, 13):
    pre_in_month = pre.sel(time = pre.time.dt.month.isin(month)).values
    indexs_in_month = indexs[indexs.index.month==month]
    pre_month = []
    for index_num in range(1, 10):
        pre_month.append(verdata(pre_in_month, np.array(indexs_in_month[str(index_num)]), 0.1)[0])
    pre_result.append(pre_month)
save_nc4(np.array(pre_result), 'pre_result')
