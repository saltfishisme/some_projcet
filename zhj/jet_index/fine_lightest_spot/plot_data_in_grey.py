import proplot as plot
import matplotlib.pyplot as plt
import xarray as xr

uwnd = xr.open_dataset(r'D:\basis\data\old_rea\uwnd.mon.mean.nc')
lat = uwnd['lat']
lon = uwnd['lon']
uwnd = uwnd['uwnd'].isel(time=0, level=7).values
import numpy as np

fig, ax = plot.subplots(proj='cyl', figsize=[9,5], proj_kw={'lon_0': 180})

m = ax[0].contourf(lon, lat, uwnd)

data = np.loadtxt('mask.txt')
# data=np.ma.masked_equal(data, 0)
# m = ax[0].contourf(lon, lat, data)

from matplotlib.patches import Circle
import cartopy.crs as ccrs
import cartopy
import shapely
radius_in_meters = 13

clon=lon[57]
clat = lat[23]
import matplotlib.patches as mpatches
ax.add_patch(mpatches.Ellipse([clon, clat], 27*2.5, 5*2.5, transform=ccrs.PlateCarree(),
                              facecolor=None, edgecolor='black', fill=False))
# circle_points = cartopy.geodesic.Geodesic.circle(lon=lon[clon], lat=lat[clat], radius=radius_in_meters*111000, endpoint=False)
# geom = shapely.geometry.Polygon(circle_points)
# ax.add_geometries((geom,), crs=cartopy.crs.PlateCarree(), facecolor='red', edgecolor='none', linewidth=0)



fig.colorbar(m, loc='b', extendsize='1.7em')
# plt.savefig('m.png', dpi=60)
plt.show()
