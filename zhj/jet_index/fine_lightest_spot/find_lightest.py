# # import the necessary packages
# from imutils import contours
# from skimage import measure
# import numpy as np
# import argparse
# import imutils
# import cv2
#
#
# # construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("--image", default=r'D:\basis\some_project\fine_lightest_spot\01.jpg',
# 	help="path to the image file")
# args = vars(ap.parse_args())
#
# # load the image, convert it to grayscale, and blur it
# image = cv2.imread(args["image"])
# #
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# blurred = cv2.GaussianBlur(gray, (11, 11), 0)
#
# # threshold the image to reveal light regions in the
# # blurred image
#
# thresh = cv2.threshold(blurred, 200, 255, cv2.THRESH_BINARY)[1]
# # perform a series of erosions and dilations to remove
# # any small blobs of noise from the thresholded image
# thresh = cv2.erode(thresh, None, iterations=2)
#
# thresh = cv2.dilate(thresh, None, iterations=4)
#
# # perform a connected component analysis on the thresholded
# # image, then initialize a mask to store only the "large"
# # components
#
# labels = measure.label(thresh, connectivity=2, background=0)
#
# mask = np.zeros(thresh.shape, dtype="uint8")
# # loop over the unique components
#
# for label in np.unique(labels):
# 	# if this is the background label, ignore it
# 	if label == 0:
# 		continue
# 	# otherwise, construct the label mask and count the
# 	# number of pixels
# 	labelMask = np.zeros(thresh.shape, dtype="uint8")
# 	labelMask[labels == label] = 255
# 	numPixels = cv2.countNonZero(labelMask)
# 	# if the number of pixels in the component is sufficiently
# 	# large, then add it to our mask of "large blobs"
#
# 	if numPixels > 300:
# 		mask = cv2.add(mask, labelMask)
#
# # cv2.imshow("Image", mask)
# # cv2.waitKey(0)
#
#
# # find the contours in the mask, then sort them from left to
# # right
# # cv2.imshow("Image", image)
# # cv2.waitKey(0)
# cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
# 	cv2.CHAIN_APPROX_SIMPLE)
# cnts = imutils.grab_contours(cnts)
# cnts = contours.sort_contours(cnts)[0]
# # loop over the contours
#
# image1 = cv2.imread('111.png')
# for (i, c) in enumerate(cnts):
# 	# draw the bright spot on the image
# 	(x, y, w, h) = cv2.boundingRect(c)
# 	((cX, cY), radius) = cv2.minEnclosingCircle(c)
# 	a = cv2.minAreaRect(c)
# 	print(a)
# 	ellipse = cv2.fitEllipse(c)
#
# 	cv2.rectangle(image1, a[0],a[1] (255, 0, 255))
# 	cv2.ellipse(image1, ellipse,
# 				(255, 0, 255))
# 	# cv2.circle(image, (int(cX), int(cY)), int(radius),
# 	# 	(0, 0, 255), 3)
# 	# cv2.putText(image, "#{}".format(i + 1), (x, y - 15),
# 	# 	cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
# # show the output image
# cv2.imshow("Image", image1)
# cv2.waitKey(0)
# # plt.show()
import numpy as np
a = np.ones([3,2])
print(a)
a = np.delete(a, [1,2], axis=0)
print(a)
# from matplotlib import pyplot
# from shapely.geometry.point import Point
# import shapely.affinity
# from descartes import PolygonPatch
# # Note: download figures.py manually from shapely github repo, put it in shapely install directory
# # from shapely.figures import SIZE, GREEN, GRAY, set_limits
#
#
# # 1st elem = center point (x,y) coordinates
# # 2nd elem = the two semi-axis values (along x, along y)
# # 3rd elem = angle in degrees between x-axis of the Cartesian base
# #            and the corresponding semi-axis
# ellipse = ((0, 0),(7, 4),36)
#
# # Let create a circle of radius 1 around center point:
# circ = shapely.geometry.Point(ellipse[0]).buffer(1)
#
# # Let create the ellipse along x and y:
# ell  = shapely.affinity.scale(circ, int(ellipse[1][0]), int(ellipse[1][1]))
#
# # Let rotate the ellipse (clockwise, x axis pointing right):
# ellr = shapely.affinity.rotate(ell,ellipse[2])
#
# # If one need to rotate it clockwise along an upward pointing x axis:
# elrv = shapely.affinity.rotate(ell,90-ellipse[2])
# # According to the man, a positive value means a anti-clockwise angle,
# # and a negative one a clockwise angle.
#
#
# fig = pyplot.figure()
# ax = fig.add_subplot(111)
# patch = PolygonPatch(elrv, alpha=0.5, zorder=2)
# print(patch.area)
# ax.add_patch(patch)
# # set_limits(ax, -10, 10, -10, 10)
# pyplot.show()