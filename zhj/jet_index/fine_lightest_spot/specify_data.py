import pandas as pd
import numpy as np

data = pd.read_csv('file_out_north.txt', delim_whitespace=True, header=None, names=['YEAR', 'MONTH', '1','2','3','4','5','6','7'])
data['DAY'] = np.ones(len(data.index))
index = pd.to_datetime(data[['YEAR', 'MONTH', 'DAY']])
data.index = index
data = data.drop(['DAY'], axis=1)
data = data.sort_values(['1'], ascending=False).drop_duplicates(['YEAR','MONTH'], keep='last').sort_index()
data = data.resample('M').mean()

# print(data)
data = (data.fillna(method='bfill')+data.fillna(method='ffill'))/2
print(data['1952'])
data['MONTH'] = data.index.month

np.savetxt('data.txt', data, fmt='%i %i %0.1f %0.1f %0.2f %0.2f %0.2f %i %i',newline = '\n')