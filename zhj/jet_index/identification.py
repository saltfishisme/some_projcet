import numpy as np
import matplotlib.pyplot as plt
import random
import nc4
import xarray as xr
import cmaps

data_climate_nc = xr.open_dataset('air_djf.nc')['air']
data_climate = data_climate_nc.values

testYear = np.array([1979, 1989, 1990, 1992, 1993, 1994, 2002, 2007, 2008, 2009, 2014])
index_TestYear = testYear - 1979
data_test = data_climate[index_TestYear, :]


def monte_carlo(data_climate, data_test, type_test='double'):
    """
    :param data_climate: [time, lat, lon]
    :param data_test: [time, lat, lon]
    :param type_test: str, 'right', 'left' for one-tailed test, and 'double' for two-tailed test
    :return: bool array, [lat, lon, 3], containing 99%, 95%, and 90% confidence interval.
    """
    data_length = data_test.shape[0]

    def monte_carlo_in_axis(data_climate_axis, data_test_axis):

        if type_test == 'right':
            p = [99, 95, 90]
        elif type_test == 'left':
            p = [1, 5, 10]
        elif type_test == 'double':
            p = [99, 95, 90]
        climate_mean = np.nanmean(data_climate_axis)
        div = []
        for i in range(1000):
            s_data1 = np.array(random.sample(list(data_climate_axis), data_length))
            div.append(np.nanmean(s_data1) - climate_mean)
        div = np.array(div)
        if type_test == 'double':
            div = np.abs(div)

        div_Pvalue = np.percentile(div, p)

        div_test = np.nanmean(data_test_axis) - climate_mean
        if type_test == 'right':
            return div_test > div_Pvalue
        elif type_test == 'left':
            return div_test < div_Pvalue
        elif type_test == 'double':
            return np.abs(div_test) > div_Pvalue
        return div_Pvalue

    div_Pvalues = np.empty([data_climate.shape[1], data_climate.shape[2], 3])
    for i in range(data_climate.shape[1]):
        for j in range(data_climate.shape[2]):
            data_climate_in_axis = data_climate[:, i, j]
            if (np.isnan(data_climate_in_axis)).all():
                continue
            div_Pvalues[i, j] = monte_carlo_in_axis(data_climate_in_axis, data_test[:, i, j])

    return div_Pvalues


a = monte_carlo(data_climate, data_test, type_test='right')

# 存储为标准nc文件
nc4.save_standard_nc(np.transpose(a, [2,0,1]), 'bool', ['99%', '95%', '90%'],
                     lon=data_climate_nc.lon, lat=data_climate_nc.lat, file_name='pvalue_t2m_right')
exit(0)
###############################
''' plot '''

a = nc4.read_nc4('pvalue_t2m_right_2')
data_climate_nc = xr.open_dataset('air_djf.nc')['air']
data_climate = data_climate_nc.values
testYear = np.array([1979, 1989, 1990, 1992, 1993, 1994, 2002, 2007, 2008, 2009, 2014])
index_TestYear = testYear - 1979
data_test = data_climate[index_TestYear, :]
data_C = np.nanmean(data_climate, axis=0)
data = np.nanmean(data_test, axis=0)

import cartopy.crs as ccrs

fig, ax = plt.subplots(1, 1, subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
# cb = ax.contourf(data_climate_nc.lon, data_climate_nc.lat, data-data_C, cmap=cmaps.MPL_gnuplot, linewidths=0.1)
# plt.colorbar(cb)
ax.contour(data_climate_nc.lon, data_climate_nc.lat, data - data_C, colors='red', linewidths=0.1,
           transform=ccrs.PlateCarree())
ax.pcolormesh(data_climate_nc.lon, data_climate_nc.lat, np.ma.masked_values(a[:, :, 2], 0), cmap=cmaps.MPL_gnuplot,
              transform=ccrs.PlateCarree())
ax.coastlines(linewidth=0.1)
plt.show()
exit(0)
