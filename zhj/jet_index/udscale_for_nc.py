import xarray as xr
from nc4 import read_nc4
#
# pre = xr.open_dataset(r'/home/zhj/data1/CN05.1/CN05.1_Tm_1961_2014_daily_1x1.nc')\
#     .sel(time=slice('1979-01-01', '2015-01-01'))
# data = pre.resample(time="1MS").mean(dim="time")
# data.to_netcdf('tem_month.nc4')
#
# import xarray as xr
# from nc4 import read_nc4
# pre = xr.open_dataset(r'/home/zhj/data1/CN05.1/CN05.1_Pre_1961_2014_daily_025x025.nc')\
#     .sel(time=slice('1979-01-01', '2015-01-01'))
# data = pre.resample(time="1MS").sum(dim="time")
# data.to_netcdf('pre_month.nc4')

import numpy as np
aa = xr.open_dataset('tem_month.nc4')
lon = aa['lon'].values
lat = aa['lat'].values
def main(variable):
    a = read_nc4(r'D:\basis\some_projects\jet_index\%s_result'%variable)
    a = np.ma.masked_invalid(a)
    import proplot as plot
    import matplotlib.pyplot as plt
    import cartopy.crs as ccrs

    month_name = ['Jan.','Feb.','Mar.','Apr.','May.','Jun.','Jul.','Aug.','Sept.','Oct.','Nov.','Dec.',]
    index_name = ['lon', 'lat', 'width','height', 'angle', 'area_e', 'area_a', 'strength_e', 'strength_a']
    for month in range(9):
        data = a[:,month,:,:]
        fig,ax = plot.subplots(ncols=3, nrows=4, tight=False, figsize=(6,8),
                               proj=ccrs.AlbersEqualArea(central_longitude=105)
                               , right='1em', left='1em', top='3.8em', bottom='4em')

        dmin = np.nanmin(data)
        dmax = np.nanmax(data)
        ddmm = np.max([abs(dmin), abs(dmax)])
        for i in range(12):
            fill = ax[i].pcolormesh(lon, lat, data[i], cmap='coolwarm', vmin=-ddmm, vmax=ddmm)
            ax[i].format(title=month_name[i])
            from plot_in_proplot import shp_clip_for_proplot
            shp_clip_for_proplot(ax[i], clipshape=False)

            ax2 = fig.add_axes([0.2+0.01*i, 0.5, 0.2, 0.2], projection=ccrs.AlbersEqualArea(central_longitude=105))
            ax2.pcolormesh(lon, lat, data[i])
            # ax2.format(latlim=[17, 54], lonlim=[80, 127])
            ax2.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
            from plot_in_proplot import shp_clip_for_proplot
            shp_clip_for_proplot(ax2, clipshape=False)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
            ip = InsetPosition(ax[i], [0, -0.045, 0.15, 0.3])
            ax2.set_axes_locator(ip)
        cbaxes = fig.add_axes([0.1, 0.04, 0.8, 0.01])
        cb = plt.colorbar(fill, cax=cbaxes, orientation="horizontal")
        ax.format(latlim=[17, 54], lonlim=[80, 127], abc=True, abcloc='ul', abcstyle='a)'
                  , suptitle='%s %s'%(variable, index_name[month]))
        plt.savefig('%s_%s.jpg'%(variable, index_name[month]), dpi=400)
main('PRCP')
main('TEM')


