import pandas as pd
import numpy as np
row_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data/'
trans_data_path = r"D:\basis\some_projects\zhj\rainstorm\trans_data/"

def drought(selcet_year):
    line_data = pd.read_pickle(trans_data_path+'drought_lin.pickle')
    line_data.columns = ['year', '0', '1', '2', '3', '4', '5', '6', '7', '8', 'lat', 'lon']
    line_data = line_data[line_data['year'] >= selcet_year]
    def fit_lines(y):
        x = np.arange(y.shape[0])
        def sfit(x,y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            return rvalue
        result = []
        for i in ['0', '1', '2', '3', '4', '5', '6', '7', '8']:
            result.append(sfit(x, np.array(y[i])))
        return result
    def fit_lines_p(y):
        x = np.arange(y.shape[0])
        def sfit(x,y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            return pvalue
        result = []
        for i in ['0', '1', '2', '3', '4', '5', '6', '7', '8']:
            result.append(sfit(x, np.array(y[i])))
        return result

    linegress = line_data.groupby(line_data.index).apply(lambda x: fit_lines(x))
    linegress = pd.DataFrame(linegress)
    for si in range(9):
        linegress['%i' % (si)] = np.array([float(i[si]) for i in linegress[0]])


    def plotss(sdata, vari, title, ranges, colormap):
        import numpy as np
        import matplotlib.pyplot as plt
        import proplot as plot
        import cmaps
        import cartopy.crs as ccrs
        def rbf(ranges, datas, vari):
            from scipy.interpolate import Rbf
            olon = np.linspace(ranges[0][2], ranges[0][3], 88)
            olat = np.linspace(ranges[0][0], ranges[0][1], 88)

            olon, olat = np.meshgrid(olon, olat)
            # 插值处理
            lon = datas['lon']
            lat = datas['lat']
            data = np.array(datas[vari])
            func = Rbf(lon, lat, data, function='linear')
            data_new = func(olon, olat)
            return olon, olat, data_new

        lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, vari)
        type2 = np.ma.masked_where((type1>-0.27)&(type1<0.27), type1)
        fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[2.8,2.8], tight=False
                                , left='0.1em', right='0.1em', top='1.5em', bottom='1.4em')
        colors = ['#131212','#2B2B2B','#434242','#5D5D5C','#FFFFFD','#868585','#A3A3A2','#C0BFBE','#0e0e0e','#DEDEDD','#F2F2F2']
        def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0,60]):
            def plots(ax):
                bcb = ax.contour(lons, lats, type1, levels= 10, extend='both',transform=ccrs.PlateCarree(), linewidth=0.1,colors=colors)

                cb = ax.contourf(lons, lats, type2, levels= [-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1], extend='both',transform=ccrs.PlateCarree(), cmap='Negpos')
                # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
                #             locator=leveltick, ticklabels=levellabel)

                ax.colorbar(cb, loc='b', length=1, width='0.5em', space='0.5em',ticklabelsize=7,
                            locator=[-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1], ticklabels=[str(i) for i in [-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1]])
                # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
                # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=bcb, clipshape=True)
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            def plotss(ax):
                colors = ['#4F7095','#6799D5','#A6BFEB','#E0E8F9','#FFFFFF','#F9E2DE','#F7D8D3','#EDAFA6','#E1786B','#B24E47','#773030']
                cb = ax.contourf(lons, lats, type2, transform=ccrs.PlateCarree(), colors=colors, vmin=ax2_ranges[0],vmax=ax2_ranges[1])

                ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            plots(ax)
            ax.format(latlim=[17, 54], lonlim=[80, 127])

            ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
            plotss(ax2)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

            ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
            ax2.set_axes_locator(ip)



        all(ax[0], type1, np.arange(-1, 1, 0.2), np.arange(-1,1, 0.2), [0.2, 0.5, 0.2, 0.2], ranges)

        # ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
        ax.format(suptitle=title)
        plt.rcParams['font.family'] = ['sans-serif']
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig('lingress_%s.png'%(title), dpi=500)
        plt.close()

    sdata = linegress
    # add right lat, lon
    ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
    sll = ll.loc[sdata.index]
    sdata['lat'] = sll['lat']
    sdata['lon'] = sll['lon']
    bbb = sdata.max()
    vari_name = ['特旱', '重旱','中旱','轻旱','正常','微湿','中湿','重湿','特湿']
    range_all = [40,20,20,20,20,10,10,10,20]
    print(sdata.max, sdata.min)
    import cmaps
    for i in range(9):
        plotss(sdata, '%i'%i, '%i年-2019年 %s月数趋势分布'%(selcet_year, vari_name[i]), [-2, 2], cmaps.MPL_bwr)


def rain(line_data, selcet_year, use_col, vari_name):

    line_data = line_data[line_data['year']>=selcet_year]


    def fit_lines(y):
        x = np.arange(y.shape[0])
        def sfit(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            return rvalue

        result = []
        for i in use_col:
            result.append(sfit(x, np.array(y[i])))
        return result

    def fit_lines_p(y):
        x = np.arange(y.shape[0])

        def sfit(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            return pvalue

        result = []
        for i in use_col:
            result.append(sfit(x, np.array(y[i])))
        return result

    linegress = line_data.groupby(line_data.index).apply(
        lambda x: fit_lines(x))
    linegress = pd.DataFrame(linegress)

    for ci, si in enumerate(use_col):
        linegress['%s' % (si)] = np.array([float(i[ci]) for i in linegress[0]])

    def plotss(sdata, vari, title, ranges, colormap):
        import numpy as np
        import matplotlib.pyplot as plt
        import proplot as plot
        import cmaps
        import cartopy.crs as ccrs
        def rbf(ranges, datas, vari):
            from scipy.interpolate import Rbf
            olon = np.linspace(ranges[0][2], ranges[0][3], 88)
            olat = np.linspace(ranges[0][0], ranges[0][1], 88)

            olon, olat = np.meshgrid(olon, olat)
            # 插值处理
            lon = datas['lon']
            lat = datas['lat']
            data = np.array(datas[vari])
            func = Rbf(lon, lat, data, function='linear')
            data_new = func(olon, olat)
            return olon, olat, data_new

        lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, vari)
        type2 = type1
        type2 = np.ma.masked_where((type1 > -0.27) & (type1 < 0.27), type1)
        fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[2.8,2.8], tight=False
                                , left='0.1em', right='0.1em', top='1.5em', bottom='1.4em')
        colors = ['#131212', '#2B2B2B', '#434242', '#5D5D5C', '#FFFFFD', '#868585', '#A3A3A2', '#C0BFBE', '#0e0e0e',
                  '#DEDEDD', '#F2F2F2']

        def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0, 60]):
            def plots(ax):
                bcb = ax.contour(lons, lats, type1, levels=10, extend='both', transform=ccrs.PlateCarree(),
                                 linewidth=0.1, colors=colors)

                cb = ax.contourf(lons, lats, type2, levels=levels,
                                 extend='both', transform=ccrs.PlateCarree(), cmap='Negpos')
                # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
                #             locator=leveltick, ticklabels=levellabel)

                ax.colorbar(cb, loc='b', length=1, width='0.5em', space='0.5em',ticklabelsize=7,
                            locator=[-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1], ticklabels=[str(i) for i in [-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1]])
                # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
                # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=bcb, clipshape=True)
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            def plotss(ax):
                colors = ['#6799D5','#A6BFEB','#E0E8F9','#FFFFFF','#F9E2DE','#F7D8D3','#EDAFA6','#E1786B','#B24E47','#773030']
                cb = ax.contourf(lons, lats, type2, levels=levels, transform=ccrs.PlateCarree(), colors=colors)

                ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            plots(ax)
            ax.format(latlim=[17, 54], lonlim=[80, 127])

            ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
            plotss(ax2)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

            ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
            ax2.set_axes_locator(ip)



        all(ax[0], type1, [-1, -0.8, -0.6, -0.4, -0.27, 0.27, 0.4, 0.6, 0.8, 1], [-1, -0.8, -0.6, -0.4, -0.27, 0.27, 0.4, 0.6, 0.8, 1], [0.2, 0.5, 0.2, 0.2], ranges)

        # ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
        ax.format(suptitle=title)
        plt.rcParams['font.family'] = ['sans-serif']
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig('lingress_%s.png' % (title), dpi=500)
        plt.close()



    sdata = linegress
    # add right lat, lon
    ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
    sll = ll.loc[sdata.index]
    sdata['lat'] = sll['lat']
    sdata['lon'] = sll['lon']
    bbb = sdata.max()
    range_all = [40, 20, 20, 20, 20, 10, 10, 10, 20]
    print(sdata.max, sdata.min)
    import cmaps
    for i, name in enumerate(vari_name):
        plotss(sdata, use_col[i], '%i年-2019年 %s趋势分布' % (selcet_year, vari_name[i]), [-2, 2], cmaps.MPL_bwr)

def rain_month(line_data, season_name, selcet_year, use_col, vari_name):
    line_data = line_data[line_data['year'] >= selcet_year]

    def fit_lines(y):
        x = np.arange(y.shape[0])
        def sfit(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            return rvalue

        result = []
        for i in use_col:
            print(i)
            result.append(sfit(x, np.array(y[i])))
        return result

    def fit_lines_p(y):
        x = np.arange(y.shape[0])

        def sfit(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            return pvalue

        result = []
        for i in use_col:
            result.append(sfit(x, np.array(y[i])))
        return result

    linegress = line_data.groupby(line_data.index).apply(
        lambda x: fit_lines(x))
    linegress = pd.DataFrame(linegress)

    for ci, si in enumerate(use_col):
        linegress['%s' % (si)] = np.array([float(i[ci]) for i in linegress[0]])

    def plotss(sdata, vari, title, ranges, colormap):
        import numpy as np
        import matplotlib.pyplot as plt
        import proplot as plot
        import cmaps
        import cartopy.crs as ccrs
        def rbf(ranges, datas, vari):
            from scipy.interpolate import Rbf
            olon = np.linspace(ranges[0][2], ranges[0][3], 88)
            olat = np.linspace(ranges[0][0], ranges[0][1], 88)

            olon, olat = np.meshgrid(olon, olat)
            # 插值处理
            lon = datas['lon']
            lat = datas['lat']
            data = np.array(datas[vari])
            func = Rbf(lon, lat, data, function='linear')
            data_new = func(olon, olat)
            return olon, olat, data_new

        lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, vari)
        type2 = np.ma.masked_where((type1 > -0.27) & (type1 < 0.27), type1)
        fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[2.8,2.8], tight=False
                                , left='0.1em', right='0.1em', top='1.5em', bottom='1.4em')
        colors = ['#131212', '#2B2B2B', '#434242', '#5D5D5C', '#FFFFFD', '#868585', '#A3A3A2', '#C0BFBE', '#0e0e0e',
                  '#DEDEDD', '#F2F2F2']

        def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0, 60]):
            def plots(ax):
                bcb = ax.contour(lons, lats, type1, levels=10, extend='both', transform=ccrs.PlateCarree(),
                                 linewidth=0.1, colors=colors)

                cb = ax.contourf(lons, lats, type2, levels=levels,
                                 extend='both', transform=ccrs.PlateCarree(), cmap='Negpos')
                # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
                #             locator=leveltick, ticklabels=levellabel)

                ax.colorbar(cb, loc='b', length=1, width='0.5em', space='0.5em',ticklabelsize=7,
                            locator=[-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1], ticklabels=[str(i) for i in [-1,-0.8,-0.6,-0.4,-0.27,0.27,0.4,0.6,0.8,1]])
                # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
                # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=bcb, clipshape=True)
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            def plotss(ax):
                colors = ['#6799D5','#A6BFEB','#E0E8F9','#FFFFFF','#F9E2DE','#F7D8D3','#EDAFA6','#E1786B','#B24E47','#773030']
                cb = ax.contourf(lons, lats, type2, levels=levels, transform=ccrs.PlateCarree(), colors=colors)

                ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            plots(ax)
            ax.format(latlim=[17, 54], lonlim=[80, 127])

            ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
            plotss(ax2)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

            ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
            ax2.set_axes_locator(ip)



        all(ax[0], type1, [-1, -0.8, -0.6, -0.4, -0.27, 0.27, 0.4, 0.6, 0.8, 1], [-1, -0.8, -0.6, -0.4, -0.27, 0.27, 0.4, 0.6, 0.8, 1], [0.2, 0.5, 0.2, 0.2], ranges)

        # ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
        ax.format(suptitle=title)
        plt.rcParams['font.family'] = ['sans-serif']
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig('lingress_%s.png' % (title), dpi=500)
        plt.close()

    sdata = linegress
    # add right lat, lon
    ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
    sll = ll.loc[sdata.index]
    sdata['lat'] = sll['lat']
    sdata['lon'] = sll['lon']
    bbb = sdata.max()
    range_all = [40, 20, 20, 20, 20, 10, 10, 10, 20]
    print(sdata.max, sdata.min)
    import cmaps
    for i, name in enumerate(vari_name):
        plotss(sdata, use_col[i], '%i年-2019年%s%s趋势分布' % (selcet_year, season_name, vari_name[i]), [-2, 2], cmaps.MPL_bwr)


def rain_month_pre_data():
    result_yearly = pd.DataFrame(pd.read_pickle('result_monthly.pickle').reindex())
    result_yearly.columns = ['p']
    result_yearly = result_yearly.reset_index()
    result_yearly.columns = ['year', 'month', 'station', 'p']
    result_yearly['days'] = np.array([int(i[0]) for i in result_yearly['p']])
    result_yearly['intensity'] = np.array([int(i[1]) for i in result_yearly['p']])
    result_yearly = result_yearly[['year', 'month', 'station', 'days', 'intensity']]
    line_data = result_yearly.set_index('station')

    # 处理成年值

#############################################################################
# use_col = ['p']
# vari_name = ['平均降水']
#
# line_data = pd.read_pickle(trans_data_path+'pre_seasonly.pickle')
# line_data = line_data.set_index('station')
# line_data['lat'] = 1
# line_data['lon'] = 1
# ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
#
# for sta in line_data.index.unique():
#     sdata = line_data.loc[sta]
#     sll = ll.loc[sta]
#     line_data['lat'].loc[sta] = sll['lat']
#     line_data['lon'].loc[sta] = sll['lon']
# month_name = ['春季', '夏季', '秋季', '冬季']
#
# for select_year in [1960, 1980]:
#     for si, i in enumerate([3,6,9,12]):
#         rain_month(line_data[line_data['season']==i], month_name[si], select_year, use_col, vari_name)
#
#
# line_data = pd.read_pickle(trans_data_path+'pre_yearly.pickle')
# line_data = line_data.set_index('station')
# line_data['lat'] = 1
# line_data['lon'] = 1
# ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
#
# for sta in line_data.index.unique():
#     sdata = line_data.loc[sta]
#     sll = ll.loc[sta]
#     line_data['lat'].loc[sta] = sll['lat']
#     line_data['lon'].loc[sta] = sll['lon']
# for select_year in [1960, 1980]:
#     rain(line_data, select_year, use_col, vari_name)
#
#
# ##################################################################################
# use_col = ['days', 'intensity']
# vari_name = ['暴雨日数', '暴雨强度']
# line_data = pd.read_pickle(trans_data_path+'rain_monthly_lin.pickle')
# month_name = ['春季', '夏季', '秋季', '冬季']
# for si, i in enumerate([3,6,9,12]):
#     rain_month(line_data[line_data['month']==i].copy(), month_name[si], 1960, use_col, vari_name)
#
# line_data = pd.read_pickle(trans_data_path+'rain_monthly_lin.pickle')
# month_name = ['春季', '夏季', '秋季', '冬季']
# for si, i in enumerate([3,6,9,12]):
#     rain_month(line_data[line_data['month']==i].copy(), month_name[si], 1980, use_col, vari_name)
#
# line_data = pd.read_pickle(trans_data_path+'rain_year_lin.pickle')
# rain(line_data, 1960, use_col, vari_name)
# rain(line_data, 1980, use_col, vari_name)

drought(1960)
drought(1980)
