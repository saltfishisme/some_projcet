import pandas as pd
import numpy as np
row_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data/'
trans_data_path = r"D:\basis\some_projects\zhj\rainstorm\trans_data/"

'''1，历史资料分析某个省份或者沿海风的变化趋势；2，可以分析cmip6的资料，看看未来全国风资源的变化。'''


def screen_data():
    '''处理缺失数据'''
    def for_999999(data):
        data[data == 999999] = np.nan
        data[data == 999998] = np.nan
        data[data == 999996] = np.nan
        data[data == 999990] = 0.01
        data[data > 999800] = (data[data > 998000] - 998000)*10
        data[data > 999700] = (data[data > 999700] - 999700)*10
        data[data > 999600] = (data[data > 999600] - 999600)*10
        return data

    def for_32766(data):
        data[data==32766] = np.nan
        data[data == 32700] = 0.01
        data[data > 32000] = data[data > 32000] - 32000
        data[data > 31000] = data[data > 31000] - 31000
        data[data > 30000] = data[data > 30000] - 30000
        return data
    # 警告，先9再3

    data = pd.read_csv(r'D:\basis\data\row\PRE.txt', delim_whitespace=True, header=None, usecols=[0,4,5,6,9])
    print(data)
    data[9] = for_999999(data[9])
    data[9] = for_32766(data[9])
    data[9] = data[9]/10

    data = data[data[4]>=1960]
    data = data[data[4]<=2019]
    data = data.set_index(pd.to_datetime(data[4] * 10000 + data[5] * 100 + data[6], format='%Y%m%d'))
    data.to_pickle('data_mask.pickle')

# screen_data()


# data = pd.read_pickle('data_mask.pickle')
# # 暴雨
#
# def anomaly(data):
#     means = data[9].mean()
#     print(means)
#     print(data[9])
#     data[9] = (np.array(data[9])-means)/means
#
#     result[9].iloc[data.index] = data[9]
#     return means
#
# result_monthly = data.groupby([data.index.year,data.index.month, data[0]]).sum().reset_index()
# result = result_monthly.copy()
# sanomaly = result_monthly.groupby([result_monthly['level_1'], result_monthly[0]]).apply(lambda x:anomaly(x))
#
# result.to_pickle('dourght_monthly.pickle')

# print(sanomaly)
# result_yearly.to_pickle('result_yearly.pickle')
# for i in np.arange(-1, 1,0.2):
#     print(i, i+0.2)
         # s1.append(data[9][(data[9]>i)&(data[9]<=i+0.2)].shape[0])

# result_monthly = pd.DataFrame(pd.read_pickle('dourght_monthly.pickle'))
# def classify(result_yearly):
#     result = []
#     for count, i in enumerate([-100, -0.8, -0.6, -0.4, -0.2, 0.2, 0.4, 0.6, 0.8]):
#         if i == 0.8:
#             result.append(result_yearly[9][(result_yearly[9] >= i) & (result_yearly[9] < i+100)].shape[0])
#         elif i == -100:
#             result.append(result_yearly[9][(result_yearly[9] >= i) & (result_yearly[9] < -0.8)].shape[0])
#
#         else:
#             result.append(result_yearly[9][(result_yearly[9] >= i) & (result_yearly[9] < i+0.2)].shape[0])
#     return result
#
# sanomaly = result_monthly.groupby([result_monthly['level_0'], result_monthly[0]]).apply(lambda x:classify(x))
# print(sanomaly)
# result_yearly.columns=['p']
# result_yearly = result_yearly.reset_index()
# result_yearly.columns=['year','month', 'station', 'p']

#
# result_yearly['4'] = np.array([int(i[4]) for i in result_yearly['p']])+np.array([int(i[5]) for i in result_yearly['p']])

# result_yearly.to_pickle('2drought_monthly.pickle')
# skip 0

# def find_ll(data):

# add right lat, lon



def plotss(sdata, vari, title, ranges, colormap):
    import numpy as np
    import matplotlib.pyplot as plt
    import proplot as plot
    import cmaps
    import cartopy.crs as ccrs
    def rbf(ranges, datas, vari):
        from scipy.interpolate import Rbf
        olon = np.linspace(ranges[0][2], ranges[0][3], 88)
        olat = np.linspace(ranges[0][0], ranges[0][1], 88)

        olon, olat = np.meshgrid(olon, olat)
        # 插值处理
        lon = datas['lon']
        lat = datas['lat']
        data = np.array(datas[vari])
        func = Rbf(lon, lat, data, function='linear')
        data_new = func(olon, olat)
        return olon, olat, data_new

    lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, vari)
    type1 = np.ma.masked_where(type1 < 0, type1)
    fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[3,2.9], tight=False
                            , left='1em', top='1.5em', bottom='2em')
    def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0,60]):
        def plots(ax):
            cb = ax.contourf(lons, lats, type1, levels= levels, extend='both',transform=ccrs.PlateCarree(), cmap=colormap)
            # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
            #             locator=leveltick, ticklabels=levellabel)
            ax.colorbar(cb, loc='b', length=0.8, width='0.5em', space='0.5em',
                        locator=tickslevel)
            # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
            # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

            from plot_in_proplot import shp_clip_for_proplot
            shp_clip_for_proplot(ax, fill=cb, clipshape=True)

        def plotss(ax):
            cb = ax.contourf(lons, lats, type1, transform=ccrs.PlateCarree(), cmap=colormap, vmin=ax2_ranges[0],vmax=ax2_ranges[1])

            ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
            from plot_in_proplot import shp_clip_for_proplot
            shp_clip_for_proplot(ax, fill=cb, clipshape=True)

        plots(ax)
        ax.format(latlim=[17, 54], lonlim=[80, 127])

        ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
        plotss(ax2)
        from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

        ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
        ax2.set_axes_locator(ip)

    if ranges[1] < 30:
        level1 = np.arange(0, ranges[1], 1)
        ticks=3
    else:
        level1 = np.arange(0, ranges[1], 2)
        ticks = 5

    all(ax[0], type1, level1, np.arange(0, ranges[1], ticks), [0.2, 0.5, 0.2, 0.2], ranges)

    # ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
    ax.format(suptitle=title)
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams['font.sans-serif'] = ['SimHei']
    # plt.show()
    plt.savefig('drought_%s.png'%(title), dpi=500)
    plt.close()
row_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data/'
trans_data_path = r"D:\basis\some_projects\zhj\rainstorm\trans_data/"
vari_name = ['特旱', '重旱','中旱','轻旱','正常','微湿','中湿','重湿','特湿']
result_monthly = pd.read_pickle(trans_data_path+'dourght_monthly.pickle')
result_monthly.columns = ['year', 'month', 'station', 'p']

range_all = [40,20,20,20,20,10,10,10,20]
linegress = pd.DataFrame()
for year in range(1960, 2019, 5):
    sdata_row = result_monthly[(result_monthly['year'] >= year) & (result_monthly['year'] < year + 5)]


    def classify(result_yearly):
        result = []
        for count, i in enumerate([-100, -0.8, -0.6, -0.4, -0.2, 0.2, 0.4, 0.6, 0.8]):
            if i == 0.8:
                result.append(result_yearly['p'][(result_yearly['p'] >= i) & (result_yearly['p'] < i + 100)].shape[0])
            elif i == -100:
                result.append(result_yearly['p'][(result_yearly['p'] >= i) & (result_yearly['p'] < -0.8)].shape[0])

            else:
                result.append(result_yearly['p'][(result_yearly['p'] >= i) & (result_yearly['p'] < i + 0.2)].shape[0])
        return result

    sdata = sdata_row.groupby(sdata_row['station']).apply(lambda x: classify(x)).reset_index().set_index('station')

    for si in range(9):
        sdata['%i'%(si)] = np.array([int(i[si]) for i in sdata[0]])
    # add right lat, lon
    ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
    sll = ll.loc[sdata.index]
    sdata['lat'] = sll['lat']
    sdata['lon'] = sll['lon']
    bbb = sdata.max()
    # sdata[0] = np.repeat(year, sdata.shape[0])
    # linegress = pd.concat([linegress,sdata])
    # print(linegress)
    for i in range(9):
        # print(sdata.max())
        erange = bbb['%i'%i]
        if bbb['%i'%i] == 0:
            erange = 1
        else:
            print('%i年-%i年 %s天数分布'%(year, year+4, vari_name[i]), erange)
            import cmaps
            if i <4:
                plotss(sdata, '%i'%i, '%i年-%i年 %s月数分布'%(year, year+4, vari_name[i]), [0, range_all[i]], cmaps.MPL_YlOrRd)
            else:
                plotss(sdata, '%i'%i, '%i年-%i年 %s月数分布'%(year, year+4, vari_name[i]), [0, range_all[i]], cmaps.WhiteBlue)


# linegress.to_pickle('drought_lin.pickle')

