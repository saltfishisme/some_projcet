import pandas as pd
import numpy as np
row_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data/'
trans_data_path = r"D:\basis\some_projects\zhj\rainstorm\trans_data/"

'''1，历史资料分析某个省份或者沿海风的变化趋势；2，可以分析cmip6的资料，看看未来全国风资源的变化。'''


def screen_data():
    '''处理缺失数据'''
    def for_999999(data):
        data[data == 999999] = np.nan
        data[data == 999998] = np.nan
        data[data == 999996] = np.nan
        data[data == 999990] = 0.01
        data[data > 999800] = (data[data > 998000] - 998000)*10
        data[data > 999700] = (data[data > 999700] - 999700)*10
        data[data > 999600] = (data[data > 999600] - 999600)*10
        return data

    def for_32766(data):
        data[data==32766] = np.nan
        data[data == 32700] = 0.01
        data[data > 32000] = data[data > 32000] - 32000
        data[data > 31000] = data[data > 31000] - 31000
        data[data > 30000] = data[data > 30000] - 30000
        return data
    # 警告，先9再3

    data = pd.read_csv(r'D:\basis\data\row\PRE.txt', delim_whitespace=True, header=None, usecols=[0,4,5,6,9])
    print(data)
    data[9] = for_999999(data[9])
    data[9] = for_32766(data[9])
    data[9] = data[9]/10

    data = data[data[4]>=1960]
    data = data[data[4]<=2019]
    data = data.set_index(pd.to_datetime(data[4] * 10000 + data[5] * 100 + data[6], format='%Y%m%d'))
    data.to_pickle('data_mask.pickle')

# screen_data()

# 计算季均降水 年均降水
# data = pd.read_pickle(trans_data_path+'data_mask.pickle')
#
# data = data.groupby([data.index.year, data.index.month, data[0]]).sum().reset_index()
# data.columns = ['year', 'month', 'station', 'p']
#
# data['season'] = 1
# for i, month in enumerate([[3, 4, 5], [6, 7, 8], [9, 10, 11], [12, 1, 2]]):
#     data['season'][(data['month'] == month[0]) | (data['month'] == month[1]) | (data['month'] == month[2])]=month[0]
#
# result_seasonly = data['p'].groupby([data['year'],data['season'], data['station']]).sum().reset_index()
# result_seasonly.to_pickle(trans_data_path+'pre_seasonly.pickle')
#
# result_yearly = data['p'].groupby([data['year'], data['station']]).sum().reset_index()
#
# result_yearly.to_pickle(trans_data_path+'pre_yearly.pickle')

# 计算季累计降水距平
# def anomaly(data):
#     means = data['p'].mean()
#     data['p'] = np.array(data['p'])-means
#     result['p'].iloc[data.index] = data['p']
#     return means
#
# result_seasonly = pd.read_pickle(trans_data_path+'pre_seasonly.pickle')
# print(result_seasonly)
# result = result_seasonly.copy()
# sanomaly = result_seasonly.groupby([result_seasonly['station'], result_seasonly['season']]).apply(lambda x:anomaly(x))
# result.to_pickle(trans_data_path+'pre_anomaly_seasonly.pickle')


# 计算年均降水距平
# def anomaly(data):
#     means = data['p'].mean()
#     data['p'] = np.array(data['p'])-means
#     result['p'].iloc[data.index] = data['p']
#     return means
#
# result_yearly = pd.read_pickle(trans_data_path + 'pre_yearly.pickle')
# result = result_yearly.copy()
# sanomaly = result_yearly.groupby(result_yearly['station']).apply(lambda x:anomaly(x))
# result.to_pickle(trans_data_path+'pre_anomaly_yearly.pickle')
################################################################################################
'''111111'''
# def plotss(sdata, vari, title, ranges, colormap):
#     import numpy as np
#     import matplotlib.pyplot as plt
#     import proplot as ZhengZhou
#     import cmaps
#     import cartopy.crs as ccrs
#     def rbf(ranges, datas, vari):
#         from scipy.interpolate import Rbf
#         olon = np.linspace(ranges[0][2], ranges[0][3], 88)
#         olat = np.linspace(ranges[0][0], ranges[0][1], 88)
#
#         olon, olat = np.meshgrid(olon, olat)
#         # 插值处理
#         lon = datas['lon']
#         lat = datas['lat']
#         data = np.array(datas[vari])
#         func = Rbf(lon, lat, data, function='linear')
#         data_new = func(olon, olat)
#         return olon, olat, data_new
#
#     lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, vari)
#
#     fig, ax = ZhengZhou.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[3,2.9], tight=False
#                             , left='1em', top='1.5em', bottom='2em')
#     def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0,60]):
#
#         def plots(ax):
#             cb = ax.contourf(lons, lats, type1, levels= levels, extend='both',transform=ccrs.PlateCarree()
#                              , cmap='Negpos')
#
#             ax.colorbar(cb, loc='b', length=0.8, width='0.5em', space='0.5em',
#                         locator=tickslevel, ticklabels=[str(round(i,2)) for i in tickslevel])
#             from plot_in_proplot import shp_clip_for_proplot
#             shp_clip_for_proplot(ax, fill=cb, clipshape=True)
#
#         def plotss(ax):
#             # cb = ax.contourf(lons, lats, type1, levels=levels, transform=ccrs.PlateCarree(), cmap=colormap)
#             cb = ax.contourf(lons, lats, type1, levels=levels, transform=ccrs.PlateCarree(), colors=colormap)
#             ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
#             from plot_in_proplot import shp_clip_for_proplot
#             shp_clip_for_proplot(ax, fill=cb, clipshape=True)
#
#         plots(ax)
#         ax.format(latlim=[17, 54], lonlim=[80, 127])
#
#         ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
#         plotss(ax2)
#         from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
#         ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
#         ax2.set_axes_locator(ip)
#
#     level1 = list(range(ranges[0], ranges[1], ranges[2]))
#
#     level1.insert(0, -200)
#     level1.extend([200])
#
#     all(ax[0], type1, level1, np.arange(ranges[0], ranges[1], ranges[3]), [0.2, 0.5, 0.2, 0.2], ranges)
#
#     # ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
#     ax.format(suptitle=title)
#     plt.rcParams['font.family'] = ['sans-serif']
#     plt.rcParams['font.sans-serif'] = ['SimHei']
#     # plt.show()
#     plt.savefig('pre_%s.png'%(title), dpi=500)
#     plt.close()

def plotss(sdata, vari, title, ranges):
    import matplotlib.pyplot as plt
    import cartopy.crs as ccrs
    import numpy as np
    import cmaps
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams['font.sans-serif'] = ['SimHei']
    fig = plt.figure(figsize=[4.2,4.2])

    # 准备数据
    def rbf(ranges, datas, vari):
        from scipy.interpolate import Rbf
        olon = np.linspace(ranges[0][2], ranges[0][3], 88)
        olat = np.linspace(ranges[0][0], ranges[0][1], 88)

        olon, olat = np.meshgrid(olon, olat)
        # 插值处理
        lon = datas['lon']
        lat = datas['lat']
        data = np.array(datas[vari])
        func = Rbf(lon, lat, data, function='linear')
        data_new = func(olon, olat)
        return olon, olat, data_new

    lons, lats, data = rbf([[2.5, 60, 73, 135]], sdata, vari)

    levels = list(range(ranges[0], ranges[1], ranges[2]))
    # levels.insert(0, -200)
    # levels.extend([200])

    def plot_ax(position):
        sax = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105),
                           frameon=False)
        sax.patch.set_visible(False)
        import matplotlib
        # norm = matplotlib.colors.BoundaryNorm(levels, len(levels))
        import matplotlib.colors as p_colors
        divnorm = p_colors.TwoSlopeNorm(vmin=levels[0], vcenter=0, vmax=levels[-1])
        cb = sax.contourf(lons, lats, data, levels=levels, extend='both', norm=divnorm
                          , transform=ccrs.PlateCarree()
                          , cmap=cmaps.NCV_blu_red)

        from plot_in_proplot import shp_clip_for_proplot
        shp_clip_for_proplot(sax, fill=cb, clipshape=True)


        return sax, cb

    # ax1 single
    ax1, cb = plot_ax([0.01, 0.05, 0.98, 0.89])
    ax1.gridlines(alpha=0.5)
    im_ratio = data.shape[1] / data.shape[0]
    plt.colorbar(cb, ax=ax1, fraction=0.04 * im_ratio, pad=0.01, aspect=50,orientation="horizontal",
                 ticks=list(range(ranges[0], ranges[1], ranges[3])), format='%i')
    ax1.set_extent([80, 127, 15, 54], ccrs.Geodetic())

    # ax2 single
    ax2, cb2 = plot_ax([0, 0, 0.12, 0.25])
    ax2.set_extent([105, 122.5, 2.5, 25], ccrs.Geodetic())
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax1, [0, -0.056, 0.12, 0.28])
    ax2.set_axes_locator(ip)

    # add province capatical

    ax1.set_title(title)
    # plt.show()
    title = title.split('/')[0]
    plt.savefig('%s.png' % title, dpi=500)
    plt.close()

# result_yearly = pd.read_pickle('pre_anomaly_seasonly.pickle')
# print(result_yearly)
season_number = [0,3,6,9,12]
season_name = ['', '春季', '夏季', '秋季', '冬季']
ranges_all = [[-300, 301, 40, 100], [-120, 121, 10, 40], [-250, 251, 20, 40], [-160, 161, 10, 40], [-80, 81, 5, 20]]

def main(result_yearly, season, ranges, unit):
    maxs = []; mins = []
    for year in range(1960, 2018, 5):
        sdata_row = result_yearly[(result_yearly['year'] >= year) & (result_yearly['year'] < year + 5)]
        sdata = sdata_row.groupby(sdata_row['station']).mean().reset_index().set_index('station')

        # add right lat, lon
        ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
        sll = ll.loc[sdata.index]
        sdata['lat'] = sll['lat']
        sdata['lon'] = sll['lon']
        print('%i年-%i年%s降水距平分布'%(year, year+4, season))

        maxs.append(sdata.max()[2]); mins.append((sdata.min()[2]))

        plotss(sdata, 'p', '%i年-%i年%s降水距平分布(%s)'%(year, year+4, season, unit), ranges)

    return maxs, mins



for i in range(4,5):
    if i == 0:
        sresult_yearly = pd.read_pickle(trans_data_path+'pre_anomaly_yearly.pickle')
        units = 'mm/年'
    else:
        result_anomaly_season = pd.read_pickle(trans_data_path+'pre_anomaly_seasonly.pickle')
        sresult_yearly = result_anomaly_season[result_anomaly_season['season']==season_number[i]]
        print(sresult_yearly)
        units = 'mm/季'
    maxs, mins = main(sresult_yearly, season_name[i], ranges_all[i], units)
    print(np.percentile(maxs,0.5), np.percentile(mins,0.5))



