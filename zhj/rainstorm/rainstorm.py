import pandas as pd
import numpy as np


'''1，历史资料分析某个省份或者沿海风的变化趋势；2，可以分析cmip6的资料，看看未来全国风资源的变化。'''


def screen_data():
    '''处理缺失数据'''
    def for_999999(data):
        data[data == 999999] = np.nan
        data[data == 999998] = np.nan
        data[data == 999996] = np.nan
        data[data == 999990] = 0.01
        data[data > 999800] = (data[data > 998000] - 998000)*10
        data[data > 999700] = (data[data > 999700] - 999700)*10
        data[data > 999600] = (data[data > 999600] - 999600)*10
        return data

    def for_32766(data):
        data[data==32766] = np.nan
        data[data == 32700] = 0.01
        data[data > 32000] = data[data > 32000] - 32000
        data[data > 31000] = data[data > 31000] - 31000
        data[data > 30000] = data[data > 30000] - 30000
        return data
    # 警告，先9再3

    data = pd.read_csv(r'D:\basis\data\row\PRE.txt', delim_whitespace=True, header=None, usecols=[0,4,5,6,9])
    print(data)
    data[9] = for_999999(data[9])
    data[9] = for_32766(data[9])
    data[9] = data[9]/10

    data = data[data[4]>=1960]
    data = data[data[4]<=2019]
    data = data.set_index(pd.to_datetime(data[4] * 10000 + data[5] * 100 + data[6], format='%Y%m%d'))
    data.to_pickle('data_mask.pickle')

# screen_data()


# data = pd.read_pickle('data_mask.pickle')
# print(data.max(), data.min())
# # 暴雨
# def rainstorm_charac(data):
#     rainstorm = data[data[9]>50]
#     r_days= rainstorm.shape[0]
#     if r_days != 0:
#         r_intensity= rainstorm[9].sum()/r_days
#     else:
#         r_intensity = 0
#     return [r_days, r_intensity]
#
# result_yearly = data.groupby([data.index.year, data[0]]).apply(lambda x: rainstorm_charac(x))
#
# result_yearly.to_pickle('result_yearly.pickle')

result_yearly = pd.DataFrame(pd.read_pickle('result_yearly.pickle').reindex())
result_yearly.columns=['p']
result_yearly = result_yearly.reset_index()
result_yearly.columns=['year','station', 'p']
result_yearly['days'] = np.array([int(i[0]) for i in result_yearly['p']])
result_yearly['intensity'] = np.array([int(i[1]) for i in result_yearly['p']])
result_yearly = result_yearly[['year','station','days', 'intensity']]
print(pd.read_pickle('rain_year_lin.pickle'))
print(result_yearly)
# skip 0

# def find_ll(data):

# add right lat, lon
def plotss(sdata, title):
    import numpy as np
    import matplotlib.pyplot  as plt
    import proplot as plot
    import cmaps
    import cartopy.crs as ccrs
    def rbf(ranges, datas, vari):
        from scipy.interpolate import Rbf
        olon = np.linspace(ranges[0][2], ranges[0][3], 88)
        olat = np.linspace(ranges[0][0], ranges[0][1], 88)

        olon, olat = np.meshgrid(olon, olat)
        # 插值处理
        lon = datas['lon']
        lat = datas['lat']
        data = np.array(datas[vari])
        func = Rbf(lon, lat, data, function='linear')
        data_new = func(olon, olat)
        return olon, olat, data_new

    lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, 'days')
    lons, lats, type2 = rbf([[2.5, 60, 73, 135]], sdata, 'intensity')
    type1 = np.ma.masked_where(type1 < 0, type1)
    type2 = np.ma.masked_where(type2 < 50, type2)
    fig, ax = plot.subplots(ncols=2, projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[6,2.9], tight=False
                            , left='1em', top='1.5em', bottom='2em')
    def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0,60]):
        def plots(ax):
            print(levels)
            cb = ax.contourf(lons, lats, type1, levels= levels, extend='both',transform=ccrs.PlateCarree(), cmap=cmaps.WhiteBlue)
            # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
            #             locator=leveltick, ticklabels=levellabel)
            ax.colorbar(cb, loc='b', length=0.8, width='0.5em', space='0.5em',
                        locator=tickslevel)
            # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
            # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

            from plot_in_proplot import shp_clip_for_proplot
            shp_clip_for_proplot(ax, fill=cb, clipshape=True)

        def plotss(ax):
            cb = ax.contourf(lons, lats, type1, levels= levels, transform=ccrs.PlateCarree(), cmap=cmaps.WhiteBlue, vmin=ax2_ranges[0],vmax=ax2_ranges[1])

            ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
            from plot_in_proplot import shp_clip_for_proplot
            shp_clip_for_proplot(ax, fill=cb, clipshape=True)

        plots(ax)
        ax.format(latlim=[17, 54], lonlim=[80, 127])

        ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
        plotss(ax2)
        from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

        ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
        ax2.set_axes_locator(ip)
    level1 = list(range(0,61,5))
    level1.extend([85])
    level2 = list(range(50,111,5))
    level2.extend([145])
    all(ax[0], type1, level1, list(range(0,61,10)), [0.2, 0.5, 0.2, 0.2], [0,60])
    all(ax[1], type2, level2,list(range(50,111,10)), [0.3, 0.5, 0.2, 0.2], [50,110])
    ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
    ax[1].format(title='b)暴雨强度(mm/d)', titleloc='ul')
    ax.format(suptitle=title)
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams['font.sans-serif'] = ['SimHei']
    # plt.show()
    plt.savefig('days_intensity_%s.png'%title, dpi=500)

linegress = pd.DataFrame()

for year in range(1960, 2019, 1):
    sdata_row = result_yearly[(result_yearly['year'] >= year) & (result_yearly['year'] < year + 5)]
    sdata = sdata_row.groupby(sdata_row['station']).sum().reset_index().set_index('station')

    sdata_row['intensity'] = sdata_row['intensity'].replace(0, np.nan)
    sdata['intensity'] = sdata_row['intensity'].groupby(sdata_row['station']).mean().reset_index().set_index('station')
    sdata['intensity'] = sdata['intensity'].fillna(0)

    # add right lat, lon
    ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
    sll = ll.loc[sdata.index]
    sdata['lat'] = sll['lat']
    sdata['lon'] = sll['lon']
    # print(sdata.max())
    sdata['year'] = np.repeat(year, sdata.shape[0])
    linegress = pd.concat([ linegress, sdata])
    # plotss(sdata, '%i年-%i年'%(year, year+4))
linegress.to_pickle('rain_year_lin.pickle')


