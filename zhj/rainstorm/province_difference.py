





import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import shapely.geometry as sgeom

import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
import numpy as np
import pandas as pd


def plot(data, columns_name_plot_in_province_capatical_data, title, colorbar_kw):
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams['font.sans-serif'] = ['SimHei']
    fig = plt.figure(figsize=[2.8,2.8])
    print(data)
    # add colorbar
    def create_colorbar(ax_cb, labels, label_color):
        import matplotlib.pyplot as plt
        from matplotlib import colors
        import numpy as np
        yticks = np.linspace(0, 1, len(labels)).take(np.arange(0,len(labels), 3))
        labels_less_1 = np.argwhere(np.abs(labels) < 1).flatten().shape[0]
        if (labels_less_1 > 1) & (labels_less_1 < 3):
            yticklabels = np.array([str(np.round(i, 1)) for i in labels]).take(np.arange(0,len(labels), 3))
        elif (labels_less_1 > 2):
            yticklabels = np.array([str(np.round(i, 2)) for i in labels]).take(np.arange(0,len(labels), 3))
        else:
            yticklabels = np.array([str(int(i)) for i in labels]).take(np.arange(0,len(labels), 3))

        colormap = label_color
        tmap = colors.LinearSegmentedColormap.from_list('cmap', colormap)
        sm = plt.cm.ScalarMappable(cmap=tmap, norm=plt.Normalize(vmin=0, vmax=1))
        cbar = plt.colorbar(sm, alpha=0.5,ticks=yticks, cax=ax_cb, orientation='horizontal')

        cbar.ax.set_xticklabels(yticklabels, fontsize=6)
        # plt.show()

    cb_ax = fig.add_axes([0.05, 0.08, 0.9, 0.02])
    create_colorbar(cb_ax, colorbar_kw[0], colorbar_kw[1])
    #################################
    ax = fig.add_axes([0, 0.11, 1, 0.82], projection=ccrs.AlbersEqualArea(central_longitude=105),
                      frameon=False)
    ax.patch.set_visible(False)

    ax.set_extent([80,127, 15, 54], ccrs.Geodetic())

    colors = np.array(data['colors'])

    for i, geo in enumerate(shpreader.Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries()):
        if i == 33:
            continue
        ax.add_geometries(geo,
            ccrs.PlateCarree(), facecolor=colors[i], edgecolor='black', linewidth=0.5, alpha=0.5)
    from plot_in_proplot import shp_clip_for_proplot
    shp_clip_for_proplot(ax, clipshape=False)
    def sub_ax():
        ax2 = fig.add_axes([0, 0, 0.12, 0.25], projection=ccrs.AlbersEqualArea(central_longitude=105),
                          frameon=False)
        ax2.patch.set_visible(False)
        ax2.set_extent([105, 122.5, 2.5, 25], ccrs.Geodetic())
        colors = np.array(data['colors'])
        for i, geo in enumerate(shpreader.Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries()):
            if i == 33:
                continue
            ax2.add_geometries(geo,
                              ccrs.PlateCarree(), facecolor=colors[i], edgecolor='black', linewidth=0.5, alpha=0.5)
        from plot_in_proplot import shp_clip_for_proplot
        shp_clip_for_proplot(ax2, clipshape=False)
        return ax2

    ax2 = sub_ax()
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0, -0.056, 0.12, 0.28])
    ax2.set_axes_locator(ip)
    # add province capatical


    data_nonan = data.copy()
    data_nonan = data_nonan.dropna()
    lons = np.array(data_nonan['lon'])
    lats = np.array(data_nonan['lat'])
    p_c_name = np.array(data_nonan[columns_name_plot_in_province_capatical_data])
    for i in range(data_nonan.shape[0]):
        slon = lons[i]; slat = lats[i]; s_p_c_name = p_c_name[i]
        from matplotlib.transforms import offset_copy

        ax.plot(slon, slat, marker='o', color='black', markersize=2,
                alpha=0.7, transform=ccrs.Geodetic())

        geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
        text_transform = offset_copy(geodetic_transform, units='dots', x=-25)
        from matplotlib import colors as pakage_colors

        # Add text 25 pixels to the left of the volcano.
        fc = pakage_colors.to_rgba('white')
        fc = fc[:-1] + (0.6,)
        ax.text(slon, slat, u'%s'%np.round(s_p_c_name, 1), fontsize=3.8, weight='bold',
                verticalalignment='center', horizontalalignment='right',
                transform=text_transform,
                bbox=dict(facecolor=fc,boxstyle='round', edgecolor='black', linewidth=0.1))

    try:
        print(title)
        title_1, title_2 = title.split('/')
        ax.set_title(title_1+'差值(%s/年)'%title_2, fontsize=9)
        print(title_1)
        # plt.show()
        plt.savefig('%s.png' % title_1, dpi=500)
    except:
        print(title)
        ax.set_title(title+'差值(次/年)', fontsize=9)
        # plt.show()
        plt.savefig('%s.png' % title, dpi=500)

    plt.close()


def select_colors(data_row, levels=None):
    '''
    input: array, 1-D
    '''
    colors_blue = ['#0404FF','#1F1FFF','#2121FF','#2A2AFF','#3D3DFF','#5858FF','#7373FF','#8080FF','#8D8DFF','#A6A6FF','#C0C0FF','#D1D1FF','#D9D9FF','#DBDBFF','#F2F2FF']
    colors_red = ['#FFF2F2','#FFD9D9','#FFC0C0','#FFA6A6','#FF8D8D','#FF7373','#FF5E5E','#FF5858','#FF5353','#FF3D3D','#FF2121','#FF0505','#FF0404']

    neg_values_index = np.argwhere(data_row<=0).flatten()
    pos_values_index = np.argwhere(data_row>0).flatten()

    bin_all = np.array([str('#F2F2FF') for i in data_row])
    label_all = []
    colors_all = []
    def find_colors(colors, data):
        if levels is None:
            colors_len = len(colors)
        bin, label = pd.qcut(data, colors_len, duplicates='drop', retbins=True)
        label = np.array(label)
        # map label to color
        #    按照间隔挑选颜色，避免都是浓郁色彩
        select_number = label.shape[0]-1
        if select_number != len(colors):
            ss = np.linspace(0, len(colors)-1, select_number).astype(int)
            colors = np.array(colors).take(ss)
        bin.categories = colors
        label_all.extend(label)
        colors_all.extend(colors)
        return np.array(bin), [label, colors]
    if len(neg_values_index) > 1:
        bin_blue, kw_blue = find_colors(colors_blue, data_row[neg_values_index])
        bin_all[neg_values_index] = bin_blue
    elif len(neg_values_index) == 1:
        label_all.extend(data_row[neg_values_index])
        colors_all.extend(['#8D8DFF'])
        bin_all[neg_values_index] = '#8D8DFF'
    if len(pos_values_index) > 0:
        bin_red, kw_red = find_colors(colors_red, data_row[pos_values_index])
        bin_all[pos_values_index] = bin_red
    print(data_row)

    return bin_all, [label_all, colors_all]



def match_the_order_of_shp(data, match_columns_name):
    province_information = pd.read_csv(r'D:/basis/data/shp/shengjie/attribute_4p.csv', encoding='gbk')
    data['match_name'] = np.array([str(i)[:2] for i in np.array(data[match_columns_name])])
    b = pd.merge(province_information, data, on=['match_name'], how='outer')
    return b

count = 0
row_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data'
trans_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data'

filename_s = ['2010_2014_flood', '2015_2018_flood',
              '2010_2014_drought', '2015_2018_drought']

use_col_s=[['地区','受灾人口/万人','死亡人口/人','失踪人口/人','直接经济损失/亿元',
'农作物受灾面积/千公顷','农作物成灾面积/千公顷','农作物绝收面积/千公顷','因灾粮食减产/亿公斤',
'经济作物损失/亿元','停产工矿企业/个','铁路中断/条次','公路中断/条次',
'机场、港口临时关停/个次','供电线路中断/条次','通信中断/条次','损坏水库大中型/座',
'损坏水库小型/座','损坏提防处数/处','长度/公里','损坏水闸/座','水利设施损失/亿元'],
['地区','受灾人口/万人','死亡人口/人','失踪人口/人','直接经济损失/亿元',
'农作物受灾面积/千公顷','农作物成灾面积/千公顷','农作物绝收面积/千公顷','因灾粮食减产/亿公斤',
'经济作物损失/亿元','停产工矿企业/个','铁路中断/条次','公路中断/条次',
'机场、港口临时关停/个次','供电线路中断/条次','通信中断/条次','损坏水库大中型/座',
'损坏水库小型/座','损坏提防处数/处','长度/公里','损坏水闸/座','水利设施损失/亿元'],
['地区','作物受灾面积/千公顷','作物成灾面积/千公顷','作物绝收面积/千公顷','因旱饮水困难人口/万人',
'因旱饮水困难大牲畜/万头'],
['地区','作物受灾面积/千公顷','作物成灾面积/千公顷','作物绝收面积/千公顷','因旱饮水困难人口/万人',
'因旱饮水困难大牲畜/万头']]
use_col_intersection1 = np.intersect1d(np.array(use_col_s[0][1:]), np.array(use_col_s[1][1:]))
use_col_intersection2 = np.intersect1d(np.array(use_col_s[2][1:]), np.array(use_col_s[3][1:]))
use_col_s=[['地区','供电线路中断/条次', '停产工矿企业/个', '公路中断/条次', '农作物受灾面积/千公顷', '农作物成灾面积/千公顷',
       '农作物绝收面积/千公顷', '受灾人口/万人', '因灾粮食减产/亿公斤', '失踪人口/人', '损坏提防处数/处',
       '损坏水库大中型/座', '损坏水库小型/座', '损坏水闸/座', '机场、港口临时关停/个次', '死亡人口/人',
       '水利设施损失/亿元', '直接经济损失/亿元', '经济作物损失/亿元', '通信中断/条次', '铁路中断/条次',
       '长度/公里'],
           ['地区','作物受灾面积/千公顷', '作物成灾面积/千公顷', '作物绝收面积/千公顷', '因旱饮水困难人口/万人',
       '因旱饮水困难大牲畜/万头']]
use_col_s=[['地区','农作物受灾面积/千公顷'],
           ['地区','作物受灾面积/千公顷']]
use_col_s=[['地区.4','主要灾害次数'],
           ['地区.2','主要灾害次数']]

title_s = ['洪水导致',
           '干旱导致']


for i in [0,1]:
    use_col = use_col_s[i]
    title = title_s[i]

    fill0 = False
    if use_col[1] == '主要灾害次数':
        fill0 = True

    data1 = pd.read_csv(row_data_path+'/%s.csv'%filename_s[i*2], encoding='gbk')
    data2 = pd.read_csv(row_data_path + '/%s.csv' % filename_s[i*2+1], encoding='gbk')

    if fill0 is True:
        data1 = data1.fillna(0)
        data2 = data2.fillna(0)


    sdata = data2[use_col[1:]].div(4.0) - data1[use_col[1:]].div(5.0)

    sdata[use_col[0]] = data2[use_col[0]]

    sdata = sdata.dropna()

    def different_vari(sdata, columns_name_plot_in_province_capatical_data):
        colors, colorbar_kw = select_colors(np.array(sdata[columns_name_plot_in_province_capatical_data]))
        sdata['colors'] = colors

        sdata = match_the_order_of_shp(sdata, use_col[0])
        ### For provinces without data, fill colors with white '#FFFFFF'
        sdata['colors'] = sdata['colors'].fillna('#FFFFFF')
        plot(sdata,columns_name_plot_in_province_capatical_data, title_s[i]+columns_name_plot_in_province_capatical_data, colorbar_kw)

    for vari in use_col[1:]:
        different_vari(sdata.copy(), vari)