row_data_path = r'D:\basis\some_projects\zhj\rainstorm\row_data/'
trans_data_path = r"D:\basis\some_projects\zhj\rainstorm\trans_data/"


def drought():
    import pandas as pd
    import numpy as np

    def plotss(sdata, vari, title, ranges, colormap):
        import numpy as np
        import matplotlib.pyplot as plt
        import proplot as plot
        import cmaps
        import cartopy.crs as ccrs
        def rbf(ranges, datas, vari):
            from scipy.interpolate import Rbf
            olon = np.linspace(ranges[0][2], ranges[0][3], 88)
            olat = np.linspace(ranges[0][0], ranges[0][1], 88)

            olon, olat = np.meshgrid(olon, olat)
            # 插值处理
            lon = datas['lon']
            lat = datas['lat']
            data = np.array(datas[vari])
            func = Rbf(lon, lat, data, function='linear')
            data_new = func(olon, olat)
            return olon, olat, data_new

        lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, vari)

        fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[3,2.9], tight=False
                                , left='1em', top='1.5em', bottom='2em')
        def all(ax, type1, levels, tickslevel, position, ax2_ranges=[0,60]):
            def plots(ax):
                cb = ax.contourf(lons, lats, type1, levels= levels, extend='both',transform=ccrs.PlateCarree(), cmap=colormap)
                # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
                #             locator=leveltick, ticklabels=levellabel)
                ax.colorbar(cb, loc='b', length=0.8, width='0.5em', space='0.5em',
                            locator=tickslevel, ticklabels=[str(i) for i in tickslevel])
                # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
                # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            def plotss(ax):
                if ranges[1] == 6:
                    colors =['#4646FF','#7676FF','#9898FF','#A2A2FF','#D2D2FF','#FFFCFC','#FFD2D2','#FFA0A0','#FF7474','#FF4444','#FF1414','#FF0000']
                else:
                    colors = ['#2424FF','#3C3CFF','#5656FF','#6E6EFF','#8686FF','#9E9EFF','#B4B4FF','#BCBCFF','#CECEFF','#E6E6FF','#FEFEFF','#FFE4E4','#FFCCCC','#FFB4B4','#FF9C9C','#FF8484','#FF6C6C','#FF5454','#FF3C3C','#FF2424','#FF0c0c','#FF0505','#F10000','#FF0000']
                print(colors)
                cb = ax.contourf(lons, lats, type1, levels=levels, transform=ccrs.PlateCarree(), colors=colors)

                ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            plots(ax)
            ax.format(latlim=[17, 54], lonlim=[80, 127])

            ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
            plotss(ax2)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

            ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
            ax2.set_axes_locator(ip)


        level1 = list(range(ranges[0], ranges[1], 1))
        level1.remove(0)
        level1.insert(0, -100)
        level1.extend([100])

        all(ax[0], type1, level1, list(range(ranges[0], ranges[1], ranges[2])), [0.2, 0.5, 0.2, 0.2], ranges)

        # ax[0].format(title='a)暴雨日数(d)', titleloc='ul')
        ax.format(suptitle=title)
        plt.rcParams['font.family'] = ['sans-serif']
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig('drought_距平%s.png'%(title), dpi=500)
        plt.close()

    vari_name = ['特旱', '重旱','中旱','轻旱','正常','微湿','中湿','重湿','特湿']
    result_monthly = pd.read_pickle(trans_data_path+'dourght_monthly.pickle')
    result_monthly.columns = ['year', 'month', 'station', 'p']

    range_all = [10,5,5,5,5,5,5,5,10]

    for year in range(1960, 2019, 5):
        sdata_row = result_monthly[(result_monthly['year'] >= year) & (result_monthly['year'] < year + 5)]

        def classify(result_yearly):
            result = []
            for count, i in enumerate([-100, -0.8, -0.6, -0.4, -0.2, 0.2, 0.4, 0.6, 0.8]):
                if i == 0.8:
                    result.append(result_yearly['p'][(result_yearly['p'] >= i) & (result_yearly['p'] < i + 100)].shape[0])
                elif i == -100:
                    result.append(result_yearly['p'][(result_yearly['p'] >= i) & (result_yearly['p'] < -0.8)].shape[0])

                else:
                    result.append(result_yearly['p'][(result_yearly['p'] >= i) & (result_yearly['p'] < i + 0.2)].shape[0])
            return result

        sdata = sdata_row.groupby(sdata_row['station']).apply(lambda x: classify(x)).reset_index().set_index('station')

        for si in range(9):
            sdata['%i'%(si)] = np.array([int(i[si]) for i in sdata[0]])
        # add right lat, lon
        ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
        sll = ll.loc[sdata.index]
        sdata['lat'] = sll['lat']
        sdata['lon'] = sll['lon']
        bbb = sdata.max()
        means = pd.read_pickle(trans_data_path+'means_drought.pickle')
        print(means.columns)
        use_col = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        for i in use_col:
            sdata['means'] = means[str(i)]
            sdata[str(i)] = sdata[str(i)] - sdata['means']
        import cmaps
        # print(linegress)
        for i in range(9):
            # print(sdata.max())
            plotss(sdata, '%i' % i, '%i年-%i年 %s月数距平' % (year, year + 4, vari_name[i]), [-range_all[i], range_all[i]+1,2],
                   cmaps.MPL_bwr)


drought()

def rain():
    import pandas as pd
    import numpy as np

    result_yearly = pd.DataFrame(pd.read_pickle('result_yearly.pickle').reindex())
    result_yearly.columns = ['p']
    result_yearly = result_yearly.reset_index()
    result_yearly.columns = ['year', 'station', 'p']
    result_yearly['days'] = np.array([int(i[0]) for i in result_yearly['p']])
    result_yearly['intensity'] = np.array([int(i[1]) for i in result_yearly['p']])
    result_yearly = result_yearly[['year', 'station', 'days', 'intensity']]


    def plotss(sdata, title, ranges1, ranges2):
        import numpy as np
        import matplotlib.pyplot  as plt
        import proplot as plot
        import cmaps
        import cartopy.crs as ccrs
        def rbf(ranges, datas, vari):
            from scipy.interpolate import Rbf
            olon = np.linspace(ranges[0][2], ranges[0][3], 88)
            olat = np.linspace(ranges[0][0], ranges[0][1], 88)

            olon, olat = np.meshgrid(olon, olat)
            # 插值处理
            lon = datas['lon']
            lat = datas['lat']
            data = np.array(datas[vari])
            func = Rbf(lon, lat, data, function='linear')
            data_new = func(olon, olat)
            return olon, olat, data_new

        lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, 'days')
        lons, lats, type2 = rbf([[2.5, 60, 73, 135]], sdata, 'intensity')
        # type1 = np.ma.masked_where(type1 < 0, type1)
        # type2 = np.ma.masked_where(type2 < 50, type2)
        fig, ax = plot.subplots(ncols=2, projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[6, 2.9],
                                tight=False
                                , left='1em', top='1.5em', bottom='2em')

        def all(ax, type1, levels, tickslevel, position, ax2_true=False):
            def plots(ax):
                print(levels)
                cb = ax.contourf(lons, lats, type1, levels=levels, extend='both', transform=ccrs.PlateCarree(),
                                 cmap=cmaps.MPL_bwr)
                # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
                #             locator=leveltick, ticklabels=levellabel)
                ax.colorbar(cb, loc='b', length=0.8, width='0.5em', space='0.5em',
                            locator=tickslevel, ticklabels=[str(i) for i in tickslevel])
                # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
                # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            def plotss(ax):
                if ax2_true is not True:
                    colors=['#0e0eFF','#2C2CFF','#4E4EFF','#6868FF','#8A8AFF','#A6A6FF','#C6C6FF','#E2E2FF','#FFFCFC','#FFE0E0','#FFC4C4','#FFA4A4','#FF8888','#FF6868','#FF4C4C','#FF2C2C','#FF0c0c','#FF0000']
                else:
                    colors=['#0202FF','#0a0aFF','#1A1AFF','#2A2AFF','#3A3AFF','#4848FF','#5858FF','#6868FF','#7878FF','#8A8AFF','#9A9AFF','#AAAAFF','#BABAFF','#CACAFF','#DADAFF','#EAEAFF','#FAFAFF','#FFF8F8','#FFE8E8','#FFD8D8','#FFC8C8','#FFB8B8','#FFA8A8','#FF9898','#FF8888','#FF7878','#FF6868','#FF5858','#FF4848','#FF3838','#FF2828','#FF1818','#FF0808','#FF0000']
                cb = ax.contourf(lons, lats, type1, levels=levels, transform=ccrs.PlateCarree(), colors=colors)

                ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            plots(ax)
            ax.format(latlim=[17, 54], lonlim=[80, 127])

            ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
            plotss(ax2)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

            ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
            ax2.set_axes_locator(ip)

        level1 = list(range(ranges1[0], ranges1[1], 2))

        level1.insert(0, -100)
        level1.extend([100])
        print(level1)

        level2 = list(range(ranges2[0], ranges2[1], 2))
        level2.insert(0, -200)
        level2.extend([200])
        all(ax[0], type1, level1, list(range(ranges1[0], ranges1[1], ranges1[2])), [0.2, 0.5, 0.2, 0.2], False)
        all(ax[1], type2, level2, list(range(ranges2[0], ranges2[1], ranges2[2])), [0.3, 0.5, 0.2, 0.2], True)
        ax[0].format(title='a)暴雨日数距平(d)', titleloc='ul')
        ax[1].format(title='b)暴雨强度距平(mm/d)', titleloc='ul')
        ax.format(suptitle=title)
        plt.rcParams['font.family'] = ['sans-serif']
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig('days_intensity_距平%s.png' % title, dpi=500)



    linegress = pd.DataFrame()

    means = pd.read_pickle('means_yearly.pickle')

    for year in range(1960, 2019, 5):
        sdata_row = result_yearly[(result_yearly['year'] >= year) & (result_yearly['year'] < year + 5)]
        sdata = sdata_row.groupby(sdata_row['station']).sum().reset_index().set_index('station')

        sdata_row['intensity'] = sdata_row['intensity'].replace(0, np.nan)
        sdata['intensity'] = sdata_row['intensity'].groupby(sdata_row['station']).mean().reset_index().set_index(
            'station')
        sdata['intensity'] = sdata['intensity'].fillna(0)

        # add right lat, lon
        ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
        sll = ll.loc[sdata.index]
        sdata['lat'] = sll['lat']
        sdata['lon'] = sll['lon']
        # print(sdata.max())
        use_col = ['days', 'intensity']

        compare_intensity_0 = sdata.copy()
        for i in use_col:
            sdata['means'] = means[i]
            print(sdata['means'].loc[59981])
            sdata[i] = sdata[i] - sdata['means']
        sdata['intensity'][compare_intensity_0['intensity']==0] = 0
        plotss(sdata, '%i年-%i年'%(year, year+4),[-15,16,5], [-30,31,10])

def rain_monthly():

    import pandas as pd
    import numpy as np
    result_yearly = pd.DataFrame(pd.read_pickle('result_monthly.pickle').reindex())
    result_yearly.columns = ['p']
    result_yearly = result_yearly.reset_index()
    result_yearly.columns = ['year', 'month', 'station', 'p']
    result_yearly['days'] = np.array([int(i[0]) for i in result_yearly['p']])
    result_yearly['intensity'] = np.array([int(i[1]) for i in result_yearly['p']])
    result_yearly = result_yearly[['year', 'month', 'station', 'days', 'intensity']]

    def plotss(sdata, title, ranges1, ranges2):
        import numpy as np
        import matplotlib.pyplot  as plt
        import proplot as plot
        import cmaps
        import cartopy.crs as ccrs
        def rbf(ranges, datas, vari):
            from scipy.interpolate import Rbf
            olon = np.linspace(ranges[0][2], ranges[0][3], 88)
            olat = np.linspace(ranges[0][0], ranges[0][1], 88)

            olon, olat = np.meshgrid(olon, olat)
            # 插值处理
            lon = datas['lon']
            lat = datas['lat']
            data = np.array(datas[vari])
            func = Rbf(lon, lat, data, function='linear')
            data_new = func(olon, olat)
            return olon, olat, data_new

        lons, lats, type1 = rbf([[2.5, 60, 73, 135]], sdata, 'days')
        lons, lats, type2 = rbf([[2.5, 60, 73, 135]], sdata, 'intensity')
        # type1 = np.ma.masked_where(type1 < 0, type1)
        # type2 = np.ma.masked_where(type2 < 50, type2)
        fig, ax = plot.subplots(ncols=2, projection=ccrs.AlbersEqualArea(central_longitude=105), figsize=[6, 2.9],
                                tight=False
                                , left='1em', top='1.5em', bottom='2em')

        def all(ax, type1, levels, tickslevel, position, ax2_true=False):
            def plots(ax):
                print(levels)
                cb = ax.contourf(lons, lats, type1, levels=levels, extend='both', transform=ccrs.PlateCarree(),
                                 cmap=cmaps.MPL_bwr)
                # ax.colorbar(cb, loc='b', length=0.9, width='0.5em', space='0.5em',
                #             locator=leveltick, ticklabels=levellabel)
                ax.colorbar(cb, loc='b', length=0.8, width='0.5em', space='0.5em',
                            locator=tickslevel, ticklabels=[str(i) for i in tickslevel])
                # cbaxes = fig.add_axes([0.04, 0.09, 0.2, 0.01])
                # cbss = plt.colorbar(cb, cax=cbaxes, orientation="horizontal")

                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            def plotss(ax):
                if ax2_true is not True:
                    if ranges1[1] == 16:
                        colors = ['#0e0eFF', '#2C2CFF', '#4E4EFF', '#6868FF', '#8A8AFF', '#A6A6FF', '#C6C6FF', '#E2E2FF',
                                  '#FFFCFC', '#FFE0E0', '#FFC4C4', '#FFA4A4', '#FF8888', '#FF6868', '#FF4C4C', '#FF2C2C',
                                  '#FF0c0c', '#FF0000']
                    else:
                        colors = ['#9898FF', '#A2A2FF', '#D2D2FF', '#FFFCFC', '#FFD2D2',
                                  '#FFA0A0', '#FF7474', '#FF4444', '#FF1414', '#FF0000']
                else:
                    colors = ['#0202FF', '#0a0aFF', '#1A1AFF', '#2A2AFF', '#3A3AFF', '#4848FF', '#5858FF', '#6868FF',
                              '#7878FF', '#8A8AFF', '#9A9AFF', '#AAAAFF', '#BABAFF', '#CACAFF', '#DADAFF', '#EAEAFF',
                              '#FAFAFF', '#FFF8F8', '#FFE8E8', '#FFD8D8', '#FFC8C8', '#FFB8B8', '#FFA8A8', '#FF9898',
                              '#FF8888', '#FF7878', '#FF6868', '#FF5858', '#FF4848', '#FF3838', '#FF2828', '#FF1818',
                              '#FF0808', '#FF0000']
                print(colors)
                cb = ax.contourf(lons, lats, type1, levels=levels, transform=ccrs.PlateCarree(), colors=colors)

                ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
                from plot_in_proplot import shp_clip_for_proplot
                shp_clip_for_proplot(ax, fill=cb, clipshape=True)

            plots(ax)
            ax.format(latlim=[17, 54], lonlim=[80, 127])

            ax2 = fig.add_axes(position, projection=ccrs.AlbersEqualArea(central_longitude=105))
            plotss(ax2)
            from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

            ip = InsetPosition(ax, [0, -0.054, 0.12, 0.28])
            ax2.set_axes_locator(ip)

        level1 = list(range(ranges1[0], ranges1[1], 2))

        level1.insert(0, -100)
        level1.extend([100])
        print(level1)

        level2 = list(range(ranges2[0], ranges2[1], 2))
        level2.insert(0, -200)
        level2.extend([200])
        all(ax[0], type1, level1, list(range(ranges1[0], ranges1[1], ranges1[2])), [0.2, 0.5, 0.2, 0.2], False)
        all(ax[1], type2, level2, list(range(ranges2[0], ranges2[1], ranges2[2])), [0.3, 0.5, 0.2, 0.2], True)
        ax[0].format(title='a)暴雨日数距平(d)', titleloc='ul')
        ax[1].format(title='b)暴雨强度距平(mm/d)', titleloc='ul')
        ax.format(suptitle=title)
        plt.rcParams['font.family'] = ['sans-serif']
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.show()
        plt.savefig('days_intensity_距平%s.png' % title, dpi=500)
        plt.close()

    month_name = ['春季', '夏季', '秋季', '冬季']

    ax1_kw_season = [[-5,6,2], [-15,16,5],[-5,6,2],[-5,6,2]]


    for year in range(1960, 2020, 5):
        sdata_row = result_yearly[(result_yearly['year'] >= year - 1) & (result_yearly['year'] < year + 6)]
        sdata_row = sdata_row[~((sdata_row['year'] == year - 1) & (sdata_row['month'] <= 11))]
        sdata_row = sdata_row[~((sdata_row['year'] == year + 5) & (sdata_row['month'] > 2))]
        for i, month in enumerate([[3, 4, 5], [6, 7, 8], [9, 10, 11], [12, 1, 2]]):
            ssdata_row = sdata_row[
                (sdata_row['month'] == month[0]) | (sdata_row['month'] == month[1]) | (sdata_row['month'] == month[2])]

            sdata = ssdata_row.groupby(ssdata_row['station']).sum().reset_index().set_index('station')

            ssdata_row['intensity'] = ssdata_row['intensity'].replace(0, np.nan)
            sdata['intensity'] = ssdata_row['intensity'].groupby(ssdata_row['station']).mean().reset_index().set_index(
                'station')
            sdata['intensity'] = sdata['intensity'].fillna(0)

            # add right lat, lon
            ll = pd.read_csv(r'D:\basis\data\row\all_station.csv', index_col=0)
            sll = ll.loc[sdata.index]
            sdata['lat'] = sll['lat']
            sdata['lon'] = sll['lon']

            use_col = ['days', 'intensity']
            means = pd.read_pickle('means_monthly_%i.pickle'%month[0])
            compare_intensity_0 = sdata.copy()
            for ci in use_col:
                sdata['means'] = means[ci]
                sdata[ci] = sdata[ci] - sdata['means']
            sdata['intensity'][compare_intensity_0['intensity'] == 0] = 0
            plotss(sdata, '%i年-%i年 %s' % (year, year + 4, month_name[i]),ax1_kw_season[i], [-30,31,10])


import pandas as pd
import numpy as np
# prepare data (data provided in lingress, which is the concat of sdata in step0

# line_data = pd.read_pickle('rain_monthly_lin.pickle')
# month_name = ['春季', '夏季', '秋季', '冬季']
# for si, i in enumerate([3,6,9,12]):
#     sdata = line_data[line_data['month']==i]
#     sdata['intensity'] = sdata['intensity'].replace(0, np.nan)
#     means = sdata.groupby(sdata.index).mean()
#     means['intensity'] = means['intensity'].fillna(0)
#     means.to_pickle('means_monthly_%i.pickle'%i)


# import pandas as pd
# import numpy as np
# line_data = pd.read_pickle('rain_year_lin.pickle')
# line_data['intensity'] = line_data['intensity'].replace(0, np.nan)
# means = line_data.groupby(line_data.index).mean()
# means['intensity'] = means['intensity'].fillna(0)
#
# print(means[np.isnan(means['intensity'])])
# means.to_pickle('means_yearly.pickle')

#
# line_data = pd.read_pickle('drought_lin.pickle')
# print(line_data)
# means = line_data.groupby(line_data.index).mean()
# print(means)
# means.to_pickle('means_drought.pickle')

# rain_monthly()
