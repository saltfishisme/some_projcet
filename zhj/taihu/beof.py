import numpy as np
import pandas as pd
import beofplot
from eofs.standard import Eof
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from pandas import DataFrame


"data: （参考eof函数定义）np.array， shape=[年份，站点数量]" \
"       如果transport_dataframe=True, 则data: dataframe, index为站点名， colunms为‘pre’,'year'" \
"geosta: dataframe, index为站点名，colunmns为lon,lat" \
"color： 彩色‘color’, 黑色'bw'" \
"name: 储存时的名字，建议每个都有改变" \
"shps： 传入shp文件，如果为None则不做白化，传入则进行白化。"
def eof_analysis(data, geosta, sy, ey, eofranges, neof=2, color='color', name='eof', shps=None):
    if isinstance(data, DataFrame) == True:
        lenstation = len(geosta.index)
        n = ey + 1 - sy
        pre = np.empty([n, lenstation])
        # 提取雨量值
        for i, year in enumerate(range(sy, ey + 1)):
            for j, sta in enumerate(data.index.unique()):
                try:
                    print(sta, year)
                    pre[i, j] = data[(data['year'] == year)]['pre'].loc[sta]
                except:
                    pre[i, j] = np.nan
        data = pre
    solver = Eof(data)
    eof = solver.eofsAsCorrelation(neofs=neof)
    pc = solver.pcs(npcs=neof, pcscaling=1)
    np.savetxt('pc1_NAT.txt', pc[:, 0])
    np.savetxt('pc2_NAT.txt', pc[:, 1])
    print(solver.varianceFraction(neigs=neof+2))
    lisss = ['(a)', '(c)', '(b)', '(d)']
    fig = plt.figure(figsize=[6.5,5])
    grid = plt.GridSpec(11, 88)

    import cmaps
    cmaps=[cmaps.MPL_OrRd, cmaps.MPL_rainbow]
    for i in range(2):

        if color == 'color':
            peof = beofplot.ploteofs(plt.subplot(grid[i*5+i:i*5+5+i, 0:50], projection=ccrs.PlateCarree(central_longitude=102)),
                        eof[i], np.array(geosta['lon']), np.array(geosta['lat']), eofranges,
                        labela=lisss[i], color=color, shps=shps, cmaps=cmaps[i])
            ppc = beofplot.plotpc(plt.subplot(grid[i*5+i:i*5+5+i, 59:88]), pc[:, i], sy, ey, lisss[i+2])

        else:
            peof = beofplot.ploteofs(plt.subplot(grid[i*5+i:i*5+5+i, 0:55], projection=ccrs.PlateCarree(central_longitude=102)),
                        eof[i], np.array(geosta['lon']), np.array(geosta['lat']), eofranges,
                        labela=lisss[i], color=color, shps=shps)
            ppc = beofplot.plotpc(plt.subplot(grid[i*5+i:i*5+5+i, 55:88]), pc[:, i], sy, ey, lisss[i+2])
    # if color == 'color':
    #     fig.subplots_adjust(left=0.04, bottom=0.15, right=0.96, top=0.97, wspace=0.22, hspace=0.24)
    #     cb_ax = fig.add_axes([0.14, 0.06, 0.76, 0.02])
    #     cbar = fig.colorbar(peof, cax=cb_ax, orientation='horizontal')
    plt.subplots_adjust(left=0.0, bottom=0.06, right=0.99, top=0.95, wspace=0.2, hspace=0.2)
    # plt.show()
    fig.savefig('eof%s.jpg' % (name+str(color)), dpi=600)
    return pc


if __name__ == "__main__":
    data = pd.read_csv('data_mask.csv', index_col=0)
    index = data.index.unique()
    lat = []
    lon = []
    for i in index:
        lat.extend([data['lat'].loc[i].unique()[-1]/100])

        lon.extend([data['lon'].loc[i].unique()[-1]/100])
        # lon.extend(data['lon'].loc[i].unique()[-1])
        # print(lat)
    station = pd.DataFrame(index=index)
    station['lat'] = lat
    station['lon'] = lon

    data = data['pre'].groupby([data['year'], data.index]).sum().reset_index().set_index('station')
    import matplotlib.pyplot as plt

    eof_analysis(data, station, 1970, 2019,
                 [[30,32.3,118.9,121.5],[30,30.5, 31,31.5, 32],[119,119.5, 120,120.5, 121,121.5]],
                 color='color', shps=None)

