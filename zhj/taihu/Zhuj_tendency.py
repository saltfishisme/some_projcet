import pandas as pd
import numpy as np
data = np.loadtxt(r'D:\basis\data\row\WIN.txt')
print('yes')
'''太湖流域 多年降水平均平面图， 多年趋势平面图， 各站点+站点平均曲线图'''
stations = ['58448','58450','58457','58464','58467','58259','58265','58345','58354','58356','58358','58362']

'''提取数据'''
data = pd.read_csv(r'D:\basis\data\row\WIN.txt', delim_whitespace=True, header=None, index_col=0)

data = data.loc[[int(i) for i in stations]]
data.to_csv('data.csv')

'''处理缺失数据'''
# data = pd.read_csv('data.csv')
# def for_999999(data):
#     data[data == 999999] = np.nan
#     data[data == 999998] = np.nan
#     data[data == 999996] = np.nan
#     data[data == 999990] = 0.01
#     data[data > 999800] = (data[data > 999800] - 999800)*10
#     data[data > 999700] = (data[data > 999700] - 999700)*10
#     data[data > 999600] = (data[data > 999600] - 999600)*10
#     return data
# def for_32766(data):
#     data[data==32766] = np.nan
#     data[data==32700] = 0.01
#     data[data > 32000] = data[data > 32000] - 32000
#     data[data > 31000] = data[data > 31000] - 31000
#     data[data > 30000] = data[data > 30000] - 30000
#     return data
# # 警告，先9再3
# data[['7','8','9']] = for_999999(data[['7','8','9']])
# data[['7','8','9']] = for_32766(data[['7','8','9']])
# data[['7','8','9']] = data[['7','8','9']]/10
# data = data[data['4']>1959]
# data = data[data['4']<2020]
# data.to_csv('data_mask.csv')

import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import proplot as plot
import cmaps
'''计算多年平均， 计算多年趋势'''
# def rbf(ranges, datas, vari):
#     from scipy.interpolate import Rbf
#     olon = np.linspace(ranges[0][2], ranges[0][3], 88)
#     olat = np.linspace(ranges[0][0], ranges[0][1], 88)
#
#     olon,olat = np.meshgrid(olon, olat)
#     # 插值处理
#     lon = datas['2']/100
#     lat = datas['1']/100
#     data = datas[vari]
#     func = Rbf(lon, lat, data, function='linear')
#     data_new = func(olon, olat)
#     return olon, olat, data_new
# def fit_line2(x):
#     from scipy.stats import linregress
#     (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(np.array(x['4']).flatten(), np.array(x['9']).flatten())
#     return beta_coeff
#
#
# data = pd.read_csv('data_mask.csv')
#
# lonlat = data[['0','1','2']]
# lonlat = lonlat.drop_duplicates(['0'])
# lonlat = lonlat.set_index(lonlat['0'])
#
#
#
# tend = data['9'].groupby([data['0'], data['4']]).mean().reset_index()
# tend2 = tend.groupby(tend['0']).apply(lambda x: fit_line2(x))
# lonlat['trend'] = tend2
#
# sums = data['9'].groupby([data['0'], data['4']]).sum().reset_index()
# sums = sums['9'].groupby(sums['0']).mean()
# lonlat['sum'] = sums
#
# lonlat.columns=['station', 'lat', 'lon', 'trend', 'sum']
# lonlat['lat'] = lonlat['lat']/100
# lonlat['lon'] = lonlat['lon']/100
#
# lonlat.to_csv('result.csv')
# data = np.array(lonlat['sum'])
# labels = 'mm'
# title = '太湖流域站点降水多年平均图'
# position =[0.85,0.04]
# data = np.array(lonlat['trend'])*100
# labels = '*10${^-}$${^2}$mm/a'
# title = '太湖流域站点降水多年趋势图'
# position = [0.84,0.025]
#
#
# lon = np.array(lonlat['2'] / 100)
# lat = np.array(lonlat['1'] / 100)
# df = pd.DataFrame()
# df['stations'] = stations
# df['lon'] = lon
# df['lat'] = lat
# df['trend'] =
# olon, olat, odata = rbf([[30,33,119,122]], lonlat, vari)

# import cartopy.crs as ccrs
# import matplotlib.pyplot as plt
# import proplot as ZhengZhou
# import cmaps
# fig, ax = ZhengZhou.subplots(ncols=1, nrows=1, proj='cyl', figsize=(4,3), tight=False,
#                         left='2em', right='3.5em', bottom='2em')
# ax.format(lonlim=(118.9,121.5), latlim=(32.3,30), lonlabels=True, latlabels=True)
#
# sc = ax[0].scatter(lon,lat, c=data, s=10, cmap=cmaps.cmocean_amp, edgecolor='black')
# ax[0].colorbar(sc, loc='r', space='0.2em')
# for i in range(len(lon)):
#     ax.text(lon[i]-0.238, lat[i]+0.029, '%0.2f'%(data[i]),
#             transform=ccrs.PlateCarree(),
#             bbox=dict(facecolor='white', alpha=0.4, boxstyle='round'))
#
# ax[0].format()
# from matplotlib.image import imread
# ax.imshow(
#     imread(r'D:\basis\data\shp\NE1_50M_SR_W.tif'),
#     origin='upper',
#     transform=ccrs.PlateCarree(),
#     extent=[-180, 180, -90, 90])
#
# from pylab import mpl
# mpl.rcParams['font.sans-serif'] = ['SimHei']
# mpl.rcParams['axes.unicode_minus'] = False
# ax[0].format(suptitle=title)
#
# ax[0].annotate(labels, position, xycoords='figure fraction')
# plt.show()
# plt.savefig('%s.png'%title, dpi=600)

'''总的站点+各站点平均曲线'''
# data = pd.read_csv('data_mask.csv')
# aver = data['9'].groupby([data['0'], data['4']]).sum().reset_index().set_index('0')
#
# aver_all = data['9'].groupby([data['0'],data['4']]).sum().reset_index()
# aver_all = aver_all['9'].groupby(aver_all['4']).mean()
#
# aver.to_csv('annual_aver_station.csv')
# aver_all.to_csv('annual_aver_all.csv')

def plotss(ax, x, data, position=[0.01,0.93,0.85], fontsize=8):
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False
    ax.plot(x, data, marker='.', color='black', markersize=5, label='观测')
    ax.format(xlabel='年')

    def fit_line(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return [beta_coeff, intercept, rvalue, pvalue, stderr], line2
    [beta_coeff, intercept, rvalue, pvalue, stderr], y = fit_line(x, data)
    ax.plot(x, y, linestyle='--', color='black', alpha=0.8, label='趋势', zorder=20)

    ax.text(position[0], position[1],
            'y=%.2f+(%.2fx)' % (intercept, beta_coeff), transform=ax.transAxes,fontsize=fontsize
            , zorder=50,
            bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', alpha=0.7,pad=0.00001))
    ax.text(position[0], position[2],
            'R=%.3f ,P=%.3f' % (rvalue, pvalue), transform=ax.transAxes,fontsize=fontsize, zorder=50,
            bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', pad=0.00001, alpha=0.8))
    ax.legend(loc='lr', fontsize=6)


# fig, ax = ZhengZhou.subplots(ncols=3, nrows=4, figsize=[8,5])
# aver = data['9'].groupby([data['0'], data['4']]).sum().reset_index().set_index('0')
#
# abc = 'a b c d e f g h i j k l m n o p q r s t'.split(' ')
#
# for i,sta in enumerate(stations):
#     sdata = aver.loc[sta]
#     plotss(ax[i], np.array(sdata['4']), np.array(sdata['9']),[0.01,0.9,0.78],7)
#     ax[i].annotate('(%s)%s站点'% (abc[i],sta), [0.00,1.02], xycoords='axes fraction', fontsize=7)
# ax.format(ylabel='年降水/mm',yticks=400, xticks=8, xlim=[1959,2020],suptitle='太湖流域不同站点降水年际变化图')
#
# # plt.show()
# plt.savefig('站点降水年际变化图.png', dpi=600)

# 总站点
# fig, ax = ZhengZhou.subplots(figsize=[5,2.5])
# aver_all = data['9'].groupby([data['0'],data['4']]).sum().reset_index()
# aver_all = aver_all['9'].groupby(aver_all['4']).mean()
# plotss(ax, aver_all.index, np.array(aver_all))
# ax.format(ylabel='年降水/mm', title='太湖流域降水年际变化图', xticks=5)
#
# # plt.show()
# plt.savefig('太湖流域降水年际变化图.png', dpi=600)







