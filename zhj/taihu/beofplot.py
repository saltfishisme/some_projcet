import cartopy.crs as ccrs
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import Rbf
from matplotlib.path import Path
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from matplotlib.patches import PathPatch
from shapely.geometry import Polygon as ShapelyPolygon
from shapely.geometry import Point as ShapelyPoint

def ploteofs(ax, data, lon, lat, ranges, labela='eof', color='color', shps=None, cmaps=False):
    # 插值初定义
    olon = np.linspace(ranges[0][2], ranges[0][3], 88)
    olat = np.linspace(ranges[0][0], ranges[0][1], 88)

    olon,olat = np.meshgrid(olon, olat)
    # 插值处理
    data = np.ma.masked_invalid(data)
    func = Rbf(lon, lat, data, function='linear')
    data_new = func(olon, olat)

    #建立坐标系，并将裁剪的path得出
    if shps is not None:
        plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
        plat = shps[:, 1]
        plon = shps[:, 0]

        upath = Path(list(zip(plon, plat)))
        upath = PathPatch(upath, transform=plate_carre_data_transform)

        # line= ax.ZhengZhou(plon, plat, transform=plate_carre_data_transform, color='k', alpha=0.7)

        fill = ax.contour(olon, olat, data_new, 8, transform=ccrs.PlateCarree(), colors='k', alpha=0.8)

        for collection in fill.collections:
            collection.set_clip_path(upath)


        # 删除clabel
        # aaa = ccrs.PlateCarree(central_longitude=102).transform_points(ccrs.Geodetic(), plon, plat)
        # clip_map_shapely = ShapelyPolygon(aaa)
        # for text_object in CS_label:
        #     if not clip_map_shapely.contains(ShapelyPoint(text_object.get_position())):
        #         text_object.set_visible(False)

    # fill = ax.contour(olon, olat, data_new, 10, transform=ccrs.PlateCarree(), colors='k', alpha=0.8)
    # ax.clabel(fill)
    if color == 'bw':
        fill = ax.contourf(olon, olat, data_new, 10, transform=ccrs.PlateCarree(), cmap=plt.cm.gray, alpha=0.4)
    #
    if color == 'color':
        if cmaps is not False:

            fill = ax.contourf(olon, olat, data_new, 40, transform=ccrs.PlateCarree(), cmap=cmaps)
            plt.colorbar(fill)
        else:
            import cmaps
            fill = ax.contourf(olon, olat, data_new, 40, transform=ccrs.PlateCarree(), cmap=cmaps.MPL_rainbow)
            plt.colorbar(fill)


    # #添加shp文件进入图片中
    # from cartopy.io.shapereader import Reader
    # from cartopy.feature import ShapelyFeature
    # fname = r'bianjie.shp'
    # shape_feature = ShapelyFeature(Reader(fname).geometries(), ccrs.PlateCarree(), edgecolor='k', facecolor='none', linewidths=0.55)
    # ax.add_feature(shape_feature)

    #设置坐标轴经纬度标志
    # ax.set_xticks(range(int(np.min(olon)), int(np.max(olon)), int((np.max(olon)-np.min(olon))/3)), crs=ccrs.PlateCarree())
    # ax.set_yticks(range(int(np.min(olat)), int(np.max(olat)), int((np.max(olat)-np.min(olat))/2)), crs=ccrs.PlateCarree())
    ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
    ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
    ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())

    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)

    plt.text(ranges[0][2], ranges[0][1]+(ranges[0][1]-ranges[0][0])*(0.15/6), labela, transform=ccrs.PlateCarree())

    ax.tick_params(labelsize=8)
    ax.coastlines()


    # pathss = shp2clip(r'D:\basis\changjiang\eof\bianjie.shp')
    # plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
    # upath = PathPatch(pathss, transform=plate_carre_data_transform)
    # for collection in fill.collections:
    #     collection.set_clip_path(upath)
    # for collection in sfill.collections:
    #     collection.set_clip_path(upath)
    #
    # from cartopy.feature import ShapelyFeature
    # from cartopy.io.shapereader import Reader
    # shape_feature = ShapelyFeature(Reader(r'D:\basis\changjiang\eof\bianjie.shp').geometries(), ccrs.PlateCarree(),
    #                                edgecolor='k', facecolor='none', linewidths=0.55)
    # ax.add_feature(shape_feature)

    # CS_label = ax.clabel(fill, fmt='%6.1f')

    return

def plotpc(ax, pc, sy, ey, labela):
    ma = fast_moving_average(pc, 11)
    years = range(sy, ey + 1)
    ax.bar(years, pc, color='black')
    plt.plot(years, ma, color='k', linewidth=1.5)

    ax.axhline(0, color='k')
    ax.axhline(1, color='k', linestyle='--', alpha=0.5)
    ax.axhline(-1, color='k', linestyle='--', alpha=0.5)
    ax.set_xticks(range(sy, ey + 1, int((ey-sy)/5)))
    # ax.set_yticks([-2, -1, 0, 1, 2])
    plt.xlim(sy-1, ey+1)
    plt.ylim(-3, 3)

    plt.text(sy + 1, 3.15, labela)
    ax.tick_params(labelsize=8)
    return plt


def fast_moving_average(x, N):
    return np.convolve(x, np.ones((N,)) / N)[(N - 1):]

def nor(data):
    max = np.max(data)
    min = np.min(data)
    cdata = []
    for i in data:
        cdata.append((i-min)/(max-min))
    return cdata

def plotline(ax, pc, sy, ey, labela, nors=None):
    print(pc.shape)
    if nors != None:
        pc = nor(pc)
        plt.ylim(0, 1.2)
    ma = fast_moving_average(pc, 11)
    years = range(sy, ey + 1)
    ax.plot(years, pc, color='black')
    ax.axhline(0, color='k')
    ax.set_xticks(range(sy, ey + 1, int((ey-sy)/5)))
    # ax.set_yticks([-2, -1, 0, 1, 2])
    plt.xlim(sy, ey)
    plt.text(sy, 1.1, labela)
    ax.tick_params(labelsize=8)
    return plt

def shp2clip(shpfile):
    import shapefile
    from matplotlib.path import Path
    from matplotlib.patches import PathPatch
    sf = shapefile.Reader(shpfile)
    vertices = []
    codes = []
    for shape_rec in sf.shapeRecords():
        pts = shape_rec.shape.points
        prt = list(shape_rec.shape.parts) + [len(pts)]
        for i in range(len(prt) - 1):
            for j in range(prt[i], prt[i+1]):
                vertices.append((pts[j][0], pts[j][1]))
            codes += [Path.MOVETO]
            codes += [Path.LINETO] * (prt[i+1] - prt[i] - 2)
            codes += [Path.CLOSEPOLY]
        clip = Path(vertices, codes)
    return clip

