import pandas as pd
import math as mt
import numpy as np

'''通过p3曲线判断丰水年枯水年'''

def trans_x_to_hs(p):
    def Linterpo(x, y, xi):
        if xi < x[0]:
            yi = y[0] + (xi - x[0]) / (x[0] - x[1]) * (y[0] - y[1])
        if xi > x[-1]:
            yi = y[-1] + (xi - x[-1]) / (x[-1] - x[-2]) * (y[-1] - y[-2])
        else:
            for j in range(len(x)):
                if xi >= x[j] and xi < x[j + 1]:
                    yi = y[j] + (xi - x[j]) / (x[j + 1] - x[j]) * (y[j + 1] - y[j])
        return yi
    # 转换为海森几率坐标
    hs = pd.read_csv('Heisenberg_paper.csv')
    hs_p = np.array(hs['网格线几率'])/100
    hs_x = np.array(hs['网格线坐标'])
    result=[]
    for i in p:
        result.append(Linterpo(hs_p, hs_x, i))
    return result



def p3_plot(x, p3_x, p3_p):
    x.sort()
    x.reverse()
    lens = len(x)
    x_p = [(i+1)/(lens+1) for i in range(lens)]
    import proplot as plot
    fig, ax = plot.subplots(figsize=(6,3.5))
    ax.scatter(trans_x_to_hs(x_p), x, size=5, labels='年降雨经验频率')
    ax.plot(trans_x_to_hs(np.array(p3_p)), p3_x, labels='P-III降雨频率曲线', color='black')
    hs = pd.read_csv('Heisenberg_paper.csv')
    hs_p = np.array(hs['刻度几率'])
    hs_x = np.array(hs['刻度坐标'])
    hs_p=['0.01', '0.05', '0.5', '1', '5', '10', '20', '30', '40', '50', '60', '70', '80', '90', '95', '99', '99.99', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan']
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False
    ax.legend()
    ax.format(xlocator=hs_x, xminorlocator='null', xticklabels=hs_p,
              xlabel='p(%)', ylabel='降水(mm)')
    import matplotlib.pyplot as plt
    # plt.show()
    plt.savefig('a.png', dpi=600)



ws = pd.read_csv('data_y.txt')

pdata = ws['2019']  # 导入降水系列数据

# 计算样本统计参数
mean = np.mean(pdata)  # 降水系列的均值
std = np.std(pdata)  # 降水系列的标准偏差
Cs = pdata.skew()  # 降水系列的偏态系数
Cv = std / mean  # 降水系列的变差系数
max = max(pdata)  # 最大值
min = min(pdata)  # 最小值


print(mean, Cs, Cv)
Cs = 0.7
Cv = 0.19

# 计算P3分布统计参数
alpha = 4 / Cs ** 2  # 参数alpha
beta = 2 / (mean * Cs * Cv)  # 参数beta
x0 = mean * (1 - 2 * Cv / Cs)  # 参数x0

pmp = max + 3 * std  # 构造极端最大降水量
pseq = [i for i in range(mt.ceil(pmp / 10) * 10, mt.floor(x0 / 10) * 10, -10)]  # 构造降水等差序列
seq = [i for i in range(1, len(pseq) + 1)]  # 将降水等差序列排序

quantiles = []  # 计算p3分布各分位点的概率密度函数值
for i in range(len(pseq)):
    quantile = beta ** alpha / mt.gamma(alpha) * (pseq[i] - x0) ** (alpha - 1) * mt.exp(-(pseq[i] - x0) * beta)
    quantiles.append(quantile)

probabilities = []  # 计算各分位点之间的区间概率
probabilities.append(0)
for i in range(1, len(quantiles)):
    probability = (quantiles[i - 1] + quantiles[i]) * (pseq[i - 1] - pseq[i]) / 2
    probabilities.append(probability)

accum_probblts = []  # 计算累计概率
for i in range(len(probabilities)):
    accum_probblts.append(sum(probabilities[:i + 1]))
p3_plot(list(pdata), pseq, accum_probblts)

def Linterpo(x, y, xi):
    if xi < x[0]:
        yi = y[0] + (xi - x[0]) / (x[0] - x[1]) * (y[0] - y[1])
    if xi > x[-1]:
        yi = y[-1] + (xi - x[-1]) / (x[-1] - x[-2]) * (y[-1] - y[-2])
    else:
        for j in range(len(x)):
            if xi >= x[j] and xi < x[j + 1]:
                yi = y[j] + (xi - x[j]) / (x[j + 1] - x[j]) * (y[j + 1] - y[j])
    return yi

# p = [0.5,0.6,0.7]
p = [0.125, 0.375, 0.625, 0.875]  # 设计频率
for i in range(len(p)):
    pp = round(Linterpo(accum_probblts, pseq, p[i]), 1)
    print(str(round(p[i] * 100, 1)) + '%设计频率的年降水量是' + str(pp) + 'mm')