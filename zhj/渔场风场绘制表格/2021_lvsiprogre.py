import pandas as pd
import numpy as np


'''此处浪向实际上为浪高'''

def cal(adress, station, windplus=1):
    # a = pd.read_csv(r'D:\basis\some_project\渔场风场绘制表格\qidong\qidong\32068101.2020%s00.csv'%adress, usecols=[1, 3, 4, 8],
    #                 index_col=0, parse_dates=True, header=None, skiprows=1, names=['', 'winddir', 'speed', 'wavedir'])


    def choose_winddir(winddir, type=8):
        # 传入一天的风向，传出这天上午和下午风向的最多出现的
        def change_winddir_into_wind16(winddir, type):
            if type == 16:
                a = [11.26, 33.76, 56.26, 78.76, 101.26, 123.76, 146.26, 168.76, 191.26, 213.76, 236.26, 258.76, 281.26,
                     303.76,
                     326.26]
                b = [33.75, 56.25, 78.75, 101.25, 123.75, 146.25, 168.75, 191.25, 213.75, 236.25, 258.75, 281.25, 303.75,
                     326.25, 348.75]
                addtion = [348.76, 11.25]
                windnumber = np.arange(22.5, 361, 22.5)
                for i in range(15):
                    winddir[(winddir >= a[i]) & (winddir <= b[i])] = windnumber[i]
                winddir[(winddir >= addtion[0]) | (winddir <= addtion[1])] = windnumber[15]
            if type == 8:
                a = [22.5, 67.5, 112.5, 157.5, 202.5, 247.5, 292.5]
                b = [67.5, 112.5, 157.5, 202.5, 247.5, 292.5, 337.5]
                addtion = [337.5, 22.5]
                windnumber = np.arange(45,361,45)
                for i in range(7):
                    winddir[(winddir >= a[i]) & (winddir < b[i])] = windnumber[i]
                winddir[(winddir >= addtion[0]) | (winddir < addtion[1])] = windnumber[7]
            return winddir

        winddir16 = {22.5: '北东北', 45.0: '东北', 67.5: '东东北', 90.0: '东', 112.5: '东东南', 135.0: '东南', 157.5: '南东南',
                     180.0: '南', 202.5: '南西南', 225.0: '西南', 247.5: '西西南', 270.0: '西', 292.5: '西西北',
                     315.0: '西北', 337.5: '北西北', 360.0: '北'}

        def choose_most_frequent_wind(winddir):
            # 选取序列中最多的风速
            from collections import Counter
            return Counter(winddir).most_common(1)[0][0]

        # 将风速处理成标准风向
        winddir['winddir'] = change_winddir_into_wind16(winddir['winddir'], type=type)

        morning = np.array(winddir[winddir.index.hour <= 11]['winddir'])
        after = np.array(winddir[winddir.index.hour > 11]['winddir'])

        m = choose_most_frequent_wind(morning)
        a = choose_most_frequent_wind(after)
        return '%s到%s风'%(winddir16[m], winddir16[a])


    def choose_windspeed(windspeed, times):
        '''rank_levels = [0, 0.3, 1.6, 3.4, 5.5, 8, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, 37.0, 41.5, 46.2, 51.0, 56.1,
                   61.3]

    rank = []
    for i in range(0, len(rank_levels)-2):
        rank.append((rank_levels[i]+rank_levels[i+2]-0.1)/2)
    print(rank)'''
        def find_level(windspeed):
            windspeed = np.array(windspeed['speed'])

            maxwind = np.around(np.nanmax(windspeed), 1)

            rank_levels = [0.75, 1.9, 3.5, 5.65, 8.1, 10.9, 13.95, 17.3, 20.8, 24.6,
                           28.55, 32.7, 37.05, 41.55, 46.2, 51.1, 56.1]
            rank_levels = [0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2,20.8,24.5,28.5,32.7,37,41.5,46.2,51,56.1]
            rank_levels = [0, 0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, 37, 41.5, 46.2, 51,
                           56.1]
            rank_levels = [0, 0.85, 1.9, 3.6, 5.75, 8.2, 11.0, 14.05, 17.4, 20.9, 24.7, 28.65, 32.8, 37.15, 41.65, 46.3, 51.2]

            fina = np.digitize(maxwind, rank_levels)-1

            return '%i-%i' % (fina, fina + 1)

        return find_level(windspeed)


    def choose_wavedir(wavedir):
        wavedir = np.array(wavedir['wavedir'])
        maxdir = np.nanmax(wavedir)
        return '%i-%i'%(np.floor(maxdir), np.ceil(maxdir))

    obs_station = station
    if (station == '3303273A') :
        obs_station = '33032701'
    elif (station == '3303811A'):
        obs_station ='33038101'

    import os
    if os.path.exists(huadongyuan_data_add+r'\%s.2021%s00.csv'%(station, adress)):
        a = pd.read_csv(huadongyuan_data_add+r'\%s.2021%s00.csv'%(station, adress),
                        index_col=1, parse_dates=True)
    else:
        return False
    a = a[['10米风速', '10米风向']]
    a.columns = ['speed', 'winddir']
    a['speed'] = np.array(a['speed'])*windplus
    a.index.name = None
    begin = a.index[0]
    ends = begin +pd.to_timedelta('2D')
    a = a[str(begin)[:10]:str(ends)[:10]]

    dir = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_winddir(x)).reset_index()
    speed_morning = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_windspeed(x, 'mor')).reset_index()
    speed_after = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_windspeed(x, 'aft')).reset_index()

    df = pd.DataFrame()

    df['winddir'] = dir[0]
    df['sp'] = speed_morning[0]

    return df


def creat_word_table(document, title, df):
    from docx import Document
    from docx.enum.table import WD_TABLE_ALIGNMENT
    from docx.shared import Cm
    from docx.oxml.ns import qn
    '''
    document = Document()
document.styles['Normal'].font.name = '宋体'
document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')'''
    from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
    p = document.add_paragraph('')
    p.add_run(title).bold = True
    p.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    tb = document.add_table(rows=len(df.index), cols=len(df.columns))
    # tb.add_row()

    for row in range(len(df.index)):
        for col in range(len(df.columns)):
            tb.cell(row, col).text = str(df.iloc[row, col])

    tb.style = 'Table Grid'
    tb.autofit = True




def main(observation_address, station, windplus, document, time_list_number):
    time_list = time_list_all[time_list_number]
    print('请注意检查time_list')
    for time in time_list:
        time_a, time_b = time.split('.')
        if len(time_b)<2:
            time_b = '0%s'%time_b
        if len(time_a) < 2:
            time_a = '0%s' % time_a

        time_format = '%s%s'%(time_a, time_b)

        a = pd.read_excel(observation_address,
                          sheet_name=time)
        a = a.iloc[1:4]

        ##############将上下午合并成一天#################################
        def return_max(df):
            result1 = []
            result2 = []
            for index, series in df.iterrows():
                be1 = series.iloc[1];
                be2 = series.iloc[3]

                if len(be1) != 1:
                    b1 = int(be1[0]);
                    e1 = int(series.iloc[1][2])
                else:
                    sb1 = int(be1)

                if len(be2) != 1:
                    b2 = int(series.iloc[3][0]);
                    e2 = int(series.iloc[3][2])
                else:
                    sb2 = int(be2)
                if ('e1' in vars()) & ('e2' in vars()):
                    if (b1 >= b2) & (e1 >= e2):
                        result1.append(series.iloc[0])
                        result2.append(series.iloc[1])
                    else:
                        result1.append(series.iloc[2])
                        result2.append(series.iloc[3])
                else:
                    if ('sb1' in vars()):
                        if (sb1 >= e2):
                            result1.append(series.iloc[0])
                            result2.append(series.iloc[1])
                        else:
                            result1.append(series.iloc[2])
                            result2.append(series.iloc[3])
                    if ('sb2' in vars()):
                        if (sb2 < e1):
                            result1.append(series.iloc[0])
                            result2.append(series.iloc[1])
                        else:
                            result1.append(series.iloc[2])
                            result2.append(series.iloc[3])

            return result1, result2

        trans_huanghai1, trans_huanghai2 = return_max(a[['南通沿岸', '（上午）', '南通沿岸.1', '（下午）']])
        a['南通沿岸'] = trans_huanghai1
        a[''] = trans_huanghai2
        a = a[['吕泗渔场', 'Unnamed: 1', 'Unnamed: 2', '黄海南部', 'Unnamed: 4', '南通沿岸', '']]



        date_ranges = pd.date_range('2020-%s-%s'%(time_format[:2], time_format[2:]), periods=3, freq='1D').strftime('%m.%d')
        trans_date = np.array(date_ranges)
        a['吕泗渔场'] = trans_date

        # 找到华东院数据
        b = cal(time_format, station, windplus)
        if b is False:
            continue
        b = b.iloc[0:3].set_index(a.index)

        # 合并两个数据
        df = pd.concat([a,b], axis=1)

        title_df.columns = df.columns
        df = pd.concat([title_df, df], axis=0)

        df = df[['吕泗渔场', 'Unnamed: 1', 'Unnamed: 2','winddir', 'sp', '黄海南部', 'Unnamed: 4',
   '南通沿岸', '']]
        df = df.fillna('')

        df = df[['吕泗渔场', 'winddir', 'sp', 'Unnamed: 1', 'Unnamed: 2',
                 '黄海南部', 'Unnamed: 4',
   '南通沿岸', '']]
        creat_word_table(document, time_format, df)
    return


def mainmain(station, stationname):
    print(stationname)
    sta_df_all = pd.DataFrame()
    for plus in [1,1.1,1.2,1.3,1.4,1.5]:
        from docx import Document
        from docx.oxml.ns import qn
        document = Document()
        document.styles['Normal'].font.name = '宋体'
        document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')


        stast_df = main(observation_address,station, plus, document,0)

        document.save('%s风速乘%s.docx' % (stationname, str(plus)))
    # document.save('%s风速乘%s.docx'%(stationname, str(plus)))


# ######### constant vari
station = [32068101, 32068102, 32068103]
stationname = ['H1', 'H2', 'H3']
title_df = pd.DataFrame()
title_df['title1'] = ['启东', '中信海洋', '', '黄海南部', ''	,
                      '南通沿岸', '', '华东院','']
title_df['title2'] = ['预报日期','风向','风级',
                      '风向','风级','风向','风级','风向','风级']
title_df = title_df.T

time_list_all=[['2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '2.7', '2.8', '2.9', '2.10', '2.11', '2.12', '2.13', '2.14', '2.15', '2.16', '2.17', '2.18', '2.19', '2.20', '2.21', '2.22', '2.23', '2.24', '2.25', '2.26', '2.27', '2.28']]

huadongyuan_data_add = r'D:\basis\some_projects\zhj\渔场风场绘制表格\202102'

for i in range(0,3):
    mainmain(station[i], stationname[i])

