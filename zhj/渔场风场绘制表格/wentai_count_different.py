
import pandas as pd
import numpy as np

'''此处浪向实际上为浪高'''

def cal(adress, station, windplus=1):
    # a = pd.read_csv(r'D:\basis\some_project\渔场风场绘制表格\qidong\qidong\32068101.2020%s00.csv'%adress, usecols=[1, 3, 4, 8],
    #                 index_col=0, parse_dates=True, header=None, skiprows=1, names=['', 'winddir', 'speed', 'wavedir'])


    def choose_winddir(winddir, type=8):
        # 传入一天的风向，传出这天上午和下午风向的最多出现的
        def change_winddir_into_wind16(winddir, type):
            if type == 16:
                a = [11.26, 33.76, 56.26, 78.76, 101.26, 123.76, 146.26, 168.76, 191.26, 213.76, 236.26, 258.76, 281.26,
                     303.76,
                     326.26]
                b = [33.75, 56.25, 78.75, 101.25, 123.75, 146.25, 168.75, 191.25, 213.75, 236.25, 258.75, 281.25, 303.75,
                     326.25, 348.75]
                addtion = [348.76, 11.25]
                windnumber = np.arange(22.5, 361, 22.5)
                for i in range(15):
                    winddir[(winddir >= a[i]) & (winddir <= b[i])] = windnumber[i]
                winddir[(winddir >= addtion[0]) | (winddir <= addtion[1])] = windnumber[15]
            if type == 8:
                a = [22.5, 67.5, 112.5, 157.5, 202.5, 247.5, 292.5]
                b = [67.5, 112.5, 157.5, 202.5, 247.5, 292.5, 337.5]
                addtion = [337.5, 22.5]
                windnumber = np.arange(45,361,45)
                for i in range(7):
                    winddir[(winddir >= a[i]) & (winddir < b[i])] = windnumber[i]
                winddir[(winddir >= addtion[0]) | (winddir < addtion[1])] = windnumber[7]
            return winddir

        winddir16 = {22.5: '北东北', 45.0: '东北', 67.5: '东东北', 90.0: '东', 112.5: '东东南', 135.0: '东南', 157.5: '南东南',
                     180.0: '南', 202.5: '南西南', 225.0: '西南', 247.5: '西西南', 270.0: '西', 292.5: '西西北',
                     315.0: '西北', 337.5: '北西北', 360.0: '北'}

        def choose_most_frequent_wind(winddir):
            # 选取序列中最多的风速
            from collections import Counter
            return Counter(winddir).most_common(1)[0][0]

        # 将风速处理成标准风向
        winddir['winddir'] = change_winddir_into_wind16(winddir['winddir'], type=type)

        morning = np.array(winddir[winddir.index.hour <= 11]['winddir'])
        after = np.array(winddir[winddir.index.hour > 11]['winddir'])

        m = choose_most_frequent_wind(morning)
        a = choose_most_frequent_wind(after)
        return '%s到%s风'%(winddir16[m], winddir16[a])


    def choose_windspeed(windspeed, times):
        '''rank_levels = [0, 0.3, 1.6, 3.4, 5.5, 8, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, 37.0, 41.5, 46.2, 51.0, 56.1,
                   61.3]

    rank = []
    for i in range(0, len(rank_levels)-2):
        rank.append((rank_levels[i]+rank_levels[i+2]-0.1)/2)
    print(rank)'''
        def find_level(windspeed):
            windspeed = np.array(windspeed['speed'])

            maxwind = np.around(np.nanmax(windspeed), 1)

            rank_levels = [0.75, 1.9, 3.5, 5.65, 8.1, 10.9, 13.95, 17.3, 20.8, 24.6,
                           28.55, 32.7, 37.05, 41.55, 46.2, 51.1, 56.1]
            rank_levels = [0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2,20.8,24.5,28.5,32.7,37,41.5,46.2,51,56.1]
            rank_levels = [0, 0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, 37, 41.5, 46.2, 51,
                           56.1]
            rank_levels = [0, 0.85, 1.9, 3.6, 5.75, 8.2, 11.0, 14.05, 17.4, 20.9, 24.7, 28.65, 32.8, 37.15, 41.65, 46.3, 51.2]

            fina = np.digitize(maxwind, rank_levels)-1

            return '%i-%i' % (fina, fina + 1)

        return find_level(windspeed)


    def choose_wavedir(wavedir):
        wavedir = np.array(wavedir['wavedir'])
        maxdir = np.nanmax(wavedir)
        return '%i-%i'%(np.floor(maxdir), np.ceil(maxdir))

    obs_station = station
    if (station == '3303273A') :
        obs_station = '33032701'
    elif (station == '3303811A'):
        obs_station ='33038101'

    import os
    if os.path.exists(huadongyuan_data_add+r'\%s.2020%s00.csv'%(station, adress)):
        a = pd.read_csv(huadongyuan_data_add+r'\%s.2020%s00.csv'%(station, adress),
                        index_col=1, parse_dates=True)
    elif os.path.exists(huadongyuan_data_add+r'\%s.2020%s00.csv'%(obs_station, adress)):
        a = pd.read_csv(huadongyuan_data_add+r'\%s.2020%s00.csv'%(obs_station, adress),
                        index_col=1, parse_dates=True)
    else:
        return False
    a = a[['10米风速', '10米风向']]
    a.columns = ['speed', 'winddir']
    a['speed'] = np.array(a['speed'])*windplus
    a.index.name = None
    begin = a.index[0]
    ends = begin +pd.to_timedelta('2D')
    a = a[str(begin)[:10]:str(ends)[:10]]

    dir = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_winddir(x)).reset_index()
    speed_morning = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_windspeed(x, 'mor')).reset_index()
    speed_after = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_windspeed(x, 'aft')).reset_index()

    df = pd.DataFrame()

    df['winddir'] = dir[0]
    df['sp'] = speed_morning[0]

    return df


def cal2(adress, station, windplus=1):

    def choose_windspeed(windspeed, times):

        def find_level(windspeed):
            windspeed = np.array(windspeed['speed'])

            maxwind = np.around(np.nanmax(windspeed), 1)

            rank_levels = [0.75, 1.9, 3.5, 5.65, 8.1, 10.9, 13.95, 17.3, 20.8, 24.6,
                           28.55, 32.7, 37.05, 41.55, 46.2, 51.1, 56.1]
            rank_levels = [0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2,20.8,24.5,28.5,32.7,37,41.5,46.2,51,56.1]
            rank_levels = [0, 0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, 37, 41.5, 46.2, 51,
                           56.1]
            rank_levels = [0, 0.85, 1.9, 3.6, 5.75, 8.2, 11.0, 14.05, 17.4, 20.9, 24.7, 28.65, 32.8, 37.15, 41.65, 46.3, 51.2]

            fina = np.digitize(maxwind, rank_levels)-1

            return '%i-%i' % (fina, fina + 1)

        return find_level(windspeed)


    def choose_wavedir(wavedir):
        wavedir = np.array(wavedir['wavedir'])
        maxdir = np.nanmax(wavedir)
        return '%i-%i'%(np.floor(maxdir), np.ceil(maxdir))


    import os

    a = pd.read_excel(r'D:\basis\some_projects\zhj\渔场风场绘制表格\温州海域数据.xlsx',
                    index_col=0, parse_dates=True, sheet_name=station)
    a = a[['20m风速']]*0.9

    a.columns = ['speed']

    a['speed'] = np.array(a['speed'])
    a.index.name = None
    ###############################################################
    begin = pd.to_datetime('2020-%s-%s'%(adress[:2], adress[2:]))

    ##################################################
    ends = begin +pd.to_timedelta('2D')
    a = a[str(begin)[:10]:str(ends)[:10]]


    speed_morning = a.groupby([a.index.month, a.index.day]).apply(lambda x: choose_windspeed(x, 'mor')).reset_index()

    df = pd.DataFrame()

    df['obs'] = speed_morning[0]

    return df

def creat_word_table(document, title, df):
    from docx import Document
    from docx.enum.table import WD_TABLE_ALIGNMENT
    from docx.shared import Cm
    from docx.oxml.ns import qn
    '''
    document = Document()
document.styles['Normal'].font.name = '宋体'
document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')'''
    from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
    p = document.add_paragraph('')
    p.add_run(title).bold = True
    p.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    tb = document.add_table(rows=len(df.index), cols=len(df.columns))
    # tb.add_row()

    for row in range(len(df.index)):
        for col in range(len(df.columns)):
            tb.cell(row, col).text = str(df.iloc[row, col])

    tb.style = 'Table Grid'
    tb.autofit = True



def main(stast_df, type,station, windplus, document, compare_index):

    time_list = time_list_all[type - 4]
    print('请注意检查time_list')
    for time in time_list:
        time_a, time_b = time.split('.')
        if len(time_b) < 2:
            time_b = '0%s' % time_b
        if len(time_a) < 2:
            time_a = '0%s' % time_a

        time_format = '%s%s' % (time_a, time_b)
        print(time_format)
        a = pd.read_excel(r'D:\basis\some_projects\zhj\渔场风场绘制表格\wentai\%i月渔场风力预报.xlsx' % type,
                          sheet_name=time)
        a = a.iloc[1:4]

        ##############将上下午合并成一天#################################
        def return_max(df):
            result1 = []
            result2 = []
            print(df)
            for index, series in df.iterrows():
                be1 = series.iloc[1];
                be2 = series.iloc[3]

                if len(be1) != 1:
                    b1 = int(be1[0]);
                    e1 = int(series.iloc[1][2])
                else:
                    sb1 = int(be1)

                if len(be2) != 1:
                    b2 = int(series.iloc[3][0]);
                    e2 = int(series.iloc[3][2])
                else:
                    sb2 = int(be2)
                if ('e1' in vars()) & ('e2' in vars()):
                    if (b1 >= b2) & (e1 >= e2):
                        result1.append(series.iloc[0])
                        result2.append(series.iloc[1])
                    else:
                        result1.append(series.iloc[2])
                        result2.append(series.iloc[3])
                else:
                    if ('sb1' in vars()):
                        if (sb1 >= e2):
                            result1.append(series.iloc[0])
                            result2.append(series.iloc[1])
                        else:
                            result1.append(series.iloc[2])
                            result2.append(series.iloc[3])
                    if ('sb2' in vars()):
                        if (sb2 < e1):
                            result1.append(series.iloc[0])
                            result2.append(series.iloc[1])
                        else:
                            result1.append(series.iloc[2])
                            result2.append(series.iloc[3])

            return result1, result2

        trans_huanghai1, trans_huanghai2 = return_max(a[['浙江南部沿岸', '（上午）', '浙江南部沿岸.1', '（下午）']])
        print(trans_huanghai2)
        a['浙江南部沿岸'] = trans_huanghai1
        a[''] = trans_huanghai2
        a = a[['温台渔场', 'Unnamed: 1', 'Unnamed: 2', '东海南部', 'Unnamed: 4', '浙江南部沿岸', '']]
        print(a.columns)

        date_ranges = pd.date_range('2020-%s-%s' % (time_format[:2], time_format[2:]), periods=3,
                                    freq='1D').strftime('%m.%d')
        trans_date = np.array(date_ranges)
        a['温台渔场'] = trans_date

        # 找到华东院数据
        b = cal(time_format, station, windplus)
        if b is False:
            continue
        b = b.iloc[0:3].set_index(a.index)
        # 合并两个数据
        df = pd.concat([a, b], axis=1)

        # 找到华东院实测数据
        obs_station = station
        if (station == '3303273A'):
            obs_station = '33032701'
        elif (station == '3303811A'):
            obs_station = '33038101'
        b = cal2(time_format, obs_station, windplus)
        if b is False:
            print('skip ############################################')
            continue
        b = b.iloc[0:3].set_index(df.index)
        # 合并两个数据
        df = pd.concat([df, b], axis=1)

        title_df.columns = df.columns
        df = pd.concat([title_df, df], axis=0)
        df = df[['温台渔场', 'Unnamed: 1', 'Unnamed: 2', 'winddir', 'sp', 'obs',
                 '东海南部', 'Unnamed: 4',
                 '浙江南部沿岸', '']]
        df = df.fillna('')

        df = df[['obs','sp', 'Unnamed: 2', 'Unnamed: 4', '']]
        df = df.iloc[2:]
        df = df.T
        pp_result_all = []

        for pred_days in range(1, 4):
            pp_1 = judge_right_wrong(df[pred_days].iloc[0], df[pred_days].iloc[compare_index])
            pp_result_all.append(pp_1)
        # for pred_days in range(1, 4):
        #     pp_1 = judge_right_wrong(df[pred_days].iloc[0], df[pred_days].iloc[2:])
        #     pp_2 = judge_right_wrong(df[pred_days].iloc[1], df[pred_days].iloc[2:])
        #
        #     pp_result_all.append(max(pp_1, pp_2))
        stast_df[time] = pp_result_all

    return stast_df
'''温台渔场，最终能不能生成这样的excel。就是，观测分别与华东院以及其他家，独立做比对。'''

def judge_right_wrong(pred, obs):
    '''pred: str, '5-6';
    obs: np.array, [5-6,7-8,9-10,...] '''
    pred = str(pred)

    if isinstance(obs, str):
        if pred == obs:
            return 1
        else:
            return -1

    def trans_obs_into_setp1(obs):
        level_higher_than_3 = False
        result = []
        for i in np.array(obs):
            try:
                obs0, obs1 = i.split('-')
                obs0 = int(obs0);
                obs1 = int(obs1)
                ss = obs1 - obs0
                if ss > 2:
                    level_higher_than_3 = True
                if ss != 1:
                    for find in range(ss):
                        result.append('%i-%i' % (obs0 + find * 1, obs0 + 1 + find * 1))
                else:
                    result.append('%i-%i' % (obs0, obs1))
            except:
                i = int(i)
                result.append('%i-%i' % (i-1, i))
                result.append('%i-%i' % (i, i+1))

        return [pd.DataFrame(result), level_higher_than_3]

    obs_new, level_higher_than_3 = trans_obs_into_setp1(obs)

    def judge_for_obs_in_step1(pred, obs_new):
        # 当 pred 在 obs里
        if np.isin(pred, obs_new):

            a = obs_new.value_counts()
            # 判断出现最多的元素的列表
            max_freq = np.max(np.array(a))
            freq_list = a.index[np.array(a) == max_freq]
            if np.isin(pred, freq_list):
                return 1
            else:

                if level_higher_than_3 == True:
                    pred_0 ,pred_1=pred.split('-')
                    pred_0 = int(pred_0);
                    pred_1 = int(pred_1)
                    compare = []
                    for i in np.array(obs):
                        obs0, obs1 = i.split('-')
                        obs0 = int(obs0);
                        obs1 = int(obs1)
                        if (pred_0 >= obs0) & (pred_1 <= obs1):
                            compare.append(1)
                        else:
                            compare.append(0)
                    if sum(compare) >= 2:
                        return 1
                    else:
                        return 0


                return 0
        else:
            return -1

    return judge_for_obs_in_step1(pred, obs_new)

station = ['3303273A', '3303811A']
stationname = ['3273A', '3811A']


title_df = pd.DataFrame()
title_df['title1'] = ['启东', '中信海洋', '', '东海南部', ''	,
                      '浙江南部沿岸', '', '温州','', '实测']
title_df['title2'] = ['预报日期','风向','风级',
                      '风向','风级','风向','风级','风向','风级', '风级']
title_df = title_df.T

time_list_all=[['4.11', '4.12', '4.13', '4.15', '4.17', '4.18', '4.19', '4.20', '4.21', '4.22', '4.23', '4.26', '4.27', '4.29', '4.30'], ['5.1', '5.2', '5.3', '5.4', '5.5', '5.8', '5.10', '5.11', '5.15', '5.16', '5.17', '5.18', '5.19', '5.21', '5.23', '5.24', '5.25', '5.28', '5.29', '5.30', '5.31'], ['6.1', '6.4', '6.6', '6.8', '6.10', '6.12', '6.13', '6.15', '6.17', '6.26', '6.27', '6.29'], ['7.1', '7.3', '7.7', '7.9', '7.11', '7.12', '7.14', '7.17', '7.19', '7.20', '7.22', '7.26', '7.28', '7.29']]

choose_month = 1
if choose_month == 1:
    huadongyuan_data_add = r'D:\basis\some_projects\zhj\渔场风场绘制表格\forecast\forecast'
    station = ['3303273A', '3303811A']
if choose_month == 2:
    huadongyuan_data_add = r'D:\basis\some_projects\zhj\渔场风场绘制表格\forecast\78'
    station = ['33032701', '33038101']

save_name = ['华东院', '中信海洋', '东海南部','浙江南部沿岸']
compare_index_all = [1,2,3,4]

def mainmain(station, stationname,ci):

    sta_df_all = pd.DataFrame()
    # for plus in [1]:
    for plus in [1,1.1,1.2,1.3,1.4,1.5]:
        from docx import Document
        from docx.oxml.ns import qn
        document = Document()
        document.styles['Normal'].font.name = '宋体'
        document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), '宋体')

        stast_df = pd.DataFrame()
        if choose_month == 1:
            main(stast_df, 4,station, plus, document, compare_index_all[ci])
            main(stast_df, 5, station, plus, document, compare_index_all[ci])
        if choose_month == 2:
            main(stast_df, 6, station, plus, document, compare_index_all[ci])
            main(stast_df, 7, station, plus, document, compare_index_all[ci])

        sta_df_all = pd.concat((sta_df_all, stast_df.T), axis=1)


    value_df = pd.DataFrame()
    scol = 0
    for index, item in sta_df_all.iteritems():  # 遍历dataframe的每一列
        pd1 = pd.DataFrame({"2": item.value_counts().values}, index=item.value_counts().index)
        value_df[scol] = pd1['2']
        scol = scol + 1
    value_df = value_df.sort_index()
    value_df.columns = sta_df_all.columns
    sta_df_all = pd.concat((sta_df_all, value_df), axis=0)
    sta_df_all.to_csv('%s风速_%s对比.csv'%(stationname, save_name[ci]))
    # document.save('%s风速乘%s.docx'%(stationname, str(plus)))
for i in range(0,2):
    mainmain(station[i], stationname[i], 0)

# for ci in range(1,4):
#     for i in range(0,2):
#         mainmain(station[i], stationname[i], ci)


def get_sheet():
    result = []
    for i in range(4,8):
        xl = pd.ExcelFile(r'D:\basis\some_projects\zhj\渔场风场绘制表格\wentai\%i月渔场风力预报.xlsx'%i)

        result.append(xl.sheet_names)  # see all sheet names

    print(result)

# get_sheet()