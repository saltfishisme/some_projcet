
import numpy as np
from netCDF4 import Dataset
import os

def Etopo(lon_area, lat_area, resolution):
    ### Input
    # resolution: resolution of topography for both of longitude and latitude [deg]
    # (Original resolution is 0.0167 deg)
    # lon_area and lat_area: the region of the map which you want like [100, 130], [20, 25]
    ###
    ### Output
    # Mesh type longitude, latitude, and topography data
    ###

    # Read NetCDF data
    data = Dataset(r"E:\ETOPO1_Ice_g_gdal.grd", "r")

    # Get data
    lon_range = data.variables['x_range'][:]
    lat_range = data.variables['y_range'][:]
    topo_range = data.variables['z_range'][:]
    spacing = data.variables['spacing'][:]
    dimension = data.variables['dimension'][:]
    z = data.variables['z'][:]
    lon_num = dimension[0]
    lat_num = dimension[1]

    # Prepare array
    lon_input = np.zeros(lon_num);
    lat_input = np.zeros(lat_num)
    for i in range(lon_num):
        lon_input[i] = lon_range[0] + i * spacing[0]
    for i in range(lat_num):
        lat_input[i] = lat_range[0] + i * spacing[1]

    # Create 2D array
    lon, lat = np.meshgrid(lon_input, lat_input)

    # Convert 2D array from 1D array for z value
    topo = np.reshape(z, (lat_num, lon_num))

    # Skip the data for resolution
    if ((resolution < spacing[0]) | (resolution < spacing[1])):
        print('Set the highest resolution')
    else:
        skip = int(resolution / spacing[0])
        lon = lon[::skip, ::skip]
        lat = lat[::skip, ::skip]
        topo = topo[::skip, ::skip]

    topo = topo[::-1]

    # Select the range of map
    range1 = np.where((lon >= lon_area[0]) & (lon <= lon_area[1]))
    lon = lon[range1];
    lat = lat[range1];
    topo = topo[range1]
    range2 = np.where((lat >= lat_area[0]) & (lat <= lat_area[1]))
    lon = lon[range2];
    lat = lat[range2];
    topo = topo[range2]

    # Convert 2D again
    lon_num = len(np.unique(lon))
    lat_num = len(np.unique(lat))
    lon = np.reshape(lon, (lat_num, lon_num))
    lat = np.reshape(lat, (lat_num, lon_num))
    topo = np.reshape(topo, (lat_num, lon_num))

    return lon, lat, topo


def degree2radians(degree):
    # convert degrees to radians
    return degree * np.pi / 180


def mapping_map_to_sphere(lon, lat, radius=1):
    # this function maps the points of coords (lon, lat) to points onto the sphere of radius radius
    lon = np.array(lon, dtype=np.float64)
    lat = np.array(lat, dtype=np.float64)
    lon = degree2radians(lon)
    lat = degree2radians(lat)
    xs = radius * np.cos(lon) * np.cos(lat)
    ys = radius * np.sin(lon) * np.cos(lat)
    zs = radius * np.sin(lat)
    return xs, ys, zs


# Import topography data
# Select the area you want
resolution = 0.8
lon_area = [-180., 180.]
lat_area = [-90., 90.]
# Get mesh-shape topography data
lon_topo, lat_topo, topo = Etopo(lon_area, lat_area, resolution)
xs, ys, zs = mapping_map_to_sphere(lon_topo, lat_topo)

from matplotlib import cm
from matplotlib.colors import LightSource
import matplotlib.pyplot as plt


my_col = cm.jet(topo/np.amax(topo))
print(my_col.shape)
print(topo.shape)

Ctopo = [[0, 'rgb(0, 0, 70)'],[0.2, 'rgb(0,90,150)'],
          [0.4, 'rgb(150,180,230)'], [0.5, 'rgb(210,230,250)'],
          [0.50001, 'rgb(0,120,0)'], [0.57, 'rgb(220,180,130)'],
          [0.65, 'rgb(120,100,0)'], [0.75, 'rgb(80,70,0)'],
          [0.9, 'rgb(200,200,200)'], [1.0, 'rgb(255,255,255)']]

values = [-1.1,0, 0.2, 0.4, 0.5, 0.50001, 0.57, 0.65, 0.75, 0.9, 1.1]
colors =['#000046', '#005A96', '#96B4E6', '#D2E6FA', '#007800', '#DCB482', '#786400', '#504600', '#C8C8C8', '#FFFFFF']
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import cmaps
cmap =cmaps.GMT_nighttime.resampled(100)



# import pandas as pd
# topo_copy = topo.copy()
# topo_copy[topo_copy<-6000] = -6000
# topo_copy[topo_copy>6000] = 6000
# topo_copy = topo_copy/6000
# print(topo_copy.shape)
# img = np.empty(my_col.shape)
# for i in range(topo_copy.shape[0]):
#     for j in range(topo_copy.shape[1]):
#         for iii in range(4):
#             img[i, j, iii] = cmap(topo_copy[i,j])[iii]


# a = pd.cut(topo_copy.flatten()/6000, values, labels=colors)
# print(np.max(topo_copy), np.min(topo_copy))
# a = np.array(a).reshape(topo.shape)

import PIL
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

# load bluemarble with PIL
bm = PIL.Image.open(r"D:\earthmap.jpg")
# it's big, so I'll rescale it, convert to array, and divide by 256 to get RGB values that matplotlib accept
# bm = np.array(bm.resize([d/5 for d in bm.size]))/256.
bm = np.array(bm)/256


# coordinates of the image - don't know if this is entirely accurate, but probably close
lons = np.linspace(-180, 180, bm.shape[1])
lats = np.linspace(-90, 90, bm.shape[0])[::-1]

# repeat code from one of the examples linked to in the question, except for specifying facecolors:
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d',computed_zorder=False)

lons, lats = np.meshgrid(lons, lats)
x,y,z = mapping_map_to_sphere(lons, lats)
ax.view_init(elev=41, azim=85)
ax.plot_surface(x, y, z, rstride=2, cstride=2, facecolors = bm, zorder=20)
ax.set_axis_off()
# plt.show()



import xarray as xr
def roll_longitude_from_359to_negative_180(long):
    return xr.where(long > 180,
        long - 360,
        long)

import numpy as np
import xarray as xr
uv1 = xr.open_dataset(r'uv_01.nc')
uv2 = xr.open_dataset(r'uv_02.nc')
uv_polar = xr.concat([uv1, uv2], dim="time").mean(dim='time').sel(latitude=slice(70,50))
uv_extro = xr.concat([uv1, uv2], dim="time").mean(dim='time').sel(latitude=slice(20,0))

def call_scatter(uv, wind_thre=16):
    u = uv['u'].values
    v = uv['v'].values
    wind_speed = np.sqrt(u**2+v**2)
    lat = uv.latitude.values
    lon = uv.longitude.values
    wind_lon_2d, wind_lat_2d = np.meshgrid(lon, lat)

    evlon = roll_longitude_from_359to_negative_180(wind_lon_2d)

    fig, ax = plt.subplots(1,1, subplot_kw={'projection':ccrs.})


    evlat = wind_lat_2d

    # Convert to spherical coordinates
    xs_ev_org, ys_ev_org, zs_ev_org = mapping_map_to_sphere(evlon, evlat)


    ratio = 1.12
    xs_ev_up = xs_ev_org*ratio
    ys_ev_up = ys_ev_org*ratio
    zs_ev_up = zs_ev_org*ratio



    # ax.contour(xs_ev_up, ys_ev_up, zs_ev_up,
    #            extend3d=True, cmap=cm.coolwarm, zorder=50)

    arg_index = np.where(wind_speed>wind_thre)


    ax.scatter(xs_ev_up[arg_index].flatten(),
                          ys_ev_up[arg_index].flatten(),
                          zs_ev_up[arg_index].flatten(), zorder=20, alpha=0.1)

call_scatter(uv_polar, 16)
# call_scatter(uv_extro, 1)
# fig.write_html("D:/test.html", include_plotlyjs=True, full_html=True)
plt.show()
# import cdsapi
#
# c = cdsapi.Client()
#
# c.retrieve(
#     'reanalysis-era5-pressure-levels-monthly-means',
#     {
#         'format': 'netcdf',
#         'product_type': 'monthly_averaged_reanalysis',
#         'variable': [
#             'u_component_of_wind', 'v_component_of_wind',
#         ],
#         'pressure_level': [
#             '200', '300',
#         ],
#         'year': [
#             '1993',
#         ],
#         'month': [
#             '01','02'
#         ],
#         'time': '00:00',
#     },
#     'uv_02_1992.nc')
#
# exit(0)
""" +====================================================================="""
######### backup ###############


import numpy as np
from netCDF4 import Dataset
import os

import nc4


def Etopo(lon_area, lat_area, resolution):
    ### Input
    # resolution: resolution of topography for both of longitude and latitude [deg]
    # (Original resolution is 0.0167 deg)
    # lon_area and lat_area: the region of the map which you want like [100, 130], [20, 25]
    ###
    ### Output
    # Mesh type longitude, latitude, and topography data
    ###

    # Read NetCDF data
    data = Dataset(r"E:\ETOPO1_Ice_g_gdal.grd", "r")

    # Get data
    lon_range = data.variables['x_range'][:]
    lat_range = data.variables['y_range'][:]
    topo_range = data.variables['z_range'][:]
    spacing = data.variables['spacing'][:]
    dimension = data.variables['dimension'][:]
    z = data.variables['z'][:]
    lon_num = dimension[0]
    lat_num = dimension[1]

    # Prepare array
    lon_input = np.zeros(lon_num);
    lat_input = np.zeros(lat_num)
    for i in range(lon_num):
        lon_input[i] = lon_range[0] + i * spacing[0]
    for i in range(lat_num):
        lat_input[i] = lat_range[0] + i * spacing[1]

    # Create 2D array
    lon, lat = np.meshgrid(lon_input, lat_input)

    # Convert 2D array from 1D array for z value
    topo = np.reshape(z, (lat_num, lon_num))

    # Skip the data for resolution
    if ((resolution < spacing[0]) | (resolution < spacing[1])):
        print('Set the highest resolution')
    else:
        skip = int(resolution / spacing[0])
        lon = lon[::skip, ::skip]
        lat = lat[::skip, ::skip]
        topo = topo[::skip, ::skip]

    topo = topo[::-1]

    # Select the range of map
    range1 = np.where((lon >= lon_area[0]) & (lon <= lon_area[1]))
    lon = lon[range1];
    lat = lat[range1];
    topo = topo[range1]
    range2 = np.where((lat >= lat_area[0]) & (lat <= lat_area[1]))
    lon = lon[range2];
    lat = lat[range2];
    topo = topo[range2]

    # Convert 2D again
    lon_num = len(np.unique(lon))
    lat_num = len(np.unique(lat))
    lon = np.reshape(lon, (lat_num, lon_num))
    lat = np.reshape(lat, (lat_num, lon_num))
    topo = np.reshape(topo, (lat_num, lon_num))

    return lon, lat, topo


def degree2radians(degree):
    # convert degrees to radians
    return degree * np.pi / 180


def mapping_map_to_sphere(lon, lat, radius=1):
    # this function maps the points of coords (lon, lat) to points onto the sphere of radius radius
    lon = np.array(lon, dtype=np.float64)
    lat = np.array(lat, dtype=np.float64)
    lon = degree2radians(lon)
    lat = degree2radians(lat)
    xs = radius * np.cos(lon) * np.cos(lat)
    ys = radius * np.sin(lon) * np.cos(lat)
    zs = radius * np.sin(lat)
    return xs, ys, zs


# Import topography data
# Select the area you want
resolution = 0.25
lon_area = [-180., 180.]
lat_area = [-90., 90.]
# Get mesh-shape topography data
lon_topo, lat_topo, topo = Etopo(lon_area, lat_area, resolution)
xs, ys, zs = mapping_map_to_sphere(lon_topo, lat_topo)




Ctopo = [[0, 'rgb(0, 0, 70)'],[0.2, 'rgb(0,90,150)'],
          [0.4, 'rgb(150,180,230)'], [0.5, 'rgb(210,230,250)'],
          [0.50001, 'rgb(0,120,0)'], [0.57, 'rgb(220,180,130)'],
          [0.65, 'rgb(120,100,0)'], [0.75, 'rgb(80,70,0)'],
          [0.9, 'rgb(200,200,200)'], [1.0, 'rgb(255,255,255)']]


cmin = -8000
cmax = 8000

# import PIL
# import matplotlib.pyplot as plt
# import numpy as np
#
# # load bluemarble with PIL
# bm = PIL.Image.open(r"D:\earthmap.jpg")
# # it's big, so I'll rescale it, convert to array, and divide by 256 to get RGB values that matplotlib accept
# # bm = np.array(bm.resize([d/5 for d in bm.size]))/256.
# bm = np.array(bm)[::6,::6]
# bm_str = []
# for ii in range(bm.shape[0]):
#     for jj in range(bm.shape[1]):
#         C=np.array(bm[ii,jj])
#         bm_str.append('rgb' + str((C[0], C[1], C[2])))
# bm = np.array(bm_str).reshape(bm.shape[0:2])
# print(bm)
# # nc4.save_nc4(bm_str, 'bm_str')
# # exit(0)
#
# # bm = nc4.read_nc4('bm_str')
# # print(bm)
# # exit(0)
# # coordinates of the image - don't know if this is entirely accurate, but probably close
# lons = np.linspace(-180, 180, bm.shape[1])
# lats = np.linspace(-90, 90, bm.shape[0])[::-1]
#
# bm = bm
# lons = lons
# lats = lats
# # repeat code from one of the examples linked to in the question, except for specifying facecolors:
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d',computed_zorder=False)
#
# lons, lats = np.meshgrid(lons, lats)
# x,y,z = mapping_map_to_sphere(lons, lats)

topo_sphere=dict(type='surface',
  x=xs,
  y=ys,
  z=zs,

  surfacecolor=topo,
colorscale=Ctopo,
  cmin=cmin,
  cmax=cmax)


noaxis=dict(showbackground=False,
  showgrid=False,
  showline=False,
  showticklabels=False,
  ticks='',
  title='',
  zeroline=False)


import plotly.graph_objs as go

titlecolor = 'white'
bgcolor = 'black'

layout = go.Layout(
  autosize=False, width=1200, height=800,
  title = '3D spherical topography map',
  titlefont = dict(family='Courier New', color=titlecolor),
  showlegend = False,
  scene = dict(
    xaxis = noaxis,
    yaxis = noaxis,
    zaxis = noaxis,
    aspectmode='manual',
    aspectratio=go.layout.scene.Aspectratio(
      x=1, y=1, z=1)),
  paper_bgcolor = bgcolor,
  plot_bgcolor = bgcolor)


from plotly.offline import plot

import xarray as xr
def roll_longitude_from_359to_negative_180(long):
    return xr.where(long > 180,
        long - 360,
        long)

import numpy as np
import xarray as xr
uv1 = xr.open_dataset(r'uv_01_1992.nc')
uv2 = xr.open_dataset(r'uv_02_1992.nc')
uv_polar = xr.concat([uv1, uv2], dim="time").sel(latitude=slice(90,53),level=200).isel(time=0)
uv_extro = xr.concat([uv1, uv2], dim="time").sel(latitude=slice(50,0),longitude=slice(10,190),level=300).isel(time=0)


def call_scatter(uv, wind_thre=16, wind_max=30,cbar='?'):
    u = uv['u'].values
    v = uv['v'].values
    wind_speed = np.sqrt(u**2+v**2)
    lat = uv.latitude.values
    lon = uv.longitude.values
    wind_lon_2d, wind_lat_2d = np.meshgrid(lon, lat)

    evlon = roll_longitude_from_359to_negative_180(wind_lon_2d)

    evlat = wind_lat_2d
    evDepth = wind_speed


    # Convert to spherical coordinates
    xs_ev_org, ys_ev_org, zs_ev_org = mapping_map_to_sphere(evlon, evlat)


    ratio = 1.1
    xs_ev_up = xs_ev_org*ratio
    ys_ev_up = ys_ev_org*ratio
    zs_ev_up = zs_ev_org*ratio

    import matplotlib
    from matplotlib import cm

    def matplotlib_to_plotly(cmap, pl_entries):
        h = 1.0 / (pl_entries - 1)
        pl_colorscale = []

        for k in range(pl_entries):
            C = list(map(np.uint8, np.array(cmap(k * h)[:3]) * 255))
            pl_colorscale.append([k * h, 'rgb' + str((C[0], C[1], C[2]))])

        return pl_colorscale

    def MlibCscale_to_Plotly(cbar, bar_range=None):
        if bar_range is None:
            bar_range = [0.25, 1]
        from matplotlib.colors import ListedColormap
        viridis_big = matplotlib.colormaps[cbar]
        cmap = ListedColormap(viridis_big(np.linspace(bar_range[0], bar_range[1], 128)))
        # cmap = matplotlib.cm.get_cmap(cbar)
        rgb = []
        norm = matplotlib.colors.Normalize(vmin=wind_thre, vmax=wind_max)

        for i in range(wind_thre, wind_max):
            k = matplotlib.colors.colorConverter.to_rgb(cmap(norm(i)))
            rgb.append(k)

        Cscale = matplotlib_to_plotly(cmap, wind_max)

        return Cscale

    def to01(x):
        result = (x - np.min(x)) / (np.max(x) - np.min(x))
        result[result<0] = 0
        result[result>1] = 1
        print(result)
        return result



    def plot_scatter(arg_index, alpha, bar_range):
        return go.Scatter3d(x = xs_ev_up[arg_index].flatten(),
                              y = ys_ev_up[arg_index].flatten(),
                              z = zs_ev_up[arg_index].flatten(),
                              mode='markers',
                            opacity=alpha,
                              marker = dict(
                                  size = 1.5,
                                  ### choose color option
                                  # color='blue',
                                  color=evDepth[arg_index].flatten(),
                                  ### choose color option
                                  colorscale=MlibCscale_to_Plotly(cbar, bar_range),

                                  ### choose color option,

        ),
                              )

    scatter_feature = []

    levels = np.arange(0,1.01,0.1)
    alpha = np.arange(0.01,1.001,0.1)
    compare = to01(wind_speed)

    lat_skip = 4
    lon_skip = 8
    cone = go.Streamtube(x=xs_ev_up[::lat_skip, ::lon_skip].flatten(),
                   y=ys_ev_up[::lat_skip, ::lon_skip].flatten(),
                   z=zs_ev_up[::lat_skip, ::lon_skip].flatten(),
                   u=u[::lat_skip, ::lon_skip].flatten(),
                   v=v[::lat_skip, ::lon_skip].flatten(),
                   w=np.zeros(u.shape)[::lat_skip, ::lon_skip].flatten(),

                )

    scatter_feature.append(cone)
    # for ii in range(len(levels[:-1])):
    #     arg_index = np.where((compare>levels[ii])&(compare<levels[ii+1])&(wind_speed > wind_thre))
    #     scatter_feature.append(plot_scatter(arg_index, alpha[ii], [levels[ii]+0.1, levels[ii+1]+0.1]))

    # print(np.max(wind_speed))
    # arg_index = np.where((wind_speed > wind_thre))
    # scatter_feature.append(plot_scatter(arg_index, 0.8,[0.5,1]))
    return scatter_feature

scatter_feature1 = call_scatter(uv_polar,17,30, 'Blues')
scatter_feature2 = call_scatter(uv_extro,30,70,'YlOrBr')
fig = go.Figure(data=[topo_sphere]+scatter_feature1+scatter_feature2, layout=layout)

fig.update_layout(title_text = '3D spherical topography map')
os.chdir(r"D:/")
plot(fig, validate = False, filename='3DSphericalTopography.html',
    auto_open=True)