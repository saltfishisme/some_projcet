import xarray as xr
import numpy as np
import pandas as pd
from nc4 import read_nc4,save_nc4
import os
import cmaps
'''可以画一个多年平均的全国的可降水量分布图吗？按年春夏秋冬。单位mm/d，就是日均可降水量'''
def get_data(dir):
    time = get_time()

    count = 0
    names = ['all', 'spring', 'summer', 'fall', 'winter']
    for jj, stime in enumerate(time):
        result = np.zeros([181, 360])
        for i in stime:
            count += 1
            a = xr.open_dataset(r'dir/WaterVapor.%s.nc'%i).variables['tcwv'].values
            result = (result+a)/count
        save_nc4(result, 'pwat_%s'%names[jj])
#
def get_time():
    year = pd.date_range('19790101', '20191231', freq='1D')
    spring = year[np.isin(year.month,[3,4,5])].strftime('%Y%m%d')
    summer = year[np.isin(year.month, [6, 7, 8])].strftime('%Y%m%d')
    fall = year[np.isin(year.month, [9, 10, 11])].strftime('%Y%m%d')
    winter = year[np.isin(year.month,[12,1,2])].strftime('%Y%m%d')
    result = [year.strftime('%Y%m%d'), spring, summer, fall, winter]
    return [len(i) for i in result]
#

# get_data('/home/linhaozhong/work/Data/MoistureTrackData/ERA5/Total_column_water_vapour')
b = get_time()
names = ['all', 'spring', 'summer', 'fall', 'winter']
ss = xr.open_dataset(r'D:\a_matlab\era5\WaterVapor.19790101.nc')
lats = ss.latitude.values
lons = ss.longitude.values

def plotss(type1, title):
    import numpy as np
    import matplotlib.pyplot  as plt
    import proplot as plot
    import cartopy.crs as ccrs

    # 改变颜色

    fig, ax = plot.subplots(projection=ccrs.AlbersEqualArea(central_longitude=105),
                            figsize=[5, 4], tight=False
                            , left='1em', top='2em', bottom='4em')
    ax = ax[0]

    def plots(ax):
        fill = ax.contourf(lons, lats, type1, levels=levels,transform=ccrs.PlateCarree(),
                           cmap=cmaps.MPL_YlGnBu)
        cbaxes = fig.add_axes([0.12, 0.08, 0.78, 0.01])
        cb = plt.colorbar(fill, cax=cbaxes, orientation="horizontal")

        from plot_in_proplot import shp_clip_for_proplot
        shp_clip_for_proplot(ax, fill, clipshape=True)

    def plotss(ax):
        fill = ax.contourf(lons, lats, type1, levels=levels,transform=ccrs.PlateCarree(),
                           cmap=cmaps.MPL_YlGnBu)

        ax.set_extent(([105, 122.5, 2.5, 25]), crs=ccrs.PlateCarree())
        from plot_in_proplot import shp_clip_for_proplot
        shp_clip_for_proplot(ax, fill, clipshape=True)

    plots(ax)
    ax.format(latlim=[17, 54], lonlim=[80, 127])

    ax2 = fig.add_axes([0.2, 0.5, 0.2, 0.2], projection=ccrs.AlbersEqualArea(central_longitude=105))
    plotss(ax2)
    # ax2.format(latlim=[17, 54], lonlim=[80, 127])

    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition

    ip = InsetPosition(ax, [0, -0.045, 0.15, 0.3])
    ax2.set_axes_locator(ip)
    ax.set_title(title)
    # plt.show()
    plt.savefig('%s.png'%title[9:11], dpi=600)
    plt.close()
    # f = a[np.where((a<200)& (a>=100))]
    # print(f.min(), f.max())

datas = []
levels = np.arange(0,70,2)
titles = ['', '春季', '夏季', '秋季', '冬季']
for i in range(5):
    data = read_nc4('pwat_%s'%names[i])

    datas.append((np.mean(data, axis=0))/b[i])
    plotss(datas[i], '1979-2019%s日均可降水量分布图 (mm/d)'%titles[i])


