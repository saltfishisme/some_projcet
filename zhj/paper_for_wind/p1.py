import pandas as pd
import numpy as np


'''1，历史资料分析某个省份或者沿海风的变化趋势；2，可以分析cmip6的资料，看看未来全国风资源的变化。'''

stations = np.loadtxt('station_in_neimenggu.txt')
index = np.loadtxt('index.txt')
stations =[j for j in stations if j in index]

def s1_extract_data():
    '''提取数据'''
    data = pd.read_csv(r'D:\basis\data\row\WIN.txt', delim_whitespace=True, header=None, index_col=0)
    data = data.loc[[int(i) for i in stations]]
    data.to_csv('data.csv')

def screen_data():
    '''处理缺失数据'''
    data = pd.read_csv('data.csv')
    def for_999999(data):
        data[data == 999999] = np.nan
        data[data == 999998] = np.nan
        data[data == 999996] = np.nan
        data[data > 998000] = (data[data > 998000] - 998000)*10
        return data
    def for_32766(data):
        data[data==32766] = np.nan
        data[data > 1000] = data[data > 1000] - 1000
        return data
    # 警告，先9再3
    data[['7','8','10']] = for_999999(data[['7','8','10']])
    data[['7','8','10']] = for_32766(data[['7','8','10']])
    data[['7','8','10']] = data[['7','8','10']]/10
    data = data[data['4']>1978]
    data = data[data['4']<2020]
    data[[str(i) for i in [0,1,2,3,4,5,6,7,8,10]]].to_csv('data_mask.csv')




def f1():
    '''f1'''
    # %%
    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    import proplot as plot
    import cmaps
    def plotss(ax, x, data, position=[0.01, 0.93, 0.85], fontsize=8):
        from pylab import mpl
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        mpl.rcParams['axes.unicode_minus'] = False
        ax.plot(x, data, marker='.', color='black', markersize=5, label='观测')
        ax.format(xlabel='年')

        def fit_line(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            line2 = x * beta_coeff + intercept
            return [beta_coeff, intercept, rvalue, pvalue, stderr], line2

        [beta_coeff, intercept, rvalue, pvalue, stderr], y = fit_line(x, data)
        ax.plot(x, y, linestyle='--', color='black', alpha=0.8, label='趋势', zorder=20)

        ax.text(position[0], position[1],
                'Y = %.2f+(%.3fX)' % (intercept, beta_coeff), transform=ax.transAxes, fontsize=fontsize
                , zorder=50,
                bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', alpha=0.7, pad=0.00001))
        if round(pvalue, 3) < 0.001:
            pvalue = 0.001
        ax.text(position[0], position[2],
                'R${^2}$ = %.3f ,P = %.3f' % (rvalue ** 2, pvalue), transform=ax.transAxes, fontsize=fontsize,
                zorder=50,
                bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', pad=0.00001, alpha=0.8))
        ax.legend(loc='ll', fontsize=fontsize)

    def calcute_ratio(x):
        power_wind = x[(x >= 5) & (x <= 25)].shape[0]
        all_wind = x.shape[0]
        return power_wind / all_wind

    ratio = data['8'].groupby(data['4']).apply(lambda x: calcute_ratio(x))

    # #总站点
    fig, ax = plot.subplots(figsize=[5, 3])
    plotss(ax, ratio.index, np.array(ratio), position=[0.7, 0.93, 0.85], fontsize=9)
    ax.format(ylabel='', title='发电风速年占比多年变化趋势图', xticks=4, xlim=[1978, 2020])

    # plt.show()
    plt.savefig('f1.png', dpi=600)

def f2():
    '''f2'''

    '''计算多年平均， 计算多年趋势'''

    def rbf(ranges, datas, vari):
        from scipy.interpolate import Rbf
        olon = np.linspace(ranges[0][2], ranges[0][3], 88)
        olat = np.linspace(ranges[0][0], ranges[0][1], 88)

        olon, olat = np.meshgrid(olon, olat)
        # 插值处理
        lon = datas['lon']
        lat = datas['lat']
        data = np.array(datas[vari])
        func = Rbf(lon, lat, data, function='linear')
        data_new = func(olon, olat)
        return olon, olat, data_new

    data = pd.read_csv('data_mask.csv')

    lonlat = data[['0', '1', '2']]
    lonlat = lonlat.drop_duplicates(['0'])
    lonlat = lonlat.set_index(lonlat['0'])
    lon = np.array(lonlat['2'] / 100)
    lat = np.array(lonlat['1'] / 100)
    stations = np.array(lonlat.index)

    def fit_lines(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return rvalue

    def fit_lines_p(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return pvalue

    def calcute_ratio(x):
        power_wind = x[(x >= 5) & (x <= 25)].shape[0]
        all_wind = x.shape[0]
        return power_wind / all_wind

    ratio = data['8'].groupby([data['4'], data['0']]).apply(lambda x: calcute_ratio(x)).reset_index()
    sratio = ratio['8'].groupby(ratio['0']).mean()
    linegress = ratio['8'].groupby(ratio['0']).apply(lambda x: fit_lines(np.array(x.index), np.array(x)))
    pvalue = ratio['8'].groupby(ratio['0']).apply(lambda x: fit_lines_p(np.array(x.index), np.array(x)))
    linegress = linegress

    df = pd.DataFrame()
    df['stations'] = stations
    df['lon'] = lon
    df['lat'] = lat
    df['trend'] = np.array(linegress)
    df['p'] = np.array(pvalue)
    df['ratio'] = np.array(sratio)

    olon, olat, odata = rbf([[37, 54, 97, 126]], df, 'trend')
    olon, olat, oratio = rbf([[37, 54, 97, 126]], df, 'ratio')
    oratio = np.clip(oratio, 0, 0.95)
    data = linegress
    labels = '*10${^-}$${^3}$'
    title = '太湖流域站点降水多年趋势图'
    position = [0.84, 0.025]

    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    import proplot as plot
    import cmaps
    fig, ax = plt.subplots(ncols=2, figsize=(6, 2.4), subplot_kw={'projection': ccrs.PlateCarree()})
    # ax.format(lonlim=([97,126]), latlim=(37,54), lonlabels=True, latlabels=True)
    plt.subplots_adjust(top=1.0,
                        bottom=0.2,
                        left=0.065,
                        right=0.965,
                        hspace=0.2,
                        wspace=0.2)

    def trend(ax):
        import cmaps

        # odata=np.ma.masked_where(op>0.05, odata)
        # op[np.where(op>0.05)] = 1
        # op[np.where(op<=0.05)]=0
        colormap = ['#DFDFDF', '#C7C7C7', '#AFAFAF'
            , '#979797', '#7F7F7F', '#373737', '#ffffff', '#0f0f0f']

        fill = ax.contourf(olon, olat, odata, levels=[-0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, 0.3, 1],
                           colors=colormap, alpha=0.7)

        def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1):
            def shp2clip(shpfile):
                import shapefile
                from matplotlib.path import Path
                sf = shapefile.Reader(shpfile)
                vertices = []
                codes = []
                for shape_rec in sf.shapeRecords():
                    pts = shape_rec.shape.points
                    prt = list(shape_rec.shape.parts) + [len(pts)]
                    for i in range(len(prt) - 1):
                        for j in range(prt[i], prt[i + 1]):
                            vertices.append((pts[j][0], pts[j][1]))
                        codes += [Path.MOVETO]
                        codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                        codes += [Path.CLOSEPOLY]
                    clip = Path(vertices, codes)
                return clip

            import cartopy.crs as ccrs
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader
            from matplotlib.patches import PathPatch
            # ax.format(coast=True, lonlabels='b', latlabels='l', lonlim=(ranges[0][2], ranges[0][3]), latlim=(ranges[0][0], ranges[0][1]))

            # # #添加shp文件进入图片中
            if clipshape == True:
                pathss = shp2clip(r'D:\basis\data\shp\heilongjiang.shp')
                plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
                upath = PathPatch(pathss, transform=plate_carre_data_transform)

                for collection in fill.collections:
                    collection.set_clip_path(upath)

            shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\heilongjiang.shp').geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.55)
            ax.add_feature(shape_feature)

        shp_clip_for_proplot(ax, fill=fill)

        sc = ax.scatter(lon, lat, c='black', s=5, edgecolor='black')

        tickloc = [-0.4, -0.2, 0, 0.2, 0.4]
        # ss = plt.colorbar(fill,values=tickloc, loc='b', space='0.2em', width='0.3em', title='aa')
        cb_ax = fig.add_axes([0.55, 0.12, 0.4, 0.02])
        cbar = fig.colorbar(fill, cax=cb_ax, ticks=[-0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, 0.3, 1],
                            orientation='horizontal', aspect=40)
        # ss = ax.colorbar(fill)
        # print(ss.get_xmin)

        # for i in range(len(lon)):
        #     ax.text(lon[i]-0.238, lat[i]+0.029, '%0.2f'%(data[i]),
        #             transform=ccrs.PlateCarree(),
        #             bbox=dict(facecolor='white', alpha=0.4, boxstyle='round'))

        from pylab import mpl
        # plt.rcParams['font.sans-serif'] = ['KaiTi']
        plt.rcParams['axes.unicode_minus'] = False
        # ax[0].format(suptitle=title)
        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        ranges = [[35, 52, 97, 127], [35, 40, 45, 50, 55], [100, 108, 116, 124]]
        ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
        ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
        ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        ax.annotate('(b)', [0.02, 0.92], xycoords='axes fraction')
        # ax[0].annotate(labels, position, xycoords='figure fraction')

    def ratio(ax):
        import cmaps

        # odata=np.ma.masked_where(op>0.05, odata)
        # op[np.where(op>0.05)] = 1
        # op[np.where(op<=0.05)]=0
        colormap = ['#DFDFDF', '#C7C7C7', '#AFAFAF'
            , '#979797', '#7F7F7F', '#373737', '#ffffff', '#0f0f0f']

        fill = ax.contourf(olon, olat, oratio, levels=np.arange(0.4, 1.1, 0.1),
                           cmap=cmaps.gsltod, alpha=0.7)

        def shp_clip_for_proplot(ax, fill=None, clipshape=True, shplinewidth=1):
            def shp2clip(shpfile):
                import shapefile
                from matplotlib.path import Path
                sf = shapefile.Reader(shpfile)
                vertices = []
                codes = []
                for shape_rec in sf.shapeRecords():
                    pts = shape_rec.shape.points
                    prt = list(shape_rec.shape.parts) + [len(pts)]
                    for i in range(len(prt) - 1):
                        for j in range(prt[i], prt[i + 1]):
                            vertices.append((pts[j][0], pts[j][1]))
                        codes += [Path.MOVETO]
                        codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                        codes += [Path.CLOSEPOLY]
                    clip = Path(vertices, codes)
                return clip

            import cartopy.crs as ccrs
            from cartopy.feature import ShapelyFeature
            from cartopy.io.shapereader import Reader
            from matplotlib.patches import PathPatch
            # ax.format(coast=True, lonlabels='b', latlabels='l', lonlim=(ranges[0][2], ranges[0][3]), latlim=(ranges[0][0], ranges[0][1]))

            # # #添加shp文件进入图片中
            if clipshape == True:
                pathss = shp2clip(r'D:\basis\data\shp\heilongjiang.shp')
                plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
                upath = PathPatch(pathss, transform=plate_carre_data_transform)

                for collection in fill.collections:
                    collection.set_clip_path(upath)

            shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\heilongjiang.shp').geometries(),
                                           ccrs.PlateCarree(),
                                           edgecolor='k', facecolor='none', linewidths=0.55)
            ax.add_feature(shape_feature)

        shp_clip_for_proplot(ax, fill=fill)

        sc = ax.scatter(lon, lat, c='black', s=5, edgecolor='black')

        tickloc = [-0.4, -0.2, 0, 0.2, 0.4]
        # ss = plt.colorbar(fill,values=tickloc, loc='b', space='0.2em', width='0.3em', title='aa')
        cb_ax = fig.add_axes([0.05, 0.12, 0.4, 0.02])
        cbar = fig.colorbar(fill, ticks=np.arange(0.4, 1.1, 0.1), cax=cb_ax,
                            orientation='horizontal', aspect=40)

        # print(ss.get_xmin)

        # for i in range(len(lon)):
        #     ax.text(lon[i]-0.238, lat[i]+0.029, '%0.2f'%(data[i]),
        #             transform=ccrs.PlateCarree(),
        #             bbox=dict(facecolor='white', alpha=0.4, boxstyle='round'))

        from pylab import mpl
        # plt.rcParams['font.sans-serif'] = ['KaiTi']
        plt.rcParams['axes.unicode_minus'] = False
        # ax[0].format(suptitle=title)
        from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
        ranges = [[35, 52, 97, 127], [35, 40, 45, 50, 55], [100, 108, 116, 124]]
        ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
        ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
        ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        ax.annotate('(a)', [0.02, 0.92], xycoords='axes fraction')

    ratio(ax[0])
    trend(ax[1])
    # plt.show()
    plt.savefig('f2.png', dpi=600)


data = pd.read_csv('data_mask.csv')


def f3():
    def calcute_ratio(x):
        power_wind = x[(x >= 5) & (x <= 25)].shape[0]
        all_wind = x.shape[0]
        return power_wind / all_wind

    df = pd.DataFrame(index=np.arange(1,13))
    month_ratio = data['8'].groupby(data['5']).apply(lambda x: calcute_ratio(x))
    df['month_ratio'] = np.array(month_ratio)


    def fit_lines(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return rvalue

    def fit_lines_p(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return pvalue


    ratio = data['8'].groupby([data['4'], data['5']]).apply(lambda x: calcute_ratio(x)).reset_index()

    linegress = ratio['8'].groupby(ratio['5']).apply(lambda x: fit_lines(np.array(x.index), np.array(x)))
    pvalue = ratio['8'].groupby(ratio['5']).apply(lambda x: fit_lines_p(np.array(x.index), np.array(x)))

    df['trend'] = np.array(linegress)
    df['p'] = np.array(pvalue)
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(ncols=2, figsize=[7,2.8])
    plt.subplots_adjust(top=0.905,
    bottom=0.18,
    left=0.08,
    right=0.975,
    hspace=0.2,
    wspace=0.3)
    def plot_bar(ax, data, ablabel,ylabel):
        ax.bar(np.arange(1,13), np.array(data), color='white', hatch="///", edgecolor='black')
        ax.set_xticks(np.arange(1,13))
        ax.annotate(ablabel, [0.01, 1.02], xycoords='axes fraction')
        plt.rcParams['font.sans-serif'] = ['simsun']
        plt.rcParams['axes.unicode_minus'] = False
        ax.set_xlabel('月份')
        ax.set_ylabel(ylabel)
    plot_bar(ax[0], df['month_ratio'], '(a)', '')
    plot_bar(ax[1], df['trend'], '(b)','')
    # plt.show()
    plt.savefig('f3.png', dpi=600)


def f4():
    def calcute_ratio_small(x):
        power_wind = x[(x < 3)].shape[0]
        all_wind = x.shape[0]
        return power_wind / all_wind
    def calcute_ratio_big(x):
        power_wind = x[(x >25)].shape[0]
        all_wind = x.shape[0]
        return power_wind / all_wind
    df = pd.DataFrame(index=np.arange(1, 13))

    month_ratio = data['7'].groupby(data['5']).apply(lambda x: calcute_ratio_small(x))
    df['month_ratio_samll'] = np.array(month_ratio)
    month_ratio = data['10'].groupby(data['5']).apply(lambda x: calcute_ratio_big(x))
    df['month_ratio_big'] = np.array(month_ratio)

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(ncols=2, figsize=[7, 2.8])
    plt.subplots_adjust(top=0.905,
                        bottom=0.18,
                        left=0.08,
                        right=0.975,
                        hspace=0.2,
                        wspace=0.3)

    def plot_bar(ax, data, ablabel, ylabel):
        ax.bar(np.arange(1, 13), np.array(data), color='white', hatch="///", edgecolor='black')
        ax.set_xticks(np.arange(1, 13))
        ax.annotate(ablabel, [0.01, 1.02], xycoords='axes fraction')
        plt.rcParams['font.sans-serif'] = ['simsun']
        plt.rcParams['axes.unicode_minus'] = False
        ax.set_xlabel('月份')
        ax.set_ylabel(ylabel)

    plot_bar(ax[0], df['month_ratio_samll'], '(a)', '')
    plot_bar(ax[1], df['month_ratio_big'], '(b)', '')
    plt.show()
    plt.savefig('f4.png', dpi=600)

f4()

