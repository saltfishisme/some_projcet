import pandas as pd
import numpy as np
import xarray as xr
from nc4 import read_nc4, save_nc4
'''1，分析内蒙古高空风变化'''
vwnd = xr.open_dataset(r'D:\basis\data\rea2\monthly\vwnd.mon.mean.nc').sel(time=slice('1979','2019'))
# print(vwnd.level)
uwnd = xr.open_dataset(r'D:\basis\data\rea2\monthly\uwnd.mon.mean.nc').sel(time=slice('1979','2019'))
mask = read_nc4('mask')[0]
# print(mask[np.argwhere(mask>0.1)])
def f1():
    '''f1'''
    # 算年平均

    # vwnd = vwnd.groupby('time.year').mean()
    # uwnd = uwnd.groupby('time.year').mean()
    #
    # vwnd = vwnd['vwnd'].values
    # uwnd = uwnd['uwnd'].values
    # wnd = (vwnd**2 + uwnd**2)**(1/2)
    # save_nc4(wnd, 'f1')
    def fit_lines(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return beta_coeff
    def fit_lines_p(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return pvalue
    data = read_nc4('f1')
    ratio = []
    ratio_p =[]
    for level in range(data.shape[1]):
        sdata =[]
        for year in range(data.shape[0]):
            sdata.append(data[year, level,:,:][np.where(mask>0.1)])
        ss = np.mean(np.array(sdata), axis=1)
        result = fit_lines(np.arange(1979,2020), ss)
        result_p = fit_lines_p(np.arange(1979, 2020), ss)
        ratio.append(result)
        ratio_p.append(result_p)
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(figsize=[6,3])
    plt.subplots_adjust(top=0.95,
    bottom=0.155,
    left=0.12,
    right=0.99,
    hspace=0.2,
    wspace=0.3)
    def plot_bar(ax, data, ablabel,ylabel, hatch='///', x=None):
        if x is None:
            x = np.arange(data.shape[0])
        ax.bar(x, np.array(data), color='white', hatch=hatch, edgecolor='black')
        ax.set_xticks(np.arange(17))
        ax.set_xticklabels([1000,  925,  850,  700,  600,  500,  400,  300,  250,  200,
            150,  100,   70,   50,   30,   20,   10])
        ax.annotate(ablabel, [0.01, 1.02], xycoords='axes fraction')
        ax.axhline(0, color='black', linewidth=1)
        plt.rcParams['font.sans-serif'] = ['simsun']
        plt.rcParams['axes.unicode_minus'] = False
        ax.set_xlabel('气压/hPa')
        ax.set_ylabel(ylabel)
    print(np.argwhere(np.array(ratio_p)<0.1))
    plot_bar(ax, np.array(ratio)[:4], '', '回归系数', hatch='////', x=[0,1,2,3])
    plot_bar(ax, np.array(ratio)[4:], '', '回归系数', hatch='//', x=np.arange(4,17))
    # plt.show()
    plt.savefig('f1.png', dpi=600)

def f2():
    '''f1'''
    # 算年平均
    # vwnd = vwnd.groupby('time.year').mean()
    # uwnd = uwnd.groupby('time.year').mean()
    #
    # vwnd = vwnd['vwnd'].values
    # uwnd = uwnd['uwnd'].values
    # wnd = (vwnd**2 + uwnd**2)**(1/2)
    # save_nc4(wnd, 'f1')

    def fit_lines(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return beta_coeff
    def fit_lines_p(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return pvalue
    data = read_nc4('f1')
    ratio = []
    level_rank = [[2,3,4,5,6], [7,8,9,10], [11,12,13]]
    for levels in level_rank:
        adata = []
        for level in levels:
            sdata = []
            for year in range(data.shape[0]):
                sdata.append(data[year, level, :, :][np.where(mask > 0.1)])
            ss = np.mean(np.array(sdata), axis=1)
            adata.append(ss)
        ratio.append(np.mean(np.array(adata), axis=0))

    import matplotlib.pyplot as plt
    import proplot as plot
    fig, ax = plot.subplots(nrows=3, figsize=[3.75,4.5])
    # plt.subplots_adjust(top=0.95,
    # bottom=0.155,
    # left=0.12,
    # right=0.99,
    # hspace=0.2,
    # wspace=0.3)

    def plotss(ax, x, data, position=[0.01, 0.93, 0.85], fontsize=8):
        from pylab import mpl
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        mpl.rcParams['axes.unicode_minus'] = False
        ax.plot(x, data, marker='.', color='black', markersize=5, label='观测')
        ax.format(xlabel='年')

        def fit_line(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            line2 = x * beta_coeff + intercept
            return [beta_coeff, intercept, rvalue, pvalue, stderr], line2

        [beta_coeff, intercept, rvalue, pvalue, stderr], y = fit_line(x, data)
        ax.plot(x, y, linestyle='--', color='black', alpha=0.8, label='趋势', zorder=20)

        ax.text(position[0], position[1],
                'y=%.2f+(%.2fx)' % (intercept, beta_coeff), transform=ax.transAxes, fontsize=fontsize
                , zorder=50,
                bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', alpha=0.7, pad=0.00001))
        ax.text(position[0], position[2],
                'R=%.3f ,P=%.3f' % (rvalue, pvalue), transform=ax.transAxes, fontsize=fontsize, zorder=50,
                bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', pad=0.00001, alpha=0.8))
        ax.legend(loc='lr', fontsize=6)
    print(ratio)
    for i in range(3):
        plotss(ax[i], np.arange(1979,2020), ratio[i], position=[0.69,0.91,0.81])
    # plt.rcParams['font.sans-serif'] = ['simsun']
    # plt.rcParams['axes.unicode_minus'] = False
    ax.format(ylabel='风速/(m*s${^-}$${^1}$)',abc=True, abcloc='ul', abcstyle='(a)')
    # plt.show()
    plt.savefig('f2.png', dpi=600)


def f3():
    '''f1'''
    # 算年平均
    # vwnd = vwnd.groupby('time.year').mean()
    # uwnd = uwnd.groupby('time.year').mean()
    #
    # vwnd = vwnd['vwnd'].values
    # uwnd = uwnd['uwnd'].values
    # wnd = (vwnd**2 + uwnd**2)**(1/2)
    # save_nc4(wnd, 'f1')

    data = read_nc4('f1')
    ratio = []
    level_rank = [[2,3,4,5,6], [7,8,9,10], [11,12,13]]
    for levels in level_rank:
        adata = []
        for level in levels:
            sdata = []
            for year in range(data.shape[0]):
                sdata.append(data[year, level, :, :][np.where(mask > 0.1)])
            ss = np.mean(np.array(sdata), axis=1)
            adata.append(ss)
        ratio.append(np.mean(np.array(adata), axis=0))

    import matplotlib.pyplot as plt
    import proplot as plot
    fig, ax = plot.subplots(nrows=3, figsize=[3.75,4.5])
    # plt.subplots_adjust(top=0.95,
    # bottom=0.155,
    # left=0.12,
    # right=0.99,
    # hspace=0.2,
    # wspace=0.3)

    def plotss(ax, x, data, position=[0.01, 0.93, 0.85], fontsize=8):
        from pylab import mpl
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        mpl.rcParams['axes.unicode_minus'] = False
        ax.plot(x, data, marker='.', color='black', markersize=5, label='观测')
        ax.format(xlabel='年')

        def fit_line(x, y):
            from scipy.stats import linregress
            (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
            line2 = x * beta_coeff + intercept
            return [beta_coeff, intercept, rvalue, pvalue, stderr], line2

        [beta_coeff, intercept, rvalue, pvalue, stderr], y = fit_line(x, data)
        ax.plot(x, y, linestyle='--', color='black', alpha=0.8, label='趋势', zorder=20)

        ax.text(position[0], position[1],
                'y=%.2f+(%.2fx)' % (intercept, beta_coeff), transform=ax.transAxes, fontsize=fontsize
                , zorder=50,
                bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', alpha=0.7, pad=0.00001))
        ax.text(position[0], position[2],
                'R=%.3f ,P=%.3f' % (rvalue, pvalue), transform=ax.transAxes, fontsize=fontsize, zorder=50,
                bbox=dict(facecolor='white', boxstyle='round', edgecolor='white', pad=0.00001, alpha=0.8))
        ax.legend(loc='lr', fontsize=6)

    for i in range(3):
        plotss(ax[i], np.arange(1979,2020), ratio[i], position=[0.69,0.91,0.81])
    # plt.rcParams['font.sans-serif'] = ['simsun']
    # plt.rcParams['axes.unicode_minus'] = False
    ax.format(ylabel='风速/(m*s${^-}$${^1}$)',abc=True, abcloc='ul', abcstyle='(a)')
    # plt.show()
    plt.savefig('f2.png', dpi=600)

def f4():
    def fit_lines(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return beta_coeff
    def fit_lines_p(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return pvalue

    def get_ratio(filename):
        data = read_nc4(filename)
        ratio = []
        ratio_p =[]
        level_rank = [[2,3,4,5,6], [7,8,9,10], [11,12,13]]
        for levels in level_rank:
            adata = []
            for level in levels:
                sdata = []
                for year in range(data.shape[0]):
                    sdata.append(data[year, level, :, :][np.where(mask > 0.1)])
                ss = np.mean(np.array(sdata), axis=1)
                adata.append(ss)
            a_ss = np.mean(np.array(adata), axis=0)
            result = fit_lines(np.arange(1979,2020), a_ss)
            result_p = fit_lines_p(np.arange(1979, 2020), a_ss)
            ratio.append(result)
            ratio_p.append(result_p)
        return ratio, ratio_p

    ratio = []
    ratio_p = []
    # ss = get_ratio('f1')
    # ratio.append(ss[0])
    # ratio_p.append(ss[1])
    for i in range(4):
        ss = get_ratio('f4%i'%i)
        ratio.append(ss[0])
        ratio_p.append(ss[1])

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(ncols=4,figsize=[6.5,2.5])
    plt.subplots_adjust(top=0.92,
bottom=0.115,
left=0.095,
right=0.985,
hspace=0.2,
wspace=0.48)
    def plot_bar(ax, data, ablabel,ylabel, xlabel='',hatch='///', x=None, ylim=None):
        if x is None:
            x = np.arange(data.shape[0])
        ax.bar(x, np.array(data), color='white', hatch=hatch, edgecolor='black', width=0.6)
        ax.axhline(0, color='black',linewidth=1)
        ax.set_xticks(np.arange(3))
        ax.set_xticklabels(['TML',  'TT',  'SL'])
        ax.annotate(ablabel, [0.01, 1.02], xycoords='axes fraction')
        plt.rcParams['font.sans-serif'] = ['simsun']
        plt.rcParams['axes.unicode_minus'] = False
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        ax.set_ylim(ylim)

            # if len(xway) == 1:
            #     plot_bar(ax[si][sj], [np.array(ratio[i])[way]], '', '回归系数', hatch='////', x=xway)
            #     plot_bar(ax[si][sj], np.array(ratio)[4:], '', '回归系数', hatch='//', x=np.arange(4,17))

    print(ratio)
    plot_bar(ax[0], [np.array(ratio[0])[1]], '(a) 春季', '回归系数','春季', hatch='////', x=[1])
    plot_bar(ax[0], [0.01097961934186025, 0.055278604620425116], '', '回归系数','', hatch='//', x=[0,2], ylim=[0,0.08])

    plot_bar(ax[1], np.array(ratio[1]), '(b) 夏季', '','',hatch='//', ylim=[-0.07,0])

    plot_bar(ax[2], np.array(ratio[2]), '(c) 秋季', '','', hatch='////', ylim=[-0.07,0])

    plot_bar(ax[3], np.array(ratio[3]), '(d) 冬季', '','', hatch='//', ylim=[-0.07,0])

    # plt.show()
    plt.savefig('f4.png', dpi=600)

def f4_before():
    def season(svwnd, suwnd, i):
        svwnd = svwnd.groupby('time.year').mean()
        print(svwnd)
        suwnd = suwnd.groupby('time.year').mean()

        svwnd = svwnd['vwnd'].values
        suwnd = suwnd['uwnd'].values
        wnd = (svwnd**2 + suwnd**2)**(1/2)
        save_nc4(wnd, 'f4%s' %i)

    months = [[12,1,2],[3,4,5],[6,7,8],[9,10,11]]
    for i in range(4):

        season(vwnd.sel(time=np.isin(uwnd['time.month'], months[i])),
               uwnd.sel(time=np.isin(uwnd['time.month'], months[i])),
               i)
# f4_before()

def table():
    '''f1'''
    # 算年平均

    # vwnd = vwnd.groupby('time.year').mean()
    # uwnd = uwnd.groupby('time.year').mean()
    #
    # vwnd = vwnd['vwnd'].values
    # uwnd = uwnd['uwnd'].values
    # wnd = (vwnd**2 + uwnd**2)**(1/2)
    # save_nc4(wnd, 'f1')
    def fit_lines(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return beta_coeff
    def fit_lines_p(x, y):
        from scipy.stats import linregress
        (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
        line2 = x * beta_coeff + intercept
        return pvalue
    df = pd.DataFrame()
    def get_ratio(filename):
        data = read_nc4(filename)
        ratio = []
        ratio_p = []
        for level in range(data.shape[1]):
            sdata = []
            for year in range(data.shape[0]):
                sdata.append(data[year, level, :, :][np.where(mask > 0.1)])
            ss = np.mean(np.array(sdata), axis=1)
            result = fit_lines(np.arange(1979, 2020), ss)
            result_p = fit_lines_p(np.arange(1979, 2020), ss)


            ratio.append(result)
            ratio_p.append(result_p)
        df['%s b' % (filename)] = np.array(ratio)*10
        df['%s p'%(filename)] = np.array(ratio_p)
        return ratio, ratio_p


    # ss = get_ratio('f1')
    # ratio.append(ss[0])
    # ratio_p.append(ss[1])
    get_ratio('f1')
    for i in range(4):
        ss = get_ratio('f4%i'%i)
    df.to_excel('df11.xlsx')


f2()











